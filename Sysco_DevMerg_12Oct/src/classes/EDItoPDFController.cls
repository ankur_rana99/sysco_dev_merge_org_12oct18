public class EDItoPDFController
{
    public Invoice__c invDetail{get;set;}
    public InvDetail PDFInv{get;set;}
    public list<id> polist{get;set;}
    public list<Purchase_Order__c> poDetail{get;set;}
    public Purchase_Order__c assignedPO{get;set;}
    Public Decimal linetotal{get;set;}
    Public GRN__c assignedgrn{get;set;}
    Public List<EDI_Allowance_Charges__c> assignedEDIallow{get;set;}
    public EDItoPDFController()
    {
        string InvId=ApexPages.currentPage().getParameters().get('invid');
        system.debug('------InvId-------'+InvId);
        
        if(InvId!=null)
        {
                invDetail=[select id,Name,Purchase_Order__c,Vendor__c,OPCO_Code__c,Invoice_No__c,Invoice_Date__c,Total_Amount__c,Payment_Terms__c,Document_Type__c,Purchase_Order_Number__c,Ship_From_Vendor__c,(select id,Name,GRN__r.GRN_Date__c,Catch_Weight__c,GL_Code__c,Total_Freight_Cases__c,Quantity__c,UOM__c,Rate__c,Product_Service_Description__c,Amount__c,Product_Service_No__c,PO_Line_Item__c,Pack__c,Size__c,Item_Catch_Weight_Indicator__c from Invoice_Line_Items_Invoice__r),(select Purchase_Order__c from Invoice_PO_Details__r),(Select id,Allowance_Amount__c,Allowance_Charge_Type__c from EDI_Allowance_Charges__r),Purchase_Order__r.id,Purchase_Order__r.Name,Purchase_Order__r.External_Key__c,Purchase_Order__r.OC_Number__c,Purchase_Order__r.PO_Date__c,Purchase_Order__r.PO_No__c,Vendor__r.id,Vendor__r.Name,Vendor__r.City__c,Vendor__r.Address1__c,Vendor__r.Address2__c,Vendor__r.Address3__c,Vendor__r.Address4__c,Vendor__r.Unique_Key__c,Vendor__r.Vendor_No__c ,Vendor__r.Zip_Postal_Code__c,Vendor__r.Country__c from Invoice__c where id=:InvId];
            polist=new list<id>();
            if((invDetail.Invoice_Line_Items_Invoice__r) != null &&  (invDetail.Invoice_Line_Items_Invoice__r).size()>0){
            for(Invoice_PO_Detail__c ip:invDetail.Invoice_PO_Details__r)
            {
                polist.add(ip.Purchase_Order__c);
            }
            
            if(polist.size()>0)
            {
                poDetail=[select id, Name, External_Key__c, OC_Number__c, PO_Date__c,PO_No__c,Ship_From_Vendor__c,OPCO_Code__c,Pickup_Allowance__c,MSC_Freight_Charges__c,MSC_Merch_Charges__c,(select id,Name,Product_Service_No__c,Product_Service_Description__c,UOM__c,Size__c,Pack__c from PO_Line_Item_PO__r),(select id,name,Freight_Charges__c from GRNs__r) from Purchase_Order__c where id in :polist];
            }
            else
            {
                poDetail=new list<Purchase_Order__c>();
            }
            if(poDetail != null && poDetail.size()==1){
            if(poDetail.size()>1){
                for(Purchase_Order__c po : poDetail){
                    if(invDetail.Purchase_Order_Number__c == po.PO_No__c){
                        assignedPO = po;
                        for(GRN__c grn : assignedPO.GRNs__r){
                            assignedgrn = grn;
                            continue;
                        }
                        breaK;
                    }
                }
            }
            else{
                
                assignedPO = poDetail[0];
                
               for(GRN__c grn : assignedPO.GRNs__r){
                            assignedgrn = grn;
                            continue;
                        }
                
                }
                
            try{
            if(assignedPO.Ship_From_Vendor__c !=null){
                Account acc = [Select id,name,Vendor_Name__c,Vendor_No__c,OPCO_Code__c from Account Where Vendor_No__c =: assignedPO.Ship_From_Vendor__c and OPCO_Code__c =: assignedPO.OPCO_Code__c];
            	invDetail.Ship_From_Vendor__c = acc.name;
            }
            }
                Catch(Exception exc){
                    System.debug('Ship from vendor is wrong');
                    
                }
            
            }
            
           
            /*for(EDI_Allowance_Charges__c ediallow : invDetail.EDI_Allowance_Charges__r){
                    assignedEDIallow.add(ediallow);
                }*/
                assignedEDIallow = invDetail.EDI_Allowance_Charges__r;
            if((invDetail.Invoice_Line_Items_Invoice__r) != null &&  (invDetail.Invoice_Line_Items_Invoice__r).size()>0){
                PDFInv=new InvDetail(invDetail,poDetail,assignedgrn,assignedEDIallow);
            }
            }
            else{
                PDFInv=new InvDetail(null,null,null,null);
            }
            
        }
        else
        {
        }
    }
    public class InvDetail
    {
        public invoice__c inv{get;set;}
        public list<Invoice_Line_Item__c> invLine{get;set;}
        public list<Purchase_Order__c> poList{get;set;}
        Public OPCO_Code__c opco{get;set;}
        Public list<String> Size{get;set;}
        Public list<String> pack{get;set;}
        Public GRN__c grnh{get;set;}
        Public list<EDI_Allowance_Charges__c> EDIallowance{get;set;}
        public InvDetail(invoice__c invdeatil, list<Purchase_Order__c> podetail,GRN__c grn,list<EDI_Allowance_Charges__c> assignedEDIallow)
        {
            inv=invdeatil;
            invLine=new list<Invoice_Line_Item__c>();
            poList=podetail;
            grnh=grn;
            EDIallowance=assignedEDIallow;
            /*if(invdeatil.EDI_Allowance_Charges__r !=null){
                EDIallowance=assignedEDIallow;
            }*/
            System.debug('inv.OPCO_Code__c :'+inv.OPCO_Code__c);
            opco = [Select name,AddressStreet__c,City__c,OPCO_Code__c,State__c,OPCO_Name__c,Zip__c from OPCO_Code__c where id =: inv.OPCO_Code__c];
            System.debug('opco :'+opco);
            if(invdeatil.Invoice_Line_Items_Invoice__r.size()>0)
            {
                for(Invoice_Line_Item__c Il:invdeatil.Invoice_Line_Items_Invoice__r)
                {
                    for(Purchase_Order__c po:poList)
                    {
                        for(PO_Line_Item__c pol:po.PO_Line_Item_PO__r)
                        {
                            if(pol.Product_Service_No__c==Il.Product_Service_No__c)
                            {
                                Il.PO_Line_Item__c = pol.Id;
                                Il.Product_Service_Description__c = pol.Product_Service_Description__c;
                                Il.UOM__c = pol.UOM__c;
                                Il.Size__c = pol.Size__c;
                                Il.Pack__c = pol.Pack__c;
                                break;
                            }
                        }                        
                    }
                    invLine.add(Il);
                }
            }
        }
    }
}