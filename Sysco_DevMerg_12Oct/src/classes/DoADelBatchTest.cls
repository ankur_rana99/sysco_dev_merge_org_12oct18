@isTest
public class DoADelBatchTest {
      public static testMethod void testFirst(){
          
        List<DOA_Matrix__c> doaNeedsTobeDel=new List<DOA_Matrix__c>();
        DOA_Matrix__c dao=new DOA_Matrix__c();
        dao.Active__c=false;
        dao.DoA_Title__c='abc';
        doaNeedsTobeDel.add(dao);
        insert doaNeedsTobeDel;
          
        DoADelBatch obj=new DoADelBatch(doaNeedsTobeDel);
        Database.executeBatch(obj);
          
      }
}