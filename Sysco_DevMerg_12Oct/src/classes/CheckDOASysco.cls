global class CheckDOASysco {

    static List<Invoice__c> errorInvoice = new List<Invoice__c>();  
    static Map<String, Decimal> invoiceToCurrentCycle = new Map<String, Decimal> ();
    static Map<String, List<DOA_Matrix__c>> funTranVsDOAMap= new Map<String, List<DOA_Matrix__c>>();
    static Map<string, DOA_Matrix__c> cpasDoaMap = new Map<string, DOA_Matrix__c>();
    static Map<string, List<DOA_Matrix__c>> nonCpasDoaMap = new Map<string, List<DOA_Matrix__c>>();
    static Map<Id, User_Master__c> userVsSupMap = new Map<Id, User_Master__c>(); 
    static DOA_Setting__c DOAConfig = new DOA_Setting__c();
    Static Map<Id,Id> invVsReqMap = new Map<Id,Id>();
    Static Map<Id,String> errormessage = new Map<Id,String>();
    @InvocableMethod(label = 'CheckDOASysco' description = 'Check for DOA Limit and route to new approver')
    global static List<Invoice__c> checkUserDOA(List<Invoice__c> invList) {
        System.debug('Log---CheckDOASysco');
        
        try {
            set<id> invidset = new set<id> ();
            set<id> opcoCodeSet = new set<id> ();
            set<Id> invCurAppSet = new set<Id> ();
            //Map<Id,Id> invVsReqMap = new Map<Id,Id>();
            Map<String,Object> entityVsThreshold = new Map<String,Object>();
            //Map<Id,string> opcovalue = new Map<Id,string>();
        //  entityVsThreshold.put('Corporate',400001);
        //  entityVsThreshold.put('251M-650M',150000);
        //  entityVsThreshold.put('250M',150000);
        //  entityVsThreshold.put('SBS',150000);
        //  entityVsThreshold.put('Over 650M',150000);
            for (DOA_Setting__c doasetting :[SELECT Id, Name, Threshold_Amount__c, Amount_Field__c, First_Approver_Selection_Field__c, DOA_Invoice_Field_Mapping__c,Entity_Threshold_Mapping__c FROM DOA_Setting__c]) {
                DOAConfig = doasetting;
                entityVsThreshold = (Map<String, Object>) JSON.deserializeUntyped(doasetting.Entity_Threshold_Mapping__c);
            }

            Boolean isCurrentApproverExit = false;
            for (invoice__c inv : invList) {
            System.debug(inv);
                InvIdSet.add(inv.Id);
                if (inv.Current_Approver__c != null)
                {
                    invCurAppSet.add(inv.Current_Approver__c);
                    invCurAppSet.add((Id)inv.get(DOAConfig.First_Approver_Selection_Field__c));
                    
                }else{
                    if (DOAConfig.First_Approver_Selection_Field__c != null && DOAConfig.First_Approver_Selection_Field__c != '') {
                        invCurAppSet.add((Id)inv.get(DOAConfig.First_Approver_Selection_Field__c));
                        //opcovalue.put((Id)inv.get(DOAConfig.First_Approver_Selection_Field__c),inv.OPCO_Code__c);
            opcoCodeSet.add(inv.OPCO_Code__c);
                    }
                }       
                invVsReqMap.put(inv.Id,inv.Requestor_Buyer__c); 
            }

            //To prepare map for User and its supervisor
                
            if (invCurAppSet.size() > 0)
            {
                for (User_Master__c userMaster : [SELECT Id, DAP_Title__c, Entity__c, OPCO_Code__c, Title_Type__c, Supervisor__c ,Supervisor__r.Status__c , Status__c  FROM User_Master__c  where id = :invCurAppSet and Status__c IN ('Active')])
                {
                    userVsSupMap.put(userMaster.Id, userMaster);
                    
                    opcoCodeSet.add(userMaster.OPCO_Code__c);
                    /*if(userMaster.OPCO_Code__c != opcovalue.get(userMaster.Id)){
                        opcoCodeSet.add(opcovalue.get(userMaster.Id));
                    }*/
                }
            }
            System.debug('userVsSupMap : '+userVsSupMap);
            System.debug('opcoCodeSet : '+opcoCodeSet);
            System.debug('InvIdSet : '+InvIdSet);
            List<Approval_Detail__c> appList = [SELECT Id, Name, Approver_User__c, Is_Threshold__c, Approval_Cycle__c, Invoice__c, Approver_Action__c FROM Approval_Detail__c WHERE Invoice__c IN :InvIdSet];
             System.debug('appList : '+appList);
            Map<string, Approval_Detail__c> appMap = new Map<string, Approval_Detail__c> ();
            Map<string, Integer> appCntMap = new Map<string, Integer> ();
            Map<string, Integer> appCntUserWiseMap = new Map<string, Integer> ();

            Map<string, DOA_Matrix__c> doaMap = new Map<string, DOA_Matrix__c>();
            
            for (DOA_Matrix__c doaObj : [SELECT Id, Active__c, Approver_User__c, is_Functional__c, Supervisor__c, DoA_Title__c, Entity__c, DOA_Limit__c,OPCO_Code__c FROM DOA_Matrix__c where Active__c = true and OPCO_Code__c = :opcoCodeSet order by DOA_Limit__c])
            {
                doaMap.put(doaObj.Approver_User__c+'~'+doaObj.Entity__c+'~'+doaObj.DoA_Title__c,doaObj);
                if (!doaObj.is_Functional__c)
                {
                    //For Non-CPAS invoice finance user 
                    if (nonCpasDoaMap.containsKey(doaObj.OPCO_Code__c)){
                        nonCpasDoaMap.get(doaObj.OPCO_Code__c).add(doaObj);
                    }else{
                        List<DOA_Matrix__c> doaList = new List<DOA_Matrix__c>();
                        doaList.add(doaObj);
                        nonCpasDoaMap.put(doaObj.OPCO_Code__c,doaList);
                    }
                    
                    //For CPAS invoice finance user 
                    if (doaObj.DoA_Title__c== 'OpCo CFO'){
                        cpasDoaMap.put(doaObj.OPCO_Code__c,doaObj);
                    }                   
                }
            } 
        //  Map<Id, User_Master__c> userVsSupMap = new Map<Id, User_Master__c>(); 
        
            System.debug('cpasDoaMap-->' + cpasDoaMap);

                
                
            System.debug('doaMap-->' + doaMap);

                
                
            for (Approval_Detail__c appObj : appList) {
                appMap.put(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c, appObj);
                if (appCntMap.containsKey(appObj.Invoice__c+'~'+ appObj.Approval_Cycle__c))
                {
                    Integer i = appCntMap.get(appObj.Invoice__c+'~'+ appObj.Approval_Cycle__c);
                    appCntMap.put(appObj.Invoice__c+'~'+ appObj.Approval_Cycle__c, ++i);        
                }else{
                    appCntMap.put(appObj.Invoice__c+'~'+ appObj.Approval_Cycle__c, 1);
                }
                if (appCntUserWiseMap.containsKey(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c))
                {
                    Integer i = appCntUserWiseMap.get(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c);
                    appCntUserWiseMap.put(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c, ++i);     
                }else{
                    appCntUserWiseMap.put(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c, 1);
                }
            }
            system.debug('appCntMap:' + appCntMap);
            system.debug('appMap:' + appMap);
            
            
                                            
            List<Approval_Detail__c> appDetUpdate = new List<Approval_Detail__c> ();
            for (Invoice__c inv : invList) {
                
                System.debug('inv.User_Action__c : ' + inv.User_Action__c);
                System.debug('inv.Current_Approver__c : ' + inv.Current_Approver__c);
                System.debug('inv.Current_State__c : ' + inv.Current_State__c);
                System.debug('inv.Current_Approver__c : ' + inv.Current_Approver__c);
                System.debug('inv.Current_Cycle__c : ' + inv.Current_Cycle__c);
                System.debug('inv.Pending_Approval__c : ' + inv.Pending_Approval__c);
                System.debug('inv.Current_State_Assign_Date__c : ' + inv.Current_State_Assign_Date__c);
                //---- First Approver selection ----//
                if (inv.Current_Approver__c == null) {

                    Approval_Detail__c appObj = new Approval_Detail__c();
                    appObj.Invoice__c = inv.Id;
                    appObj.Status__c = 'Pending';
                    if (DOAConfig.First_Approver_Selection_Field__c != null && DOAConfig.First_Approver_Selection_Field__c != '') {
                        appObj.Approver_User__c = String.valueOf(inv.get(DOAConfig.First_Approver_Selection_Field__c));
                    }

                    if (appList.size() > 0) {
                        appObj.Approval_Cycle__c = inv.Current_Cycle__c + 1;
                    } else {
                        appObj.Approval_Cycle__c = inv.Current_Cycle__c;
                    }
                    invoiceToCurrentCycle.put(inv.Id,appObj.Approval_Cycle__c);
                    appDetUpdate.add(appObj);

                } else {

                    Id invId = inv.Id;

                    Integer cycleNo = (Integer) inv.Current_Cycle__c;
                    //Integer appCnt =  database.countQuery('SELECT count() FROM Approval_Detail__c WHERE Invoice__c = :invId and Approval_Cycle__c = :cycleNo');
                    Integer appCnt = appCntMap.get(invId+'~'+cycleNo);
                    String action = (inv.User_Action__c != null ? ( (inv.User_Action__c == 'Approve' || inv.User_Action__c == 'Send For Approval' || inv.User_Action__c == 'Submit' ) ? 'Approved' : (inv.User_Action__c == 'Reject' ? 'Rejected' : '')) : '');
                    action = (action == '' ? ( inv.User_Action__c == 'Send Back to AP' ? 'Recalled' : '') : action);
                    System.debug('action : ' + action);
                    System.debug('@@ inv.Current_Approver__c : ' + inv.Current_Approver__c);
                    System.debug('@@ inv.Current_Approver__c :- ' + inv.Current_Approver__c + '~' + integer.valueof(inv.Current_Cycle__c));
                    Approval_Detail__c appObj = appMap.get(invId+'~'+inv.Current_Approver__c + '~' + integer.valueof(inv.Current_Cycle__c));
                    system.debug('appObj:' + appObj);
                    boolean isThresholdApproval = appObj.Is_Threshold__c;
                    if (appObj != null) {
                        appObj.Status__c = action;
                        appObj.Approver_Action__c = inv.User_Action__c;
                        appDetUpdate.add(appObj);
                    }
                    
                    Decimal cycle = 1;
                    if (appCntUserWiseMap.containsKey(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c) &&
                        appCntUserWiseMap.get(appObj.Invoice__c+'~'+appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c) >=2)
                    {
                        cycle = inv.Current_Cycle__c + 1;
                    }else if (inv.Current_Cycle__c != null )
                    {
                        cycle = inv.Current_Cycle__c;
                    }
                    invoiceToCurrentCycle.put(invId,cycle);

                    Decimal compareAmount = DOAConfig.Amount_Field__c !=null  && inv.get(DOAConfig.Amount_Field__c) != null ? Decimal.valueOf(String.valueOf(inv.get(DOAConfig.Amount_Field__c))) : inv.Total_Amount__c;

                    if ( inv.WorkType__c == 'CPAS' && (inv.User_Action__c == 'Approve' || inv.User_Action__c == 'Submit'))
                    {
                        if (inv.Current_Approver__c != inv.Requestor_Buyer__c ){ //&& appCnt == 2
                            Continue;
                        }
                        try {           
                            populateAppDetForFinance(appDetUpdate,inv,compareAmount);
                        }
                        catch (CustomCRUDFLSException  e){
                            appObj = new Approval_Detail__c();
                                    appObj.Invoice__c = inv.Id;
                                    appObj.Status__c = 'Pending';
                                    appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                                    //appObj.Approver_User__c = curApprover;
                                    appObj.Approval_Cycle__c = cycle;
                                    appDetUpdate.add(appObj);
                              errormessage.put(inv.Id,'OpcoCFO not found');
                           // appDetUpdate.remove(appDetUpdate.size()-1);
                        }
                        Continue;                       
                    }
                                                            
                    Id curApprover = inv.Current_Approver__c;
                    System.debug('curApprover :'+curApprover);
                    System.debug(userVsSupMap.get(curApprover));
                    System.debug(doaMap);
                    System.debug(doaMap.get(curApprover + '~' + userVsSupMap.get(curApprover).Entity__c+'~'+userVsSupMap.get(curApprover).DAP_Title__c));
                    DOA_Matrix__c doa = doaMap.get(curApprover + '~' + userVsSupMap.get(curApprover).Entity__c+'~'+userVsSupMap.get(curApprover).DAP_Title__c); 
                    Boolean doaFlow = false;
                    System.debug('supervisor is :'+userVsSupMap.containsKey(userVsSupMap.get(curApprover).Supervisor__c));
                    System.debug('supervisor details :'+userVsSupMap.get(curApprover).Supervisor__r.Status__c);
                    if (userVsSupMap.containsKey(curApprover) && userVsSupMap.get(curApprover).Supervisor__r.Status__c == 'Active'){
                        curApprover = userVsSupMap.get(curApprover).Supervisor__c;                      
                    }
                    else{
                        System.debug('Supervisor Not Found In User Master.');
                        appDetUpdate.remove(appDetUpdate.size()-1);
                        Invoice__c errorInv = new Invoice__c(Id = invId, Exception_Reason__c='Supervisor Missing');
                        errorInvoice.add(errorInv);
                        appObj = new Approval_Detail__c();
                        appObj.Invoice__c = inv.Id;
                        appObj.Status__c = 'Pending';
                        appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                        //appObj.Approver_User__c = curApprover;
                         appObj.Approval_Cycle__c = cycle;
                         appDetUpdate.add(appObj);
                         errormessage.put(inv.Id,'Supervisor Missing');
                        Continue;
                        //throw new CustomCRUDFLSException('Supervisor Missing');
                    }
                    //In case of DOA not found, get next user and process current as approved and make entry for next one as pending
                    if (doa == null)
                        doaFlow = true;
            //      else                        
                //      if (inv.User_Action__c != 'Reject' && !doa.is_Functional__c)
                    //      Continue;   
                    //In case of Individual Payment Request - Two level approval is needed
                    if(inv.WorkType__c == 'Individual Payment Request' && appCntMap.get(invId+'~1') == 1){
                        doaFlow = true;
                    }
                        System.debug('inv.User_Action__c :'+compareAmount);
                        System.debug('isThresholdApproval :'+isThresholdApproval);
                        System.debug('doaFlow :'+(compareAmount >= (Decimal)(entityVsThreshold.get(userVsSupMap.get(inv.Current_Approver__c).Entity__c))));
                    System.debug('doa :'+doa);
                     System.debug('I am here before if');                   
                    if (doaFlow || doa != null) {
                        if ( (inv.User_Action__c == 'Approve' || inv.User_Action__c == 'Send For Approval' || inv.User_Action__c == 'Submit') 
                        && ( ( !isThresholdApproval && (doaFlow || doa.DOA_Limit__c == null || doa.DOA_Limit__c == 0 || doa.DOA_Limit__c < compareAmount ) ) )) {
                            System.debug('daofound');
                            if (curApprover != null) {
                                appObj = new Approval_Detail__c();
                                appObj.Invoice__c = inv.Id;
                                appObj.Status__c = 'Pending';
                                appObj.Approver_User__c = curApprover;
                                appObj.Approval_Cycle__c = cycle;
                                appDetUpdate.add(appObj);

                            } /*else {
                                System.debug('Supervisor Not Found.');
                                appDetUpdate.remove(appDetUpdate.size()-1);
                                Invoice__c errorInv = new Invoice__c(Id = invId, Exception_Reason__c='Supervisor Missing');
                                
                                errorInvoice.add(errorInv);
                                Continue;
                                //throw new CustomCRUDFLSException('Supervisor Missing');
                            }*/
                            
                            //Adding for new changes by Chiranjeevi
                            else{
                                System.debug('Supervisor Not Found.');
                                    //appDetUpdate.remove(appDetUpdate.size()-1);
                                    Invoice__c errorInv = new Invoice__c(Id = invId, Exception_Reason__c='Supervisor Missing');
                                    //errorInv.User_Action__c='Reject';
                                    errorInvoice.add(errorInv);
                                    appObj = new Approval_Detail__c();
                                    appObj.Invoice__c = inv.Id;
                                    appObj.Status__c = 'Pending';
                                    appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                                    //appObj.Approver_User__c = curApprover;
                                    appObj.Approval_Cycle__c = cycle;
                                    appDetUpdate.add(appObj);
                                  errormessage.put(inv.Id,'Supervisor Missing');
                                Continue;
                                }
                        }
                        
                        
                        /*else if((inv.User_Action__c == 'Approve' || inv.User_Action__c == 'Send For Approval' || inv.User_Action__c == 'Submit') 
                        && doa.DOA_Limit__c >= compareAmount && compareAmount > (Decimal)(entityVsThreshold.get(userVsSupMap.get( ((User_Master__c)inv.get(DOAConfig.First_Approver_Selection_Field__c)).Id ).Entity__c  )) && !isThresholdApproval && doa.DoA_Title__c != 'CFO'){ //Thresold Check
                    System.debug('I am here inside if');*/
                        else if((inv.User_Action__c == 'Approve' || inv.User_Action__c == 'Send For Approval' || inv.User_Action__c == 'Submit') 
                        && doa.DOA_Limit__c >= compareAmount && compareAmount > (Decimal)(entityVsThreshold.get(userVsSupMap.get((Id)inv.get(DOAConfig.First_Approver_Selection_Field__c)).Entity__c  )) && !isThresholdApproval && doa.DoA_Title__c != 'CFO'){ //Thresold Check
                    System.debug('I am here inside if');
                                try 
                                {           
                                    populateAppDetForFinance(appDetUpdate,inv,compareAmount);
                                }
                                catch (CustomCRUDFLSException  e)
                                {
                                    //appDetUpdate.remove(appDetUpdate.size()-1);
                                    appObj = new Approval_Detail__c();
                                    appObj.Invoice__c = inv.Id;
                                    appObj.Status__c = 'Pending';
                                    appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                                    //appObj.Approver_User__c = curApprover;
                                    appObj.Approval_Cycle__c = cycle;
                                    appDetUpdate.add(appObj);
                                    errormessage.put(inv.Id,'Finance DOA Missing');
                                    Continue;
                                }
                        
                            
                        }else if(inv.User_Action__c == 'Reject' && inv.Current_Approver__c != invVsReqMap.get(inv.Id)){
                            appObj = new Approval_Detail__c();
                            appObj.Invoice__c = inv.Id;
                            appObj.Status__c = 'Pending';
                            appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                            appObj.Approval_Cycle__c = cycle;
                            appDetUpdate.add(appObj);
                        }
                    } else {
                        System.debug('DOA Not Found.');
                        appDetUpdate.remove(appDetUpdate.size()-1);
                        Invoice__c errorInv = new Invoice__c(Id = invId, Exception_Reason__c='Functional DOA Missing');
                        errorInvoice.add(errorInv);
                        appObj = new Approval_Detail__c();
                        appObj.Invoice__c = inv.Id;
                        appObj.Status__c = 'Pending';
                        appObj.Approver_User__c = invVsReqMap.get(inv.Id);
                        //appObj.Approver_User__c = curApprover;
                        appObj.Approval_Cycle__c = cycle;
                        appDetUpdate.add(appObj);
                        errormessage.put(inv.Id,'Functional DOA Missing');
                        Continue;
                        //throw new CustomCRUDFLSException('Functional DOA Missing');
                        //get supervisour
                        //

                    }
                }

            }
            System.debug(invoiceToCurrentCycle);
            upsert new List<Approval_Detail__c>(appDetUpdate);

            Map<String, String> invoiceToStatus = new Map<String, String> ();
            Map<String, String> invoiceToCurrentApp = new Map<String, String> ();
            List<Approval_Detail__c> appDetail = [Select Id, Approval_Cycle__c, Approver_Action__c, Approver_User__c, Invoice__c, Status__c, Invoice__r.Current_Cycle__c From Approval_Detail__c Where Invoice__c IN :InvIdSet and Status__c != 'Recalled'];
            for (Approval_Detail__c apd : appDetail) {
                if (apd.Approval_Cycle__c == invoiceToCurrentCycle.get(apd.Invoice__c))
                if (invoiceToStatus.containsKey(apd.Invoice__c)) {
                    if (invoiceToStatus.get(apd.Invoice__c) != 'Rejected' && invoiceToStatus.get(apd.Invoice__c) != 'Pending' 
                    && invoiceToStatus.get(apd.Invoice__c) != 'Send For Approval' && invoiceToStatus.get(apd.Invoice__c) != 'Submit' 
                    && !apd.Status__c.equals('Approved')) {
                        if (apd.Status__c == 'Rejected' && apd.Approver_User__c != invVsReqMap.get(apd.Invoice__c))
                            continue;
                        invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
                        invoiceToCurrentApp.put(apd.Invoice__c, apd.Approver_User__c);
                    }
                    System.debug('invoiceToStatus-->' + invoiceToStatus);
                } else {
                    if (apd.Status__c == 'Rejected' && apd.Approver_User__c != invVsReqMap.get(apd.Invoice__c))
                        continue;
                    invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
                    invoiceToCurrentApp.put(apd.Invoice__c, apd.Approver_User__c);
                    System.debug('invoiceToStatus-->' + invoiceToStatus);                                       
                }
            }
            List<Invoice__c> needToUpdate = new List<Invoice__c> ();
            Set<String> appdInv = new Set<String> ();
            if (!invoiceToStatus.isEmpty()) {
                Invoice__c tmp;
                for (String id : invoiceToStatus.keySet()) {
                    system.debug('inside for loop');
                    boolean isexception = false;
                    tmp = new Invoice__c();
                    tmp.Id = id;
                    /*for(Invoice__c invtemp : errorInvoice){
                            if(invtemp.id == id){
                                isexception = true;
                                System.debug('inside isexception ');
                                break;
                            }
                        }*/
          if(errormessage.containsKey(id)){
            isexception = true;
          }
                    if(!isexception){
                    tmp.Current_Cycle__c = invoiceToCurrentCycle.get(id);
                    tmp.Approval_Status__c = invoiceToStatus.get(id);
                    tmp.Current_Approver__c = invoiceToCurrentApp.get(id);
                    if (invoiceToStatus.get(id) == 'Approved') {
                        system.debug('inside for Approved');
                        
                        appdInv.add(id);
                        tmp.Current_Approver__c = null;
                        tmp.User_Action__c = 'Send For Posting'; 
                        
                    }else if(invoiceToStatus.get(id) == 'Rejected'){
                        system.debug('inside for Reject');
                        if (tmp.Current_Approver__c != invVsReqMap.get(id))
                        {
                            tmp.Current_Approver__c = invVsReqMap.get(id);
                        }else{
                            tmp.Current_Approver__c = null;
                            tmp.Approval_Status__c = '';
                        }
                      
                    }
                        
                    
                }
                    else{
                            System.debug('inside final else');
                            //tmp.User_Action__c = 'Reject';
                            tmp.Current_State__c = 'Pending Approval Review';
                            tmp.Exception_Reason__c='Supervisor Missing';
                          tmp.Exception_Reason__c=errormessage.get(id);
                            tmp.Current_Approver__c=invVsReqMap.get(id);
                            //tmp.Current_Approver__c = invoiceToCurrentApp.get(id);
                        }
                    needToUpdate.add(tmp);
                }
            }
                        
            System.debug('@@needToUpdate: ' + needToUpdate);
            
            if (needToUpdate.size() > 0) {
                System.debug('Inside properupdate');
                upsert needToUpdate;        
            }
           /* if (errorInvoice.size() > 0)
            {
                System.debug('Inside exception update');
                upsert errorInvoice;
            }*/
        } catch(exception ex) {
            System.debug('Exception110');
            system.debug('error' + ex + '----' + ex.getLinenumber());           
        }
        return null;
    }

    public static void populateAppDetForFinance(List<Approval_Detail__c> appDetUpdate,Invoice__c inv,Decimal compareAmount){
        //to populate finance dao
        Id curApprovaer = inv.Current_Approver__c;
        //User_Master__c baseUser = (User_Master__c)inv.get(DOAConfig.First_Approver_Selection_Field__c);
        id baseUser =invVsReqMap.get(inv.Id);
        Boolean isFound = false;        
        Id opcoCfoUser = null;
        boolean isThreshold = false;

        if(inv.WorkType__c == 'CPAS') { 
           /* if (cpasDoaMap.containsKey(userVsSupMap.get(curApprovaer).OPCO_Code__c))
            {
                opcoCfoUser = cpasDoaMap.get(userVsSupMap.get(curApprovaer).OPCO_Code__c).Approver_User__c;             
            }*/
            if (cpasDoaMap.containsKey(inv.OPCO_Code__c))
            {
                opcoCfoUser = cpasDoaMap.get(inv.OPCO_Code__c).Approver_User__c;             
            }
                
        }
        else if(userVsSupMap.get(appDetUpdate.get(appDetUpdate.size()-1).Approver_User__c).Title_Type__c == 'Finance' ){

            opcoCfoUser = userVsSupMap.get(appDetUpdate.get(appDetUpdate.size()-1).Approver_User__c).Supervisor__c;
            isThreshold = true;

        }
        /*else if(nonCpasDoaMap.containsKey(userVsSupMap.get(baseUser.Id).OPCO_Code__c)){

            for (DOA_Matrix__c doaObj : nonCpasDoaMap.get(userVsSupMap.get(baseUser.Id).OPCO_Code__c)){
                if (doaObj.DOA_Limit__c >= compareAmount){
                    opcoCfoUser = doaObj.Approver_User__c;
                    isThreshold = true;
                    break;
                }
            }
                 
        }*/
        else if(nonCpasDoaMap.containsKey(userVsSupMap.get(baseUser).OPCO_Code__c)){

            for (DOA_Matrix__c doaObj : nonCpasDoaMap.get(userVsSupMap.get(baseUser).OPCO_Code__c)){
                if (doaObj.DOA_Limit__c >= compareAmount){
                    opcoCfoUser = doaObj.Approver_User__c;
                    isThreshold = true;
                    break;
                }
            }
                 
        }
            System.debug('opcoCfoUser : '+opcoCfoUser);
        System.debug('curApprovaer : '+curApprovaer);
        if (opcoCfoUser != null && opcoCfoUser != curApprovaer ){
                System.debug(opcoCfoUser);
                Approval_Detail__c appObj = new Approval_Detail__c();
                appObj.Invoice__c = inv.Id;
                appObj.Status__c = 'Pending';
                appObj.Approver_User__c = opcoCfoUser;
                appObj.Approval_Cycle__c = invoiceToCurrentCycle.get(inv.Id);
                appObj.Is_Threshold__c = isThreshold;
                appDetUpdate.add(appObj);
        }
    
        if (opcoCfoUser == null )
        {
            System.debug('Finance User Not Found.');
            Invoice__c errorInv = new Invoice__c(Id = inv.Id, Exception_Reason__c='Finance DOA Missing');
            errorInvoice.add(errorInv);
            throw new CustomCRUDFLSException('Finance DOA Missing');
        }   
    }


}