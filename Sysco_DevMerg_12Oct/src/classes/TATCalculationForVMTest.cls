@isTest
public class TATCalculationForVMTest{

    public static testMethod void testFirst(){
        Vendor_Maintenance__c vm = new Vendor_Maintenance__c();
        vm.Name__c = 'abs';
        insert vm;
        list<Vendor_Maintenance__c>vmList = new list<Vendor_Maintenance__c>();
        vmList.add(vm);
        TATCalculationForVM.calculateStatewiseTAT(vmList);
    }
}