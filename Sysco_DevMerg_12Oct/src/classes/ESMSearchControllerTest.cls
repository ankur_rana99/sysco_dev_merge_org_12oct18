@isTest
public class ESMSearchControllerTest {
    public static testMethod void testFirst(){
        SearchConfiguration__c config=new SearchConfiguration__c();
        config.Name='Temp';
        config.Clone__c=true;
        config.Create__c=true;
        config.CreateInquiry__c=true;
        config.AccessProfile__c='System Administrator,Processor,Exception Resolution';
        config.BulkAssignmentUserType__c='Processor,Administrator';
        config.CriteriaFieldSetName__c='Addi_UI_Common';
        config.ObjectAPIName__c='Invoice__c';
        config.POFlip__c=false;
        config.FilterCriteria__c='Current_State__c=\'Rejected\'';
        config.ResultFieldSetName__c='Addi_UI_Common';
        insert config;
        
        System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Profile';
        sysconfig.Sub_Module__c='Supplier_Profile_ID';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
        
        Map < String, String > filterMap=new   Map < String, String >();
        List < String > selectedQueue=new List < String >();
        ESMSearchController.getCases('00Bm00000017h6MEAQ',config.Name, 1, 15, 'LastModifiedDate', 'desc', selectedQueue, filterMap , '','{\"Current_State__c\":\"Ready For Processing\",\"Document_Type__c\":\"\",\"WorkType__c\":\"\",\"Invoice_Type__c\":\"\",\"Input_Source__c\":\"\"}');
        ESMSearchController.getUserInfo();
        ESMSearchController.getObjectName('Temp');
        ESMSearchController.getQueueDropdown();
        ESMSearchController.getSortFieldList('Temp');
        ESMSearchController.getFieldDropdown('Temp');
        ESMSearchController.getViews('Temp');
        ESMSearchController.getSerachConfiguration('Temp');
        ESMSearchController.getFieldsetMember('Temp');
        ESMSearchController.bulkPermissionCheckForUser('abc');
       	ESMSearchController.isVendor();
    }
}