@isTest
public class SyscoGetSpendCategoriesDataTest{
    
    public static testMethod void testFirst(){    
        WorkDay_EndPoints__c sysWED=new WorkDay_EndPoints__c();
        sysWED.Name='SpendCategories';
        sysWED.isfirstrun__c = 'Y';
        sysWED.Endpoint_URL__c='callout:Sysco_Spend_Categories/ccx/service/customreport2/sysco4/ISU%20FDM%20Spend%20Categories/Foundation_Data_-_Spend_Categories';
        insert sysWED;
                 
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());
        
        SyscoGetSpendCategoriesData getsc  = new SyscoGetSpendCategoriesData();
        Integer batchSize = 200; 
        system.database.executebatch(getsc,batchSize);       
        Test.StopTest();  
    }    
}