@isTest
public class EmailToCaseManagerTest{
    static String namespace = UtilityController.esmNameSpace;    
    @testSetup 
    static void setupTestData() {
        EmailTemplate validEmailTemplate = new EmailTemplate();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'testTemplate';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            
            insert validEmailTemplate;
        }
        ESMDataGenerator.createInvoice('EDI');
        ESMDataGenerator.createIgnoreRule(validEmailTemplate.id);
        ESMDataGenerator.createAcceptRule(validEmailTemplate.id);
        ESMDataGenerator.createAcceptRuleInvoice(validEmailTemplate.id);
        ESMDataGenerator.createExitingCaseRule(validEmailTemplate.id);
        ESMDataGenerator.createIdentifyExitingCaseRule(validEmailTemplate.id);
        
        //Create Default account object
        Account acc=new Account();
        acc.Name=ESMConstants.DEFAULT_ACCOUNT_NAME;
        insert acc;
        
        CaseManager__c caseObj=new CaseManager__c();
        //caseObj.Process__c='AP';
        caseObj.WorkType__c='Non-PO Invoices';
        caseObj.Document_Type__c='Electricity';
        //caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Subject__c='Test';
        //caseObj.Current_State__c='Closed';
        caseObj.Subject_Readonly__c='Test';
        insert caseObj;
        
        
    }
    
    public static testMethod void testFirst(){
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@sftpl.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='temp data';
        emltobean.plainTextBody='temp data';
        emltobean.subject='AP_PO';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
        Test.startTest();
        emltocase.processCase(emltobean,'CaseManager__c');
        emltocase.processCase(emltobean,'CaseManager__c');
        Test.stopTest();
    }
    
    public static testMethod void testSecond(){
        
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@gmail.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='test';
        emltobean.plainTextBody='test';
        CaseManager__c cm = new CaseManager__c();
        insert cm;
        System.debug('casemanager in test class' + cm);
        cm = [Select Name,Status__c from CaseManager__c Limit 1];
        emltobean.subject='lose [ref:_'+cm.Name +':ref]';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
        emltocase.processCase(emltobean,'CaseManager__c');

    }
    public static testMethod void testSecondOK(){
        
        
        
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@test.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='test';
        emltobean.plainTextBody='test';
        CaseManager__c cm = new CaseManager__c();
        insert cm;
        System.debug('casemanager in test class' + cm);
        cm = [Select Name,Status__c from CaseManager__c Limit 1];
        emltobean.subject='lose [ref:_'+cm.Name +':ref]';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
        Test.startTest();
        emltocase.processCase(emltobean,'CaseManager__c');
        Test.stopTest();
        
    }
    
    public static testMethod void testExistingNonExisting(){
        
        
        
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@test.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='test';
        emltobean.plainTextBody='test';
        CaseManager__c cm = new CaseManager__c();
        insert cm;
        System.debug('casemanager in test class' + cm);
        cm = [Select Name,Status__c from CaseManager__c Limit 1];
        emltobean.subject='lose [ref:_CN-1232131:ref]';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
        Test.startTest();
        emltocase.processCase(emltobean,'CaseManager__c');
        Test.stopTest();
        
    }
    
    
    public static testMethod void testIgnoreRule(){
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@gmail.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='test';
        emltobean.plainTextBody='test';
        emltobean.subject='AP_PO';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
        Test.startTest();
        emltocase.processCase(emltobean,'CaseManager__c');
        Test.stopTest();
    }
    public static testMethod void testExisting(){
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@sftpl.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='test';
        emltobean.plainTextBody='test';
        CaseManager__c cm = new CaseManager__c();
        insert cm;

        cm = [Select Name,Status__c from CaseManager__c Limit 1];
        emltobean.subject='lose [ref:_'+cm.Name +':ref]';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.debugLog = new List<String>();
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        Test.startTest();
        emltocase.processCase(emltobean,'CaseManager__c');

        cm = [Select Name,Status__c from CaseManager__c Limit 1];
        cm.Status__c = 'Closed';
        update cm;

        emltocase.processCase(emltobean,'CaseManager__c');
        Test.stopTest();
        
    }

public static testMethod void testInvoice(){
        EmailToCaseManager emltocase=new EmailToCaseManager();
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'abc@test.com',
            Phone = '123456'); 
        insert c;
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
        Map<string, string> strmp=new Map<string, string>();
        strmp.put('temp','test');
        strmp.put('Date','test');
    
        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        etocatch.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        etocatch.add(attachmentInline);
    
        EmailToCaseBean emltobean=new EmailToCaseBean();
        List<String> lststr=new List<String>();
        lststr.add('abc@test.com');
        emltobean.ccAddresses =lststr;
        emltobean.toAddresses =lststr;
        emltobean.fromAddress='abc@sftpl.com'; 
        emltobean.fromName='test';
        emltobean.htmlBody='temp data';
        emltobean.plainTextBody='temp data';
        emltobean.subject='AP_PO';
        emltobean.planSubject='test';
        emltobean.envelopFromAddress='temp';
        emltobean.envelopToAddress='temp';
        emltobean.inReplyTo='temp';
        emltobean.attachments=etocatch;
        emltobean.header=strmp;
        emltobean.debugLog = new List<String>();
    
   		
    
        Test.startTest();
        emltocase.processCase(emltobean,'Invoice__c');
        Test.stopTest();
    }    
    
}