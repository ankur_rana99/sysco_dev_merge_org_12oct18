@isTest
public with sharing class MergeCaseControllerTest { 
    @testSetup
    static void setupTestData() {
        CaseManager__c caseManager = new CaseManager__c();
        
        caseManager.WorkType__c = 'PO Invoice';
        caseManager.User_Action__c = 'Create';
        caseManager.Status__c = 'Start';
        insert caseManager;

        TestDataGenerator.createContent(caseManager);

        CaseManager__c caseManager1 = new CaseManager__c();
        
        caseManager1.WorkType__c = 'PO Invoice';
        caseManager1.User_Action__c = 'Create';
        caseManager1.Status__c = 'Start';
        insert caseManager1;


        

        System.assert(true);
    }
    private static testmethod void testMergeCase() {
        

        List<CaseManager__c> caseManagers = [select Id, name, Process__c, WorkType__c, Status__c from CaseManager__c];
        
        Test.startTest();
        
        
        Case cse = new case (STATUS = 'New', ORIGIN = 'Web', TYPE='Claims',  SUBJECT = 'Test', DESCRIPTION= 'Testing');
        Insert cse; 
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();      
        contentlink.ShareType= 'V';
        contentlink.LinkedEntityId = Cse.Id; 
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;

        Messaging.InboundEmail InVoundEmail = new Messaging.InboundEmail() ;
        
        list<EmailMessage> emaillist = new list<EmailMessage>();

        EmailMessage emailmsg =new EmailMessage();
        emailmsg.FromAddress = 'test@abc.org';
        emailmsg.Incoming = True;
        emailmsg.ToAddress= 'test@xyz.org';
        emailmsg.Subject = 'Test email';
        emailmsg.HtmlBody = 'Test email body';
        emailmsg.ParentId = cse.Id; 
        insert emailmsg;
        emaillist.add(emailmsg);

        EmailMessage emailmsg1 =new EmailMessage();
        emailmsg1.FromAddress = 'test@abc.org';
        emailmsg1.Incoming = True;
        emailmsg1.ToAddress= 'test@xyz.org';
        emailmsg1.Subject = 'Test email';
        emailmsg1.HtmlBody = 'Test email body';
        //emailmsg1.RelatedToId=cse.Id;
        emailmsg1.ParentId = cse.Id; 
        insert emailmsg1;
        emaillist.add(emailmsg1);


        
        EmailMessage newEmailMsg=emailmsg.clone(false,true,true,true);
        List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();

        String sourceString ='Demo';
        String replaceByString ='CN-562';
        String startIdentifier ='[ref:_';
        String endIdentifier =':ref]';
        String regex ='\\[ref:_(.*?):ref\\]';
        MergeCaseController.replaceCaseNumberSubject(sourceString,replaceByString,startIdentifier,endIdentifier,regex);
        MergeCaseController.oldNewEmailContentDocWrapper oldnewemailWraplist =new MergeCaseController.oldNewEmailContentDocWrapper(emailmsg.Id,newEmailMsg,new List<ContentDocumentLink>());
        MergeCaseController.MergeCase(new List<CaseManager__c>{caseManagers[0]},caseManagers[1],'Test');
        
        Test.stopTest();

        System.assert(caseManagers.size()>0);
    
    }
}