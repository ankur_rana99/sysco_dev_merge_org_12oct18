global class SceduleEDItoPDF implements Schedulable{

    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        SceduleEDItoPDF SEP = new SceduleEDItoPDF(); 
        return System.schedule('Scedule EDI to PDF', sched, SEP);
    }

    global void execute(SchedulableContext sc) {
        BatchEDItoPDFInvoice BEP = new BatchEDItoPDFInvoice();
        system.database.executebatch(BEP,200);
        //ID batchprocessid = Database.executeBatch(BEP,200);           
    }
}