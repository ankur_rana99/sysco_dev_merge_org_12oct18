global class SyscoAutoMatchBatchableScheduler implements Schedulable {
    
    global SyscoAutoMatchBatchableScheduler(){
    }
    global void execute(SchedulableContext objSchedulableContext) {
     SyscoAutoMatchBatchable syscoAutoMatch  = new SyscoAutoMatchBatchable(objSchedulableContext);
     Integer batchSize = 100; 
     system.database.executebatch(syscoAutoMatch,batchSize); 
    }
}