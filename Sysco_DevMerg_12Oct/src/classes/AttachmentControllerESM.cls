global class AttachmentControllerESM {
     @AuraEnabled
     public static Map < String, Object > getUserInfo() {
        Map < String, Object > userDetails = new Map < String, Object > ();
        try {           
            userDetails.put('UserName', userinfo.getName());
            userDetails.put('InstanceName',  [SELECT Id, InstanceName FROM Organization].InstanceName);
         
		    System.debug('UserName'+userinfo.getName());
			   
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return userDetails;
    }
	 
    @AuraEnabled
    public static Map<String,Object> getAttachments(String caseId) {
	 System.debug('getAttachments');
        Map<String,Object> resp=new Map<String,Object>();
        List<ContentDocumentLink> contentDocumentLinks=new List<ContentDocumentLink>();
       
        try{
		
	
			if (UtilityController.isFieldAccessible('ContentDocumentLink',null))
			{
			   contentDocumentLinks=[SELECT  ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:caseId order by SystemModstamp desc];

			}
		
        } catch (Exception e) {
           System.debug('msg :'+e.getMessage());
		   System.debug('trace :'+e.getStackTraceString());
		    
         }
           System.debug('attachments : '+contentDocumentLinks);  
		resp.put('attachments',contentDocumentLinks);
        return resp;
    }

	 @AuraEnabled
    public static Map<String,Object> insertData(String primaryDocJson, List<ContentVersion> NewAttachments, String parentId){
	 System.debug('parentId'+parentId);
	 
	 System.debug('attachments'+NewAttachments.size());
	 System.debug('primaryDocJson '+primaryDocJson);

	if (primaryDocJson!=null && primaryDocJson!='')
	{
		 Map<String,Boolean> content= (Map<String,Boolean>)JSON.deserialize(primaryDocJson, Map<String,Boolean>.class);
        system.debug('#####'+content);
        List<ContentVersion> cv=new List<ContentVersion>();

       for(string s:content.keySet()){
	   System.debug('flag '+content.get(s));
	 
            cv.add(new ContentVersion(Id=s,Is_Primary_Doc__c=content.get(s)));
        }
		 
			if (UtilityController.isFieldUpdateable('ContentVersion',null))
			{
			 update cv;
			}
       
	}
	 

	List<ContentDocumentLink> contentDocumentLinkList=new List<ContentDocumentLink>();
	
	
         for(Integer i=0;i<NewAttachments.size();i++)
		 {
			System.debug('attachments[i].ContentDocumentId'+NewAttachments[i].ContentDocumentId);
			contentDocumentLinkList.add(new ContentDocumentLink(
		   ContentDocumentId=NewAttachments[i].ContentDocumentId,
		   LinkedEntityId=parentId,
		   ShareType='V',
			Visibility='AllUsers'));
	  }
			System.debug('ContentDocumentLinks  before'+ contentDocumentLinkList);
			
		if (UtilityController.isFieldUpdateable('ContentDocumentLink',null) && UtilityController.isFieldCreateable('ContentDocumentLink',null))
		{
		upsert contentDocumentLinkList;
		}
			
			System.debug('ContentDocumentLinks  after'+ contentDocumentLinkList);

			return getAttachments(parentId);
	}
  
    @AuraEnabled
    public static Map<String,Object> deleteAttachments(List<String> docId,String caseId) {
      
        try{
			if (UtilityController.isDeleteable('ContentDocument',null))
			{
			 delete [SELECT Id FROM ContentDocument where LatestPublishedVersionId In :docId];
			}
            
          
        }catch (Exception e) {
           
            ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
        } 
        return getAttachments(caseId);
       
    }
}