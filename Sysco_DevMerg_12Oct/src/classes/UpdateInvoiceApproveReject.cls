public class UpdateInvoiceApproveReject {

    @InvocableMethod(label = 'UpdateInvoiceApproveReject' description = 'If anyone will reject then invoice should be move into reject else if all approver will approve then invoice should get approved.')
    public static void checkUserDOA(List<Invoice__c> invoice) {
        System.debug('invoice--' + invoice);
        Set<Id> invId = new Set<Id> ();
        
        for (Invoice__c i : invoice) {
            invId.add(i.Id);
        }
        Map<String, String> invoiceToStatus = new Map<String, String> ();
        List<Approval_Detail__c> appDetail = [Select Id, Approval_Cycle__c, Approver_Action__c, Approver_User__c, Invoice__c, Status__c, Invoice__r.Current_Cycle__c From Approval_Detail__c Where Invoice__c IN :invId];
        System.debug('appDetail-->' + appDetail);
        for (Approval_Detail__c apd : appDetail) {
            if(apd.Approval_Cycle__c == apd.Invoice__r.Current_Cycle__c)
            if (invoiceToStatus.containsKey(apd.Invoice__c)) {
                if (invoiceToStatus.get(apd.Invoice__c) != 'Rejected' && invoiceToStatus.get(apd.Invoice__c) != 'Pending' && !apd.Status__c.equals('Approved')) {
                    invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
                }
                System.debug('invoiceToStatus-->' + invoiceToStatus);
            } else {
                invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
                System.debug('invoiceToStatus-->' + invoiceToStatus);
            }
        }
        List<Invoice__c> needToUpdate = new List<Invoice__c> ();
        Set<String> appdInv = new Set<String> ();
        if (!invoiceToStatus.isEmpty()) {
            Invoice__c tmp;
            for (String id : invoiceToStatus.keySet()) {
                if (invoiceToStatus.get(id) == 'Approved' || invoiceToStatus.get(id) == 'Rejected') {
                    if (invoiceToStatus.get(id) == 'Approved') {
                        appdInv.add(id);
                    }
                    tmp = new Invoice__c();
                    tmp.Id = id;
                    tmp.Approval_Status__c = invoiceToStatus.get(id);
                    needToUpdate.add(tmp);
                }
            }
        }

        System.debug('@@@ needToUpdate : ' + needToUpdate);
        //throw new DMLException();
        if (needToUpdate.size() > 0) {
            update needToUpdate;
            String query = '';
            String selectfields = '';
            Map<string, Schema.sObjectType> describeInfo = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fMap = describeInfo.get('Invoice__c').getDescribe().Fields.getMap();
            if (fMap != null) {
                for (Schema.SObjectField f : fMap.values()) {
                    selectfields += f.getDescribe().getName().toLowerCase() + ',';
                }
                selectfields = selectfields.removeEnd(',');
                query = 'select ' + selectfields + ' from Invoice__c where Id in :appdInv ';
                needToUpdate = Database.query(query);
                QCCalculation.checkForQC(needToUpdate, 'Invoice__c');
            }
        }

    }

}