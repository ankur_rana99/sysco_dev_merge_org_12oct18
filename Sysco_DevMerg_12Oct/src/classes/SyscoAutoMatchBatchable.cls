global class SyscoAutoMatchBatchable implements Database.Batchable<SObject>{
    
    global SchedulableContext objSchedulableContext;
    global map<Id, Invoice__c> mapInvoices;
    global map<Id, String> mapInvoiceWithExceptions;
    global map<Id, String> mapInvoiceWithVariance;
    global map<Id, String> mapInvoiceWithFlagAF;
    global map<Id, String> mapInvoiceTotalAmount;
    global map<Id, String> mapInvWithLineTolerance;
    global map<Id, String> mapInvWithChargeTolerance;
    global map<Id, String> mapInvWithLineVariance;
    global map<Id, String> mapInvWithChargeVariance;
    global map<Id, String> mapInvoiceWithPUACharges = new map<Id, String>();
    global map<Id, String> mapInvoiceWithMSCCharges = new map<Id, String>();
    global map<Id, String> mapInvoiceWithFreightCharges = new map<Id, String>();
    global String esmNameSpace;
    global Invoice_Configuration__c invConf;
    
    global SyscoAutoMatchBatchable (SchedulableContext objSchedulableContext) {
        mapInvoices = new map<Id, Invoice__c>();
        this.objSchedulableContext=objSchedulableContext;
        mapInvoiceWithExceptions = new map<Id, String> ();
        mapInvoiceWithVariance = new map<Id, String> ();
        mapInvoiceWithFlagAF = new map<Id, String> ();
        mapInvoiceTotalAmount = new map<Id, String> ();
        mapInvWithLineTolerance = new map<Id, String> ();
        mapInvWithChargeTolerance = new map<Id, String> ();
        mapInvWithLineVariance = new map<Id, String> ();
        mapInvWithChargeVariance = new map<Id, String> ();
        mapInvoiceWithPUACharges = new map<Id, String>();
        mapInvoiceWithMSCCharges = new map<Id, String>();
        mapInvoiceWithFreightCharges = new map<Id, String>();
        esmNameSpace = UtilityController.esmNameSpace;
    }
    
    /**
     * @description gets invoked when the batch job starts
     * @param context contains the job ID
     * @returns the record set as a QueryLocator object that will be batched for execution
     */ 
    global List<Invoice__c> start(Database.BatchableContext context) {
        try{
            Set<string> invCurrentStateSet = new Set<string>();
            //set<String> setInvName = new set<string>{'INV-00005846','INV-00005847','INV-00005848','INV-00005849'};
            if(Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
                invConf = Invoice_Configuration__c.getInstance(Userinfo.getUserid());
            } else {
                invConf = Invoice_Configuration__c.getOrgDefaults();
            }
            if(!String.isBlank(invConf.Auto_Match_Current_State__c)){
                if(invConf.Auto_Match_Current_State__c.contains(',')){
                    for(String currentstate:invConf.Auto_Match_Current_State__c.split(',')){
                        invCurrentStateSet.add(currentstate);
                    }
                } else {
                    invCurrentStateSet.add(invConf.Auto_Match_Current_State__c);
                }
                System.debug('Log invCurrentStateSet'+invCurrentStateSet);
            }
            //return Database.Query(CommonFunctionController.getSOQLQuery(esmNameSpace+'Invoice__c', 'Current_State__c IN:invCurrentStateSet AND Name IN: setInvName '));            
            return Database.Query(CommonFunctionController.getSOQLQuery(esmNameSpace+'Invoice__c', 'Current_State__c IN:invCurrentStateSet'));            
        } catch(Exception ex){
             UtilityController.customDebug(ex, 'SyscoAutoMatchBatchable');
        }
        return null;
    }

    /**
     * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
     * @param context contains the job ID
     * @param scope contains the batch of records to process.
     */ 
    global void execute(Database.BatchableContext context, List<Invoice__c> scope) {
        try {
            Set<Id> invIdSet = new Set<Id>();
            system.debug('=====SyscoAutoMatchBatchable execute======');
            for(Invoice__c inv: scope){
                invIdSet.add(inv.Id);
                mapInvoices.put(inv.Id, inv);
            }
            List<Purchase_Order__c> lstPOToUpdate = new List<Purchase_Order__c>();
            System.debug('Log mapInvoices'+mapInvoices);
            List<Invoice__c> lstInvoice = new List<Invoice__c>();
            SysycoAutoMatchAndValidationHelper helper = new SysycoAutoMatchAndValidationHelper();
            map<String, map<Id, String>> mapOutputFromHelper = helper.validateCheck(scope, new List<Invoice_Line_Item__c>(), new List<Purchase_Order__c>(), true);
            mapInvoiceWithExceptions = mapOutputFromHelper.get('Exceptions');
            mapInvoiceWithVariance = mapOutputFromHelper.get('Variances');
            mapInvoiceWithFlagAF = mapOutputFromHelper.get('AF');
            mapInvoiceTotalAmount = mapOutputFromHelper.get('Amount');
            mapInvWithLineTolerance = mapOutputFromHelper.get('LineTolerance');
            mapInvWithChargeTolerance = mapOutputFromHelper.get('ChargeTolerance');
            mapInvWithLineVariance = mapOutputFromHelper.get('LineVariance');
            mapInvWithChargeVariance = mapOutputFromHelper.get('ChargeVariance');
            mapInvoiceWithPUACharges = mapOutputFromHelper.get('PUACharges');
            mapInvoiceWithMSCCharges = mapOutputFromHelper.get('MSCCharges');
            mapInvoiceWithFreightCharges = mapOutputFromHelper.get('FreightCharges');
            
            System.debug('mapInvoiceWithPUACharges:::: ' + mapInvoiceWithPUACharges);
            System.debug('mapInvoiceWithMSCCharges:::: ' + mapInvoiceWithMSCCharges);
            System.debug('mapInvoiceWithFreightCharges:::: ' + mapInvoiceWithFreightCharges);
            map<Id, Purchase_Order__c> mapOrdersToUpdate = new map<Id, Purchase_Order__c> ();
            map<Id, Invoice_Line_Item__c> mapINVLToUpdate = new map<Id, Invoice_Line_Item__c> ();
            System.debug('mapOutputFromHelper.get==Batch=>' + mapOutputFromHelper.get('INVL'));
            mapOrdersToUpdate = SysycoAutoMatchAndValidationHelper.mapPOToUpdate;
            mapINVLToUpdate = SysycoAutoMatchAndValidationHelper.mapLinesToUpdate;
            System.debug('mapINVLToUpdate==Batch=>' + mapINVLToUpdate);
            lstInvoice = processInvoices();
            System.debug('lstInvoice ==> ' + lstInvoice );
            System.debug('mapLinesToUpdate==> ' + SysycoAutoMatchAndValidationHelper.mapLinesToUpdate);
            // updating invoices
            if(!lstInvoice.isEmpty()){
                List<Database.SaveResult> lstInvoiceResults = Database.update(lstInvoice, false);
                System.debug('lstInvoiceResults ==> ' + lstInvoiceResults );
                for (Database.SaveResult sr : lstInvoiceResults) {
                    System.debug('sr ==> ' + sr );
                    if (sr.isSuccess()){
                        System.debug('Successfully updated Invoice with ID: ' + sr.getId());
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Invoice fields that affected this error: ' + err.getFields());
                        }
                    }
                }
                // updating PO
                if(mapOrdersToUpdate.keyset() != null){
                    System.debug('mapOrdersToUpdate.values():::: ' + mapOrdersToUpdate.values());
                    List<Database.SaveResult> lstPOUpdateResults = Database.update(mapOrdersToUpdate.values(), false);
                    for (Database.SaveResult sr : lstPOUpdateResults) {
                        if (sr.isSuccess()){
                            System.debug('Successfully updated PO with ID: ' + sr.getId());
                        } else {
                            for(Database.Error err : sr.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('PO fields that affected this error: ' + err.getFields());
                            }
                        }
                    }
                }
                // updating invoice lines
                if(mapINVLToUpdate.keyset() != null){
                    System.debug('mapINVLToUpdate.values():::: ' + mapINVLToUpdate.values());
                    List<Database.SaveResult> lstINVLUpdateResults = Database.update(mapINVLToUpdate.values(), false);
                    for (Database.SaveResult sr : lstINVLUpdateResults) {
                        if (sr.isSuccess()){
                            System.debug('Successfully updated Invoice Line Item with ID: ' + sr.getId());
                        } else {
                            for(Database.Error err : sr.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Invoice Line Item fields that affected this error: ' + err.getFields());
                            }
                        }
                    }
                }
            }    
        } catch(Exception ex){
             UtilityController.customDebug(ex, 'SyscoAutoMatchBatchable');
        }
    }
    
    global List<Invoice__c> processInvoices() {
        system.debug('=====processInvoices execute======');
        List<Invoice__c> lstInvoicesToUpdate = new List<Invoice__c>();
        for(Id invId : mapInvoiceWithExceptions.keyset()){
            String invoiceType = '';
            Invoice__c invoiceToUpdate = new Invoice__c();
            invoiceToUpdate.Id =  invId;
            invoiceToUpdate.Invoice_Rounding_Amount__c =   mapInvoiceWithVariance.get(invId) != null ? Decimal.valueOf(mapInvoiceWithVariance.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
            invoiceToUpdate.Amount__c = mapInvoiceTotalAmount.get(invId) != null ? Double.valueOf(mapInvoiceTotalAmount.get(invId)) : 0.0;
            invoiceToUpdate.Line_Level_Tolerance__c = mapInvWithLineTolerance.get(invId) != null ? mapInvWithLineTolerance.get(invId) : '';
            invoiceToUpdate.Charge_Level_Tolerance__c = mapInvWithChargeTolerance.get(invId) != null ? mapInvWithChargeTolerance.get(invId) : '';
            invoiceToUpdate.AF_Flag__c = mapInvoiceWithFlagAF.get(invId) != null ? mapInvoiceWithFlagAF.get(invId) : 'N';
            invoiceToUpdate.Line_Level_Variance__c = mapInvWithLineVariance.get(invId) != null ? Decimal.valueOf(mapInvWithLineVariance.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
            invoiceToUpdate.Charge_Level_Variance__c = mapInvWithChargeVariance.get(invId) != null ? Decimal.valueOf(mapInvWithChargeVariance.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
            
            System.debug('The following error has occurred : ' + mapInvoiceWithExceptions.get(invId));
            String exceptionStr = '';
            if(mapInvoices.get(invId) != null){
                if(!String.isBlank(mapInvoices.get(invId).WorkType__c)){
                    if(!String.isBlank(mapInvoices.get(invId).Invoice_Type__c) && mapInvoices.get(invId).WorkType__c == 'Merchandize' && mapInvoices.get(invId).Invoice_Type__c == 'M' ){
                        invoiceType = 'M';
                    }
                    else if(!String.isBlank(mapInvoices.get(invId).Invoice_Type__c) && mapInvoices.get(invId).WorkType__c == 'Freight Only' && mapInvoices.get(invId).Invoice_Type__c == 'F' ){
                        invoiceType = 'F';
                    }
                    if(mapInvoices.get(invId).WorkType__c == 'Drop Shipments'){
                        if(!String.isBlank(mapInvoices.get(invId).Invoice_Origin__c) && mapInvoices.get(invId).Invoice_Origin__c == 'EDI'){
                            invoiceType = 'D-EDI';
                        } else if(mapInvoices.get(invId).Invoice_Origin__c != 'EDI'){
                            invoiceType = 'D-Man';
                        }
                    }
                }
                
                if(!String.isBlank(mapInvoiceWithExceptions.get(invId))){
                    if(String.isBlank(mapInvoices.get(invId).Auto_Match_Indicator__c)){
                        invoiceToUpdate.Auto_Match_Indicator__c =  'N';
                    }
                    for(String message : mapInvoiceWithExceptions.get(invId).split(';')){
                        System.debug('****message***' + message + ' invId===> ' + invId);
                        if(message == System.Label.Requester_Missing || message == System.Label.GRN_Line_Not_Found || message == System.Label.PO_Line_Not_Found || message == System.Label.Price_Mismatch || message == System.Label.Catch_Weight_Mismatch || message == System.Label.Quantity_Mismatch){
                            message = null;
                        }
                        if (message != null && message != System.Label.Duplicate_Invoice) {
                            exceptionStr += message + ';';
                        }
                        else if(message == System.Label.Duplicate_Invoice){
                            exceptionStr = message;
                        }
                        if(message ==  System.Label.PO_Cancelled_Closed || message == System.Label.PO_already_consumed || message == System.Label.Invoice_Line_Not_Found){
                            invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                        }
                        else if(message == System.Label.Duplicate_Invoice){
                            System.debug('****Duplicate Invoice***');
                            invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                        }
                        else if(message == System.Label.Delivery_date_in_Future){
                            System.debug('****Delivery date in Future***');
                            invoiceToUpdate.Remove_Hold_Date__c = System.Today() + 30;
                            invoiceToUpdate.User_Action__c = System.Label.Hold;
                        }
                        else if(message == System.Label.GRN_Overdue){
                            System.debug('****GRN Overdue***');
                            invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                        }
                        else if(message == System.Label.PO_Already_Vouchered){
                            System.debug('****Invoice Already Vouchered***');
                            invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                        }
                        else if(message == System.Label.Amount_Mismatch){
                            if(invoiceType == 'F'){
                                System.debug('****Freight Amount mismatch***');
                                if(mapInvoices.get(invId).Input_Source__c == 'ALF'){
                                    invoiceToUpdate.User_Action__c = System.Label.Send_For_VRFS;
                                }
                                else {
                                    invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                                }
                            } else if(invoiceType == 'D-Man'){
                                System.debug('****D-Man Amount Mismatch***');
                                invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                                /*if(mapInvoices.get(invId).Requestor_Buyer__c != null){
                                    invoiceToUpdate.User_Action__c = System.Label.Send_to_Buyer;
                                } else {
                                    invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                                }*/
                            } else {
                                System.debug('***Others Amount mismatch***');
                                invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                            }
                        }
                        else if((invoiceType == 'M' && invoiceType == 'D-EDI' && invoiceType == 'D-Man') && (message == System.Label.Line_Amount_Mismatch || message == System.Label.Charges_Mismatch)){
                            System.debug('***Others Line Amount/ Charges mismatch***');
                            invoiceToUpdate.User_Action__c = System.Label.Send_For_Manual_Processing;
                        }
                    }
                    invoiceToUpdate.Exception_Reason__c = exceptionStr;
                } else {
                    System.debug('****Auto Match Pass***');
                    invoiceToUpdate.User_Action__c = System.Label.Send_For_Posting;
                    if(String.isBlank(mapInvoices.get(invId).Auto_Match_Indicator__c)){
                        invoiceToUpdate.Auto_Match_Indicator__c =  'Y';
                        invoiceToUpdate.Exception_Reason__c =  null;
                        invoiceToUpdate.Freight_codes__c = mapInvoices.get(invId).Freight_Charges__c != null &&  mapInvoices.get(invId).Freight_Charges__c > 0? 'FRTPP' : '';
                        invoiceToUpdate.Misc_codes__c = mapInvoices.get(invId).Other_Charges_Surcharges__c != null && mapInvoices.get(invId).Other_Charges_Surcharges__c > 0 ? 'MSCPP' : '';
                        if(!String.isBlank(mapInvoices.get(invId).Invoice_Origin__c) && mapInvoices.get(invId).Invoice_Origin__c == 'EDI'){
                            if(mapInvoices.get(invId).Freight_Charges__c == null){
                                invoiceToUpdate.Freight_Charges__c = mapInvoiceWithFreightCharges.get(invId) != null ? Decimal.valueOf(mapInvoiceWithFreightCharges.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
                            }
                            if(mapInvoices.get(invId).Other_Charges_Surcharges__c == null) {
                                invoiceToUpdate.Other_Charges_Surcharges__c = mapInvoiceWithMSCCharges.get(invId) != null ? Decimal.valueOf(mapInvoiceWithMSCCharges.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
                            } 
                            if(mapInvoices.get(invId).Pickup_Allowance__c == null) {
                                invoiceToUpdate.Pickup_Allowance__c = mapInvoiceWithPUACharges.get(invId) != null ? Decimal.valueOf(mapInvoiceWithPUACharges.get(invId)).setScale(2, RoundingMode.HALF_UP) : 0.0;
                            }
                            invoiceToUpdate.Freight_codes__c = mapInvoiceWithFreightCharges.get(invId) != null ? 'FRTPP' : '';
                            invoiceToUpdate.Misc_codes__c = mapInvoiceWithMSCCharges.get(invId) != null ? 'MSCPP' : '';
                        }
                    }
                }
            }
            system.debug('=====processInvoices execute===invoiceToUpdate.Id===' + invoiceToUpdate.Id);
            system.debug('=====processInvoices execute===User_Action__c======' + invoiceToUpdate.User_Action__c);
            system.debug('=====processInvoices execute===Exception_Reason__c====' + invoiceToUpdate.Exception_Reason__c);
            system.debug('=====processInvoices execute===Invoice_Rounding_Amount__c===' + invoiceToUpdate.Invoice_Rounding_Amount__c);
            system.debug('=====processInvoices execute===invoiceToUpdate.Remove_Hold_Date__c===' + invoiceToUpdate.Remove_Hold_Date__c);
            system.debug('=====processInvoices execute===Freight_Charges__c====' + invoiceToUpdate.Freight_Charges__c);
            system.debug('=====processInvoices execute===Other_Charges_Surcharges__c===' + invoiceToUpdate.Other_Charges_Surcharges__c);
            system.debug('=====processInvoices execute===invoiceToUpdate.Pickup_Allowance__c===' + invoiceToUpdate.Pickup_Allowance__c);
            system.debug('=====processInvoices execute===invoiceToUpdate.Freight_codes__c===' + invoiceToUpdate.Freight_codes__c);
            system.debug('=====processInvoices execute===AF_Flag__c======' + invoiceToUpdate.AF_Flag__c);
            lstInvoicesToUpdate.add(invoiceToUpdate);
        }
        return lstInvoicesToUpdate;
    }

    /**
     * @description gets invoked when the batch job finishes. Place any clean up code in this method.
     * @param context contains the job ID
     */ 
    

    /*global void execute(SchedulableContext sc) {
        scheduleNow();
    }*/

    global void finish(Database.BatchableContext context) {
        system.debug('=====AutoMatchBatchable finish======');
        String jobId;
        try{
            if(this.objSchedulableContext!= null){
                 jobId=this.objSchedulableContext.getTriggerId();
            }
            List<CronTrigger> objCronTrigger = [SELECT Id, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            if(objCronTrigger!=null && objCronTrigger.size()>0){
                System.debug('Next fire time: ' + objCronTrigger[0].NextFireTime);
            } else{
                system.debug('***** objCronTrigger is Null ********');
            }
        }catch(Exception ex){
            system.debug(ex.getStackTraceString());
        }
        finally{
            system.debug(' Going to Abort Scheduled Job,JobId ['+jobId+'] Time ['+DateTime.now()+'] ');
            if(this.objSchedulableContext != null) {
                System.abortJob(this.objSchedulableContext.getTriggerId());
            }
            system.debug(' Scheduled Job Aborted Successfully ,JobId ['+jobId+'] Time ['+DateTime.now()+']');
            scheduleNow();
        }
    }

    global void scheduleNow(){
        try{
            system.debug('*********** In scheduleNow 0f class SyscoAutoMatchBatchable*******'+DateTime.now());
            DateTime objDateTime = datetime.now().addMinutes(5);
            String cron = '';
            cron += objDateTime.second();
            cron += ' ' + objDateTime.minute();
            cron += ' ' + objDateTime.hour();
            cron += ' ' + objDateTime.day();
            cron += ' ' + objDateTime.month();
            cron += ' ' + '?';
            cron += ' ' + objDateTime.year();
            String jobId=System.schedule('SyscoAutoMatchBatchable', cron, new SyscoAutoMatchBatchableScheduler());
            system.debug(' New Job Scheduled Successfully ,JobId ['+jobId+'] Time ['+DateTime.now()+'] ');
        }catch(Exception ex){
            system.debug(ex.getStackTraceString());
        }
    }   

}