@isTest
public class CaseAdditionalDetailsTest {
    public static testMethod void unitTest01(){
        List<CaseManager__c> cts= ESMDataGenerator.createCases(1);
        CaseManager__c cm=cts.get(0);
        Comments__c inc =new Comments__c();
        inc.Comment__c ='test';
        inc.CaseManager__c = cm.id;
        insert inc;
        CaseAdditionalDetails.getCaseComments(cm.id);
    }
}