@isTest
public class CaseServiceTest{
    
     public static testMethod void unitTest01(){
       CaseService.CaseUpdateResponse wrapperField = new CaseService.CaseUpdateResponse();
       wrapperField.ErrorMessage = 'abc';
        CaseManager__c cm = new CaseManager__c();
          insert cm ;
          ID cmId = cm.Id;
          //ID sessionId = cm.ID;
          CaseService.getCaseById(cmId);
          CaseService.getCaseByIdWithInteraction(cmId);
       
          EmailMessage em =new EmailMessage();
          CaseService cs = new CaseService();
          cs.addNoteFromService(em);
        
          QualityCheck__c qc = new QualityCheck__c();
         insert qc ;
         ID qcId = qc.Id;
        String qcName =qc.Name;
        CaseService.getQualityControls(qcId,qcName);
        list<CaseManager__c> cml = new list<CaseManager__c>();
         cm.Comments__c='comments';
        cml.add(cm);
        CaseService.updateCases(cml);
        CaseService.updateCaseOwner(cml);
        
         //List<ContentDocument> documents =new List<ContentDocument> ();
            //documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];
            //system.debug('----documents--'+documents);
            //
            ContentVersion cv = new ContentVersion();
             cv.Title = 'Demopdf';
             cv.PathOnClient = 'Demopdf.pdf';
             cv.VersionData = Blob.valueOf('Test Content');
             cv.IsMajorVersion = true;
        	insert cv;
        system.debug('cv--'+cv);
        ContentDocumentLink contentDocumentLinkObj = new ContentDocumentLink();
                contentDocumentLinkObj.ContentDocumentId = cv.ContentDocumentId;
                //contentDocumentLinkObj.LinkedEntityId = FileName;
                contentDocumentLinkObj.ShareType = 'V';
                system.debug('------contentDocumentLinkObj-------'+contentDocumentLinkObj.ContentDocumentId);
         CaseService.getAttachmentLinks(cmId);
        CaseService.getQualityControls(cmId,'NewCase');
        //ContentDocumentLink cdl = new ContentDocumentLink();
        //insert cdl ;
        //ID cdlId = cdl.Id;
        //CaseService.getAttachmentLinks(cdlId);
        Map<string,string> ftb = new Map<string,string>();
         ftb.put('abc','xyz');
       
        CaseService.addAttachments(ftb, cm);
        
        
    }
}