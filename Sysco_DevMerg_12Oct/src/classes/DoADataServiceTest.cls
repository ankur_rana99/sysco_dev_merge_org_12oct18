@isTest
public class DoADataServiceTest {
     public static testMethod void testFirst(){
       
          
         OpCo_Code__c op=new OpCo_Code__c();
         op.OPCO_Code__c='073';
         insert op;
         
         User_Master__c userMaster=new User_Master__c();
         userMaster.User_Type__c='Requestor/Buyer';
         userMaster.OpCo_Code__c=op.Id;
         userMaster.Entity__c='Corporate';
         userMaster.DAP_Title__c='SVP';
         insert userMaster;
        
         
         Functional_DAP__c functional_dap=new Functional_DAP__c();
         functional_dap.Entity__c='Corporate';
         functional_dap.DAP_Title__c='SVP';
         insert functional_dap;
         
         Finance_DAP__c finance_dap=new Finance_DAP__c();
         finance_dap.Entity__c='Corporate';
         finance_dap.DAP_Title__c='SVP';
         insert finance_dap;
         
         DoADataService.prepareDataForDoA();
         
     }
}