global class SyscoGetOpcoData implements database.Batchable<Sobject>,Schedulable,Database.Stateful,Database.AllowsCallouts{

private SchedulableContext objSchedulableContext;
private OPCO_Code__c opco;
private OPCO_Code__c opcoEmpty;
private List<OPCO_Code__c> opcoList;
private List<OPCO_Code__c> emptyopcoList = new List<OPCO_Code__c>(); 

global SyscoGetOpcoData(){       
}

global List<OPCO_Code__c> start(Database.BatchableContext BC) {
    try{
        String dateFormatString = 'yyyy-MM-dd';
        opco = new OPCO_Code__c();
        opcoEmpty = new OPCO_Code__c();
        opcoList = new List<OPCO_Code__c>();
        System.debug(' Method Called Opco...');
        
        Date today = Date.today();
        Datetime dt1 = Datetime.newInstance(today.year(), today.month(),today.day());
        String tday = dt1.format(dateFormatString);
        
        Date previousday = Date.today() - 1;
        Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(),previousday.day());
        String prevday = dt2.format(dateFormatString);
        
        WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('OpcoData');
        if(WEEndpoint.isfirstrun__c == 'Y'){
           prevday = '';                      
        }
        
        
        String XMLString;
        HttpRequest feedRequest = new HttpRequest();
        feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday);
        feedRequest.setMethod('GET');
        Http http = new Http();
        HTTPResponse CompanyResponse = http.send(feedRequest);
        System.debug(CompanyResponse.getBody());
        XMLString = CompanyResponse.getBody();
        DOM.Document doc=new DOM.Document();
        doc.load(XMLString);
        DOM.XmlNode rootNode=doc.getRootElement();
        parseXML(rootNode);
        if(opco!=null && opco!=opcoEmpty  )
            opcoList.add(opco); 
        system.debug('opcoList+++++++++++++' + opcoList);              
        
    }catch(exception e){
        system.debug(e.getMessage());
    } 
    return opcoList;          
}

global void Execute(Database.BatchableContext objBatchableContext,List<OPCO_Code__c> opcoc){
    try{ 
        system.debug('opcoc.size' + opcoc.size());
        system.debug('opcoc+++' + opcoc);
        system.debug('emptyopcoList' + emptyopcoList);
        if(opcoc.size()>0 && opcoc != emptyopcoList) {  
            system.debug('opcoc.size()' + opcoc.size());
            system.debug('opcoc' + opcoc);           
            upsert opcoc OPCO_Code__c;
        }
        else
            system.debug('No Records to Process');
    }catch(exception e){
        system.debug(e.getMessage());
    }
}

global void finish(Database.BatchableContext BC) {
    WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('OpcoData'); 
    System.debug('WEEndpoint..... '+ WEEndpoint.isfirstrun__c);
    if(WEEndpoint.isfirstrun__c == 'Y'){
        System.debug('WEEndpoint.isfirstrun__c +'+ WEEndpoint.isfirstrun__c);
       WEEndpoint.isfirstrun__c = 'N';
       Update WEEndpoint;                       
    }      
 }
    
Private void parseXML(DOM.XMLNode node) {
    if(node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if(node.getName()=='Report_Entry'){
            if(opco!=null && opco!=opcoEmpty  )
                opcoList.add(opco);
            opco = new OPCO_Code__c();  
        }
        if(node.getName()=='name'){
            opco.OPCO_Name__c=node.getText().trim();
            if(opco.OPCO_Name__c.length() > 79)
                opco.Name=opco.OPCO_Name__c.substring(0,80);
            else
                opco.Name=opco.OPCO_Name__c;
        }
        if(node.getName()=='Employer_s_Federal_ID_Number'){
            opco.OPCO_Code__c =node.getText().trim();
            opco.Name=opco.OPCO_Code__c;
            }
        if(node.getName()=='Inactive'){
            opco.Inactive__c=node.getText().trim();
            if(opco.Inactive__c == '0')
               opco.Status__c = 'Active';
            else
                opco.Status__c = 'Inactive';
        }        
    }
    System.debug('Opco------->' + opco);
    for (Dom.XMLNode child: node.getChildElements()) {
        parseXML(child);
    }
}

global void execute(SchedulableContext objSchedulableContext) {
     SyscoGetOpcoData opcodata  = new SyscoGetOpcoData();
     Integer batchSize = 2000; 
     system.database.executebatch(opcoData,batchSize); 
    }

}