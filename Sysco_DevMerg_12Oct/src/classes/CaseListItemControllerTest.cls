@isTest
public with sharing class CaseListItemControllerTest {
	public static testMethod void unitTest01() {
		

		Integer noOfSplits = 2;
		String Comments = 'Test Comments';
		List<CaseManager__c> caseList = new List<CaseManager__c> ();

		CaseManager__c caseObj = new CaseManager__c();
		caseObj.WorkType__c = 'PO Invoices';
		caseObj.Document_Type__c = 'Electricity';
		caseObj.Amount__c = 12000;
		caseObj.Status__c = 'Ready for processing';
		caseObj.escalation__c = true;
		insert caseObj;
		caseList.add(caseObj);

		CaseManager__c caseObjNew = new CaseManager__c();
		caseObjNew.WorkType__c = 'PO Invoices';
		caseObjNew.Document_Type__c = 'Electricity';
		caseObjNew.Amount__c = 10000;
		caseObjNew.Status__c = 'Ready for processing';
		caseObjNew.escalation__c = true;
		insert caseObjNew;
		caseList.add(caseObjNew);
		//End Case Creation
		String attachId = TestDataGenerator.createContent(caseObjNew);



		String SplitList='[{"attachment": "'+attachId+'"}]';

		//String objectType='Sobject';
		String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
		String fsname='TestFieldset';
		
		Test.startTest();

		CaseListItemController.toggleFavourite(caseObj, 'Favourite');
		CaseListItemController.toggleFavourite(caseObj, 'UnFavourite');
		CaseListItemController.returnSplitCases(caseObj,noOfSplits);
		CaseListItemController.getAttachments(caseObj.Id);
		CaseListItemController.performMergeAction(caseList,caseObj,Comments);
		CaseListItemController.performSplitAction(SplitList,caseObj,objectType,fsname,Comments);
		
		Test.stopTest();

		List<CaseManager__c> caseManagers=[select id from CaseManager__c];
		System.assert(caseManagers.size()>0);
		//End Calling Methods of Controller
	}
}