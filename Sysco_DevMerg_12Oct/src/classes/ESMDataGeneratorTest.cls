@isTest
public class ESMDataGeneratorTest {
   // public static string coraNameSpace = UtilityController.coraNameSpace; 
    public static testmethod void testCase1() {
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        
        System_Configuration__c sysconfig2 = new System_Configuration__c(); 
        sysconfig2.Module__c='Invoice__c'; 
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State'; 
        sysconfig2.Value__c = 'Awaiting OCR Feed'; 
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        List<String> lt= new List<String>{'app1@gmail.com'};
        Purchase_Order__c po = ESMDataGenerator.insertPurchaseOrder();
        PO_Line_Item__c pol = ESMDataGenerator.insertPOLineItem(po.Id);
        GRN__c grn = ESMDataGenerator.insertGRN(po.Id);
        GRN_Line_Item__c grnl = ESMDataGenerator.insertGRNLineItem(grn.Id, po.Id, pol.Id);
        ESMDataGenerator.insertPOLINVLMap();
        
        Invoice__c inv = ESMDataGenerator.insertPOInvoice();
        Invoice__c inv1 = ESMDataGenerator.insertPOInvoice('inv-1',po.Id);
        ESMDataGenerator.insertTriggerConfiguration(true);
        ESMDataGenerator.insertInvoiceConfiguration();
        ESMDataGenerator.insertPOInvoice(1);
        ESMDataGenerator.insertNONPOInvoice(1);
        ESMDataGenerator.insertAdditionalLineItem(inv1.Id);
        Invoice_Line_Item__c invl = ESMDataGenerator.insertInvoiceLineItem(inv1.Id, po.Id, pol.Id, grn.Id, grnl.Id);
      //  ESMDataGenerator.createInvoice('test');
        ESMDataGenerator.insertBuyerUser(lt);
        ESMDataGenerator.invoiceVendorMaintenance(null);
        ESMDataGenerator.insertPaymentDetail();
        ESMDataGenerator.insertApprovalDetail();
        ESMDataGenerator.invoicePOInsert();
        ESMDataGenerator.invoiceNPOInsert();
        ESMDataGenerator.createOldCases(1);
        ESMDataGenerator.insertSystemConfiguration();
        ESMDataGenerator.CreateRuleAlwaysMoveToQC();
        List<CaseManager__c> trc = ESMDataGenerator.createCases(1);
        ESMDataGenerator.getCase();
        ESMDataGenerator.getOldCase();
        ESMDataGenerator.createCaseTracker();
        ESMDataGenerator.createEmailMessage(trc.get(0));
        ESMDataGenerator.createContent(trc.get(0));
            //ESMDataGenerator.createUser();
          //  ESMDataGenerator.createEmailTemplate();
        ESMDataGenerator.getPackagedFieldName('hj');
        ESMDataGenerator.getWithNameSpace('ssd');
        ESMDataGenerator.getNamespacePrefix();
//             ESMDataGenerator.CreateOP();
//             ESMDataGenerator.CreateInPO();
           //   ESMDataGenerator.createIC();
        ESMDataGenerator.CreateRuleAlwaysMoveToQCAmount();
        ESMDataGenerator.getValidFieldSetName('CaseManager__c','abc');
        ESMDataGenerator.createIgnoreRule('abc');
        ESMDataGenerator.createAcceptRule('abc');
        ESMDataGenerator.createExitingCaseRule('abc');
        ESMDataGenerator.createIdentifyExitingCaseRule('abc');
    }
}