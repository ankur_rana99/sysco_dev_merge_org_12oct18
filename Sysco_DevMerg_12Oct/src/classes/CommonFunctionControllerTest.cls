@isTest
public class CommonFunctionControllerTest {
    static String namespace = UtilityController.esmNameSpace;
    public static testmethod void testmethod1() {
        Invoice_Configuration__c invconfig = new Invoice_Configuration__c();
        invconfig.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
        insert invconfig;

        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        OCR_Accuracy_Report_Mapping__c OCRrpt = new OCR_Accuracy_Report_Mapping__c();
        OCRrpt.FieldName_on_Object__c = 'Pickup_Allowance__c';
        OCRrpt.FieldName_on_OCR__c = 'Pickup_Allowance__c';
        OCRrpt.Object_Name__c = 'Invoice__c';
        insert OCRrpt;

        Account vendor = new Account();
        vendor.Name = 'Vendor';
        vendor.Vendor_Name__c = 'Vendor';
        vendor.Vendor_No__c = '01';
        vendor.Vendor_Type__c = 'F';
        vendor.isO_to_O__c = false;
        insert vendor;
        list < Account > ListVendor = [SELECT Id FROM Account];

        OPCO_Code__c opco = new OPCO_Code__c();
        opco.Name = '059';
        opco.OPCO_Code__c = '059';
        opco.OPCO_Name__c = '059';
        insert opco;
        list < OPCO_Code__c > ListOPCO = [SELECT Id, OPCO_Code__c, OPCO_Name__c FROM OPCO_Code__c];

        Invoice__c inv = new Invoice__c();
        inv.Invoice_Date__c = Date.Today();
        inv.Net_Due_Date__c = Date.Today().addDays(60);
        inv.Total_Amount__c = 1000;
        inv.WorkType__c = 'Freight Only';
        inv.Document_Type__c = 'PO Invoice';
        inv.Invoice_Type__c = 'F';
        inv.Current_State__c = 'Pending Cora Validations';
        inv.Invoice_No__c = '12345';
        inv.OPCO_Code__c = ListOPCO[0].Id;
        inv.OPCO__c = '059';
        inv.Vendor_No__c = '01';
        inv.Freight_Charges__c = 10;
        inv.Other_Charges_Surcharges__c = 10;
        inv.Samples_Charges__c = 10;
        inv.Pickup_Allowance__c = 10;
        inv.Invoice_Origin__c = '';
        insert inv;

        Purchase_Order__c po = new Purchase_Order__c();
        po.Amount__c = 100;

        insert po;
        Invoice_PO_Detail__c invpodtl = new Invoice_PO_Detail__c();
        invpodtl.Unique_Key__c = 'test';
        invpodtl.Invoice__c = inv.Id;
        invpodtl.Purchase_Order__c = po.Id;
        insert invpodtl;

        Invoice_Line_Item__c invlnitem = new Invoice_Line_Item__c();
        invlnitem.Invoice__c = inv.Id;
        insert invlnitem;

        GRN__c grn = new GRN__c();
        grn.Purchase_Order__c = po.Id;
        grn.Total_Merchandise_Amount__c = 1000;
        grn.Total_Freight_Amount__c = 1000;
        grn.Vendor__c = ListVendor[0].Id;
        grn.Total_Amount__c = 1000;
        insert grn;
        list < GRN__c > ListGRN = [SELECT Id FROM GRN__c];

        List < Invoice__c > invlst = new List < Invoice__c > ();
        invlst.add(inv);

        Map < String, string > strmap = new Map < String, string > ();
        strmap.put('Timezone|Timezone_For_History_Fields', 'test-temp-');
        strmap.put('Timezone|Date_Format_For_History_Fields', 'MM/dd/yyyy HH:mm');

        CommonFunctionController cmnfun = new CommonFunctionController();
        cmnfun.getCreatableFieldsSOQL('Invoice__c', 'test');

        set < String > poid = new set < String > ();
        poid.add(po.Id);

        UtilityController.InvFieldMap = strmap;

        CommonFunctionController.getSOQLQuery('Invoice__c');
        CommonFunctionController.getSOQLQuery('Invoice__c', '');
        CommonFunctionController.getAllFieldsOfObject('Invoice_Line_Item__c');
        CommonFunctionController.getFieldsFromFieldSet('Invoice__c', 'test');
        CommonFunctionController.getCreatableFieldsSOQL('Invoice__c', 'test', true);
        CommonFunctionController.getCreatableFieldsSOQL('Invoice__c', 'test', 'test', true);
        CommonFunctionController.getFieldsFromFieldSetNew('Invoice__c', 'Billing_Address');
        CommonFunctionController.getFieldsMapFromFieldSetNew('Invoice__c', 'test');
        CommonFunctionController.insertCommentHistory(invlst, 'temp', 'abc', true);
        CommonFunctionController.getGRNline(inv.Id);
        CommonFunctionController.getSetOfPOidFrminvoice(inv.Id);
        CommonFunctionController.getInvoiceConfiguration();
        CommonFunctionController.getGRNline(poid);
        CommonFunctionController.getRandomnumber(1);
        CommonFunctionController.InsertExceptionHistory(invlst, 'temp', strmap);
        CommonFunctionController.getInvLineItemQuery(inv, 'temp', '', 'abc', poid);
        CommonFunctionController.getSelectQueryFromFieldSet('Invoice__c', 'test123', 'test1', 'test', true);
        CommonFunctionController.fillInvLineItem('test', inv.Id);
        CommonFunctionController.getSobject(inv.Id, 'Invoice__c');
        CommonFunctionController.fillOtherChargesList('test', inv.Id);
        CommonFunctionController.getPOList('', poid);
        CommonFunctionController.getPOList('', 'test1', poid, '123');
        CommonFunctionController.getPOline(poid);
        CommonFunctionController.getInvoicePODetail('', inv.Id);
        //CommonFunctionController.getGrnList('',poid);
        //	CommonFunctionController.getReadOnlyFields(namespace,'temp2','');
        //	CommonFunctionController.getReadOnlyFields('a','b','c');
        //	CommonFunctionController.getGlobalConfiguration();
    }
    public static testmethod void testmethod2() {

        Invoice_Configuration__c invconfig = new Invoice_Configuration__c();
        invconfig.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
        insert invconfig;

        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;
        Purchase_Order__c po = new Purchase_Order__c();
        po.Amount__c = 100;
        insert po;

        Invoice__c inv = new Invoice__c();
        inv.Amount__c = 100;
        inv.Comments__c = 'test';
        inv.Invoice_No__c = 'ESM-1';
        //inv.User_Action__c = 'Validate';
        inv.Exception_Reason__c = 'temp';
        inv.Bill_To_City__c = 'test';
        inv.Purchase_Order__c = po.Id;
        insert inv;

        Invoice_PO_Detail__c invpodtl = new Invoice_PO_Detail__c();
        invpodtl.Unique_Key__c = 'test';
        invpodtl.Invoice__c = inv.Id;
        invpodtl.Purchase_Order__c = po.Id;
        insert invpodtl;

        List < Invoice__c > invlst = new List < Invoice__c > ();
        invlst.add(inv);

        Map < String, string > strmap = new Map < String, string > ();
        strmap.put('Timezone|Timezone_For_History_Fields', 'mm-dd-yyyy hh:mm');
        strmap.put('Timezone|Date_Format_For_History_Fields', 'MM/dd/yyyy HH:mm');

        CommonFunctionController cmnfun = new CommonFunctionController();

        cmnfun.getCreatableFieldsSOQL('Invoice__c', 'test');

        set < String > poid = new set < String > ();
        poid.add('test');
        
        UtilityController.InvFieldMap = strmap;

        CommonFunctionController.getSOQLQuery('Invoice__c');
        CommonFunctionController.getSOQLQuery('Invoice__c', '');
        CommonFunctionController.getAllFieldsOfObject('Invoice__c');
        CommonFunctionController.getFieldsFromFieldSet('Invoice__c', 'Basic_Information_Fields');
        CommonFunctionController.getCreatableFieldsSOQL('Invoice__c', 'test', true);
        CommonFunctionController.getCreatableFieldsSOQL('Invoice__c', 'test', 'test', true);
        CommonFunctionController.getFieldsFromFieldSetNew('Invoice__c', 'test');
        CommonFunctionController.getFieldsMapFromFieldSetNew('Invoice__c', 'Basic_Information_Fields');
        CommonFunctionController.insertCommentHistory(invlst, 'temp', 'abc', true);
        CommonFunctionController.insertCommentHistorySysco(invlst, 'Invoice__c', namespace, strmap);
        //CommonFunctionController.insertCommentHistoryField(invlst,namespace + 'Invoice__c','',strmap);
        CommonFunctionController.getGRNline(inv.Id);
        CommonFunctionController.getSetOfPOidFrminvoice(inv.Id);
        CommonFunctionController.getInvoiceConfiguration();
        CommonFunctionController.getGRNline(poid);
        CommonFunctionController.getRandomnumber(1);
        CommonFunctionController.InsertExceptionHistory(invlst, 'temp', strmap);
        CommonFunctionController.getInvLineItemQuery(inv, 'temp', 'test', 'abc', poid);
        CommonFunctionController.getSelectQueryFromFieldSet('Invoice__c', 'Basic_Information_Fields', '', 'test', true);
        CommonFunctionController.fillInvLineItem('', inv.Id);
        CommonFunctionController.getSobject(inv.Id, 'Invoice__c');
        CommonFunctionController.fillOtherChargesList('', inv.Id);
        CommonFunctionController.getPOList('', poid);
        Set < String > strst = new Set < String > ();
        CommonFunctionController.getPOList('', '', strst, '123');
        CommonFunctionController.getPOline(poid);
        CommonFunctionController.getInvoicePODetail('', inv.Id);
        //	CommonFunctionController.getReadOnlyFields(namespace + 'Invoice__c','Basic_Information_Fields','');
        //CommonFunctionController.getReadOnlyFields('a','b','c');
        //	CommonFunctionController.getGlobalConfiguration();
    }


}