@isTest
public class InteractionListInvoiceControllerTest {
    
 public static testMethod void testFirst(){
       System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
                  
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         
         Account accobj = new Account();
         accobj.Name='TestAccount';
         insert accobj;

         Invoice__c invobj = new Invoice__c();    
         invobj.Invoice_Date__c = Date.Today();
         invobj.Net_Due_Date__c = Date.Today().addDays(60);    
         invobj.Total_Amount__c = 500.00;
    
         System.debug('invobj1' + invobj);
         insert invobj;
     
        List<CaseManager__c> cts= ESMDataGenerator.createCases(1);
        CaseManager__c cm=cts.get(0);
        cm.Invoice__c=invobj.Id;
     
        InteractionListInvoiceController.getInteractionByCase(invobj.Id,1,15);
     
      ContentVersion cvu= new ContentVersion(
            versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body'),
             Is_Primary_Doc__c=false,
              Origin='H',
              PathOnClient='abc.pdf');
     
     insert cvu;
     
     String docQuery='SELECT Id,ContentDocumentId from ContentVersion where id=\''+cvu.Id+'\'';
      List<ContentVersion> ContentVersionList;
     
      ContentVersionList=(List<ContentVersion>)database.query(docQuery);
        
      ContentDocumentLink sdl=new ContentDocumentLink(
            ContentDocumentId=ContentVersionList[0].ContentDocumentId,
            LinkedEntityId=invobj.Id,
            ShareType='V',
            Visibility='AllUsers'); 
     insert sdl;
     
     EmailMessage em=new EmailMessage();
     insert em;
     List<EmailMessage> emaillist=new List<EmailMessage>();
     emaillist.add(em);
     InteractionListInvoiceController.InteractionWrapper interactionwrp=new InteractionListInvoiceController.InteractionWrapper();
     interactionwrp.pageSize=5;
     interactionwrp.page=2;
     interactionwrp.total=10;
     interactionwrp.emails=emaillist;
     
     InteractionListInvoiceController.getAttachments(invobj.Id);
     InteractionListInvoiceController.getContactsByInteraction(invobj.Id,invobj.Id,'vishwa.patel@sftpl.com');
     InteractionListInvoiceController.makeReadMail(em.Id);
     
 }
}