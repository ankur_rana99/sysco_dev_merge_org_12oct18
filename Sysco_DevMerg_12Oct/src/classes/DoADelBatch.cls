global class DoADelBatch implements Database.Batchable<SObject> {
	
	global List<DOA_Matrix__c> doaNeedsTobeDel = new List<DOA_Matrix__c>();

	global DoADelBatch(List<DOA_Matrix__c> doaNeedsTobeDel) {
		this.doaNeedsTobeDel = doaNeedsTobeDel;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global System.Iterable<SObject> start(Database.BatchableContext context) {
		return this.doaNeedsTobeDel;
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<DOA_Matrix__c> scope) {
		Database.delete(scope);	
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}