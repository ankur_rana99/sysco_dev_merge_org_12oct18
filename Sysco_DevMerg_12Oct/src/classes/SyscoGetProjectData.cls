global class SyscoGetProjectData implements database.Batchable<Sobject>,Schedulable,Database.Stateful,Database.AllowsCallouts{

private SchedulableContext objSchedulableContext;
private Project__c pro;
private Project__c proempty;
private List<Project__c> proList;
private List<Project__c> emptyproList = new List<Project__c>(); 

global SyscoGetProjectData(){       
}

global List<Project__c> start(Database.BatchableContext BC) {
    try{
        String dateFormatString = 'yyyy-MM-dd';
        pro = new Project__c();
        proempty = new Project__c();
        proList = new List<Project__c>();
        System.debug(' Method Called PC...');
        
        Date today = Date.today();
        Datetime dt1 = Datetime.newInstance(today.year(), today.month(),today.day());
        String tday = dt1.format(dateFormatString);
        
        Date previousday = Date.today() - 1;
        Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(),previousday.day());
        String prevday = dt2.format(dateFormatString);
        
        WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('ProjectData');
        if(WEEndpoint.isfirstrun__c == 'Y'){
           prevday = '';                      
        }
        
        String XMLString;
        HttpRequest feedRequest = new HttpRequest();
        feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + 'Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday);
        feedRequest.setMethod('GET');
        Http http = new Http();
        HTTPResponse ProjectResponse = http.send(feedRequest);
        System.debug(ProjectResponse.getBody());
        XMLString = ProjectResponse.getBody();
        DOM.Document doc=new DOM.Document();
        doc.load(XMLString);
        DOM.XmlNode rootNode=doc.getRootElement();
        parseXML(rootNode);
        if(pro!=null && pro!= proempty )
            proList.add(pro);
    }catch(exception e){
        system.debug(e.getMessage());
    }   
    return proList;    
}

global void Execute(Database.BatchableContext objBatchableContext,List<Project__c> projectList){
    try{
        if(projectList.size()>0 && projectList != emptyproList) {  
            system.debug('ccen.size()' + projectList.size());
            system.debug('projectList' + projectList);           
            upsert projectList Reference_ID__c;  
        }
        else
            system.debug('No Records to Process');                    
               
    }catch(exception e){
        system.debug(e.getMessage());
    }
}

global void finish(Database.BatchableContext BC) {
    WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('ProjectData'); 
    System.debug('WEEndpoint..... '+ WEEndpoint.isfirstrun__c);
    if(WEEndpoint.isfirstrun__c == 'Y'){
       System.debug('WEEndpoint.isfirstrun__c +'+ WEEndpoint.isfirstrun__c);
       WEEndpoint.isfirstrun__c = 'N';
       Update WEEndpoint;                       
    }   
 }
    
Private void parseXML(DOM.XMLNode node) {
    if(node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if(node.getName()=='Report_Entry'){
            if(pro!=null && pro!= proempty )
                proList.add(pro);
            pro = new Project__c();  
        }
        if(node.getName()=='projectName'){
            pro.Project_Name__c=node.getText().trim();
            if(pro.Project_Name__c.length() > 79)
                pro.Name=pro.Project_Name__c.substring(0,80);
            else
                pro.Name=pro.Project_Name__c;
        }
        if(node.getName()=='referenceID')
            pro.Reference_ID__c=node.getText().trim();
        if(node.getName()=='Inactive_-_Current')
            pro.Inactive__c=node.getText().trim();  
    }
    System.debug('pro------->' + pro);
    for (Dom.XMLNode child: node.getChildElements()) {
        parseXML(child);
    }
}

global void execute(SchedulableContext objSchedulableContext) {
     SyscoGetProjectData syscoProject  = new SyscoGetProjectData();
     Integer batchSize = 2000; 
     system.database.executebatch(syscoProject,batchSize); 
    }

}