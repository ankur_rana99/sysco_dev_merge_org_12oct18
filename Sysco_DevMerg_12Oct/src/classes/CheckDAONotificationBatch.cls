global class CheckDAONotificationBatch implements Database.Batchable<SObject>, Schedulable, Database.stateful {
    
    private Map<Id,List<Invoice__c>> userVsInvListRem = new Map<Id,List<Invoice__c>>();
    private Map<Id,List<Invoice__c>> userVsInvList1stEsc = new Map<Id,List<Invoice__c>>();
    private Map<Id,List<Invoice__c>> userVsInvList1stEscRem = new Map<Id,List<Invoice__c>>();
    private Map<Id,List<Invoice__c>> userVsInvList2ndEsc = new Map<Id,List<Invoice__c>>();
    
    private Integer remForDays;
    private Integer firtsEscRemAfter;
    private String buyerPortalURL;
    private String HTML_BODY_START;
    private String HTML_BODY_END;

    private String businessHoursId;


    /**
     * @description Executes the scheduled Apex job. 
     * @param sc contains the job ID
     */ 
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new CheckDAONotificationBatch(3,2), 200);
    }

    global CheckDAONotificationBatch() {
        this(3,2);
    }

    global CheckDAONotificationBatch(Integer remForDays, Integer firtsEscRemAfter) {
        this.remForDays = remForDays;
        this.firtsEscRemAfter = firtsEscRemAfter;
        buyerPortalURL = Invoice_Configuration__c.getOrgDefaults().Buyer_Portal_URL__c;
        HTML_BODY_START = '<p>Dear current_approver_name,</p>'+
                            '<p>The following invoices are awaiting for your action.</p>'+
                            '<table style="height: 47px;" border="1" width="551">'+
                            '<tbody>'+
                            '<tr>'+
                            '<td style="width: 129px;"><strong>Invoice No.</strong></td>'+
                            '<td style="width: 131px;"><strong>Vendor Name</strong></td>'+
                            '<td style="width: 131px;"><strong>Invoice Date</strong></td>'+
                            '<td style="width: 132px;"><strong>OPCO Code</strong></td>'+
                            '</tr>';
        HTML_BODY_END =  '</tbody>'+
                            '</table>'+
                            '<p>Request you to provide the required information/ approval for expediting the processing of the invoice.</p>'+
                            '<p>Login here <a href="'+buyerPortalURL+'" target="_blank" rel="noopener">Buyer Portal</a></p>'+
                            '<p>Regards,<br />Sysco Global AP Team</p>';
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        businessHoursId = bh.Id; 
    }
    
    /**
     * @description gets invoked when the batch job starts
     * @param context contains the job ID
     * @returns the record set as a QueryLocator object that will be batched for execution
     */ 
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('SELECT Id, Current_State_Assign_Date__c, Net_Due_Date__c, Document_Type__c, Vendor__r.Name, '+
                        'Current_Approval_Pending_Days__c, Current_Approver__r.Name, Current_Approver__r.Email__c ,Current_Approver__r.Id, Current_Approver__r.Supervisor_Email_Id__c, Name, '+
                        ' OPCO_Code__r.OPCO_Code__c, Invoice_Date__c '+
                                    'FROM Invoice__c where Current_Approval_Pending_Days__c != -1 '+
                                    'and Current_Approval_Pending_Days__c != null');
    }

    /**
     * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
     * @param context contains the job ID
     * @param scope contains the batch of records to process.
     */ 
    global void execute(Database.BatchableContext context, List<Invoice__c> scope) {
        
        for (Invoice__c invObject : scope){
                    
        //  Datetime myDate = datetime.newInstanceGmt(invObject.Current_State_Assign_Date__c.year(),invObject.Current_State_Assign_Date__c.month(),invObject.Current_State_Assign_Date__c.day());
        //  System.debug('Current Staste Assign date : '+invObject.Current_State_Assign_Date__c);
        //  System.debug('Newly created date : '+myDate);
            Long diff = BusinessHours.diff(businessHoursId, invObject.Current_State_Assign_Date__c, datetime.now());
            System.debug(diff);
            System.debug(datetime.now());
            Long finalPendingDays = diff/(1000*60*60*24);
            if (finalPendingDays >= 1 && finalPendingDays <= remForDays){
                //First 3 reminders
                addInvoiceInMapForUser(userVsInvListRem,invObject.Current_Approver__r.Id, invObject);
            }else if(finalPendingDays == (remForDays+1)){
                //1st escalation
                addInvoiceInMapForUser(userVsInvList1stEsc,invObject.Current_Approver__r.Id, invObject);
            }else if(finalPendingDays == (remForDays + firtsEscRemAfter+1)){
                //1st escalation reminder
                addInvoiceInMapForUser(userVsInvList1stEscRem,invObject.Current_Approver__r.Id, invObject);
            }
            /*else if(finalPendingDays > (remForDays + firtsEscRemAfter +1)){
                //2nd escalation
                addInvoiceInMapForUser(userVsInvList2ndEsc,invObject.Current_Approver__r.Id, invObject);
            }*/

        }

        
        
    }

    global static void addInvoiceInMapForUser(Map<Id,List<Invoice__c>> userVsInvoiceMap, Id userId, Invoice__c invObj) {
        if (userVsInvoiceMap.containsKey(userId))
        {
            userVsInvoiceMap.get(userId).add(invObj);   
        }else{
            List<Invoice__c> invList = new List<Invoice__c>();
            invList.add(invObj);
            userVsInvoiceMap.put(userId,invList);
        }
    }

    
    /**
     * @description gets invoked when the batch job finishes. Place any clean up code in this method.
     * @param context contains the job ID
     */ 
    global void finish(Database.BatchableContext context) {
    List<Messaging.SingleEmailMessage>  myEmails = new List<Messaging.SingleEmailMessage>();
        System.debug('-----Reminder--------');
        if (userVsInvListRem.size() > 0)
        {
            System.debug('-----Reminder--------');
            for (Id userId : userVsInvListRem.keySet())
            {
                myEmails.add(this.prepareEmial('rem',userId,userVsInvListRem));
            }                       
        }
        if (userVsInvList1stEsc.size() > 0)
        {
            System.debug('-----1st Escalation--------');
            for (Id userId : userVsInvList1stEsc.keySet())
            {
                myEmails.add(this.prepareEmial('1stEsc',userId,userVsInvList1stEsc));               
            }
        }
        if (userVsInvList1stEscRem.size() > 0)
        {
            System.debug('-----1st Escalation Reminder--------');
            for (Id userId : userVsInvList1stEscRem.keySet())
            {               
                myEmails.add(this.prepareEmial('1stEscRem',userId,userVsInvList1stEscRem));
            }
        }
        if (userVsInvList2ndEsc.size() > 0)
        {
            System.debug('-----2nd Escation--------');
            for (Id userId : userVsInvList2ndEsc.keySet())
            {
                
                    myEmails.add(this.prepareEmial('2ndEsc',userId,userVsInvList2ndEsc));
                
            }
        }
        System.debug(myEmails);
        try{
            Messaging.sendEmail(myEmails);
        } Catch(Exception e){
            System.debug('Exception Occured while sending Emails from DOA Notification Batch : ' + e);
        }
        
    }

    global Messaging.SingleEmailMessage prepareEmial(String type,Id userId, Map<Id,List<Invoice__c>> userVsInvList){
            System.debug(userId);
                
                
                String toEmialAdd = null;  
                String supEmailAdd = null;
                String buyerName = null;
                
                String invoiceTable = '';
                for (Invoice__c invObj : userVsInvList.get(userId))
                {                   
                    invoiceTable += '<tr>'+
                    '<td style="width: 129px;">'+(( invObj.Name == null ||  invObj.Name == '') ? '-' : invObj.Name) +'</td>'+
                    '<td style="width: 131px;">'+(( invObj.Vendor__c == null || invObj.Vendor__r.Name == null ||  invObj.Vendor__r.Name == '') ? '-' : invObj.Vendor__r.Name)+'</td>'+
                    '<td style="width: 131px;">'+(( invObj.Invoice_Date__c == null ) ? '-' : String.valueOf(invObj.Invoice_Date__c))+'</td>'+
                    '<td style="width: 132px;">'+(( invObj.OPCO_Code__r == null || invObj.OPCO_Code__r.OPCO_Code__c == null ||  invObj.OPCO_Code__r.OPCO_Code__c == '') ? '-' : invObj.OPCO_Code__r.OPCO_Code__c)+'</td>'+
                    '</tr>';    
                    if (toEmialAdd == null && invObj.Current_Approver__c != null)
                    {
                        toEmialAdd =    invObj.Current_Approver__r.Email__c;
                    }
                    if (buyerName == null && invObj.Current_Approver__c != null)
                    {
                        buyerName =     invObj.Current_Approver__r.Name;
                    }
                    if (supEmailAdd == null && invObj.Current_Approver__c != null)
                    {
                        supEmailAdd = invObj.Current_Approver__r.Supervisor_Email_Id__c;
                    }
                            
                }
                System.debug('HTML_BODY_START--' +HTML_BODY_START);
                System.debug('buyerName--' +buyerName);
        		//OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'donotreply@sysco.com'];
       			//system.debug('oranization wide addes is'+owea.size());
                String htmlBody = HTML_BODY_START.replaceAll('current_approver_name',buyerName);
                htmlBody += invoiceTable;
                htmlBody += HTML_BODY_END;
                System.debug(htmlBody); 
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();                            
                email.setSubject('Daily Notification - Pending Invoices');
                email.setToAddresses(new String[] {toEmialAdd});
        		/*if ( owea.size() > 0 ) {
                    
   				 	email.setOrgWideEmailAddressId(owea.get(0).Id);
				}*/
                List<String> ccList = new List<String>();
            //  ccList.add('dilshad.shaikh@sftpl.com');
                if (type == '1stEsc' || type == '1stEscRem'){
                    ccList.add(supEmailAdd);                    
                }
                if (type == '2ndEsc'){
                    //Add SBS for cc 
                    ccList.add(supEmailAdd);                
                }
                if (ccList.size() > 0)
                {
                    email.setccAddresses(ccList);
                }               
                
                //email.setSaveAsActivity(false);
                //email.setTargetObjectId(item.OwnerId);
                //email.setTemplateId(emailTemplate.Id);
            //  email.setWhatId(userId);
                email.setHtmlBody(htmlBody);
                return email;
    }
}