@isTest
public class AutoClosureNotificationHelperTest {
    @testSetup 
    static void setupTestData() {
        
        EmailTemplate validEmailTemplate = new EmailTemplate();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'System Closure Email(Auto Closure)';
            validEmailTemplate.DeveloperName = 'System_Closure_Email';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }
        
        //Create Default account object
        Account acc=new Account();
        acc.Name=ESMConstants.DEFAULT_ACCOUNT_NAME;
        insert acc;
        
        

    }
    
    public static testMethod void testInitializeComponent() {
	    List<CaseManager__c> cases =   ESMDataGenerator.createCases(2);

        EmailMessage newEmail = TestDataGenerator.createEmailMessage(cases[0]);
        AutoClosureNotificationHelper.sendCaseAutoClosureNotification(cases);
        Map<Id, CaseManager__c> caseTrackerOldMap = new Map<Id,CaseManager__c>();
        for(CaseManager__c cas : cases){
           cas.Current_State__c = 'Pending For Archival'; 
           caseTrackerOldMap.put(cas.Id, cas);
        }
        update cases;
        
		for(Id casId : caseTrackerOldMap.keyset()){
            caseTrackerOldMap.get(casId).Current_State__c = 'Pending on Global Team';
        }
        AutoClosureNotificationHelper.sendCaseClosureNotification(cases,caseTrackerOldMap);
        
        
    }
}