@isTest
public class CheckDAONotificationBatchTest{

	public static testMethod void testFirst(){
		System_Configuration__c sysconfig=new System_Configuration__c ();
		sysconfig.Module__c='Custom_Record_History';
		sysconfig.Value__c ='Invoice__c';
		insert sysconfig;

		System_Configuration__c sysconfig2 = new System_Configuration__c(); 
		sysconfig2.Module__c='Invoice__c'; 
		sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State'; 
		sysconfig2.Value__c = 'Awaiting OCR Feed'; 
		insert sysconfig2;

		Invoice_Configuration__c invConf = new Invoice_Configuration__c();
		invConf.Enable_Quantity_Calculation__c = true;
		invConf.GRN_Flag_In_PO_Header__c = true;
		invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
		invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
		invConf.Enable_Auto_Flipping_With_GRN__c = true;
		invConf.PO_Number_Field_for_Search__c = 'Name';
		invConf.Invoice_Reject_Current_State__c ='';
		invConf.Auto_Match_Flow_Method__c = 'Sequence';
		invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
		invConf.Auto_Validation_Current_States__c = '';
		insert invConf;

		OPCO_Code__c opco = new OPCO_Code__c();
		opco.Name = '059';
		opco.OPCO_Code__c = '059';
		opco.OPCO_Name__c = '059';
		insert opco;

		OPCO_Code__c opco1 = new OPCO_Code__c();
		opco1.Name = '011';
		opco1.OPCO_Code__c = '011';
		opco1.OPCO_Name__c = '011';
		insert opco1;

		Account acc = new Account();
		acc.Unique_Key__c = '059-100';
		acc.Vendor_No__c = '100';
		acc.OPCO_Code__c = opco.Id;
		acc.Name = 'Cora';
		acc.Vendor_Type__c = 'F';
		acc.Payment_Vendor_Terms__c = '059-NET25';
		acc.Vendor_No__c = '100';
		insert acc;

		Account acc1 = new Account();
		acc1.Unique_Key__c = '059-101';
		acc1.Vendor_No__c = '101';
		acc1.OPCO_Code__c = opco.Id;
		acc1.Name = 'Cora';
		acc1.Vendor_Type__c = 'M';
		acc1.Payment_Vendor_Terms__c = '059-NET25';
		acc1.Vendor_No__c = '101';
		insert acc1;

		User_Master__c ums = new User_Master__c();
		ums.Name = 'Cora';
		ums.User_Name__c = 'cora@cora.com';
		ums.Email__c = 'cora@cora.com';
		ums.First_Name__c = 'cora';
		ums.Status__c = 'Active';
		ums.Entity__c = 'OpCo';
		ums.OPCO_Code__c = opco.Id;
		ums.Title_Type__c = 'Functional';
		insert ums;

		User_Master__c um = new User_Master__c();
		um.Name = 'Cora User';
		um.User_Name__c = 'corauser@cora.com';
		um.Email__c = 'corauser@cora.com';
		um.First_Name__c = 'cora user';
		um.Status__c = 'Active';
		um.Entity__c = 'OpCo';
		um.OPCO_Code__c = opco.Id;
		um.Title_Type__c = 'Functional';
		um.Supervisor__c = ums.Id;
		insert um;



		Buyer_Code__c bc = new Buyer_Code__c();
		bc.OPCO_Code__c = opco.Id;
		bc.Name = '080';
		bc.Buyer_ID__c = '080';
		bc.Unique_Key__c = '059-080';
		bc.Buyer_User_ID__c = um.Id;
		insert bc;

		Purchase_Order__c  po = new Purchase_Order__c();
		po.PO_No__c = '100';
		po.OPCO_Code__c = opco.Id;
		po.Amount__c = 123;
		po.PO_Type__c = 'F';
		po.Vendor__c = acc.Id;
		po.Freight_Terms__c = 'NET25';
		po.Payment_Vendor_Terms__c = 'NET25';
		po.Unique_Key__c = '059-100';
		po.Buyer_ID__c = '080';
		insert po;

		String fieldValue = String.valueOf(po.Id);

		list<Purchase_Order__c> Listpo = new list<Purchase_Order__c>();
		Listpo.add(po);

		Payment_Term__c pt = new Payment_Term__c();
		pt.Name = 'NET25';
		pt.OPCO_Code__c = opco.Id;
		pt.Discount_Days__c = 0;
		pt.Discount_Percentage__c = 0;
		pt.Transaction_Type__c = 'F';
		pt.Unique_Key__c = '059-NET25';
		pt.Due_Date__c = 0;
		pt.Due_Days__c = 25;
		pt.Due_Month__c = 0;
		insert pt;

		Invoice__c invobj = new Invoice__c();    
		invobj.Invoice_Date__c = Date.Today();
		invobj.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj.Total_Amount__c = 500.00;
		invobj.Current_State__c='Vouched';
		invobj.Invoice_Type__c = 'M';
		invobj.WorkType__c = 'CPAS';
		invobj.Invoice_Origin__c  ='test';
		invobj.Payment_Terms_Inv__c = '059-NET25';
		invobj.OPCO__c = '059';
		invobj.OPCO_Code__c = opco.Id;
		invobj.Current_Approver__c = um.Id;
		invobj.Current_State_Assign_Date__c = System.now() - 1;
		insert invobj;

		Invoice__c invobj1 = new Invoice__c();    
		invobj1.Invoice_Date__c = Date.Today();
		invobj1.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj1.Total_Amount__c = 500.00;
		invobj1.Current_State__c='Vouched';
		invobj1.Invoice_Type__c = 'M';
		invobj1.WorkType__c = 'PO Invoice';
		invobj1.Invoice_Origin__c  ='EDI';
		invobj1.Vendor__c = acc.Id;
		invobj1.Payment_Terms_Inv__c = '059-NET25';
		invobj1.Purchase_Order__c = po.Id;
		invobj1.OPCO__c = '059';
		invobj1.OPCO_Code__c = opco.Id;
		invobj1.Current_Approver__c = um.Id;
		invobj1.Current_State_Assign_Date__c = System.now() - 2;
		insert invobj1;

		Invoice__c invobj2 = new Invoice__c();    
		invobj2.Invoice_Date__c = Date.Today();
		invobj2.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj2.Total_Amount__c = 500.00;
		invobj2.Current_State__c='Vouched';
		invobj2.Invoice_Type__c = 'M';
		invobj2.WorkType__c = 'PO Invoice';
		invobj2.Vendor__c = acc1.Id;
		invobj2.Payment_Terms_Inv__c = '059-NET25';
		invobj2.Purchase_Order__c = po.Id;
		invobj2.OPCO__c = '059';
		invobj2.OPCO_Code__c = opco.Id;
		invobj2.Current_Approver__c = um.Id;
		invobj2.Current_State_Assign_Date__c = System.now() - 3;
		insert invobj2;

		Invoice__c invobj3 = new Invoice__c();    
		invobj3.Invoice_Date__c = Date.Today();
		invobj3.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj3.Total_Amount__c = 500.00;
		invobj3.Current_State__c='Vouched';
		invobj3.Invoice_Type__c = 'A';
		invobj3.WorkType__c = 'PO Invoice';
		invobj3.Vendor__c = acc1.Id;
		invobj3.Payment_Terms_Inv__c = '059-NET25';
		invobj3.Purchase_Order__c = po.Id;
		invobj3.OPCO__c = '011';
		invobj3.OPCO_Code__c = opco1.Id;
		invobj3.Current_Approver__c = um.Id;
		invobj3.Current_State_Assign_Date__c = System.now() - 10;
		insert invobj3;
		
		Invoice__c invobj4 = new Invoice__c();    
		invobj4.Invoice_Date__c = Date.Today();
		invobj4.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj4.Total_Amount__c = 500.00;
		invobj4.Current_State__c='Vouched';
		invobj4.Invoice_Type__c = 'A';
		invobj4.WorkType__c = 'PO Invoice';
		invobj4.Vendor__c = acc1.Id;
		invobj4.Payment_Terms_Inv__c = '059-NET25';
		invobj4.Purchase_Order__c = po.Id;
		invobj4.OPCO__c = '011';
		invobj4.OPCO_Code__c = opco1.Id;
		invobj4.Current_Approver__c = um.Id;
		invobj4.Current_State_Assign_Date__c = System.now() - 60;
		insert invobj4;
		
		Invoice__c invobj5 = new Invoice__c();    
		invobj5.Invoice_Date__c = Date.Today();
		invobj5.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj5.Total_Amount__c = 500.00;
		invobj5.Current_State__c='Vouched';
		invobj5.Invoice_Type__c = 'A';
		invobj5.WorkType__c = 'PO Invoice';
		invobj5.Vendor__c = acc1.Id;
		invobj5.Payment_Terms_Inv__c = '059-NET25';
		invobj5.Purchase_Order__c = po.Id;
		invobj5.OPCO__c = '011';
		invobj5.OPCO_Code__c = opco1.Id;
		invobj5.Current_Approver__c = um.Id;
		invobj5.Current_State_Assign_Date__c = System.now() - 365;
		insert invobj5;

		list<Invoice__c> listInv = new list<Invoice__c>();
		listInv.add(invobj);
		listInv.add(invobj1);
		listInv.add(invobj2);
		listInv.add(invobj3);
		listInv.add(invobj4);
		listInv.add(invobj5);

		Map<Id,List<Invoice__c>> userVsInvoiceMap = new Map<Id,List<Invoice__c>>();
		list<Invoice__c> listInvEmail = new list<Invoice__c>();
		listInvEmail.add(invobj);
		userVsInvoiceMap.put(um.Id,listInvEmail);
		Test.startTest();
        
		CheckDAONotificationBatch.addInvoiceInMapForUser(userVsInvoiceMap,invobj.id,invobj);
	
		CheckDAONotificationBatch CDNB= new CheckDAONotificationBatch(); 
		CDNB.execute(null);
        //CDNB.execute(null,listInvEmail);
		
		Test.StopTest();
	}

}