@isTest
public class UpdateApprovalDetailApproveRejectTest{
	public static testMethod void testFirst(){
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
        
		Invoice__c inv = ESMDataGenerator.insertPOInvoice();
  		Approval_Detail__c ad = new Approval_Detail__c ();
     	//ad.Name = 'APD-00000313';
		ad.Email__c= 'abc@123.cpm';
     	ad.Approver_Action__c = 'Approve';
     	ad.Approval_Cycle__c = 1;
     	ad.Status__c = 'Rejected';
     	//ad.Vendor_Maintenance__c = '';
     	ad.Invoice__c = inv.Id;
     	insert ad;
     	system.debug('ad---'+ad);
     	List<Approval_Detail__c> appDetail = new List<Approval_Detail__c>();
     	appDetail.add(ad);
     	UpdateApprovalDetailApproveReject.checkUserDOA(appDetail);
 	}
}