/*
  Authors      :   Rahul Pastagiya
  Date created :   09/01/2018
  Purpose      :   Helper class for Case Manager object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
            Date:  
            Purpose of modification:  
            Method/Code segment modified: 
 */
public class CaseManagerHandler {
     /*
        Authors: Rahul Pastagiya
        Purpose: To update pervious queue when any user is assigned to case
        Dependencies : NA
    */
    public static void updatePrevQueue(List<CaseManager__c> newCaseTracker, List<CaseManager__c> oldCaseTracker){
        Set<Id> ownerIds = new Set<Id> ();
        //To get all cases owner
        //
        for (CaseManager__c ct : oldCaseTracker) {
            if (ct.OwnerId != null) {
                ownerIds.add(ct.OwnerId);
            }
        }
        system.debug('old:::::::'+oldCaseTracker);
        system.debug(':::::::'+newCaseTracker+':'+oldCaseTracker);
        //Logic for Previous Queue and Current Owner       
        Map<Id, Group> groups = new Map<Id, Group> ([SELECT Id, DeveloperName FROM Group where type = 'Queue' and Id in :ownerIds]);
        system.debug('---->'+[SELECT Id, DeveloperName, type FROM Group]);
        for (Integer i = 0; i < newCaseTracker.size(); i++) {
            Id newOwnerId = newCaseTracker[i].OwnerId;
            Id currentOwnerId = oldCaseTracker[i].OwnerId;
            
            if(newCaseTracker[i].Current_State__c != oldCaseTracker[i].Current_State__c){
                system.debug(newCaseTracker[i].Current_State__c+':'+oldCaseTracker[i].Current_State__c);
                system.debug(newOwnerId+':'+currentOwnerId+':'+(groups.get(currentOwnerId)));
				if (string.valueOf(newOwnerId).startsWith('00G')) {
                    newCaseTracker[i].Previous_Queue_Name__c = null; 
                } else if (string.valueOf(currentOwnerId).startsWith('00G') && string.valueOf(newOwnerId).startsWith('005')) {
                    newCaseTracker[i].Previous_Queue_Name__c = String.valueOf((groups.get(currentOwnerId)).get('DeveloperName'));
                }
                
                if(string.valueOf(currentOwnerId).startsWith('005')){
                    newCaseTracker[i].Previous_Performer__c = oldCaseTracker[i].OwnerId;
                }                
            }
           /* if (newOwnerId != currentOwnerId) {
                if (string.valueOf(newOwnerId).startsWith('00G')) {
                    newCaseTracker[i].Previous_Queue_Name__c = null;
                } else if (string.valueOf(currentOwnerId).startsWith('00G') && string.valueOf(newOwnerId).startsWith('005')) {
                    newCaseTracker[i].Previous_Queue_Name__c = String.valueOf((groups.get(currentOwnerId)).get('DeveloperName'));
                }
                
                if(string.valueOf(currentOwnerId).startsWith('005')){
                    newCaseTracker[i].Previous_Performer__c = oldCaseTracker[i].OwnerId;
                }
                
            }
            */
        }
    }
    
     /*
        Authors: Niraj Prajapati
        Purpose: To populate report related fields
        Dependencies : NA
    */
    public static void populateReportRelatedFields(List<CaseManager__c> newCaseTracker, List<CaseManager__c> oldCaseTracker){
        //Code Start for Populate Report related Fields - Niraj Prajapati
        //Logic for update previous performer on every status change
        for (Integer i = 0; i < newCaseTracker.size(); i++) {
            Id newOwnerId = newCaseTracker[i].OwnerId;
            Id currentOwnerId = oldCaseTracker[i].OwnerId;
            if((newCaseTracker[i].Current_State__c != oldCaseTracker[i].Current_State__c) ){
                newCaseTracker[i].Current_State_Time__c= System.now();
                if((newCaseTracker[i].Current_State__c != 'Start' || newCaseTracker[i].Current_State__c !='') && (String.valueOf(currentOwnerId).startsWith('005')))
                    newCaseTracker[i].Previous_Performer__c = oldCaseTracker[i].OwnerId;
            }
            //System.debug('*****'+String.valueOf(newOwnerId).startsWith('005'));
            if((newCaseTracker[i].Current_State__c == 'Ready For Processing') && (String.valueOf(newOwnerId).startsWith('005')) && (String.isempty(oldCaseTracker[i].First_Touch__c) || oldCaseTracker[i].First_Touch__c==null) ){
                newCaseTracker[i].First_Touch__c =  userinfo.getuserid();
                /*Code Start for PUX-464 */
                newCaseTracker[i].First_Touch_Date__c = System.now();
                /*Code End for PUX-464 */ 
            }
            //Logic for update Rejected Date
            if((newCaseTracker[i].User_Action__c != oldCaseTracker[i].User_Action__c) && (newCaseTracker[i].User_Action__c=='Reject' || newCaseTracker[i].User_Action__c=='Rejected')   ){
                newCaseTracker[i].Rejected_Date__c = System.now();
            }
            //Logic for update Process Date,First Pass Yield and Original Processer
           if(newCaseTracker[i].Current_State__c == 'Ready For Processing' && newCaseTracker[i].User_Action__c=='Process' && (newCaseTracker[i].User_Action__c != oldCaseTracker[i].User_Action__c)){
                newCaseTracker[i].Processed_Date__c = System.today();
                if(oldCaseTracker[i].First_Pass_Yield__c =='' || oldCaseTracker[i].First_Pass_Yield__c == null)
                    newCaseTracker[i].First_Pass_Yield__c = 'Y';
                if(String.isempty(oldCaseTracker[i].Processor_Name__c) || oldCaseTracker[i].Processor_Name__c == null)
                    newCaseTracker[i].Processor_Name__c =  userinfo.getuserid();
                if(String.isempty(oldCaseTracker[i].Original_Processor__c) || oldCaseTracker[i].Original_Processor__c == null)
                    newCaseTracker[i].Original_Processor__c =  userinfo.getuserid();
            }
            if(newCaseTracker[i].Current_State__c == 'Ready For Processing' && newCaseTracker[i].User_Action__c!='Process' && (newCaseTracker[i].User_Action__c != oldCaseTracker[i].User_Action__c)){
                //newCaseTracker[i].Processed_Date__c = System.today();
                if(oldCaseTracker[i].First_Pass_Yield__c =='' || oldCaseTracker[i].First_Pass_Yield__c == null)
                    newCaseTracker[i].First_Pass_Yield__c = 'N';
                if(String.isempty(oldCaseTracker[i].Processor_Name__c) || oldCaseTracker[i].Processor_Name__c == null)
                    newCaseTracker[i].Processor_Name__c =  userinfo.getuserid();
                if(String.isempty(oldCaseTracker[i].Original_Processor__c) || oldCaseTracker[i].Original_Processor__c == null)
                    newCaseTracker[i].Original_Processor__c =  userinfo.getuserid();
            }
            //Logic for update isQCed,QCStatusand QCPerformer fields
            if(newCaseTracker[i].Current_State__c == 'Ready For QC' ){
                if(String.isempty(oldCaseTracker[i].QC_Performer__c) || oldCaseTracker[i].QC_Performer__c == null)
                    newCaseTracker[i].QC_Performer__c =  userinfo.getuserid();
                if(oldCaseTracker[i].QC_Status__c=='' || oldCaseTracker[i].QC_Status__c == null){
                    if(newCaseTracker[i].User_Action__c == 'QC Complete')
                        newCaseTracker[i].QC_Status__c = 'Pass';
                    else if(newCaseTracker[i].User_Action__c == 'QC Reject'){
                        newCaseTracker[i].QC_Status__c = 'Fail';
                        newCaseTracker[i].Rejector_Name__c = userinfo.getuserid();
                    }
                }
                if(oldCaseTracker[i].IsQCed__c=='' || oldCaseTracker[i].IsQCed__c == null){
                    newCaseTracker[i].IsQCed__c = 'Yes';
                }    
            }
        }
        //Code End for Populate Report related Fields - Niraj Prajapati
    }
}