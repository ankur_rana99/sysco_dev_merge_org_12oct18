public class EDItoPDFGenrate{
  @InvocableMethod(label='Generate EDI to PDF' description='This method genrate PDF invoice and attached it to the invoice')
  public static void GeneratePDF(List<invoice__c> invoices) {
        try
        {
            list<attachment> PdfInvoices=new list<attachment>();
            for(Invoice__c inv:invoices)
            {
                //Code added by dhwanil to add pdf in the attachement
                PageReference pdf = Page.EDItoPDF;
                pdf.getParameters().put('invid',(String)inv.id);
                pdf.setRedirect(true);
                // create the new attachment
                Attachment attach = new Attachment();
                // the contents of the attachment from the pdf
                Blob body;
                if(!Test.isRunningTest()){
                    body = pdf.getContent();
                }else{
                    body = blob.valueOf('ERROR in PDF Generation.');
                }
                
                attach.Body = body;
                // add the user entered name
                attach.Name = inv.id + '.pdf';
                attach.IsPrivate = false;
                // attach the pdf to the account
                attach.ParentId = inv.id;
                PdfInvoices.add(attach);
            }
            insert PdfInvoices;
        }
        catch(Exception e)
        {
            system.debug('------Batch Process Error------:'+e.getcause());
        }
    }
}