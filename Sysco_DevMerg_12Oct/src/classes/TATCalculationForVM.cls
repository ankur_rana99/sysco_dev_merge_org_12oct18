public class TATCalculationForVM {
  @InvocableMethod(label = 'Statewise TAT Calculation For VM' description = 'Statewise TAT Calculation For VM')
  public static void calculateStatewiseTAT(List<Vendor_Maintenance__c> vendors) {
    TATCalculation.calculateStateChange(vendors, 'Vendor_Maintenance__c');
  }
}