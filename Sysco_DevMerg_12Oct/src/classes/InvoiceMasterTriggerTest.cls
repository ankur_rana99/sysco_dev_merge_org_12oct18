@IsTest
public class InvoiceMasterTriggerTest {
    public static string esmNamespace = UtilityController.esmNamespace;
    public static testmethod void testCase1() {
        system.assert(true, true);
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;
        ESMDataGenerator.insertSystemConfiguration();
        ESMDataGenerator.insertInvoiceConfiguration();
        ESMDataGenerator.insertTriggerConfiguration(false);

        Account acc = new Account(name = 'User2545Name@test.com');
        insert acc;

        
        
        List<Invoice__c> invl = new List<Invoice__c> ();
        invl.addAll(ESMDataGenerator.insertPOInvoice(2));
        invl.addAll(ESMDataGenerator.insertNONPOInvoice(2));
        Purchase_Order__c po = ESMDataGenerator.insertPurchaseOrder();
        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', invl.size());
        for (Purchase_Order__c pos : poList)
        {
            pos.Vendor__c = acc.Id;
        }
        upsert poList;
                
        Invoice__c obj = new Invoice__c();
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 100;
        obj.Comments__c = 'test comment';
        //obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Total_Amount__c = 100;
        obj.Purchase_Order__c=po.Id;
        obj.Vendor__c=acc.Id;
        //obj.Amount_Currency__c = 100;
        //obj.Do_GRN_Match__c = true;
        System.debug('esmnamesesmNameSpace = ' + esmNameSpace);
        //if (UtilityController.isFieldCreateable(esmNameSpace+'Invoice__c', esmNameSpace+'Document_Type__c,'+esmNameSpace +'Invoice_No__c,'+esmNameSpace+'Invoice_Date__c,Amount__c,Comments__c,User_Actions__c,Tracker_Id__c,Current_State__c')) {
        insert obj;
        ESMDataGenerator.insertInvoicePODetail(obj.Id,po.Id);
        Contact c = new Contact();
        c.FirstName = 'Paul';
        c.LastName  = 'Test';
        c.AccountId = acc.Id;       
        insert c;

   /*     User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = '11kk',
         Title = 'ab',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         ContactId=c.Id

        );
        insert u;*/

        OCR_Accuracy_Report_Mapping__c OCRrpt=new OCR_Accuracy_Report_Mapping__c();
        OCRrpt.FieldName_on_Object__c='Pickup_Allowance__c';
        OCRrpt.FieldName_on_OCR__c='Pickup_Allowance__c';
        OCRrpt.Object_Name__c='Invoice__c';
        insert OCRrpt;

        obj.Comments__c ='Temp Comment';
       // obj.User_Action__c='Bulk Assignment';
        
        update obj;
        
      
    }
    public static testmethod void testCase2() {
        system.assert(true, true);
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;
        ESMDataGenerator.insertSystemConfiguration();
        System_Configuration__c sysConf = new System_Configuration__c();
        sysConf.Module__c = 'Invoice__c';
        sysConf.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysConf.Value__c = 'Ready For Processing';
        insert sysConf;
       
        ESMDataGenerator.insertInvoiceConfiguration();
        ESMDataGenerator.insertTriggerConfiguration(true);
        

        Account acc = new Account(name = 'User2545Name@test.com');
        insert acc;

    

        List<Invoice__c> invl = new List<Invoice__c> ();
        invl.addAll(ESMDataGenerator.insertPOInvoice(2));
        invl.addAll(ESMDataGenerator.insertNONPOInvoice(2));
        Purchase_Order__c po = ESMDataGenerator.insertPurchaseOrder();
        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', invl.size());
        for (Purchase_Order__c pos : poList)
        {
            pos.Vendor__c = acc.Id;
        }
        upsert poList;
                
        Invoice__c obj = new Invoice__c();
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 100;
        obj.Comments__c = 'test comment';
        //obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Total_Amount__c = 100;
        obj.Purchase_Order__c=po.Id;
        obj.Vendor__c=acc.Id;
        //obj.Amount_Currency__c = 100;
        //obj.Do_GRN_Match__c = true;
        System.debug('esmnamesesmNameSpace = ' + esmNameSpace);
        //if (UtilityController.isFieldCreateable(esmNameSpace+'Invoice__c', esmNameSpace+'Document_Type__c,'+esmNameSpace +'Invoice_No__c,'+esmNameSpace+'Invoice_Date__c,Amount__c,Comments__c,User_Actions__c,Tracker_Id__c,Current_State__c')) {
        insert obj;
        ESMDataGenerator.insertInvoicePODetail(obj.Id,po.Id);
        Contact c = new Contact();
        c.FirstName = 'Paul';
        c.LastName  = 'Test';
        c.AccountId = acc.Id;
        
        insert c;

        OCR_Accuracy_Report_Mapping__c OCRrpt=new OCR_Accuracy_Report_Mapping__c();
        OCRrpt.FieldName_on_Object__c='Pickup_Allowance__c';
        OCRrpt.FieldName_on_OCR__c='Pickup_Allowance__c';
        OCRrpt.Object_Name__c='Invoice__c';
        insert OCRrpt;

        obj.Comments__c ='Temp Comment';
      //  obj.User_Action__c='Bulk Assignment';
        
        update obj;
        
      
    }
    
}