public class SyscoInvoiceLineSequence {
    
    @InvocableMethod(label = 'Invoice Line Sequence' description = 'Invoice Line Sequence')
    public static void executeInvoiceLineSequence(List<Invoice__c> lstInvoices) {
        String esmNameSpace = UtilityController.esmNameSpace;
        set<Id> invIdSet = new set<Id>();
        map<String, String> mapInvRef = new map<String, String>();
        System.debug('***********Calling executeInvoiceLineSequence************');
        for(Invoice__c inv : lstInvoices){
            invIdSet.add(inv.Id);
            mapInvRef.put(inv.Id, inv.Cora_Ref_No__c);
            
        }
        List<Invoice_Line_Item__c> lstLines = Database.query(CommonFunctionController.getSOQLQuery(esmNameSpace+'Invoice_Line_Item__c', 'Invoice__c IN: invIdSet and Invoice__c !=null ORDER BY Name'));
        
        Integer lineItemCnt = 1;
        for(Invoice_Line_Item__c line : lstLines){
            //if(String.isBlank(line.Invoice_Line_Item_No__c) && String.isBlank(line.Invoice_Line_No__c) && mapInvRef != null && mapInvRef.get(line.Invoice__c) != null){
            if(mapInvRef != null && mapInvRef.get(line.Invoice__c) != null){
                line.Invoice_Line_No__c  = String.valueOf(lineItemCnt);
                line.Invoice_Line_Item_No__c = mapInvRef.get(line.Invoice__c) + '-' + String.valueOf(lineItemCnt);
            }
            lineItemCnt++;
        }
        update lstLines;
    }

}