public class RelatedListComponentController { 
@AuraEnabled
    public static RelatedListWrapper getRelatedListDataServer(String objectName, String objrecordId, String jsonObj , Integer pageNumber, Integer recordsToDisplay, String relationField ,String whereClauseJson,String orderByJson) {
        RelatedListWrapper rlist = new RelatedListWrapper();
        System.debug('relationField :'+relationField);
        try {
            Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
            String totalCaseCountQuery='';
            List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
            String selectedRows = UtilityController.getAllSelectedFields(cols);
            System.debug('selectedRows :::::: ' + selectedRows);
            String whereClause;
            String orderBy ;
            if (whereClauseJson != null){
                whereClause = relationField+'=:objrecordId AND '+ whereClauseJson ;
            }
            else{
                whereClause = relationField+'=:objrecordId';
            }
            if (orderByJson != null){
                orderBy = 'ORDER BY '+orderByJson;
            }
            else
            {
                orderBy='';
            }
            String query = 'select ' + selectedRows + ' from  ' + objectName + ' WHERE ' + whereClause +' '+orderBy ;
            query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
            totalCaseCountQuery = 'select count() from '+objectName+' where '+ whereClause;
            
            System.debug('query :::::: ' + query);
            List<sObject> objList = new List<sObject> ();
            if (UtilityController.isFieldAccessible(objectName, selectedRows))
            {
                objList = Database.query(query);
                System.debug('objList :' +objList);
            }
            EsmHelper.dataHelper dh = UtilityController.getWrapperFromJson(objList, objectName, cols);
            System.debug('data helper :');
            System.debug(dh);
            

            rlist.total =  (database.countQuery(totalCaseCountQuery)); // total records passed;
            rlist.pageSize = recordsToDisplay;
            rlist.page = pageNumber;
            rlist.cases = dh;

        } catch(Exception ex) {
            rlist.serverError = UtilityController.customDebug(ex, 'RelatedListComponentController');
        }
        System.debug('rlist:'+rlist);
        return rlist;
    }

      public class RelatedListWrapper {
        @AuraEnabled public Integer pageSize { get; set; }
        @AuraEnabled public Integer page { get; set; }
        @AuraEnabled public Integer total { get; set; }
        @AuraEnabled public EsmHelper.dataHelper cases { get; set;}
        @AuraEnabled public String serverError {get;set;}
    }

}