public class SyscoOnChangeManager implements OnChangeConfig {
	public String prefix = UtilityController.esmNameSpace;

	public boolean execute(SObject childSobject, List < Invoice_Line_Item__c > invLineItemList, List < PO_Line_Item__c > pOLineItemList, List < GRN_Line_Item__c > grnLineItemList, String field, String fieldValue, String childNameSpace, Map < object, object > extraParamMap) {
		system.debug('**field** ' + field);
		system.debug('***fieldValue** ' + fieldValue);
		system.debug('########## inside @SyscoOnChangeManager  ############');
		try {
			Invoice__c invoice = (Invoice__c) childSobject;
			Set < Id > setVendorId = new set < Id > ();
			setVendorId.add(invoice.Vendor__c);
			// Change added for Workday by - Vaibhav - *****start***
			Set < Id > setWDVendorId = new set < Id > ();
			setWDVendorId.add(invoice.Vendor_WD__c);
			// Change added for Workday by - Vaibhav - *****ends***
			List < Invoice__c > lstInvoices = new List < Invoice__c > ();
			String orderId = '';
			if (!String.isBlank(invoice.Purchase_Order__c)) {
				orderId = invoice.Purchase_Order__c;
			}
			Id vendorId = invoice.Vendor__c;
            
            
			lstInvoices.add(invoice);
			// Change added for Workday by - Vaibhav - *****start***
			List < Account > vendorMaster = new List < Account > ();
			if (invoice.ERP_Type__c != 'Workday') {
				vendorMaster = SyscoUtil.populateVendorMaster(setVendorId);
			} else {
				vendorMaster = SyscoUtil.populateVendorMaster(setWDVendorId);
			}
			system.debug('vendorMaster:'+vendorMaster);
			// Change added for Workday by - Vaibhav - *****ends***
			Purchase_Order__c pOrder = new Purchase_Order__c();
			if (!String.isBlank(orderId)) {
				pOrder = [SELECT Id, Vendor__c, Vendor__r.VENDOR_TYPE__c, PO_Type__c FROM Purchase_Order__c WHERE Id =: orderId LIMIT 1];
			}

			SyscoCORAIndexingHelper helper = new SyscoCORAIndexingHelper(lstInvoices);
			Invoice__c result;
			/*
			// **********************************\\Changes added by Vaibhav to add custom validation hard stops - start//****************************
			set<String> setCenterNames = new set<String>();
			set<Id> setCenterId = new set<Id>();
			set<Id> setcategory = new set<Id>();
			Boolean isUnMatched = false;
			map<Id, String> mapCategoryRef  = new map<Id, String>();
			map<String, List<CostCenterHierarchy__c>> mapCentersWithHrrchy = new map<String, List<CostCenterHierarchy__c>>();
			List<String> lstUserHrrchy = new List<String>();
			map<String, List<String>> mapCategoryWithHrrchy = new map<String, List<String>>();
			for(Invoice_Line_Item__c line : invLineItemList){
				setCenterId.add(line.Cost_Code__c);
				setcategory.add(line.Spend_Category__c);
			}
			for(Spend_Categories__c cat : [SELECT Id, Reference_ID__c, Spend_Categories_Name__c, Inactive__c, Name FROM Spend_Categories__c WHERE Id IN: setcategory]){
				mapCategoryRef.put(cat.Id, cat.Reference_ID__c);
			}
			if(!invLineItemList.isEmpty() && setCenterId != null && setcategory != null && invoice.ERP_Type__c == 'Workday'){
				for(WD_CustomValidations__c std :  [SELECT SC_Ref_Id__c, CCH_Ref_Id__c, Name, Id FROM WD_CustomValidations__c]){
					if(!mapCategoryWithHrrchy.containsKey(std.SC_Ref_Id__c)){
						mapCategoryWithHrrchy.put(std.SC_Ref_Id__c , new List<String>() );
					}
					mapCategoryWithHrrchy.get(std.SC_Ref_Id__c).add(std.CCH_Ref_Id__c);
				}
				for(Cost_Center__c center : [SELECT Description__c, OPCO_Code__c, Cost_Center_Name__c, Inactive__c, Reference_ID__c, Input_Source__c, Company_Res_WID__c, Company_Res_OReference_id__c, Company_Res_CReference_id__c, Unique_Key__c, Name, Id FROM Cost_Center__c WHERE Input_Source__c = 'Workday' AND Id IN : setCenterId]){
					setCenterNames.add(center.Cost_Center_Name__c);
				}
				for(CostCenterHierarchy__c hrchy : [SELECT CostCenter_Name__c, CostCenter_RefId__c, Hierarchy_Name__c, Hierarchy_RefID__c, Inactive__c, InputSource__c, Unique_Key__c, Name, Id FROM CostCenterHierarchy__c WHERE InputSource__c = 'Workday' AND CostCenter_Name__c IN: setCenterNames]){
					if(!String.isBlank(hrchy.CostCenter_RefId__c) && !mapCentersWithHrrchy.containsKey(hrchy.CostCenter_RefId__c)){
						mapCentersWithHrrchy.put(hrchy.CostCenter_RefId__c, new List<CostCenterHierarchy__c>());
					}
					mapCentersWithHrrchy.get(hrchy.CostCenter_RefId__c).add(hrchy);
					lstUserHrrchy.add(hrchy.Hierarchy_RefID__c);
				}
				for(String cat : mapCategoryRef.values()){
					System.debug('My chosen Category==> ' + cat);
					// for checking categories and heirarchy in Static table
					if(mapCategoryWithHrrchy.containsKey(cat) && mapCategoryWithHrrchy.get(cat) != null){
						System.debug('About to lost, but got found somehow!!!');
						for(String userHrrchy : lstUserHrrchy){
							for(String hrrchy : mapCategoryWithHrrchy.get(cat)){
								if(userHrrchy == hrrchy){
									System.debug('Yeaaahh! I got Matched');
									break;
								} else {
									System.debug('Urghhh! I got Lost');
									isUnMatched = true;
								}
					
							}
						}
					}
				}
				if(isUnMatched){
					extraParamMap.put('alertmessage', 'Spend category is not matching with standards');
				}
			}
			*/
			// **********************************\\Changes added by Vaibhav to add custom validation hard stops - Ends//****************************
			
			// Change added for Workday by - Vaibhav - *****start***
			if (field == 'Vendor__c' || field == 'Vendor_WD__c') {
				result = helper.invoiceIndexing(lstInvoices, false) != null ? helper.invoiceIndexing(lstInvoices, false)[0] : new Invoice__c();
				System.debug('@@ On Change Vendor @@' + result);
				helper.assignVendorAddress(invoice, String.valueOf(invoice.OPCO__c) + String.valueOf(invoice.Vendor_No__c));
				System.debug('@@ On Change Vendor result.Payment_Terms_Inv__c' + result.Payment_Terms_Inv__c);
				invoice.Term_Start_Date__c = result.Term_Start_Date__c;
				invoice.Payment_Terms_Inv__c = result.Payment_Terms_Inv__c;
				List < Payment_Term__c > ptList = new List < Payment_Term__c >();
				ptList = [SELECT Id, Name FROM Payment_Term__c WHERE Unique_Key__c =: result.Payment_Terms_Inv__c];
                System.debug('@@ On Change Vendor @@ptList ' + ptList);             
				if (ptList.size() > 0) {
					Payment_Term__c pt = ptList[0];
					invoice.Payment_Term__r = pt;
					invoice.Payment_Term__c = pt.Id;
				} else {
					invoice.Payment_Term__r = null;
					invoice.Payment_Term__c = null;
				}
                System.debug('@@ On Change Vendor @@invoice.Payment_Term__c ' + invoice.Payment_Term__c);                        
				invoice.Payment_Due_Date__c = result.Payment_Due_Date__c;
				invoice.Priority__c = result.Priority__c;
				invoice.Invoice_Category__c = result.Invoice_Category__c;
				invoice.Invoice_Type__c = result.Invoice_Type__c;
				invoice.Discount_Applicable__c = result.Discount_Applicable__c;
				invoice.Requestor_Email_Id__c = result.Requestor_Email_Id__c;
				invoice.Purchase_Order_Number__c = result.Purchase_Order_Number__c;
				invoice.Vendor_No__c = result.Vendor_No__c;
				invoice.Connection_Name__c = result.Connection_Name__c;
				invoice.Invoice_Control_Amount__c = result.Invoice_Control_Amount__c; // NOT FEASIBLE IN PRODUCTION ENV //
				// Change added for Workday by - Vaibhav - *****ends***
			}
			System.debug('@@ On Change Vendor @@ invoice ' + invoice);
			if (field == 'Purchase_Order__c') {

				if (invoice.WorkType__c != null && invoice.Invoice_Origin__c != 'EDI') {
					helper.assignPriorities(invoice, invoice.WorkType__c, invoice.Document_Type__c);
					result = helper.invoiceIndexing(lstInvoices, false) != null ? helper.invoiceIndexing(lstInvoices, false)[0] : new Invoice__c();
					invoice.Term_Start_Date__c = result.Term_Start_Date__c;
					invoice.Payment_Terms_Inv__c = result.Payment_Terms_Inv__c;
					List < Payment_Term__c > ptList = [SELECT Id, Name FROM Payment_Term__c WHERE Unique_Key__c =: result.Payment_Terms_Inv__c];
					if (ptList.size() > 0) {
						Payment_Term__c pt = ptList[0];
						invoice.Payment_Term__r = pt;
						invoice.Payment_Term__c = pt.Id;
					} else {
						invoice.Payment_Term__r = null;
						invoice.Payment_Term__c = null;
					}
					invoice.Payment_Due_Date__c = result.Payment_Due_Date__c;
					invoice.Priority__c = result.Priority__c;
				    invoice.Discount_Applicable__c = result.Discount_Applicable__c;
					invoice.Requestor_Email_Id__c = result.Requestor_Email_Id__c;
					invoice.Purchase_Order_Number__c = result.Purchase_Order_Number__c;
					invoice.Invoice_Control_Amount__c = result.Invoice_Control_Amount__c; // NOT FEASIBLE IN PRODUCTION ENV //

				}
				if (pOrder != null && vendorMaster != null && pOrder.PO_Type__c != null && pOrder.Vendor__r.VENDOR_TYPE__c != null && invoice.Invoice_Origin__c == 'EDI') {

					helper.processEDIInvoices(invoice, invoice.WorkType__c, pOrder.Vendor__r.VENDOR_TYPE__c, pOrder.PO_Type__c);
					result = helper.invoiceIndexing(lstInvoices, false) != null ? helper.invoiceIndexing(lstInvoices, false)[0] : new Invoice__c();
					invoice.Term_Start_Date__c = result.Term_Start_Date__c;
					invoice.Payment_Terms_Inv__c = result.Payment_Terms_Inv__c;
					List < Payment_Term__c > ptList = [SELECT Id, Name FROM Payment_Term__c WHERE Unique_Key__c =: result.Payment_Terms_Inv__c];
					if (ptList.size() > 0) {
						Payment_Term__c pt = ptList[0];
						invoice.Payment_Term__r = pt;
						invoice.Payment_Term__c = pt.Id;
					} else {
						invoice.Payment_Term__r = null;
						invoice.Payment_Term__c = null;
					}
					invoice.Payment_Due_Date__c = result.Payment_Due_Date__c;
					invoice.Priority__c = result.Priority__c;
					invoice.Discount_Applicable__c = result.Discount_Applicable__c;
					invoice.Requestor_Email_Id__c = result.Requestor_Email_Id__c;
					invoice.Purchase_Order_Number__c = result.Purchase_Order_Number__c;
					invoice.Invoice_Control_Amount__c = result.Invoice_Control_Amount__c; // NOT FEASIBLE IN PRODUCTION ENV //   
				}
			}
			// added by Kuldeep  - OCR Fail- To set Invoice type - starts
			if(field == 'WorkType__c'){
                String uid = UserInfo.getUserId();
                    List < user > userlist = new List < user > ();
                  userlist = [select id, name, contactId, contact.Name, account.Name, account.OPCO_Code__c from user where id =: uid];
                  System.debug('userlist----INVclass--' + userlist);
                  System.debug('userlist--acc name----' + userlist[0].account.Name);
                    invoice.Vendor__c = userlist[0].accountId;
                    invoice.Vendor_Name__c = userlist[0].account.Name;
                    invoice.OPCO_Code__c = userlist[0].account.OPCO_Code__c;
	            if(invoice.WorkType__c != null && invoice.Invoice_Origin__c != 'EDI'){
	                helper.assignPriorities(invoice, invoice.WorkType__c, invoice.Document_Type__c);
	            }
	            // If EDI is reclassified as NON PO, Special Case
	            if(invoice.WorkType__c == 'Non PO Invoice' && invoice.Invoice_Origin__c == 'EDI'){
	                helper.assignPriorities(invoice, invoice.WorkType__c, invoice.Document_Type__c);
	            }
	        }
           // added by shital Rathod save button Issue --- vendor & Opco code Auto population------ start 
            if(field == 'Supplier_Work_Type__c'){
	            if(invoice.Supplier_Work_Type__c != null && invoice.Supplier_Work_Type__c != ''){
	                String uid = UserInfo.getUserId();
                    List < user > userlist = new List < user > ();
                  userlist = [select id, name, contactId, contact.Name, account.Name, account.OPCO_Code__c from user where id =: uid];
                  System.debug('userlist----INVclass--' + userlist);
                  System.debug('userlist--acc name----' + userlist[0].account.Name);
                    invoice.Vendor__c = userlist[0].accountId;
                    invoice.Vendor_Name__c = userlist[0].account.Name;
                    invoice.OPCO_Code__c = userlist[0].account.OPCO_Code__c;
                 
	            }
	        }
            // added by shital Rathod save button Issue --- vendor & Opco code ---- end
            
            
	        if(field == 'OPCO_Code__c ') {
	            List<OPCO_Code__c> opcoList = [SELECT OPCO_Code__c FROM OPCO_Code__c WHERE id =: invoice.OPCO_Code__c LIMIT 1];
	            if(opcoList.size() > 0){
	                invoice.OPCO__c = String.valueOf(opcoList[0].OPCO_Code__c);
	            }
	        }
        	// added by Kuldeep  - OCR Fail- To set Invoice type - Ends
			// NOT FEASIBLE IN PRODUCTION ENV //
			if (field == 'Amount__c' || field == 'Other_Charges_Surcharges__c' || field == 'Freight_Charges__c' || field == 'Pickup_Allowance__c' || field == 'Samples_Charges__c' || field == 'Tax__c') {
				invoice.Invoice_Control_Amount__c = helper.calculateInvoiceControlAmount(invoice);
			}

			if (invoice.Comments__c == null || invoice.Comments__c == '') {
				invoice.Comments__c = '';
			}
			system.debug('########## inside @SyscoOnChangeManager  ############');
			return true;
		} catch (Exception e) {
			system.debug('@@ Exception in On Change Manager @@' + e);
			return true;
		}
	}

}