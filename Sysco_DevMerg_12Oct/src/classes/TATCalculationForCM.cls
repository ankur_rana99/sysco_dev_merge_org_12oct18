public class TATCalculationForCM {
  @InvocableMethod(label = 'Statewise TAT Calculation For CM' description = 'Statewise TAT Calculation For CM')
  public static void calculateStatewiseTAT(List<CaseManager__c> Cases) {
    TATCalculation.calculateStateChange(Cases, 'CaseManager__c');
  }
}