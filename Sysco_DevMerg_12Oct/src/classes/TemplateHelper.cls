public class TemplateHelper { 
    /*
      Wrapper class handle Merge Template response.
     */
    public class EmailToCaseTemplateResponse {
        public string body {
            get; set;
        }
        public string subject {
            get; set;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method take templateId and case object id as parameter and returns subject and body with merged values.
      Dependencies: Use in "SendMail" method.
     */
    public static EmailToCaseTemplateResponse mergeTemplate(string templateId, string caseTrackerId) {
        EmailToCaseTemplateResponse response = new EmailToCaseTemplateResponse();

        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, null, caseTrackerId);

        response.body = email.getHTMLBody();
        response.subject = email.getSubject();

        return response;
    }
}