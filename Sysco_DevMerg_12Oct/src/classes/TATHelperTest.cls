@isTest
public class TATHelperTest{

    public static testMethod void testFirst(){
       TATHelper tt=new TATHelper();
        TATHelper.Option wrapOption1 = new TATHelper.Option();
      	TATHelper.Option wrapOption = new TATHelper.Option('aa','bb');
        wrapOption.value = 'sa';
        wrapOption.text = 'aa';

        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = 'User_Action__c';
        wrapCriteria.objName = 'Invoice__c';
        wrapCriteria.operator = '==';
        wrapCriteria.type = 'String';
        wrapCriteria.value = 'Reject';
        
        TATHelper.JSONValue wrapJson = new TATHelper.JSONValue();
        wrapJson.additionalEmails = 's';
        wrapJson.sendNotification = false;
        wrapJson.template = 's';
        wrapJson.notificationTimeUnit = 'hh';
        wrapJson.notificationTime = 3;
        wrapJson.calendar='10-9-17';
        wrapJson.tatTimeUnit = '10';
        wrapJson.tatTime = 8;
        
        TATHelper.TATRule tatRule = new TATHelper.TATRule();
        tatRule.ruleName = 'TestRule';
        tatRule.objectName = 'Invoice__c';
        tatRule.JSONValue = null;
        tatRule.order = 1;
        tatRule.isActive = false;
        tatRule.id = '';
        
        TATHelper.FieldInfo wrapField = new TATHelper.FieldInfo();
        wrapField.label = 'FirstName';
        wrapField.apiName = 'Invoice__c';
        wrapField.type = 'String';
        //wrapField.pickListValue = 'd';
        //wrapField.operator = '==';
    }
}