global class AutoClosureNotificationHelper {
    
    public static void sendCaseClosureNotification(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<CaseManager__c> notficationList = new List<CaseManager__c> ();
        
        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.Current_State__c != caseTrackerOld.Current_State__c && caseTrackerNew.Current_State__c == 'Pending For Archival') {
                notficationList.add(caseTrackerNew);
            }
        }
        
        if (notficationList.size() > 0) {
            sendNotification(notficationList, 'Closure');  
        }
    }
    @InvocableMethod(label = 'Send Auto Closure Notification' description = 'Send Auto Closure Notification')
    global static void sendCaseAutoClosureNotification(List<CaseManager__c> caseManagers) {
        if (caseManagers.size() > 0) {
            sendNotification(caseManagers, 'System Closure Email(Auto Closure)');
        }
    }
    public static void sendNotification(List<CaseManager__c> caseManagers, string type) {
        
        List<Messaging.SingleEmailMessage> mailMessageList = new List<Messaging.SingleEmailMessage> ();
        EmailTemplate templateId = [Select id,Name from EmailTemplate where name =:type Limit 1];
        List<OrgWideEmailAddress> owa = [select id, Address,DisplayName FROM OrgWideEmailAddress];
        Map<String,String> mailboxEmailMap = new Map<String,String>();
        for(OrgWideEmailAddress orgWide : owa){
            mailboxEmailMap.put(orgWide.DisplayName,orgWide.Id);
        }
        Set<Id> caseIds = new Set<Id>();
         for (CaseManager__c caseManager : caseManagers) {
             caseIds.add(caseManager.Id);
         }
        
        List<CaseManager__c> casesList = new List<CaseManager__c>();
        /* Changes for CAS-436 | Ashish Kr. | 24-07-2018 starts */
        casesList = [Select Id,Name, (Select Id, Subject, fromAddress, createdDate, ToAddress, CcAddress, HtmlBody, TextBody from Emails Where Incoming = false Order By CreatedDate DESC Limit 1) FROM CaseManager__c where Id in:caseIds];
        /* Changes for CAS-436 | Ashish Kr. | 24-07-2018 ends */
        system.debug(casesList);

        Map<Id,List<EmailMessage>> emailMessageCaseMap = new Map<Id,List<EmailMessage>>();
        for(CaseManager__c cas : casesList){
             emailMessageCaseMap.put(cas.Id, cas.Emails);
        }
        

        
        for (CaseManager__c caseManager : caseManagers) {
            AutoClosureNotificationHelper.Notification notificationObj = new AutoClosureNotificationHelper.Notification();
            //EmailToCaseNotificationManager notificationManager = new EmailToCaseNotificationManager();
            if(templateId != null){
                notificationObj.emailTemplate = templateId.id;
            }
            notificationObj.toAddressField = null;
            notificationObj.ccAddressField = 'None';
            notificationObj.bccAddressField = 'None';
            notificationObj.additionalRecipient = null;
            if(owa != null){
                notificationObj.orgWideId = mailboxEmailMap.get(caseManager.Mailbox_Name__c);
                
            }
            notificationObj.mailTrailIn = 'In Mail';
            notificationObj.includeIn = 'TO';
            
            
            Messaging.SingleEmailMessage mailMsg = createMailMessage(caseManager, notificationObj,emailMessageCaseMap.get(caseManager.id));
            mailMessageList.add(mailMsg);
            
            /*
            ReminderAndEscalationHelper.ReminderRule rule = ReminderAndEscalationHelper.findRule(caseManager, remRuleConfiges);
            if (rule != null && rule.JSONValue.notifications != null && rule.JSONValue.notifications.size() > 0) {
            ReminderAndEscalationHelper.Notification notificationObj = rule.JSONValue.notifications[0];
            Messaging.SingleEmailMessage mailMsg = createMailMessage(caseManager, notificationObj);
            mailMessageList.add(mailMsg);
            }
            */
            
        }
        if (mailMessageList.size() > 0) {
            try{
                Messaging.sendEmail(mailMessageList);
            }
            catch(Exception ex){
                
            }
        }
    }
    
    private static Messaging.SingleEmailMessage createMailMessage(CaseManager__c caseManager, AutoClosureNotificationHelper.Notification notificationObj, List<EmailMessage> emailMsgList) {
        Messaging.SingleEmailMessage mailMsg = new Messaging.SingleEmailMessage();
        TemplateHelper.EmailToCaseTemplateResponse emailMergeResponse = TemplateHelper.mergeTemplate(notificationObj.emailTemplate, caseManager.id);
        List<string> toAddresses = new List<string> ();
        List<string> ccAddresses = new List<string> ();
        List<string> bccAddresses = new List<string> ();
        
        string mailBody = emailMergeResponse.body;
        
        if (notificationObj.mailTrailIn != null && notificationObj.mailTrailIn != 'None') {
            string body = '';
            //List<EmailMessage> emailMsgList = [Select Id, Subject, fromAddress, createdDate, ToAddress, CcAddress, HtmlBody, TextBody from EmailMessage Where RelatedToId = :caseManager.id and Incoming = false Order By CreatedDate Limit 1];
            if (emailMsgList.size() > 0) {
                body = getBodyToAddAsAttachment(emailMsgList[0]);
                
                if (notificationObj.mailTrailIn == 'Attachment') {
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName('Mail.html');
                    efa.setBody(Blob.valueOf(body));
                    mailMsg.setFileAttachments(new List<Messaging.EmailFileAttachment> { efa });
                }
                else if (notificationObj.mailTrailIn == 'In Mail') {
                    mailBody = mailBody + '<br/><hr/><br/>' + body;
                }
                
                // mark toaddress of last outbound email in CC address
                if (!String.isEmpty(emailMsgList[0].ToAddress)) {
		            string fieldValue = String.valueOf(emailMsgList[0].ToAddress);
		            if(!String.isEmpty(fieldValue)){
		                ccAddresses.addAll(fieldValue.split(';'));
		            }
		        }
		        
            }
        }
        
        
        mailMsg.setHtmlBody(mailBody);
        mailMsg.setSubject(emailMergeResponse.Subject);
        mailMsg.setWhatId(caseManager.Id);
        
        
        //toAddresses.add('Ashish.Kumar1e693d@genpact.com');
        
        if(caseManager.get('Requestor_Email_Id__c') !=null){
        	toAddresses.add(String.valueOf(caseManager.get('Requestor_Email_Id__c')));
        }
        
        
        if (!String.isEmpty(notificationObj.toAddressField)) {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.toAddressField));
            if(!String.isEmpty(fieldValue)){
                toAddresses.addAll(fieldValue.split(';'));
            }
        }
        
        if (!String.isEmpty(notificationObj.ccAddressField) && notificationObj.ccAddressField!='None') {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.ccAddressField));
            if(!String.isEmpty(fieldValue)){
                ccAddresses.addAll(fieldValue.split(';'));
            }
        }
        if (!String.isEmpty(notificationObj.bccAddressField) && notificationObj.bccAddressField!='None') {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.bccAddressField));
            if(!String.isEmpty(fieldValue)){
                bccAddresses.addAll(fieldValue.split(';'));
            }
        }
        if (!string.isEmpty(notificationObj.additionalRecipient)) {
            List<string> additionalList = notificationObj.additionalRecipient.split(';');
            if (!string.isEmpty(notificationObj.includeIn)) {
                if (notificationObj.includeIn == 'TO') {
                    toAddresses.addAll(additionalList);
                }
                else if (notificationObj.includeIn == 'CC') {
                    ccAddresses.addAll(additionalList);
                }
                else {
                    bccAddresses.addAll(additionalList);
                }
            }
            else {
                toAddresses.addAll(additionalList);
            }
        }
        if (!String.isEmpty(notificationObj.orgWideId)) {
            mailMsg.setOrgWideEmailAddressId(notificationObj.orgWideId);
        }
        
        
        
        mailMsg.setToAddresses(toAddresses);
        mailMsg.setCCAddresses(ccAddresses);
        mailMsg.setBCCAddresses(bccAddresses);
        
        return mailMsg;
    }
    
    private static string getBodyToAddAsAttachment(EmailMessage emailMsg) {
        String FORM_HTML_START = '<HTML><head><title></title><meta charset="UTF-8"></head><BODY> <div style="font-family: Arial Unicode MS">';
        String FORM_HTML_END = '</div></BODY></HTML>';
        String emailBody = '';
        emailBody = emailBody + '' + FORM_HTML_START;
        emailBody = emailBody + 'From:' + emailMsg.fromAddress + '<br>';
        emailBody = emailBody + 'Received Date:' + emailMsg.createdDate + '<br>';
        emailBody = emailBody + 'To:' + emailMsg.ToAddress + '<br>';
        if (emailMsg.CcAddress != null) {
            emailBody = emailBody + 'Cc:' + emailMsg.CcAddress + '<br>';
        }
        emailBody = emailBody + 'subject:' + emailMsg.subject + '<br><br><br><br>';
        
        if (emailMsg.htmlBody != null) {
            emailBody = emailBody + emailMsg.HtmlBody;
        } else {
            emailBody = emailBody + emailMsg.TextBody;
        }
        emailBody = emailBody + FORM_HTML_END;
        
        return emailBody;
    }
    
    public class Notification {
        public string emailTemplate { get; set; }
        public string after { get; set; }
        public string additionalRecipient { get; set; }
        public string afterUnit { get; set; }
        public string includeIn { get; set; }
        public string toAddressField { get; set; }
        public string ccAddressField { get; set; }
        public string bccAddressField { get; set; }
        public string mailTrailIn { get; set; }
        public string orgWideId { get; set; }
        /*public boolean sendOriginalEmailAsAttachment{get; set; }*/
    }
}