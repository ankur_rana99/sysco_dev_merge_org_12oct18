public with sharing class ValidationHelper {
    /*
Authors: Somnath Patil
Date created: 22/11/2017
Purpose: This class contains all wrapper classes and common methods that use in QC System.
Dependencies: QCCalculation,QCRuleController
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification:
                Method/Code segment modified:
                
*/ 

    
    /*
      Authors: Somnath Patil
      Purpose: Wrapper class to store option like value,text.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }
    /*
      Authors: Somnath Patil
      Purpose: This wrapper class is used to store Field information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public list<Option> pickListValue {
            get; set;
        }
        public List<string> operator{
            get; set;
        }
    }
    /*
      Authors: Somnath Patil
      Purpose: This wrapper class is used to store Criteria information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string refField{get;set;}
        
    }
    public class Attribute {
        public string additionalRuleField{get;set;}
        public string additionalRuleValue{get;set;}
    } 
    /*
      Authors: Somnath Patil
      Purpose: This wrapper class is used to store JSONValue information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class JSONValue {
        public List<Criteria> criterias { get; set; }
        public List<Attribute> attributes { get; set; }
    }

    /*
      Authors: Somnath Patil
      Purpose: This wrapper class is used to store QCRule information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class ValidationRule {
        
        public JSONValue JSONValue { get; set; }
         public string additionRuleName { get; set; }
         public JSONValue additionRules { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        public boolean isActive{get;set;}
        public boolean isCustom{get;set;}
         public string type { get; set; }
        public boolean isStopExecution{get;set;}
        public string errorType{get;set;}
        public string errorCode{get;set;}
        public String objectName{get;set;}
    }

    public class QCFormResponse{
        @AuraEnabled
        public string qcFormFieldsetName{get;set;}
        @AuraEnabled
        public string qcFormHeader{get;set;}
    }

    /*
      Authors: Somnath Patil
      Purpose: This wrapper class is used to store QC Form Field information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    /*public class QCFormFieldInfo {
        public string apiName {
            get; set;
        }
        public boolean isRequired{
            get;set;
        }
        public string label{
            get;set;
        }
    }*/

}