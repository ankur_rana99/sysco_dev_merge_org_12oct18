public class MainPageController {

    @AuraEnabled
    public static Map<String, Object> getUserInfo() {
        Map<String, Object> userDetails = new Map<String, Object> ();
        try {
            userDetails.put('UserName', userinfo.getName());
            userDetails.put('InstanceName', [SELECT Id, InstanceName FROM Organization].InstanceName);
        } catch(Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return userDetails;
    }
    /*  Added by Ashish Kr. | CASP2-17 | 25 June 2018  start */
    @AuraEnabled
    public static String getUserProfile(){
        String loggedInUserProfile = '';
        try{
            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            loggedInUserProfile = PROFILE[0].Name;  
        }catch(Exception e){
            System.debug(e);
        }
        System.debug('===loggedInUserProfile'+loggedInUserProfile);
        return loggedInUserProfile;     
    }
    
    /* Added by Ashish Kr. | CASP2-17 | 25 June 2018  end */
    @AuraEnabled
    public static EsmHelper.DataMetadataWrapper getSObjectDetails(String layout, String recId, String sObjectName) {
        EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
        wrapper.isAccesible = true;
        
        //Check supplier profile user
        System_Configuration__c sysConf = [SELECT Value__c FROM System_Configuration__c where Module__c = 'Profile' and Sub_Module__c = 'Supplier_Profile_ID'];
        Boolean isSupplierUser = false;
        List<Supplier_Profile__c> suppList = new List<Supplier_Profile__c> ();
        if (sysConf.Value__c != null && sysConf.Value__c != '' && UserInfo.getProfileId().contains(sysConf.Value__c)) {
            isSupplierUser = true;
        }
        sObject dataModal;
        JSONGenerator finalJson = JSON.createGenerator(true);
        finalJson.writeStartObject();
        //try {
        List<Layout_Configuration__c> lst = new List<Layout_Configuration__c> ();
        if (isSupplierUser) {
            lst = [SELECT Config_Json__c FROM Layout_Configuration__c where Is_Active__c = true and Object_Name__c = :sObjectName and System__c = 'Supplier Portal' and Page_Layout__c = :layout];
        }
        else
        {
            lst = [SELECT Config_Json__c FROM Layout_Configuration__c where Is_Active__c = true and Object_Name__c = :sObjectName and System__c = 'Processor Portal' and Page_Layout__c = :layout];
        }

        finalJson.writeObjectField('layoutJson', lst);
        if (layout == 'EDIT' || layout == 'VIEW')
        {
            dataModal = Database.query(CommonFunctionController.getCreatableFieldsSOQL(sObjectName, 'Id = :recId', '', true));
            system.debug('dataModal=ID====='+dataModal.ID);
          // if(isSupplierUser){ 
            if (layout == 'EDIT') {
               try
                {
                   if(isSupplierUser){
                            system.debug('ownerid======'+dataModal.get('OwnerId')); 
                            system.debug('UserInfo.getUserId()======'+UserInfo.getUserId()); 
                                // OwnerId == UserInfo.getUserId()
                       if(dataModal.get('OwnerId') == UserInfo.getUserId()){
                                dataModal.put('OwnerId',UserInfo.getUserId());
                                system.debug('dataModal=OwnerId====='+dataModal.get('OwnerId'));
                                update dataModal;
                                // update
                                dataModal = Database.query(CommonFunctionController.getCreatableFieldsSOQL(sObjectName, 'Id = :recId', '', true));
            
                                List<Invoice_Line_Item__c> invList = new List<Invoice_Line_Item__c>();
                                invList.addAll(CommonFunctionController.fillOtherChargesList(UtilityController.esmNameSpace,dataModal.ID));
                                invList.addAll(CommonFunctionController.fillInvLineItem(UtilityController.esmNameSpace,dataModal.ID));
                                for(Invoice_Line_Item__c invl :invList){
                                    invl.put('OwnerId',UserInfo.getUserId());
                                }
                                upsert invList;
            
                                List<sobject> InvoicePODetail=CommonFunctionController.getInvoicePODetail(UtilityController.esmNameSpace,dataModal.ID);
                                for(sobject invPO :InvoicePODetail){
                                    invPO.put('OwnerId',UserInfo.getUserId());
                                }
                                upsert InvoicePODetail;
                                //---Jira # ESMPROD-1261 --| start--- 
                                EsmHelper.DataMetadataWrapper wppCc = callOnLoadCustomClasscallOnChangeManager(dataModal, invList, new List<Purchase_Order__c> (), 'On Load Custom Class', 'Invoice__c');
                                dataModal = wppCc.currentObject;
                                System.debug('dataModal-----' + dataModal);
                                //---Jira # ESMPROD-1261 --| end--- 
                                wrapper.isAccesible = true;
                                // make it false
                       }else{
                           wrapper.isAccesible = false;
                       }
                   
                   }/*else{
                       wrapper.isAccesible = false;
                   }*/
                }catch(Exception e)
                {
                    System.debug(ExceptionHandlerUtility.customDebug(e,'MainPageController'));
                    wrapper.error = ExceptionHandlerUtility.customDebug(e,'MainPageController');
                    wrapper.isAccesible = false;
                }
            }
         // }
        }
        else
        { //---Jira # ESMPROD-1261 --| start--- 
        EsmHelper.DataMetadataWrapper wppCc = callOnLoadCustomClasscallOnChangeManager(dataModal, new List<Invoice_Line_Item__c> (), new List<Purchase_Order__c> (), 'On Load Custom Class', sObjectName);
        dataModal = wppCc.currentObject;
         //---Jira # ESMPROD-1261 --| end---
        }
        /*}
       catch(Exception ex) {
       ExceptionHandlerUtility.customDebug(ex,'MainPageController');
       }*/

    //finalJson.writeObjectField('dataModal', dataModalList);
    finalJson.writeObjectField('sObjectName', sObjectName);
    finalJson.writeObjectField('sObjectMetadata', UtilityController.getObjectMetadata(sObjectName));
    finalJson.writeObjectField('sObjectFieldMetadata', UtilityController.getFieldsMetadata(sObjectName));
    wrapper.customLabels = populateLabels();
    wrapper.currentObject = dataModal;
    wrapper.layoutJson = finalJson.getAsString();

    return wrapper;
}
//----Jira # ESMPROD-1261 --| start---
  @AuraEnabled
  public static EsmHelper.DataMetadataWrapper callOnChangeManager(String className, sObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String field, String value) 
  {
   return callOnChangeManager(className, childSobject, invLineItemList, puchaseOrder, field, value, 'Invoice__c');
  }
  //-----Jira # ESMPROD-1261 --| end---
 
//----Jira # ESMPROD-1261 --- Changed Invoice__c childSobject to sObject childSobject and String objName add 'String objName' in function signature
@AuraEnabled
public static EsmHelper.DataMetadataWrapper callOnChangeManager(String className, sObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String field,String value, String objName) {
    try {
        OnChangeConfig obj;
        try {
            if (className != null && !String.valueOf(className).equals('')) {
                obj = (OnChangeConfig) Type.forName(className).newInstance();
            }
        } catch(Exception e) {
            //---Jira # ESMPROD-1261 --| start--- 
            EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
            wrapper.currentObject = childSobject;
            wrapper.error = 'Exception while getting class to run onchnage logic : ' + e.getMessage() + ':' + e.getStackTraceString();
            return wrapper;
            //---Jira # ESMPROD-1261 --| end---
        }
        if (obj != null) {
            String childNameSpace = UtilityController.esmNameSpace;
            Set<String> poId = new Set<String> ();
            for (Purchase_Order__c po : puchaseOrder) {
                poId.add(po.id);
            }
            Map<object, object> extraParamMap = new Map<object, object> ();
            List<PO_Line_Item__c> poLineItemList = CommonFunctionController.getPOline(poId);
            extraParamMap.put('POSet', poId);
            //extraParamMap.put('INVCONF', invConf);
            extraParamMap.put('lstPO', puchaseOrder);
            List<GRN_Line_Item__c> grnLineItemList = CommonFunctionController.getGRNline(poId);
            //boolean t = obj.execute(childSobject, invLineItemList, poLineItemList, grnLineItemList, field, val, childNameSpace, extraParamMap);
            //System.debug('value5424--'+value);
            obj.execute(childSobject, invLineItemList, poLineItemList, grnLineItemList, field,value, childNameSpace, extraParamMap);
            EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
            //---Jira # ESMPROD-1261 --| start--- 
            if (extraParamMap != null && extraParamMap.get('alertmessage') != null)
            {
              wrapper.displayError = String.valueOf(extraParamMap.get('alertmessage'));
            }
             //---Jira # ESMPROD-1261 --| End---
            wrapper.currentObject = childSobject;
            return wrapper;
        }
        return null;
    } catch(Exception ex) {
        EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
        wrapper.error =ExceptionHandlerUtility.customDebug(ex,'callOnChangeManager');
        return wrapper;
    }
}

//--- Validate Invoice data ---//
@AuraEnabled
public static EsmHelper.ExceptionValidationWrapper validateInvoiceData(sObject invoiceObject, List<Invoice_Line_Item__c> invLineItemList, List<sObject> otherChargesList, List<Purchase_Order__c> poList, String childNameSpace) {
    try {
        String jsonValidationExceptionMsg = '';
        invoiceObject.put(childNameSpace + 'Exception_Reason__c', '');

        System_Configuration__c sysConf = [SELECT Value__c FROM System_Configuration__c where Module__c = 'Invoice_Processing' and Sub_Module__c = 'Approval_Required_For_Additional_Charges'];

        if (otherChargesList != null && otherChargesList.size() > 0 && sysConf.Value__c != null && sysConf.Value__c != '' && sysConf.Value__c.equalsIgnoreCase('true')) {
            invoiceObject.put(childNameSpace + 'Approval_Required_For_Additional_Charges__c', true);
        }
        else {
            invoiceObject.put(childNameSpace + 'Approval_Required_For_Additional_Charges__c', false);
        }

        EsmHelper.ExceptionValidationWrapper wrapper = new EsmHelper.ExceptionValidationWrapper();
        Map<String, String> validMap = new Map<String, String> ();
        validMap = InvoiceValidationExceptionController.executeValidationException(invoiceObject, invLineItemList, otherChargesList, poList);
        if (validMap != null && validMap.get('sytemExceptionOccur')==null) {
            //System.debug('sysexcep52----'+validMap);
            String tempExceptionReason = validMap.get('jsonValidationExceptionMsg');
            //System.debug('sysexcep53----'+tempExceptionReason);
            Map<String, Object> mpp =  (Map<String, Object>) JSON.deserializeUntyped(tempExceptionReason);
            //System.debug('sysexcep54----'+mpp.get('Invoice_Exception'));
            invoiceObject.put(childNameSpace + 'Exception_Reason__c', String.valueOf(mpp.get('Invoice_Exception')));
            wrapper.currentObject = invoiceObject;
            wrapper.invLines = invLineItemList;
            wrapper.validationMap = validMap;
            return wrapper;
        }else{
            wrapper.error = validMap.get('sytemExceptionOccur');
            return wrapper;
        }
    }
    catch(Exception ex) {
        ExceptionHandlerUtility.customDebug(ex,'MainPageController');
    }
    return null;
}

//--- Save Invoice and its related data ---//
//---Jira # ESMPROD-1261 --| start--- changed to Map<String, string>  from Map<Boolean, string>
@AuraEnabled
public static Map<string, string> SaveInvoiceData(Invoice__c invoiceObject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> poList, Invoice_Line_Item__c[] otherChargesList,
                                                   String pageMode, String primaryDocJsonAtt, List<ContentVersion> NewAttachmentsAtt, QualityCheck__c qcObject) {
    System.debug('SaveInvoiceData pageMode ---'+  pageMode);
    System.debug('SaveInvoiceData invLineItemList ---'+  invLineItemList);                                                   
    if (pageMode.equalsIgnoreCase('new')) 
    {
        invoiceObject.Id = null;
        System.debug('SaveInvoiceData enter' + NewAttachmentsAtt + '------'+NewAttachmentsAtt.size());
        if (NewAttachmentsAtt == null || NewAttachmentsAtt.size() == 0)
        {
            //---Jira # ESMPROD-1261 --| start---
            Map<string, string> m1 = new Map<string, string>();         
            m1.put('failure',Label.Attachment_Not_Added);
            //---Jira # ESMPROD-1261 --| END---
            return m1; 
        }
    }else{
        System.debug('else invoiceObject.Id ---'+  invoiceObject.Id);
        Id invoiceId = invoiceObject.Id;
        List<ContentDocumentLink> contentVersionList = [SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = :invoiceId];
        Integer attCnt =0;
        System.debug('else contentVersionList ---'+  contentVersionList);
        if (contentVersionList!=null)
        {
            attCnt = contentVersionList.size();
        }
        if (NewAttachmentsAtt != null && NewAttachmentsAtt.size() > 0)
        {
            attCnt = attCnt + NewAttachmentsAtt.size();
        }
        if (attCnt == 0)
        {
            //---Jira # ESMPROD-1261 --| start---
            Map<string, string> m1 = new Map<string, string>();
            m1.put('failure',Label.Attachment_Not_Added);
            //---Jira # ESMPROD-1261 --| End---
            return m1; 
        }
        
    }
    System.debug('----invLineItemList ---'+  invLineItemList);
    System.debug('----NewAttachmentsAtt ---'+  invLineItemList);
    return InvoiceController.saveInvoice(invoiceObject, invLineItemList, poList, otherChargesList, pageMode, primaryDocJsonAtt, NewAttachmentsAtt, qcObject);
}
@AuraEnabled
public static cloneWrapper getInvoiceDetails(String invoiceId) {
    cloneWrapper cw = new cloneWrapper();
    cw.Invoice = Database.query(CommonFunctionController.getSOQLQuery('Invoice__c', 'Id=:invoiceId'));
    cw.attachment = getAttachment(invoiceId);
    return cw;
}

public static List<ContentVersion> getAttachment(String invoiceId) {
    List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink> ();
    List<ContentVersion> contentVersionList = new List<ContentVersion> ();
    List<ContentVersion> contentVersionListToBeSent = new List<ContentVersion> ();
    try {
        if (UtilityController.isFieldAccessible('ContentDocumentLink', null))
        {
            contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = :invoiceId];

        }
        for (Integer i = 0; i < contentDocumentLinks.size(); i++)
        {

            ContentVersion cv = [SELECT Title, PathOnClient, VersionData, Origin, IsMajorVersion, Is_Primary_Doc__c FROM ContentVersion where ContentDocumentId = :contentDocumentLinks[i].ContentDocumentId limit 1];
            contentVersionList.add(cv.clone(false, true));

        }

        insert contentVersionList;
        String contentVersionIds = '';
        for (Integer i = 0; i < contentVersionList.size(); i++) {
            contentVersionIds = contentVersionIds + '\'' + contentVersionList[i].id + '\',';

        }

        contentVersionIds = contentVersionIds.removeEnd(',');
        String query = 'SELECT id, ContentDocumentId, Title, LastModifiedDate ,FileType,ContentSize,FileExtension,Is_Primary_Doc__c FROM ContentVersion where Id in (' + contentVersionIds + ')';
        contentVersionListToBeSent = Database.query(query);
    }
    catch(Exception e) {
        System.debug('msg :' + e.getMessage());
        System.debug('trace :' + e.getStackTraceString());

    }
    /*  lead l = [select id, firstname, lastname, company from lead where id = '00Q3000000aKwVN' limit 1][0];
      lead l2 = l.clone(false, true);
      insert l2;*/
    return contentVersionListToBeSent;

}
public class cloneWrapper {
    @AuraEnabled
    public Invoice__c Invoice { get; set; }
    @AuraEnabled
    public List<ContentVersion> attachment { get; set; }
}
public static Map<String,String> populateLabels(){
    Map<String,String> mp =new Map<String,String>();
    mp.put('Validate_First',Label.Validate_First);
    mp.put('Resolve_Error_Prior_Posting',Label.Resolve_Error_Prior_Posting);
    mp.put('Validated_Successfully',Label.Validated_Successfully);
    mp.put('Invalid_User_Action',Label.Invalid_User_Action);
    mp.put('Fill_All_Required_Detail',Label.Fill_All_Required_Detail);
    return mp;
}
  //---Jira # ESMPROD-1261 --| start--- 
  @AuraEnabled
  public static EsmHelper.DataMetadataWrapper callOnLoadCustomClasscallOnChangeManager(sObject childSobject,
                 List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String actionType, String objectName) {
    EsmHelper.DataMetadataWrapper wp = new EsmHelper.DataMetadataWrapper();
    try
    {
      System.debug('On change Manager method called');
      System.debug('childobj :' + childSobject);
      System.debug('actionType -----' + actionType);
      System.debug('objectName266 -----' + objectName);
      OnChangeConfig obj;
      System_Configuration__c sysConf = Database.Query('SELECT Value__c FROM System_Configuration__c where Module__c = :objectName and Sub_Module__c = :actionType');
      wp = callOnChangeManager(sysConf.Value__c, childSobject, invLineItemList, puchaseOrder, null, null);
      return wp;
    }
    catch(Exception e)
    {
      ExceptionHandlerUtility.customDebug(e, 'callOnLoadCustomClasscallOnChangeManager');
      wp.currentObject = childSobject;
      return wp;
    }
  }
  //---Jira # ESMPROD-1261 --| end---  
}