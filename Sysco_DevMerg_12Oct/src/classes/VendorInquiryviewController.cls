// ----JIRA CASP2-82 ---added by SHital Rathod----Start ---
public without sharing class  VendorInquiryviewController {
// ----JIRA CASP2-82 ---added by SHital Rathod----Start ---    
    @AuraEnabled
   /* public static Map<ID,List<ContentDocumentLink>> getinitialData(String CaseObj) {
      
	    Set<ID> interactionIds = new Set<ID>();
		for(EmailMessage interaction : [SELECT Id FROM EmailMessage WHERE RelatedToId=:CaseObj ORDER BY CreatedDate desc]){
		    interactionIds.add(interaction.ID);
		}
       
      // if(interactionList != Null || interactionList.size() > 0)
        System.debug('in loop getattachementData---'+interactionIds);
        Map<ID,List<ContentDocumentLink>> interactionIdWithAttachMap = new  Map<ID,List<ContentDocumentLink>>();
        for(ContentDocumentLink attachment : [SELECT ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.ParentId,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId IN : interactionIds]){
            if(!interactionIdWithAttachMap.containsKey(attachment.LinkedEntityId)){
                interactionIdWithAttachMap.put(attachment.LinkedEntityId, new List<ContentDocumentLink>());
            }
            else
                interactionIdWithAttachMap.get(attachment.LinkedEntityId).add(attachment);
        }
        System.debug('interactionIdWithAttachMap'+interactionIdWithAttachMap);
      return interactionIdWithAttachMap;
    }*/
	// JIRA | CASP2-118  | Shital Rathod changes Start 
	 public static List<EmailMessage> getinitialData(String CaseObj) {
       
        List<EmailMessage> interactionList = [SELECT Id,Subject, TextBody, HtmlBody, FromName,HasAttachment, FromAddress,CreatedById,CreatedDate FROM EmailMessage WHERE RelatedToId=:CaseObj ORDER BY CreatedDate desc];
        return interactionList;
    }


    
	 @AuraEnabled
	public static List<ContentDocumentLink> getattachementData(String CaseObj, List<String> Interactionid) {
		system.debug('InteractionID====='+Interactionid);
		List<ContentDocumentLink> attachmentList = [SELECT ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.ParentId,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId =: CaseObj];
		system.debug('attachmentList====='+attachmentList);
		return attachmentList;
		/*Set<ID> interactionIds = new Set<ID> ();
		for (EmailMessage interaction :[SELECT Id FROM EmailMessage WHERE RelatedToId = :CaseObj ORDER BY CreatedDate desc]) {
			interactionIds.add(interaction.ID);
		}

		// if(interactionList != Null || interactionList.size() > 0)
		System.debug('in loop getattachementData---' + interactionIds);
		Map<ID, List<ContentDocumentLink>> interactionIdWithAttachMap = new Map<ID, List<ContentDocumentLink>> ();
		for (ContentDocumentLink attachment :[SELECT ContentDocument.Owner.Name, ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c, ContentDocument.Id, ContentDocument.ParentId, ContentDocument.Title, ContentDocument.FileType, ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id, ContentDocument.LastModifiedDate, ContentDocument.FileExtension, ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId  =: CaseObj AND ContentDocument.Id IN :Interactionid ]) {
			if (!interactionIdWithAttachMap.containsKey(attachment.LinkedEntityId)) {
				interactionIdWithAttachMap.put(attachment.LinkedEntityId, new List<ContentDocumentLink> ());
			}
			else
			interactionIdWithAttachMap.get(attachment.LinkedEntityId).add(attachment);
		}
		System.debug('interactionIdWithAttachMap' + interactionIdWithAttachMap);*/
		//return interactionIdWithAttachMap;
	}
	// JIRA | CASP2-118  | Shital Rathod changes End 


	// JIRA | CASP2-118  | Shital Rathod changes End 
  
	

    @AuraEnabled
    public static String getUserid() {
        return UserInfo.getUserId();
    }
	// JIRA | CASP2-172  | Somnath changes start 
    @AuraEnabled
    public static CaseManager__c getCase(String CaseObj) {
        system.debug('---------ctid------'+CaseObj);
        CaseManager__c cm = [SELECT Id,Name,Priority__c,Subject__c,Status__c,Comments__c,Current_State__c FROM CaseManager__c WHERE Id=:CaseObj];
        system.debug('---------cm------'+cm);
        return cm;
    }
	// JIRA | CASP2-172  | Somnath changes End 
    @AuraEnabled
    public static List<String> getPicklistvalues(){
        CaseManager__c objName= new CaseManager__c();
        String field_apiname='Priority__c';
        List<String> optionlist = new List<String>();
 
        Schema.sObjectType sobject_type = objName.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
 
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_apiname).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            optionlist.add(a.getValue());
        }
        return optionlist;
    }
    
    // JIRA | CASP2-118  | Shital Rathod changes -- Signature Change
    @AuraEnabled
    public static EmailMessage[] saveCaseData (CaseManager__c CaseObj,String mailbody,String[] DocId) {
        EmailMessage[] newEmail = new EmailMessage[0];
        try 
        {
            //[Start] Case manager insert data
            //System.debug('CaseObj -- '+CaseObj);
            String startToken = ESMConstants.SUBJECT_START_IDENTIFIER;
            String endToken = ESMConstants.SUBJECT_END_IDENTIFIER;
            String emailSubject = 'Reply : ' + (String)CaseObj.get(Schema.CaseManager__c.Subject__c);
            //String emailBody = mailbody;
        
            upsert CaseObj;
            //[End] Case manager insert data

            //[Start] Email Message insert data
            String ctid =   CaseObj.Id;
           // String Emailid =   CaseObj.Id;
            
            newEmail.add(new EmailMessage(Subject = emailSubject,
                                            TextBody = mailbody,
                                            HtmlBody = mailbody,
                                            FromName = UserInfo.getName(), 
                                            FromAddress = UserInfo.getUserEmail(),
                                            Incoming = true,
                                            RelatedToId = ctid,
                                            status = '0'));
            System.debug('newEmail -- '+newEmail);
            insert newEmail;


			// JIRA | CASP2-118  | Shital Rathod changes strat
			System.debug('newEmail -- '+newEmail[0].RelatedToId);
			System.debug('DocId-- '+DocId);
           //get latest content document link with emailID, clone ,relatedtoID = emailId 
          List<ContentDocumentLink> attachmentListTemp = [SELECT ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.ParentId,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize,ShareType FROM ContentDocumentLink where LinkedEntityId =: newEmail[0].RelatedToId AND ContentDocument.Id IN : DocId order by SystemModstamp desc];
           System.debug('attachmentListTemp -- '+attachmentListTemp);
            List<ContentDocumentLink> attachmentListTempClone = new List<ContentDocumentLink>();
            for(ContentDocumentLink temp : attachmentListTemp){
				 System.debug('newEmail ID--Loop '+newEmail[0].Id);
				 System.debug('newEmail IN IF '+newEmail[0].Id);
				ContentDocumentLink  cnl=  temp.clone();
				cnl.LinkedEntityId = newEmail[0].Id;
				attachmentListTempClone.add(cnl);
			 
                System.debug('attachmentListTempClone ----- '+attachmentListTempClone);
				
            }
            
            System.debug('attachmentListTempClone -- '+attachmentListTempClone);
            insert attachmentListTempClone;  
			// JIRA | CASP2-118  | Shital Rathod changes End

			    
            //[End] Email Message insert data

            //[Start] Send Email
            /*string query = 'SELECT Id,Name,Subject__c, SuppliedEmail__c FROM CaseManager__c WHERE Id =:ctid Limit 1';
            CaseManager__c ct = Database.query(query);
            List<String> toList = new List<String> ();

            
            String mailBody = emailBody;
            if (String.isNotBlank(ct.SuppliedEmail__c))
            {
                toList.add(ct.SuppliedEmail__c);
                String subject=ct.Subject__c + ':' + startToken + ct.Name + endToken;
                
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toList);
                mail.setSubject(subject);
                mail.setHtmlBody(mailBody);
                mail.setWhatId(ctid);
                mails.add(mail);
                Messaging.sendEmail(mails);
            }*/
            //[End] Send Email
            
        }
        catch(Exception ex) {
                ExceptionHandlerUtility.writeException('VendorInquiryviewController', ex.getMessage(), ex.getStackTraceString());
        }
        return newEmail;
    }

    // Added By Niza -- CASP2-125
	@AuraEnabled
	public static List<ContentDocument> newUploadedAttachments(String[] docIDs) {
		System.debug('In' + docIDs);
		List<ContentDocument> attList = new List<ContentDocument> ();
		attList = [SELECT Title, FileExtension FROM ContentDocument where Id IN :docIDs];
		System.debug('Out' + attList);
		return attList; 
	}

	// Added By Niza -- CASP2-125
	@AuraEnabled
	public static boolean deleteAttachments(List<String> docuIDs) {
		system.debug('in loop---' + docuIDs);
		try {
			if (UtilityController.isDeleteable('ContentDocument', null))
			{
				delete[SELECT Id FROM ContentDocument where ID IN : docuIDs];
			}
		} catch(Exception e) {
			ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
		}
		return true;
	}

	// Added By Niza -- CASP2-125
    @AuraEnabled
    public static String getCustomSettingsDetails(){
        Sysco_SystemConfiguration__c confgset = Sysco_SystemConfiguration__c.getValues('ERPConfig');
        return confgset.Allow_File_Format__c;
    }
}