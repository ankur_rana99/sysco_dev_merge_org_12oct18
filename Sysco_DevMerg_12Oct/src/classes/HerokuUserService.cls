global class HerokuUserService{
    
    //--- Added by Ronak | JIRA:ESMPROD-211 | 17-02-2016 | Starts ---//
    //--- Changed by Bhupinder| JIRA:ESMPROD-275 | 04-10-2016 | Starts ---//
    private static String akritivesm = UtilityController.esmNameSpace;
  //--- Changed by Bhupinder| JIRA:ESMPROD-275 | 04-10-2016 | Ends ---//
    public static System_Configuration__c sysConf;
    private static Map<String,Schema.SObjectField> objectFields;

    public HerokuUserService(){
        //if(null != System_Configuration__c.getInstance(Userinfo.getUserid())) {
            //sysConf = System_Configuration__c.getInstance(Userinfo.getUserid());
            //} else {
            //sysConf = System_Configuration__c.getOrgDefaults();
        //}
       sysConf = [SELECT  Value__c FROM System_Configuration__c where Module__c='Heroku User Service' and Sub_Module__c ='HeroKu Base URL' limit 1];
    }
    static{
         objectFields = Schema.getGlobalDescribe().get(akritivesm+'User_Master__c').getDescribe().fields.getMap();
    }
     //--- Added by Ronak | JIRA:ESMPROD-253 | 17-02-2016 | Starts ---//
    private static Map<String, Object> createMap(User_Master__c user){
  
        Map<String, Object> userDetailMap = new Map<String, Object>(); 
        for(Schema.SObjectField fieldDef : objectFields.values()){
                userDetailMap.put(fieldDef.getDescribe().getName(), user.get(fieldDef.getDescribe().getName()));
        }
        return userDetailMap;
    }
    //--- Added by Ronak | JIRA:ESMPROD-253 | 17-02-2016 | Starts ---//
    /* apiOperation = insert / update */
    private static void executeApiCall(list<Id> ids){
        //System_Configuration__c sysConfNew;
        //if(null != System_Configuration__c.getInstance(Userinfo.getUserid())) {
            //sysConfNew = System_Configuration__c.getInstance(Userinfo.getUserid());
            //} else {
            //sysConfNew = System_Configuration__c.getOrgDefaults();
        //}
        
        //if(null == sysConfNew || null == sysConfNew.HeroKu_Base_URL__c){
            //return;
        //}
        //--- Added by Ronak | JIRA:ESMPROD-253 | 17-02-2016 | Starts ---//
        list<Map<String, Object>> allUserObjet = new list<Map<String, Object>>();
        String theQuery = 'SELECT ';
        for(Schema.SObjectField fieldDef : objectFields.values()){
            theQuery += fieldDef.getDescribe().getName()+ ',';
        }
        theQuery = theQuery.removeEnd(',');
        theQuery += ' FROM '+akritivesm+'User_Master__c WHERE Id IN :ids and '+akritivesm+'User_Type__c = \'Requestor\'';
        system.debug('--theQuery --'+theQuery );
        list<User_Master__c> users = DataBase.query(theQuery);
        //--- Added by Ronak | JIRA:ESMPROD-253 | 17-02-2016 | Ends ---//      
        for(User_Master__c user : users){
            
            if(user.User_Type__c == 'Requestor'){
            
                allUserObjet.add(createMap(user));
            }
        }
        
        if(0 < allUserObjet.size()){
            try{
                System.debug('Http from =>' +String.valueOf(JSON.serialize(allUserObjet)));
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(Blob.valueOf(UserInfo.getOrganizationId())); 
                
                System.debug('authorizationHeader ' + authorizationHeader+'=>'+UserInfo.getOrganizationId());
                
                HttpRequest req = new HttpRequest();
                req.setMethod('POST');
                req.setEndpoint(sysConf.Value__c+'/buyer-user/create-or-update');
                req.setHeader('Authorization', authorizationHeader);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(String.valueOf(JSON.serialize(allUserObjet)));
                
                Http http = new Http();
                HTTPResponse res = http.send(req);
                /*System.debug('Fulfillment service returned '+ res.getBody());*/
                
                 System.debug('Http from =>' + req.getEndpoint() + ' : ' +
                                 res.getStatusCode() + '=>'+res.getBody()+'=>'+ res.getStatus());
                 System.debug('Http from 123=> ' + authorizationHeader );
            }catch(Exception e){
                system.debug('exception'+e);
            }
        }
    }

    @future(callout=true)
    public static void createHerokuUserDetails(list<Id> ids){
        executeApiCall(ids);
    }
    
    public void createHerokuUserDetails(Set<Id> ids){
        if(null != sysConf && null != sysConf.Value__c){
            list<User_Master__c> users = [SELECT Id
                 FROM User_Master__c 
                 WHERE Id IN :ids and User_Type__c = 'Requestor'];
                 
            if(null != users && 0 < users.size()){
                list<Id> idsNew =  new list<Id>();
                for(User_Master__c bu : users){
                    idsNew.add(bu.Id);
                }
                createHerokuUserDetails(idsNew);
            }
        }
    }
    
    
    //--- Added by Ronak | JIRA:ESMPROD-211 | 17-02-2016 | Ends ---// 
}