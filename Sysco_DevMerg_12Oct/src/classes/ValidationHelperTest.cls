@isTest
public class ValidationHelperTest{
    
       public static testMethod void testFirst(){

       ValidationHelper.Option OptionWrap = new  ValidationHelper.Option();
		OptionWrap.value = 'a';
		OptionWrap.text = 'b';

		 ValidationHelper.Option OptionWrap1 = new  ValidationHelper.Option('a','b');

		 ValidationHelper.FieldInfo FieldInfoWrap = new ValidationHelper.FieldInfo();
		 FieldInfoWrap.label = 'a';
		 FieldInfoWrap.apiName = 'invoice';
		 FieldInfoWrap.type = 'create';
		
		 ValidationHelper.Criteria CriteriaWrap = new ValidationHelper.Criteria();
		 CriteriaWrap.field = 'Document_Type__c';
		// CriteriaWrap.operator = 'equals';
		// CriteriaWrap.vlaue = 'PO Invoice';
		 CriteriaWrap.type = 'create';
		 CriteriaWrap.refField = 'sObject';

		 ValidationHelper.Attribute AttributeWrap = new ValidationHelper.Attribute();
		 AttributeWrap.additionalRuleField = 'Tolerance_In_Amount';
		 AttributeWrap.additionalRuleValue = '1';

		 ValidationHelper.ValidationRule ValidationRuleWrap = new  ValidationHelper.ValidationRule();

		ValidationRuleWrap.JSONValue = null;
		ValidationRuleWrap.additionRuleName = 'Test';
		ValidationRuleWrap.additionRules = null;
		ValidationRuleWrap.ruleName= 'a';
		ValidationRuleWrap.order = 1;
		ValidationRuleWrap.id = '1';
		ValidationRuleWrap.isActive = false;
		ValidationRuleWrap.isCustom = false;
		ValidationRuleWrap.type = 'create';
		ValidationRuleWrap.isStopExecution = false;
		ValidationRuleWrap.errorType = 'Exception';
		ValidationRuleWrap.errorCode = 'INVOICE_LINE_NOT_FOUND';
		ValidationRuleWrap.objectName = 'invoice__c';

		ValidationHelper.QCFormResponse QCFormResponseWrap = new ValidationHelper.QCFormResponse();
		QCFormResponseWrap.qcFormFieldsetName = 'aa';
		QCFormResponseWrap.qcFormHeader = 'bb';

    }

}