public class SyscoInvoiceOnSaveClass implements OnChangeConfig {
    public boolean execute(SObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<PO_Line_Item__c> pOLineItemList, List<GRN_Line_Item__c> grnLineItemList, String field, String value, String childNameSpace, Map<object, object> extraParamMap) {
        System.debug('sdd45---'+Double.valueOf(childSobject.get('Tax__c')));
        SYstem.debug('****Inside SyscoInvoiceOnSaveClass*******');
        if (Double.valueOf(childSobject.get('Tax__c')) == 2.1)
        {
            extraParamMap.put('alertmessage','Custom apex save alert Tax__c cannot be 2');
        }
        childSobject.put('Tax__c', 6.000);
		Invoice__c invoice = (Invoice__c) childSobject;
		
		
			// **********************************\\Changes added by Vaibhav to add custom validation hard stops - start//****************************
			set<String> setCenterNames = new set<String>();
			set<Id> setCenterId = new set<Id>();
			set<Id> setcategory = new set<Id>();
			Boolean isUnMatched = false;
			map<Id, String> mapCategoryRef  = new map<Id, String>();
			map<String, List<CostCenterHierarchy__c>> mapCentersWithHrrchy = new map<String, List<CostCenterHierarchy__c>>();
			List<String> lstUserHrrchy = new List<String>();
			map<String, List<String>> mapCategoryWithHrrchy = new map<String, List<String>>();
			for(Invoice_Line_Item__c line : invLineItemList){
				setCenterId.add(line.Cost_Code__c);
				setcategory.add(line.Spend_Category__c);
			}
			for(Spend_Categories__c cat : [SELECT Id, Reference_ID__c, Spend_Categories_Name__c, Inactive__c, Name FROM Spend_Categories__c WHERE Id IN: setcategory]){
				mapCategoryRef.put(cat.Id, cat.Reference_ID__c);
			}
			if(!invLineItemList.isEmpty() && setCenterId != null && setcategory != null && invoice.ERP_Type__c == 'Workday'){
				for(WD_CustomValidations__c std :  [SELECT SC_Ref_Id__c, CCH_Ref_Id__c, Name, Id FROM WD_CustomValidations__c]){
					if(!mapCategoryWithHrrchy.containsKey(std.SC_Ref_Id__c)){
						mapCategoryWithHrrchy.put(std.SC_Ref_Id__c , new List<String>() );
					}
					mapCategoryWithHrrchy.get(std.SC_Ref_Id__c).add(std.CCH_Ref_Id__c);
				}
				for(Cost_Center__c center : [SELECT Description__c, OPCO_Code__c, Cost_Center_Name__c, Inactive__c, Reference_ID__c, Input_Source__c, Company_Res_WID__c, Company_Res_OReference_id__c, Company_Res_CReference_id__c, Unique_Key__c, Name, Id FROM Cost_Center__c WHERE Input_Source__c = 'Workday' AND Id IN : setCenterId]){
					setCenterNames.add(center.Cost_Center_Name__c);
				}
				for(CostCenterHierarchy__c hrchy : [SELECT CostCenter_Name__c, CostCenter_RefId__c, Hierarchy_Name__c, Hierarchy_RefID__c, Inactive__c, InputSource__c, Unique_Key__c, Name, Id FROM CostCenterHierarchy__c WHERE InputSource__c = 'Workday' AND CostCenter_Name__c IN: setCenterNames]){
					if(!String.isBlank(hrchy.CostCenter_RefId__c) && !mapCentersWithHrrchy.containsKey(hrchy.CostCenter_RefId__c)){
						mapCentersWithHrrchy.put(hrchy.CostCenter_RefId__c, new List<CostCenterHierarchy__c>());
					}
					mapCentersWithHrrchy.get(hrchy.CostCenter_RefId__c).add(hrchy);
					lstUserHrrchy.add(hrchy.Hierarchy_RefID__c);
				}
				for(String cat : mapCategoryRef.values()){
					System.debug('My chosen Category==> ' + cat);
					// for checking categories and heirarchy in Static table
					if(mapCategoryWithHrrchy.containsKey(cat) && mapCategoryWithHrrchy.get(cat) != null){
						System.debug('About to lost, but got found somehow!!!');
						for(String userHrrchy : lstUserHrrchy){
							for(String hrrchy : mapCategoryWithHrrchy.get(cat)){
								if(userHrrchy == hrrchy){
									System.debug('Yeaaahh! I got Matched');
									break;
								} else {
									System.debug('Urghhh! I got Lost');
									isUnMatched = true;
								}
					
							}
						}
					}
				}
				if(isUnMatched){
					extraParamMap.put('alertmessage', 'Spend category is not matching with standards');
				}
			}
			
			// **********************************\\Changes added by Vaibhav to add custom validation hard stops - Ends//****************************
			
		/*	String uid = UserInfo.getUserId();
            List < user > userlist = new List < user > ();
			userlist = [select id, name, contactId, contact.Name, account.Name, account.OPCO_Code__c from user where id =: uid];
            System.debug('userlist----INVclass--' + userlist);
			System.debug('userlist--acc name----' + userlist[0].account.Name);
			childSobject.put('Vendor__c', userlist[0].accountId);
			childSobject.put('Vendor_Name__c', userlist[0].account.Name);
			childSobject.put('OPCO_Code__c', userlist[0].account.OPCO_Code__c);*/

        //extraParamMap.put('alertmessage', 'Custom alert');
        
        /*
		Supplier Comments changes - By Ashish Kr. | CASP2-6 | 21 June 2018
		*/
        system.debug('Supplier Comments changes - By Ashish Kr. supplierComments ');
        Map<String,String> supplierComments_SCMap = new Map<String,String>();
        Supplier_Comment_Configuration__mdt[] supplierCommentConfigs = [Select MasterLabel, QualifiedApiName, Comments__c from Supplier_Comment_Configuration__mdt];
        Map<String,String> shortCode_CommentsMap = new Map<String,String>();
        if(supplierCommentConfigs.size()>0){
            for(Supplier_Comment_Configuration__mdt supplierCommentConfig : supplierCommentConfigs){
                shortCode_CommentsMap.put(supplierCommentConfig.MasterLabel,supplierCommentConfig.Comments__c);
            }
        }
        
        System.debug('shortCode_CommentsMap - ' + shortCode_CommentsMap);
        if(invLineItemList.size()>0){
            for (Invoice_Line_Item__c invl : invLineItemList) {
                String catchWeight_SC = '';
                String quantity_SC = '';
                String price_SC = '';
                
                if(!String.isBlank(invl.Catch_Weight_Short_Code__c)){
                    catchWeight_SC = 'Catch Weight - ' + invl.Catch_Weight_Short_Code__c;
                }
                if(!String.isBlank(invl.Qty_Short_Code__c)){
                    quantity_SC = 'Quantity - '+ invl.Qty_Short_Code__c;
                }
                if(!String.isBlank(invl.Price_Short_Code__c)){
                    price_SC = 'Price - ' +invl.Price_Short_Code__c;
                }
                system.debug('invoice cora ref '+ invl.id);
                system.debug('po line item '+ invl.PO_Line_Item__c);
                if(shortCode_CommentsMap.containsKey(catchWeight_SC)){
                    String comment = '';
                    if(!supplierComments_SCMap.containsKey(catchWeight_SC)){
                        comment = shortCode_CommentsMap.get(catchWeight_SC) + ' '+ invl.Invoice_Line_No__c;
                    }else{
                        comment = supplierComments_SCMap.get(catchWeight_SC) + ' ' + invl.Invoice_Line_No__c;
                    }
                    supplierComments_SCMap.put(catchWeight_SC,comment);
                }
                
                if(shortCode_CommentsMap.containsKey(quantity_SC)){
                    String comment = '';
                    if(!supplierComments_SCMap.containsKey(quantity_SC)){
                        comment = shortCode_CommentsMap.get(quantity_SC) + ' '+ invl.Invoice_Line_No__c;
                    }else{
                        comment = supplierComments_SCMap.get(quantity_SC) + ' ' + invl.Invoice_Line_No__c;
                    }
                    supplierComments_SCMap.put(quantity_SC,comment);
                }
                
                if(shortCode_CommentsMap.containsKey(price_SC)){
                    String comment = '';
                    if(!supplierComments_SCMap.containsKey(price_SC)){
                        comment = shortCode_CommentsMap.get(price_SC) + ' '+ invl.Invoice_Line_No__c;
                    }else{
                        comment = supplierComments_SCMap.get(price_SC) + ' ' + invl.Invoice_Line_No__c;
                    }
                    supplierComments_SCMap.put(price_SC,comment);
                }
            } 
        }
        
        
        system.debug('supplier Comments ' + supplierComments_SCMap);
        String supplierComments = '';
        for(String shortCodeKey : supplierComments_SCMap.keySet()){
        	supplierComments = supplierComments + supplierComments_SCMap.get(shortCodeKey);
        	supplierComments = supplierComments + '\n' + '-----------------------------------' + '\n ';
        }
        system.debug('supplierComments '+ supplierComments);
		childSobject.put('Supplier_Comments__c',supplierComments);
        
        /* 
		Supplier comments changes ends - By Ashish Kr.
		*/
        return false;
    }
}