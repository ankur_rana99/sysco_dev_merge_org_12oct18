/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: This class is use to store data of email attachments.
  Dependencies: EmailToCaseBean,EmailToCaseManager,EmailToCaseNotificationManager,EmailToCaseService
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class EmailToCaseAttachment {

    public String body { get; set; }
    public String fileName { get; set; }
    public String mimeTypeSubType { get; set; }
    public Map<string, string> headers { get; set; }
    public blob binaryBody { get; set; }
    public string type { get; set; }

    /*
      Authors: Chandresh Koyani
      Purpose: Get Extention of attachment.
      Dependencies: EmailToCaseBean.cls.
     */
    public string extension {
        get {
            if (!string.isEmpty(fileName) && fileName.indexOf('.') != - 1) {
                return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
            }
            return '';
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose:  Check this attachment is inline of not.
      Dependencies: EmailToCaseNotificationManager.cls.
     */
    public boolean isInline {
        get {
            if (this.headers != null) {
                string contentDisposition = Headers.get('Content-Disposition');
                if (contentDisposition != null && contentDisposition.split(';') [0].equalsIgnoreCase('inline')) {
                    return true;
                }
            }
            return false;
        }
    }

}