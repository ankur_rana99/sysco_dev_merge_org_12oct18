global class SyscoGetCostCenterData implements database.Batchable < Sobject > , Schedulable, Database.Stateful, Database.AllowsCallouts {
    private SchedulableContext objSchedulableContext;
    List < Cost_Center__c > lstExistingCostCenters;
    public Map < String, String > mapOPCO;
    public Map < String, Cost_Center__c > mapExistingCostCenters;

    global SyscoGetCostCenterData() {
        mapOPCO = new Map < String, String > ();
        mapExistingCostCenters = new Map < String, Cost_Center__c > ();
        for (OPCO_Code__c opco: [SELECT OPCO_Name__c, OPCO_Code__c, Id, Name FROM OPCO_Code__c]) {
            mapOPCO.put(opco.OPCO_Code__c, opco.Id);
        }
        for (Cost_Center__c cc: [SELECT Input_Source__c, Unique_Key__c, Cost_Center_Name__c, OPCO_Code__c, Id, Name, Description__c, Company_Res_WID__c, Company_Res_CReference_id__c, Company_Res_OReference_id__c, Inactive__c, Reference_ID__c FROM Cost_Center__c WHERE Input_Source__c = 'Workday' Order By Name Asc]) {
            mapExistingCostCenters.put(cc.Unique_Key__c, cc);
        }
    }

    global List < Cost_Center__c > start(Database.BatchableContext BC) {
        List < Cost_Center__c > lstCostCenter = new List < Cost_Center__c > ();
        try {
            String dateFormatString = 'yyyy-MM-dd';
            Date today = Date.today();
            Datetime dt1 = Datetime.newInstance(today.year(), today.month(),today.day());
            String tday = dt1.format(dateFormatString);
            Date previousday = Date.today() - 1;
            Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(),previousday.day());
            String prevday = dt2.format(dateFormatString);
            
            WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('CostCenter');
            if(WEEndpoint.isfirstrun__c == 'Y'){
               prevday = '';                      
            }
            
            String XMLString;
            HttpRequest feedRequest = new HttpRequest();
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c);
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + '2018-08-11'); 
            feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday); 
            System.debug('endpoint===> ' + WEEndpoint.Endpoint_URL__c);
            feedRequest.setMethod('GET');
            feedRequest.setTimeout(10000);
            Http http = new Http();
            HTTPResponse CostCenterResponse = http.send(feedRequest);
            System.debug(CostCenterResponse.getBody());
            XMLString = CostCenterResponse.getBody();
            DOM.Document doc = new DOM.Document();
            doc.load(XMLString);
            DOM.XmlNode rootNode = doc.getRootElement();
            System.debug(' rootNode...' + rootNode);
            if (rootNode.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                for (Dom.XMLNode child: rootNode.getChildElements()) {
                    if (child.getName() == 'Report_Entry') {
                        for (Dom.XMLNode cr: Child.getChildElements()) {
                            if (cr.getName() == 'Company_Restrictions') {
                String CCName = Child.getChildElement('name', Child.getNamespace()).getText();
                String referenceID = Child.getChildElement('referenceID', Child.getNamespace()).getText();
                String displayName =  referenceID +'-'+ CCName;             
                if(displayName.length() > 80 ){
                  displayName = displayName.substring(0,80);
                }
                                Dom.XMLNode[] Company_Restrictions = cr.getChildren();
                                if (mapOPCO != null && mapOPCO.get(Company_Restrictions[2].getText().trim()) != null) {
                                    Cost_Center__c costCenter = new Cost_Center__c(
                                        Unique_Key__c   = Child.getChildElement('referenceID', Child.getNamespace()).getText() + '-' + Company_Restrictions[1].getText().trim(), 
                                        OPCO_Code__c   = mapOPCO.get(Company_Restrictions[2].getText().trim()), 
                                        Cost_Center_Name__c = Child.getChildElement('name', Child.getNamespace()).getText(), 
                                        Inactive__c   = Child.getChildElement('Inactive', Child.getNamespace()).getText(), 
                                        Reference_ID__c = Child.getChildElement('referenceID', Child.getNamespace()).getText(), 
                                        Input_Source__c = 'Workday', 
                                        Name = displayName, 
                                        Company_Res_WID__c = Company_Restrictions[0].getText().trim(), Company_Res_OReference_id__c = Company_Restrictions[1].getText().trim(), 
                                        Company_Res_CReference_id__c = Company_Restrictions[2].getText().trim());
                                    lstCostCenter.add(costCenter);
                                }
                            }
                        }
                    }
                }
            }
            System.debug('lstCostCenter----size--->: ' + lstCostCenter.size() + '===><lstCostCenter------->' + lstCostCenter);
        } catch (exception e) {
            system.debug(e.getMessage());
        }
        return lstCostCenter;
    }

    global void Execute(Database.BatchableContext objBatchableContext, List < Cost_Center__c > lstCCenters) {
        try {
            if (lstCCenters.size() > 0 && !lstCCenters.isempty()) {
                Schema.SObjectField externalId = Cost_Center__c.Fields.Unique_Key__c;
                Database.UpsertResult[] results = Database.upsert(lstCCenters, externalId, false);
                for (Integer index = 0, size = results.size(); index < size; index++) {
                    if (results[index].isSuccess()) {
                        if (results[index].isCreated()) {
                            System.debug(lstCCenters[index].Unique_Key__c + ' was created');
                        } else {
                            System.debug(lstCCenters[index].Unique_Key__c + ' was updated');
                        }
                    } else {
                        System.debug(lstCCenters[index].Unique_Key__c + ' was failed');
                        for (Database.Error err: results[index].getErrors()) {
                            System.debug('The following error has occurred.');
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
        } catch (exception e) {
            system.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC) { 
     WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('CostCenter'); 
     System.debug('WEEndpoint..... '+ WEEndpoint.isfirstrun__c);
         if(WEEndpoint.isfirstrun__c == 'Y'){
            System.debug('WEEndpoint.isfirstrun__c +'+ WEEndpoint.isfirstrun__c);
            WEEndpoint.isfirstrun__c = 'N';
            Update WEEndpoint;                       
        }  
    }

    global void execute(SchedulableContext objSchedulableContext) {
        SyscoGetCostCenterData syscoCC = new SyscoGetCostCenterData();
        Integer batchSize = 200;
        system.database.executebatch(syscoCC, batchSize);
    }
}