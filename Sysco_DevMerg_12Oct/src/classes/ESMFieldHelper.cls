public class ESMFieldHelper {

    public static List<ESMFieldHelper.FieldDetails> getFieldsByFieldSet(string fieldsetName, CaseManager__c caseManager, string sessionId) {
        List<ESMFieldHelper.FieldDetails> fieldDetails = new List<ESMFieldHelper.FieldDetails> ();
        try {

            string objectName = ObjectUtil.getWithNameSpace('CaseManager__c');
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
            fieldsetName = ObjectUtil.getValidFieldSetName(objectName, fieldsetName);
            Schema.DescribeSObjectResult describeObjectResult = targetType.getDescribe();
            Map<String, Schema.SObjectField> fieldsMap = describeObjectResult.fields.getMap();
            Schema.FieldSet fieldSet = describeObjectResult.fieldSets.getMap().get(fieldsetName);

            Map<string, set<string>> pickListValuesBasedOnRecordType = null;
            if (caseManager != null)
            {
                string recordTypeName = '';
                if (String.isEmpty(String.valueOf(caseManager.get('recordtypeid'))))
                {
                    for (Schema.RecordTypeInfo recordTypeInfoResult : describeObjectResult.getRecordTypeInfos()) {
                        if (recordTypeInfoResult.isDefaultRecordTypeMapping()) {
                            recordTypeName = recordTypeInfoResult.getName() == 'Master' ? '' : recordTypeInfoResult.getName();
                            break;
                        }
                    }
                }
                else {
                    recordTypeName = (String) caseManager.getSObject('RecordType').get('Name');
                }
                if (!String.isEmpty(recordTypeName)) {
                    pickListValuesBasedOnRecordType = UtilityController.GetPicklistValuesBasedOnRecordType(objectName, recordTypeName, sessionId);
                }

            }

            for (Schema.FieldSetMember fieldSetMember : fieldSet.getFields()) {
                String fieldAPIName = fieldsMap.containsKey(fieldSetMember.getFieldPath().toLowerCase()) ? fieldSetMember.getFieldPath() : ObjectUtil.getWithNameSpace(fieldSetMember.getFieldPath());
                ESMFieldHelper.FieldDetails fieldDetail = new ESMFieldHelper.FieldDetails();
                Schema.DescribeFieldResult describeFieldResult = fieldsMap.get(fieldAPIName).getDescribe();

                fieldDetail.fieldLabelName = fieldSetMember.getLabel();
                fieldDetail.fieldAPIName = fieldAPIName;
                fieldDetail.fieldType = String.valueOf(fieldSetMember.getType());
                fieldDetail.fieldRelationship = describeFieldResult.getRelationshipName();
                fieldDetail.display = '';

                if (fieldDetail.fieldType == 'REFERENCE') {
                    List<Schema.SObjectType> relation = describeFieldResult.getReferenceTo();
                    fieldDetail.relationship = relation[0].getDescribe().getName();
                }
                if (fieldDetail.fieldType == 'PICKLIST' || fieldDetail.fieldType == 'MULTIPICKLIST') {
                    List<Object> options = new List<Object> ();
                    Set<String> recordTypeValues = new Set<String> ();
                    if (pickListValuesBasedOnRecordType != null)
                    {
                        recordTypeValues = pickListValuesBasedOnRecordType.get(objectName + '.' + describeFieldResult.getName());
                    }
                    for (Schema.PicklistEntry pickListEntry : describeFieldResult.getPicklistValues()) {
                        if (pickListValuesBasedOnRecordType == null || (recordTypeValues != null && recordTypeValues.contains(pickListEntry.getValue()))) {

                            Map<String, String> obj = new Map<String, string> ();
                            obj.put('label', pickListEntry.getLabel());
                            obj.put('value', pickListEntry.getValue());
                            obj.put('selected', 'false');
                            options.add(obj);
                        }
                    }
                    fieldDetail.options = options;
                    if (describeFieldResult.getController() != null) {
                        fieldDetail.controlField = describeFieldResult.getController().getDescribe().getName();
                        Map<String, List<String>> dependentValueMap = UtilityController.getDependentOptionsImpl(objectName, (String) fieldDetail.controlField, fieldAPIName);
                        Map<String, List<Object>> dependentValueMapFinal = new Map<String, List<Object>> ();

                        for (String fieldName : dependentValueMap.keySet()) {
                            List<Object> suboptions = new List<Object> ();
                            for (String fieldVal : dependentValueMap.get(fieldName)) {
                                if (pickListValuesBasedOnRecordType == null || (recordTypeValues != null && recordTypeValues.contains(fieldVal))) {
                                    Map<String, string> obj1 = new Map<String, string> ();
                                    obj1.put('label', fieldVal);
                                    obj1.put('value', fieldVal);
                                    obj1.put('selected', 'false');
                                    suboptions.add(obj1);
                                }
                            }
                            dependentValueMapFinal.put(fieldName, suboptions);
                        }
                        fieldDetail.dependentValues = dependentValueMapFinal;
                    }
                }

                if (fieldSetMember.getRequired()) {
                    fieldDetail.fieldBehavior = 'Required';
                    fieldDetail.defaultBehaviour = 'Mandatory';
                }
                else {
                    fieldDetail.defaultBehaviour = 'Edit';
                    fieldDetail.fieldBehavior = 'Edit';
                }

                fieldDetails.add(fieldDetail);
            }
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return fieldDetails;
    }

    public class SectionFields {
        @AuraEnabled
        public String sectionName { get; set; }
        @AuraEnabled
        public String sectionId { get; set; }
        @AuraEnabled
        public list<FieldDetails> relatedFields { get; set; }
    }

    public class FieldResponse {
        @AuraEnabled
        public List<SectionFields> sectionFields { get; set; }
        @AuraEnabled
        public ESMUISetupController.JSONValue fieldJson { get; set; }
    }

    //Wrapper class for Field
    public class FieldDetails {
        @AuraEnabled
        public String fieldLabelName { get; set; }
        @AuraEnabled
        public String fieldAPIName { get; set; }
        @AuraEnabled
        public String fieldType { get; set; }
        @AuraEnabled
        public String fieldBehavior { get; set; }
        @AuraEnabled
        public String fieldRelationship { get; set; }
        @AuraEnabled
        public Object options { get; set; }
        @AuraEnabled
        public String relationship { get; set; }
        @AuraEnabled
        public Object controlField { get; set; }
        @AuraEnabled
        public Object dependentValues { get; set; }
        @AuraEnabled
        public string display { get; set; }
        @AuraEnabled
        public String defaultBehaviour { get; set; }
    }


    public static Map<String, List<String>> getDependentOptionsImpl(string objApiName, string contrfieldApiName, string depfieldApiName) {
        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();

        Map<String, List<String>> objResults = new Map<String, List<String>> ();
        //get the string to sobject global map
        Map<String, Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();

        if (!Schema.getGlobalDescribe().containsKey(objectName)) {
            return null;
        }

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType == null) {
            return objResults;
        }
        Bitset bitSetObj = new Bitset();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)) {
            return objResults;
        }

        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();

        objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer> ();
        for (Integer contrIndex = 0; contrIndex < contrEntries.size(); contrIndex++) {
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getLabel();
            objResults.put(label, new List<String> ());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry> ();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper> ();
        for (Integer dependentIndex = 0; dependentIndex < depEntries.size(); dependentIndex++) {
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            objEntries.add(depentry);
        }
        objJsonEntries = (List<PicklistEntryWrapper>) JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        List<Integer> indexes;

        for (PicklistEntryWrapper objJson : objJsonEntries) {
            if (objJson.validFor == null || objJson.validFor == '') {
                continue;
            }
            String myString = objJson.validFor;
            myString = myString.replaceAll('[^a-zA-Z0-9]', '');
            indexes = bitSetObj.testBits(myString, controllingIndexes);
            //indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);

            for (Integer idx : indexes) {
                String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }

        }
        objEntries = null;
        objJsonEntries = null;
        return objResults;
    }
    //This method will used to get recordtype wise picklist value
    public static Map<string, set<string>> GetPicklistValuesBasedOnRecordType(string objectName, string recordTypeName, String strSessionId) {
        Map<string, set<string>> pickListValueMap = new Map<string, set<string>> ();
        ESMMetadataService.MetadataPort service = new ESMMetadataService.MetadataPort();
        service.SessionHeader = new ESMMetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = strSessionId;
        ESMMetadataService.RecordType recordType = (ESMMetadataService.RecordType) service.readMetadata('RecordType', new String[] { objectName + '.' + recordTypeName }
        ).getRecords() [0];
        for (ESMMetadataService.RecordTypePicklistValue rpk : recordType.picklistValues) {
            set<string> picklistvalues = new set<string> ();
            for (ESMMetadataService.PicklistValue val : rpk.values) {
                if (!string.isEmpty(val.fullName)) {
                    picklistvalues.add(EncodingUtil.urldecode(val.fullName, 'UTF-8'));
                }
            }
            pickListValueMap.put(objectName + '.' + rpk.picklist, picklistvalues);
        }
        return pickListValueMap;
    }

    public static boolean isRuleMatch(ESMUISetupController.Rule rule, CaseManager__c caseTrackerObject) {
        boolean isMatch = false;
        if (rule.criterias != null) {
            for (ESMUISetupController.Criteria cre : rule.criterias) {
                if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                    string fieldValue = string.valueOf(caseTrackerObject.get(cre.field));
                    isMatch = isMatch(cre.operator, cre.value, fieldValue, cre.type);
                }
                else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                    integer fieldValue = integer.valueOf(caseTrackerObject.get(cre.field));
                    isMatch = isMatch(cre.operator, cre.value, fieldValue);
                }
                else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                    boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.field));
                    isMatch = isMatch(cre.operator, cre.value, fieldValue);
                }
                if (!isMatch) {
                    break;
                }
            }
        }
        return isMatch;
    }

    @testVisible
    private static boolean isMatch(string operator, string value, string fieldValue, string type) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == value;
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != value;
        }
        else if (operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(value)) {
                List<string> tempList = value.split(',');
                for (string str : tempList) {
                    if (type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue != null && fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue != null && fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
        else if (operator.equalsIgnoreCase('does not contains in list')) {
            if (!string.isEmpty(value)) {
                List<string> tempList = value.split(',');
                for (string str : tempList) {
                    if (fieldValue != null && fieldValue.equalsIgnoreCase(str)) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for integer value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(string operator, string value, integer fieldValue) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('less than')) {
            return fieldValue < integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('greater than')) {
            return fieldValue > integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= integer.valueOf(value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(string operator, string value, boolean fieldValue) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(value);
        }
        return false;
    }
}