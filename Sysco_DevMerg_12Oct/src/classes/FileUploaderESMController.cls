public with sharing class FileUploaderESMController {
    // Added by Shital Rathod AS per Niza changes 
   // static String  coraNameSpace = UtilityController.coraNameSpace;
    @AuraEnabled
    public static String getInstanceName() {
        return [select InstanceName from Organization limit 1].InstanceName;
    }
    
    @AuraEnabled
    public static boolean getPortalUserDetails(){
        return [select IsPortalEnabled  from User where id =: userInfo.getUserId()].IsPortalEnabled ;
    }
}