/*
* Name           : QueryBuilder.apxc
* Author         : Atul Hinge
* Description    : used to build query
*/
public class QueryBuilder {
    public enum SortOrder {ASCENDING, DESCENDING}
    public Schema.SObjectType table {get; private set;}
    Map<String,Schema.SObjectField> columns;
    private Set<String> fields;
    private String conditionExpression;
    private Integer limitCount;
    private Integer offsetCount;
    private String order;
    private Boolean enforceFLS;
    private Boolean sortSelectFields = true;
    private Schema.ChildRelationship relationship;
    private Map<Schema.ChildRelationship, QueryBuilder> subselectQueryMap;
    private Set<string> accessList;
    public QueryBuilder(Schema.SObjectType obj){
        this.table =obj;
        columns=table.getDescribe().fields.getMap();
        fields=new Set<String>();
        accessList = new Set<String>{'Read'};
        order=' CreatedDate desc ';
        this.addFields();
    }
    public QueryBuilder(Schema.ChildRelationship relationship){
       this(relationship.getChildSObject());
       this.relationship = relationship;
    }
    public String query(){
        String result = 'SELECT ';
        
        if (fields.size() == 0){
            result += 'Id';
        }else {
            List<String> fieldsToQuery = new List<String>(fields);
            if(sortSelectFields){
                fieldsToQuery.sort(); 
            }   
            result += String.join(fieldsToQuery,', ');
        }
        
        if(subselectQueryMap != null && !subselectQueryMap.isEmpty()){
            for (QueryBuilder childRow : subselectQueryMap.values()){
                result += ', (' + childRow.query() + ') ';
            }   
        }
        
       result += ' FROM ' + (relationship != null ? relationship.getRelationshipName() : table.getDescribe().getName());
        
        if(conditionExpression != null)
            result += ' WHERE '+conditionExpression;
        
        if(order != null){
            result += ' ORDER BY ' + order;
        }
        
        if(limitCount != null)
            result += ' LIMIT '+limitCount;
        
        if(offsetCount != null)
            result += ' OFFSET '+offsetCount;
        
        return result;
    }
    public QueryBuilder setCondition(String conditionExpression){
        this.conditionExpression = conditionExpression;
        return this;
    }
    private String getFieldPath(String fieldName){
        if(!fieldName.contains('.')){ 
            Schema.SObjectField token = columns.get(fieldName.toLowerCase());
            return token.getDescribe().getName();
        }
        return fieldName;
    }
    private void addFields(){
        for(String key:columns.keySet()){
            addField(columns.get(key));
        }
    }
    private void addField(String fieldName){
        fields.add(getFieldPath(fieldName));
    }
    private void addField(Schema.SObjectField field){
        boolean isValid=true;
        Schema.DescribeFieldResult dfr = field.getDescribe();
        if (accessList.contains('Read') && !dfr.isAccessible()) {
           isValid=false;
        }
        if (accessList.contains('Edit') && !dfr.isUpdateable()) {
             isValid=false;
        }
        if (accessList.contains('Create') && !dfr.isCreateable()) {
            isValid=false;
        }
       
        if (isValid) {
            fields.add(dfr.getName());
        }
        //fields.add(getFieldPath(fieldName));
    }
    public void addRelationShipFields(Set<String> relationShipfieldName){
        if(relationShipfieldName != null){
            for(String key:relationShipfieldName){
                addField(key);
            }
        } 
    }
     public void addRelationShipQuery(Set<String> relationShipName){
        if(relationShipName != null){
            for(String key:relationShipName){
                subselectQuery(key);
            }
        } 
    }
  
    public QueryBuilder subselectQuery(String relationshipName){
        ChildRelationship relationship = getChildRelationship(relationshipName);
        return setSubselectQuery(relationship);
        
    }
      private Schema.ChildRelationship getChildRelationship(String relationshipName){
        for (Schema.ChildRelationship childRow : table.getDescribe().getChildRelationships()){
            if (childRow.getRelationshipName() == relationshipName){ 
                return childRow;
            }   
        }
        return null;
    }

    private QueryBuilder setSubselectQuery(ChildRelationship relationship){
        
        if (this.subselectQueryMap == null){
            this.subselectQueryMap = new Map<ChildRelationship, QueryBuilder>();
        }
        if (this.subselectQueryMap.containsKey(relationship)){
            return subselectQueryMap.get(relationship);
        }
        
        QueryBuilder subselectQuery = new QueryBuilder(relationship);
        subselectQueryMap.put(relationship, subSelectQuery);
        return subSelectQuery;
    }
}