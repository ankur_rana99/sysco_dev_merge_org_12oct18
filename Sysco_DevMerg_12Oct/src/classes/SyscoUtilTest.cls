@isTest
public class SyscoUtilTest{
	public static testMethod void testFirst(){
		System_Configuration__c sysconfig=new System_Configuration__c ();
		sysconfig.Module__c='Custom_Record_History';
		sysconfig.Value__c ='Invoice__c';
		insert sysconfig;

		System_Configuration__c sysconfig2 = new System_Configuration__c(); 
		sysconfig2.Module__c='Invoice__c'; 
		sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State'; 
		sysconfig2.Value__c = 'Awaiting OCR Feed'; 
		insert sysconfig2;

		Invoice_Configuration__c invConf = new Invoice_Configuration__c();
		invConf.Enable_Quantity_Calculation__c = true;
		invConf.GRN_Flag_In_PO_Header__c = true;
		invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
		invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
		invConf.Enable_Auto_Flipping_With_GRN__c = true;
		invConf.PO_Number_Field_for_Search__c = 'Name';
		invConf.Invoice_Reject_Current_State__c ='';
		invConf.Auto_Match_Flow_Method__c = 'Sequence';
		invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
		invConf.Auto_Validation_Current_States__c = '';
		insert invConf; 

		OPCO_Code__c opco = new OPCO_Code__c();
		opco.Name = '059';
		opco.OPCO_Code__c = '059';
		opco.OPCO_Name__c = '059';
		insert opco;
		
		OPCO_Code__c opco1 = new OPCO_Code__c();
		opco1.Name = '011';
		opco1.OPCO_Code__c = '011';
		opco1.OPCO_Name__c = '011';
		insert opco1;
		
		Account acc = new Account();
		acc.Unique_Key__c = '059-100';
		acc.Vendor_No__c = '100';
		acc.OPCO_Code__c = opco.Id;
		acc.Name = 'Cora';
		acc.Vendor_Type__c = 'F';
		acc.Payment_Vendor_Terms__c = '059-NET25';
		acc.Vendor_No__c = '100';
		insert acc;
		
		Account acc1 = new Account();
		acc1.Unique_Key__c = '059-101';
		acc1.Vendor_No__c = '101';
		acc1.OPCO_Code__c = opco.Id;
		acc1.Name = 'Cora';
		acc1.Vendor_Type__c = 'M';
		acc1.Payment_Vendor_Terms__c = '059-NET25';
		acc1.Vendor_No__c = '101';
		insert acc1;
		
		User_Master__c um = new User_Master__c();
		um.Name = 'Cora';
		um.User_Name__c = 'cora@cora.com';
		um.Email__c = 'cora@cora.com';
		um.First_Name__c = 'cora';
		um.Status__c = 'Active';
		um.Entity__c = 'OpCo';
		um.OPCO_Code__c = opco.Id;
		um.Title_Type__c = 'Functional';
		insert um;
		
		Buyer_Code__c bc = new Buyer_Code__c();
		bc.OPCO_Code__c = opco.Id;
		bc.Name = '080';
		bc.Buyer_ID__c = '080';
		bc.Unique_Key__c = '059-080';
		bc.Buyer_User_ID__c = um.Id;
		insert bc;
		
		Purchase_Order__c  po = new Purchase_Order__c();
		po.PO_No__c = '100';
		po.OPCO_Code__c = opco.Id;
		po.Amount__c = 123;
		po.PO_Type__c = 'F';
		po.Vendor__c = acc.Id;
		po.Freight_Terms__c = 'NET25';
		po.Payment_Vendor_Terms__c = 'NET25';
		po.Unique_Key__c = '059-100';
		po.Buyer_ID__c = '080';
		insert po;

		list<Purchase_Order__c> Listpo = new list<Purchase_Order__c>();
		Listpo.add(po);
		
		Payment_Term__c pt = new Payment_Term__c();
		pt.Name = 'NET25';
		pt.OPCO_Code__c = opco.Id;
		pt.Discount_Days__c = 0;
		pt.Discount_Percentage__c = 0;
		pt.Transaction_Type__c = 'F';
		pt.Unique_Key__c = '059-NET25';
		pt.Due_Date__c = 0;
		pt.Due_Days__c = 25;
		pt.Due_Month__c = 0;
		insert pt;
		
		Invoice__c invobj = new Invoice__c();    
		invobj.Invoice_Date__c = Date.Today();
		invobj.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj.Total_Amount__c = 500.00;
		invobj.Current_State__c='Vouched';
		invobj.Invoice_Type__c = 'M';
		invobj.WorkType__c = 'CPAS';
		invobj.Invoice_Origin__c  ='test';
		invobj.Payment_Terms_Inv__c = '059-NET25';
		invobj.OPCO__c = '059';
		invobj.OPCO_Code__c = opco.Id;
		invobj.Purchase_Order__c = po.Id;
		insert invobj;

		set<Id> invoiceIds = new set<Id>();
		invoiceIds.add(invobj.Id);
		set<Id> setVendorIds = new set<Id>();
		setVendorIds.add(acc.Id);
		set<String> setVendorExtIds = new set<String>();
		setVendorExtIds.add(acc.Unique_Key__c);
	
		SyscoUtil.populateMasterData(invoiceIds,setVendorIds,setVendorExtIds);
		SyscoUtil.populateUserMaster();
		SyscoUtil.populateVendorMaster(setVendorIds);
		Set<String> allPurchaseOrders = new Set<String>();
		allPurchaseOrders.add('059-100');
		SyscoUtil.populatePurchaseOrders(allPurchaseOrders);
		Set<String> setPaymentTermExtId = new Set<String>();
		setPaymentTermExtId.add('059-NET25');
		SyscoUtil.getPaymentTerms(setPaymentTermExtId);
		set<String> key = new set<String>();
		key.add('059-080');
		SyscoUtil.populateBuyerCode(key);
	}
}