public class VendorInquiryFieldSetController {

	@AuraEnabled
	public static EsmHelper.dataHelperRecord getFields() {
		String objName = 'CaseManager__c';
		String fsname = 'VendorInquiry';
		List<EsmHelper.Columns> cols = new List<EsmHelper.Columns> ();
		String selectedRows = UtilityController.getAllSelectedFields(cols);
		CaseManager__c CaseObj = new CaseManager__c();

		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objName);
		Schema.DescribeSObjectResult describe = targetType.getDescribe();
		Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
		Schema.FieldSet fs = fsMap.get(fsName);
		List<Schema.FieldSetMember> fieldSet = fs.getFields();
		List<FieldSetMember> fset = new List<FieldSetMember> ();
		String ReadOnlyField = VendorInquiryFieldSetController.getReadOnlyFields();
		for (Schema.FieldSetMember f : fieldSet) {
			EsmHelper.Columns col = new EsmHelper.Columns();
			col.name = f.getFieldPath();
			if (String.valueOf(f.getType()) == 'REFERENCE')
			{
				col.reference = 'NAME';
				col.type = 'reference';
			}
			col.readonly = False;
			if (ReadOnlyField.contains(col.name))
			{
				col.readonly = True;
			}
			col.required = false; 
			if (f.getRequired())
			{
				col.required = True;
			}
			cols.add(col);
		}
		EsmHelper.dataHelperRecord dh = UtilityController.getWrapperFromJson(CaseObj, objName, cols);
		System.debug('Dh11111---' + dh);

		return dh;
	}

	public static String getReadOnlyFields() {
		try
		{
			String objName = 'CaseManager__c';
			String fsname = 'VendorInquiryReadOnly';
			String ReadOnlyFields = CommonFunctionController.getReadOnlyFields(objName, fsname, null);
			return ReadOnlyFields;
		}
		catch(Exception e)
		{
			system.debug('-----error----' + e.getlinenumber() + ':' + e.getcause());
			return null;
		}
	}

	@AuraEnabled
	public static VendorInquiryFieldSetMember.UnameMail getUsernameEmail() {
		VendorInquiryFieldSetMember.UnameMail ue = new VendorInquiryFieldSetMember.UnameMail();

		//System.debug('Username -- '+ UserInfo.getName());
		//System.debug('User Email -- '+ UserInfo.getUserEmail());
		ue.UserName = UserInfo.getName();
		ue.Email = UserInfo.getUserEmail();
		return ue;
	}
	// Updated By Somnath start CASP2-119

	@AuraEnabled
	public static String getInvoicenumber(String invoiceid) {
		System.debug('invoiceid--- ' + invoiceid);
		//Select id,Name from invoice__c where id='a173700000019eVAAQ'
		if (invoiceid != null)
		{
			Invoice__c cm = [Select id, Name, Vendor__c from invoice__c where id = :invoiceid];
			String invoicenm = cm.Name;
			System.debug('invoicenm -- ' + invoicenm);
			return invoicenm;
		}
		return null;
	}
	@AuraEnabled
	public static String saveCaseData(CaseManager__c CaseObj, List<String> docIDs) {
		try
		{
			//[Start] Case manager insert data
			// ------- Vendor Inquiry sharing Issue ---- Added by Shital Rathod----start--- 
			List<String> setListData = new List<String> ();
			if (CaseObj.Invoice__c != null)
			{
				Invoice__c inv = [select id, name, Vendor__c from Invoice__c where id = :CaseObj.Invoice__c];
				System.debug('inv----- ' + inv.Vendor__c);
				// ------- Vendor Inquiry sharing Issue ---- Added by Shital Rathod----end--- 
				CaseObj.put('Vendor__c', inv.Vendor__c);
			}
			else {
				String uid = UserInfo.getUserId();
				List<user> userlist = new List<user> ();
				userlist = [select id, name, contactId, contact.Name, account.Name, account.OPCO_Code__c from user where id = :uid];
				if (userlist != null && userlist.size() > 0)
				{
					CaseObj.put('Vendor__c', userlist[0].accountId);
					System.debug('Vendor__c ' + userlist[0].accountId);
				}
			}
			String startToken = ESMConstants.SUBJECT_START_IDENTIFIER;
			String endToken = ESMConstants.SUBJECT_END_IDENTIFIER;
			String emailSubject = (String) CaseObj.get(Schema.CaseManager__c.Subject__c);
			String emailBody = (String) CaseObj.get(Schema.CaseManager__c.Comments__c);
			System.debug('CaseObj ------ ' + CaseObj);
			upsert CaseObj;
			System.debug('name of case ' + CaseObj.Name);

			//[End] Case manager insert data

			System.debug('New ID -- ' + CaseObj.Id);

			//[Start] Email Message insert data
			String ctid = CaseObj.Id;
			System.debug(' ctid 110 ' + ctid + ' , ' + emailSubject + ' , ' + emailBody + ' , ' + UserInfo.getName() + ' , ' + UserInfo.getUserEmail());
			EmailMessage[] newEmail = new EmailMessage[0];
			newEmail.add(new EmailMessage(Subject = emailSubject,
			                              TextBody = emailBody,
			                              HtmlBody = emailBody,
			                              FromName = UserInfo.getName(),
			                              FromAddress = UserInfo.getUserEmail(),
			                              Incoming = true,
			                              RelatedToId = ctid,
			                              status = '0'));
			System.debug('newEmail---- ' + newEmail);
			insert newEmail;
			//[End] Email Message insert data



			//Added By Niza -- Start-- CASP2-134
			List<ContentDocumentLink> attachmentListTemp = new List<ContentDocumentLink> ();
			List<ContentDocumentLink> attachmentListTempCloneForCase = new List<ContentDocumentLink> ();
			List<ContentDocumentLink> attachmentListTempCloneForEmail = new List<ContentDocumentLink> ();
			System.debug('---docids' + docIDs.size());
			if (docIDs.size() > 0) {

				attachmentListTemp = [SELECT ContentDocument.Owner.Name, ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c, ContentDocument.Id, ContentDocument.ParentId, ContentDocument.Title, ContentDocument.FileType, ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id, ContentDocument.LastModifiedDate, ContentDocument.FileExtension, ContentDocument.ContentSize, ShareType FROM ContentDocumentLink where ContentDocument.Id IN :docIDs order by SystemModstamp desc];
				System.debug('-----------' + attachmentListTemp.size());
				for (ContentDocumentLink temp : attachmentListTemp) {
					System.debug('-----' + temp.LinkedEntityId);
				}

				if (attachmentListTemp != Null || !attachmentListTemp.isEmpty()) {
					System.debug('attachmentListTemp -- ' + attachmentListTemp);
					for (ContentDocumentLink temp : attachmentListTemp) {
						// For Case
						ContentDocumentLink cdl1 = new ContentDocumentLink();
						cdl1 = temp.clone();
						System.debug('cd1' + cdl1);
						cdl1.LinkedEntityId = CaseObj.Id;
						attachmentListTempCloneForCase.add(cdl1);

						/* // For Email
						  ContentDocumentLink  cdl2= new ContentDocumentLink();
						  cdl2=  temp.clone();
						  cdl2.LinkedEntityId = newEmail[0].Id;
						  attachmentListTempCloneForEmail.add(cdl2);  */

						System.debug('attachmentListTempCloneForCase ----- ' + attachmentListTempCloneForCase);
						// System.debug('attachmentListTempCloneForEmail ----- '+attachmentListTempCloneForEmail); 
					}
				}


				// For Case
				if (attachmentListTempCloneForCase != NULL || !attachmentListTempCloneForCase.isEmpty()) {
					System.debug('attachmentListTempCloneForCase -- ' + attachmentListTempCloneForCase);
					insert attachmentListTempCloneForCase;
				}

				/* 
				  // For Email 
				  if(attachmentListTempCloneForEmail != NULL || !attachmentListTempCloneForEmail.isEmpty()){
				  System.debug('attachmentListTempCloneForEmail -- '+attachmentListTempCloneForEmail);
				  insert attachmentListTempCloneForEmail;
				  }  
				 
				  //Added By Niza -- End--  CASP2-134 */

			}

			//[Start] Send Email
			string query = 'SELECT Id,Vendor__c,Name,Subject__c, SuppliedEmail__c FROM CaseManager__c WHERE Id =:ctid Limit 1';
			CaseManager__c ct = Database.query(query);
			System.debug('ct ------ ' + ct);
			List<String> toList = new List<String> ();

			//System.debug('ct -- '+ct);
			//String mailBody = System.Label.Case_Created_Manually_Email + ct.Name;

			//--------- Updated(Commented) by Shital Rathod JIRA-CASP2-153 Changes Start |Duplicate Interaction while Creating Inquiry ----
			/*String mailBody = emailBody;
			  if (String.isNotBlank(ct.SuppliedEmail__c))
			  {
			  toList.add(ct.SuppliedEmail__c);
			  //sendSingleMail(null, toList, null, ct.Subject__c + ':' + startToken + ct.Name + endToken, mailBody, ct.Id);
			  String subject = ct.Subject__c + ':' + startToken + ct.Name + endToken;

			  List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
			  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			  mail.setToAddresses(toList);
			  mail.setSubject(subject);
			  mail.setHtmlBody(mailBody);
			  mail.setWhatId(ctid);
			  mails.add(mail);
			  Messaging.sendEmail(mails);
			  }*/
			//--------- Updated(Commented) by Shital Rathod JIRA-CASP2-153 Changes End |Duplicate Interaction while Creating Inquiry ----
			//[End] Send Email
			system.debug('---Try----' + ct.name + ':' + ct.id);

			return ct.Name;
		}
		catch(Exception ex) {
			system.debug('---Catch Block----');
			ExceptionHandlerUtility.writeException('VendorInquiryFieldSetController', ex.getMessage(), ex.getStackTraceString());
			return null;
		}
	}

	// Updated By Somnath end CASP2-119

	// Added By Niza -- CASP2-134
	@AuraEnabled
	public static string getUserID() {
		System.debug('userInfo.getUserId()' + userInfo.getUserId());
		return userInfo.getUserId();
	}

	// Added By Niza -- CASP2-134
	@AuraEnabled
	public static List<ContentDocument> newUploadedAttachments(String[] docIDs) {
		System.debug('In' + docIDs);
		List<ContentDocument> attList = new List<ContentDocument> ();
		attList = [SELECT Title, FileExtension FROM ContentDocument where Id IN :docIDs];
		System.debug('Out' + attList);
		return attList;
	}

	// Added By Niza -- CASP2-134
	@AuraEnabled
	public static boolean deleteAttachments(List<String> docuIDs) {
		system.debug('in loop---' + docuIDs);
		try {
			if (UtilityController.isDeleteable('ContentDocument', null))
			{
				delete[SELECT Id FROM ContentDocument where ID IN : docuIDs];
			}
		} catch(Exception e) {
			ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
		}
		return true;
	}

	// Added By Niza -- CASP2-134
	@AuraEnabled
	public static String getCustomSettingsDetails() {
		Sysco_SystemConfiguration__c confgset = Sysco_SystemConfiguration__c.getValues('ERPConfig');
		return confgset.Allow_File_Format__c;
	}

}