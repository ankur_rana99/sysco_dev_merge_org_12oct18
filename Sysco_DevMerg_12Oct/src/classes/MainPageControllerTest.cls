@isTest
public class MainPageControllerTest { 
    static string nameSpace = UtilityController.esmNameSpace;
    
	 public static testmethod void testmethod1()
	 {
         System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
         
	   List<ContentVersion> newContentVersion=new  List<ContentVersion>();
          ContentVersion cvu= new ContentVersion(
              versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body'),
             Is_Primary_Doc__c=false,
             Description='Primary=a1wm0000000tvRS',
              Origin='H',
              PathOnClient='abc.pdf',
              title='abc.pdf'
          );
         newContentVersion.add(cvu);

	 MainPageController.cloneWrapper clnwrp=new MainPageController.cloneWrapper();
	 Invoice__c inv=new Invoice__c();
	 inv.Amount__c=100;
	 insert inv;

	 List<Invoice__c> otherChargesList=new List<Invoice__c>();
	 otherChargesList.add(inv);

	 clnwrp.Invoice=inv;
	 clnwrp.attachment=newContentVersion;

	 Invoice_Line_Item__c invline=new Invoice_Line_Item__c();
	 invline.Amount__c=100;
	 insert invline;
	 List<Invoice_Line_Item__c> invLineItemList=new List<Invoice_Line_Item__c> ();
	 invLineItemList.add(invline);

	 Purchase_Order__c po=new Purchase_Order__c();
	 po.Amount__c=100;
	 insert po;

	List<Purchase_Order__c> puchaseOrder=new List<Purchase_Order__c> ();
	puchaseOrder.add(po);

	QualityCheck__c qchk=new QualityCheck__c();
	
	System_Configuration__c sysconfig1=new System_Configuration__c();
	sysconfig1.Module__c ='Profile';
	sysconfig1.Sub_Module__c ='Supplier_Profile_ID';
	sysconfig1.Value__c ='test';
	insert sysconfig1;
         
	Invoice_Line_Item__c[] invln;
	 MainPageController.getUserInfo();
	 MainPageController.getSObjectDetails('EDIT', inv.Id, nameSpace+'Invoice__c');
	 MainPageController.callOnChangeManager('OnChangeManager', inv, invLineItemList,puchaseOrder,'test', 'test1');
	 MainPageController.callOnChangeManager('OnChangeManager', inv, invLineItemList,puchaseOrder,'test', 'test1');
	 MainPageController.validateInvoiceData(inv, invLineItemList, otherChargesList, puchaseOrder, 'abc');
	MainPageController.SaveInvoiceData(inv, invLineItemList,puchaseOrder,invln,'1', 'test', newContentVersion, qchk);
	 MainPageController.getInvoiceDetails(inv.Id);
	 MainPageController.getAttachment(inv.Id);
//	 MainPageController.callOnLoadCustomClasscallOnChangeManager(inv,invLineItemList,puchaseOrder, 'test1',nameSpace+'Invoice__c');
	// MainPageController.SaveObjectData(inv, '1','abc', newContentVersion, qchk,nameSpace+'Invoice__c');
	// MainPageController.getCloneSobjectDetails(inv.Id,nameSpace+'Invoice__c');
	 
	 }
	 public static testmethod void testmethod2()
	 {
         System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
         
	   List<ContentVersion> newContentVersion=new  List<ContentVersion>();
          ContentVersion cvu= new ContentVersion(
              versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body'),
             Is_Primary_Doc__c=false,
             Description='Primary=a1wm0000000tvRS',
              Origin='H',
              PathOnClient='abc.pdf',
              title='abc.pdf'
          );
         newContentVersion.add(cvu);

	 MainPageController.cloneWrapper clnwrp=new MainPageController.cloneWrapper();
	 Invoice__c inv=new Invoice__c();
	 inv.Amount__c=100;
	 insert inv;

	 List<Invoice__c> otherChargesList=new List<Invoice__c>();
	 otherChargesList.add(inv);

	 clnwrp.Invoice=inv;
	 clnwrp.attachment=newContentVersion;

	 Invoice_Line_Item__c invline=new Invoice_Line_Item__c();
	 invline.Amount__c=100;
	 insert invline;
	 List<Invoice_Line_Item__c> invLineItemList=new List<Invoice_Line_Item__c> ();
	 invLineItemList.add(invline);

	 Purchase_Order__c po=new Purchase_Order__c();
	 po.Amount__c=100;
	 insert po;

	List<Purchase_Order__c> puchaseOrder=new List<Purchase_Order__c> ();
	puchaseOrder.add(po);

	QualityCheck__c qchk=new QualityCheck__c();
	
	System_Configuration__c sysconfig1=new System_Configuration__c();
	sysconfig1.Module__c ='Invoice_Processing';
	sysconfig1.Sub_Module__c ='Approval_Required_For_Additional_Charges';
	sysconfig1.Value__c ='true';
	insert sysconfig1;

	CaseManager__c cm=new CaseManager__c();
	cm.Amount__c=100;
	insert cm;
	

	 MainPageController.getUserInfo();
	//MainPageController.getSObjectDetails('EDIT', inv.Id, nameSpace+'Invoice__c', inv);
	 MainPageController.callOnChangeManager('OnChangeConfig', inv, invLineItemList,puchaseOrder,'test','test1');
	 MainPageController.callOnChangeManager('test', inv, invLineItemList,puchaseOrder,'test', 'test1');
	 MainPageController.validateInvoiceData(inv, invLineItemList, otherChargesList, puchaseOrder, nameSpace);
	// MainPageController.SaveInvoiceData(inv, invLineItemList,poList, Invoice_Line_Item__c[] otherChargesList,String pageMode, String primaryDocJsonAtt, List<ContentVersion> NewAttachmentsAtt, QualityCheck__c qcObject);
	 MainPageController.getInvoiceDetails(inv.Id);
	 MainPageController.getAttachment(inv.Id);
	// MainPageController.callOnLoadCustomClasscallOnChangeManager(inv,invLineItemList,puchaseOrder, 'test1',nameSpace+'Invoice__c');
	// MainPageController.SaveObjectData(cm,'1','abc', newContentVersion, qchk,nameSpace+'CaseManager__c');
	// MainPageController.getCloneSobjectDetails(inv.Id,nameSpace+'Invoice__c');
	 
	 }
}