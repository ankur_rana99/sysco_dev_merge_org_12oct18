@isTest
public class TemplateHelperTest{
    public static testMethod void testFirst(){
        TemplateHelper.EmailToCaseTemplateResponse EmailToCaseTemplateResponseWrap = new TemplateHelper.EmailToCaseTemplateResponse();
        EmailToCaseTemplateResponseWrap.body = 'abc';
        EmailToCaseTemplateResponseWrap.subject = 'text';
        CaseManager__c cm = new CaseManager__c();
        cm.Amount__c = 100;
        insert cm ;     

        EmailToCaseNotificationManager.EmailToCaseNotification emailToCaseNotification=new EmailToCaseNotificationManager.EmailToCaseNotification();

        EmailTemplate em = [select id, name from EmailTemplate limit 1];
       system.debug('@@@ email template name is :' +em);
        emailToCaseNotification.TemplateId=em.id;
        
      
        TemplateHelper.mergeTemplate(emailToCaseNotification.TemplateId,cm.Id);
        
    }
}