@isTest
public class UtilityControllerTest {
    public static string esmNamespace = UtilityController.esmNamespace;
    public static testMethod void testFirst(){
        system.assert(true,true);
        UtilityController.ResponseofUser ResponseofUserWrap = new UtilityController.ResponseofUser();
        ResponseofUserWrap.userId = '1';
        ResponseofUserWrap.sessionId = '1';
        UtilityController.getUserSession();

        Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';

         insert invConf;
        
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;


         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         Invoice__c invobj = new Invoice__c();    
         invobj.Invoice_Date__c = Date.Today();
         invobj.Net_Due_Date__c = Date.Today().addDays(60);    
         invobj.Total_Amount__c = 500.00;
         invobj.Current_State__c='Vouched';
         System.debug('invobj1' + invobj);
         insert invobj;
         List<Invoice__c> InvList = new List<Invoice__c>();
         InvList.add(invobj);
         UtilityController.isFieldAccessible('Invoice__c','Current_State__c');
         UtilityController.getCreateExceptionMessage('Invoice__c','Current_State__c');
         UtilityController.getCreateExceptionMessage('Invoice__c');
         UtilityController.getAccessExceptionMessage('Invoice__c','Current_State__c');
         UtilityController.getAccessExceptionMessage('Invoice__c');
         UtilityController.getUpdateExceptionMessage('Invoice__c');
         UtilityController.getUpdateExceptionMessage('Invoice__c','Current_State__c');
         UtilityController.getDeleteExceptionMessage('Invoice__c');
         UtilityController.getObjectCreateExceptionMessage('Invoice__c');
         UtilityController.getObjectAccessExceptionMessage('Invoice__c');
         UtilityController.getObjectUpdateExceptionMessage('Invoice__c');
         UtilityController.getObjectDeleteExceptionMessage('Invoice__c');
         UtilityController.getUniqueRamdomId('1000','2000');

         UtilityController.convertAmtCurrency(1000.0,'test');
         UtilityController.getPackagedFieldName('Current_State__c');
         UtilityController.getWithNameSpace('Current_State__c');
         UtilityController.getValidFieldSetName('Invoice__c','Current_State__c');

         UtilityController uc = new UtilityController();

         uc.isSupplierUser();
         UtilityController.getNewSobject('Invoice__c');

         CaseManager__c cm = new CaseManager__c();
         cm.Amount__c = 100;
         cm.Comments__c = 'test';
         //cm.User_Action__c='create';
         //cm.Current_State__c='Start';
        // cm.Last_Response_Date__c='2018-06-22 11:25:56';

         insert cm ;
         System.debug('cm---' +cm);
          UtilityController.createCaseApex(cm);
//         UtilityController.createCaseApexnew(cm, null, null);
         UtilityController.updateCaseApex(cm);
         UtilityController.updateOwner(cm);
         UtilityController.getCaseById(cm.Id);
        
        UtilityController.getAllFieldSet('test');
        UtilityController.getLookupValue('Invoice__c','test');
        UtilityController.getDependentOptionsImpl('Invoice__c','test','test');

        ContentVersion cv = new ContentVersion();
        cv.Title = 'genpact-logo';
        cv.ContentDocumentId = cv.Id;
        cv.PathOnClient = 'Demopdf.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        insert cv;

        //System.debug ('cv---' +cv);

        String fromMail='hitakshi.patel@sftpl.com';
        String[] toMail = new List<String>();
        toMail.add('abc@test.com');
        toMail.add('abc1@test.com');

        String[] ccMail = new List<String>();
        ccMail.add('xyz@test.com');
        ccMail.add('xyz1@test.com');

        String subject='Case Query';
        String msg='This is Demo';


        UtilityController.getAttachment(cv.Id);
        UtilityController.plmLabels();
        UtilityController.sendSingleMail(fromMail,toMail,ccMail,subject,msg,cm.Id);
        //UtilityController.getAttachment('1');
        UtilityController.saveTheFile(cm,'abc.png','abc','test',false);
        //UtilityController.appendToFile(cm.Id,'test');
        //UtilityController.saveTheChunk(cm,'abc.png','abc','test',cm.Id,false);
        

        EsmHelper.Columns colsWrap = new EsmHelper.Columns();
        colsWrap.Criteria = null;
        colsWrap.requiredCriteria = null;
        colsWrap.name ='abc';
        colsWrap.reference = 'invoice';

        List<EsmHelper.Columns> listEsmHelper = new List<EsmHelper.Columns>();
        listEsmHelper.add(colsWrap);
        UtilityController.getAllSelectedFields(listEsmHelper);
        UtilityController.getDescribeFieldResultList('Invoice__c','invoice');
        UtilityController.getFieldsetMemberList('Invoice__c','invoice');
        UtilityController.getObjectMetadata('Invoice__c');
        UtilityController.getFieldsMetadata('Invoice__c');
        UtilityController.getHelperColumnsForWrapper('colsWrap');
        UtilityController.getWrapperFromJson(InvList,'Invoice__c',listEsmHelper);
        UtilityController.getWrapperFromJson(null,'Invoice__c',listEsmHelper,'abc.txt');
        //UtilityController.getWrapperFromJson(null,'Invoice__c',listEsmHelper);

        Map<String, String> objMap = new Map<String, String> ();
        objMap.put('abc','xyz');
        UtilityController.getWrapperFromJson(InvList,'Invoice__c',listEsmHelper,objMap);
        
    
        list<CaseManager__c> listCm = new list<CaseManager__c>();
        listCm.add(cm);
        UtilityController.sendNotification(listCm);
        UtilityController.FileWrapper FileWrapper1 = new UtilityController.FileWrapper();
        FileWrapper1.fileName = 'abc.txt';
        FileWrapper1.content = 'test';
        FileWrapper1.dataURL='/abc.txt';
        FileWrapper1.fileType ='txt';
        FileWrapper1.fileExt = 'abc';
        FileWrapper1.isPrimary = false;

    /*  UtilityController.FieldSetResponse FieldSetResponseWrap = UtilityController.FieldSetResponse();
        FieldSetResponseWrap.fieldMembers
        */
        UtilityController.isFieldUpdateable('Invoice__c','Current_State__c');
        UtilityController.isFieldCreateable('Invoice__c','Current_State__c');
        UtilityController.isDeleteable('Invoice__c','Current_State__c');
        try{
            throw new CustomCRUDFLSException('UtilityControllerTestException');
        }
        catch(Exception ex){
            UtilityController.customDebug(ex, 'UtilityControllerTest');
        }
        DOA_Setting__c ds = new DOA_Setting__c();
        //ds.Name = 'DOAS-00000';
        ds.Amount_Field__c = 'Total_Amount__c';
        ds.DOA_Invoice_Field_Mapping__c = '{"'+'Transactions_or_Areas__c" : "'+'Transaction_Area__c", "'+'Function_Area__c" : "'+'Function_Area__c"}';
        ds.First_Approver_Selection_Field__c = 'Requestor_Buyer__c';
    
        insert ds;
        System.debug('ds---' +ds);

        Set<String> invEntityset = new Set<String>();
        invEntityset.add('test');
        Set<Id> invCurAppset = new Set<Id>();
        invCurAppset.add(ds.Id);
        UtilityController.getDOAMatrixList(InvList,'test',ds,'pending',invEntityset,invCurAppset);
        //service.SessionHeader.sessionId = Page.SessionId.getContent().toString().trim();
        
        //UtilityController.GetPicklistValuesBasedOnRecordType('Invoice__c','RecordType',null);
        // ESMMetadataService.MetadataPort service = new ESMMetadataService.MetadataPort();
       // service.SessionHeader = new ESMMetadataService.SessionHeader_element();
        //service.SessionHeader.sessionId = strSessionId;
        //UtilityController.getFields('Invoice__c','invoice__c',cm,cm.Id);
        //UtilityController.FieldSetResponse FieldSetResponseWrap = UtilityController.FieldSetResponse();
        //FieldSetResponseWrap.
        UtilityController.getFieldSetName('Create','test','test','test','/abc','developer');
        UtilityController.getLookupNameApex('Invoice__c',null);
        //ContentVersion cv = new ContentVersion();
        //cv.Title = 'genpact-logo';
        //cv.Id = '068m0000000BGUDAA4';
        //cv.FileType = 'PNG';
        //cv.FileExtension = png;
        //System.debug('cv---' +cv);
        //insert cv;

        //System.debug ('cv1---' +cv);

    
      }
    
    }