/*
* Authors      :  Atul Hinge
* Date created :  18/08/2017
* Purpose      :  used as a abstraction layer over Salesforce objects.
* Dependencies :  QueryBuilder.cls
* JIRA ID      :  None
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
public  abstract with sharing class SobjectSelector {
    /*
        Authors: Atul Hinge
        Purpose: override getSObjectType to provide service to a perticular object
    */
    abstract Schema.SObjectType getSObjectType();
    private Set<String> relationShipFields;
    private Set<String> relationShipNames;
   // abstract List<Schema.SObjectField> getSObjectFieldList();
   /*
        Authors: Atul Hinge
        Purpose: Call this method to select object record based on Set of Id           
    */
    public List<SObject> selectSObjectsById(Set<Id> idSet){
        return Database.query(buildQuerySObjectById());
    }
    /*
        Authors: Atul Hinge
        Purpose: Call this method to select object record whereclause 
                 pass only clause like : (cond1 AND Cond3 or cond2)          
    */
    public List<SObject> selectSObjectsByClause(String clause){
        return Database.query(buildQuerySObjectByClause(clause));
    }
      /*
        Authors: Atul Hinge
        Purpose: Call this method to select all object record 
                           
    */
    public List<SObject> selectSObjectsByAll(){
        return Database.query(buildQuerySObjectByAll());
    }
    /*
        Authors: Atul Hinge
        Purpose: to build Query/internal use only          
    */
    private String buildQuerySObjectById(){
        QueryBuilder qb=newQueryBuilder().setCondition('id in :idSet');
        return qb.query();
    }
    /*
        Authors: Atul Hinge
        Purpose: to build Query for where clause/internal use only          
    */
    private String buildQuerySObjectByClause(String clause){
        QueryBuilder qb=newQueryBuilder().setCondition(clause);
        return qb.query();
    }
     /*
        Authors: Atul Hinge
        Purpose: to build Query for where clause/internal use only          
    */
    private String buildQuerySObjectByAll(){
        QueryBuilder qb=newQueryBuilder();
        return qb.query();
    }
    /*
        Authors: Atul Hinge
        Purpose: to build Query          
    */
    public QueryBuilder newQueryBuilder(){
        QueryBuilder qb=new QueryBuilder(getSObjectType());
        qb.addRelationShipFields(relationShipFields);
        qb.addRelationShipQuery(relationShipNames);
        return qb;
    }
    /*
        Authors: Atul Hinge
        Purpose: to Relation Ship Fields         
    */
    public void addRelationShipFields(Set<String> relationShipFields){
        this.relationShipFields = relationShipFields;
    }
    /*
        Authors: Atul Hinge
        Purpose: to Relation Ship Queries 
                 pass here set of relationalship name not api name
    */
    public void addRelationShipQueries(Set<String> relationShipNames){
        this.relationShipNames = relationShipNames;
    }    
}