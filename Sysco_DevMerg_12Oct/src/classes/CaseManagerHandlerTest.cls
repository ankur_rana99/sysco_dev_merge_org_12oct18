@isTest
public class CaseManagerHandlerTest { 
    
   
    
    public static testmethod void testJob() { 
     List<CaseManager__c> newCaseTracker= ESMDataGenerator.createCases(2);
     List<CaseManager__c> oldCaseTracker= ESMDataGenerator.createCases(2);
     newCaseTracker[0].Current_State__c='Ready For Processing';
     newCaseTracker[0].OwnerId='005m0000001EF5iEAG';
     newCaseTracker[0].User_Action__c='Process';
        
     newCaseTracker[1].Current_State__c='Ready For Processing';
     newCaseTracker[1].OwnerId='005m0000001EF5iEAG';
     newCaseTracker[1].User_Action__c='Process';
     
     oldCaseTracker[0].OwnerId='00Gm0000001EF5iEAG';
     oldCaseTracker[0].First_Touch__c=null;
     oldCaseTracker[0].User_Action__c='Accept';
     oldCaseTracker[0].Current_State__c='Ready For Processing';
        
     oldCaseTracker[1].OwnerId='00Gm0000001EF5iEAG';
     oldCaseTracker[1].First_Touch__c=null; 
     oldCaseTracker[1].User_Action__c='Accept';
     oldCaseTracker[1].Current_State__c='Ready For Processing';
     system.debug(':::::::'+newCaseTracker+':'+oldCaseTracker);
     CaseManagerHandler.updatePrevQueue(newCaseTracker,oldCaseTracker);
     CaseManagerHandler.populateReportRelatedFields(newCaseTracker,oldCaseTracker);
        
    }
   public static testmethod void testJob2() { 
     List<CaseManager__c> newCaseTracker= ESMDataGenerator.createCases(2);
     List<CaseManager__c> oldCaseTracker= ESMDataGenerator.createCases(2);
   
            
     CaseManager__c newCaseTracker2=newCaseTracker.get(1);
     newCaseTracker2.Current_State__c='Ready For QC';
        newCaseTracker2.OwnerId='005m0000001EF5iEAG';
      newCaseTracker2.User_Action__c='Do';
    
             
     CaseManager__c oldCaseTracker2=oldCaseTracker.get(1);
     oldCaseTracker2.OwnerId='005m0000001EF5iEAG';
     oldCaseTracker2.First_Touch__c=null;
     oldCaseTracker2.User_Action__c='QC Complete';
    
     CaseManagerHandler.updatePrevQueue(newCaseTracker,oldCaseTracker);
     CaseManagerHandler.populateReportRelatedFields(newCaseTracker,oldCaseTracker);
        
    }
     
}