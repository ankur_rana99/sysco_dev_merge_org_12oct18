@isTest
public class QueryBuilderTest{
		public sObject objectvar;
    	public String childrelationship;
    	public static testmethod void testFirst() {
	
         
		System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
           
		Invoice__c invobj = new Invoice__c();    
    	invobj.Invoice_Date__c = Date.Today();
    	invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	invobj.Total_Amount__c = 500.00;
        //invobj.Current_State__c='Vouched';
        invobj.Current_State__c = 'Pending Merchandise Processing';
    	 System.debug('invobj' + invobj);
        insert invobj;
         
		ID InvId = invobj.Id;
		String sObjName=InvId.getSObjectType().getDescribe().getName();
		Map<String, Schema.SObjectType> globalDescMap=Schema.getGlobalDescribe();
        Schema.SObjectType targetType=globalDescMap.get(sObjName);
		QueryBuilder Qb= new QueryBuilder(targetType);
        
   // }
    
    //public void setobjectvar(sObject s) {
        //objectvar = s;
       // DescribeSObjectResult describe = objectVar.getSObjectType().getDescribe();

        //for (Schema.ChildRelationship child : describe.getChildRelationships()) {
          //  System.debug('===================> relationship name ' + child.getRelationshipName());
           // System.debug('===================> field name ' + child.getField());
        //}
    //}

   // public sObject getobjectvar() {
      //  return objectvar;
   // }

    //public void setchildrelationship(String s) {
    //    childrelationship = s;
    ///}

//public String getchildrelationship() {
      //  return childrelationship;
   // }
	//Set<String> relationShipfieldName = new	Set<String>();
    //relationShipfieldName.add(invobj);
    //QueryBuilder Qb1= new QueryBuilder(targetType);
    //Qb1.addField(Qb1);    
        }
}