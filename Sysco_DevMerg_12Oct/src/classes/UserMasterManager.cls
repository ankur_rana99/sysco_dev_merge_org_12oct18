public with sharing class UserMasterManager implements Queueable {
    private Map<String,String> mapNewOldUserName;
 
    public UserMasterManager(Map<String,String> mapNewOldUser){
        this.mapNewOldUserName = mapNewOldUser;
    }
    
    public void insertUpdateBuyerUserfuture(){
        
        Map<string,User_Master__c> mapUserNameBuyerUser = new Map<string,User_Master__c>();
        Map<string,User> mapUserNameUser = new Map<string,User>();
        list<User_Master__c> lstBuyerUserToUpsert = new list<User_Master__c>();
        set<string> userNameSet = new Set<string>();
        User_Master__c buyrUserObj;
        User u ;
         
        list<User_Master__c> BuyerUserlst = [Select User_Name__c,Email__c,First_Name__c,Last_Name__c,Middle_Initial__c,Status__c,Supervisor__c,User_Type__c from User_Master__c where User_Name__c in: mapNewOldUserName.values()];

        for(User_Master__c  buyer :BuyerUserlst){
            mapUserNameBuyerUser.put(buyer.User_Name__c,buyer);
        }
        
        list<User> lstUser = [Select Username,FirstName,LastName,Email,IsActive,User_Type__c from User where Username in : mapNewOldUserName.keySet()];
        
        for(User user :lstUser){
            mapUserNameUser.put(user.Username,user);
        }
    
        for (String newUserName : mapNewOldUserName.keySet()){
            if(userNameSet.add(newUserName)){
                u = mapUserNameUser.get(newUserName);
                if(null != mapNewOldUserName.get(newUserName) && mapUserNameBuyerUser.containsKey(mapNewOldUserName.get(newUserName)) && null != mapUserNameBuyerUser.get(mapNewOldUserName.get(newUserName))){
                 buyrUserObj =  mapUserNameBuyerUser.get(mapNewOldUserName.get(newUserName));
                }
                else{
                    buyrUserObj = new User_Master__c();
                }
                
                buyrUserObj.Name = u.FirstName != null ? u.FirstName+' '+u.LastName : u.LastName ;
                buyrUserObj.Email__c = u.Email;
                buyrUserObj.First_Name__c = u.FirstName;
                buyrUserObj.Last_Name__c = u.LastName;
                buyrUserObj.Status__c = u.IsActive ? 'Active' : 'InActive';
                buyrUserObj.User_Name__c = u.Username; 
                buyrUserObj.User_Type__c = u.User_Type__c;
                lstBuyerUserToUpsert.add(buyrUserObj);
                
            }
            
        }
        
        System.debug('=====lstBuyerUserToUpsert===='+lstBuyerUserToUpsert);
        if(lstBuyerUserToUpsert.size() > 0){
            upsert lstBuyerUserToUpsert;
        }
    }
    
    public void execute(QueueableContext context) {
        insertUpdateBuyerUserfuture();      
    }
    
}