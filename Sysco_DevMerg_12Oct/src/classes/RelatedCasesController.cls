/**
Authors: Mohit Garg
Date created: 13/02/2018
Purpose: To return related Cases for a Case
Dependencies: RelatedCases.cmp
----------------------------------------------------------------
Modifications:
                Date:  
                Purpose of modification:
                Method/Code segment modified:
                
**/

public class RelatedCasesController {
    
    /*
    @AuraEnabled
    public static List<CaseManager__c> getRelatedCasesData(String mainCaseId){
        //public Id mainCaseId = mainCaseId;
        System.debug('mainCaseId : ' +mainCaseId);
        List<Relationship__c> relatedCase = [Select Sub_Case__c from Relationship__c where CaseManager__c =:mainCaseId];
        List<Id> relatedCaseIdList = new List<Id>();
        System.debug('relatedCase : ' +relatedCase);
        
        for(Relationship__c relationship : relatedCase){
            relatedCaseIdList.add(relationship.Sub_Case__c);
        }
         System.debug('relatedCaseIdList : ' +relatedCaseIdList);
        List<CaseManager__c> relatedCasesList = [Select Id, Name, Current_State__c, CreatedDate from CaseManager__c where Id IN :relatedCaseIdList];
        System.debug('relatedCasesList : ' +relatedCasesList); 
        return relatedCasesList;
    }
    */
    @AuraEnabled
    public static List<Relationship__c> getRelatedCasesData(String mainCaseId){
    	
        List<Relationship__c> relatedCasesList = [Select Sub_Case__r.Name,Type__c,Sub_Case__r.Current_State__c,Sub_Case__r.CreatedDate from Relationship__c where CaseManager__c =:mainCaseId];
        return relatedCasesList;
        
    }
}