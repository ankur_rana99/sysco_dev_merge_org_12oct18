public class OnChangeManager implements OnChangeConfig {
    
    //--- Changed by Bhupinder | JIRA:ESMPROD-275 | 09-08-2016 | Starts ---//
    public String prefix = UtilityController.esmNameSpace;
    //--- Changed by Bhupinder | JIRA:ESMPROD-275 | 09-08-2016  | Ends ---//
    
    /*public boolean execute(SObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<PO_Line_Item__c> pOLineItemList, String field, String stringifyVal, String childNameSpace){
        return null;
    }*/
    public boolean execute(SObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<PO_Line_Item__c> pOLineItemList, List<GRN_Line_Item__c> grnLineItemList, String field,String value, String childNameSpace,Map<object,object> extraParamMap) {
        //-- Added by Rahul(22-01-2015) --Start--//
        //-- deserialized whole component value from stringifyVal and add to Map<fieldApiName,fieldvalue > --//
        Map<String, Object> fieldApiToValueMap = new Map<String, Object>();
        try
        {
          String val = String.valueOf(fieldApiToValueMap.get(field));
          //-- Added by Rahul(22-01-2015) --End--//
  
          if(field != null && !field.equals('')) {
              CommonFunctionController cfObj = new CommonFunctionController();
              if(field.equals(childNameSpace+'Purchase_Order__c')) { 
				childSobject.put('Invoice_Type__c','Service');
                 // val = 'Test Suplier';
                  if(val != null && !val.equals('')) {
                      String query = cfObj.getCreatableFieldsSOQL(prefix+'Purchase_Order__c','Name = :val');
                      List<Purchase_Order__c> purOdr= Database.query(query);
                      if(purOdr != null && purOdr.size() > 0) {
                          String ids = purOdr.get(0).Id;
                          query = cfObj.getCreatableFieldsSOQL(prefix+'PO_Line_Item__c',prefix+'Purchase_Order__c = :ids');
                          List<PO_Line_Item__c> tmp = Database.query(query);
                          //--- Added by Dilshad | JIRA:ESMPROD-128 | 16-10-2015 | Starts ---//
                          if(pOLineItemList != null){
                              pOLineItemList.clear();
                              pOLineItemList.addAll(tmp);
                              
                              /*Invoice_Configuration__c invConf = (Invoice_Configuration__c) extraParamMap.get('INVCONF');
                              List<sObject> lstPO = (List<sObject>) extraParamMap.get('lstPO');
                             // lstPO.add(purOdr.get(0));
                             childSobject.put('purchase_order__c',ids);
                             System.debug('childSobject.get(Purchase_order__c)' + childSobject.get('Purchase_order__c'));
                              CommonFunctionController.setDoGRNMatchFlag(childSobject,childNameSpace,prefix,invConf,lstPO,poLineItemList);*/
                              
                          }
                          if(invLineItemList != null){
                              invLineItemList.clear();
                          }
                          //--- Added by Dilshad | JIRA:ESMPROD-128 | 16-10-2015 | Ends ---//
                          //--- Changed by Dilshad | JIRA:ESMPROD-36 | 02-09-2015 | Starts ---//
                          //--- added by Dilshad : 26-08-2015 ---//
                          if(grnLineItemList != null && childSobject.get(childNameSpace+'Do_GRN_Match__c') != null && Boolean.valueOf(childSobject.get(childNameSpace+'Do_GRN_Match__c'))){
                              Set<String> POSet = new Set<String>();
                              POSet.add(ids);
                              //--- Added by Dilshad | JIRA:APMA-16 | 27-11-2015 | Starts ---//
                              //getGrnLineItemList(grnLineItemList,POSet);
                              /*List<GRN_Line_Item__c> tmpGRN = CommonFunctionController.fillGRNLineItem(childSobject,childNameSpace,prefix,POSet);
                              System.debug('tmpGRN : ' + tmpGRN);
                              if(tmpGRN != null && tmpGRN.size() > 0){
                                  grnLineItemList.clear();
                                  grnLineItemList.addAll(tmpGRN);
                              }
                              system.debug('grnLineItemList:'+grnLineItemList);*/
                              //--- Added by Dilshad | JIRA:APMA-16 | 27-11-2015 | Ends ---//
                          }
                          //--- Changed by Dilshad | JIRA:ESMPROD-36 | 02-09-2015 | Ends ---//
                          
                      }
                  } 
              }
              else if(field.equals(childNameSpace+'Remit_To_Country_Territory__c')) { 
          childSobject.put('Remit_To_State_Province__c','aaaaaa');
        }
               else if(field.equals(childNameSpace+'Supplier_Profile__c')) { 
                 if(extraParamMap!= null && extraParamMap.containsKey('lstPO')) {
                     list<Purchase_Order__c> polst = (list<Purchase_Order__c>)extraParamMap.get('lstPO');
                     System.debug('@@@@@@ : ' + polst);
                     if(polst != null && polst.size() > 0) {
                         polst.clear();
                     }
                 }
                 if(invLineItemList != null) {
                      invLineItemList.clear();
                  }
                  if(pOLineItemList != null) {
                      pOLineItemList.clear();
                  }
                  if(grnLineItemList != null) {
                      grnLineItemList.clear();
                  }
                   if(val != null && !val.equals('')) {
                      String query = cfObj.getCreatableFieldsSOQL(prefix+'Supplier_Profile__c','Name =: val');
                      List<Supplier_Profile__c> lstSupp = database.query(query);
                      system.debug('@@lstSupp:'+lstSupp);
                      Supplier_Profile__c sp;
      
                       if(lstSupp != null && lstSupp .size() > 0){
                          sp = lstSupp.get(0);
                          if(sp != null && sp.getSObjectType().getDescribe().getName().equals('Supplier_Profile__c')){
                              childSobject.put('Supplier_Profile__c',sp.get('Id'));
                              /*
                              //childSobject.put('Vendor_Number_S__c',sp.get('Supplier_No__c'));
                              childSobject.put('Vendor_Name__c',sp.get('Name'));
                              
                              childSobject.put('Address1__c',sp.get('Remit_To_Address1__c'));
                              childSobject.put('Address2__c',sp.get('Remit_To_Address2__c'));
                              childSobject.put('Address3__c',sp.get('Remit_To_Address3__c'));
                              childSobject.put('City__c',sp.get('Remit_To_City__c'));
                              childSobject.put('State__c',sp.get('Remit_To_State__c'));
                              childSobject.put('Zip_Code__c',sp.get('Remit_To_Zip__c'));
                              childSobject.put('Vendor_Country__c',sp.get('Remit_To_Country__c'));
                              childSobject.put('Vendor_Phone__c','');
                              childSobject.put('Vendor_email__c','');
                              childSobject.put('Vendor_Contact__c','');
                              childSobject.put('Vendor_Fax__c','');
                              childSobject.put('Remmitance_Email_Address__c','');
      
                              childSobject.put('Tax_Explaination_code__c',sp.get('Tax_Explaination_code__c'));
                              childSobject.put('Payment_Instrument__c',sp.get('Payment_Instrument__c'));
                              childSobject.put('Payment_Terms__c',sp.get('Payment_Terms__c'));
                              childSobject.put('Currency__c',sp.get('Currency__c'));
                              childSobject.put('Hold_payment_flag__c',sp.get('Hold_payment_flag__c'));
                              childSobject.put('Tax_ID__c',sp.get('Tax_ID__c'));
                              
                              
                              childSobject.put('Tax_Country__c',sp.get('Tax_Country__c'));
                              childSobject.put('Tax_Type__c',sp.get('Tax_Type__c'));
                              childSobject.put('Tax_Number__c',sp.get('Tax_Number__c'));
                              
                              childSobject.put('Payment_by_voucher__c',sp.get('Payment_by_voucher__c'));
                              childSobject.put('Swift_Code__c',sp.get('Swift_Code__c'));
                              */
                          }
                      } 
                  } 
              }
			  else if(field.equals(childNameSpace+'Vendor__c')) {
				childSobject.put('Invoice_Type__c',null);
			  }
			  
          } 
        }
         //---Added by Bhupinder Mankotia|| 28-09-2016 || Starts.---//
         catch(Exception ex){
          if(ex!=null){
              UtilityController.customDebug(ex,'OnChangeManager');
                return null;
            }  
         } 
       //---Added by Bhupinder Mankotia|| 28-09-2016 || Ends.---// 
         return true;
    }
}