@isTest
public class SceduleEDItoPDFTest{
     public static testMethod void testFirst(){
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         invConf.EDI_PDF_Process_Current_State__c = 'Vouched,Ready for PDF creation';
         insert invConf;
     SceduleEDItoPDF.sched = '0 00 00 * * ?';
     SceduleEDItoPDF.scheduleMe();
     SceduleEDItoPDF sch = new SceduleEDItoPDF();
     sch.execute(null); 
    }

}