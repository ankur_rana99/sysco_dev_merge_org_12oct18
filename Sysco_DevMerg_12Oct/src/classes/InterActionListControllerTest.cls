@isTest
public class InterActionListControllerTest {
	public static testMethod void testFirst() {
        system.assert(true,true);
		InterActionListController.getInteractionByCase('1', 1, 2);
		InterActionListController.getContactsByInteraction('1', '1', 'b');
	
		CaseManager__c cm = new CaseManager__c();
		cm.Amount__c = 100;
		insert cm;

        EmailMessage e = new EmailMessage();
		e.Subject = 'text';
		insert e;

		List<EmailMessage> interList = new List<EmailMessage> ();
		interList.add(e);
		InterActionListController.makeReadMail(e.Id);
        
        ContentVersion cont = new ContentVersion();
		cont.Title = 'file.png';
		cont.PathOnClient = 'file.png';
		cont.VersionData = EncodingUtil.base64Decode('Test Blob');
		//cont.AttachmentType__c='Related'; 
		cont.Origin = 'H';
		cont.IsMajorVersion = true;
		cont.Title = 'Google.com';
		insert cont;

		ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :cont.Id];
		ContentDocumentLink cntent = new ContentDocumentLink();
		cntent.ContentDocumentId = testContent.ContentDocumentId;
		cntent.LinkedEntityId = e.Id;
		cntent.ShareType = 'V';
		cntent.Visibility = 'AllUsers';
		insert cntent;
        
		InterActionListController.getOwnerCases(cm.Id);
		InterActionListController.moveInteractionToCase(e.Id, cm.Id, '1');
		InterActionListController.getAttachments(e.Id);
	}
}