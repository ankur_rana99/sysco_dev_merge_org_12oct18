/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: Controller class of EmailToCaseRule.page.
  Dependencies: EmailToCaseRules.page,EmailToCaseSettingHelper.cls,EmailToCaseManager.cls
  -------------------------------------------------
  Modifications:
  Date: 30/10/2017
  Purpose of modification: Changed Notification logic
  Method/Code segment modified: Added new field "SendACKMail" on JSONValue.
 
*/
public with sharing class EmailToCaseRuleController {

    public String objectList { get; set; }
    public String assignServiceName {get;set;}
    public String unAssignServiceName {get;set;}
    public String hiddenService {get;set;}
    public List<System.SelectOption> unAssignServiceList{get;set;}
    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class write response.
      Dependencies: This method is called from "EmailToCaseRules.page".
     */
     public static Map<string,List<string>> operatorMap{
        get;set;
    }

    static{
        operatorMap=new Map<string,List<string>>();
        List<string> textOperator=new List<string>{'equals','not equal to','starts with','end with','contains','does not contain','contains in list'};
        List<string> numberOperator=new List<string>{'equals','not equal to','less than','greater than','less or equal','greater or equal'};
        List<string> pickListOperator=new List<string>{'equals','not equal to','contains in list'};
        List<string> booleanOperator=new List<string>{'equals','not equal to'};

        operatorMap.put('TEXT',textOperator);
        operatorMap.put('NUMBER',numberOperator);
        operatorMap.put('CURRENCY',numberOperator);
        operatorMap.put('DOUBLE',numberOperator);
        operatorMap.put('PICKLIST',pickListOperator);
        operatorMap.put('BOOLEAN',booleanOperator);
        operatorMap.put('EMAIL',textOperator);
        operatorMap.put('PHONE',textOperator);
        operatorMap.put('STRING',textOperator);
        operatorMap.put('REFERENCE',new List<string>{'equals','not equal to','contains in list'});
        operatorMap.put('TEXTAREA',new List<string>{'contains','does not contain'});
    }
    public class RuleResponse {
        public List<EmailToCaseSettingHelper.RuleField> ruleFields { get; set; }
        public List<EmailToCaseSettingHelper.FieldInfo> caseTrackerFields { get; set; }
        public List<EmailTemplate> emailTemplates { get; set; }
        public List<OrgWideEmailAddress> orgWideAddresses { get; set; }
        public List<EmailToCaseSettingHelper.objectsAvailaible> ObjectList { get; set; }
        public List<System.SelectOption> AllEmailServiceList { get; set; }
    }
    @RemoteAction
    public static List<SelectOption> getAllEmailServiceList(){
            List<SelectOption> options = new List<SelectOption>();
            String[] oldArr,newArr;
            Set<String> serviceStore = new Set<String>();
            List<Service_Mapping__c> services = [SELECT Email_Service__c FROM Service_Mapping__c];
            System.debug('===services '+services);
            if(!services.isEmpty()) {
                for(Service_Mapping__c oldobj : services) {
                    System.debug(oldobj.Email_Service__c);
                    if(oldobj.Email_Service__c!=null){
                    oldArr = oldobj.Email_Service__c.split(','); 
                    for(String obj : oldArr){
                        serviceStore.add(obj);
                        }
                    }
                }
            }
            for(EmailServicesAddress f : [ SELECT EmailDomainName,LocalPart FROM EmailServicesAddress])
            {
                if(!serviceStore.contains(String.valueOf(f.LocalPart + '@' +f.EmailDomainName))){
                options.add(new SelectOption(f.LocalPart + '@' +f.EmailDomainName,f.LocalPart + '@' +f.EmailDomainName));
                }
            }       
            return options;
    }
    @RemoteAction
     public static List<EmailToCaseSettingHelper.objectsAvailaible> getObjectList() {
        List<EmailToCaseSettingHelper.objectsAvailaible> objects = new List<EmailToCaseSettingHelper.objectsAvailaible> ();
        Schema.DescribeFieldResult fieldResult = Email_To_Case_Rule__c.Object_Name__c.getDescribe();
        //System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        EmailToCaseSettingHelper.objectsAvailaible obj =new EmailToCaseSettingHelper.objectsAvailaible();
        for (Schema.PicklistEntry f : plvalues)
        {
            obj =new EmailToCaseSettingHelper.objectsAvailaible();
            obj.label = f.getLabel();
            obj.apiName = f.getValue();
            objects.add(obj);

        }
        return objects;
           /* List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Email_To_Case_Rule__c.Object_Name__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            options.add(new SelectOption('--None--','--None--'));
            System.debug('f'+ple);
           for( Schema.PicklistEntry f : ple)
            {
                options.add(new SelectOption(f.getValue(), f.getLabel()));
            }
            System.debug('f'+options);       
            return options;*/
        }
   /*
      Authors: Chandresh Koyani
      Purpose: Get data for page load.
      Dependencies: This method is called from "EmailToCaseRules.page".
     */
     @RemoteAction
     public static List<String> pushListAssingEmailService(String record) {
        System.debug('record'+record);
        return null;
     }
     @RemoteAction 
     public static RuleResponse getInitialiseData() {
        RuleResponse response = new RuleResponse();
        response.ObjectList = getObjectList();
        return response;
     }
     
    @RemoteAction
    public static RuleResponse getInitData(string objectName) {
        
        RuleResponse response = new RuleResponse();
        response.ruleFields = getRuleFields();
        response.caseTrackerFields = getAllFields(objectName);
        response.emailTemplates = getAllEmailTemplate();
        response.orgWideAddresses = getAllOrgWideEmailAddress();
        return response;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all rules fields.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.RuleField> getRuleFields() {
        return EmailToCaseSettingHelper.getRuleFields();
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all case object fields.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.FieldInfo> getAllFields(String objectName) {
        List<EmailToCaseSettingHelper.FieldInfo> fieldNames = new List<EmailToCaseSettingHelper.FieldInfo> ();
        System.debug('--'+objectName);
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug('-field-'+sfield);
            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated() && !type.equalsIgnoreCase('REFERENCE')) {

                EmailToCaseSettingHelper.FieldInfo fieldInfo = new EmailToCaseSettingHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<EmailToCaseSettingHelper.Option> ();
                fieldInfo.type = type;
                fieldInfo.operator = EmailToCaseSettingHelper.OperatorMap.get(fieldInfo.Type);

                if (fieldInfo.Type.equalsIgnoreCase('PICKLIST')) {
                    List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                    for (Schema.PicklistEntry pickListVal : picklist) {
                        fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                    }
                }
                else if (fieldInfo.Type.equalsIgnoreCase('BOOLEAN')) {
                    fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option('True', 'True'));
                    fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option('False', 'False'));
                }
                fieldNames.add(fieldInfo);
            }
        }
        return fieldNames;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all templates for send mail.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailTemplate> getAllEmailTemplate() {
        try {
            List<EmailTemplate> emailTemplates = [Select id, Name from EmailTemplate where isactive=true and Folder.Name=:ESMConstants.EMAIL_TO_CASE_NOTIFICATION_TEMPLATE_FOLDER];
            return emailTemplates;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }
    @RemoteAction
    public static List<Service_Mapping__c> getObjetcDetails(String objectName) {
        try {
            List<Service_Mapping__c> objectDetail = [Select Email_Service__c from Service_Mapping__c where object_name__c=:objectName] ;
            if(!objectDetail.isEmpty()) {
            for(Service_Mapping__c mapping : objectDetail){
                if(mapping.Email_Service__c=='' || mapping.Email_Service__c==null){
                    return null;
                }
                else {
                    return objectDetail;
                    }   
                }
             }
            else {
                return null;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
        return null;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Geat all OrgWide email addresses for send mail.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<OrgWideEmailAddress> getAllOrgWideEmailAddress() {
        try {
            return[SELECT Address, Id FROM OrgWideEmailAddress limit 999];
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }



    /*
      Authors: Chandresh Koyani
      Purpose: Insert/Update Rules, Generate new order number based on last order number store in system.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static string saveEmailServiceMapping(string objectName,String listOfMapping) {
        try {
            System.debug('json deseri'+objectName+','+listOfMapping);
         /* String[] oldArr,newArr;
            System.debug('serviceId'+listOfMapping);
            Set<String> serviceStore = new Set<String>();
            List<String> serviceList = new List<String>();
            List<Service_Mapping__c> services = [SELECT Email_Service__c FROM Service_Mapping__c];
            System.debug('===services '+services);
            if(!services.isEmpty()) {
                for(Service_Mapping__c oldobj : services) {
                    oldArr = oldobj.Email_Service__c.split(','); 
                    for(String obj : oldArr){
                        serviceStore.add(obj);
                    }
                }
            }
            System.debug('===serviceStore '+serviceStore);
            newArr = listOfMapping.split(',');
            System.debug('==newArr'+newArr);
            for(String newobj : newArr){
                serviceList.add(newObj);
                System.debug('==newArr'+newobj+','+serviceList);
                    System.debug('newobj--'+serviceStore.contains(newobj.trim()));
                    if(serviceStore.contains(newobj.trim())){
                        return 'fail';
                    }
            }*/
            Service_Mapping__c serviceMapping = new Service_Mapping__c();
            List<Service_Mapping__c> objName = [SELECT Object_Name__c,ID,Email_Service__c FROM Service_Mapping__c where Object_Name__c=:objectName];
            System.debug('list '+objName);
            if(!objName.isEmpty()) {
                for(Service_Mapping__c obj : objName) {
                    obj.Email_Service__c = listOfMapping; 
                }
                upsert objName;
            }
            else {
            serviceMapping.Email_Service__c = listOfMapping;
            serviceMapping.Object_Name__c = objectName;
             upsert serviceMapping;
            }
            return 'SUCCESS';
            }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }
    @RemoteAction
    public static string saveEmailToCaseRules(EmailToCaseSettingHelper.EmailToCaseRule emailToCaseRules,String objectName) {
        try {
            System.debug('here');
            String str = JSON.serialize(emailToCaseRules.JSONValue);
            System.debug('json deseri'+str);
            Email_To_Case_Rule__c emailRules = new Email_To_Case_Rule__c();
            emailRules.Rule_Name__c = emailToCaseRules.RuleName;
            emailRules.JSON__c = str;
            emailRules.Type__c = emailToCaseRules.type;
            emailRules.IsActive__c = emailToCaseRules.isActive;
            emailRules.Object_Name__c = objectName;
            if (!string.isEmpty(emailToCaseRules.Id)) {
                emailRules.Id = emailToCaseRules.Id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from Email_To_Case_Rule__c where Type__c = :emailToCaseRules.Type and Object_Name__c=:objectName];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                emailRules.Order__c = nextOrder;
            }
            System.debug('emailRule'+emailRules);
            upsert emailRules;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get all email to case rules.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.EmailToCaseRule> getAllEmailToCaseRules(String objectName) {
        List<EmailToCaseSettingHelper.EmailToCaseRule> emailRules = new List<EmailToCaseSettingHelper.EmailToCaseRule> ();

        List<Email_To_Case_Rule__c> emailToCaseRules = [select id, Name, Rule_Name__c, JSON__c, Order__c, Type__c, IsActive__c from Email_To_Case_Rule__c where Object_Name__c=:objectName  order by Order__c limit 999];

        for (Email_To_Case_Rule__c emailToCaseRuleObj : emailToCaseRules) {
            EmailToCaseSettingHelper.JSONValue jsonField = (EmailToCaseSettingHelper.JSONValue) System.JSON.deserialize(emailToCaseRuleObj.JSON__c, EmailToCaseSettingHelper.JSONValue.class);
            EmailToCaseSettingHelper.EmailToCaseRule emailRuleObj = new EmailToCaseSettingHelper.EmailToCaseRule();
            System.debug('object '+jsonField);
            emailRuleObj.JSONValue = jsonField;
            emailRuleObj.ruleName = emailToCaseRuleObj.Rule_Name__c;
            emailRuleObj.order = Integer.valueOf(emailToCaseRuleObj.Order__c);
            emailRuleObj.type = emailToCaseRuleObj.Type__c;
            emailRuleObj.Id = emailToCaseRuleObj.Id;
            emailRuleObj.isActive = emailToCaseRuleObj.IsActive__c;
            emailRules.add(emailRuleObj);
        }

        return emailRules;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Remove email to case rules based on rule Id.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void removeEmailToCaseRules(string id) {
        try {
            List<Email_To_Case_Rule__c> emailToCaseRules = [select id from Email_To_Case_Rule__c where id = :id];
            delete emailToCaseRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            Email_To_Case_Rule__c rules = new Email_To_Case_Rule__c();
            rules.Id = id;
            rules.IsActive__c = isActive;
            update rules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change order of a rule.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType, string ruleType) {
        try {
            integer previousNextCount = 0;
            if (orderType.equalsIgnoreCase('UP')) {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<Email_To_Case_Rule__c> emailToCaseRules = [select id, Order__c from Email_To_Case_Rule__c where Type__c = :ruleType and(Order__c = :currentOrder OR Order__c = :previousNextCount)];
            for (Email_To_Case_Rule__c rule : emailToCaseRules) {
                if (rule.Order__c == currentOrder) {
                    rule.Order__c = previousNextCount;
                }
                else {
                    rule.Order__c = currentOrder;
                }
            }
            update emailToCaseRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }
    @RemoteAction
    public static List<QCHelper.FieldInfo> getCaseTrackerFields(String objectName) {
        List<User> allActiveUser = [select id, Name from User];

        List<QCHelper.FieldInfo> fieldNames = new List<QCHelper.FieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated()) {

                QCHelper.FieldInfo fieldInfo = new QCHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<QCHelper.option> ();
                fieldInfo.type = type;
                fieldInfo.operator = operatorMap.get(fieldInfo.type);

                if (fieldInfo.type == 'REFERENCE') {
                    string relatedObjectName = String.valueOf(dfield.getReferenceTo()).substringBetween('(', ')');
                    if (relatedObjectName.equalsIgnoreCase('USER')) {
                        fieldNames.add(fieldInfo);
                    }
                }
                else {
                    if (fieldInfo.type == 'PICKLIST') {
                        List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                        for (Schema.PicklistEntry pickListVal : picklist) {
                            fieldInfo.pickListValue.add(new QCHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                        }
                    }
                    else if (fieldInfo.type == 'BOOLEAN') {
                        fieldInfo.pickListValue.add(new QCHelper.option('True', 'True'));
                        fieldInfo.pickListValue.add(new QCHelper.option('False', 'False'));
                    }

                    fieldNames.add(fieldInfo);
                }
            }
        }
        return fieldNames;
    }
}