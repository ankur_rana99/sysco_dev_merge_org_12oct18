/*
 * Authors    : Parth Lukhi
 * Date created : 17/08/2017 
 * Purpose      : This InterActionListController is used for getting Interactions (EmailMessages)
 *            records,attachments objects for particular Case Id or Interaction Item
 * Dependencies :  NA
 * -------------------------------------------------
 * Modifications:
                Date:  09/11/2017 
                Purpose of modification:  PUX-235 : Getting Contact Name on interction 
                Method/Code segment modified: getContactListbyInter
 * -------------------------------------------------
 * Modifications:
                Date:  18/12/2017 
                Purpose of modification:  PUX-505 : Maintain attachment @ interaction level   
 * Modifications:
                Date:  23/01/2018 
                Purpose of modification:  PUX-620 : Is Private functionality with notes feature   
                
*/

public without sharing class InterActionListController {
    /*
     * Authors: Parth Lukhi
     * Purpose:  This method is used for fetchin Interaction List based in CaseId
     * Dependencies:  EmailMessage Object
     * 
     *   Start : This method is used for fetchin Interaction List based in CaseId
     */
    @AuraEnabled
    public static InteractionWrapper getInteractionByCase(String caseId, Integer pageNumber, Integer recordsToDisplay) {
        /***Start PUX-211  Added Read__c***/
        List < EmailMessage > interList;
        Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
        InteractionWrapper iw = new InteractionWrapper();
        try {
            //PUX-505 start
      
            //PUX-620
            String currentUserId = UserInfo.getUserId();
            //System.debug('currentUserId---'+ currentUserId);
            iw.emails = (List < EmailMessage > ) database.query('SELECT ActivityId,BccAddress,CcAddress,CreatedDate,Is_Note__c,Is_Private__c,FromAddress,FromName,HasAttachment,Headers,HtmlBody,Id,Incoming,IsDeleted,MessageDate,MessageIdentifier,ReplyToEmailMessageId,Status,Subject,SystemModstamp,TextBody,ThreadIdentifier,ToAddress,ValidatedFromAddress,'+ObjectUtil.getPackagedFieldName('Read__c')+','+ObjectUtil.getPackagedFieldName('Note_Message_f__c')+','+ObjectUtil.getPackagedFieldName('HasAttachment__c')+' FROM EmailMessage where RelatedToId =:caseId AND (Is_Private__c = false OR CreatedById =: currentUserId) order by CreatedDate desc  Limit ' + recordsToDisplay + ' OFFSET ' + offset);
            //PUX-505 End
            //System.debug('iw.emails---'+ iw.emails);
            iw.total = (database.countQuery('SELECT count() FROM EmailMessage where RelatedToId =:caseId AND (Is_Private__c = false OR CreatedById =: currentUserId) Limit 2010'));
            //System.debug('iw.total---'+ iw.total);
            iw.pageSize = recordsToDisplay;
            iw.page = pageNumber;
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }

        /***End PUX-211  Added Read__c***/
        return iw;

    }

    /*
        Authors: Rahul Pastagiya
        Purpose: To wrap pagination related values
        Dependencies : None
    */
    public class InteractionWrapper {
        @AuraEnabled public Integer pageSize {
            get;
            set;
        }
        @AuraEnabled public Integer page {
            get;
            set;
        }
        @AuraEnabled public Integer total {
            get;
            set;
        }
        @AuraEnabled public List < EmailMessage > emails {
            get;
            set;
        }
    }

    /*
     * Authors: Parth Lukhi
     * Purpose:   This method is used for fetching Attachment List based in CaseId or Interaction Id
     * Dependencies:  ContentDocumentLink,ContentDocumentLink Object
     * 
     * Start : This method is used for fetching Attachment List based in CaseId or Interaction Id
     */
    @AuraEnabled
    public static List<ContentDocumentLink> getAttachments(String id) {
        //String passedId;
    
        String docQuery='SELECT  ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, ContentDocument.Owner.Name,LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:id';       
       
       List<ContentDocumentLink> contList;
       try{
            contList=(List<ContentDocumentLink>)database.query(docQuery);
        }
        catch(Exception ex){
             ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        
        return contList;
    }
    /*
     * End : This method is used for fetching Attachment List based in CaseId or Interaction Id
     */

    /*
     * Authors: Parth Lukhi
     * Purpose:    PUX-235  This method is used for fetching ContactList  based on emailAdd
     * Dependencies:  Contact Object
     * 
     * Start : PUX-235  This method is used for fetching ContactList  based on emailAdd
     */
    @AuraEnabled
    public static List < Contact > getContactsByInteraction(String caseId, String interId, String emailAdd) {
        List < Contact > contactList = new List < Contact > ();
        String interactionId;
        List < Contact > contList;
        String emailParam = '';
        Integer cnt = 0;
        try {
            //List<String> emailList=emailAdd.split(',');
            List < String > emailList = emailAdd.split('[,]{1}[\\s]?');
           
            String docQuery = 'SELECT Email,Id,Name FROM Contact where Email In :emailList';
            contList = (List < Contact > ) database.query(docQuery);
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        return contList;
    }
    /*
     * End : PUX-235 This method is used for fetching ContactList  based on emailAdd
     */
    /*
     * Authors: Parth Lukhi
     * Purpose:    This method is used for marking interaction as read 
     * Dependencies:  EmailMessage Object
     * 
     * Start : This method is used for marking interaction as read 
     */
    @AuraEnabled
    public static boolean makeReadMail(String interactionId) {
     
        String docQuery = 'SELECT Id,'+ObjectUtil.getPackagedFieldName('Read__c')+' FROM EmailMessage where Id = :interactionId';
        List < EmailMessage > interList;
        try {
            interList = (List < EmailMessage > ) database.query(docQuery);

            if (interList.size() > 0) {
                
                EmailMessage email = interList.get(0);
                email.Read__c = true;
                update interList;
                return true;
            }
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        return false;
    }
    /*
     * End : This method is used for fetching ContactList  based on emailAdd
     */
    @AuraEnabled
    public static List<CaseManager__c> getOwnerCases(String interactionId) {
     		List<CaseManager__c> caseList = new List<CaseManager__c>();
			String uid = UserInfo.getUserId();
			List<string> excludeCaseStatus=new List<string>();
            excludeCaseStatus.add('Junk Case');
			List<string> completedCaseStatus=new List<string>();
            completedCaseStatus.add('Completed');
            completedCaseStatus.add('Rejected');
            completedCaseStatus.add('Pending For Archival');
            
        try {
        	caseList = Database.query('SELECT Id, Name FROM CaseManager__c where OwnerId = :uid and Current_State__c not in : excludeCaseStatus and Current_State__c Not in : completedCaseStatus Order By CreatedDate ASC Limit 50000');
        //[SELECT Id, Name FROM CaseManager__c where OwnerId =: userInfo.getUserId() Limit 50000];
            return caseList;
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        return caseList;
    }
    /*
     * End 
     */
    @AuraEnabled
        public static String moveInteractionToCase(String interactionId, String CaseId, String CaseNumber) {
            List<EmailMessage> emailMessages = new List<EmailMessage>();
			List<EmailMessage> newEmailMessages = new List<EmailMessage>();
			List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
			List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
            List<ContentDocumentLink> newContentDocumentLinksForCase = new List<ContentDocumentLink>();
            List<ContentDocumentLink> newContentDocumentLinksForCaseTemp = new List<ContentDocumentLink>();
			
			emailMessages = [Select Id, FromAddress, ToAddress, BccAddress, CcAddress, HtmlBody, Subject, TextBody, FromName,HasAttachment,Headers, CreatedDate, RelatedToId, Incoming,HasAttachment__c,Is_Note__c,Is_Private__c from EmailMessage Where Id =:interactionId ];
			contentDocumentLinks = [SELECT ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, ContentDocument.Owner.Name,LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:interactionId order by SystemModstamp desc];
            
            if(emailMessages.size()>0){
                for(EmailMessage email : emailMessages){
                    EmailMessage newEmailMsg = email.clone(false,true,true,true);
                    newEmailMsg.Subject = replaceCaseNumberSubject(email.Subject,CaseNumber,ESMConstants.SUBJECT_START_IDENTIFIER, ESMConstants.SUBJECT_END_IDENTIFIER, ESMConstants.REGEX_FOR_CASE_SEARCH_IN_SUBJECT);
                    newEmailMsg.RelatedToId = CaseId;
                    newEmailMessages.add(newEmailMsg);
                }
            } 
            Set<Id> contentDocumentIds = new Set<Id>();
            
			try{
                if(newEmailMessages.size()>0){
                    insert newEmailMessages;
                    if(ContentDocumentLinks.size() > 0){
                    	for(ContentDocumentLink cdLink : ContentDocumentLinks){
							if(newEmailMessages.size() == 1){
								ContentDocumentLink cntent = cdLink.clone(false,true,true,true);
			                    cntent.LinkedEntityId = newEmailMessages[0].Id;
			                    cntent.ShareType = 'V';
			                    newContentDocumentLinks.add(cntent);	
                                //contentDocumentForCase
                                ContentDocumentLink cntentForCase = cdLink.clone(false,true,true,true);
			                    cntentForCase.LinkedEntityId = CaseId;
			                    cntentForCase.ShareType = 'V';
                                contentDocumentIds.add(cntentForCase.ContentDocumentId);
			                    newContentDocumentLinksForCase.add(cntentForCase);
                                
							}
						}
						//update ContentDocumentLinks;
                    }
                    
                    List<ContentDocumentLink> existingContentDocLink = new List<ContentDocumentLink>();
                    existingContentDocLink = [SELECT ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, ContentDocument.Owner.Name,LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:CaseId AND ContentDocumentId in :contentDocumentIds order by SystemModstamp desc];
                    List<Id> existingDocID = new List<Id>();
                    
                    if(existingContentDocLink.size() >0){
                        for(ContentDocumentLink existingDocLnk : existingContentDocLink){
                            existingDocID.add(existingDocLnk.ContentDocumentId);
                    	}
                    }
                     
                    if(newContentDocumentLinksForCase.size() > 0){
                        for(ContentDocumentLink newCDLink : newContentDocumentLinksForCase){
                            if(!existingDocID.contains(newCDLink.ContentDocumentId)){
                                    newContentDocumentLinksForCaseTemp.add(newCDLink);
                            }else{
                                System.debug('found existing content document link on case');
                            }
                        }
                    }
                    if(newContentDocumentLinksForCaseTemp.size() > 0){
                    	insert newContentDocumentLinksForCaseTemp;
                    }
                    /*
                    if(newContentDocumentLinksForCase.size() > 0){
                    	insert newContentDocumentLinksForCase;
                    }
                    */
                    if(ContentDocumentLinks.size() > 0){
                    	delete ContentDocumentLinks;
                    }
                }
                if(emailMessages.size()>0){
                    delete emailMessages;
                }
                return 'Interaction moved successfully';
            } catch (Exception ex) {
                ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
                return ex.getMessage();
            }
            //return 'Error occured';
        }
    
    private static String replaceCaseNumberSubject(String sourceString,String replaceByString, String startIdentifier, String endIdentifier, String regex){
    	Pattern patternObj = Pattern.compile(regex);
		Matcher matcherObj = patternObj.matcher(sourceString);
		while (matcherObj.find()) {
			    String caseStr = matcherObj.group();
			    sourceString = sourceString.replace(caseStr,startIdentifier + replaceByString + endIdentifier);
            }
            return sourceString;	
    }
    
}