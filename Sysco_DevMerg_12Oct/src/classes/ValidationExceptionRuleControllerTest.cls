@isTest
public class ValidationExceptionRuleControllerTest{
static string nameSpace = UtilityController.esmNameSpace;
       public static testMethod void testFirst(){
       ValidationExceptionRuleController.getAllObjectsFields();
       ValidationExceptionRuleController.getErrorData();
       ValidationExceptionRuleController.getErrorTypeData(); 
       ValidationExceptionRuleController.getAdditionalRuleList();
       ValidationExceptionRuleController.getQualityCheckFieldset(); 
      
       ValidationExceptionRuleController.changeState('a0P3700000C8GikEAF',false); 
       ValidationExceptionRuleController.removeRule('1');
       Validation_Exception_Rule__c validexcrule=new Validation_Exception_Rule__c();
       validexcrule.Rule__c='{"criterias":[{"value":"","type":"PICKLIST","refField":"","operator":"not equal to","field":"Document_Type__c"}],"attributes":null}';
       validexcrule.Order__c=1;
       validexcrule.isActive__c=true;
       validexcrule.isCustom__c=false;
       validexcrule.Stop_Excecution__c=false;
       validexcrule.Error_Code__c='DUPLICATE_INVOICE';
       validexcrule.Error_Type__c='Validation';
       insert validexcrule;
       
       ValidationExceptionRuleController.getAllRules();
        ValidationExceptionRuleController.changeOrder(1,'UP');
       ValidationExceptionRuleController.changeOrder(1,'MP');
       List<ValidationHelper.Criteria> criteriahelper=new List<ValidationHelper.Criteria> ();
       Validation_Exception_Rule__c valexcrule=new Validation_Exception_Rule__c();
       valexcrule.AdditionalRules__c='{"criterias":null,"attributes":[{"additionalRuleValue":"10","additionalRuleField":"Tolerance_In_Percent"},{"additionalRuleValue":"100","additionalRuleField":"Tolerance_In_Amount"}]}';
       insert valexcrule;
             

       ValidationExceptionRuleController.getAdditionalRuleData(valexcrule.Id);
       ValidationExceptionRuleController.getCaseTrackerFields(nameSpace+'invoice__c');  
       
       
       
       ValidationHelper.JSONValue jsonField = (ValidationHelper.JSONValue) System.JSON.deserialize(valexcrule.AdditionalRules__c, ValidationHelper.JSONValue.class);

       ValidationHelper.ValidationRule validObject = new  ValidationHelper.ValidationRule();
       //validObject.Attribute validAttr= new  validObject.Attribute();   
        QCHelper.QCRule qcrule=new QCHelper.QCRule();
       ValidationExceptionRuleController.saveRule(qcrule, 'QC_Rule_Config__c');
       validObject.isActive=true;
       validObject.errorType='asd';
       validObject.errorCode='102';
       validObject.isStopExecution=false;
       validObject.isCustom=true;
       validObject.additionRuleName='123';
       ValidationExceptionRuleController.saveValidationRule(validObject,nameSpace+'invoice__c',validObject,validObject);
       }
    }