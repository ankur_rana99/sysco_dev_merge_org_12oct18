global with sharing class SharingWithVendorBatch implements Database.Batchable<SObject> {
    global String prefix = UtilityController.esmNameSpace;
    List<User> newUsers = new List<User>();
    List<User> allUsers = new List<User>();
    set<string> accountIdSet = new set<string>();
    global List<sObject> oldsharedData = new List<sObject>();
    global SharingWithVendorBatch(){
        
    }
    
    global List<sObject> start(Database.BatchableContext BC) {
        newUsers = [SELECT ID, AccountID, Sharing_Done__c FROM User WHERE AccountID != NULL AND Sharing_Done__c = false];
        if(newUsers.size() > 0) {
            for(User uObj : newUsers){
                accountIdSet.add(uObj.AccountID);
            }
            set<string> allUserIdSet = new set<string>();
            //allUsers = [SELECT ID, AccountID, Sharing_Done__c FROM User WHERE AccountID IN: accountIdSet];
            for(User uObj : [SELECT ID, AccountID, Sharing_Done__c FROM User WHERE AccountID IN: accountIdSet]){
                allUserIdSet .add(uObj.Id);
            }
            if(allUsers.size() > 0){
                String InvQuery = 'SELECT Id, UserOrGroupId FROM '+prefix+'Invoice__Share WHERE UserOrGroupId IN: '+allUserIdSet;
                String POQuery =  'SELECT Id, UserOrGroupId FROM '+prefix+'Purchase_Order__Share WHERE UserOrGroupId IN: '+allUserIdSet;
                String CMQuery = 'SELECT Id, UserOrGroupId FROM '+prefix+'CaseManager__Share WHERE UserOrGroupId IN: '+allUserIdSet;
                String PaymentQuery = 'SELECT Id, UserOrGroupId FROM '+prefix+'Payment__Share WHERE UserOrGroupId IN: '+allUserIdSet;
                getRecords(InvQuery);
                getRecords(POQuery);
                getRecords(CMQuery);
                getRecords(PaymentQuery);
            }
        }
        return oldsharedData;
    }
    global void execute(Database.BatchableContext bc, List<sObject> sharingList){}
    
    global void finish(Database.BatchableContext bc){}    
    
    public void getRecords(String Query){
        Database.querylocator siblingSharingData  = Database.getquerylocator(Query);
        Database.QueryLocatorIterator it =  siblingSharingData.iterator();
        while (it.hasNext())
        {
             sObject obj = (sObject)it.next();
             oldsharedData .add(obj);
        } 
        System.debug('oldsharedData ---'+oldsharedData.size());
    }
}