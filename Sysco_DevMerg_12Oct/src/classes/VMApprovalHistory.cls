global class VMApprovalHistory {
    @InvocableMethod(label = 'VMApprovalHistory' description = 'Captures the Vendor Maintenance Approval History')
    global static List<Vendor_Maintenance__c> checkUserDOA(List<Vendor_Maintenance__c> vmList) {
        try {
            set<id> VMIdSet = new set<id> ();
            for (Vendor_Maintenance__c vm : vmList) {
                VMIdSet.add(vm.Id);
            }
            Map<string, Approval_Detail__c> appMap = new Map<string, Approval_Detail__c> ();
            List<Approval_Detail__c> appList = [SELECT Id, Name, Approver_User__c, Approval_Cycle__c, Vendor_Maintenance__c, Approver_Action__c FROM Approval_Detail__c WHERE Vendor_Maintenance__c IN :VMIdSet];
            for (Approval_Detail__c appObj : appList) {
                appMap.put(appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c, appObj);
            }
            List<Approval_Detail__c> appDetUpdate = new List<Approval_Detail__c> ();
            List<Vendor_Maintenance__c> VMToUpdate = new List<Vendor_Maintenance__c>();
            for (Vendor_Maintenance__c vm : vmList) {
                if (vm.Current_Approver__c == null) {
                    Approval_Detail__c appObj = new Approval_Detail__c();
                    appObj.Vendor_Maintenance__c = vm.Id;
                    appObj.Status__c = 'Pending';
                    appObj.Approver_User__c = vm.Requestor_Buyer__c;

                    if (appList.size() > 0) {
                        appObj.Approval_Cycle__c = vm.Current_Cycle__c + 1;
                    } else {
                        appObj.Approval_Cycle__c = vm.Current_Cycle__c;
                    }
                    Vendor_Maintenance__c tmp = new Vendor_Maintenance__c();
                    tmp.Id = vm.Id;
                    //tmp.Approval_Status__c = 'Pending';
                    tmp.Current_Approver__c = vm.Requestor_Buyer__c;
                    VMToUpdate.add(tmp);
                    appDetUpdate.add(appObj);
                } else {
                    Approval_Detail__c appObj = appMap.get(vm.Current_Approver__c + '~' + integer.valueof(vm.Current_Cycle__c));
                    if (appObj != null) {
                        String action = (vm.User_Action__c != null ? (vm.User_Action__c == 'Approve' ? 'Approved' : (vm.User_Action__c == 'Reject' ? 'Rejected' : '')) : '');
                        appObj.Status__c = action;
                        appObj.Approver_Action__c = vm.User_Action__c;
                        appDetUpdate.add(appObj);
                    }
                }
            }
            if (appDetUpdate.size() > 0) {
                upsert appDetUpdate;
            }
            if(VMToUpdate.size() > 0){
                update VMToUpdate;
            }
        } catch(exception ex) {
            System.debug('Exception110');
            system.debug('error' + ex + '----' + ex.getLinenumber());
        }
        return null;
    }
}