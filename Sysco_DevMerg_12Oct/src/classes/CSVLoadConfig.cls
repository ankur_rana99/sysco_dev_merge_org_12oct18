global interface CSVLoadConfig {
    Map<String,Map<String,Map<String,string>>> execute(Map<object,object> extraParamMap);
}