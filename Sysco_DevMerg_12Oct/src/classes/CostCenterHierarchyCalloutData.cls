/*
Auther : Nilesh Patil
Created date : 8/22/2018
*/
Global Class CostCenterHierarchyCalloutData Implements database.Batchable < Sobject > ,Schedulable, Database.Stateful,Database.AllowsCallouts {

	Private SchedulableContext objSchedulableContext;
    Global CostCenterHierarchyCalloutData() {}

    Global List < CostCenterHierarchy__c  > start(Database.BatchableContext BC) {
		
        List < CostCenterHierarchy__c  > lstCostCenterHierarchy = new List < CostCenterHierarchy__c  > ();
        try {
            String 		dateFormatString 	= 'yyyy-MM-dd';
            Date 		today 				= Date.today();
            Datetime 	dt1 				= Datetime.newInstance(today.year(), today.month(), today.day());
            String 		tday 				= dt1.format(dateFormatString);
            Date 		previousday 		= Date.today() - 1;
            Datetime 	dt2 				= Datetime.newInstance(previousday.year(), previousday.month(), previousday.day());
            String 		prevday 			= dt2.format(dateFormatString);
            WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('CostCenterHierarchy');
			
            if (WEEndpoint.isfirstrun__c == 'Y') prevday = '';
            //START : Callout
            String XMLString;
            HttpRequest feedRequest = new HttpRequest();
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c);
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=2018-06-12&Ending_Prompt=2018-06-13');
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=2018-08-11');
			feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday); 
            System.debug('Endpoint : ' + WEEndpoint.Endpoint_URL__c);
            feedRequest.setMethod('GET');
            feedRequest.setTimeout(60000);
            Http http = new Http();
            HTTPResponse CostCenterHierarchyResponse = http.send(feedRequest);
            System.debug('CostCenterHierarchyResponse Body : '+CostCenterHierarchyResponse.getBody());
            //END : Callout
			
			//START : DOM
			XMLString 			= CostCenterHierarchyResponse.getBody();
            DOM.Document doc 	= new DOM.Document();
            doc.load(XMLString);
			System.debug('XMLString : ' + XMLString);
            DOM.XmlNode rootNode = doc.getRootElement();
            System.debug('rootNode size: ' + rootNode.getChildElements().size());
            if (rootNode.getNodeType() == DOM.XMLNodeType.ELEMENT) {
				
				// Local Variables/Instance 
				Map<String,String> 		mapHierarchy_Level;
				List<String> 			Hierarchy_Level_Name;
				List<String> 			Hierarchy_Level_ReferenceID;
				CostCenterHierarchy__c  costCenterHierarchy;
				
				for (Dom.XMLNode child: rootNode.getChildElements()) {
					System.debug('child.getName() : ' + child.getName());
					if (child.getName() == 'Report_Entry') {
						// Local Variables/Instance initialization only in Report_Entry xml element
						mapHierarchy_Level 			= new Map<String,String>();
						Hierarchy_Level_Name 		= new List<String>();
						Hierarchy_Level_ReferenceID = new List<String>();
						
						//Locally stored non repeating xml elements
						String Cost_Center		= Child.getChildElement('Cost_Center', 	Child.getNamespace()).getText();
                        String referenceID 		= Child.getChildElement('Reference_ID', Child.getNamespace()).getText();
                        String inactive 		= Child.getChildElement('Inactive', 	Child.getNamespace()).getText();
						String Cost_CenterName 	= Cost_Center;
						
						// Name field not be more than 80 char so reassign with 80 char
						if(Cost_CenterName.length() > 80 ) Cost_CenterName = Cost_CenterName.substring(0,80);
						
						//This loop will get all the xml elements in array (Locally stored repeated xml elements in array)
						for (Dom.XMLNode HLevel: child.getChildElements()) {
							if(HLevel.getName().contains('_Name'))				Hierarchy_Level_Name.add(HLevel.getText());
							else if(HLevel.getName().contains('_ReferenceID'))	Hierarchy_Level_ReferenceID.add(HLevel.getText());
						}
						
						//This loop will get all the elements in array & put in map (Locally stored in map as Key=Name & Value=ReferenceID)
						for (Integer i = 0; i<Hierarchy_Level_Name.size(); i++)  
						   mapHierarchy_Level.put(Hierarchy_Level_Name[i],Hierarchy_Level_ReferenceID[i]);
						
						System.debug('Hierarchy_Level RefId & Size -------> '+referenceID+ ' & ' + mapHierarchy_Level.size()); 
						
						//This loop will get all the xml elements from map & initialization object of costCenterHierarchy with complete data then finalise to DML 
						for(string KeyName :mapHierarchy_Level.keySet()){
							costCenterHierarchy = new CostCenterHierarchy__c (CostCenter_Name__c 	= Cost_Center, 
																			  Name 					= Cost_CenterName,
																			  CostCenter_RefId__c	= referenceID, 
																			  Inactive__c 			= inactive,
																			  Hierarchy_Name__c		= KeyName,
																			  Hierarchy_RefID__c	= mapHierarchy_Level.get(KeyName),
																			  Unique_Key__c   		= referenceID + '-' + mapHierarchy_Level.get(KeyName),		
																			  InputSource__c 		= 'Workday');
							lstCostCenterHierarchy.add(costCenterHierarchy); //DML Collector progress... in loop 
						}System.debug('costCenterHierarchy  ------->' + costCenterHierarchy);
					}
                }
            }System.debug('lstCostCenterHierarchy size ------->' + lstCostCenterHierarchy.size());
			//END: DOM
        } catch (exception e) {
            system.debug(e.getMessage());
        }return lstCostCenterHierarchy;
    }

	// Batch Execution process...
    global void Execute(Database.BatchableContext objBatchableContext, List < CostCenterHierarchy__c > lstCCenters) {
        try {
            system.debug('lstCCenters.size() ' + lstCCenters.size());
			if (lstCCenters.size() > 0 && !lstCCenters.isempty())
                upsert lstCCenters;
          }catch (exception e) {
            system.debug(e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC) {
		WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('CostCenterHierarchy'); 
		System.debug('WEEndpoint..... '+ WEEndpoint.isfirstrun__c);
         if(WEEndpoint.isfirstrun__c == 'Y'){
            System.debug('WEEndpoint.isfirstrun__c +'+ WEEndpoint.isfirstrun__c);
            WEEndpoint.isfirstrun__c = 'N';
            Update WEEndpoint;                       
        } 
	}

    global void execute(SchedulableContext objSchedulableContext) {
        CostCenterHierarchyCalloutData syscoCC = new CostCenterHierarchyCalloutData();
        Integer batchSize = 200;
        system.database.executebatch(syscoCC, batchSize);
    }
}