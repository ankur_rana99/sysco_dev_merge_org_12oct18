global class newUserSharingBatch implements Database.Batchable<sObject> {
    
    global String prefix = UtilityController.esmNameSpace;
    global Map<String,List<ID>> SiblingUserwithNewUserListMap;
    global List<sObject> returnSharingList = new List<sObject>();
    
    global newUserSharingBatch(){
        List<User> newUserList = new List<User>();
        List<User> siblingUserList = new List<User>();
        Map<ID,ID> accIdWithSiblingUserIdMap = new Map<ID,ID>();
        SiblingUserwithNewUserListMap = new Map<String,List<ID>>();
        Map<ID,List<ID>> accIdWithNewUserListMap = new Map<ID,List<ID>>();
        List<AsyncApexJob> jobDetail = new  List<AsyncApexJob>();
        String query;
        DateTime DateTimeValue;
        
        // Get User List after the last batch was successfully executed
        ID classID = [SELECT ID FROM ApexClass WHERE Name = 'newUserSharingBatch'].ID;
        jobDetail = [SELECT CompletedDate FROM AsyncApexJob WHERE Status = 'Completed' AND ApexClassID =: classID Order by CreatedDate DESC LIMIT 1];
        System.debug('jobDetail'+jobDetail);
        
        if(!jobDetail.isEmpty()){
            DateTimeValue = jobDetail[0].CompletedDate;
        }
        else{
            DateTimeValue = System.now();
        }
		query = 'SELECT ID, AccountID FROM User WHERE AccountID != NULL AND CreatedDate  >=: DateTimeValue'; 
        System.debug('query==='+query);
        Database.querylocator newUserInserted  = Database.getquerylocator(query);
        System.debug('newUserInserted'+newUserInserted);
        Database.QueryLocatorIterator it =  newUserInserted.iterator();
        while (it.hasNext())
        {
             User obj = (User)it.next();
             newUserList.add(obj);
        } 
        System.debug('newUserList'+newUserList);
        
        
        // Map Account ID vs List of Newly Created User
        for(User usr : newUserList) {
            System.debug('usr'+usr.ID);
            if(accIdWithNewUserListMap.containsKey(usr.AccountID)) {
    		List<Id> usersId = accIdWithNewUserListMap.get(usr.AccountID);
    		usersId.add(usr.Id);
    		accIdWithNewUserListMap.put(usr.AccountID, usersId);
        	} else {
        		  accIdWithNewUserListMap.put(usr.AccountID, new List<Id> { usr.ID });
        	}
        }
        
        siblingUserList = [Select Id,AccountID from User Where AccountID != NULL AND AccountID IN: accIdWithNewUserListMap.keySet() Order by CreatedDate LIMIT 1];
        System.debug('siblingUserList'+siblingUserList);
        
        
        IF(siblingUserList != Null || siblingUserList.size() > 0){
            //Map Account ID vs Sibling User
            for(User siblingUsr : siblingUserList) {
                accIdWithSiblingUserIdMap.put(siblingUsr.AccountID, siblingUsr.ID);   }
            
            //Map Sibling User vs List of Newly Created User
            for(ID accID : accIdWithSiblingUserIdMap.keySet()) {
                System.debug('accID'+accID);
                SiblingUserwithNewUserListMap.put(accIdWithSiblingUserIdMap.get(accID), accIdWithNewUserListMap.get(accID));    }
        } 
         System.debug('SiblingUserwithNewUserListMap-- constructor--'+SiblingUserwithNewUserListMap);   
                
    }         
    
    // Method Details: To get Query result into a list of sObject for all the Sharing Records
    public List<sObject> getRecords(String Query){
        Database.querylocator siblingSharingData  = Database.getquerylocator(Query);
        Database.QueryLocatorIterator it =  siblingSharingData.iterator();
        while (it.hasNext())
        {
             sObject obj = (sObject)it.next();
             returnSharingList.add(obj);
        } 
        System.debug('returnSharingList---'+returnSharingList.size());
        return returnSharingList;
    }
    
    
    global List<sObject> start(Database.BatchableContext BC) {
        System.debug('SiblingUserwithNewUserListMap--- in Start--'+SiblingUserwithNewUserListMap);
        List<String> siblingIDList =  new List<String>();
        for(String Key : SiblingUserwithNewUserListMap.Keyset()){
            siblingIDList.add('\''+Key+'\''); 
        }
        IF( !siblingIDList.isempty() ){
            String InvQuery = 'Select Id,UserOrGroupId From '+prefix+'Invoice__Share Where UserOrGroupId IN '+siblingIDList;
            String POQuery = 'Select Id,UserOrGroupId From '+prefix+'Purchase_Order__Share Where UserOrGroupId IN '+siblingIDList;
            String CMQuery = 'Select Id,UserOrGroupId From '+prefix+'CaseManager__Share Where UserOrGroupId IN '+siblingIDList;
            String PaymentQuery = 'Select Id,UserOrGroupId From '+prefix+'Payment__Share Where UserOrGroupId IN '+siblingIDList;
            getRecords(InvQuery);
            getRecords(POQuery);
            getRecords(CMQuery);
            getRecords(PaymentQuery);
            System.debug('returnSharingList--'+returnSharingList.size()+'--'+returnSharingList);
        }
         return returnSharingList; 
       
    }
 
    global void execute(Database.BatchableContext bc, List<sObject> sharingList){
        List<Invoice__Share> invList = new List<Invoice__Share>();
        List<Purchase_Order__Share> POList = new List<Purchase_Order__Share>();
        List<CaseManager__Share> CMList = new List<CaseManager__Share>();
        List<Payment__Share> PaymentList = new List<Payment__Share>();
        List<SObject> finalSharingList = new List<SObject>();
        System.debug('SiblingUserwithNewUserListMap-- exec'+SiblingUserwithNewUserListMap);
        Set<ID> sharingRecIds = new Set<ID>();
        for(sobject obj : sharingList){
            sharingRecIds.add(obj.ID);
           /* ID objID = obj.ID;
            String objName = String.valueOf(objID.getSObjectType());
            if(objName == prefix+'Invoice__Share'){
                invList.add((Invoice__Share)obj);
            }
            if(objName == prefix+'Purchase_Order__Share'){
                POList.add((Purchase_Order__Share)obj);
            }
            if(objName == prefix+'CaseManager__Share'){
                CMList.add((CaseManager__Share)obj);
            }
            if(objName == prefix+'Payment__Share'){
                PaymentList.add((Payment__Share)obj);
            } */
        }
        System.debug('sharingRecIds'+sharingRecIds);
        invList = [SELECT ID, ParentId,UserOrGroupId,AccessLevel,RowCause FROM Invoice__Share WHERE ID IN: sharingRecIds];
        POList = [SELECT ID, ParentId,UserOrGroupId,AccessLevel,RowCause FROM Purchase_Order__Share WHERE ID IN: sharingRecIds];
        CMList = [SELECT ID, ParentId,UserOrGroupId,AccessLevel,RowCause FROM CaseManager__Share WHERE ID IN: sharingRecIds];
        PaymentList = [SELECT ID, ParentId,UserOrGroupId,AccessLevel,RowCause FROM Payment__Share WHERE ID IN:sharingRecIds]; 
        System.debug('invList--'+invList.size()+'POList--'+POList.size()+'CMList--'+CMList.size()+'PaymentList--'+PaymentList.size());
    
        //Inv Share
        If(invList != NULL || !invList.isEmpty()){
            for(Invoice__Share invShare : invList){
                List<ID> newUserList = SiblingUserwithNewUserListMap.get(invShare.UserOrGroupId);
                for (Integer i=0; i < newUserList.size(); i++) {
                    sObject shareObj = new Invoice__Share();
                    shareObj.put('ParentId', invShare.ParentId);
                    shareObj.put('UserOrGroupId',newUserList[i]);
                    shareObj.put('AccessLevel', 'Read');
                    shareObj.put('RowCause', Schema.Invoice__Share.RowCause.Manual);
                    finalSharingList.add(shareObj);
                }
            }
        }
        
        //PO Share
        If(POList != NULL || !POList.isEmpty()){    
            for(Purchase_Order__Share poShare : POList){
                List<ID> newUserList = SiblingUserwithNewUserListMap.get(poShare.UserOrGroupId);
                for (Integer i=0; i < newUserList.size(); i++) {
                    sObject shareObj = new Purchase_Order__Share();
                    shareObj.put('ParentId', poShare.ParentId);
                    shareObj.put('UserOrGroupId', newUserList[i]);
                    shareObj.put('AccessLevel', 'Read');
                    shareObj.put('RowCause', Schema.Purchase_Order__Share.RowCause.Manual);
                    finalSharingList.add(shareObj);
                }
            }
        }
    
        //CM Share
        If(CMList != NULL || !CMList.isEmpty()){
            for(CaseManager__Share cmShare : CMList){
                List<ID> newUserList = SiblingUserwithNewUserListMap.get(cmShare.UserOrGroupId);
                for (Integer i=0; i < newUserList.size(); i++) {
                    sObject shareObj = new CaseManager__Share();
                    shareObj.put('ParentId', cmShare.ParentId);
                    shareObj.put('UserOrGroupId', newUserList[i]);
                    shareObj.put('AccessLevel', 'Read');
                    shareObj.put('RowCause', Schema.CaseManager__Share.RowCause.Manual);
                    finalSharingList.add(shareObj);
                }
            }
        }
        
        //Payment Share
        If(PaymentList != NULL || !PaymentList.isEmpty()){
            for(Payment__Share pymtShare : PaymentList){
                List<ID> newUserList = SiblingUserwithNewUserListMap.get(pymtShare.UserOrGroupId);
                for (Integer i=0; i < newUserList.size(); i++) {
                    sObject shareObj = new Payment__Share();
                    shareObj.put('ParentId', pymtShare.ParentId);
                    shareObj.put('UserOrGroupId', newUserList[i]);
                    shareObj.put('AccessLevel', 'Read');
                    shareObj.put('RowCause', Schema.Payment__Share.RowCause.Manual);
                    finalSharingList.add(shareObj);
                }
            }
        }
        
        System.debug('finalSharingList'+finalSharingList.size());
        IF(finalSharingList != NULL || !finalSharingList.isEmpty()){
            Database.insert(finalSharingList,false);
        }
        
   }    
 
    global void finish(Database.BatchableContext bc){}    
   
}