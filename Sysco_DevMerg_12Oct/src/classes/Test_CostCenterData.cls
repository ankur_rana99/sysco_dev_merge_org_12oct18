global class Test_CostCenterData implements database.Batchable < Sobject > , Schedulable, Database.Stateful, Database.AllowsCallouts {

    private SchedulableContext objSchedulableContext;
    public Map < String, String > mapOPCO;

    global Test_CostCenterData() {
        mapOPCO = new Map < String, String > ();
        for (OPCO_Code__c opco: [SELECT OPCO_Name__c, OPCO_Code__c, Id, Name FROM OPCO_Code__c]) {
            mapOPCO.put(opco.OPCO_Code__c, opco.Id);
        }
        System.debug(' mapOPCO...' + mapOPCO);
    }

    global List < Cost_Center__c > start(Database.BatchableContext BC) {
        List < Cost_Center__c > lctCostCenter = new List < Cost_Center__c > ();
        try {
            String dateFormatString = 'yyyy-MM-dd';
            Date today = Date.today();
            Datetime dt1 = Datetime.newInstance(today.year(), today.month(), today.day());
            String tday = dt1.format(dateFormatString);
            Date previousday = Date.today() - 1;
            Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(), previousday.day());
            String prevday = dt2.format(dateFormatString);
            WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('CostCenter');
            if (WEEndpoint.isfirstrun__c == 'Y') {
                prevday = '';
            }
            String XMLString;
            HttpRequest feedRequest = new HttpRequest();
            feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c);
            //feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + '2018-08-11');
            System.debug('endpoint===> ' + WEEndpoint.Endpoint_URL__c);
            feedRequest.setMethod('GET');
            feedRequest.setTimeout(10000);
            Http http = new Http();
            HTTPResponse CostCenterResponse = http.send(feedRequest);
            System.debug(CostCenterResponse.getBody());
            XMLString = CostCenterResponse.getBody();
            DOM.Document doc = new DOM.Document();
            doc.load(XMLString);
            DOM.XmlNode rootNode = doc.getRootElement();
            System.debug(' rootNode...' + rootNode);
            if (rootNode.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                for (Dom.XMLNode child: rootNode.getChildElements()) {
                    if (child.getName() == 'Report_Entry') {
                        String name = Child.getChildElement('name', Child.getNamespace()).getText();
                        String referenceID = Child.getChildElement('referenceID', Child.getNamespace()).getText();
                        String inactive = Child.getChildElement('Inactive', Child.getNamespace()).getText();
                        for (Dom.XMLNode cr: Child.getChildElements()) {
                            if (cr.getName() == 'Company_Restrictions') {
                                Dom.XMLNode[] Company_Restrictions = cr.getChildren();
                                String wid = Company_Restrictions[0].getText().trim();
                                String orgRef = Company_Restrictions[1].getText().trim();
                                String comRef = Company_Restrictions[2].getText().trim();

                                Cost_Center__c costCenter = new Cost_Center__c(OPCO_Code__c = mapOPCO.get(comRef), Name = name, Cost_Center_Name__c = name, Inactive__c = inactive, Reference_ID__c = referenceID, Input_Source__c = 'Workday', Company_Res_WID__c = wid, Company_Res_OReference_id__c = orgRef, Company_Res_CReference_id__c = comRef);
                                lctCostCenter.add(costCenter);
                            }
                        }
                    }
                }
            }
            System.debug('lctCostCenter------->' + lctCostCenter);
        } catch (exception e) {
            system.debug(e.getMessage());
        }
        return lctCostCenter;
    }

    global void Execute(Database.BatchableContext objBatchableContext, List < Cost_Center__c > lstCCenters) {
        system.debug('lstCCenters====execute=>' + lstCCenters);
        try {
            if (lstCCenters.size() > 0 && !lstCCenters.isempty()) {
                Schema.SObjectField externalId = Cost_Center__c.Fields.Reference_ID__c;
                Database.UpsertResult[] results = Database.upsert(lstCCenters, externalId, false);
            } else
                system.debug('No Records to Process');
        } catch (exception e) {
            system.debug(e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

    global void execute(SchedulableContext objSchedulableContext) {
        Test_CostCenterData syscoCC = new Test_CostCenterData();
        Integer batchSize = 2000;
        system.database.executebatch(syscoCC, batchSize);
    }


}