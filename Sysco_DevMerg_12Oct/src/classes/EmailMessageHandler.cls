/*
  Authors      :   Hiren SONI
  Date created :   08/03/2018
  Purpose      :   Helper class for EmailMessage object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
  Date:  
  Purpose of modification:  
  Method/Code segment modified: 
 */
public class EmailMessageHandler {
	/*
	  Authors: Rahul Pastagiya
	  Purpose: To calculate the unread emails for specific case and update case manager by that count
	  Dependencies : NA
	 */
	public static void calUnreadEmailCntAndUpdateCase(List<EmailMessage> newEmailMessage) {
		//To preapre set of all cases of EmailMessages in trigger.new list     
		Set<Id> caseIdSet = new Set<Id> ();
		Set<Id> caseIdSet1 = new Set<Id> ();
		for (EmailMessage em : newEmailMessage) {
			if (em.RelatedToId != null) {
				caseIdSet.add(em.RelatedToId);
				caseIdSet1.add(em.RelatedToId);
			}
		}
		List<sObject> ctList1 = new List<sObject> ();
		//To get aggregate all interactions count Case wise 
		AggregateResult[] groupedResults1 = [SELECT COUNT(Id), RelatedToId FROM EmailMessage where RelatedToId in :caseIdSet and Is_Note__c = false GROUP BY RelatedToId];
		for (AggregateResult ar : groupedResults1) {
			Id caseId = (ID) ar.get('RelatedToId');
			Schema.SObjectType token = (caseId).getSobjectType();
			String objName = token.getDescribe().getName();
			caseIdSet1.remove(caseId);
			Integer count = (INTEGER) ar.get('expr0');
			sObject ct = UtilityController.getNewSobject(objName);
			ct.Id = caseId;
			if (objName == 'CaseManager__c')
			{
				ct.put('Interactions_Count__c', count);
			}

			ctList1.add(ct);
		}
		update ctList1;
		//To get aggregate unread count Case wise 
		AggregateResult[] groupedResults = [SELECT COUNT(Id), RelatedToId FROM EmailMessage where RelatedToId in :caseIdSet and Read__c = false GROUP BY RelatedToId];
		List<sObject> ctList = new List<sObject> ();
		for (AggregateResult ar : groupedResults) {
			Id caseId = (ID) ar.get('RelatedToId');
			Schema.SObjectType token = (caseId).getSobjectType();
			String objName = token.getDescribe().getName();
			caseIdSet.remove(caseId);
			Integer count = (INTEGER) ar.get('expr0');
			sObject ct = UtilityController.getNewSobject(objName);
			ct.Id = caseId;
			ct.put('Unread_Email_Count__c', count);
			ctList.add(ct);
		}

		//To set unread count 0 for those cases whose record not found in above query
		for (Id caseId : caseIdSet) {
			Schema.SObjectType token = (caseId).getSobjectType();
			String objName = token.getDescribe().getName();
			sObject ct = UtilityController.getNewSobject(objName);
			ct.Id = caseId;
			ct.put('Unread_Email_Count__c', 0);
			//ct.put('Interactions_Count__c',0);
			ctList.add(ct);
		}
		update ctList;
	}

	/*
	  Authors: Rahul Pastagiya
	  Purpose: To mark all out bound messages as read
	  Dependencies : NA
	 */
	public static void markOutboundMessageAsRead(List<EmailMessage> newEmailMessage) {
		System.debug('newEmailMessage ' + newEmailMessage);
		for (EmailMessage em : newEmailMessage) {
			if (!em.Incoming) {
				em.Read__c = true;
			}
		}
	}
}