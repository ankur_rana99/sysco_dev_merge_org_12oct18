@isTest
public class SyscoGetProfitCenterDataTest{
    
    public static testMethod void testFirst(){    
        WorkDay_EndPoints__c sysWED=new WorkDay_EndPoints__c();
        sysWED.Name='ProfitCenter';
        sysWED.isfirstrun__c = 'Y';
        sysWED.Endpoint_URL__c='callout:Sysco_Profit_Center/ccx/service/customreport2/sysco4/ISU%20FDM%20Business%20Segment/Foundation_Data_-_Custom_Org_-_Profit_Center';
        insert sysWED;
                 
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());
        
        SyscoGetProfitCenterData getpc  = new SyscoGetProfitCenterData();
        Integer batchSize = 200; 
        system.database.executebatch(getpc,batchSize);       
        Test.StopTest();  
    }    
}