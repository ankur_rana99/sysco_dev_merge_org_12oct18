public class SyscoUtil {
    public static map<String, List<sObject>> populateMasterData(set<Id> invoiceIds, set<Id> setVendorIds, set<String> setVendorExtIds){
        map<Id, Id> mapInvoiceIdWithPOId = new map<Id, Id>();
        map<String, List<sObject>> mapSobject = new map<String, List<sObject>>();
        set<Id> setPOId = new set<Id>(); 
        mapSobject.put('VM', populateVendorMasterExt(setVendorIds,setVendorExtIds));
        mapSobject.put('IPOD', populateInvoicePODetail(invoiceIds));
        mapSobject.put('PT', populatePaymentTerms());

        for(Invoice_PO_Detail__c invPO : populateInvoicePODetail(invoiceIds)){
            setPOId.add(invPO.Purchase_Order__c);
            mapInvoiceIdWithPOId.put(invPO.Invoice__c, invPO.Purchase_Order__c);
        }
        if(setPOId != null){
            mapSobject.put('PO', populatePurchaseOrders(setPOId));
            mapSobject.put('GRN', populateGRNs(setPOId));
        }
        System.debug('mapSobject===> ' + mapSobject);
        return mapSobject;
    }

    public static List<Account> populateVendorMaster(Set<Id> setVendorIds){
        return [Select Id, Name,Vendor_No__c,Unique_Key__c,Connection_Name__c,OPCO_Code__r.Name,OPCO_Code__r.OPCO_Name__c,Payment_Vendor_Terms__c, User_Name__c, Email_Id__c, Address1__c, Address2__c, Address3__c, Vendor_Type__c, Address4__c, City__c, Country__c, Zip_Postal_Code__c, Remit_To_Name__c, Remit_To_Address1__c, 
                Remit_To_Address2__c, Remit_To_Address3__c, Remit_To_Address4__c, Remit_To_City__c, Remit_To_Country_Territory__c, Remit_To_State_Province__c, Remit_To_Zip_Postal_Code__c 
                FROM Account 
                WHERE Id IN: setVendorIds];
    }
    public static List<Account> populateVendorMasterExt(Set<Id> setVendorIds,Set<String> setVendorExtIds){
        return [Select Id, Name,Vendor_No__c,Unique_Key__c,Connection_Name__c,OPCO_Code__r.Name,OPCO_Code__r.OPCO_Name__c,Payment_Vendor_Terms__c, User_Name__c, Email_Id__c, Address1__c, Address2__c, Address3__c, Vendor_Type__c, Address4__c, City__c, Country__c, Zip_Postal_Code__c, Remit_To_Name__c, Remit_To_Address1__c, 
                Remit_To_Address2__c, Remit_To_Address3__c, Remit_To_Address4__c, Remit_To_City__c, Remit_To_Country_Territory__c, Remit_To_State_Province__c, Remit_To_Zip_Postal_Code__c 
                FROM Account 
                WHERE Unique_Key__c IN: setVendorExtIds OR Id IN: setVendorIds];
    }

    public static List<Invoice_PO_Detail__c> populateInvoicePODetail(Set<Id> invoiceIds){
        return [SELECT Id, Invoice__c, Purchase_Order__c 
        FROM Invoice_PO_Detail__c 
        WHERE Invoice__c IN: invoiceIds];
    }
    
    // METHOD NOT IN USE - Kuldeep Singh [19 April 2018]
    public static List<User_Master__c> populateUserMaster(){
        return [Select Id, Name, First_Name__c, Buyer_ID__c, Last_Name__c, User_Name__c, Email__c,Status__c,User_Type__c 
        FROM User_Master__c 
        WHERE Status__c = 'Active' AND User_Type__c ='Requestor/Buyer' LIMIT 50000 ];
    }

    public static List<Purchase_Order__c> populatePurchaseOrders(Set<Id> setPOId){
        return [SELECT  Amount__c, Anticipated_Date__c, Vendor__r.Payment_Vendor_Terms__c, Vendor__r.VENDOR_TYPE__c, Buyer_ID__c, Buyer_No__c, Created_Date__c, Currency__c, Due_Date__c,  
                Freight_Terms__c, Freight_Vendor_No__c, Freight_Vouched_Status__c, GRN_Match_Required__c, Id, Invoice_Date__c, Line_Count__c, Merchandise_Vendor_No__c, FOB__c, 
                Merchandise_Vouched_Status__c, ML_Flag__c, Name, OC_Number__c, OPCO_Code__c,OPCO_Code__r.Name, Payment_Term__c, Payment_Vendor_Terms__c, PO_Date__c, PO_No__c, PO_Type__c ,  
                Status__c, Total_Amount__c, Vendor_Name__c, Vendor__c 
                FROM Purchase_Order__c 
                WHERE Id IN: setPOId 
                ORDER BY CreatedDate DESC LIMIT 50000];
    }
    
    public static List<Purchase_Order__c> populatePurchaseOrders(Set<String> allPurchaseOrders){
        return [SELECT  Amount__c, Anticipated_Date__c, Vendor__r.Payment_Vendor_Terms__c, Vendor__r.VENDOR_TYPE__c, Buyer_ID__c, Buyer_No__c, Created_Date__c, Currency__c, Due_Date__c,  
                Freight_Terms__c, Freight_Vendor_No__c, Freight_Vouched_Status__c, GRN_Match_Required__c, Id, Invoice_Date__c, Line_Count__c, Merchandise_Vendor_No__c, FOB__c, 
                Merchandise_Vouched_Status__c, ML_Flag__c, Name, OC_Number__c, OPCO_Code__c,OPCO_Code__r.Name, Payment_Term__c, Payment_Vendor_Terms__c, PO_Date__c, PO_No__c, PO_Type__c ,  
                Status__c, Total_Amount__c, Vendor_Name__c, Vendor__c 
                FROM Purchase_Order__c 
                WHERE Name IN: allPurchaseOrders 
                ORDER BY CreatedDate DESC LIMIT 50000];
    }
    
    public static List<GRN__c> populateGRNs(Set<Id> setPOId){
        return [SELECT Id, Purchase_Order__c, GRN_Date__c 
        FROM GRN__c 
        WHERE Purchase_Order__c IN: setPOId ORDER BY GRN_Date__c DESC LIMIT 50000];
    }

    public static List<Payment_Term__c> populatePaymentTerms(){
        return [SELECT Id, Name, Discount_Percentage__c, Net_Days_To_Pay__c, Description__c, Vendor__c, Unique_Key__c, Opco_Code__c, Discount_Days__c, Due_Days__c, Due_Month__c, Due_Date__c 
                FROM Payment_Term__c LIMIT 50000];
    }
    public static List<Payment_Term__c> getPaymentTerms(Set<String> setPaymentTermExtId){
        return [SELECT Id, Name, Discount_Percentage__c, Net_Days_To_Pay__c, Description__c, Vendor__c, Unique_Key__c, Opco_Code__c, Discount_Days__c, Due_Days__c, Due_Month__c, Due_Date__c 
                FROM Payment_Term__c 
                WHERE Unique_Key__c IN: setPaymentTermExtId];
    }
    public static List<Buyer_Code__c> populateBuyerCode(set<String> key){
        return [SELECT id, Name,Buyer_User_ID__c,Unique_Key__c,Buyer_User_ID__r.Email__c FROM Buyer_Code__c WHERE Unique_Key__c IN: key];
    }

}