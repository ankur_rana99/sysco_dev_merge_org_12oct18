@isTest
public class ESMHelperTest{

 public static testMethod void testEsmHelperMethod(){
    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

    ESMHelper.errorResult wrapperField = new ESMHelper.errorResult();
    wrapperField.ValExecMsgKey = 'abc';
    wrapperField.errorMessage = 'xyz';
    wrapperField.type = 'mno';
    wrapperField.isCustom = True;
 }
    
    public static testMethod void testCriteriaMethod(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    
    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = '==';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'String';
    wrapperCriteria.refField = 'Invoice__c';


  ESMHelper.isMatch(wrapperCriteria,'test');
  ESMHelper.isMatch(wrapperCriteria,123);
  ESMHelper.isMatch(wrapperCriteria,true);

  ESMHelper.getRandomNumber(100,500);
 }
   public static testMethod void testCriteriaMethod1(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    
    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = 'equals';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'String';
    wrapperCriteria.refField = 'Invoice__c';

  List<ESMHelper.Criteria> listcrt=new List<ESMHelper.Criteria>();
  listcrt.add(wrapperCriteria);

  ESMHelper.attributes wrapperAttribt = new ESMHelper.attributes();
  wrapperAttribt.additionalRuleValue='Test';
  wrapperAttribt.additionalRuleField='test1';
  List<ESMHelper.attributes> lstatr=new List<ESMHelper.attributes> ();
  lstatr.add(wrapperAttribt);


  ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc'; 
  wrapperJSONValue.criterias=listcrt;
  wrapperJSONValue.attributes = lstatr;

  ESMHelper.isMatch(wrapperCriteria,'test');
  
  ESMHelper.isMatch(wrapperCriteria,true);

    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = wrapperJSONValue;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'RelatedList' ;
    wrapperRule.objectName = 'Invoice__c' ;

  Invoice__c inv=new Invoice__c();
  inv.Amount__c=100;
  insert inv;

  ESMHelper.checkRuleMatch(wrapperRule,inv);

  ESMHelper.getRandomNumber(100,500);


 }
  public static testMethod void testCriteriaMethod11(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = '==';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'NUMBER';
    wrapperCriteria.refField = 'Invoice__c';

    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

  List<ESMHelper.Criteria> listcrt=new List<ESMHelper.Criteria>();
  listcrt.add(wrapperCriteria);

  ESMHelper.attributes wrapperAttribt = new ESMHelper.attributes();
  wrapperAttribt.additionalRuleValue='Test';
  wrapperAttribt.additionalRuleField='test1';
  List<ESMHelper.attributes> lstatr=new List<ESMHelper.attributes> ();
  lstatr.add(wrapperAttribt);


  ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc'; 
  wrapperJSONValue.criterias=listcrt;
  wrapperJSONValue.attributes = lstatr;

  ESMHelper.isMatch(wrapperCriteria,'test');
  
  ESMHelper.isMatch(wrapperCriteria,true);

    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = wrapperJSONValue;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'NUMBER' ;
    wrapperRule.objectName = 'Invoice__c' ;

  Invoice__c inv=new Invoice__c();
  inv.Amount__c=100;
  insert inv;

  ESMHelper.checkRuleMatch(wrapperRule,inv);

  ESMHelper.getRandomNumber(100,500);

 }
  public static testMethod void testCriteriaMethod13(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = 'not equal to';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'REFERENCE';
    wrapperCriteria.refField = 'Invoice__c';

    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

  List<ESMHelper.Criteria> listcrt=new List<ESMHelper.Criteria>();
  listcrt.add(wrapperCriteria);

  ESMHelper.attributes wrapperAttribt = new ESMHelper.attributes();
  wrapperAttribt.additionalRuleValue='Test';
  wrapperAttribt.additionalRuleField='test1';
  List<ESMHelper.attributes> lstatr=new List<ESMHelper.attributes> ();
  lstatr.add(wrapperAttribt);


  ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc'; 
  wrapperJSONValue.criterias=listcrt;
  wrapperJSONValue.attributes = lstatr;

  ESMHelper.isMatch(wrapperCriteria,'test');
  
  ESMHelper.isMatch(wrapperCriteria,true);

    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = wrapperJSONValue;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'REFERENCE' ;
    wrapperRule.objectName = 'Invoice__c' ;

  Invoice__c inv=new Invoice__c();
  inv.Amount__c=100;
  insert inv;

  ESMHelper.checkRuleMatch(wrapperRule,inv);

  ESMHelper.getRandomNumber(100,500);


 }


 public static testMethod void testCriteriaMethod2(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = 'contains in list';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'String';
    wrapperCriteria.refField = 'Invoice__c';

    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

  ESMHelper.isMatch(wrapperCriteria,'test');
  
  ESMHelper.isMatch(wrapperCriteria,true);
}
 public static testMethod void testCriteriaMethod3(){
  
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = 'equals';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'String';
    wrapperCriteria.refField = 'Invoice__c';

  List<ESMHelper.Criteria> listcrt=new List<ESMHelper.Criteria>();
  listcrt.add(wrapperCriteria);

  ESMHelper.attributes wrapperAttribt = new ESMHelper.attributes();
  wrapperAttribt.additionalRuleValue='Test';
  wrapperAttribt.additionalRuleField='test1';
  List<ESMHelper.attributes> lstatr=new List<ESMHelper.attributes> ();
  lstatr.add(wrapperAttribt);


  ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc'; 
  wrapperJSONValue.criterias=listcrt;
  wrapperJSONValue.attributes = lstatr;

  ESMHelper.isMatch(wrapperCriteria,'test');
  
  ESMHelper.isMatch(wrapperCriteria,true);

    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = wrapperJSONValue;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'RelatedList' ;
    wrapperRule.objectName = 'Invoice__c' ;

  Invoice__c inv=new Invoice__c();
  inv.Amount__c=100;
  insert inv;

  ESMHelper.checkRuleMatch(wrapperRule,inv);

  ESMHelper.getRandomNumber(100,500);


 }
    
    public static testMethod void testRuleMethod(){
  /*
    ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc';
     

    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = wrapperJSONValue;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'RelatedList' ;
    wrapperRule.objectName = 'Invoice__c' ;

  Invoice__c inv=new Invoice__c();
  inv.Amount__c=100;
  insert inv;

  ESMHelper.checkRuleMatch(wrapperRule,inv);*/
 }
  
  
  public static testMethod void testColumnsMethod(){
    ESMHelper.Columns wrapperColumns = new ESMHelper.Columns();
    wrapperColumns.name ='test';
  wrapperColumns.defaultValue ='test1';
  wrapperColumns.reference ='testref';
  wrapperColumns.readonly =false;
  wrapperColumns.required=false;
  wrapperColumns.type='text';
  wrapperColumns.label='abc';
  wrapperColumns.searchBy ='test';
  wrapperColumns.searchFilter ='abc';
  
  List<ESMHelper.Columns> clmnlist=new  List<ESMHelper.Columns>();
  clmnlist.add(wrapperColumns);
System_Configuration__c sysconfig2 = new System_Configuration__c(); 
    sysconfig2.Module__c='Invoice__c'; 
    sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State'; 
    sysconfig2.Value__c = 'Awaiting OCR Feed'; 
    insert sysconfig2;
  ESMHelper.Layout wrapperLayout=new ESMHelper.Layout();
  wrapperLayout.layout=clmnlist;

  ESMHelper.dataHelper wrapperdataHelper = new ESMHelper.dataHelper();
  List<sobject> sobjlist=new List<sobject>();
  wrapperdataHelper.DataList =sobjlist;
   wrapperdataHelper.errmsg='Test';
   wrapperdataHelper.Columns=clmnlist;
   Map<String,String> strmap=new Map<String,String>();
  strmap.put('test1','abc');
  wrapperdataHelper.DataListMap=strmap;
  
    ESMHelper.dataHelperRecord wrapperdatahlprec = new ESMHelper.dataHelperRecord();
  Invoice__c inv=new Invoice__c();
  wrapperdatahlprec.record=inv;
  wrapperdatahlprec.errmsg='error';
  wrapperdatahlprec.Columns=clmnlist;
  wrapperdatahlprec.esmNameSpace='test';

  
  
 }
 public static testMethod void testJSONValueMethod(){
    ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.alwaysMoveToQC=false;
    wrapperJSONValue.qcPerecentage=1;
    wrapperJSONValue.qcFormFieldsetName='test';
    wrapperJSONValue.qcFormHeader ='abc';   
    
    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

   /*wrapperJSONValue.criterias = '[{"abc":"test"}]';
    wrapperCriteria.operator = 'ana';
    wrapperCriteria.value = 'a';
    wrapperCriteria.type = 'b';
    wrapperCriteria.refField = 'c';*/
    
      }  
 /*   Validation_Exception_Rule__c validationRuleConfig =new Validation_Exception_Rule__c();
    validationRuleConfig.Rule__c = '{"criterias":[{"value":"121","type":"CURRENCY","refField":"","operator":"equals","field":"AMT__c"},{"value":"11","type":"CURRENCY","refField":"","operator":"not equal to","field":"AMT__c"},{"value":"56","type":"CURRENCY","refField":"","operator":"less than","field":"AMT__c"},{"value":"21","type":"CURRENCY","refField":"","operator":"greater than","field":"AMT__c"},{"value":"122","type":"CURRENCY","refField":"","operator":"less or equal","field":"AMT__c"},{"value":"3234","type":"CURRENCY","refField":"","operator":"greater or equal","field":"AMT__c"},{"value":"True","type":"BOOLEAN","refField":"","operator":"equals","field":"Approval_Required_For_Additional_Charges__c"},{"value":"False","type":"BOOLEAN","refField":"","operator":"not equal to","field":"Approval_Required_For_Additional_Charges__c"},{"value":"121","type":"STRING","refField":"","operator":"equals","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"not equal to","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"starts with","field":"Error_Message__c"},{"value":"232","type":"STRING","refField":"","operator":"contains","field":"Error_Message__c"},{"value":"23","type":"STRING","refField":"","operator":"end with","field":"Error_Message__c"},{"value":"123","type":"STRING","refField":"","operator":"does not contain","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"contains in list","field":"Error_Message__c"}],"attributes":null}';
    validationRuleConfig.Error_Type__c = 'Validation'; 
    validationRuleConfig.isActive__c = true;  
    validationRuleConfig.isCustom__c = true;
    validationRuleConfig.Error_Code__c = 'DUPLICATE_INVOICE'; 
    validationRuleConfig.Stop_Excecution__c = true;    
    validationRuleConfig.Order__c= 19;
    validationRuleConfig.AdditionalRules__c = '{"criterias":null,"attributes":[{"additionalRuleValue":"525","additionalRuleField":"Tolerance_In_Percent"},{"additionalRuleValue":"51","additionalRuleField":"Tolerance_In_Amount"}]}'; 
   
    insert validationRuleConfig;
    Invoice__c obj = new Invoice__c();
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 121;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '121';
        
        insert obj;
    EsmHelper.Rule esmRule = new EsmHelper.Rule();
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
        
        obj.Amount__c = 11;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '12';
        
        update obj;
 
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
        
        
   
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 56;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '232';
        
        insert obj;
    
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
       
        obj.Amount__c = 21;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '23';
        
        update obj;
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
        
     obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 122;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '123';
        
        insert obj;
    
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
       
        obj.Amount__c = 3234;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '123';
        
        update obj;
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
         
 } */
}