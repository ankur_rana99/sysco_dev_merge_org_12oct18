public class UpdateApprovalDetailApproveReject
{
    @InvocableMethod(label='UpdateInvoiceApproveRejectAppObj' description='If anyone will reject then invoice should be move into reject else if all approver will approve then invoice should get approved.')
        public static void checkUserDOA(List<Approval_Detail__c> appDetail) {
            Set<String> appId = new Set<String>();
            for(Approval_Detail__c apd : appDetail) {
                appId.add(apd.Invoice__c);
            }
            List<Invoice__c> invoiceList = new List<Invoice__c>();
            String query = '';
            String selectfields = '';
            Map<string, Schema.sObjectType> describeInfo = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fMap = describeInfo.get('Invoice__c').getDescribe().Fields.getMap();
            if (fMap != null) {
                for (Schema.SObjectField f : fMap.values()) {
                    selectfields += f.getDescribe().getName().toLowerCase() + ',';
                }
                selectfields = selectfields.removeEnd(',');
                query = 'SELECT ' + selectfields + ' FROM Invoice__c WHERE Id IN :appId ';
                invoiceList = Database.query(query);
            }
            UpdateInvoiceApproveReject.checkUserDOA(invoiceList);
        }
}