global class CSVLoadManager implements CSVLoadConfig {
    
    global Map<String,Map<String,Map<String,string>>> execute(Map<object,object> extraParamMap){
        system.debug('inside CSVLoadManager');
        Map<String,Map<String,Map<String,string>>> returnMap = new Map<String,Map<String,Map<String,string>>>();
        CSV_Load_Configuration__c config;
        config =  CSV_Load_Configuration__c.getOrgDefaults(); 
        string childObjectName = config.ChildObject_Name__c;
        String additionalObj = config.Additional_Object_Name__c;
        system.debug('childObjectName:-'+childObjectName);
        system.debug('additionalObj:-'+additionalObj);
        
        string childNameSpace='';
     
        String  query ='';

        system.debug('childNameSpace:-'+childNameSpace);

       Map<String, Map<String,String>> mapRefFieldKeyId = new Map<String, Map<String,String>>();
        Map<String,String> mapkeyId = new Map<String,String>();

        /*------getting all the purchase orders starts----------*/
    /*    query = 'Select id,name,'+childNameSpace+'PO_No__c from '+childNameSpace+'Purchase_Order__c ';
        list<sobject> poSobj = database.query(query);
        for(sobject pobj : poSobj){
            mapkeyId.put(string.valueof(pobj.get('name')), pobj.id);
        }
        mapRefFieldKeyId.put(childNameSpace+'Purchase_Order__c',mapkeyId);*/

        //system.debug('Purchase_Order__c:-'+mapRefFieldKeyId.get(childNameSpace+'Purchase_Order__c'));
        /*------getting all the purchase orders ends----------*/
        
        /*------getting all the supplier profiles starts----------*/
        query = 'Select id,name from '+childNameSpace+'Account';
        list<sobject> suppSobj = database.query(query);
        //list<Supplier_Profile__c> lstSP = [Select id, name,Supplier_No__c from Supplier_Profile__c];
        mapkeyId = new Map<String,String>();
        for(sobject supplierobj : suppSobj){
            mapkeyId.put(string.valueof(supplierobj.get('name')) , supplierobj.id);
        }
        mapRefFieldKeyId.put(childNameSpace+'Vendor__c',mapkeyId);
         system.debug('Supplier_No__c:-'+mapRefFieldKeyId.get(childNameSpace+'Vendor__c'));
        /*------getting all the supplier profiles ends----------*/
        
        /*------getting all the Requestor/Buyers starts----------*/
        query = 'Select id,name,'+childNameSpace+'User_Type__c from '+childNameSpace+'User_Master__c';
        list<sobject> buyerSobj = database.query(query);
        mapkeyId = new Map<String,String>();
        for(sobject buyer : buyerSobj){
            
            mapkeyId.put(string.valueof(buyer.get('name')),string.valueof(buyer.get('id')));
        }
        mapRefFieldKeyId.put(childNameSpace+'Requestor_Buyer__c',mapkeyId);
        /*------getting all the Requestor/Buyers ends----------*/
        
        /*---put all the lookup lists of invoice object in map */
        returnMap.put(childNameSpace+'Invoice__c',mapRefFieldKeyId);
        
        /*------getting all the po line items starts----------*/
        mapRefFieldKeyId = new Map<String, Map<String,String>>();
        query = 'Select id,name,'+childNameSpace+'PO_Line_No__c from '+childNameSpace+'PO_Line_Item__c';
        list<sobject> polSobj = database.query(query);
        mapkeyId = new Map<String,String>();
        for(sobject pol : polSobj){
            mapkeyId.put(string.valueof(pol.get('name')) , pol.id);
        }
        mapRefFieldKeyId.put(childNameSpace+'PO_Line_Item__c',mapkeyId);

        query = 'Select id,name,'+childNameSpace+'PO_No__c from '+childNameSpace+'Purchase_Order__c ';
        list<sobject> poSobj = database.query(query);
        for(sobject pobj : poSobj){
            mapkeyId.put(string.valueof(pobj.get('name')), pobj.id);
        }
        mapRefFieldKeyId.put(childNameSpace+'Purchase_Order__c',mapkeyId);

       //system.debug('PO_Line_Item__c:-'+mapRefFieldKeyId.get(childNameSpace+'PO_Line_Item__c'));
        
        /*------getting all the po line ends----------*/
        
        /*---put all the lookup lists of Invoice_Line_Item__c object in map */
        returnMap.put(childNameSpace+'Invoice_Line_Item__c',mapRefFieldKeyId);
        
        return returnMap;
    }
    
}