public class CaseListItemController {

    @AuraEnabled
    public static CaseManager__c toggleFavourite(CaseManager__c favCase,string action){
     CaseManager__c favouritedCase =  [Select id, subject__c,favourited__c from CaseManager__c where id=: favCase.Id];
        if(action!=null && action!=''){
            if(action == 'Favourite'){
                favouritedCase.Favourited__c = true;    
            }else if(action == 'Unfavourite'){
                favouritedCase.Favourited__c = false;    
            }
        }
        update favouritedCase;
        return favouritedCase;
    }
    
    // Method to clone the Main Case - Used in Split Functionality by Mohit
    @AuraEnabled
    public static list<CaseManager__c> returnSplitCases(CaseManager__c parentCase, Integer noOfSplit){
        list<CaseManager__c> splitCases = new list<CaseManager__c>();
        for(integer i=0;i<noOfSplit;i++){
            splitCases.add(parentCase.clone());
        }
        return splitCases;
    } 
    
    // getAttachments Method - Used in Split Functionality by Mohit
    @AuraEnabled
    public static Map<String,Object> getAttachments(String caseId) {
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='The following exception has occurred: ';
        try{
            List<ContentDocumentLink> cdl=CaseService.getAttachmentLinks(caseId);
            resp.put('attachments',cdl);
        }catch (Exception e) {
            resp.put('error', errorMessage+e.getMessage());
            ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
        } 
        return resp;
    }
    
    //Split Method used in Split Functionality by Mohit
    @AuraEnabled
    public static List<CaseManager__c> performSplitAction(String splitList,CaseManager__c mainCase,String objectType,String fsName,String Comments){
        Id mainCaseId = mainCase.id;
      String subject = mainCase.Subject__c;
      System.debug('splitList :' +splitList);
        List<Object> m =  (List<Object>) JSON.deserializeUntyped(splitList);
        System.debug('Deserialised List:' +m);
        List<Map<String,Object>> splitMapList = new List<Map<String,Object>>();
        List<CaseManager__c> SplittedCaseList = new List<CaseManager__c>();
        List<Comments__c> splitCommentsList = new List<Comments__c>();
        List<Relationship__c> relationshipList = new List<Relationship__c>();
        system.debug(m.size());
        for(Object ob : m){
            system.debug(ob);
            Map<String,Object> tempMap = new Map<String,Object>();
            tempMap = (Map<String,Object>)ob;
            splitMapList.add(tempMap);
        }
        
        List<Map<CaseManager__c,List<String>>> caseTrackerAttachmentList = new List<Map<CaseManager__c,List<String>>>();
        for(Map<String,Object> tempMap : splitMapList){
           Map<CaseManager__c,List<String>> caseTrackerAttachment = new Map<CaseManager__c,List<String>>();
            CaseManager__c caseTracker = new CaseManager__c();
            List<String> attachmentList = new List<String>();
            for(String key : tempMap.keyset()){
                if(key=='attachment'){
                  //create attachment
                    String attachmentListString = String.ValueOf(tempMap.get(key));
                    attachmentListString = attachmentListString.removeStartIgnoreCase('(');
                    attachmentListString = attachmentListString.removeEndIgnoreCase(')');                    
                    attachmentList = attachmentListString.split(', ');
                 
                }else{
                    String caseString =  JSON.serialize(tempMap.get(key));
                    caseTracker = (CaseManager__c) JSON.deserialize(caseString, CaseManager__c.class);
                }
            }
            caseTrackerAttachment.put(caseTracker,attachmentList);
            caseTrackerAttachmentList.add(caseTrackerAttachment);
            System.debug('caseTrackerAttachmentList :' +caseTrackerAttachmentList);
       }
        
        for(Map<CaseManager__c,List<String>> caseTrackerAttachment : caseTrackerAttachmentList){
            for (CaseManager__c ctKey : caseTrackerAttachment.keySet()){
              List<String> tempAtt = new List<String>();
              List<ContentDocumentLink> contentDocList = new List<ContentDocumentLink>();
              tempAtt = caseTrackerAttachment.get(ctKey);
              System.debug('tempAtt :' +tempAtt);
              ctKey.Subject__c = System.Label.Case_Created_Manually + System.now().format();
              insert ctkey;
              SplittedCaseList.add(ctkey);
             
             //Attachment linking code starts
              for(String st : tempAtt){
                        System.debug('st : ' +st);
                        Id tempId = String.valueOf(st);
                        ContentVersion  contVersion =[SELECT Checksum,ContentDocumentId,ContentLocation,ContentSize,ContentUrl,Description,FileExtension,FileType,FirstPublishLocationId,Id,IsAssetEnabled,IsDeleted,Origin,OwnerId,PathOnClient,PublishStatus,RatingCount,ReasonForChange,SharingOption,Title,VersionData,VersionNumber FROM ContentVersion WHERE ContentDocumentId = :tempId];
                         
                         ContentVersion newContVersion = new ContentVersion();
                            newContVersion.Title = contVersion.Title;
                            newContVersion.PathOnClient  = contVersion.PathOnClient;
                            newContVersion.VersionData = contVersion.VersionData;
                            newContVersion.FirstPublishLocationId  = contVersion.FirstPublishLocationId;
                        insert newContVersion;
                         
                          ContentDocumentLink contentDocLink  = new ContentDocumentLink();
                          contentDocLink.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: newContVersion.Id].ContentDocumentId;
                              contentDocLink.LinkedEntityId = ctkey.Id;
                              contentDocLink.ShareType = 'V';
                              contentDocList.add(contentDocLink);
                   } insert contentDocList;
                //Attachment linking code ends
            }
        }
        
       

      Map<Id,CaseManager__c> splitMap = new Map<Id,CaseManager__c>([Select id, Name from CaseManager__c where id IN :SplittedCaseList]);
      
      
      // Below code is used to create EmailMessage for the splitted Case
      EmailMessage[] newEmail = new EmailMessage[0];
      for(CaseManager__c ct : SplittedCaseList){
        
        String emailBody;
        String emailSubject=System.Label.Case_Created_Manually_By + ' ' +UserInfo.getFirstName() + ' ' +System.Label.At + ' '+ System.now().format()+' '+ESMConstants.SUBJECT_START_IDENTIFIER + splitMap.get(ct.Id).Name + ESMConstants.SUBJECT_END_IDENTIFIER ;
      
          emailBody=System.Label.Case_Created_Manually_By+ ' '+UserInfo.getFirstName()+ ' '+System.Label.On_Date+ ' '+System.now().format();
     
        newEmail.add(new EmailMessage(Subject = emailSubject ,
        TextBody = emailBody,
        HtmlBody =emailBody,
        FromName = UserInfo.getName(), //PUX-186 
        Incoming = true,
        RelatedToId = ct.Id,
        status = '0')); 
        
        //Code to create Split Comments
        Comments__c splitComments = new Comments__c();
        splitComments.CaseManager__c = ct.id;
        splitComments.Comment__c = Comments;
        splitCommentsList.add(splitComments);
        
        // Code to create Relationship for splitted Cases
        Relationship__c splitParent = new Relationship__c();
        splitParent.CaseManager__c = mainCaseId;
        splitParent.Sub_Case__c = ct.id;
        splitParent.Type__c = 'Split';
        relationshipList.add(splitParent);
        
        Relationship__c splitChild = new Relationship__c();
        splitChild.CaseManager__c = ct.id;
        splitChild.Sub_Case__c = mainCaseId;
        splitChild.Type__c = 'Split';
        relationshipList.add(splitChild);
      } 
      
      // Inserting Split Comments in Comments Object
      if(splitCommentsList.size() > 0){
      insert splitCommentsList;
      
      }
      //Inserting Relationships in Relationship Object
      if(relationshipList.size() >0 ){
      insert relationshipList;
      
      }
      //Inserting Email Message
      insert newEmail;
      
      return SplittedCaseList; 
    }
    
    //Merge Method by Mohit
   @AuraEnabled
    public static List<CaseManager__c> performMergeAction(List<CaseManager__c> mergeList,CaseManager__c mainCase, String mergeComments){
        Id mainCaseId = mainCase.id;
        List<CaseManager__c> mergedCases = new List<CaseManager__c>();
        List<Comments__c> mergeCommentsList = new List<Comments__c>();
        Map<Id,CaseManager__c> subCasesId = new Map<Id,CaseManager__c>();
        List<Relationship__c> mergeRelationship = new List<Relationship__c>();
        List<ContentDocumentLink> contentDocumentList = new List<ContentDocumentLink>();
        List<CaseManager__c> updateUserActionList = new List<CaseManager__c>();
        Set<Id> LinkedEntityIdSet = new Set<Id>();
        
        for(CaseManager__c mergeCases : mergeList){
          //  mergeCases.Current_State__c = 'Merged';
          // mergeCases.UserAction__c = 'Merge And Close';
          if(mergeCases.Id != mainCaseId){
            mergeCases.User_Action__c = 'Merge And Close';
            updateUserActionList.add(mergeCases);
          }
            mergedCases.add(mergeCases);
            subCasesId.put(mergeCases.id,mergeCases);
        }System.debug('updateUserActionList : ' +updateUserActionList);
        update updateUserActionList;
        //Creating Comments for Merge
        for(Id subCase : subCasesId.keyset() ){
          Comments__c mergeComment = new Comments__c();
          mergeComment.CaseManager__c = subCase;
          mergeComment.Comment__c = mergeComments;
          mergeCommentsList.add(mergeComment);
        }
        //Inserting Merge  Comments in Comments Object
        if(mergeCommentsList.size()>0){
          insert mergeCommentsList;
        }
        
        if(subCasesId.containsKey(mainCaseId)){
            subCasesId.remove(mainCaseId);
        }
        
       //Attachment linking code starts
        LinkedEntityIdSet = subCasesId.keySet();
        Map<Id,ContentDocumentLink> attachmentMap = new Map<Id,ContentDocumentLink>([Select Id,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN :LinkedEntityIdSet AND ShareType = 'V']);
        for(Id tempContentId : attachmentMap.keySet()){
            Id tempContentDocId = attachmentMap.get(tempContentId).ContentDocumentId;
            ContentVersion  contVersion =[SELECT Checksum,ContentDocumentId,ContentLocation,ContentSize,ContentUrl,Description,FileExtension,FileType,FirstPublishLocationId,Id,IsAssetEnabled,IsDeleted,Origin,OwnerId,PathOnClient,PublishStatus,RatingCount,ReasonForChange,SharingOption,Title,VersionData,VersionNumber FROM ContentVersion WHERE ContentDocumentId = :tempContentDocId];
           //Creating new ContentVersion to link attachments to Merged Case to Main Case
            ContentVersion newContVersion = new ContentVersion();
                newContVersion.Title = contVersion.Title;
                newContVersion.PathOnClient  = contVersion.PathOnClient;
                newContVersion.VersionData = contVersion.VersionData;
                //newContVersion.FirstPublishLocationId  = contVersion.FirstPublishLocationId;
            insert newContVersion;
           //Creating new ContentDocumentLink to link attachments to Merged Case to Main Case
           ContentDocumentLink contentDoc = new ContentDocumentLink();
           contentDoc.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: newContVersion.Id].ContentDocumentId;
            contentDoc.LinkedEntityId = mainCaseId;
            contentDoc.ShareType = 'V';
            contentDoc.Visibility = 'AllUsers';
            contentDocumentList.add(contentDoc);
        }
        insert contentDocumentList;
        //Attachment linking code ends
        
        //Creating Realtionships for Merged Cases
        for(Id subCase : subCasesId.keyset() ){
          Relationship__c mergeParent = new Relationship__c();
            mergeParent.CaseManager__c = mainCaseId;
            mergeParent.Sub_Case__c = subCase;
            mergeParent.Type__c = 'Merge';
            mergeRelationship.add(mergeParent);
            
            Relationship__c mergeChild = new Relationship__c();
            mergeChild.CaseManager__c = subCase;
            mergeChild.Sub_Case__c = mainCaseId; 
            mergeChild.Type__c = 'Merge';
            mergeRelationship.add(mergeChild);
        } 
        //Inserting Merge Case Realtionships in Relationship Object 
        if(mergeRelationship.size() >0 ){
          insert mergeRelationship;
        }
       return mergedCases;  
    }
}