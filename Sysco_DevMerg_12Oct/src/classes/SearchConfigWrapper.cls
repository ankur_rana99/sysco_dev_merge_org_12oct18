public class SearchConfigWrapper{

		@AuraEnabled
		public string CriteriaFieldSetName;
		@AuraEnabled
		public string ResultFieldSetName;
		@AuraEnabled
		public string ObjectAPIName;
		@AuraEnabled
		public string FilterCriteria;
		@AuraEnabled
		public string BulkAssignmentUserType;
		@AuraEnabled
		public string AccessProfile;
		@AuraEnabled
		public string ReassignmentUserType;		
		@AuraEnabled
		public string ReassignmentAccessProfile;
		@AuraEnabled
		public Boolean POFlip;
		@AuraEnabled
		public Boolean PaymeEarly;
		@AuraEnabled
		public Boolean CreateInquiry;
		@AuraEnabled
		public Boolean Clone;
		@AuraEnabled
		public Boolean Create;
		@AuraEnabled
		public Boolean isCreatePer;
		@AuraEnabled
		public Boolean isUpdatePer;
		@AuraEnabled
		public Boolean isAssignPer;
		@AuraEnabled
		public Boolean isReassignPer;
    	@AuraEnabled
		public Boolean isEditable;
		
        public SearchConfigWrapper(){
            
        }
    
		public SearchConfigWrapper(string ObjectAPIName, string CriteriaFieldSetName, string ResultFieldSetName, string FilterCriteria, String BulkAssignmentUserType, String AccessProfile, Boolean POFlip, Boolean PaymeEarly, Boolean CreateInquiry, Boolean Clone, Boolean Create, Boolean isCreatePer, Boolean isUpdatePer, String ReassignmentUserType, String ReassignmentAccessProfile,Boolean isAssignPer, Boolean isReassignPer, Boolean isEditable){			
			this.CriteriaFieldSetName = (CriteriaFieldSetName == null)?'':CriteriaFieldSetName;
			this.ResultFieldSetName = (ResultFieldSetName == null)?'':ResultFieldSetName;
			this.ObjectAPIName = (ObjectAPIName == null)?'':ObjectAPIName;
			this.FilterCriteria = (FilterCriteria == null)?'':FilterCriteria;
			this.BulkAssignmentUserType = (BulkAssignmentUserType == null)?'':BulkAssignmentUserType;
			this.AccessProfile = (AccessProfile == null)?'':AccessProfile;
			this.ReassignmentUserType = (ReassignmentUserType == null)?'':ReassignmentUserType;
			this.ReassignmentAccessProfile = (ReassignmentAccessProfile == null)?'':ReassignmentAccessProfile;
			this.POFlip = (POFlip == null)?false:POFlip;
			this.PaymeEarly = (PaymeEarly == null)?false:PaymeEarly;
			this.CreateInquiry = (CreateInquiry == null)?false:CreateInquiry;
			this.Clone = (Clone == null)?false:Clone;
			this.Create = (Create == null)?false:Create;
			this.isCreatePer = (Create == null)?false:isCreatePer;
			this.isUpdatePer = (Create == null)?false:isUpdatePer;
			this.isAssignPer = isAssignPer;
			this.isReassignPer = isReassignPer;
            this.isEditable = isEditable;
		}		
	}