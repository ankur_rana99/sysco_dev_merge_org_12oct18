@isTest
public class SyscoCORAIndexingHelperTest{

	public static testmethod void testFirst() {
		System_Configuration__c sysconfig=new System_Configuration__c ();
		sysconfig.Module__c='Custom_Record_History';
		sysconfig.Value__c ='Invoice__c';
		insert sysconfig;

		System_Configuration__c sysconfig2 = new System_Configuration__c(); 
		sysconfig2.Module__c='Invoice__c'; 
		sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State'; 
		sysconfig2.Value__c = 'Awaiting OCR Feed'; 
		insert sysconfig2;

		Invoice_Configuration__c invConf = new Invoice_Configuration__c();
		invConf.Enable_Quantity_Calculation__c = true;
		invConf.GRN_Flag_In_PO_Header__c = true;
		invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
		invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
		invConf.Enable_Auto_Flipping_With_GRN__c = true;
		invConf.PO_Number_Field_for_Search__c = 'Name';
		invConf.Invoice_Reject_Current_State__c ='';
		invConf.Auto_Match_Flow_Method__c = 'Sequence';
		invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
		invConf.Auto_Validation_Current_States__c = '';
		insert invConf;

		OPCO_Code__c opco = new OPCO_Code__c();
		opco.Name = '059';
		opco.OPCO_Code__c = '059';
		opco.OPCO_Name__c = '059';
		insert opco;

		OPCO_Code__c opco1 = new OPCO_Code__c();
		opco1.Name = '011';
		opco1.OPCO_Code__c = '011';
		opco1.OPCO_Name__c = '011';
		insert opco1;

		Account acc = new Account();
		acc.Unique_Key__c = '059-100';
		acc.Vendor_No__c = '100';
		acc.OPCO_Code__c = opco.Id;
		acc.Name = 'Cora';
		acc.Vendor_Type__c = 'F';
		acc.Payment_Vendor_Terms__c = '059-NET25';
		acc.Vendor_No__c = '100';
		insert acc;

		Account acc1 = new Account();
		acc1.Unique_Key__c = '059-101';
		acc1.Vendor_No__c = '101';
		acc1.OPCO_Code__c = opco.Id;
		acc1.Name = 'Cora';
		acc1.Vendor_Type__c = 'M';
		acc1.Payment_Vendor_Terms__c = '059-NET25';
		acc1.Vendor_No__c = '101';
		insert acc1;

		User_Master__c um = new User_Master__c();
		um.Name = 'Cora';
		um.User_Name__c = 'cora@cora.com';
		um.Email__c = 'cora@cora.com';
		um.First_Name__c = 'cora';
		um.Status__c = 'Active';
		um.Entity__c = 'OpCo';
		um.OPCO_Code__c = opco.Id;
		um.Title_Type__c = 'Functional';
		insert um;

		Buyer_Code__c bc = new Buyer_Code__c();
		bc.OPCO_Code__c = opco.Id;
		bc.Name = '080';
		bc.Buyer_ID__c = '080';
		bc.Unique_Key__c = '059-080';
		bc.Buyer_User_ID__c = um.Id;
		insert bc;

		Purchase_Order__c  po = new Purchase_Order__c();
		po.PO_No__c = '100';
		po.OPCO_Code__c = opco.Id;
		po.Amount__c = 123;
		po.PO_Type__c = 'F';
		po.Vendor__c = acc.Id;
		po.Freight_Terms__c = 'NET25';
		po.Payment_Vendor_Terms__c = 'NET25';
		po.Unique_Key__c = '059-100';
		po.Buyer_ID__c = '080';
		insert po;

		String fieldValue = String.valueOf(po.Id);

		list<Purchase_Order__c> Listpo = new list<Purchase_Order__c>();
		Listpo.add(po);

		Payment_Term__c pt = new Payment_Term__c();
		pt.Name = 'NET25';
		pt.OPCO_Code__c = opco.Id;
		pt.Discount_Days__c = 0;
		pt.Discount_Percentage__c = 0;
		pt.Transaction_Type__c = 'F';
		pt.Unique_Key__c = '059-NET25';
		pt.Due_Date__c = 0;
		pt.Due_Days__c = 25;
		pt.Due_Month__c = 0;
		insert pt;
		
		Payment_Term__c pt1 = new Payment_Term__c();
		pt1.Name = 'NET25';
		pt1.OPCO_Code__c = opco1.Id;
		pt1.Discount_Days__c = 0;
		pt1.Discount_Percentage__c = 0;
		pt1.Transaction_Type__c = 'F';
		pt1.Unique_Key__c = '011-NET25';
		pt1.Due_Date__c = 5;
		pt1.Due_Days__c = 0;
		pt1.Due_Month__c = 10;
		insert pt1;

		map<String,Payment_Term__c> pterm = new map<String,Payment_Term__c>();
		pterm.put('059-NET25',pt);
		pterm.put('011-NET25',pt1);

		GL_Code__c gl = new GL_Code__c();
		gl.Name = '100';
		gl.OPCO_Code__c = opco.Id;
		gl.External_Key__c = '100';
		gl.Unique_Key__c = '100';
		insert gl;

		Invoice__c invobj = new Invoice__c();    
		invobj.Invoice_Date__c = Date.Today();
		invobj.Term_Start_Date__c = Date.Today();
		invobj.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj.Total_Amount__c = 500.00;
		invobj.Current_State__c='Start';
		invobj.Invoice_Type__c = 'M';
		invobj.WorkType__c = 'CPAS';
		invobj.Payment_Terms_Inv__c = '059-NET25';
		invobj.OPCO__c = '059';
		invobj.OPCO_Code__c = opco.Id;
		invobj.GL_Account_Number__c = gl.Id;
		invobj.GL_Code_Text__c = '100';
		insert invobj;

		Invoice__c invobj1 = new Invoice__c();    
		invobj1.Invoice_Date__c = Date.Today();
		invobj1.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj1.Total_Amount__c = 500.00;
		invobj1.Current_State__c='Vouched';
		invobj1.Invoice_Type__c = 'M';
		invobj1.WorkType__c = 'PO Invoice';
		invobj1.Invoice_Origin__c  ='EDI';
		invobj1.Vendor__c = acc.Id;
		invobj1.Payment_Terms_Inv__c = '059-NET25';
		invobj1.PO_Numbers__c = '100';
		invobj1.OPCO__c = '059';
		invobj1.OPCO_Code__c = opco.Id;
		insert invobj1;

		Invoice__c invobj2 = new Invoice__c();    
		invobj2.Invoice_Date__c = Date.Today();
		invobj2.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj2.Total_Amount__c = 500.00;
		invobj2.Current_State__c='Vouched';
		invobj2.Invoice_Type__c = 'M';
		invobj2.WorkType__c = 'PO Invoice';
		invobj2.Vendor__c = acc1.Id;
		invobj2.Payment_Terms_Inv__c = '059-NET25';
		invobj2.Invoice_Origin__c  ='EDI';
		invobj2.Purchase_Order__c = po.Id;
		invobj2.OPCO__c = '059';
		invobj2.OPCO_Code__c = opco.Id;
		insert invobj2;

		Invoice__c invobj3 = new Invoice__c();    
		invobj3.Invoice_Date__c = Date.Today();
		invobj3.Term_Start_Date__c = Date.Today();
		invobj3.Net_Due_Date__c = Date.Today().addDays(60);    
		invobj3.Total_Amount__c = 500.00;
		invobj3.Current_State__c='Vouched';
		invobj3.Invoice_Type__c = 'A';
		invobj3.WorkType__c = 'PO Invoice';
		invobj3.Vendor__c = acc1.Id;
		invobj3.Payment_Terms_Inv__c = '011-NET25';
		invobj3.Purchase_Order__c = po.Id;
		invobj3.OPCO__c = '011';
		invobj3.OPCO_Code__c = opco1.Id;
		insert invobj3;

		list<Invoice__c> invoiceList = new list<Invoice__c>();
		invoiceList.add(invobj);
		invoiceList.add(invobj1);
		invoiceList.add(invobj2);
		invoiceList.add(invobj3);
		SyscoCORAIndexingHelper sih = new SyscoCORAIndexingHelper(invoiceList);
		sih.assignPriorities(invobj,'Merchandize','PO Invoice');
		sih.assignPriorities(invobj,'Freight Only','PO Invoice');
		sih.assignPriorities(invobj,'Direct','PO Invoice');
		sih.assignPriorities(invobj,'Drop Shipments','PO Invoice');
		sih.assignPriorities(invobj,'RME Merchandize','PO Invoice');
		sih.assignPriorities(invobj,'Direct','PO Invoice');
		sih.assignPriorities(invobj,'Normal Invoices','Non PO Invoice');
		sih.assignPriorities(invobj,'Repayments','Non PO Invoice');
		sih.assignPriorities(invobj,'Utilities','Non PO Invoice');
		sih.assignPriorities(invobj,'Individual Payment Request','Check Request');
		sih.assignPriorities(invobj,'Bulk Payment Request','Check Request');
		sih.assignPriorities(invobj,'Bulk Payment Request','Check Request');
		sih.assignPriorities(invobj,'CPAS','Check Request');
		sih.assignPriorities(invobj,'Credit Memo','Credit Memo');
		sih.assignPriorities(invobj,'Agreements','Credit Memo');
		sih.assignPriorities(invobj,'Deductions','Credit Memo');
		sih.assignPriorities(invobj,'Non AP Documents','Non AP Documents');
		sih.assignPriorities(invobj,'Unclassified','Unclassified');
		sih.processEDIInvoices(invObj2,'PO Invoice','M','REG');
		sih.processEDIInvoices(invObj2,'PO Invoice','F','REG');
		sih.processEDIInvoices(invObj2,'PO Invoice','M','DRP');
		sih.processEDIInvoices(invObj2,'PO Invoice','A','REG');
		sih.processEDIInvoices(invObj,'PO Invoice','',null);
		
		sih.calculateDueDate(invobj,pterm);
		sih.calculateDueDate(invobj3,pterm);
		
		sih.createInvoiceLineItem(invoiceList);
		sih.calculateInvoiceControlAmount(invobj);
		sih.calculateInvoiceControlAmount(invobj1);
		map<Id, Id> mapInvoiceIdWithVendorId = new map<Id, Id>();
		mapInvoiceIdWithVendorId.put(invobj.Id,acc.Id);
		sih.assignVendorAddress(invobj,'test');
		sih.assignVendorAddress(invobj1,'test');
		sih.invoiceIndexing(invoiceList,false);
	}

}