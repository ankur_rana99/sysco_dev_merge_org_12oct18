/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for AttachmentController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class AttachmentControllerTest {
 
    public static testmethod void testCase1() {
        
        system.assert(true,true);

	
        List<CaseManager__c> ctList= ESMDataGenerator.createCases(1);
        String attachId= ESMDataGenerator.createContent(ctList[0]);
        AttachmentController.getAttachments(ctList[0].Id);
        AttachmentController.deleteAttachments(new List<Id>{attachId},ctList[0].Id);
		AttachmentController.getAttachmentsESM(ctList[0].Id);
		AttachmentController.deleteAttachmentsESM(new List<Id>{attachId},ctList[0].Id);
		
		ContentVersion testContentInsert =new ContentVersion(); 
		testContentInsert.ContentURL='<a target="_blank" href="http://www.google.com/" rel="nofollow">http://www.google.com/</a> ';
		testContentInsert.Title ='Google.com'; 
		//testContentInsert.flag__c = true;
		insert testContentInsert;
		String contentJson='{"'+testContentInsert.Id+'":"Archive"}';
		AttachmentController.updateFlagOnAttachments(contentJson);
    }
    
    //Test 2
   /* public static testMethod void unitTest02(){
        try{
        	AttachmentController.getAttachments(ctList[0].Id);
        }catch (Exception e) {}
        
        try{
        	AttachmentController.deleteAttachments(new List<Id>{attachId},ctList[0].Id);
        }catch (Exception e) {}
    }*/
}