public class checkForQcCM {
  @InvocableMethod(label = 'Check For QC CM' description = 'Check for QC based on QC Rules definied')
  public static void checkForQC(List<CaseManager__c> Cases) {
    QCCalculation.checkForQC(Cases, 'CaseManager__c');
  }
}