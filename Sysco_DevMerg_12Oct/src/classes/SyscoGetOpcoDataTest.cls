@isTest
public class SyscoGetOpcoDataTest{
    
    public static testMethod void testFirst(){    
        WorkDay_EndPoints__c sysWED=new WorkDay_EndPoints__c();
        sysWED.Name='OpcoData';
        sysWED.isfirstrun__c = 'Y';
        sysWED.Endpoint_URL__c='callout:Sysco_Company/ccx/service/customreport2/sysco4/ISU%20FDM%20Companies/Foundation_Data_-_Companies';
        insert sysWED;
                 
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());
        
        SyscoGetOpcoData getopco  = new SyscoGetOpcoData();
        Integer batchSize = 200; 
        system.database.executebatch(getopco,batchSize);       
        Test.StopTest();  
    }    
}