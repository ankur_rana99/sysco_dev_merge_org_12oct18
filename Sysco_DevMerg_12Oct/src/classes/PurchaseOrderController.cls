public class PurchaseOrderController {
	@AuraEnabled
	public static EsmHelper.dataHelper getPurchaseOrderServer(String strObjectName, String jsonObj, String objId, String poFlip) {
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		try {
			List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
			String selectedRows = UtilityController.getAllSelectedFields(cols);
			String whereClause = 'where Invoice__c = :objId  and Invoice__c!=null ';
			String query = 'SELECT Purchase_Order__c FROM Invoice_PO_Detail__c ' + whereClause;
			List<Invoice_PO_Detail__c> invPO = new List<Invoice_PO_Detail__c> ();
			Set<String> poList = new Set<String> ();
			if (poFlip != null) {
				for (String pol : poFlip.split(',')) {
					poList.add(pol);
				}
			} else {
				invPO = Database.query(query);
				for (Invoice_PO_Detail__c pol : invPO) {
					poList.add(pol.Purchase_Order__c);
				}
			}


			//objId = invPO.Purchase_Order__c;
			whereClause = 'where Id = :poList';
			query = CommonFunctionController.getSOQLQuery(strObjectName) + ' ' + whereClause;
			System.debug(query);
			List<sObject> objList = new List<sObject> ();
			if (UtilityController.isFieldAccessible(strObjectName, selectedRows))
			{
				objList = Database.query(query);
			} else {
				System.debug('rightsisuue555');
				throw new CustomCRUDFLSException();
			}
			dh = UtilityController.getWrapperFromJson(objList, strObjectName, cols);
		}
		catch(Exception e)
		{
		dh.errmsg = ExceptionHandlerUtility.customDebug(e, 'UtilityController');
		}
		return dh;
	}
	@AuraEnabled
	public static EsmHelper.dataHelper getSelectedPurchaseOrderServer(String poListDet, String strObjectName) {
		System.debug('@@jsonObj :' + poListDet);
		/*try
		  {*/
		Set<String> poset = new Set<String> ();
		for (String pol : poListDet.split(',')) {
			poset.add(pol);
		}
		//objId = invPO.Purchase_Order__c;
		String whereClause = 'where Id = :poset';
		String query = CommonFunctionController.getSOQLQuery(strObjectName) + ' ' + whereClause;
		System.debug(query);
		List<sObject> objList = new List<sObject> ();
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		/*if (UtilityController.isFieldAccessible(strObjectName, selectedRows))
		  {*/
		dh.DataList = Database.query(query);
		//}

		/*}
	   catch(Exception e)
	   {
	   ExceptionHandlerUtility.customDebug(e, 'UtilityController');
	   throw new Exception(e);
	   }*/
	System.debug(dh);
	return dh;
}
}