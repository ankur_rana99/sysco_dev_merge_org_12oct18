@isTest
public class ProcessOnHoldInvoicesTest{
       public static testMethod void testFirst(){
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;

           Group testGroup = new Group(Name='test group', Type='Queue');
			insert testGroup;
           System.runAs(new User(Id=UserInfo.getUserId()))
           {
    		QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Invoice__c');
    		insert testQueue;
           }
           
           Sysco_SystemConfiguration__c custset = new Sysco_SystemConfiguration__c();
        custset.name = 'ERPConfig';
        custset.EndPointURL__c='https://api.sysco.com/fi/FTR/Invoice/v1.0/mostInvoice';
        custset.SOQLQueryLimit__c='200';
        custset.TO_Email_Address__c = 'chiranjeevi.kunamalla@test.com';
        custset.WebserviceRetryLimit__c = 2;
        custset.SyscoAPIC__c = 'https://api.syscoo.com';
        custset.client_id__c = 'testid';
        custset.client_secret__c = 'testsecrete';
           System.runAs(new User(Id=UserInfo.getUserId()))
           {
    		QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Invoice__c');
    		insert testQueue;
               custset.Buyer_Portal_Owner_Id__c = testQueue.id;
           }
        
        insert custset;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         po.Anticipated_Date__c = Date.today()-1;
         insert po;
           
		Invoice__c invobj = new Invoice__c();    
    	//invobj.Invoice_Date__c = Date.Today();
    	//invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	//invobj.Total_Amount__c = 500.00;
        //invobj.Current_State__c='Vouched';
        invobj.Current_State__c = 'Pending Anticipated Date';
           invobj.Invoice_Type__c='M';
    	 System.debug('invobj' + invobj);
        insert invobj;
        list<Invoice__c> ListInv = new list<Invoice__c>();
        ListInv.add(invobj);
           
           Invoice_PO_Detail__c inPODet=new Invoice_PO_Detail__c();
        inPODet.Invoice__c=invobj.Id;
        inPODet.Purchase_Order__c=po.Id;
        insert inPODet;
          /* Approval_Detail__c appdet = new Approval_Detail__c();
           appdet.Invoice__c = invobj.id;
           appdet.Status__c = 'Pending';
           insert appdet;*/
               
        Test.startTest();
        ProcessOnHoldInvoices pi = new ProcessOnHoldInvoices();
        DataBase.executeBatch(pi);
           pi.execute(null,ListInv);
        Test.StopTest();   
    }

}