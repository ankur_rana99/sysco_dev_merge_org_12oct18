@isTest
public class EmailToCaseTest { 
    @testSetup 
    static void setupTestData() {
        
        EmailTemplate validEmailTemplate = new EmailTemplate();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'testTemplate';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }
        ESMDataGenerator.createIgnoreRule(validEmailTemplate.id);
        ESMDataGenerator.createAcceptRule(validEmailTemplate.id);
        ESMDataGenerator.createExitingCaseRule(validEmailTemplate.id);
        ESMDataGenerator.createIdentifyExitingCaseRule(validEmailTemplate.id);
        /*ContentWorkspace cswfT =new ContentWorkspace();
        cswfT.Name ='';
        insert cswfT;*/
        //Create Default account object
        Account acc=new Account();
        acc.Name=ESMConstants.DEFAULT_ACCOUNT_NAME;
        insert acc;
        
        CaseManager__c caseObj=new CaseManager__c();
        //caseObj.Process__c='AP';
        caseObj.WorkType__c='Non-PO Invoices';
        caseObj.Document_Type__c='Electricity';
        //caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Subject__c='Test';
        //caseObj.Current_State__c='Closed';
        caseObj.Subject_Readonly__c='Test';
        insert caseObj;
        

    }/*
    @isTest
    public static void testIgnoreRules(){
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@sftpl.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='Test Subject';
        caseBean.planSubject='Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@sftpl.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);


        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean,'CaseManager__c');
    }
    @isTest
    public static void testAcceptRules(){
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='AP_PO Test Subject';
        caseBean.planSubject='AP_PO Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        
        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean,'CaseManager__c');
    }
    @isTest
    public static void testExitingCaseRules(){
        CaseManager__c caseObj=[select id,name from CaseManager__c limit 1];

        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='AP_PO Test Subject '+ESMConstants.SUBJECT_START_IDENTIFIER +caseObj.Name+ESMConstants.SUBJECT_END_IDENTIFIER;
        caseBean.planSubject='AP_PO Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        
        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean,'CaseManager__c');
    }*/
    @isTest
    public static void testIsMatchMethod(){
        EmailToCaseManager caseManager=new EmailToCaseManager();
        EmailToCaseSettingHelper.Criteria cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Current_State__c';
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        cre.Source='CASE';
        caseManager.IsMatch(cre,true);
        cre.Operator='not equal to';
        caseManager.IsMatch(cre,true);

        //Integer Check
        cre.Value='5';

        cre.Operator='not equal to';
        caseManager.IsMatch(cre,1);

        cre.Operator='equals';
        caseManager.IsMatch(cre,1);

        cre.Operator='less than';
        caseManager.IsMatch(cre,2);

        cre.Operator='greater than';
        caseManager.IsMatch(cre,2);

        cre.Operator='less or equal';
        caseManager.IsMatch(cre,2);

        cre.Operator='greater or equal';
        caseManager.IsMatch(cre,2);

        //String Check

        cre.Value='TEst';

        cre.Operator='not equal to';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='equals';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='starts with';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='end with';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='contains';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='does not contain';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='contains in list';
        caseManager.IsMatch(cre,'Test');

        EmailToCaseSettingHelper.getPlanSubject('Test Subject');
        EmailToCaseSettingHelper.limitLength('Test test',5);
        EmailToCaseSettingHelper.removePrefixFromSubject('RE: Test Subject',new List<string>{'RE:'});
    }
    @isTest
    public static void testGetFieldValueMethod(){
        EmailToCaseManager caseManager=new EmailToCaseManager();
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.CCAddresses.add('chandresh.koyani@sftpl.com');
        caseBean.ToAddresses=new List<string>();
        caseBean.ToAddresses.add('chandresh.koyani@sftpl.com');
        caseBean.EnvelopFromAddress='chandresh.koyani@sftpl.com';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';
        caseBean.Header=new Map<string,string>();
        caseBean.Header.put('Importance','High');
        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.InReplyTo='Test';
        caseBean.MessageId='';
        caseBean.References=new List<string>();
        caseBean.References.add('test');

        caseBean.Attachments=new List<EmailToCaseAttachment>();

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        EmailToCaseAttachment attachmentNew=new EmailToCaseAttachment();
        attachmentNew.Body='This is simple text';
        attachmentNew.FileName='testInline.txt';
        attachmentNew.Type='TEXT';
        caseBean.Attachments.add(attachmentNew);

        caseManager.GetFieldValue('Sender Email',caseBean);
        caseManager.GetFieldValue('To Addresses',caseBean);
        caseManager.GetFieldValue('CC Addresses',caseBean);
        caseManager.GetFieldValue('Salesforce Email',caseBean);
        caseManager.GetFieldValue('Subject',caseBean);
        caseManager.GetFieldValue('Body',caseBean);
        caseManager.GetFieldValue('Importance',caseBean);

        caseManager.GetFieldValue('Attachment Ext',caseBean);

    }
    @isTest
    public static void testEmailToCaseService(){
        Service_Mapping__c serviceMap = new Service_Mapping__c();
        serviceMap.Object_Name__c = 'CaseManager__c';
        serviceMap.Email_Service__c  = 'abc@xyz.com';
        insert serviceMap;
        EmailToCaseService service=new EmailToCaseService();

        Messaging.InboundEmail inboundEmail=new Messaging.InboundEmail();
        inboundEmail.fromAddress='';
        List<string> emailList = new List<String>();
        emailList.add('abc@xyz.com');
        emailList.add('bca@xyz.com');
        inboundEmail.toAddresses=emailList;
        inboundEmail.ccAddresses=emailList;
        inboundEmail.FromName='';

        inboundEmail.MessageId='';
        inboundEmail.references=new List<string>();
        inboundEmail.InReplyTo='';
        inboundEmail.plainTextBody='';
        inboundEmail.htmlBody='';
        inboundEmail.subject='';
        inboundEmail.headers=new List<Messaging.InboundEmail.Header>();
        
        inboundEmail.textAttachments=new List<Messaging.InboundEmail.TextAttachment>();
        Messaging.InboundEmail.TextAttachment txtAtt=new Messaging.InboundEmail.TextAttachment();
        txtAtt.Body='Test';
        txtAtt.fileName='Test';
        txtAtt.mimeTypeSubType='';
        txtAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.textAttachments.add(txtAtt);

        inboundEmail.binaryAttachments=new List<Messaging.InboundEmail.binaryAttachment>();
        Messaging.InboundEmail.binaryAttachment biAtt=new Messaging.InboundEmail.binaryAttachment();
        biAtt.Body=blob.valueOf('Test');
        biAtt.fileName='Test';
        biAtt.mimeTypeSubType='';
        biAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.binaryAttachments.add(biAtt);

        Messaging.InboundEnvelope envelope=new Messaging.InboundEnvelope();
        service.handleInboundEmail(inboundEmail,envelope);
    }
    
    @isTest
    public static void testEmailToCaseService2(){
        contact con = new contact();
        con.LastName = 'Lastname';
        con.Email = 'abc@sftpl.com';
        insert con;
        Service_Mapping__c serviceMap = new Service_Mapping__c();
        serviceMap.Object_Name__c = 'CaseManager__c';
        serviceMap.Email_Service__c  = 'abc@xyz.com';
        insert serviceMap;
        EmailToCaseService service=new EmailToCaseService();

        Messaging.InboundEmail inboundEmail=new Messaging.InboundEmail();
        inboundEmail.fromAddress='';
        List<string> emailList = new List<String>();
        emailList.add('abc@xyz.com');
        emailList.add('bca@xyz.com');
        inboundEmail.toAddresses=emailList;
        inboundEmail.ccAddresses=emailList;
        inboundEmail.FromName='';

        inboundEmail.MessageId='';
        inboundEmail.references=new List<string>();
        inboundEmail.InReplyTo='';
        inboundEmail.plainTextBody='';
        inboundEmail.htmlBody='';
        inboundEmail.subject='';
        inboundEmail.headers=new List<Messaging.InboundEmail.Header>();
        
        inboundEmail.textAttachments=new List<Messaging.InboundEmail.TextAttachment>();
        Messaging.InboundEmail.TextAttachment txtAtt=new Messaging.InboundEmail.TextAttachment();
        txtAtt.Body='Test';
        txtAtt.fileName='Test';
        txtAtt.mimeTypeSubType='';
        txtAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.textAttachments.add(txtAtt);

        inboundEmail.binaryAttachments=new List<Messaging.InboundEmail.binaryAttachment>();
        Messaging.InboundEmail.binaryAttachment biAtt=new Messaging.InboundEmail.binaryAttachment();
        biAtt.Body=blob.valueOf('Test');
        biAtt.fileName='Test';
        biAtt.mimeTypeSubType='';
        biAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.binaryAttachments.add(biAtt);

        Messaging.InboundEnvelope envelope=new Messaging.InboundEnvelope();
        service.handleInboundEmail(inboundEmail,envelope);
        List<EmailToCaseAttachment> etocatch=new List<EmailToCaseAttachment>();
         Map<string, string> strmp=new Map<string, string>();
         strmp.put('temp','test');
         strmp.put('Date','test');
        
        EmailToCaseBean emltobean=new EmailToCaseBean();
         List<String> lststr=new List<String>();
         lststr.add('abc@test.com');
         emltobean.ccAddresses =lststr;
         emltobean.toAddresses =lststr;
         emltobean.fromAddress='abc@sftpl.com'; 
         emltobean.fromName='test';
         emltobean.htmlBody='temp data';
         emltobean.plainTextBody='temp data';
         emltobean.subject='AP_PO';
         emltobean.planSubject='test';
         emltobean.envelopFromAddress='temp';
         emltobean.envelopToAddress='temp';
         emltobean.inReplyTo='temp';
         emltobean.attachments=etocatch;
         emltobean.header=strmp;
        service.getOrCreateContact(emltobean);
    }
}