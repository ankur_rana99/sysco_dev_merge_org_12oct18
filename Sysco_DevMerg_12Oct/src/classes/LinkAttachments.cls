public class LinkAttachments {
    
    @future
    public static void attchDocs(Id recordId, String ContentDocumentId, String Description){
         boolean primaryDoc=false;
         String entityId;
		 system.debug(recordId+':'+ContentDocumentId+':'+Description);
         if(Description==null || Description=='' || !((Description).contains('Primary') || (Description).contains('Additional')))
            return;
        
         if((Description).contains('Primary'))  
         {
             primaryDoc=true;
             entityId=((Description).split('='))[1];
         }
         else
         {
             entityId=((Description).split('='))[1];
         }
		ContentDocumentLink[] cdl=[select id from ContentDocumentLink where ContentDocumentId=:ContentDocumentId and LinkedEntityId=:entityId];
        ContentDocumentLink sdl=new ContentDocumentLink(
            ContentDocumentId=ContentDocumentId,
            LinkedEntityId=entityId,
            ShareType='V',
            Visibility='AllUsers'); 
         if(cdl.size()>0)
         {
         	sdl.id=cdl[0].id;	
         }
         ContentVersion cvu= new ContentVersion(
		     Id=recordId,
             Is_Primary_Doc__c=primaryDoc,
             Description='Attached='+entityId);
              
		upsert sdl;     
      	update cvu;
     
    }
}