@isTest
public class VMApprovalHistoryTest{

    public static testMethod void testFirst(){
      Vendor_Maintenance__c vm = new Vendor_Maintenance__c();
 		vm.Previous_State__c = 'Start';
        vm.Urgent__c = false;
   		insert vm;
        system.debug('vm--'+vm);
        List<Vendor_Maintenance__c> vmList = new List<Vendor_Maintenance__c> ();
        vmList.add(vm);
        VMApprovalHistory.checkUserDOA(vmList);
     }
    }