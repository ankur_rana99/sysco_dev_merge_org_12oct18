@isTest
public class ChangeOwnerSearchControllerTest {
     public static testMethod void testFirst(){
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        OCR_Accuracy_Report_Mapping__c OCRrpt = new OCR_Accuracy_Report_Mapping__c();
        OCRrpt.FieldName_on_Object__c = 'Pickup_Allowance__c';
        OCRrpt.FieldName_on_OCR__c = 'Pickup_Allowance__c';
        OCRrpt.Object_Name__c = 'Invoice__c';
        insert OCRrpt;

        SearchConfiguration__c config=new SearchConfiguration__c();
        config.Name='Temp';
        config.Clone__c=true;
        config.Create__c=true;
        config.CreateInquiry__c=true;
        config.AccessProfile__c='System Administrator,Processor,Exception Resolution';
        config.BulkAssignmentUserType__c='Processor,Administrator';
        config.CriteriaFieldSetName__c='Addi_UI_Common';
        config.ObjectAPIName__c='Invoice__c';
        config.POFlip__c=false;
        config.FilterCriteria__c='Current_State__c=\'Rejected\'';
        config.ResultFieldSetName__c='Addi_UI_Common';
        insert config;
        
         List<CaseManager__c> cts= ESMDataGenerator.createCases(1);
         CaseManager__c cm=cts.get(0); 
         upsert cm;
         List < String > caseId=new  List < String >();
         caseId.add(cm.Id);
        
        Invoice__c inv = ESMDataGenerator.createInvoice('EDI');
       	List < String > invId=new  List < String >();
       	invId.add(inv.Id);
       	
       	User usr = ESMDataGenerator.createUser();
       	insert usr;
       	
       	//ChangeOwnerSearchController.userDetail(String.ValueOf(usr.Id),invId,'Temp');
        
         ChangeOwnerSearchController.userDetail('abc',caseId,'Temp');
         ChangeOwnerSearchController.getUsers('Temp','abc');
         ChangeOwnerSearchController.changeUserAndQueue('abc',caseId,'Temp');
         ChangeOwnerSearchController.getQueues('Temp');
    }
}