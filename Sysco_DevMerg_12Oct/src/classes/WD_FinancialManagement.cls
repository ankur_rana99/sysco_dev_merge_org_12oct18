global class WD_FinancialManagement implements database.Batchable < Sobject > , Schedulable, Database.Stateful, Database.AllowsCallouts {
	global SchedulableContext objSchedulableContext;
	
	global WD_FinancialManagement() {
        this.objSchedulableContext = objSchedulableContext;
    }

    global List < Payment_Term__c > start(Database.BatchableContext BC) {
        map < String, Payment_Term__c > mapExistingTerms = new map < String, Payment_Term__c > ();
        List < Payment_Term__c > lstTerms = new List < Payment_Term__c > ();
        comWorkdayBsvcFinancialManagement.Financial_Management request_x = new comWorkdayBsvcFinancialManagement.Financial_Management();
        comWorkdayBsvc.Response_FilterType responseFilter = new comWorkdayBsvc.Response_FilterType();
        try {
			for (Payment_Term__c objTerm: [SELECT Id, Name, Description__c, Discount_Percentage__c, Net_Days_To_Pay__c, Vendor__c, Transaction_Code__c, Transaction_Type__c, Batch_ID__c, Source_System__c, Error_Message__c, Unique_Key__c, OPCO_Code__c, Discount_Days__c, Due_Days__c, Due_Month__c, Due_Date__c, Payment_Terms_ID__c, Grace_Days__c FROM Payment_Term__c WHERE Source_System__c = 'Workday']) {
                if (objTerm.Payment_Terms_ID__c != null) {
                    mapExistingTerms.put(objTerm.Payment_Terms_ID__c, objTerm);
                }
            }
			System.debug('mapExistingTerms : ' + mapExistingTerms + ' ****Size****: ' + mapExistingTerms.Keyset().size());
            
			comWorkdayBsvc.Response_ResultsType Response_Results = request_x.Get_Payment_Terms(null, null, responseFilter, null).Response_Results;
			System.debug('Response_Results===> ' + Response_Results);
			Integer PageCount = Response_Results.Total_Pages != null ? Integer.ValueOf(Response_Results.Total_Pages) : 0;
			System.debug('PageCount===> ' + PageCount);
            
			for(Integer i = 1; i < PageCount; i++ ){
				responseFilter.As_Of_Effective_Date = date.valueOf(System.Label.WD_As_Of_Effective_Date);
				responseFilter.As_Of_Entry_DateTime = datetime.valueOf(System.Label.WD_As_Of_Entry_DateTime);
				responseFilter.Page_x = String.valueOf(i);
				responseFilter.Count = '999';
				System.debug('responseFilter : ' + responseFilter);
				List < comWorkdayBsvc.Payment_TermType > Payment_TermType = new List < comWorkdayBsvc.Payment_TermType > ();
				if (request_x.Get_Payment_Terms(null, null, responseFilter, null) != null && request_x.Get_Payment_Terms(null, null, responseFilter, null).Response_Data != null && request_x.Get_Payment_Terms(null, null, responseFilter, null).Response_Data.Payment_Term != null) {
					Payment_TermType = request_x.Get_Payment_Terms(null, null, responseFilter, null).Response_Data.Payment_Term;
					System.debug('Payment_TermType : ' + Payment_TermType + ' ****Size****: ' + Payment_TermType.size());
					List < comWorkdayBsvc.Payment_Terms_DataType > lstTermsData = new List < comWorkdayBsvc.Payment_Terms_DataType > ();
					if(Payment_TermType != null && Payment_TermType.size() > 0){
						for (comWorkdayBsvc.Payment_TermType terms: Payment_TermType) {
							lstTermsData.add(terms.Payment_Term_Data);
						}
						System.debug('lstTermsData : ' + lstTermsData + ' ****Size****: ' + lstTermsData.size());
						for (comWorkdayBsvc.Payment_Terms_DataType objData: lstTermsData) {
							Payment_Term__c term = new Payment_Term__c();
							if (mapExistingTerms != null && mapExistingTerms.get(objData.Payment_Terms_ID) != null && mapExistingTerms.containsKey(objData.Payment_Terms_ID)) {
								mapExistingTerms.get(objData.Payment_Terms_ID).Unique_Key__c = objData.Payment_Terms_ID;
                                mapExistingTerms.get(objData.Payment_Terms_ID).Due_Days__c = Decimal.valueOf(objData.Due_Days);
								mapExistingTerms.get(objData.Payment_Terms_ID).Grace_Days__c = Decimal.valueOf(objData.Grace_Days);
								mapExistingTerms.get(objData.Payment_Terms_ID).Discount_Days__c = Decimal.valueOf(objData.Payment_Discount_Days);
								mapExistingTerms.get(objData.Payment_Terms_ID).Discount_Percentage__c = Decimal.valueOf(objData.Payment_Discount_Percent);
								mapExistingTerms.get(objData.Payment_Terms_ID).Name = objData.Payment_Terms_Name;
								mapExistingTerms.get(objData.Payment_Terms_ID).Source_System__c = 'Workday';

								lstTerms.add(mapExistingTerms.get(objData.Payment_Terms_ID));
							} else {
								term.Due_Days__c = Decimal.valueOf(objData.Due_Days);
								term.Grace_Days__c = Decimal.valueOf(objData.Grace_Days);
								term.Discount_Days__c = Decimal.valueOf(objData.Payment_Discount_Days);
								term.Discount_Percentage__c = Decimal.valueOf(objData.Payment_Discount_Percent);
								term.Unique_Key__c = objData.Payment_Terms_ID;
                                term.Payment_Terms_ID__c = objData.Payment_Terms_ID;
								term.Name = objData.Payment_Terms_Name;
								term.Source_System__c = 'Workday';
								lstTerms.add(term);
							}
						}
					}
				}
			}
			System.debug('lstTerms : ' + lstTerms + ' ****Size****: ' + lstTerms.size());
        } catch (exception e) {
            system.debug(e.getMessage());
        }
        return lstTerms;
    }

    global void Execute(Database.BatchableContext objBatchableContext, List < Payment_Term__c > scope) {
        try {
            System.debug('scope===> ' + scope);
            if (scope != null && !scope.isEmpty()) {
                System.debug('scope===> ' + scope);
                upsert scope;
            }
        } catch (exception e) {
            system.debug(e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC) {
		system.debug('=====WD_FinancialManagement finish======');
        String jobId;
        try{
            if(this.objSchedulableContext!= null){
                 jobId=this.objSchedulableContext.getTriggerId();
            }
            List<CronTrigger> objCronTrigger = [SELECT Id, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
            if(objCronTrigger!=null && objCronTrigger.size()>0){
                System.debug('Next fire time: ' + objCronTrigger[0].NextFireTime);
            } else{
                system.debug('***** objCronTrigger is Null ********');
            }
        }catch(Exception ex){
            system.debug(ex.getStackTraceString());
        }
        finally{
            system.debug(' Going to Abort Scheduled Job,JobId ['+jobId+'] Time ['+DateTime.now()+'] ');
            if(this.objSchedulableContext != null) {
                System.abortJob(this.objSchedulableContext.getTriggerId());
            }
            system.debug(' Scheduled Job Aborted Successfully ,JobId ['+jobId+'] Time ['+DateTime.now()+']');
            scheduleNow();
        }
    }

    global void execute(SchedulableContext objSchedulableContext) {

    }
	
	global void scheduleNow(){
        try{
            system.debug('*********** In scheduleNow 0f class WD_FinancialManagement*******'+DateTime.now());
            DateTime objDateTime = datetime.now().addMinutes(1000);
            String cron = '';
            cron += objDateTime.second();
            cron += ' ' + objDateTime.minute();
            cron += ' ' + objDateTime.hour();
            cron += ' ' + objDateTime.day();
            cron += ' ' + objDateTime.month();
            cron += ' ' + '?';
            cron += ' ' + objDateTime.year();
            String jobId=System.schedule('WD_FinancialManagement', cron, new WD_FinancialManagement());
            system.debug(' New Job Scheduled Successfully ,JobId ['+jobId+'] Time ['+DateTime.now()+'] ');
        }catch(Exception ex){
            system.debug(ex.getStackTraceString());
        }
    }

}