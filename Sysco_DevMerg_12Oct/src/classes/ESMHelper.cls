public class ESMHelper {
    public class errorResult {
        public string ValExecMsgKey { get; set; }
        public string errorMessage { get; set; }
        public string type { get; set; }
        public Boolean isCustom { get; set; }
    }
    public class Rule {
        public JSONValue JSONValue { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        public boolean isActive { get; set; }
        public string type { get; set; }
        public String objectName { get; set; }
    }
    public class JSONValue {
        public List<Criteria> criterias { get; set; }
        public List<attributes> attributes { get; set; }
        public boolean alwaysMoveToQC { get; set; }
        public integer qcPerecentage { get; set; }
        public string qcFormFieldsetName { get; set; }
        public string qcFormHeader { get; set; }

    }
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string refField { get; set; }
    }
    public class attributes {
        public string additionalRuleValue { get; set; }
        public string additionalRuleField { get; set; }
    }
    public static Boolean checkRuleMatch(EsmHelper.Rule esmRule, sObject obj) {
        boolean isMatch = false;
        for (EsmHelper.Criteria cre : esmRule.JSONValue.criterias) {
            if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                string fieldValue = string.valueOf(obj.get(cre.field));
                isMatch = isMatch(cre, fieldValue);
            }
            else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                double fieldValue = double.valueOf(obj.get(cre.field));
                isMatch = isMatch(cre, fieldValue);
            }
            else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                boolean fieldValue = boolean.valueOf(obj.get(cre.field));
                isMatch = isMatch(cre, fieldValue);
            }
            else if (cre.type.equalsIgnoreCase('REFERENCE')) {
                /*string userId = string.valueOf(obj.get(cre.field));

                  if (!string.isEmpty(userId)) {
                  User ur = userMap.get(userId);
                  if (ur != null) {
                  string fieldValue = string.valueOf(ur.get(cre.refField));
                  isMatch = isMatch(cre, fieldValue);
                  }
                  }*/
            }
            if (!isMatch) {
                break;
            }
        }
        return isMatch;
    }
    @testVisible
    private static boolean isMatch(Criteria car, string fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == car.Value;
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != car.Value;
        }
        else if (car.operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(car.Value)) {
                List<string> tempList = car.Value.split(',');
                for (string str : tempList) {
                    if (car.type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @testVisible
    private static boolean isMatch(Criteria car, double fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less than')) {
            return fieldValue < double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater than')) {
            return fieldValue > double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= double.valueOf(car.Value);
        }
        return false;
    }
    @testVisible
    private static boolean isMatch(Criteria car, boolean fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(car.Value);
        }
        return false;
    }

    @testVisible
    private static double getRandomNumber(double lower, double upper) {
        return Math.round((Math.random() * (upper - lower)) + lower);
    }
    public class dataHelper {
        @AuraEnabled
        public List<sobject> DataList { get; set; }
        @AuraEnabled
        public List<Columns> Columns { get; set; }
        @AuraEnabled
        public Map<String,String> DataListMap{ get; set; }
        @AuraEnabled
        public String errmsg {get; set; }
    }
    public class dataHelperRecord {
        @AuraEnabled
        public sobject record {get; set; }
        @AuraEnabled
        public List<Columns> Columns { get; set; }
        @AuraEnabled
        public String errmsg {get; set; }
        @AuraEnabled
        public String esmNameSpace {get; set; }
        
    }

    public class Columns {
        @AuraEnabled
        public JSON criteria { get; set; }
        @AuraEnabled
        public JSON requiredCriteria { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String defaultValue { get; set; }
        @AuraEnabled
        public String reference { get; set; }
        @AuraEnabled
        public Boolean readonly { get; set; }
        @AuraEnabled
        public Boolean required { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public Map<String, Object>  metaData{ get; set; }
		@AuraEnabled
        public String searchBy { get; set; }
		@AuraEnabled
        public String searchFilter { get; set; }
    }
    public class Layout {
        public List<Columns> layout { get; set; }
    }
    public class ObjectDescWrapper {
        public Schema.DescribeFieldResult fieldDesc;
        public Schema.DescribeSObjectResult sObjectDesc;
    }

    public class DataMetadataWrapper {
        @AuraEnabled
        public sObject currentObject;
        @AuraEnabled
        public String layoutJson;
        @AuraEnabled
        public Boolean isAccesible;
        @AuraEnabled
        public String error;
        //---Jira # ESMPROD-1261 --| start--- 
        @AuraEnabled
        public String displayError;
        //---Jira # ESMPROD-1261 --| End---
		@AuraEnabled
        public Map<String, String> customLabels;
    }

    public class ExceptionValidationWrapper {
        @AuraEnabled
        public sObject currentObject;
        @AuraEnabled
        public Map<String, String> validationMap;
        @AuraEnabled
        public List<Invoice_Line_Item__c> invLines;
        @AuraEnabled
        public String error;
    }

}