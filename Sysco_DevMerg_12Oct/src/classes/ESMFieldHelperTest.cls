@isTest
public class ESMFieldHelperTest {
    public static testMethod void testFirst(){
    CaseManager__c caseObj=ESMDataGenerator.createCaseTracker();
    ESMFieldHelper.getFieldsByFieldSet('CaseListFieldSet',caseObj,'93i44390149104');
        
   	string objApiName='CaseManager__c';
    string contrfieldApiName='WorkType__c';
    string depfieldApiName='Document_Type__c';
    
    ESMFieldHelper.getDependentOptionsImpl(objApiName,contrfieldApiName,depfieldApiName);
   // ESMFieldHelper.GetPicklistValuesBasedOnRecordType(objApiName,'Request Type','93i44390149104');
    ESMUISetupController.Rule rule=new ESMUISetupController.Rule();
    List<ESMUISetupController.Criteria> criterias=new List<ESMUISetupController.Criteria>();
    ESMUISetupController.Criteria cri=new ESMUISetupController.Criteria();
    cri.type='BOOLEAN';
    cri.operator='equals';
    cri.value='true';
    cri.field='QC_Available__c';
        
    ESMUISetupController.Criteria cri1=new ESMUISetupController.Criteria();
    cri1.type='TEXT';
    cri1.operator='contains in list';
    cri1.value='abc';
    cri1.field='PO_Number__c';
        
             
    ESMUISetupController.Criteria cri2=new ESMUISetupController.Criteria();
    cri2.type='NUMBER';
    cri2.operator='greater or equal';
    cri2.value='2';
    cri2.field='No_Of_Attachments__c';
        
    ESMUISetupController.Criteria cri3=new ESMUISetupController.Criteria();
    cri3.type='TEXT';
    cri3.operator='does not contains in list';
    cri3.value='abc';
    cri3.field='PO_Number__c';
         
    criterias.add(cri);
    criterias.add(cri1);
    criterias.add(cri2);
    criterias.add(cri3);
        
    rule.criterias=criterias;
    caseObj.QC_Available__c=true;
    caseObj.PO_Number__c='abc';
    caseObj.No_Of_Attachments__c=1;
        
    ESMFieldHelper.isRuleMatch(rule,caseObj);
        
    }
}