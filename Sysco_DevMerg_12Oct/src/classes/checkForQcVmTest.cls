@IsTest public class checkForQcVmTest {
    @IsTest(SeeAllData = true) public static void testcheckForQcVms() {
        List<Vendor_Maintenance__c> vml =new List<Vendor_Maintenance__c>();
        vml.add(ESMDataGenerator.invoiceVendorMaintenance('a'));
        vml.add(ESMDataGenerator.invoiceVendorMaintenance('b'));
        checkForQcVm.checkForQC(vml);
    }
}