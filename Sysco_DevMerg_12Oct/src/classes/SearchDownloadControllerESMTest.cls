@isTest
public class SearchDownloadControllerESMTest{
    
       public static testMethod void testFirst(){
           
        System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         
         Account accobj = new Account();
         accobj.Name='TestAccount';
         insert accobj;

    	 Invoice__c invobj = new Invoice__c();    
    	 invobj.Invoice_Date__c = Date.Today();
    	 invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	 invobj.Total_Amount__c = 500.00;
    
    	 System.debug('invobj1' + invobj);
    	 insert invobj;
         

       Test.setCurrentPage(Page.SearchDownloadPageESM);
       ApexPages.currentPage().getParameters().put('searchConfigName', 'Temp');
       ApexPages.currentPage().getParameters().put('key', invobj.Id);

         SearchConfiguration__c config=new SearchConfiguration__c();
        config.Name='Temp';
        config.Clone__c=true;
        config.Create__c=true;
        config.CreateInquiry__c=true;
        config.AccessProfile__c='System Administrator,Processor,Exception Resolution';
        config.BulkAssignmentUserType__c='Processor,Administrator';
        config.CriteriaFieldSetName__c='Addi_UI_Common';
        config.ObjectAPIName__c='Invoice__c';
        config.POFlip__c=false;
        config.FilterCriteria__c='Current_State__c=\'Rejected\'';
        config.ResultFieldSetName__c='Addi_UI_Common';
        insert config;
        
          // ApexPages.currentPage().getParameters().set('searchConfigName','Temp');
		   SearchDownloadControllerESM sdc = new SearchDownloadControllerESM();
		   sdc.limitReached = false;
		   sdc.objectTmp = 'Invoice__c';
           
          
		   sdc.selectedRowIDs = '1';
		   sdc.ObjCode = 'bb';
		   sdc.searchResultFiledSetName = 'cc';
		   sdc.contentType = 'dd';
		   //sdc.isData = false;
           sdc.esmNamespace = 'x';
           sdc.xmlheader = 'bb';
           sdc.endfile = 'cc';
           sdc.con = null;
		   //sdc.fieldNames();
		   sdc.getFieldsSetMembers('invoice');
    }

}