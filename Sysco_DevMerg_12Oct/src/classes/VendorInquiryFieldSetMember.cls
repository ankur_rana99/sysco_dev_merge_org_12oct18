public class VendorInquiryFieldSetMember {

    public VendorInquiryFieldSetMember (Schema.FieldSetMember f) {
        this.DBRequired = f.DBRequired;
        this.fieldPath = f.fieldPath;
        this.label = f.label;
        this.required = f.required;
        this.type = '' + f.getType();
    }

    public VendorInquiryFieldSetMember (Boolean DBRequired) {
        this.DBRequired = DBRequired;
    }

    @AuraEnabled
    public Boolean DBRequired { get;set; }

    @AuraEnabled
    public String fieldPath { get;set; }

    @AuraEnabled
    public String label { get;set; }

    @AuraEnabled
    public Boolean required { get;set; }

    @AuraEnabled
    public String type { get; set; }

	public class UnameMail {
		@AuraEnabled
		public String  UserName {get; set; }
		@AuraEnabled
		public String Email { get; set; }
	}
}