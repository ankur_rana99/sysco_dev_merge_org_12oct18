@isTest
public class EmailControllerTest {
    public static testMethod void testInitializeComponent() {
        EmailTemplate validEmailTemplate = new EmailTemplate();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'Notification';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }

        String interId = TestDataGenerator.createEmailMessage(TestDataGenerator.createCaseTracker()).Id;
        JsonObject obj = new JsonObject();
        obj.interactionId = interId;
        obj.Mailbox = 'AP - Mailbox';
        obj.emailTemplateFolder = 'Notification';
        String jsonString = JSON.serialize(obj);
        Map < String, Object > resp1 = EmailController.initializeComponent(jsonString);
        Map < String, Object > resp2 = EmailController.initializeComponent(null);
        Map < String, Object > resp3 = EmailController.initializeComponent('invalidId');

    }
    public static testMethod void testSendMail() {
        CaseManager__c ct = TestDataGenerator.createCaseTracker();
        String attachId = TestDataGenerator.createContent(ct);
        EmailController.EmailWrapper emailWrapper = new EmailController.EmailWrapper();
        emailWrapper.sendTo = new List < String > {
            'abc@xyz.com'
        };
        emailWrapper.sendCc = new List < String > {
            'pqr@xyz.com'
        };
        emailWrapper.sendBcc = new List < String > {
            'mno@xyz.com'
        };
        emailWrapper.subject = 'Test Subject';
        emailWrapper.attachmentIds = new List < String > {
            attachId
        };

        Test.startTest();
        Map < String, Object > resp1 = EmailController.sendMail(JSON.serialize(emailWrapper), '</tbody></table><p><br></p><p><img src="data:image/png;base64,IUtO9Ff8H6gFBzlKZAhEAAAAASUVORK5CYII="></p>', ct,false);
        // Map<String,Object> resp2=EmailController.sendMail(null,'Test Body',ct);
        Test.stopTest();
    }
    public static testMethod void testQuickResponce() {
    EmailTemplate validEmailTemplate = new EmailTemplate();

    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'Notification';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }

        
        CaseManager__c ct = TestDataGenerator.createCaseTracker();
        String interId = TestDataGenerator.createEmailMessage(ct).Id;
        String attachId = TestDataGenerator.createContent(ct);

        EmailController.EmailWrapper emailWrapper = new EmailController.EmailWrapper();
        emailWrapper.sendTo = new List < String > {
            'abc@xyz.com'
        };
        emailWrapper.sendCc = new List < String > {
            'pqr@xyz.com'
        };
        emailWrapper.sendBcc = new List < String > {
            'mno@xyz.com'
        };
        emailWrapper.subject = 'Test Subject';
        emailWrapper.attachmentIds = new List < String > {
            attachId
        };


        Test.startTest();
       // '00Xm0000000RVyDEAW'
        //Map < String, Object > resp1 = EmailController.quickReplyMethod('quickreply', validEmailTemplate.id, interId, ct.Id);
       // Map < String, Object > resp2 = EmailController.quickReplyMethod('quickreplyall', validEmailTemplate.Id, interId, ct.Id);
        EmailController.getUserId();
        Test.stopTest();
    }
    class JsonObject {
        String interactionId;
        String emailTemplateFolder;
        String Mailbox;
    }

}