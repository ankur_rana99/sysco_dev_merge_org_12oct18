@isTest
public with sharing class HighlightPanelControllerTest {
    public static string esmNamespace = UtilityController.esmNamespace;
    
    public static testMethod void testFirst(){
        
        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP Inquiries';
        caseObj.WorkType__c='PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Requestor_Email__c='highlight@hello.com';
        caseObj.Amount__c=12000;
        caseObj.Status__c='Ready for processing';
		caseObj.Mailbox_Name__c='AP - Mailbox';
        caseObj.escalation__c=true;
        caseObj.Target_TAT_Time__c = datetime.now();
        caseObj.End_Time_with_Sysco__c = datetime.now();
        
        insert caseObj;
        List<String> strLst=new List<String>();
        strLst.add('StandardReadonly');
        strLst.add('StandardReadOnlyOneLine'); 
        strLst.add('AdditionalDetails'); 
        HighlightPanelController.HighlightPanelWrapper hpw = new HighlightPanelController.HighlightPanelWrapper();
        //hpw.fieldList =strLst;
        Set < String > fieldSet = new Set < String > (strLst);
		HighlightPanelController.getRecord(ObjectUtil.getPackagedFieldName('CaseManager__c'), System.JSON.serialize(strLst), caseObj.Id, 2);
        HighlightPanelController.getRecord(ObjectUtil.getPackagedFieldName('CaseManager__History'), System.JSON.serialize(strLst), caseObj.Id, 2);
       
    }
    
     

}