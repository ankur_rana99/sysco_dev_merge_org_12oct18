public class SyscoCORAIndexing {
    @InvocableMethod(label = 'Invoice Fields Assignment' description = 'Invoice Fields Assignment')
    public static void executeInvoiceIndexing(List<Invoice__c> Invoices) {
    	System.debug('***********Calling SyscoCORAIndexing************');
    	//SyscoOnChangeManager manager = new SyscoOnChangeManager();
        //manager.execute(Invoices[0], new List<Invoice_Line_Item__c>(), new List<PO_Line_Item__c>(), new List<GRN_Line_Item__c>() , '', '', '', new Map<object,object>());
        
        SyscoCORAIndexingHelper helper = new SyscoCORAIndexingHelper (Invoices);
        helper.invoiceIndexing(Invoices, true);
        
    }
    
}