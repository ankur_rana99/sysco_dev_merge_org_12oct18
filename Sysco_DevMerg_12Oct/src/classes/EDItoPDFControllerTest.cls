@isTest
public class EDItoPDFControllerTest {
      public static testMethod void testFirst(){
          
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        //sysconfig.Sub_Module__c ='Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
          
        Purchase_Order__c  po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 123;
        insert po;
        
    
          
        OPCO_Code__c op_code = new OPCO_Code__c();
		op_code.OPCO_Code__c = '1001';
		op_code.OPCO_Name__c = 'Corporate';
		insert op_code;
       
          
        Invoice__c invobj = new Invoice__c();       
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);      
        invobj.Total_Amount__c = 500.00;
        invobj.OPCO_Code__c= op_code.Id;
        invobj.Purchase_Order__c=po.Id;
        insert invobj;
          
        Invoice_Line_Item__c LI=new Invoice_Line_Item__c();
        LI.Amount__c=500.00;
        LI.Invoice__c=invobj.Id;
        LI.Product_Service_No__c='123';
        insert LI;
          
        PO_Line_Item__c poLIiteam=new PO_Line_Item__c();
        poLIiteam.Purchase_Order__c=po.Id;
        poLIiteam.Product_Service_No__c='123';
        insert poLIiteam;
            
        Invoice_PO_Detail__c inPODet=new Invoice_PO_Detail__c();
        inPODet.Invoice__c=invobj.Id;
        inPODet.Purchase_Order__c=po.Id;
        insert inPODet;
          
     
          
        System.debug('invobj2' + invobj);
          ApexPages.currentPage().getParameters().put('invid',invobj.Id);
           EDItoPDFController obj=new EDItoPDFController();
      }
}