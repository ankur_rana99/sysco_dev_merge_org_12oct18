@isTest
public class EmailToCaseNotificationManagerTest {
     public static testMethod void testFirst(){
         EmailTemplate e=[select id from EmailTemplate limit 1];
         
         EmailToCaseNotificationManager.EmailToCaseNotification emailToCaseNotification=new EmailToCaseNotificationManager.EmailToCaseNotification();
         EmailToCaseBean emailtoCaseBean=new EmailToCaseBean();
         sobject ct=new CaseManager__c();
         insert ct;
          List<EmailToCaseAttachment> attachments=new  List<EmailToCaseAttachment>();
         EmailToCaseAttachment att=new EmailToCaseAttachment();
         attachments.add(att);
         
         emailToCaseNotification.TemplateId=e.id;
         emailToCaseNotification.caseTracker=ct;
         List<string> toAddresses=new List<string>();
         toAddresses.add('hitakshi.patel@sftpl.com');
         emailToCaseNotification.toAddresses=toAddresses;
         emailToCaseNotification.ccAddresses=toAddresses;
         emailToCaseNotification.bccAddresses=toAddresses;
      
         emailtoCaseBean.attachments=attachments;
         
         EmailToCaseNotificationManager obj=new EmailToCaseNotificationManager();
       obj.sendMail(emailToCaseNotification,emailtoCaseBean);
         
         
     }
}