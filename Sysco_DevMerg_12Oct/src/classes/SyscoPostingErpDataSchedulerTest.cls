@isTest
public class SyscoPostingErpDataSchedulerTest{
    
       public static testMethod void testFirst(){
           System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
           
           Sysco_SystemConfiguration__c custset = new Sysco_SystemConfiguration__c();
        custset.name = 'ERPConfig';
        custset.EndPointURL__c='https://api.sysco.com/fi/FTR/Invoice/v1.0/mostInvoice';
        custset.SOQLQueryLimit__c='200';
        custset.TO_Email_Address__c = 'chiranjeevi.kunamalla@test.com';
        custset.WebserviceRetryLimit__c = 2;
        custset.SyscoAPIC__c = 'https://api.syscoo.com';
        custset.client_id__c = 'testid';
        custset.client_secret__c = 'testsecrete';
        insert custset;
           
           OPCO_Code__c opcocode = new OPCO_Code__c();
        opcocode.OPCO_Name__c='059';
            opcocode.OPCO_Code__c='059';
            opcocode.State__c = 'test State';
            opcocode.AddressStreet__c = 'test Address';
            opcocode.City__c='test City';
            opcocode.Zip__c='xxxxx123';
            insert opcocode;
		
           Invoice__c inv = new Invoice__c();
        inv.Document_Type__c='Non PO Invoice' ;
        inv.Invoice_No__c='2345';
        inv.Invoice_Type__c='M';
            inv.Amount__c=2000;
            inv.Invoice_Date__c=Date.Today();
            inv.Priority__c='High';
            inv.Input_Source__c='Manual';
            inv.OPCO_Code__c=opcocode.id;
            inv.Tax__c=0;
            inv.Payment_Due_Date__c=Date.Today();
            inv.Term_Start_Date__c=Date.Today();
           
            inv.Purchase_Order_Number__c='PO-1001';
            inv.WorkType__c = 'Merchandize';
            inv.Allowance_Type__c='test';
            inv.Current_State__c = 'Ready for Posting';
            //inv.Cora_Ref_No__c = 'test12';
        insert inv;
        System.debug('invoice inserted');
        
	   SyscoPostingErpDataScheduler speds = new SyscoPostingErpDataScheduler();
       SyscoPostingErpDataScheduler speds1= new SyscoPostingErpDataScheduler(); 
	   speds1.execute(null);
    }

}