@isTest
public class UserMasterTriggerTest{

    public static testMethod void testMethod1()
    {
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
    
         List<String> buList=new List<String>();
         buList.add('approvertest1@sftpl.co.in');
         buList.add('approvertest2@sftpl.co.in');
         buList.add('approvertest3@sftpl.co.in');
         buList.add('approvertest4@sftpl.co.in');                  
         
         List<User_Master__c> umList= ESMDataGenerator.insertBuyerUser(buList); 
         
         
         
    }

    public static testMethod void testMethod2()
    {
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super1@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        //tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
    
         List<String> buList=new List<String>();
         buList.add('approvertest1@sftpl.co.in');
         buList.add('approvertest2@sftpl.co.in');
         buList.add('approvertest3@sftpl.co.in');
         buList.add('approvertest4@sftpl.co.in');                  
         
         List<User_Master__c> umList= ESMDataGenerator.insertBuyerUser(buList); 
         
         
         
    }
}