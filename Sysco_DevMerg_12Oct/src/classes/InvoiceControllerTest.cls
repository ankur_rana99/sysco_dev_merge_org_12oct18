@isTest
public class InvoiceControllerTest {
    
    public static String esmNameSpace = UtilityController.esmNameSpace;
  
    public static testmethod void testmethod1()
    {
            system.assert(true,true); 
             System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;

             
            ESMDataGenerator.insertSystemConfiguration();
            
            ESMDataGenerator.insertInvoiceConfiguration();
            Invoice__c inv  = ESMDataGenerator.insertPOInvoice();
          //  inv.User_Action__c = 'Document Reclassify';
            update inv;
                     
            Purchase_Order__c  po = ESMDataGenerator.insertPurchaseOrder();
            PO_Line_Item__c poL = ESMDataGenerator.insertPOLineItem(po.id);
            GRN__c grn =  ESMDataGenerator.insertGRN(po.id);
            GRN_Line_Item__c grnL =  ESMDataGenerator.insertGRNLineItem(grn.id,po.id,poL.id);
            Invoice_Line_Item__c invLineUpdate = ESMDataGenerator.insertInvoiceLineItem(inv.id,po.id,poL.id,grn.id,grnL.id);
              
            Invoice_PO_Detail__c invPoObj = new Invoice_PO_Detail__c();
            invPoObj.Purchase_Order__c=po.id;
            invPoObj.Invoice__c=inv.id;
            insert invPoObj;
                          
            List<Purchase_Order__c> POList = new List<Purchase_Order__c>();
            POList.add(po);
            system.debug('poList---------------'+poList);
            
            List<Invoice_PO_Detail__c > lstInvPoToInsert = new List<Invoice_PO_Detail__c >();
            lstInvPoToInsert .add(invPoObj );
            String esmNamespace = UtilityController.esmNameSpace ;
         
            
            List<Invoice_PO_Detail__c > lstInvPoToDelete = new List<Invoice_PO_Detail__c >();
            lstInvPoToDelete.add(invPoObj );
            system.debug('lstInvPoToDelete-----------------'+lstInvPoToDelete);
                        
            List<Invoice_Line_Item__c> InvLineItemList = new List<Invoice_Line_Item__c>();
            InvLineItemList.add(invLineUpdate);
            system.debug('-----InvLineItemList'+InvLineItemList);
          
            
            List<PO_Line_Item__c> POLineItemList = new List<PO_Line_Item__c>();
            POLineItemList.add(poL);
        
            system.debug('-----POLineItemList'+POLineItemList);
            List<Invoice_Line_Item__c> otherChargesList = new List<Invoice_Line_Item__c>();
            otherChargesList.add(invLineUpdate);
            system.debug('-----otherChargesList'+otherChargesList);
        
            Attachment attach = new Attachment();
            attach.name = 'test.pdf';
            attach.body = Blob.valueOf(('test data testing'));
            attach.parentId = inv.id;
            insert attach;                                                                                                             
            
             ContentVersion cv = new ContentVersion();
             cv.Title = 'Demopdf';
             cv.Is_Primary_Doc__c = false;
             cv.PathOnClient ='test';
             Blob b=Blob.valueOf('Unit Test Attachment Body');
             cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
             insert cv;    
            
            cv = [SELECT ContentDocumentId FROM ContentVersion where Id = :cv.Id]; 
            update cv ;
            system.debug('----cv--'+cv);
            
            List<ContentVersion> cvList = new List<ContentVersion>();          
            cvList.add(cv);
            system.debug('-----cvList'+cvList);
        
            
            InvoiceController.getInvoice(inv.id);  
            InvoiceController.saveInvoiceData(inv);
          
            InvoiceController.insertAttachentData('',cvList,inv.id);      
            
            InvoiceController.saveInvoice(inv,InvLineItemList,POList,otherChargesList,'NEW','',cvList,null);
         //   InvoiceController.saveData(inv,'NEW','',cvList,null,esmNameSpace +'Invoice__c');        
                 
            InvoiceController.saveInvoice(inv,InvLineItemList,POList,otherChargesList,'EDIT','',cvList,null);
      //      InvoiceController.saveData(inv,'EDIT','',cvList,null,esmNameSpace +'Invoice__c');
            
                                  
            InvoiceController inc = new InvoiceController();
            inc.validateInvoice();
    }
         
}