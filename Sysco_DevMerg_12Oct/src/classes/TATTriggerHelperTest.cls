@isTest
public class TATTriggerHelperTest{
    static String namespace = UtilityController.esmNameSpace;
    public static testMethod void testFirst(){
        system.assert(true,true);
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        List<TATHelper.Criteria> lstCriteria = new List<TATHelper.Criteria>();
        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'contains in list';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'starts with';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'end with';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'contains';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'does not contain';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Exception_Reason__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'contains in list';
        wrapCriteria.type = 'MULTIPICKLIST';
        wrapCriteria.value = 'Currency Mismatch';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'equals';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'not equal to';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'less than';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'greater than';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'less or equal';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);
        
        wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'Amount__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'greater or equal';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        Tat_Rule_Config__c tatruleconfig=new Tat_Rule_Config__c();
        tatruleconfig.IsActive__c=true;
        tatruleconfig.Object_Name__c=namespace + 'Invoice__c';
        tatruleconfig.Rule_Name__c='RFP';
        tatruleconfig.Order__c=6;
        tatruleconfig.JSON__c ='{"template":"","tatTimeUnit":"Days","tatTime":1,"sendNotification":false,"notificationTimeUnit":"Minutes","notificationTime":0,"criterias":[{"value":"Ready For Processing","type":"PICKLIST","operator":"equals","objName":"'+ namespace + 'Invoice__c","field":"'+ namespace + 'Current_State__c"}],"calendar":"'+bhs.get(0).id+'","additionalEmails":""}';
        insert tatruleconfig;
        
        CaseManager__c cm=new CaseManager__c();
        cm.Amount__c=100;
        cm.Current_State__c='Pending For Archival';    
        cm.Tat_Rule__c=tatruleconfig.Id;
        //cm.Previous_Performer__c='005-tesmp';
        insert cm;
        
       /* CaseManager__c cm1=new CaseManager__c();
        cm1.Amount__c=100;
        cm1.Current_State__c='Completed';
       
        insert cm1;*/
        
        List<CaseManager__c> lstcm=new List<CaseManager__c>();
        lstcm.add(cm);
        
        Map<Id, CaseManager__c> cmmap=new Map<Id, CaseManager__c>();
          cmmap.put(cm.id,cm);
      //  cmmap.put(cm1.id,cm1);
        
       
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
    
        TATTriggerHelper.setActualTatTime(lstcm);
        TATTriggerHelper.setActualTatTime(lstcm,cmmap);
        TATTriggerHelper.isSameMonth(system.today(),system.today());
        TATTriggerHelper.isSameWeek(system.today(),system.today());
        TATTriggerHelper.calculateTAT(lstcm);
        
        for (TATHelper.Criteria cre1 : lstCriteria)
        {
            TATTriggerHelper.isMatch(cre1,'test');
            TATTriggerHelper.isMatch(cre1,123);
            TATTriggerHelper.isMatch(cre1,true);
        }
        
        TATTriggerHelper.calculateStateChange(lstcm,cmmap);
        TATTriggerHelper.createCaseTransition(lstcm);
        TATTriggerHelper.createTATNotification(lstcm);
        TATTriggerHelper.removeTatNotification(lstcm,cmmap);
        TATTriggerHelper.getDateDiffByBusinessHours(stdBusinessHours.Id,system.today(),system.today());
       // TATTriggerHelper.getMiliSecondByUnit('test',123);
    }
    
    public static testMethod void testsecond(){
        system.assert(true,true);
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];
        Tat_Rule_Config__c tatruleconfig=new Tat_Rule_Config__c();
        tatruleconfig.IsActive__c=true;
        tatruleconfig.Object_Name__c=namespace + 'CaseManager__c';
        tatruleconfig.Rule_Name__c='create';
        tatruleconfig.Order__c=10;
        tatruleconfig.JSON__c ='{"template":"","tatTimeUnit":"Days","tatTime":1,"sendNotification":false,"notificationTimeUnit":"Minutes","notificationTime":0,"criterias":[{"value":"Ready For Processing","type":"NUMBER","operator":"equals","objName":'+namespace + '"CaseManager__c","field":'+namespace + '"Current_State__c"}],"calendar":"'+bhs.get(0).id+'","additionalEmails":""}';
        insert tatruleconfig;
        
        CaseManager__c cm=new CaseManager__c();
        cm.Amount__c=100;
        cm.Current_State__c='Pending For Archival';
        cm.Tat_Rule__c=tatruleconfig.Id;
        insert cm;
        
      /*  CaseManager__c cm1=new CaseManager__c();
        cm1.Amount__c=100;
        cm1.Current_State__c='Completed';
        
        cm1.Tat_Rule__c=tatruleconfig.Id;
        insert cm1;
        */
        List<CaseManager__c> lstcm=new List<CaseManager__c>();

        lstcm.add(cm);
        
        Map<Id, CaseManager__c> cmmap=new Map<Id, CaseManager__c>();
        cmmap.put(cm.id,cm);

    
        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'equals';
        wrapCriteria.type = 'String';
        wrapCriteria.value = '';
        TATTriggerHelper.calculateStateChange(lstcm,cmmap);
        TATTriggerHelper.isMatch(wrapCriteria,'test');
        
        TATTriggerHelper.isMatch(wrapCriteria,true);
    }
    
    public static testMethod void testThird(){
        system.assert(true,true);
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];
        Tat_Rule_Config__c tatruleconfig=new Tat_Rule_Config__c();
        tatruleconfig.IsActive__c=true;
        tatruleconfig.Object_Name__c=namespace + 'CaseManager__c';
        tatruleconfig.Rule_Name__c='create';
        tatruleconfig.Order__c=1;
        tatruleconfig.JSON__c ='{"template":"","tatTimeUnit":"Days","tatTime":1,"sendNotification":false,"notificationTimeUnit":"Minutes","notificationTime":0,"criterias":[{"value":"Ready For Processing","type":"BOOLEAN","operator":"equals","objName":'+namespace + '"CaseManager__c","field":'+namespace + '"Current_State__c"}],"calendar":"'+bhs.get(0).id+'","additionalEmails":""}';
        insert tatruleconfig;
        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'greater than';
        wrapCriteria.type = 'Number';
        wrapCriteria.value = '';

        TATTriggerHelper.isMatch(wrapCriteria,'test');
       
        TATTriggerHelper.isMatch(wrapCriteria,true);
    }
        
    public static testMethod void test4(){
        system.assert(true,true);
        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = namespace + 'User_Action__c';
        wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'does not contains in list';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        TATTriggerHelper.isMatch(wrapCriteria,'test');
        TATTriggerHelper.isMatch(wrapCriteria,123);
        TATTriggerHelper.isMatch(wrapCriteria,true);
    }
}