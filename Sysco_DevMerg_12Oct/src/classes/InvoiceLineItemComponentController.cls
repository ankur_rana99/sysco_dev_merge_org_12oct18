public class InvoiceLineItemComponentController {
	@AuraEnabled
	public static EsmHelper.dataHelper getInvoiceLineDataServer(String strObjectName, String jsonObj, String objId, String type, String layout) {
		system.debug('Called*****'+layout);
        system.debug('type*****'+type);
        EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		try {
			String namespace = UtilityController.esmNameSpace;
			List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
			List<sObject> objList = new List<sObject> ();
			if (type.equalsIgnoreCase('AdditionalLine'))
			{
				objList = CommonFunctionController.fillOtherChargesList(namespace,objId);
			} else {
				objList = CommonFunctionController.fillInvLineItem(namespace,objId);
			}
			List<sobject> grnLineList = CommonFunctionController.getGRNline(objId);
			if (layout == 'EDIT')
			{
                system.debug('objList*****'+objList);
				for (sObject invl : objList)
				{
					if (invl.getSobject(namespace + 'po_line_item__r') != null && invl.getSobject(namespace + 'po_line_item__r').get(namespace + 'grn_match_required_formula__c') != null &&
						Boolean.valueOf(invl.getSobject(namespace + 'po_line_item__r').get(namespace + 'grn_match_required_formula__c')))
					{
						Integer isGrnMatch = 0;
						for (sObject grnLoop : grnLineList)
						{
							if (grnLoop.get(namespace + 'PO_Line_Item__c') == invl.get(namespace + 'PO_Line_Item__c'))
							{
								isGrnMatch++;
								if (isGrnMatch > 1)
								{
									invl.put(namespace + 'GRN__c', null);
									invl.putSObject(namespace + 'GRN__r', null);
									invl.put(namespace + 'GRN_Line_Item__c', null);
									invl.putSObject(namespace + 'GRN_Line_Item__r', null);
									break;
								} else
								{
									invl.put(namespace + 'GRN__c', grnLoop.get(namespace + 'GRN__c'));
									invl.putSObject(namespace + 'GRN__r', grnLoop.getSobject(namespace + 'GRN__r'));
									invl.put(namespace + 'GRN_Line_Item__c', grnLoop.Id);
									invl.putSObject(namespace + 'GRN_Line_Item__r', grnLoop);

								}
							}
						}
					}
				}
			}

			dh = UtilityController.getWrapperFromJson(objList, strObjectName, cols);

			List<PO_GRN_Line_Invoice_Line_Mappings__c> cusmdt = [select Invoice_Line_Item_Field_Name__c, PO_GRN_Line_Item_Field_Name__c from PO_GRN_Line_Invoice_Line_Mappings__c];
			system.debug('cusmdt*****'+cusmdt);
            Map<String, String> mpp = new Map<String, String> ();
			for (PO_GRN_Line_Invoice_Line_Mappings__c cml : cusmdt) {
				mpp.put(String.valueOf(cml.Invoice_Line_Item_Field_Name__c), String.valueOf(cml.PO_GRN_Line_Item_Field_Name__c));
			}
            System.debug('mpp****'+mpp);
			dh.DataListMap = mpp;
			System.debug(dh);
		} catch(Exception e) {
			dh.errmsg = ExceptionHandlerUtility.customDebug(e,'InvoiceLineItemController');
		}
        system.debug('dh*****'+dh);
		return dh;
	}
	@AuraEnabled
	public static EsmHelper.dataHelper addRowForInvoiceLine(String strObjectName, String jsonObj, String objId) {

		List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
		String selectedRows = UtilityController.getAllSelectedFields(cols);
		Map<String, String> objMap = new Map<String, String> ();
		String whereClause = 'where Invoice__c = :objId';
		String query = 'select ' + selectedRows + ' from  ' + strObjectName + ' ' + whereClause;
		String[] arr = selectedRows.split(',');
		for (Integer i = 0; i < arr.size(); i++)
		{
			objMap.put(arr[i], '');
		}
		List<sObject> objList = new List<sObject> ();
        System.debug('query---' + query);
		if (UtilityController.isFieldAccessible(strObjectName, selectedRows))
		{
			objList = Database.query(query);
		}
		System.debug('---' + objList);
		//finalObjList.add(objMap);
		EsmHelper.dataHelper dh = UtilityController.getWrapperFromJson(objList, strObjectName, cols, objMap);
		System.debug('dh---' + dh);
        return dh;
	}
	@AuraEnabled
	public static Invoice_Line_Item__c getNewRecord() {
		Invoice_Line_Item__c inl = new Invoice_Line_Item__c(Invoice_Line_Item_No__c = '055');
		/*String  querry= 'SELECT ' + CommonFunctionController.getAllFieldsOfObject('Invoice_Line_Item__c') + ' From Invoice_Line_Item__c limit 1';
		  return Database.query(querry);*/
		return inl;
	}

}