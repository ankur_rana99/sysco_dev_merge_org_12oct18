@isTest(seeAllData = True)
public class PurchaseOrderMasterTriggerTest{
    
     public static testMethod void testFirst(){    
     
            Purchase_Order__c po = new Purchase_Order__c();
            po.Vendor_NotFound_InVMD__c= 'Y';
            po.Transaction_Code__c = 'POH';
            po.Transaction_Type__c = 'I' ;
            po.Batch_ID__c          = '20200000000000';
            po.Source_System__c     = 'Workday';
            po.Created_Date__c     = '2018-08-02T08:16:50';
            po.Unique_Key__c     = 'US1005-PO2018100001';
           //po.OPCO_Code__r.OPCO_Code__c  = 'US1005'; 
            po.PO_No__c  = 'PO2018100001';
            po.PO_Date__c  = Date.today();
          // po.Anticipated_Date__c = '';
            po.Vendor_Number__c = '20000328';
            po.Payment_Vendor_Terms__c = 'Z005';
            po.Currency__c = 'USD';
            po.Status__c = 'Issued';
            po.FreightAmount__c = 0;
            po.Other_Chargers_Surcharges__c = 0 ;
            po.Taxes__c = 0;
            po.Total_Amount__c = 100;
            po.Buyer_ID__c = '10113474';
            po.Bill_To_ID__c = '10099762';
            po.Bill_To_Name__c = 'Derik David Giovannoni';
            po.Bill_To_State__c = 'Texas';
            po.Bill_To_Zip__c = '77077';
            po.Ship_To_Address1__c = '123 Main Street';
            po.Ship_To_City__c = 'Houston';
            po.Ship_To_Country__c = 'United States of America';
            po.Ship_To_ID__c = '10099762' ;
            po.Ship_To_Name__c = 'Derik Giovannoni';
            po.Ship_To_State__c = 'Texas';
            po.Ship_To_Zip__c = '77077';                        
            Account acc = new account();
            acc.Name = 'TestCase';
            acc.Vendor_No__c = '20000328';
            insert acc;
        Test.startTest();    
           Database.insert(po); 
        Test.stopTest();   
          
    }
    
}