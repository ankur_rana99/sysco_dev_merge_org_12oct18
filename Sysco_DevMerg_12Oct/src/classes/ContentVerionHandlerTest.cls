@isTest
public class ContentVerionHandlerTest {
     public static testMethod void testFirst(){
         
         List<ContentVersion> newContentVersion=new  List<ContentVersion>();
          ContentVersion cvu= new ContentVersion(
              versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body'),
             Is_Primary_Doc__c=false,
             Description='Primary=a1wm0000000tvRS',
              Origin='H',
              PathOnClient='abc.pdf',
              title='\\abc.jpg'
          );
         newContentVersion.add(cvu);
         
         System_Configuration__c config=new System_Configuration__c();
         config.Module__c='Invoice__c';
         config.Sub_Module__c='Allow_Image_Size';
         config.Value__c='10';
         insert config; 
        
         
         System_Configuration__c config1=new System_Configuration__c();
         config1.Module__c='Invoice__c';
         config1.Sub_Module__c='Allowed_Primary_Format';
         config1.Value__c='.html,.pdf,.tif,.tiff,.jpg,.png,.gif,.jpeg,.jpeg,.PDF,.xls,.XLS,.xlsx,.XLSX,.PNG,.csv';     
         insert config1;
         
         ContentVerionHandler.updateFlag(newContentVersion, null);
     }
}