@isTest
public class LinkAttachmentsTest {
    
     public static testMethod void testFirst(){        
          Account testAccount = new Account();
          testAccount.Name='Test Account' ;
          insert testAccount;
          
          ContentVersion cvu= new ContentVersion(
             versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body'),
             Is_Primary_Doc__c=false,
             Description='Primary='+testAccount.id,
             Origin='H',
             PathOnClient='abc.pdf'
          );
          insert cvu;
          
          ContentVersion cvup=[SELECT Id, ContentDocumentId, Description FROM ContentVersion where Id=:cvu.id];
          
          
          system.debug('cvup.ContentDocumentId '+cvup.ContentDocumentId);
          LinkAttachments.attchDocs(cvu.id,cvup.ContentDocumentId,cvu.Description);
      }

}