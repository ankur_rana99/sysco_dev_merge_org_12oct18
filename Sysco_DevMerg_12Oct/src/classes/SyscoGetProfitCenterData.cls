global class SyscoGetProfitCenterData implements database.Batchable<Sobject>,Schedulable,Database.Stateful,Database.AllowsCallouts{

private SchedulableContext objSchedulableContext;
private Profit_Center__c pc;
private Profit_Center__c pcempty;
private List<Profit_Center__c> pcList;
private List<Profit_Center__c> emptypcList = new List<Profit_Center__c>(); 

global SyscoGetProfitCenterData(){
       
}

global List<Profit_Center__c> start(Database.BatchableContext BC) {
    try{
        String dateFormatString = 'yyyy-MM-dd';
        pc = new Profit_Center__c();
        pcempty = new Profit_Center__c();
        pcList = new List<Profit_Center__c>();
        System.debug(' Method Called PC...');
        
        Date today = Date.today();
        Datetime dt1 = Datetime.newInstance(today.year(), today.month(),today.day());
        String tday = dt1.format(dateFormatString);
        
        Date previousday = Date.today() - 1;
        Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(),previousday.day());
        String prevday = dt2.format(dateFormatString);
        
        WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('ProfitCenter');
        if(WEEndpoint.isfirstrun__c == 'Y'){
           prevday = '';                       
        }
            
        String XMLString;
        HttpRequest feedRequest = new HttpRequest();
        feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday);
        feedRequest.setMethod('GET');
        Http http = new Http();
        HTTPResponse ProftCenterResponse = http.send(feedRequest);
        System.debug(ProftCenterResponse.getBody());
        XMLString = ProftCenterResponse.getBody();
        DOM.Document doc=new DOM.Document();
        doc.load(XMLString);
        DOM.XmlNode rootNode=doc.getRootElement();
        parseXML(rootNode);
        if(pc!=null && pc!= pcempty)
           pcList.add(pc); 
    }catch(exception e){
        system.debug(e.getMessage());
    }  
    return pcList;       
    
}

global void Execute(Database.BatchableContext objBatchableContext,List<Profit_Center__c> pcen){
    try{ 
        if(pcen.size()>0 && pcen != emptypcList) {  
            system.debug('pcen.size()' + pcen.size());
            system.debug('pcen' + pcen);           
            upsert pcen Reference_ID__c;
        }
        else
            system.debug('No Records to Process');                  
                
    }catch(exception e){
        system.debug(e.getMessage());
    }
}

global void finish(Database.BatchableContext BC) { 
    WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('ProfitCenter');
    System.debug('Outside if ' + WEEndpoint.isfirstrun__c);
    if(WEEndpoint.isfirstrun__c == 'Y'){
        System.debug('insside if ' + WEEndpoint.isfirstrun__c);
       WEEndpoint.isfirstrun__c = 'N';  
       Update WEEndpoint;                        
    }
}
    
Private void parseXML(DOM.XMLNode node) {
    if(node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if(node.getName()=='Report_Entry'){
            if(pc!=null && pc!= pcempty)
               pcList.add(pc);
            pc = new Profit_Center__c();  
        }
        if(node.getName()=='name'){
            pc.Profit_Center_Name__c=node.getText().trim();
            if(pc.Profit_Center_Name__c.length() > 79)
                pc.Name=pc.Profit_Center_Name__c.substring(0,80);
            else
                pc.Name=pc.Profit_Center_Name__c;
        }
        if(node.getName()=='referenceID')
            pc.Reference_ID__c=node.getText().trim();
        if(node.getName()=='Inactive')
            pc.Inactive__c=node.getText().trim();  
    }
    System.debug('Pc------->' + pc);
    for (Dom.XMLNode child: node.getChildElements()) {
        parseXML(child);
    }
}

global void execute(SchedulableContext objSchedulableContext) {
     SyscoGetProfitCenterData syscoPC  = new SyscoGetProfitCenterData();
         Integer batchSize = 2000; 
         system.database.executebatch(syscoPC,batchSize); 
    }

}