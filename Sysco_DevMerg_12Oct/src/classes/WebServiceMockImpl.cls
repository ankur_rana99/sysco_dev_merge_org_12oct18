@isTest
global class WebServiceMockImpl implements HttpCalloutMock{
    Boolean  isMockResponseSuccessful;  // set by constructor, used to vary the mockresponse
    
    public WebServiceMockImpl(){
        isMockResponseSuccessful = true;
    }   //default Constructor
    
    //Parameterise Constructor : to Test a catch block for callout exception
    public WebServiceMockImpl(Boolean isMockResponseSuccessful){
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
    }
    
    global HttpResponse respond(HTTPRequest req){
        system.debug('req===endPoint=============='+req.getEndpoint());
        string get_EndPoint = req.getEndpoint();
        HttpResponse res = new HttpResponse();
        
        if(this.isMockResponseSuccessful) {
            res.setStatus('OK');
            res.setStatusCode(200);
        }else {
            res.setStatusCode(400);
            res.setStatus('Bad request');
        }
        
        if(get_EndPoint.contains('Cost_Center')){
            res.setBody('<wd:Report_Data xmlns:wd="urn:com.workday.report/bsvc"><wd:Report_Entry><wd:name>BT - Accounting - Corporate - Sysco</wd:name><wd:referenceID>551606451</wd:referenceID><wd:Company_Restrictions wd:Descriptor="US6451 Sysco Corporation"><wd:ID wd:type="WID">a41d1ba91aed014e393c872ce1ceb509</wd:ID><wd:ID wd:type="Organization_Reference_ID">US6451</wd:ID><wd:ID wd:type="Company_Reference_ID">US6451</wd:ID></wd:Company_Restrictions><wd:Inactive>0</wd:Inactive></wd:Report_Entry><wd:Report_Entry><wd:name>BT - Application Development - Corporate - Sysco</wd:name><wd:referenceID>550756451</wd:referenceID><wd:Company_Restrictions wd:Descriptor="US6451 Sysco Corporation"><wd:ID wd:type="WID">a41d1ba91aed014e393c872ce1ceb509</wd:ID><wd:ID wd:type="Organization_Reference_ID">US6451</wd:ID><wd:ID wd:type="Company_Reference_ID">US6451</wd:ID></wd:Company_Restrictions><wd:Inactive>0</wd:Inactive></wd:Report_Entry></wd:Report_Data>');        
            system.debug('response for Cost_Center =======' +res);
        }else if(get_EndPoint.contains('Spend_Categories')){
            res.setBody('<wd:Report_Data xmlns:wd="urn:com.workday.report/bsvc"><wd:Report_Entry><wd:Spend_Category_Name>100% Deductible Meals</wd:Spend_Category_Name><wd:referenceID>SC_100%_DEDUCTIBLE_MEALS</wd:referenceID><wd:Inactive>0</wd:Inactive></wd:Report_Entry><wd:Report_Entry><wd:Spend_Category_Name>3D Insurance</wd:Spend_Category_Name><wd:referenceID>SC_INSURANCE_-_3_D</wd:referenceID><wd:Inactive>0</wd:Inactive></wd:Report_Entry></wd:Report_Data>');
            system.debug('response for Spend_Categories =======' +res);
        }else if(get_EndPoint.contains('Projects')){
            res.setBody('<wd:Report_Data xmlns:wd="urn:com.workday.report/bsvc"><wd:Report_Entry><wd:projectName>Agreement Billback Accuracy Improvements</wd:projectName><wd:referenceID>BT.003183</wd:referenceID><wd:Inactive_-_Current>0</wd:Inactive_-_Current></wd:Report_Entry><wd:Report_Entry><wd:projectName>BIS Compliance Upgrade Plus Availability and DMZ Security</wd:projectName><wd:referenceID>BT.003302</wd:referenceID><wd:Inactive_-_Current>0</wd:Inactive_-_Current></wd:Report_Entry></wd:Report_Data>');
            system.debug('response for Projects =======' +res);
        }else if(get_EndPoint.contains('Profit_Center')){
            res.setBody('<wd:Report_Data xmlns:wd="urn:com.workday.report/bsvc"><wd:Report_Entry><wd:name>PUS6440 - SBS, Division of Sysco Resources Services, LLC</wd:name><wd:referenceID>PC_PUS6440</wd:referenceID><wd:Inactive>0</wd:Inactive></wd:Report_Entry><wd:Report_Entry><wd:name>Sysco EV5 Payroll Company</wd:name><wd:referenceID>CUSTOM_ORGANIZATION-6-2485</wd:referenceID><wd:Inactive>0</wd:Inactive></wd:Report_Entry></wd:Report_Data>');
            system.debug('response for Profit_Center=======' +res);
        }else if(get_EndPoint.contains('Companies')){
            res.setBody('<wd:Report_Data xmlns:wd="urn:com.workday.report/bsvc"><wd:Report_Entry><wd:name>A.M.Briggs,Inc.</wd:name><wd:Employer_s_Federal_ID_Number>US1138</wd:Employer_s_Federal_ID_Number><wd:Inactive>0</wd:Inactive><wd:Currency wd:Descriptor="USD"><wd:ID wd:type="WID">9e996ffdd3e14da0ba7275d5400bafd4</wd:ID><wd:ID wd:type="Currency_ID">USD</wd:ID><wd:ID wd:type="Currency_Numeric_Code">840</wd:ID></wd:Currency></wd:Report_Entry></wd:Report_Data>');
            system.debug('response for Companies=======' +res);
        }
        return res;
    }  
}