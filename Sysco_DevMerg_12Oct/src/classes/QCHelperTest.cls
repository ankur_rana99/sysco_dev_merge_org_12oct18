@isTest
public class QCHelperTest {
	public static testmethod void TestMethod1() {
	
	QCHelper.Option QCopt=new QCHelper.Option();
	QCopt.value='test';
	QCopt.text='temp';
	QCHelper.Option QCopt1=new QCHelper.Option('test1','test2');

	List<QCHelper.Option> optlst=new List<QCHelper.Option>();
	List<string> strlst=new List<string>();

	QCHelper.FieldInfo QCfldInfo=new QCHelper.FieldInfo();
	QCfldInfo.label='test';
	QCfldInfo.apiName='test1';
	QCfldInfo.type='test2';
	QCfldInfo.pickListValue=optlst;
	QCfldInfo.operator=strlst;
	

	QCHelper.Criteria cri=new QCHelper.Criteria();
	cri.field='test';
	cri.operator='==';
	cri.value='temp';
	cri.type='test1';
	cri.refField='test2';
	List<QCHelper.Criteria> crtlst=new List<QCHelper.Criteria>();
		
	QCHelper.JSONValue QCjson=new QCHelper.JSONValue();
	QCjson.qcFormFieldsetName='test';
	QCjson.qcFormHeader='test1';
	QCjson.alwaysMoveToQC=true;
	QCjson.qcPerecentage=3;
	QCjson.criterias=crtlst;
	

	QCHelper.QCRule QCrulwrp=new QCHelper.QCRule();
	QCrulwrp.ruleName='temp';
	QCrulwrp.order=2;
	QCrulwrp.id='test';
	QCrulwrp.isActive=true;
	QCrulwrp.type='test2';
	QCrulwrp.objectName='test3';
	QCrulwrp.JSONValue=QCjson;

	
	QCHelper.QCFormResponse QCfrmRes=new QCHelper.QCFormResponse();
	QCfrmRes.qcFormFieldsetName='test';
	QCfrmRes.qcFormHeader='test2';

	QCHelper.objectsAvailaible obj=new QCHelper.objectsAvailaible();
	obj.label='test';
	obj.apiName='test1';
	}
 }