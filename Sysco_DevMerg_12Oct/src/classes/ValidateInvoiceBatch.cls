global with sharing class ValidateInvoiceBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable{

    global String esmNameSpace = UtilityController.esmNameSpace;
    global String childObjName = esmNameSpace+'Invoice__c';
    
    global ValidateInvoiceBatch(){
        
    }
    global ValidateInvoiceBatch(SchedulableContext objSchedulableContext){
        
    }
   
    // Start Method
    global List<sObject> start(Database.BatchableContext BC){
        Invoice_Configuration__c invConf = CommonFunctionController.getInvoiceConfiguration();
         if(invConf.Auto_Validation_Current_States__c != null && invConf.Auto_Validation_Current_States__c != ''){
             List<String> state = new List<String>();
             state = invConf.Auto_Validation_Current_States__c.split(',');
             if(UtilityController.isFieldAccessible(esmNameSpace+'Invoice__c',null)) {
                return Database.query('SELECT '+CommonFunctionController.getAllFieldsOfObject(childObjName)+' FROM '+esmNameSpace+'Invoice__c WHERE '+esmNameSpace+'Current_State__c IN :state LIMIT 500');
             } else {
                return null;
             }
         } else {
            return null;
         }
    }
      
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject>scope) {
        // Logic to be Executed batch wise      
        
        if(scope != null) {
            for(sObject inv : scope) {
                if(inv != null) {
                    String invId = String.valueOf(inv.get('Id'));
                    //fetch inovice line item
                    List<Invoice_Line_Item__c> invLineItemList = CommonFunctionController.fillInvLineItem(esmNameSpace, invId);
                    inv.put(esmNameSpace+'User_Action__c','Validate');
                          
                    //fetch parent object Tracker
                    //List<akritivtlm__Tracker__c> trkLst = CommonFunctionController.getSobject(String.valueOf(inv.get('Tracker_Id__c')),'akritivtlm__Tracker__c');
                  
                    //fetch other charges
                    List<Invoice_Line_Item__c> otherChargesList = CommonFunctionController.fillOtherChargesList(esmNameSpace,invId);
                  
                    //fetch purchase order
                    List<Purchase_Order__c> lstPO;
                    Set<String> setPoId = new Set<String>();
                    List<Invoice_PO_Detail__c> invPoList = new List<Invoice_PO_Detail__c>();
                    if(Invoice_PO_Detail__c.getSObjectType().getDescribe().isAccessible() 
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Id.isAccessible()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isAccessible()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isAccessible()) {
                        invPoList = [Select Id,Invoice__c,Purchase_Order__c from Invoice_PO_Detail__c where Invoice__c =:invId];
                    } else {
                        throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                    }
                    
                    for(Invoice_PO_Detail__c invPo : invPoList){
                        setPoId.add(invPO.Purchase_Order__c);
                    }
                    CommonFunctionController cfObj = new CommonFunctionController();
                    InvoiceValidationExceptionController invValExeObj = new InvoiceValidationExceptionController();
                    
                    // Fetch Purchase Order
                    lstPO = CommonFunctionController.getPOList(esmNameSpace, setPoId);

                    Map<object,object> validationMap = new Map<object,object>();
                    
                    
                    Map<string,string> validMap = invValExeObj.executeValidationException(inv, invLineItemList, otherChargesList, lstPO,false);
                    
                    if(validMap.get('jsonValidationExceptionMsg') != null){
                        if(UtilityController.isFieldUpdateable(esmNameSpace+'Invoice_Line_Item__c',null)) {
                            update InvLineItemList;
                        }
                    }
    
                }
            }
            if(UtilityController.isFieldUpdateable(esmNameSpace+'Invoice__c',null)) {
                update scope;
            }
        }
    }
 
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext objSchedulableContext) {
        ValidateInvoiceBatch viBat = new ValidateInvoiceBatch(objSchedulableContext);
        system.database.executebatch(viBat, 50); 
    }
}