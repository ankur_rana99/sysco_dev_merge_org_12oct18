/*
* Name           : CaseSelector.apxc
* Author         : Atul Hinge
* Description    : used as a abstraction layer over CaseTracker object
*/

public class CaseSelector extends SobjectSelector{
static final String userId = UserInfo.getUserId();
    public CaseSelector(){
        super.addRelationShipFields(new Set<String>{'Vendor__r.Name'});
       // super.addRelationShipQueries(new Set<String>{'Histories','Emails'});
    }
    public CaseSelector(Set<String> relationShipFields,Set<String> relationShipQueries){
        if(relationShipFields !=null && relationShipFields.size() != 0){
            super.addRelationShipFields(relationShipFields);
        }
        if(relationShipQueries !=null && relationShipQueries.size() != 0){
            super.addRelationShipQueries(relationShipQueries);
        }
    }
    public Schema.SObjectType getSObjectType(){
        return CaseManager__c.sObjectType;
    }
    public List<CaseManager__c> selectCasesByIds(Set<Id> idSet){
        return (List<CaseManager__c>)super.selectSObjectsById(idSet);
    }
    public List<CaseManager__c> selectCasesByClause(String clause){
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    public List<CaseManager__c> selectSharedCases(String whereClause){
        
        String clause=' OwnerId != \''+CaseSelector.userId+'\'and '+ObjectUtil.getPackagedFieldName('Previous_Queue_Name__c__c')+'!= null ';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    public List<CaseManager__c> selectClosedCases(String whereClause){
        string statusCompleted='Completed';
        String clause=' OwnerId =\''+CaseSelector.userId+'\' and '+ObjectUtil.getPackagedFieldName('Current_State__c')+' =\'Completed\'' ;
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    public List<CaseManager__c> selectMyTaskCases(String whereClause){
        string statusCompleted='Completed';
        String clause=' OwnerId =\''+CaseSelector.userId+'\' and Current_State__c !=\'Completed\'';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    public List<CaseManager__c> selectUnassignedCases(String whereClause){
         
        String clause=' '+ObjectUtil.getPackagedFieldName('Previous_Queue_Name__c__c')+'!= null';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select All Cases
    */
    public List<CaseManager__c> selectAllCases(){
        return (List<CaseManager__c>)super.selectSObjectsByAll();
    }
    public void addRelationShip(Set<String> relationShipNames){
         super.addRelationShipQueries(relationShipNames);
    }
   
}