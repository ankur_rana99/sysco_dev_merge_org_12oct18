@isTest
public class SyscoGetProjectDataTest{
    
    public static testMethod void testFirst(){    
        WorkDay_EndPoints__c sysWED=new WorkDay_EndPoints__c();
        sysWED.Name='ProjectData';
        sysWED.isfirstrun__c = 'Y';
        sysWED.Endpoint_URL__c='callout:Sysco_Projects/ccx/service/customreport2/sysco4/ISU%20FDM%20Projects/Foundation_Data_-_Projects?Project_Hierarchies!WID=a41d1ba91aed0183969f845135d02b63&Include_Subordinate_Project_Hierarchies=1&';
        insert sysWED;
                 
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());
        
        SyscoGetProjectData getprojData  = new SyscoGetProjectData();
        Integer batchSize = 200; 
        system.database.executebatch(getprojData,batchSize);       
        Test.StopTest();  
    }    
}