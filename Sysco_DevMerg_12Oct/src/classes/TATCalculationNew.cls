public class TATCalculationNew {
    @InvocableMethod(label = 'Primary TAT Calculation' description = 'Primary TAT Calculation')
    public static void calculateNewTAT(List<Invoice__c> Invoices) {
        calculateTAT(Invoices, 'Invoice__c');
    }
    public static void calculateTAT(List<Invoice__c> invList, String objectName) {
        try {
            Invoice_Configuration__c invconf=Invoice_Configuration__c.getOrgDefaults();
            String calid;
            if (invconf!=null)
            {
                calid=invconf.TAT_Calendar_ID__c;
            }
            List<Invoice__c> updtinvTrackeList = new List<Invoice__c>();
            // Datetime in 'America/El_Salvador' TimeZone (GMT Time - 6 Hours)
            DateTime pstartdate = Datetime.now().addHours(-6);
            System.debug('Processing Start Date = ' + pstartdate);
            for (Invoice__c invTrackerNew : invList) {
                Invoice__c UpdatedInvoice = new Invoice__c();   
                //DateTime pstartdate =DateTime.valueOf(invTrackerNew.get('Processing_Start_Date__c'));   
                integer sthrs=Integer.valueOf(Datetime.now().format('HH','America/El_Salvador'));
                integer stmin=Integer.valueOf(Datetime.now().format('mm','America/El_Salvador'));
                system.debug('calid :'+ calid +'--pstartdate :'+ pstartdate+'--sthrs : '+sthrs + '--stmin : '+stmin);
                String dayOfWeek = pstartdate.format('E','America/El_Salvador');
                Datetime targetTATDateTime;
                integer tattime = 3;
                Integer tathours = 27;
                long twodaysmilisecond = tathours * 60 * 60 * 1000L;
                System.debug('Day of Week : ' + dayOfWeek);
                if (dayOfWeek=='Sunday' || dayOfWeek=='Saturday' || dayOfWeek=='Sun' || dayOfWeek=='Sat')
                {
                    system.debug('Weekends');
                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
                    DateTime TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
                    system.debug('TATDateTime---if---'+TATDateTime);
                    UpdatedInvoice.Primary_Target_TAT_Date__c = TATDateTime;
                }
                // Within SHIFT WINDOW
                else if((sthrs >= 8 && stmin >= 1) && sthrs <= 24){
                    system.debug('Within Shift Window AND SAME DAY');
                    tattime = 4;
                    tathours = 36;
                    twodaysmilisecond = tathours * 60 * 60 * 1000L;
                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
                    DateTime TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
                    system.debug('TATDateTime---Else if1---'+TATDateTime);
                    UpdatedInvoice.Primary_Target_TAT_Date__c = TATDateTime;
                }
                else if (sthrs<=8){
                    system.debug('Before Shift Window 00 AM to 08 AM');
                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
                    DateTime TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
                    system.debug('TATDateTime---Else if2---'+TATDateTime);
                    UpdatedInvoice.Primary_Target_TAT_Date__c = TATDateTime;
                }
                system.debug('targetTATDateTime :'+ targetTATDateTime+'--twodaysmilisecond : '+twodaysmilisecond);
                UpdatedInvoice.Id = invTrackerNew.Id;
               // UpdatedInvoice.Processing_Start_Date__c = pstartdate;
                updtinvTrackeList.add(UpdatedInvoice);
                
            }
            update updtinvTrackeList;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATCalculationNew', ex.getMessage(), ex.getStackTraceString());
        }
    }
}