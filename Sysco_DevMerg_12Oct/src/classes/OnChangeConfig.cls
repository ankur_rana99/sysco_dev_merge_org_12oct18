public interface OnChangeConfig {
    //boolean execute(SObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<PO_Line_Item__c> pOLineItemList, String field, String val, String childNameSpace);
	boolean execute(SObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<PO_Line_Item__c> pOLineItemList, List<GRN_Line_Item__c> grnLineItemList,String field,String value, String childNameSpace,Map<object,object> extraParamMap);
}