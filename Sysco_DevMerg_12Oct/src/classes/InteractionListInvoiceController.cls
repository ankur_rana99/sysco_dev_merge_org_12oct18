/*
 * Authors    : Parth Lukhi
 * Date created : 17/08/2017 
 * Purpose      : This InterActionListController is used for getting Interactions (EmailMessages)
 *            records,attachments objects for particular Case Id or Interaction Item
 * Dependencies :  NA
 * -------------------------------------------------
 * Modifications:
                Date:  09/11/2017 
                Purpose of modification:  PUX-235 : Getting Contact Name on interction 
                Method/Code segment modified: getContactListbyInter
 * -------------------------------------------------
 * Modifications:
                Date:  18/12/2017 
                Purpose of modification:  PUX-505 : Maintain attachment @ interaction level   
 * Modifications:
                Date:  23/01/2018 
                Purpose of modification:  PUX-620 : Is Private functionality with notes feature   
                
*/

public without sharing class InteractionListInvoiceController {
    /*
     * Authors: Parth Lukhi
     * Purpose:  This method is used for fetchin Interaction List based in CaseId
     * Dependencies:  EmailMessage Object
     * 
     *   Start : This method is used for fetchin Interaction List based in CaseId
     */
    @AuraEnabled
    public static List<CaseManager__c> getInteractionByCase(String invoiceId, Integer pageNumber, Integer recordsToDisplay) {
        /***Start PUX-211  Added Read__c***/
       // List < EmailMessage > interList;
    //    Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
    //    InteractionWrapper iw = new InteractionWrapper();
        List < CaseManager__c > cmList = new List < CaseManager__c >(); 
        try {
            //PUX-505 start
        
            //PUX-620
            String currentUserId = UserInfo.getUserId();
            
       //     iw.emails = (List < EmailMessage > ) database.query('SELECT ActivityId,BccAddress,CcAddress,CreatedDate,FromAddress,FromName,HasAttachment,Headers,HtmlBody,Id,Incoming,IsDeleted,MessageDate,MessageIdentifier,ReplyToEmailMessageId,Status,Subject,SystemModstamp,TextBody,ThreadIdentifier,ToAddress,ValidatedFromAddress FROM EmailMessage where RelatedToId =:caseId AND (Is_Private__c = false OR CreatedById =: currentUserId) order by CreatedDate desc  Limit ' + recordsToDisplay + ' OFFSET ' + offset);
            //PUX-505 End
      //      system.debug('####'+iw.emails);
         //   iw.total = (database.countQuery('SELECT count() FROM EmailMessage where RelatedToId =:caseId AND (Is_Private__c = false OR CreatedById =: currentUserId) Limit 2010'));
       //     iw.pageSize = recordsToDisplay;
        //    iw.page = pageNumber;
              cmList =   (List < CaseManager__c > )database.query('select id,name,(SELECT  id, CreatedDate, TextBody, Subject, FromName, FromAddress, ToAddress, HasAttachment, Incoming FROM Emails order by CreatedDate desc), User_Action__c from CaseManager__c where Invoice__c = :invoiceId order by createdDate desc');
            
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }

        /***End PUX-211  Added Read__c***/
        return cmList;

    }

    /*
        Authors: Rahul Pastagiya
        Purpose: To wrap pagination related values
        Dependencies : None
    */
    public class InteractionWrapper {
        @AuraEnabled public Integer pageSize {
            get;
            set;
        }
        @AuraEnabled public Integer page {
            get;
            set;
        }
        @AuraEnabled public Integer total {
            get;
            set;
        }
        @AuraEnabled public List < EmailMessage > emails {
            get;
            set;
        }
    }

    /*
     * Authors: Parth Lukhi
     * Purpose:   This method is used for fetching Attachment List based in CaseId or Interaction Id
     * Dependencies:  ContentDocumentLink,ContentDocumentLink Object
     * 
     * Start : This method is used for fetching Attachment List based in CaseId or Interaction Id
     */
    @AuraEnabled
    public static List<ContentDocumentLink> getAttachments(String id) {
        //String passedId;
      
        String docQuery='SELECT  ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, ContentDocument.Owner.Name,LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:id';       
       
       List<ContentDocumentLink> contList;
       try{
            contList=(List<ContentDocumentLink>)database.query(docQuery);
        }
        catch(Exception ex){
             ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        
        return contList;
    }
    /*
     * End : This method is used for fetching Attachment List based in CaseId or Interaction Id
     */

    /*
     * Authors: Parth Lukhi
     * Purpose:    PUX-235  This method is used for fetching ContactList  based on emailAdd
     * Dependencies:  Contact Object
     * 
     * Start : PUX-235  This method is used for fetching ContactList  based on emailAdd
     */
    @AuraEnabled
    public static List < Contact > getContactsByInteraction(String caseId, String interId, String emailAdd) {
        List < Contact > contactList = new List < Contact > ();
        String interactionId;
        List < Contact > contList;
        String emailParam = '';
        Integer cnt = 0;
        try {
            //List<String> emailList=emailAdd.split(',');
            List < String > emailList = emailAdd.split('[,]{1}[\\s]?');
           
            String docQuery = 'SELECT Email,Id,Name FROM Contact where Email In :emailList';
            contList = (List < Contact > ) database.query(docQuery);
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        return contList;
    }
    /*
     * End : PUX-235 This method is used for fetching ContactList  based on emailAdd
     */
    /*
     * Authors: Parth Lukhi
     * Purpose:    This method is used for marking interaction as read 
     * Dependencies:  EmailMessage Object
     * 
     * Start : This method is used for marking interaction as read 
     */
    @AuraEnabled
    public static boolean makeReadMail(String interactionId) {
        
        String docQuery = 'SELECT Id,'+ObjectUtil.getPackagedFieldName('Read__c')+' FROM EmailMessage where Id = :interactionId';
        List < EmailMessage > interList;
        try {
            interList = (List < EmailMessage > ) database.query(docQuery);

            if (interList.size() > 0) {
               
                EmailMessage email = interList.get(0);
                //email.Read__c = true;
                update interList;
                return true;
            }
        } catch (Exception ex) {
            ExceptionHandlerUtility.writeException('InterActionListController', ex.getMessage(), ex.getStackTraceString());
        }
        return false;
    }
    /*
     * End : This method is used for fetching ContactList  based on emailAdd
     */
}