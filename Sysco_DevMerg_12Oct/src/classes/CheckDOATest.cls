@IsTest public class CheckDOATest {
    @IsTest(SeeAllData = true) public static void testCheckDOA1() {
        List<String> bul=new List<String>();
        bul.add('approvertest1@sftpl.co.in');
        bul.add('approvertest2@sftpl.co.in');
        bul.add('approvertest3@sftpl.co.in');
        bul.add('approvertest4@sftpl.co.in');
        List<User_Master__c> umll= ESMDataGenerator.insertBuyerUser(bul);
        
        List<DOA_Matrix__c> daom =new List<DOA_Matrix__c>();
        DOA_Matrix__c dao=new DOA_Matrix__c();
        dao.Name ='dao1';
        dao.DOA_Limit__c =100;
        dao.Condition__c='PO_RFPI';
        dao.Approver_User__c =umll.get(0).Id;
        dao.Supervisor__c =umll.get(1).Id;
        daom.add(dao);
        
        dao=new DOA_Matrix__c();
        dao.Name ='dao2';
        dao.Condition__c='PO_RFPI';
        dao.DOA_Limit__c =200;
        dao.Approver_User__c =umll.get(1).Id;
        dao.Supervisor__c =umll.get(2).Id;
        daom.add(dao);
        
        dao=new DOA_Matrix__c();
        dao.Name ='dao3';
        dao.Condition__c='PO_RFPI';
        dao.DOA_Limit__c =10000000;
        dao.Approver_User__c =umll.get(2).Id;
        dao.Supervisor__c =umll.get(3).Id;
        daom.add(dao);
        insert daom;
        ESMDataGenerator.CreateRuleAlwaysMoveToQCAmount();  
        invoice__c inv = ESMDataGenerator.invoiceNPOInsert();
        inv.Requestor_Buyer__c = umll.get(0).Id;
        insert inv;
        inv.Current_Approver__c = umll.get(0).Id;
        update inv;
        inv.User_Action__c ='Approve';
        system.debug('1app---'+inv);
        update inv;
        system.debug('2app---'+inv);
        inv.User_Action__c ='Approve';
        update inv;
        system.debug('3app---'+inv);
        inv.User_Action__c ='Reject';
        update inv;
    }
    @IsTest(SeeAllData = true) public static void testCheckDOA2() {
        List<String> bul=new List<String>();
        bul.add('approvertest1@sftpl.co.in');
        bul.add('approvertest2@sftpl.co.in');
        bul.add('approvertest3@sftpl.co.in');
        bul.add('approvertest4@sftpl.co.in');
        List<User_Master__c> umll= ESMDataGenerator.insertBuyerUser(bul);
        
        List<DOA_Matrix__c> daom =new List<DOA_Matrix__c>();
        DOA_Matrix__c dao=new DOA_Matrix__c();
        dao.Name ='dao1';
        dao.DOA_Limit__c =100;
        dao.Condition__c='PO_RFPI';
        dao.Approver_User__c =umll.get(0).Id;
        dao.Supervisor__c =umll.get(1).Id;
        daom.add(dao);
        
        dao=new DOA_Matrix__c();
        dao.Name ='dao2';
        dao.Condition__c='PO_RFPI';
        dao.DOA_Limit__c =200;
        dao.Approver_User__c =umll.get(1).Id;
        dao.Supervisor__c =umll.get(2).Id;
        daom.add(dao);
        
        dao=new DOA_Matrix__c();
        dao.Name ='dao3';
        dao.Condition__c='PO_RFPI';
        dao.DOA_Limit__c =10000000;
        dao.Approver_User__c =umll.get(2).Id;
        dao.Supervisor__c =umll.get(3).Id;
        daom.add(dao);
        insert daom;
        ESMDataGenerator.CreateRuleAlwaysMoveToQCAmount();  
        invoice__c inv = ESMDataGenerator.invoiceNPOInsert();
        inv.Requestor_Buyer__c = umll.get(0).Id;
        insert inv;
        inv.Current_Approver__c = umll.get(0).Id;
        update inv;
        inv.User_Action__c ='Approve';
        system.debug('1app---'+inv);
        update inv;
        system.debug('2app---'+inv);
        inv.User_Action__c ='Approve';
        update inv;
        system.debug('3app---'+inv);
        inv.User_Action__c ='Approve';
        update inv;
        List<Invoice__c> invl =new List<Invoice__c>();
        invl.add(inv);
        QCCalculation.checkForQC(invl, 'Invoice__c');
    }
}