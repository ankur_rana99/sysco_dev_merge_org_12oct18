/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ESMMetadataServiceTest {

    public static string esmNamespace = UtilityController.esmNamespace;
    public static testmethod void testCase1() {
        system.assert(true,true);
        ESMMetadataService esmmetadata = new ESMMetadataService();
        ESMMetadataService.ReadRecordTypeResult obj1 = new ESMMetadataService.ReadRecordTypeResult();
        ESMMetadataService.readRecordTypeResponse_element obj2 = new ESMMetadataService.readRecordTypeResponse_element();
        ESMMetadataService.ReadCustomFieldResult obj3 = new ESMMetadataService.ReadCustomFieldResult();
        ESMMetadataService.readCustomFieldResponse_element obj4 = new ESMMetadataService.readCustomFieldResponse_element();
        ESMMetadataService.RecordTypePicklistValue obj5 = new ESMMetadataService.RecordTypePicklistValue();
        ESMMetadataService.RecordType obj6 = new ESMMetadataService.RecordType();
        ESMMetadataService.ValueSettings obj7 = new ESMMetadataService.ValueSettings();
        ESMMetadataService.CustomValue obj8 = new ESMMetadataService.CustomValue();
        ESMMetadataService.ValueSetValuesDefinition obj9 = new ESMMetadataService.ValueSetValuesDefinition();
        ESMMetadataService.ValueSet obj10 = new ESMMetadataService.ValueSet();
        ESMMetadataService.GlobalPicklistValue obj11 = new ESMMetadataService.GlobalPicklistValue();
        ESMMetadataService.PicklistValue obj12 = new ESMMetadataService.PicklistValue();
        ESMMetadataService.Picklist obj13 = new ESMMetadataService.Picklist();
        ESMMetadataService.FilterItem obj14 = new ESMMetadataService.FilterItem();
        ESMMetadataService.LookupFilter obj15 = new ESMMetadataService.LookupFilter();
        ESMMetadataService.CustomField obj16 = new ESMMetadataService.CustomField();
        ESMMetadataService.readMetadata_element obj17 = new ESMMetadataService.readMetadata_element();
        ESMMetadataService.SessionHeader_element obj18 = new ESMMetadataService.SessionHeader_element();
        ESMMetadataService.AllOrNoneHeader_element obj19 = new ESMMetadataService.AllOrNoneHeader_element();
        ESMMetadataService.CallOptions_element obj20 = new ESMMetadataService.CallOptions_element();
        ESMMetadataService.LogInfo obj21 = new ESMMetadataService.LogInfo();
        ESMMetadataService.DebuggingHeader_element obj22 = new ESMMetadataService.DebuggingHeader_element();
        ESMMetadataService.DebuggingInfo_element obj23 = new ESMMetadataService.DebuggingInfo_element();
        ESMMetadataService.describeMetadata_element obj24 = new ESMMetadataService.describeMetadata_element();
        ESMMetadataService.describeMetadataResponse_element obj25 = new ESMMetadataService.describeMetadataResponse_element();
        ESMMetadataService.DescribeMetadataObject obj26 = new ESMMetadataService.DescribeMetadataObject();
        ESMMetadataService.DescribeMetadataResult obj27 = new ESMMetadataService.DescribeMetadataResult();
        ESMMetadataService.MetadataPort obj28 = new ESMMetadataService.MetadataPort();
        
        obj1.getRecords();
        obj2.getResult();
        obj3.getRecords();
        obj4.getResult();
    }
}