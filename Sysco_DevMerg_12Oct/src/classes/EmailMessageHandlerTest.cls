@isTest
public class EmailMessageHandlerTest{
    public static testMethod void testFirst(){
    CaseManager__c caseObj=ESMDataGenerator.createCaseTracker();
         EmailMessage newEmailMessage=new EmailMessage();
         newEmailMessage.Is_Note__c=false;
         newEmailMessage.RelatedToId=caseObj.Id;
         newEmailMessage.Read__c=false;
         insert newEmailMessage;
         
         List<EmailMessage> newEmailMessageList=new List<EmailMessage>();
         newEmailMessageList.add(newEmailMessage);
         
		EmailMessageHandler.calUnreadEmailCntAndUpdateCase(newEmailMessageList);
        EmailMessageHandler.markOutboundMessageAsRead(newEmailMessageList);
    }
}