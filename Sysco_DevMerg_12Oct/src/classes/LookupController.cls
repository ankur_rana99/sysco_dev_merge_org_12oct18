public class LookupController {
	@AuraEnabled
	public static List<sObject> fetchLookUpValues(String searchKeyWord, String ObjectName, String searchBy, String searchFilter, sObject mainObject) {
		system.debug('ObjectName-->' + ObjectName);
		String searchKey = '%';
		if (searchKeyWord != null)
		{
			searchKey = searchKeyWord + searchKey;
		}
		searchKey = '%' + searchKey + '%';
		system.debug('searchKey-->' + searchKey);
		List<sObject> returnList = new List<sObject> ();
		String searchFilterQuery = '';
		if (searchFilter != null && searchFilter != '')
		{
			searchFilterQuery = ' and ';
			if (searchFilter.contains('{SOBJECT.'))
			{
				String searchFilterLater ='';
				while (searchFilter.contains('{SOBJECT.'))
				{
					System.debug('getter---' + searchFilter.substringAfter('{SOBJECT.').substringBefore('}'));
					System.debug('mainObject--' + mainObject);
					searchFilterQuery = searchFilterQuery + searchFilter.substringBefore('{SOBJECT.');
					if (mainObject != null && mainObject.get(searchFilter.substringAfter('{SOBJECT.').substringBefore('}')) != null)
					{
						searchFilterQuery = searchFilterQuery +'\''+ String.escapeSingleQuotes(String.valueOf(mainObject.get(searchFilter.substringAfter('{SOBJECT.').substringBefore('}'))))+'\'';
					} else {
						searchFilterQuery = searchFilterQuery + 'null';
					}

					searchFilter = searchFilter.substringAfter('{SOBJECT.').substringAfter('}');
					searchFilterLater = searchFilter;
					System.debug('searchFilter later' + searchFilter);
				}
				searchFilterQuery = searchFilterQuery +  searchFilterLater;
			}
			else
			{
				searchFilterQuery = searchFilterQuery + searchFilter;
			}
		}
		// Create a Dynamic SOQL Query For Fetch Record List with LIMIT 999
		String searchByQuery ='';
		if (!searchBy.equalsIgnoreCase('name'))
		{
			searchByQuery = searchBy +',Name';
		}else{
			searchByQuery = searchBy;
		}
		String sQuery = 'select id, ' + searchByQuery + ' from ' + ObjectName + ' where ' + searchBy + ' LIKE :searchKey ';
		if (searchFilterQuery != null && searchFilterQuery != '')
		{
			sQuery = sQuery + ' ' + searchFilterQuery + ' ';
		}
		sQuery = sQuery + ' order by ' + searchBy + ' limit 100';
		System.debug('sQuery455--' + sQuery);
		List<sObject> lstOfRecords = Database.query(sQuery);

		for (sObject obj : lstOfRecords) {
			returnList.add(obj);
		}
		return returnList;
	}
}