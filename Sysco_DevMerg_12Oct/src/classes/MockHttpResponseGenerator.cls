@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://api.syscoo.com', req.getEndpoint());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"936e810c-8beb-32ff-824c-3f11ca8f439e","scope":"am_application_scope default","token_type":"Bearer","expires_in":3600}');
        res.setStatusCode(200);
        return res;
        
    }
}