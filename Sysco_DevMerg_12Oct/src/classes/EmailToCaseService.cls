/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: Landing class for email, When system recevied email
  Dependencies: 
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/

public with sharing class EmailToCaseService implements Messaging.InboundEmailHandler {

    /*
      Authors: Chandresh Koyani
      Purpose: When salesforce recevied new email, it will call this method.
      - Extract email fields
      - Extract attachments
      - Extract envelope fields
      - Extract body 
      - Call Manager class for processing.
      Dependencies: Method of InboundEmailHandler interface.
     
     */
    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        EmailToCaseBean emailtoCaseBean = new EmailToCaseBean();
        emailtoCaseBean.debugLog=new List<string>();
        Integer emailAttachmentSize = 0;
        try {
                    emailtoCaseBean.appendIntoDebugLog('handleInboundEmail','Extract Data Start');
                    emailAttachmentSize = checkAttachmentSize(email);
                    emailtoCaseBean.fromAddress = email.fromAddress;
                    emailtoCaseBean.toAddresses = email.toAddresses;
                    emailtoCaseBean.ccAddresses = email.ccAddresses;
                    emailtoCaseBean.fromName = email.fromName;
        
                    emailtoCaseBean.envelopFromAddress = envelope.fromAddress;
                    emailtoCaseBean.envelopToAddress = envelope.toAddress;
        
                    emailtoCaseBean.attachments = extractAttachments(email);
        
                    emailtoCaseBean.messageId = email.messageId;
                    emailtoCaseBean.references = email.references;
                    emailtoCaseBean.inReplyTo = email.inReplyTo;
        
                    emailtoCaseBean.header = getHeaderMap(email.headers);
        
                    
                    if (email.plainTextBody != null) {
                        emailtoCaseBean.plainTextBody = email.plainTextBody.escapeXml();
                    } else {
                        emailtoCaseBean.plainTextBody = '';
                    }
        
                    if (email.htmlBody != null) {
                        emailtoCaseBean.htmlBody = email.htmlBody;
        
                    }
                    else {
                        emailtoCaseBean.htmlBody = '';
                    }
                    emailtoCaseBean.subject = email.subject;
        
                    if (!string.isEmpty(emailtoCaseBean.subject)) {
                        emailtoCaseBean.planSubject = EmailToCaseSettingHelper.getPlanSubject(emailtoCaseBean.subject);
                    }
                    String objectName = getObjectName(emailtoCaseBean.toAddresses);
                    if(objectName==null || objectName==''){
                        objectName = getObjectName(emailtoCaseBean.ccAddresses);
                    } 
                    emailtoCaseBean.appendIntoDebugLog('handleInboundEmail','Extract Data End');
                    if (objectName != null)
                    {
                        EmailToCaseManager manager = new EmailToCaseManager();
                        if(emailAttachmentSize < 10485760){
                            manager.processCase(emailtoCaseBean, objectName);
                        }else{
                                EmailTemplate templateId = [Select id,Name from EmailTemplate where name = 'Email Size and Attachment Restriction' Limit 1];
                                EmailToCaseNotificationManager notificationManager = new EmailToCaseNotificationManager();
                                EmailToCaseNotificationManager.EmailToCaseNotification notificationObj = new EmailToCaseNotificationManager.EmailToCaseNotification();
                                if(templateId != null){
                                    notificationObj.templateId = templateId.id;
                                }
                                notificationObj.ccAddresses = null;
                                notificationObj.bccAddresses = null;
                                notificationObj.caseTracker = null;
                                Contact contObj = getOrCreateContact(emailtoCaseBean);
                                notificationObj.contact = contObj;
                                OrgWideEmailAddress owa = [select id, Address from OrgWideEmailAddress Limit 1];
                                if(owa != null){
                                    notificationObj.orgWideId = owa.id; 
                                }
                                notificationObj.attachments = null;
                                notificationObj.toAddresses = null;
                                System.debug('Ignore mail notification processing--1');
                                notificationManager.sendIgnoreEmailForAttachmentSize(notificationObj, emailtoCaseBean);
                        }
                    }else
                        {
                            emailtoCaseBean.appendIntoDebugLog('handleInboundEmail','NoObjectFound');
                        }
            
        }
        catch(EmailToCaseException ex) {
            ExceptionHandlerUtility.writeEmailException(emailtoCaseBean,ex.caseManager, ex.getMessage(), ex.getStackTraceString());
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeEmailException(emailtoCaseBean,null, ex.getMessage(), ex.getStackTraceString());
        }
        return result;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get attachment from InboundEmail and create list of EmailToCaseAttachment.cls
      Dependencies: called from "handleInboundEmail" method to fill attachment.
     
     */
    private List<EmailToCaseAttachment> extractAttachments(Messaging.InboundEmail email) {
        List<EmailToCaseAttachment> emailAttachments = new List<EmailToCaseAttachment> ();
        if (email.textAttachments != null) {
            for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
                EmailToCaseAttachment emailAttachment = new EmailToCaseAttachment();
                emailAttachment.body = textAttachment.Body;
                emailAttachment.fileName = textAttachment.fileName;
                emailAttachment.headers = getHeaderMap(textAttachment.headers);
                emailAttachment.mimeTypeSubType = textAttachment.mimeTypeSubType;
                emailAttachment.type = 'Text';
                emailAttachments.add(emailAttachment);
            }
        }
        if (email.binaryAttachments != null) {
            for (Messaging.InboundEmail.binaryAttachment binaryAttachment : email.binaryAttachments) {
                EmailToCaseAttachment emailAttachment = new EmailToCaseAttachment();
                emailAttachment.binaryBody = binaryAttachment.Body;
                emailAttachment.fileName = binaryAttachment.fileName;
                emailAttachment.headers = getHeaderMap(binaryAttachment.headers);
                emailAttachment.mimeTypeSubType = binaryAttachment.mimeTypeSubType;
                emailAttachment.type = 'Binary';
                emailAttachments.add(emailAttachment);
            }
        }

        return emailAttachments;
    }
    
    /*
      Authors: Ashish Kumar
      Purpose: Get attachment from InboundEmail and check collective size of attachmetns
      Dependencies: called from "handleInboundEmail" method to check attachment size.
     
     */
    private Integer checkAttachmentSize(Messaging.InboundEmail email) {
        Integer emailBinaryAttachmentSize = 0;
        if (email.binaryAttachments != null) {
            for (Messaging.InboundEmail.binaryAttachment binaryAttachment : email.binaryAttachments) {
                emailBinaryAttachmentSize = emailBinaryAttachmentSize + binaryAttachment.Body.size();
                
            }
        }
        if(email.textAttachments != null) {
            for(Messaging.InboundEmail.TextAttachment file : email.textAttachments) {
                emailBinaryAttachmentSize = emailBinaryAttachmentSize + file.body.length();
            }
        }
        System.debug('emailAttachmentSize '  + emailBinaryAttachmentSize);
        return emailBinaryAttachmentSize;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get Key/Value pair of email header.
      Dependencies: Use in "handleInboundEmail" method to get email header and "ExtractAttachments" method to get attachment header.
     
     */
    private Map<string, string> getHeaderMap(List<Messaging.InboundEmail.Header> emailHeader) {
        Map<string, string> headerMap = new Map<string, string> ();
        for (Messaging.InboundEmail.Header header : emailHeader) {
            headerMap.put(header.name, header.value);
        }
        return headerMap;
    }

    public String getObjectName(String[] emails) {
        if (emails != null)
        {
            List<Service_Mapping__c> serList = [select Email_Service__c, Object_Name__c from Service_Mapping__c];
            System.debug(serList);
            if (serList != null)
            {
                Map<String, String> mapObj = new Map<String, String> ();
                for (Service_Mapping__c sobj : serList) {
                    if (sobj.Email_Service__c != null)
                    {
                        for (String nam : sobj.Email_Service__c.split(',')) {
                            mapObj.put(nam, sobj.Object_Name__c);
                        }
                    }
                }
                for (String objEmail : mapObj.keySet()) {
                    System.debug('objEmail--->' + objEmail);
                    System.debug('emails--->' + emails);
                    for (String email : emails) {
                        System.debug(mapObj.get(email));
                        if (email.equals(objEmail))
                        {
                            return mapObj.get(email);
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public contact getOrCreateContact(EmailToCaseBean emailtoCaseBean) {
        contact contObj = getContact(emailtoCaseBean.fromAddress);
        if (contObj == null) {
            contObj = createContact(emailtoCaseBean);
        }
        return contObj;
    }
    
    /*
      Authors: Chandresh Koyani
      Purpose: Create new contact object for from object with DefualtAccount.
      Dependencies: Use in ProcessAcceptRules and ProcessIgnoreRules method.
     */
    private contact createContact(EmailToCaseBean emailtoCaseBean) {
        emailtoCaseBean.appendIntoDebugLog('createContact', 'Start');
        try {
            Account acc = [select id from Account where Name = :ESMConstants.DEFAULT_ACCOUNT_NAME];
            emailtoCaseBean.appendIntoDebugLog('createContact', 'Account=>' + acc);

            contact cnt = new contact();
            cnt.LastName = emailtoCaseBean.fromName;
            cnt.Email = emailtoCaseBean.fromAddress;
            cnt.AccountId = acc.Id;
            insert cnt;
            emailtoCaseBean.appendIntoDebugLog('createContact', 'Contact object created successfully, Contact=>' + cnt);

            return cnt;
        }
        catch(Exception ex) {
            emailtoCaseBean.appendIntoDebugLog('createContact', 'Exception=>' + ex.getMessage() + ', StackTrace=>' + ex.getStackTraceString());
            throw new EmailToCaseException(ex);
        }
        finally {
            emailtoCaseBean.appendIntoDebugLog('createContact', 'End');
        }
        //return null;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check for contact object with email.
      Dependencies: Use in ProcessAcceptRules and ProcessIgnoreRules method.
     */
    private contact getContact(string email) {
        try {
            List<contact> contactList = [select id, Email from contact where Email = :email];
            if (!contactList.isEmpty()) {
                return contactList[0];
            }
        }
        catch(Exception ex) {
            throw new EmailToCaseException(ex);
        }
        return null;
    }
}