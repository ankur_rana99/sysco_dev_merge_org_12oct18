@isTest
public class BatchEDItoPDFInvoiceTest { 
    
    public static testmethod void testScheduledJob() { 
        
      
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         invConf.EDI_PDF_Process_Current_State__c = 'Vouched,Ready for PDF creation';
         insert invConf;
        
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        //sysconfig.Sub_Module__c ='Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        
        
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         Invoice__c invobj = new Invoice__c();    
    	 invobj.Invoice_Date__c = Date.Today();
    	 invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	 invobj.Total_Amount__c = 500.00;
         invobj.Current_State__c='Vouched';
    	 System.debug('invobj1' + invobj);
    	 insert invobj;
        
          Test.startTest();
        List<Invoice__c> newlist1 = [Select id,name,Document_Type__c,Cora_Ref_No__c,Invoice_No__c,Invoice_Type__c,Amount__c,Invoice_Date__c,Priority__c,InputSource__c,OPCO_Code__r.OPCO_Code__c,Tax__c,Payment_Term__c,Payment_Term__r.name,Payment_Due_Date__c,Term_Start_Date__c,Discount_Amount__c,Samples_Charges__c,Freight_Charges__c,Voucher_Comments__c,Comments__c,Purchase_Order__c,Vendor__r.Vendor_No__c,Total_Amount__c,Vendor_Name__c,Update_Flag__c,Vendor_Override__r.Vendor_No__c,Resend_to_ERP_count__c,Message__c,Other_Charges_Surcharges__c,Pickup_Allowance__c,Auto_Match_Indicator__c,AF_Flag__c,Vendor_Address_Code__c,Invoice_Rounding_Amount__c,Batch_No__c,Purchase_Order_Number__c,SC_Code__c,Allowance_Type__c from Invoice__c];
        BatchEDItoPDFInvoice BE= new BatchEDItoPDFInvoice(); 
        DataBase.executeBatch(BE);
        BE.execute(null,newlist1);
        Test.StopTest();
    }
        
}