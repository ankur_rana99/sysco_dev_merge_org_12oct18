/*
  Authors: Rohan Joshi
  Date created: 16/1/2018
  Purpose: This class is used to calculate QC Logic, it contains only one public method its used from "ProcessBulider".
  Dependencies: Called from "ProcessBuilder".
  -------------------------------------------------
*/
public class QCCalculation {
    public static final double MIN_LIMIT = 1;
    public static final double MAX_LIMIT = 100;

    public static void checkForQC(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
      List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'sampling'  and object_Name__c ='CaseManager__c' order by Order__c];
      Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);
      for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
      CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
      if (caseTrackerNew.User_Action__c != caseTrackerOld.User_Action__c && caseTrackerNew.User_Action__c == 'Process') {
      QCHelper.QCRule rule = findRule(caseTrackerNew, qcRuleConfiges, userMap);
      if (rule != null) {
      if (rule.JSONValue.AlwaysMoveToQC) {
      caseTrackerNew.QC_Available__c = true;
      caseTrackerNew.QC_Rule__c = rule.Id;
      }
      else {
      double perecentage = double.valueOf(rule.JSONValue.qcPerecentage);
      if (perecentage > getRandomNumber(MIN_LIMIT, MAX_LIMIT)) {
      caseTrackerNew.QC_Available__c = true;
      caseTrackerNew.QC_Rule__c = rule.Id;
      }
      else {
      caseTrackerNew.QC_Available__c = false;
      caseTrackerNew.QC_Rule__c = rule.Id;
      }
      }
      }
      }
      }
      }
       public static QCHelper.QCFormResponse getQCFormFieldInfo(String caseId) {
            Schema.SObjectType token = ((Id) caseId).getSobjectType();
            return getQCFormFieldInfo(caseId,token.getDescribe().getName());
       }
     public static QCHelper.QCFormResponse getQCFormFieldInfo(string caseId,String objName) {
      List<sObject> caseTrackers =Database.query(CommonFunctionController.getSOQLQuery(objName,'Id =:caseId '));
      QCHelper.QCFormResponse response = new QCHelper.QCFormResponse();

      if (caseTrackers != null && caseTrackers.size() > 0) {
      sObject caseTrackerObj = caseTrackers[0];
      try {
      List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'form' order by Order__c];
      Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);

      QCHelper.QCRule rule = findRule(caseTrackerObj, qcRuleConfiges, userMap);
      if (rule != null) {
      response.qcFormFieldsetName = rule.JSONValue.qcFormFieldsetName;
      response.qcFormHeader = rule.JSONValue.qcFormHeader;
      }
      system.debug('********'+response.qcFormFieldsetName);
      system.debug('********'+response.qcFormHeader);
      }
      catch(Exception ex) {
      ExceptionHandlerUtility.writeException('QCCalculation', ex.getMessage(), ex.getStackTraceString());
      }
      }
      return response;
      }

    private static QCHelper.QCRule findRule(sObject caseTrackerObject, List<QC_Rule_Config__c> qcRuleConfiges, Map<string, User> userMap) {

        for (QC_Rule_Config__c qcRuleConfig : qcRuleConfiges) {

            QCHelper.QCRule qcRule = new QCHelper.QCRule();
            qcRule.id = qcRuleConfig.id;
            qcRule.JSONValue = (QCHelper.JSONValue) System.JSON.deserialize(qcRuleConfig.JSON__c, QCHelper.JSONValue.class);

            if (qcRule.JSONValue != null && qcRule.JSONValue.criterias != null) {
                boolean isMatch = false;
                for (QCHelper.Criteria cre : qcRule.JSONValue.criterias) {
                    if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                        string fieldValue = string.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                        double fieldValue = double.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                        boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('REFERENCE')) {
                        string userId = string.valueOf(caseTrackerObject.get(cre.field));

                        if (!string.isEmpty(userId)) {
                            User ur = userMap.get(userId);
                            if (ur != null) {
                                string fieldValue = string.valueOf(ur.get(cre.refField));
                                isMatch = isMatch(cre, fieldValue);
                            }
                        }
                    }
                    if (!isMatch) {
                        break;
                    }
                }

                if (isMatch) {
                    return qcRule;
                }
            }
        }
        return null;

    }

    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, string fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == car.Value;
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != car.Value;
        }
        else if (car.operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(car.Value)) {
                List<string> tempList = car.Value.split(',');
                for (string str : tempList) {
                    if (car.type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, double fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less than')) {
            return fieldValue < double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater than')) {
            return fieldValue > double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= double.valueOf(car.Value);
        }
        return false;
    }
    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, boolean fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(car.Value);
        }
        return false;
    }

    @testVisible
    private static double getRandomNumber(double lower, double upper) {
        return Math.round((Math.random() * (upper - lower)) + lower);
    }

    @InvocableMethod(label = 'Check For QC' description = 'Check for QC based on QC Rules definied')
    public static void checkForQC(List<Invoice__c> Invoices) {
        checkForQC(Invoices, 'Invoice__c');
    }
    public static void checkForQC(List<sObject> objectList, String objectName) {
        try {
            System.debug('objectList--->'+objectList);
            List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'sampling' and object_Name__c = :objectName order by Order__c];
            Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);
            System.debug('checkForQCEnter');
            List<sObject> updateCaseList = new List<sObject> ();
            for (sObject caseTrackerObj : objectList) {
                System.debug('caseTrackerObj-->'+caseTrackerObj);
                System.debug('qcRuleConfiges-->'+qcRuleConfiges);
                QCHelper.QCRule rule = findRule(caseTrackerObj, qcRuleConfiges, userMap);
                System.debug('rule-->'+rule);
                if (rule != null) {
                    sObject updateObj = Schema.getGlobalDescribe().get(objectName).newSObject();
                    updateObj.Id = caseTrackerObj.Id;
                    if (rule.JSONValue.AlwaysMoveToQC) {
                        updateObj.put('QC_Available__c', true);
                        updateCaseList.add(updateObj);
                    }
                    else {
                        double perecentage = double.valueOf(rule.JSONValue.qcPerecentage);
                        if (perecentage > getRandomNumber(MIN_LIMIT, MAX_LIMIT)) {
                            updateObj.put('QC_Available__c', true);
                            updateCaseList.add(updateObj);
                        }
                        else {
                            updateObj.put('QC_Available__c', false);
                            updateCaseList.add(updateObj);
                        }
                    }
                    updateObj.put('QC_Rule__c', rule.Id);
                }

            }
            System.debug('updateCaseList--->'+updateCaseList);
            if (updateCaseList.size() > 0) {
                update updateCaseList;
            }
        }
        catch(Exception ex) {
            System.debug('error From Qc');
            ExceptionHandlerUtility.writeException('QCCalculation', ex.getMessage(), ex.getStackTraceString());
        }
    }
}