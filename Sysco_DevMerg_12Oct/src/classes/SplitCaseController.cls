public class SplitCaseController {

    @AuraEnabled
    public static List<CaseManager__c> performSplitCase(CaseManager__c mainCase, string splitCasesStr, string comments) {
        List<CaseManager__c> splittedCases = null;
        try
        {
            Set<String> selectedCaseIds = new Set<String> ();

            List<SplitCase> splitCases = (List<SplitCase>) JSON.deserialize(splitCasesStr, List<SplitCase>.class);
            for (SplitCase splitCaseObj : splitCases) {
                for (ContentDocumentLink contentDocumentLinkObj : splitCaseObj.SelectedAttachments) {
                    selectedCaseIds.add(contentDocumentLinkObj.ContentDocument.Id);
                }
            }
            Map<String, ContentVersion> contentVersionMap = new Map<String, ContentVersion> ();
            for (ContentVersion contentVersion :[SELECT ContentDocumentId, Title, VersionData, PathOnClient FROM ContentVersion WHERE ContentDocumentId = :selectedCaseIds])
            {
                contentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
            }

            List<CaseManager__c> caseManagers = new List<CaseManager__c> ();
            List<ContentVersion> contentversionList = new List<ContentVersion> ();
            for (SplitCase splitCaseObj : splitCases) {
                CaseManager__c caseManager = splitCaseObj.CaseManager;
                caseManager.Email_Subject__c = System.Label.Case_Created_Manually + System.now().format();
                caseManager.Current_State__c = 'Start';
                caseManager.User_Action__c = 'Create';
				caseManager.Input_Source__c='Manual';
				caseManager.Pending_Reason__c = '';
				caseManager.Is_Assigned__c = true;
                caseManager.No_Of_Attachments__c = splitCaseObj.SelectedAttachments.size();
				caseManager.Last_Response_Date__c=Datetime.now();
                caseManagers.add(caseManager);

                splitCaseObj.ContentVersions = new List<ContentVersion> ();
                for (ContentDocumentLink contentDocumentLinkObj : splitCaseObj.SelectedAttachments)
                {
                    ContentVersion contentversionObj = contentVersionMap.get(contentDocumentLinkObj.ContentDocument.Id);
                    ContentVersion newContVersion = new ContentVersion();
                    newContVersion.Title = contentversionObj.Title;
                    newContVersion.PathOnClient = contentversionObj.PathOnClient;
                    newContVersion.VersionData = contentversionObj.VersionData;
                    contentversionList.add(newContVersion);
                    splitCaseObj.ContentVersions.add(newContVersion);
                }
            }
            system.debug(caseManagers);
            insert caseManagers;
            insert contentversionList;
            Map<String, ContentVersion> conetntVersionListWithId = new Map<String, ContentVersion> ([SELECT id, ContentDocumentId, Title FROM ContentVersion where Id = :contentversionList]);

            List<ContentDocumentLink> contentDocLinkList = new List<ContentDocumentLink> ();
            for (SplitCase splitCaseObj : splitCases) {
                for (ContentVersion contentVersionObj : splitCaseObj.ContentVersions)
                {
                    Id contentDocumentId = conetntVersionListWithId.get(contentVersionObj.id).ContentDocumentId;
                    ContentDocumentLink contentDocLink = new ContentDocumentLink();
                    contentDocLink.ContentDocumentId = contentDocumentId;
                    contentDocLink.LinkedEntityId = splitCaseObj.CaseManager.Id;
                    contentDocLink.ShareType = 'V';
                    contentDocLinkList.add(contentDocLink);
                }
            }
            insert contentDocLinkList;
            splittedCases = [Select id, Name from CaseManager__c where id IN :caseManagers];
            List<EmailMessage> emailMessages = new List<EmailMessage> ();
            List<Comments__c> splitCommentsList = new List<Comments__c> ();
            List<Relationship__c> relationshipList = new List<Relationship__c> ();
            for (CaseManager__c caseManager : splittedCases) {

                String emailSubject = System.Label.Case_Created_Manually_By + ' ' + UserInfo.getFirstName() + ' ' + System.Label.At + ' ' + System.now().format() + ' ' + ESMConstants.SUBJECT_START_IDENTIFIER + caseManager.Name + ESMConstants.SUBJECT_END_IDENTIFIER;
                string emailBody = System.Label.Case_Created_Manually_By + ' ' + UserInfo.getFirstName() + ' ' + System.Label.On_Date + ' ' + System.now().format();
                emailMessages.add(new EmailMessage(Subject = emailSubject,
                                                   TextBody = emailBody,
                                                   HtmlBody = emailBody,
                                                   FromName = UserInfo.getName(),
                                                   Incoming = true,
                                                   RelatedToId = caseManager.Id,
                                                   status = '0'));

                //Code to create Split Comments
                Comments__c splitComments = new Comments__c();
                splitComments.CaseManager__c = caseManager.id;
                splitComments.Comment__c = comments;
                splitCommentsList.add(splitComments);

                // Code to create Relationship for splitted Cases
                Relationship__c splitParent = new Relationship__c();
                splitParent.CaseManager__c = mainCase.Id;
                splitParent.Sub_Case__c = caseManager.id;
                splitParent.Type__c = 'Split';
                relationshipList.add(splitParent);

                Relationship__c splitChild = new Relationship__c();
                splitChild.CaseManager__c = caseManager.id;
                splitChild.Sub_Case__c = mainCase.Id;
                splitChild.Type__c = 'Split';
                relationshipList.add(splitChild);
            }
            insert emailMessages;
            insert splitCommentsList;
            insert relationshipList;
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return splittedCases;

    }

    Public class SplitCase {
        @AuraEnabled
        public CaseManager__c CaseManager {
            get; set;
        }

        @AuraEnabled
        public List<ContentDocumentLink> SelectedAttachments {
            get; set;
        }
        @AuraEnabled
        public List<ContentVersion> ContentVersions {
            get; set;
        }
    }


    @AuraEnabled
    public static List<SplitCase> CloneCase(CaseManager__c parentCase, Integer noOfSplit) {
        List<SplitCase> splitCases = new List<SplitCase> ();
        try {
            for (integer index = 0; index < noOfSplit; index++) {
                SplitCase splitCase = new SplitCase();
                splitCase.CaseManager = parentCase.clone();
                splitCase.SelectedAttachments = new List<ContentDocumentLink> ();
                splitCase.ContentVersions = new List<ContentVersion> ();
                splitCases.add(splitCase);
            }
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        System.debug(splitCases);
        return splitCases;
    }

    @AuraEnabled
    public static Map<String, Object> getAttachments(String caseId) {
        Map<String, Object> responseMap = new Map<String, Object> ();
        try {
            List<ContentDocumentLink> contentDocuments = [SELECT Id, ContentDocument.Id, ContentDocument.Title FROM ContentDocumentLink where LinkedEntityId = :caseId order by SystemModstamp desc];
            responseMap.put('attachments', contentDocuments);
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return responseMap;
    }

    @AuraEnabled
    public static ESMFieldHelper.FieldResponse getSplitCaseFields(CaseManager__c caseManager, string sessionId) {
        ESMFieldHelper.FieldResponse fieldResponse = new ESMFieldHelper.FieldResponse();
        try {
            fieldResponse.SectionFields = new List<ESMFieldHelper.SectionFields> ();

            string fieldSetName = ESMConstants.SPLIT_FIELD_SET;

            ESMFieldHelper.SectionFields sectionDetails = new ESMFieldHelper.SectionFields();
            sectionDetails.sectionName = '';
            sectionDetails.sectionId = '';
            sectionDetails.relatedFields = new list<ESMFieldHelper.FieldDetails> ();
            fieldResponse.SectionFields.add(sectionDetails);

            sectionDetails.relatedFields = ESMFieldHelper.getFieldsByFieldSet(fieldSetName, caseManager, sessionId);

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return fieldResponse;
    }
}