@isTest
public class EDItoPDFGenrateTest {
  public static testMethod void testFirst(){
        
      	System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
          
        Purchase_Order__c  po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 123;
        insert po;
        
        Invoice__c invobj = new Invoice__c();       
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);      
        invobj.Total_Amount__c = 500.00;        
        insert invobj;
      
       List<invoice__c> invoices=new  List<invoice__c>();
       invoices.add(invobj);
      
       EDItoPDFGenrate.GeneratePDF(invoices);
  }
}