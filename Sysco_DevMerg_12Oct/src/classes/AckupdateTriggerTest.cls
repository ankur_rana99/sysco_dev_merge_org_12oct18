@isTest
public class AckupdateTriggerTest {
    public static String method1(String currentstate){
    System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        
        Sysco_SystemConfiguration__c custset = new Sysco_SystemConfiguration__c();
        custset.name = 'ERPConfig';
        custset.EndPointURL__c='https://api.sysco.com/fi/FTR/Invoice/v1.0/mostInvoice';
        custset.SOQLQueryLimit__c='200';
        custset.TO_Email_Address__c = 'chiranjeevi.kunamalla@test.com';
        custset.WebserviceRetryLimit__c = 2;
        custset.SyscoAPIC__c = 'https://api.syscoo.com';
        custset.client_id__c = 'testid';
        custset.client_secret__c = 'testsecrete';
        insert custset;
        
        OPCO_Code__c opcocode = new OPCO_Code__c();
        opcocode.OPCO_Name__c='059';
            opcocode.OPCO_Code__c='059';
            opcocode.State__c = 'test State';
            opcocode.AddressStreet__c = 'test Address';
            opcocode.City__c='test City';
        	opcocode.Zip__c='xxxxx123';
            insert opcocode;
        
        Account acc = new Account();
        acc.name = 'Vendor test name';
            acc.OPCO_Code__c = opcocode.id;
            acc.Vendor_No__c = '1234';
            acc.Unique_Key__c = '059-1234';
            acc.Vendor_Name__c = 'testVendor';
           	insert acc;

        Payment_Term__c payterm = new Payment_Term__c();
        payterm.name='testpayment';
            payterm.OPCO_Code__c = opcocode.id;
            payterm.Unique_Key__c = '059-testpayment';
            payterm.Due_Month__c = 0;
            payterm.Due_Days__c = 90;
            payterm.Due_Date__c = 0;
            insert payterm;
			
        Purchase_Order__c po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 1000;
        po.Status__c = 'CL';
        po.FOB__c = 1000;
        po.Vendor__c = acc.Id;
        po.Total_Amount__c = 1000;
        po.Anticipated_Date__c = System.Today() - 5;
        insert po;
    
       invoice__c inv3 = new invoice__c();
        inv3.Document_Type__c='PO invoice';
        inv3.invoice_No__c='23456789';
        inv3.invoice_Type__c='M';
            inv3.Amount__c=2000;
            inv3.invoice_Date__c=Date.Today();
            inv3.Priority__c='High';
            inv3.Input_Source__c='Manual';
            inv3.OPCO_Code__c=opcocode.id;
            inv3.Tax__c=0;
            inv3.Payment_Term__c=payterm.id;
            inv3.Payment_Due_Date__c=Date.Today();
            inv3.Term_Start_Date__c=Date.Today();
            inv3.Discount_Amount__c=0;
            inv3.Samples_Charges__c=0;
			inv3.Freight_Charges__c =0;
			inv3.Voucher_Comments__c='';
			inv3.Comments__c='';
			inv3.Purchase_Order__c=po.id;
			inv3.Vendor__c=acc.id;
			//inv3.Total_Amount__c=2000;
			inv3.Vendor_Name__c='test vendor';
			inv3.Update_Flag__c='N';
			//inv3.Vendor_Override__c=''
			inv3.Resend_to_ERP_count__c=0;
			//inv3.Message__c=
			inv3.Other_Charges_Surcharges__c=0;
			inv3.Pickup_Allowance__c=0;
			inv3.Auto_Match_Indicator__c='Y';
			inv3.AF_Flag__c='Y';
			inv3.Vendor_Address_Code__c='01';
			inv3.invoice_Rounding_Amount__c=0;
			inv3.Batch_No__c='100101';
			//inv3.Purchase_Order_Number__c='PO-1001';
			inv3.WorkType__c = 'Merchandize';
			inv3.Allowance_Type__c='test';
        	inv3.Current_State__c = currentstate;
        	
        insert inv3;
        
        Invoice_Line_Item__c invline3 = new Invoice_Line_Item__c();
        invline3.Invoice_Line_No__c = '1';
        invline3.Amount__c=2000;
        invline3.GL_Code__c=null;
       	invline3.Quantity__c=2;
        invline3.Rate__c=2;
        invline3.Invoice__c= inv3.Id;
        insert invline3;
        
        Invoice__c neinv = [Select id,name,Document_Type__c,Cora_Ref_No__c,Invoice_No__c,Invoice_Type__c,Amount__c,Invoice_Date__c,Priority__c,InputSource__c,OPCO_Code__r.OPCO_Code__c,Tax__c,Payment_Term__c,Payment_Term__r.name,Payment_Due_Date__c,Term_Start_Date__c,Discount_Amount__c,Samples_Charges__c,Freight_Charges__c,Voucher_Comments__c,Comments__c,Purchase_Order__c,Vendor__r.Vendor_No__c,Total_Amount__c,Vendor_Name__c,Update_Flag__c,Vendor_Override__r.Vendor_No__c,Resend_to_ERP_count__c,Message__c,Other_Charges_Surcharges__c,Pickup_Allowance__c,Auto_Match_Indicator__c,AF_Flag__c,Vendor_Address_Code__c,Invoice_Rounding_Amount__c,Batch_No__c,Purchase_Order_Number__c,SC_Code__c,Allowance_Type__c from Invoice__c where Id =: inv3.id];
		List<String> str2=(neinv.name).split('-');
        neinv.Cora_Ref_No__c=str2[0];
        update neinv;
        
        return neinv.Cora_Ref_No__c;
        
    }
    public static testMethod void method2(){
    String corarefno = method1('Awaiting ERP ACK');
        
        Sysco_ACK_Interface__c ack = new Sysco_ACK_Interface__c();
        ack.Cora_Ref_No__c = corarefno;
        ack.ACK_Created_Date__c = 'Date.Today()';
        //ack.Batch_No__c = '1234';
        ack.Invoice_ERP_Status__c = 'OPN';
        ack.Invoice_Line_No__c = '0';
        ack.SUS_Error_Message_Code__c= '23';
        insert ack;
        
        /*Sysco_ACK_Interface__c ack1 = new Sysco_ACK_Interface__c();
        ack1.Cora_Ref_No__c = neinv.Cora_Ref_No__c;
        ack1.ACK_Created_Date__c = 'Date.Today()';
        //ack.Batch_No__c = '1234';
        ack1.Invoice_ERP_Status__c = 'VCH';
        ack1.Invoice_Line_No__c = '0';
        ack1.SUS_Error_Message_Code__c= '23';
        insert ack1;
*/
    }
    public static testMethod void method3(){
    String corarefno = method1('Awaiting ERP ACK');
        
        Sysco_ACK_Interface__c ack1 = new Sysco_ACK_Interface__c();
        ack1.Cora_Ref_No__c = corarefno;
        ack1.ACK_Created_Date__c = 'Date.Today()';
        //ack.Batch_No__c = '1234';
        ack1.Invoice_ERP_Status__c = 'VCH';
        ack1.Invoice_Line_No__c = '0';
        ack1.SUS_Error_Message_Code__c= '23';
        insert ack1;

    }
    
    public static testMethod void method4(){
    String corarefno = method1('Awaiting ERP ACK');
        
        Sysco_ACK_Interface__c ack1 = new Sysco_ACK_Interface__c();
        ack1.Cora_Ref_No__c = corarefno;
        ack1.ACK_Created_Date__c = 'Date.Today()';
        //ack.Batch_No__c = '1234';
        ack1.Invoice_ERP_Status__c = 'VCH';
        ack1.Invoice_Line_No__c = '1';
        ack1.SUS_Error_Message_Code__c= '20';
        insert ack1;

    }
    
    public static testMethod void method5(){
    String corarefno = method1('Posting Failed');
        
        Sysco_ACK_Interface__c ack1 = new Sysco_ACK_Interface__c();
        ack1.Cora_Ref_No__c = corarefno;
        ack1.ACK_Created_Date__c = 'Date.Today()';
        //ack1.Batch_No__c = '1234';
        ack1.Invoice_ERP_Status__c = 'OPN';
        ack1.Invoice_Line_No__c = '0';
        ack1.SUS_Error_Message_Code__c= '23';
        insert ack1;

    }
    

}