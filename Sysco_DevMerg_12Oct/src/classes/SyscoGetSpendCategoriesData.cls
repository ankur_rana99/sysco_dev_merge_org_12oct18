global class SyscoGetSpendCategoriesData implements database.Batchable<Sobject>,Schedulable,Database.Stateful,Database.AllowsCallouts{

private SchedulableContext objSchedulableContext;
private List<Spend_Categories__c> scList ;
private Spend_Categories__c sc;
private Spend_Categories__c scEmpty;
private List<Spend_Categories__c> emptyscList = new List<Spend_Categories__c>(); 

global SyscoGetSpendCategoriesData(){       
}

global List<Spend_Categories__c> start(Database.BatchableContext BC) {
    try{
        String dateFormatString = 'yyyy-MM-dd';
        scList = new List<Spend_Categories__c>();
        sc = new Spend_Categories__c();
        scEmpty = new Spend_Categories__c();
        System.debug(' Method Called SC...');
        
        Date today = Date.today();
        Datetime dt1 = Datetime.newInstance(today.year(), today.month(),today.day());
        String tday = dt1.format(dateFormatString);
        
        Date previousday = Date.today() - 1;
        Datetime dt2 = Datetime.newInstance(previousday.year(), previousday.month(),previousday.day());
        String prevday = dt2.format(dateFormatString);
        
        WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('SpendCategories');
        if(WEEndpoint.isfirstrun__c == 'Y'){
           prevday = '';                      
        }
        
        String XMLString;
        HttpRequest feedRequest = new HttpRequest();
        feedRequest.setEndpoint(WEEndpoint.Endpoint_URL__c + '?Starting_Prompt=' + prevday + '&Ending_Prompt=' + tday);
        feedRequest.setMethod('GET');
        Http http = new Http();
        HTTPResponse SpendCategoryResponse = http.send(feedRequest);
        System.debug(SpendCategoryResponse.getBody());
        XMLString = SpendCategoryResponse.getBody();
        DOM.Document doc=new DOM.Document();
        doc.load(XMLString);
        DOM.XmlNode rootNode=doc.getRootElement();
        parseXML(rootNode);
        if(sc!=null && sc!=scEmpty )
            scList.add(sc); 
    }catch(exception e){
        system.debug(e.getMessage());
    } 
    return scList;
    }

global void Execute(Database.BatchableContext objBatchableContext,List<Spend_Categories__c> scat){
    try{
        if(scat.size()>0 && scat != emptyscList) {  
            system.debug('scat.size()' + scat.size());
            system.debug('scat' + scat);           
            upsert scat Reference_ID__c;
        }
        else
            system.debug('No Records to Process');        
    }catch(exception e){
        system.debug(e.getMessage());
    }
}

 global void finish(Database.BatchableContext BC) { 
     WorkDay_EndPoints__c WEEndpoint = WorkDay_EndPoints__c.getValues('SpendCategories'); 
     System.debug('WEEndpoint..... '+ WEEndpoint.isfirstrun__c);
     if(WEEndpoint.isfirstrun__c == 'Y'){
        System.debug('WEEndpoint.isfirstrun__c +'+ WEEndpoint.isfirstrun__c);
        WEEndpoint.isfirstrun__c = 'N';
        Update WEEndpoint;                       
    }  
 }
    
Private void parseXML(DOM.XMLNode node) {
    if(node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if(node.getName()=='Report_Entry'){
            if(sc!=null && sc!=scEmpty )
                scList.add(sc);
            sc = new Spend_Categories__c();  
        }
        if(node.getName()=='Spend_Category_Name'){
            sc.Spend_Categories_Name__c=node.getText().trim();
            if(sc.Spend_Categories_Name__c.length() > 79)
                sc.Name=sc.Spend_Categories_Name__c.substring(0,80);
            else
                sc.Name=sc.Spend_Categories_Name__c;
        }
        if(node.getName()=='referenceID')
            sc.Reference_ID__c=node.getText().trim();
        if(node.getName()=='Inactive')
            sc.Inactive__c=node.getText().trim();  
    }
    System.debug('scTemp------->' + sc);
    for (Dom.XMLNode child: node.getChildElements()) {
        parseXML(child);
    }
}

global void execute(SchedulableContext objSchedulableContext) {
     SyscoGetSpendCategoriesData syscoSC  = new SyscoGetSpendCategoriesData();
     Integer batchSize = 2000; 
     system.database.executebatch(syscoSC,batchSize); 
    }
}