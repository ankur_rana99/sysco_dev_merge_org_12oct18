global class ProcessOnHoldInvoices implements Database.Batchable < SObject > {
	static String namespace = UtilityController.esmNameSpace;
	static List<Invoice__c> invoices = new List<Invoice__c> ();
	global static Map<String, Purchase_Order__c> mapInvoicePO = new Map<String, Purchase_Order__c> ();
	global static Map<String, GRN__c> mapPOGRN = new Map<String, GRN__c> ();
	global ProcessOnHoldInvoices() {
		// only for on hold invoice
		// pending merchandise processing

	}
	global List<Invoice__c> start(Database.BatchableContext context) {
		//system.debug('invoices5452--'+invoices);
		//return Database.Query(CommonFunctionController.getSOQLQuery('Invoice__c', '(Invoice_Type__c =\'M\' OR Invoice_Type__c =\'F\') and Current_State__c IN (\'Pending Anticipated Date\',\'Ready For Manual Processing\')'));
        return Database.Query(CommonFunctionController.getSOQLQuery('Invoice__c', '(Invoice_Type__c =\'M\' OR Invoice_Type__c =\'F\') and name IN (\'INV-00002665\')'));
	}
	global void execute(Database.BatchableContext context, List<Invoice__c> invoices) {
		Set<String> invIdSet = new Set<String> ();
		for (Invoice__c inv : invoices) {
			invIdSet.add(inv.Id);
		}
		List<Invoice_PO_Detail__c> invPoDetail = CommonFunctionController.getInvoicePODetail(namespace, invIdSet);
		Set<String> invPODetIdSet = new Set<String> ();
		for (Invoice_PO_Detail__c invPo : invPoDetail) {
			invPODetIdSet.add(invPo.Purchase_Order__c);
		}
		List<Purchase_Order__c> poList = CommonFunctionController.getPOList(namespace, invPODetIdSet);
		List<GRN__c> grnList = CommonFunctionController.getGrnList(namespace, invPODetIdSet);
		for (GRN__c grn : grnList) {
			mapPOGRN.put(grn.Purchase_Order__c, grn);
		}
		for (Invoice_PO_Detail__c invPo : invPoDetail) {
			for (Purchase_Order__c po : poList) {
				if (invPo.Purchase_Order__c == po.Id)
				{
					mapInvoicePO.put(invPo.Invoice__c, po);
				}
			}
		}
		system.debug('invoices5453--' + invoices);
		system.debug('mapInvoicePO5453--' + mapInvoicePO);
		for (Invoice__c invoice : invoices) {
			Purchase_Order__c po = mapInvoicePO.get(invoice.Id);
			if (invoice.get(namespace + 'Invoice_Type__c') != null && String.valueOf(invoice.get(namespace + 'Invoice_Type__c')).equals('M'))
			{
                if(String.valueOf(invoice.get(namespace + 'Current_State__c')).equals('Pending Anticipated Date')){
				if (po != null && po.Anticipated_Date__c != null && po.Anticipated_Date__c <= Date.today())
				{
					if (mapPOGRN == null || mapPOGRN.get(po.Id) == null)
					{
						invoice.User_Action__c = Label.User_Action_Send_for_Manual_Processing;
					}
					else
					{
						invoice.User_Action__c = Label.User_Action_Proceed;
					}
					// need to check fo grn // update state
				} else if (mapPOGRN != null && po != null && mapPOGRN.get(po.Id) != null) {
					invoice.User_Action__c = Label.User_Action_Proceed;
				}
                }
                else if((String.valueOf(invoice.get(namespace + 'Current_State__c')).equals('Ready For Manual Processing')) && (String.valueOf(invoice.get(namespace + 'Exception_Reason__c')) != null) && (String.valueOf(invoice.get(namespace + 'Exception_Reason__c')).equals('GRN Overdue'))){
                    if (mapPOGRN != null && po != null && mapPOGRN.get(po.Id) != null) {
                        invoice.User_Action__c = Label.User_Action_Send_for_CORA_Validation;
                    }
                }
                
                
				//from which date 30 days will be calculated ?? -- done --- Remove_Hold_Date__c
			}
			/*else if (invoice.get(namespace + 'Invoice_Type__c') != null && String.valueOf(invoice.get(namespace + 'Invoice_Type__c')).equals('F'))
			{
				if (invoice.Remove_Hold_Date__c != null && invoice.Remove_Hold_Date__c.addDays(30) > Date.today())
				{
					invoice.User_Action__c = Label.User_Action_Send_to_SBS_Cypress_Team;
				}
				else if (po != null && String.isBlank(String.valueOf(po.get(namespace + 'Merchandise_Vouched_Status__c')))
				         && !String.isBlank(String.valueOf(po.get(namespace + 'Freight_Vouched_Status__c'))))
				{
					invoice.User_Action__c = Label.User_Action_Proceed;
				}
			}*/
		}
		system.debug('invoices5454--' + invoices);
		upsert invoices;
	}
	global void finish(Database.BatchableContext context) {
		system.debug('=====InvoiceAutoProcess finish======');
	}
}