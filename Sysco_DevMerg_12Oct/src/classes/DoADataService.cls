/*
 * Authors      :  Rahul Pastagiya
 * Date created :  27/03/2018
 * Purpose      :  Prepare Data for DoA
 * Dependencies :  
 * JIRA ID      :  
 * -----------------------------------------------------------
 * Modifications: 1
 *        Authors:  
 *        Date:  
 *        Purpose of modification:  
 *        Method/Code segment modified: 
 * 
 */
public class DoADataService { 
    
    private Boolean isTruncated = false;
    private boolean isException = false;

    private Map<String, Functional_DAP__c> entityAndDoaTitleVSFunctionalListMap = new Map<String, Functional_DAP__c>();
    private Map<String, Finance_DAP__c> entityAndDoaTitleVSFinanceListMap = new Map<String, Finance_DAP__c>();

    private static List<DOA_Matrix__c> doaMetrixList = new List<DOA_Matrix__c>();

    public static List<DOA_Matrix__c> prepareDataForDoA(){
        DoADataService dds = new DoADataService();
        dds.getRequestorUsers();
        return doaMetrixList;
    }

    private void getRequestorUsers(){
                    
        Set<String> dapTitleSet = new Set<String>();
        Set<String> entitySet = new Set<String>();

        List<User_Master__c> userMasterList = new List<User_Master__c>(); 
        //For get all requestor users       
        for (User_Master__c userMaster : [SELECT Id, Email__c, Name, OpCo_Code__c, Supervisor__c, 
                                            DAP_Title__c, Title_Type__c , Entity__c
                                            FROM User_Master__c  
                                            where Status__c = 'Active'
                                            and User_Type__c = 'Requestor/Buyer' 
                                            and OpCo_Code__c != null and OpCo_Code__c != '' 
                                            and Entity__c != null and Entity__c != ''
                                            and Title_Type__c != null and Title_Type__c != '' 
                                            and DAP_Title__c != null and DAP_Title__c != ''] )
        {        
            userMasterList.add(userMaster);
            dapTitleSet.add(userMaster.DAP_Title__c);
            entitySet.add(userMaster.Entity__c);
        }

        for (Functional_DAP__c  functionalDAP :  [SELECT Id, DAP_Title__c, Entity__c, Approval_Limit__c 
                                                            FROM Functional_DAP__c 
                                                            where Entity__c in :entitySet and DAP_Title__c in :dapTitleSet]){
                    
            entityAndDoaTitleVSFunctionalListMap.put(functionalDAP.Entity__c+'~'+functionalDAP.DAP_Title__c, functionalDAP);
            
        }

        for (Finance_DAP__c  financeDAP :  [SELECT Id, DAP_Title__c, Entity__c, Function_Area__c, Transactions_or_Areas__c, Approval_Limit__c 
                                                            FROM Finance_DAP__c 
                                                            where Entity__c in :entitySet and DAP_Title__c in :dapTitleSet]){
            
            entityAndDoaTitleVSFinanceListMap.put(financeDAP.Entity__c+'~'+financeDAP.DAP_Title__c, financeDAP);
            
        }

        for (User_Master__c user : userMasterList)
        {
            this.populateDOAMetix(user);
        }
        if (!isException)
        {
            if (!isTruncated)
            {
       //         DELETE[SELECT Id FROM DOA_Matrix__c LIMIT 10000];
                isTruncated = true;
            }           
    //        insert doaMetrixList;
            System.debug('DoADataService : Success');
        }

                
    }


    private void populateDOAMetix(User_Master__c userMaster){
        
        try 
        {                   
            //Popuate DOA Metrix from Functional DAP            
            if (userMaster.Title_Type__c == 'Functional' && entityAndDoaTitleVSFunctionalListMap.containsKey(userMaster.Entity__c+'~'+userMaster.DAP_Title__c))
            {                           
                Functional_DAP__c functionalDAP = entityAndDoaTitleVSFunctionalListMap.get(userMaster.Entity__c+'~'+userMaster.DAP_Title__c);
                
                DOA_Matrix__c doaMetrix = new DOA_Matrix__c(Active__c=true,
                                                                Entity__c = functionalDAP.Entity__c,
                                                                DoA_Title__c = functionalDAP.DAP_Title__c,
                                                                Approver_User__c=userMaster.Id,
                                                                DOA_Limit__c=functionalDAP.Approval_Limit__c,
                                                                Supervisor__c=userMaster.Supervisor__c,
                                                                OPCO_Code__c = userMaster.OPCO_Code__c,
                                                                is_Functional__c=true);
                doaMetrixList.add(doaMetrix);
                System.debug('doaMetrix :'+doaMetrix);            
                
            }
            System.debug('doaMetrixList :'+doaMetrixList.size());
            
            //Popuate DOA Metrix from Finance DAP
            if (userMaster.Title_Type__c == 'Finance' &&  entityAndDoaTitleVSFinanceListMap.containsKey(userMaster.Entity__c+'~'+userMaster.DAP_Title__c))
            {           
                Finance_DAP__c financeDAP = entityAndDoaTitleVSFinanceListMap.get(userMaster.Entity__c+'~'+userMaster.DAP_Title__c);
                
                DOA_Matrix__c doaMetrix = new DOA_Matrix__c(Active__c=true,
                                                                Entity__c = financeDAP.Entity__c,
                                                                DoA_Title__c = financeDAP.DAP_Title__c,
                                                                Approver_User__c=userMaster.Id,
                                                                DOA_Limit__c=financeDAP.Approval_Limit__c,
                                                                Supervisor__c=userMaster.Supervisor__c,
                                                                OPCO_Code__c = userMaster.OPCO_Code__c,
                                                                is_Functional__c=false);
                doaMetrixList.add(doaMetrix);
                System.debug('doaMetrix :'+doaMetrix);            
            
            }
            System.debug('doaMetrixList :'+doaMetrixList.size());           
        }
        catch (Exception  e)
        {
            isException = true;
            System.debug('Exeception in DoADataService /n'+e);
        }
        
    }

}