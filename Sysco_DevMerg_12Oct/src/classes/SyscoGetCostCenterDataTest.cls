@isTest
public class SyscoGetCostCenterDataTest{
    
    public static testMethod void testFirst(){    
        WorkDay_EndPoints__c sysWED=new WorkDay_EndPoints__c();
        sysWED.Name='CostCenter';
        sysWED.isfirstrun__c = 'Y';
        sysWED.Endpoint_URL__c='callout:Sysco_Cost_Center/ccx/service/customreport2/sysco4/ISU+FDM+Cost+Center/Foundation_Data_-_Cost_Center';
        insert sysWED;
                 
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());
        
        SyscoGetCostCenterData getccd  = new SyscoGetCostCenterData();
        Integer batchSize = 200; 
        system.database.executebatch(getccd,batchSize);       
        Test.StopTest();  
    }
    
}