public with sharing class InvoiceValidationExceptionController {
    static Boolean Enable_Quantity_Calculation = false;
    static Boolean Enable_Auto_Flipping_With_GRN = false;
    static Boolean isManualProcessing = true;
    public static Map<String, String> validationMap = new Map<String, String> ();
    public static Map<Id, String> mapInvoiceWithExceptions = new Map<Id, String> ();
    public static Map<String, String> ValExecMsgMap = new Map<String, String> ();
    public static Map<String, String> LineLevelError = new Map<String, String> ();
    public static Map<String, PO_Line_Item__c> poLineMap = new Map<String, PO_Line_Item__c> ();
    public static Map<string, string> ValidationExceptionMap = new Map<string, string> ();

    public Map<String, String> executeValidationException(sobject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Invoice_Line_Item__c> otherChargesList, List<Purchase_Order__c> lstPO, Boolean isManual)
    {
        isManualProcessing = isManual;
        return executeValidationException(childSobject, invLineItemList, otherChargesList, lstPO);
    }
    public static Map<String, String> executeValidationException(sobject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Invoice_Line_Item__c> otherChargesList, List<Purchase_Order__c> lstPO)
    {
        List<Invoice__c> lstInvoices = new List<Invoice__c> ();
        ValExecMsgMap = new Map<String, String> ();
        String exceptionStr = '';
        Map<Id, String> mapInvoiceWithVariance = new Map<Id, String> ();
        Map<Id, String> mapInvoiceWithFlagAF = new map<Id, String> ();
        Map<Id, String> mapInvWithLineTolerance = new Map<Id, String> ();
        Map<Id, String> mapInvWithChargeTolerance = new Map<Id, String> ();
        Map<Id, String> mapInvWithLineVariance = new Map<Id, String> ();
        Map<Id, String> mapInvWithChargeVariance = new Map<Id, String> ();

        Invoice__c invoice = (Invoice__c) childSobject;
        String childNameSpace = UtilityController.esmNameSpace;
        lstInvoices.add(invoice);
        SysycoAutoMatchAndValidationHelper helper = new SysycoAutoMatchAndValidationHelper();
        List<Invoice__c> l = new List<Invoice__c> ();
        l.add((Invoice__c) childSobject);
        l = duplicateCheckForInvoices(l);
        if (l != null && l.get(0) != null && l.get(0).isDuplicate__c)
        {
            ValExecMsgMap.put('Invoice_Exception', 'duplicate Invoice');
            validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
        }
        validationMap.put('jsonValidationExceptionMsg', JSON.serialize(costCenterCheckForInvoices(lstInvoices, invLineItemList)));
        map<String, map<Id, String>> mapOutputFromHelper = helper.validateCheck(lstInvoices, invLineItemList, lstPO, false);
        mapInvoiceWithExceptions = mapOutputFromHelper.get('Exceptions');
        mapInvoiceWithVariance = mapOutputFromHelper.get('Variances');
        mapInvWithLineTolerance = mapOutputFromHelper.get('LineTolerance');
        mapInvWithChargeTolerance = mapOutputFromHelper.get('ChargeTolerance');
        mapInvWithLineVariance = mapOutputFromHelper.get('LineVariance');
        mapInvWithChargeVariance = mapOutputFromHelper.get('ChargeVariance');
        mapInvoiceWithFlagAF = mapOutputFromHelper.get('AF');
        map<Id, Invoice_Line_Item__c> mapINVLToUpdate = SysycoAutoMatchAndValidationHelper.mapLinesToUpdate;
        if (mapInvoiceWithExceptions.get(invoice.Id) != null) {
            for (String message : mapInvoiceWithExceptions.get(invoice.Id).split(';')) {
                System.debug('****message***' + message);
                if (message == System.Label.GRN_Line_Not_Found || message == System.Label.PO_Line_Not_Found || message == System.Label.Price_Mismatch || message == System.Label.Catch_Weight_Mismatch || message == System.Label.Quantity_Mismatch) {
                    message = null;
                }
                if (message != null && message != System.Label.Duplicate_Invoice) {
                    exceptionStr += message + ';';
                }
                else if(message == System.Label.Duplicate_Invoice){
                    exceptionStr = message;
                }
            }
        }
        if (!String.isBlank(exceptionStr)) {
            childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionStr);
        } else {
            childSobject.put(childNameSpace + 'Exception_Reason__c', mapInvoiceWithExceptions.get(invoice.Id));
        }

        childSobject.put(childNameSpace + 'Invoice_Rounding_Amount__c', mapInvoiceWithVariance.get(invoice.Id) != null ? Decimal.valueOf(mapInvoiceWithVariance.get(invoice.Id)).setScale(2, RoundingMode.HALF_UP) : 0.0);
        childSobject.put(childNameSpace + 'Line_Level_Tolerance__c', mapInvWithLineTolerance.get(invoice.Id) != null ? mapInvWithLineTolerance.get(invoice.Id) : '');
        childSobject.put(childNameSpace + 'Charge_Level_Tolerance__c', mapInvWithChargeTolerance.get(invoice.Id) != null ? mapInvWithChargeTolerance.get(invoice.Id) : '');
        childSobject.put(childNameSpace + 'Line_Level_Variance__c', mapInvWithLineVariance.get(invoice.Id) != null ? Decimal.valueOf(mapInvWithLineVariance.get(invoice.Id)).setScale(2, RoundingMode.HALF_UP) : 0.0);
        childSobject.put(childNameSpace + 'Charge_Level_Variance__c', mapInvWithChargeVariance.get(invoice.Id) != null ? Decimal.valueOf(mapInvWithChargeVariance.get(invoice.Id)).setScale(2, RoundingMode.HALF_UP) : 0.0);
        childSobject.put(childNameSpace + 'AF_Flag__c', mapInvoiceWithFlagAF.get(invoice.Id) != null ? mapInvoiceWithFlagAF.get(invoice.Id) : 'N');
        if (!String.isBlank(exceptionStr)) {
            ValExecMsgMap.put('Invoice_Exception', exceptionStr);
        } else {
            ValExecMsgMap.put('Invoice_Exception', mapInvoiceWithExceptions.get(invoice.Id));
        }
        validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));

        // updating invoice lines
        if (mapINVLToUpdate.keyset() != null) {
            System.debug('mapINVLToUpdate.values():::: ' + mapINVLToUpdate.values());
            List<Database.SaveResult> lstINVLUpdateResults = Database.update(mapINVLToUpdate.values(), false);
            for (Database.SaveResult sr : lstINVLUpdateResults) {
                if (sr.isSuccess()) {
                    System.debug('Successfully updated Invoice Line Item with ID: ' + sr.getId());
                } else {
                    for (Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Invoice Line Item fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        return validationMap;
    }
    
    public static Map<String, String> costCenterCheckForInvoices(List<Invoice__c> invList, List<Invoice_Line_Item__c> lineItems) { 
    	Map<String, String> ValidExecMsgsMap = new Map<String, String>();
    	if(!String.isBlank(invList[0].Current_State__c) && invList[0].ERP_Type__c == 'Workday' && invList[0].Document_Type__c == 'Non PO Invoice'){
        	for (Invoice_Line_Item__c  line : lineItems) {
                if(line.Cost_Code__c != null && !String.isBlank(line.Cost_Code__r.Name) && String.isBlank(line.Project__c) && line.Cost_Code__r.Name == 'BT - Data Processing - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Finance,Vendor Management & Procurement - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Enterprise Project Management Office - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Master Data Management Project Cost - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Project Enterprise Sales Marketing - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Enterprise Information Management - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Emerging Technologies and Mobile - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Enterprise Technology Services - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Infra HLP Security QA CAFINA - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Quality Analysis and Testing - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Security and Administration - Corporate - Sysco' ||
    				line.Cost_Code__r.Name == 'BT - Training and Development - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Application Development - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Enterprise Architecture - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Integration Deployment - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Infrastructure - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Communication - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Service Desk - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Marketing - Corporate - Sysco' || 
    				line.Cost_Code__r.Name == 'BT - Sales - Corporate - Sysco'){
    				ValidExecMsgsMap.put('Cost_Exception', 'Please provide Project for the specified cost code');
                }
            }
        }
        return ValidExecMsgsMap;
    }
    
    public static List<Invoice__c> duplicateCheckForInvoices(List<Invoice__c> invList) {
        Set<String> opcoCode = new Set<String> ();
        Set<String> invNo = new Set<String> ();
        Set<String> vendor = new Set<String> ();
        Set<String> invId = new Set<String> ();
        Set<String> chkDupl = new Set<String> ();
        for (Invoice__c invM : invList)
        {
            if (!String.isBlank(invM.OPCO_Code__c) && !String.isBlank(invM.Invoice_No__c) && !String.isBlank(invM.Vendor_No__c))
            {
                if (!chkDupl.contains(invM.OPCO_Code__c + '~' + invM.Invoice_No__c + '~' + invM.Vendor_No__c))
                {
                    chkDupl.add(invM.OPCO_Code__c + '~' + invM.Invoice_No__c + '~' + invM.Vendor_No__c);
                    invM.isDuplicate__c = false;
                }
                else
                {
                    invM.isDuplicate__c = true;
                }
            }
            else
            {
                invM.isDuplicate__c = false;
            }
        }
        for (Invoice__c invM : invList)
        {
            opcoCode.add(invM.OPCO_Code__c);
            invNo.add(invM.Invoice_No__c);
            vendor.add(invM.Vendor_No__c);
            if (invM.Id != null)
            {
                invId.add(invM.Id);
            }
        }
        List<Invoice__c> invlDat = Database.query(CommonFunctionController.getSOQLQuery(UtilityController.esmNameSpace + 'Invoice__c', 'OPCO_Code__c IN :opcoCode AND Invoice_No__c IN :invNo AND Vendor_No__c IN :vendor and (Reject_Reasons__c = null or Reject_Reasons__c=\'\') and Id not in :invId AND current_State__c != \'Rejected\' '));
        for (Invoice__c invM : invList)
        {
            if (!invM.isDuplicate__c && !String.isBlank(invM.OPCO_Code__c) && !String.isBlank(invM.Invoice_No__c) && !String.isBlank(invM.Vendor_No__c))
            {
                for (Invoice__c inv : invlDat)
                {
                    if (inv.OPCO_Code__c == invM.OPCO_Code__c && inv.Vendor_No__c == invM.Vendor_No__c && inv.Invoice_No__c.equalsIgnoreCase(invM.Invoice_No__c))
                    {
                        invM.isDuplicate__c = true;
                    }
                }
            }
        }
        System.debug(invList);
        return invList;
    }

    public static Set<String> setDuplicateCheckForInvoices(List<Invoice__c> invList) {
        invList = duplicateCheckForInvoices(invList);
        Set<String> setInv = new Set<String> ();
        for (Invoice__c inv : invList) {
            if (inv.isDuplicate__c) {
                setInv.add(inv.Id);
            }
        }
        return setInv;
    }
}