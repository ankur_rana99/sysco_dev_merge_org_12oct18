@isTest
public class CheckDOASyscoTest {

    static List < User_Master__c > userMastList = new List < User_Master__c > ();
    //static List < User_Master__c > userMastList1 = new List < User_Master__c > ();
    static List<DOA_Matrix__c> daoMatList = new List<DOA_Matrix__c>();
    
    public static void setupData() {
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        //sysconfig.Sub_Module__c ='Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        
        Purchase_Order__c po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 123;
        insert po;
        
        OPCO_Code__c op_code = new OPCO_Code__c();
        op_code.OPCO_Code__c = '1001';
        op_code.OPCO_Name__c = 'Corporate';
        insert op_code;
        
        User_Master__c userMast1 = new User_Master__c();
        userMast1.name='testuser1';
        userMast1.User_Name__c='testuser1@123';
        userMast1.OPCO_Code__c = op_code.Id;
        userMast1.DAP_Title__c = 'CEO';
        userMast1.Entity__c = 'Corporate';
        userMast1.Title_Type__c = 'Finance';
        userMast1.Status__c = 'Active';
        userMast1.User_Type__c = 'Requestor/Buyer';
        insert userMast1;
        
        system.debug('user1 inserted: '+userMast1);
    //  userMastList.add(userMast1);
        
        User_Master__c userMast2 = new User_Master__c();
        userMast2.name='testuser2';
        userMast2.User_Name__c='testuser2@123';
        userMast2.OPCO_Code__c = op_code.Id;
        userMast2.DAP_Title__c = 'VP';
        userMast2.Entity__c = 'Corporate';
        userMast2.Title_Type__c = 'Finance';
        userMast2.Status__c = 'Active';
        userMast2.User_Type__c = 'Requestor/Buyer';
        userMast2.Supervisor__c = userMast1.Id;
        insert userMast2;
        //userMastList.add(userMast2);
        
        User_Master__c userMast3 = new User_Master__c();
        userMast3.name='testuser3';
        userMast3.User_Name__c='testuser3@123';
        userMast3.OPCO_Code__c = op_code.Id;
        userMast3.Entity__c = 'Corporate';
        userMast3.DAP_Title__c = 'Market CFO';
        userMast3.Title_Type__c = 'Finance';
        userMast3.Status__c = 'Active';
        userMast3.User_Type__c = 'Requestor/Buyer';
        userMast3.Supervisor__c = userMast2.Id;
        //userMastList.add(userMast3);
        
        User_Master__c userMast4 = new User_Master__c();
        userMast4.name='testuser4';
        userMast4.User_Name__c='testuser4@123';
        userMast4.OPCO_Code__c = op_code.Id;
        userMast4.DAP_Title__c = 'OpCo CFO';
        userMast4.Entity__c = 'Corporate';
        userMast4.Title_Type__c = 'Finance';
        userMast4.Status__c = 'Active';
        userMast4.User_Type__c = 'Requestor/Buyer';
        
        User_Master__c userMast5 = new User_Master__c();
        userMast5.name='testuser5';
        userMast5.User_Name__c='testuser5@123';
        userMast5.OPCO_Code__c = op_code.Id;
        userMast5.DAP_Title__c = 'VP';
        userMast5.Entity__c = 'Corporate';
        userMast5.Title_Type__c = 'Functional';
        userMast5.Status__c = 'Active';
        userMast5.User_Type__c = 'Requestor/Buyer';
        
        userMastList.add(userMast5);
        insert userMastList;
        userMastList.clear();
        userMastList.add(userMast1);
        userMastList.add(userMast2);
        userMastList.add(userMast3);
        userMastList.add(userMast4);
        userMastList.add(userMast5);
            
        
        DOA_Setting__c daoSet = new DOA_Setting__c();
        daoSet.Amount_Field__c = 'Total_Amount__c';
        daoSet.First_Approver_Selection_Field__c = 'Requestor_Buyer__c';
        daoSet.Threshold_Amount__c = 75000;
        insert daoSet;
        system.debug('DOA inserted: '+daoSet);
        DOA_Matrix__c daoMat1 = new DOA_Matrix__c();
        daoMat1.OPCO_Code__c = op_code.Id;
        daoMat1.is_Functional__c = false;       
        daoMat1.Supervisor__c = null;
        daoMat1.Entity__c = 'Corporate';
        daoMat1.Active__c = true;
        daoMat1.DoA_Title__c = 'OpCo CFO';
        daoMat1.Approver_User__c = userMast4.Id;
        daoMatList.add(daoMat1);
        
        DOA_Matrix__c daoMat2 = new DOA_Matrix__c();
        daoMat2.OPCO_Code__c = op_code.Id;
        daoMat2.is_Functional__c = false;
        daoMat2.Supervisor__c = userMast2.Id;
        daoMat2.Entity__c = 'Corporate';
        daoMat2.Active__c = true;
        daoMat2.DoA_Title__c = 'Market CFO';
        daoMat2.DOA_Limit__c = 50000;
        daoMat2.Approver_User__c = userMast3.Id;
        daoMatList.add(daoMat2);
        
        DOA_Matrix__c daoMat3 = new DOA_Matrix__c();
        daoMat3.OPCO_Code__c = op_code.Id;
        daoMat3.is_Functional__c = false;
        daoMat3.Supervisor__c = userMast1.Id;
        daoMat3.Entity__c = 'Corporate';
        daoMat3.Active__c = true;
        daoMat3.DoA_Title__c = 'VP';
        daoMat3.DOA_Limit__c = 100000;
        daoMat3.Approver_User__c = userMast2.Id;
        daoMatList.add(daoMat3);
        insert daoMatList;
        system.debug('DOAmatrix inserted: '+daoMatList);
        
        
        Approval_Detail__c appDet = new Approval_Detail__c();
        //appDet.Approver_User__c = userMastList.get(2).Id;
        appDet.Approval_Cycle__c = 1;
        //appDet.Invoice__c = invobj.Id;
        appDet.Status__c = 'Pending';
        appDet.Is_Threshold__c = false;             
        insert appDet;
        system.debug('Appr details: '+appDet);
    }
    //When Current Approver = null
    public static testMethod void unitTest01() {
  
        List < User_Master__c > userMastList = new List < User_Master__c > (); 
        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        //sysconfig.Sub_Module__c ='Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c='Auto_Match';
        sysconfig1.Sub_Module__c ='Auto_Match_Current_State';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;
        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c='Invoice__c';
        sysconfig2.Sub_Module__c ='OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        
        Purchase_Order__c po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 123;
        insert po;
        
        OPCO_Code__c op_code = new OPCO_Code__c();
        op_code.OPCO_Code__c = '1001';
        op_code.OPCO_Name__c = 'Corporate';
        insert op_code;
        
        User_Master__c userMast1 = new User_Master__c();
        userMast1.name='testuser1';
        userMast1.User_Name__c='testuser1@123';
        userMast1.OPCO_Code__c = op_code.Id;
        userMast1.DAP_Title__c = 'CEO';
        userMast1.Entity__c = 'Corporate';
        userMast1.Title_Type__c = 'Finance';
        userMast1.Status__c = 'Active';
        userMast1.User_Type__c = 'Requestor/Buyer';
        insert userMast1;
        
        system.debug('user1 inserted: '+userMast1);
    //  userMastList.add(userMast1);
        
        User_Master__c userMast2 = new User_Master__c();
        userMast2.name='testuser2';
        userMast2.User_Name__c='testuser2@123';
        userMast2.OPCO_Code__c = op_code.Id;
        userMast2.DAP_Title__c = 'VP';
        userMast2.Entity__c = 'Corporate';
        userMast2.Title_Type__c = 'Finance';
        userMast2.Status__c = 'Active';
        userMast2.User_Type__c = 'Requestor/Buyer';
        //userMast2.Supervisor__c = userMast1.Id;
        insert userMast2;
        //userMastList.add(userMast2);
        
        User_Master__c userMast3 = new User_Master__c();
        userMast3.name='testuser3';
        userMast3.User_Name__c='testuser3@123';
        userMast3.OPCO_Code__c = op_code.Id;
        userMast3.Entity__c = 'Corporate';
        userMast3.DAP_Title__c = 'Market CFO';
        userMast3.Title_Type__c = 'Finance';
        userMast3.Status__c = 'Active';
        userMast3.User_Type__c = 'Requestor/Buyer';
        userMast3.Supervisor__c = userMast2.Id;
        insert userMast3;
        //userMastList.add(userMast3);
        
        User_Master__c userMast4 = new User_Master__c();
        userMast4.name='testuser4';
        userMast4.User_Name__c='testuser4@123';
        userMast4.OPCO_Code__c = op_code.Id;
        userMast4.DAP_Title__c = 'OpCo CFO';
        userMast4.Entity__c = 'Corporate';
        userMast4.Title_Type__c = 'Finance';
        userMast4.Status__c = 'Active';
        userMast4.User_Type__c = 'Requestor/Buyer';
        insert userMast4;
        
        User_Master__c userMast5 = new User_Master__c();
        userMast5.name='testuser5';
        userMast5.User_Name__c='testuser5@123';
        userMast5.OPCO_Code__c = op_code.Id;
        userMast5.DAP_Title__c = 'VP';
        userMast5.Entity__c = 'Corporate';
        userMast5.Title_Type__c = 'Functional';
        userMast5.Status__c = 'Active';
        userMast5.User_Type__c = 'Requestor/Buyer';
        insert userMast5; 
        
        DOA_Setting__c daoSet = new DOA_Setting__c();
        daoSet.Amount_Field__c = 'Total_Amount__c';
        daoSet.First_Approver_Selection_Field__c = 'Requestor_Buyer__c';
        daoSet.Threshold_Amount__c = 75000;
        insert daoSet;
        system.debug('DOA inserted: '+daoSet);
        DOA_Matrix__c daoMat1 = new DOA_Matrix__c();
        daoMat1.OPCO_Code__c = op_code.Id;
        daoMat1.is_Functional__c = false;       
        daoMat1.Supervisor__c = null;
        daoMat1.Entity__c = 'Corporate';
        daoMat1.Active__c = true;
        daoMat1.DoA_Title__c = 'OpCo CFO';
        daoMat1.Approver_User__c = userMast4.Id;
        daoMatList.add(daoMat1);
        
        DOA_Matrix__c daoMat2 = new DOA_Matrix__c();
        daoMat2.OPCO_Code__c = op_code.Id;
        daoMat2.is_Functional__c = false;
        daoMat2.Supervisor__c = userMast2.Id;
        daoMat2.Entity__c = 'Corporate';
        daoMat2.Active__c = true;
        daoMat2.DoA_Title__c = 'Market CFO';
        daoMat2.DOA_Limit__c = 50000;
        daoMat2.Approver_User__c = userMast3.Id;
        daoMatList.add(daoMat2);
        
        DOA_Matrix__c daoMat3 = new DOA_Matrix__c();
        daoMat3.OPCO_Code__c = op_code.Id;
        daoMat3.is_Functional__c = false;
        daoMat3.Supervisor__c = userMast1.Id;
        daoMat3.Entity__c = 'Corporate';
        daoMat3.Active__c = true;
        daoMat3.DoA_Title__c = 'VP';
        daoMat3.DOA_Limit__c = 100000;
        daoMat3.Approver_User__c = userMast2.Id;
        daoMatList.add(daoMat3);
        insert daoMatList;
        system.debug('DOAmatrix inserted: '+daoMatList);
        
        
        //setupData();
        
        Invoice__c invobj1 = new Invoice__c();
        invobj1.Invoice_Date__c = Date.Today();
        invobj1.Net_Due_Date__c = Date.Today().addDays(60);
        invobj1.Total_Amount__c = 60000;
        //invobj1.Requestor_Buyer__c = userMast1.id;
       // invobj1.User_Action__c = 'Send For Approval';
        invobj1.Current_Cycle__c = 1;
         insert invobj1;
        /*List < Invoice__c > invList1 = new List < Invoice__c > ();
        invList1.add(invobj1);
        insert invList1;
        /*
        System.debug('invobj1' + invobj1);
        /*Approval_Detail__c appDet1 = new Approval_Detail__c();
        appDet1.Approver_User__c = userMast.Id;
        appDet1.Approval_Cycle__c = 1;
        appDet1.Invoice__c = invobj1.Id;
        appDet1.Is_Threshold__c = false;
        List < Approval_Detail__c > appDetList1 = new List < Approval_Detail__c > ();
        appDetList1.add(appDet1);
        insert appDetList1;*/
        
        //System.debug('appDet1' + appDet1);
        //CheckDOASysco.checkUserDOA(invList1);
    }
    //When Current Approver = R/B
    public static testMethod void unitTest02() {
        setupData();
        
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 60000;
        invobj.Current_Approver__c = userMastList.get(2).id;
        invobj.Requestor_Buyer__c = userMastList.get(2).id;
        invobj.Document_Type__c = 'Non PO Invoice';
        invobj.WorkType__c = 'Normal Invoices';
       // invobj.User_Action__c = 'Approve';
        invobj.Current_Cycle__c = 1;
        
        // insert invobj;
        List < Invoice__c > invList = new List < Invoice__c > ();
        invList.add(invobj);
        insert invList;
        
        List < Approval_Detail__c > appDetList = new List < Approval_Detail__c > ();
        Approval_Detail__c appDet = new Approval_Detail__c();
        appDet.Approver_User__c = userMastList.get(2).Id;
        appDet.Approval_Cycle__c = 1;
        appDet.Invoice__c = invobj.Id;
        appDet.Status__c = 'Pending';
        appDet.Is_Threshold__c = false;             
        appDetList.add(appDet);
        insert appDetList;
        
        System.debug('appDet' + appDet);
        CheckDOASysco.checkUserDOA(invList);
    }
    
    //When Work Type = 'CPAS' // Opco CFO user
    public static testMethod void unitTest03() {
        setupData();
        
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 60000;
        invobj.Current_Approver__c = userMastList.get(2).id;
        invobj.Requestor_Buyer__c = userMastList.get(2).id;
        invobj.Document_Type__c = 'Payment Request';
        invobj.WorkType__c = 'CPAS';
      //  invobj.User_Action__c = 'Approve';
        invobj.Current_Cycle__c = 1;
        // insert invobj;
        List < Invoice__c > invList = new List < Invoice__c > ();
        invList.add(invobj);
        insert invList;
        
        System.debug('invobj' + invobj);
        Approval_Detail__c appDet = new Approval_Detail__c();
        appDet.Approver_User__c = userMastList.get(2).Id;
        appDet.Approval_Cycle__c = 1;
        appDet.Invoice__c = invobj.Id;
        appDet.Is_Threshold__c = false;
        appDet.Status__c = 'Pending';
        List < Approval_Detail__c > appDetList = new List < Approval_Detail__c > ();
        appDetList.add(appDet);
        insert appDetList;
        
        System.debug('appDet' + appDet);
        CheckDOASysco.checkUserDOA(invList);
    }
    
     //When Current Approver Reject invoice
    public static testMethod void unitTest04() {
        setupData();
        
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 60000;
        invobj.Current_Approver__c = userMastList.get(1).id;
        invobj.Requestor_Buyer__c = userMastList.get(2).id;
        invobj.Document_Type__c = 'Non PO Invoice';
        invobj.WorkType__c = 'Normal Invoices';
        //invobj.User_Action__c = 'Reject';
        invobj.Current_Cycle__c = 1;
        
        // insert invobj;
        List < Invoice__c > invList = new List < Invoice__c > ();
        invList.add(invobj);
        insert invList;
        
      /*  List < Approval_Detail__c > appDetList = new List < Approval_Detail__c > ();
        Approval_Detail__c appDet = new Approval_Detail__c();
        appDet.Approver_User__c = userMastList.get(1).Id;
        appDet.Approval_Cycle__c = 1;
        appDet.Invoice__c = invobj.Id;
        appDet.Status__c = 'Pending';
        appDet.Is_Threshold__c = false;             
        appDetList.add(appDet);
        insert appDetList;*/
        
       // System.debug('appDet' + appDet);
        CheckDOASysco.checkUserDOA(invList);
    }
    
     //When Current Approver Reject invoice
    public static testMethod void unitTest05() {
        setupData();
        
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 90000;
        invobj.Current_Approver__c = userMastList.get(1).id;
        invobj.Requestor_Buyer__c = userMastList.get(2).id;
        invobj.Document_Type__c = 'Non PO Invoice';
        invobj.WorkType__c = '  Normal Invoices';
       // invobj.User_Action__c = 'Approve';
        invobj.Current_Cycle__c = 1;
        
        // insert invobj;
        List < Invoice__c > invList = new List < Invoice__c > ();
        invList.add(invobj);
        insert invList;
        
      /*  List < Approval_Detail__c > appDetList = new List < Approval_Detail__c > ();
        Approval_Detail__c appDet = new Approval_Detail__c();
        appDet.Approver_User__c = userMastList.get(1).Id;
        appDet.Approval_Cycle__c = 1;
        appDet.Invoice__c = invobj.Id;
        appDet.Status__c = 'Pending';
        appDet.Is_Threshold__c = false;             
        appDetList.add(appDet);
        insert appDetList;*/
        
       // System.debug('appDet' + appDet);
        CheckDOASysco.checkUserDOA(invList);
    }
    
     public static testMethod void unitTest06() {
        setupData();
        
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 90000;
        invobj.Current_Approver__c = userMastList.get(4).id;
        invobj.Requestor_Buyer__c = userMastList.get(2).id;
        invobj.Document_Type__c = 'Non PO Invoice';
        invobj.WorkType__c = '  Normal Invoices';
        //invobj.User_Action__c = 'Approve';
        invobj.Current_Cycle__c = 1;
        
        // insert invobj;
        List < Invoice__c > invList = new List < Invoice__c > ();
        invList.add(invobj);
        insert invList;
        
        /*List < Approval_Detail__c > appDetList = new List < Approval_Detail__c > ();
        Approval_Detail__c appDet = new Approval_Detail__c();
        appDet.Approver_User__c = userMastList.get(4).Id;
        appDet.Approval_Cycle__c = 1;
        appDet.Invoice__c = invobj.Id;
        appDet.Status__c = 'Pending';
        appDet.Is_Threshold__c = false;             
        appDetList.add(appDet);
        insert appDetList;*/
        
     //   System.debug('appDet' + appDet);
        CheckDOASysco.checkUserDOA(invList);
    }
    
}