global class SyscoPostingErpDataScheduler implements Schedulable 
    {
        global SyscoPostingErpDataScheduler(){
    }
    global void execute(SchedulableContext objSchedulableContext) {
     SyscoPostingErpData syscoerp  = new SyscoPostingErpData(objSchedulableContext);
     Integer batchSize = 50; 
     system.database.executebatch(syscoerp,batchSize); 
    }
}