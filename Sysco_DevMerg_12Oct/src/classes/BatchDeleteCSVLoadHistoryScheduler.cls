global class BatchDeleteCSVLoadHistoryScheduler implements Schedulable  {

    global void execute(SchedulableContext objSchedulableContext) {
        BatchDeleteCSVLoadHistory deletecsvhis = new BatchDeleteCSVLoadHistory();
        system.database.executebatch(deletecsvhis,200);         
    }
}