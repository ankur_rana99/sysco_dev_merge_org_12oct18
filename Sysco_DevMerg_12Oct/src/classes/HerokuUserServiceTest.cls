@isTest
public class HerokuUserServiceTest {
 public static testmethod void testmethod1()
 {
      System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Heroku User Service';
        sysconfig.Sub_Module__c='HeroKu Base URL';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
     
        System_Configuration__c sysconfig1=new System_Configuration__c ();
        sysconfig1.Module__c='Custom_Record_History';
        sysconfig1.Value__c ='Invoice__c';
        insert sysconfig1;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         
         Account accobj = new Account();
         accobj.Name='TestAccount';
         insert accobj;

    	 Invoice__c invobj = new Invoice__c();    
    	 invobj.Invoice_Date__c = Date.Today();
    	 invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	 invobj.Total_Amount__c = 500.00;
    
    	 System.debug('invobj1' + invobj);
    	 insert invobj;
     
     HerokuUserService obj=new HerokuUserService();
     
     User_Master__c user=new User_Master__c();
     user.First_Name__c='Vishwa';
     user.User_Type__c='Requestor/Buyer';
     insert user;
     
    // obj.createMap(user);
     Set<Id> ids=new Set<Id>();
     ids.add(user.Id);
       List<Id> ids1=new List<Id>();
     ids1.add(user.Id);
     HerokuUserService.createHerokuUserDetails(ids1);
     obj.createHerokuUserDetails(ids);
 
 }
}