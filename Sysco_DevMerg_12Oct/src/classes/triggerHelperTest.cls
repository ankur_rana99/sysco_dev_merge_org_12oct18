@isTest
public class triggerHelperTest{
       public static testMethod void testFirst(){
    System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
           
    Invoice__c invobj = new Invoice__c();    
      invobj.Invoice_Date__c = Date.Today();
      invobj.Net_Due_Date__c = Date.Today().addDays(60);    
      invobj.Total_Amount__c = 500.00;
        invobj.Status__c  = 'Approved';
        //invobj.Current_State__c='Vouched';
        invobj.Current_State__c = 'Pending Merchandise Processing';
       System.debug('invobj' + invobj);
        insert invobj;
        list<Invoice__c> listInv = new list<Invoice__c>();
       listInv.add(invobj);
          
           Invoice__c invobj1 = new Invoice__c();    
      invobj1.Invoice_Date__c = Date.Today();
      invobj1.Net_Due_Date__c = Date.Today().addDays(60);    
      invobj1.Total_Amount__c = 500.00;
        invobj1.Status__c  = 'Approved';
        //invobj.Current_State__c='Vouched';
        invobj1.Current_State__c = 'Pending Merchandise Processing';
       System.debug('invobj' + invobj);
        insert invobj1;
        list<Invoice__c> listInv1 = new list<Invoice__c>();
       listInv1.add(invobj); 
           
        Map<Id, sObject> newMap = new Map<Id, sObject>();   
        newMap.put(invobj.id,invobj);
        Map<Id, sObject> oldMap = new Map<Id, sObject>();   
        oldMap.put(invobj1.id,invobj);
        triggerHelper.insertUpdateHistory(listInv,listInv1,true,true,newMap,oldMap,'Invoice__c');
        triggerHelper.captureRecordHistory(listInv,listInv1,true,true,newMap,oldMap,'');
      
    }

}