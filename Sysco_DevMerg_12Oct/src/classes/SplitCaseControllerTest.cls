@isTest
public with sharing class SplitCaseControllerTest {
	@testSetup
	static void setupTestData() {
		CaseManager__c caseManager = new CaseManager__c();

		caseManager.Status__c = 'Start';
        caseManager.WorkType__c = 'PO Invoice';
        caseManager.User_Action__c = 'Create';
        caseManager.Requester_Email_Cus__c = 'user@user.com';
        caseManager.Process__c = 'AP Inquiries';
        caseManager.Mailbox_Name__c = 'AP - Mailbox';
        caseManager.Requestor_Email_Id__c  = 'test@email.com';
		insert caseManager;
		SplitCaseController.getSplitCaseFields(caseManager,caseManager.id);
		TestDataGenerator.createContent(caseManager);
	}
	
	
	private static testmethod void testSplit() {
        
		CaseManager__c caseManager = [select Id, name, Process__c,Mailbox_Name__c,User_Action__c,Requestor_Email_Id__c,WorkType__c, Status__c, RecordTypeId, RecordType.Name from CaseManager__c];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
		 Map<String, Object> responseObj=SplitCaseController.getAttachments(caseManager.id);

		List<SplitCaseController.SplitCase> splitList = SplitCaseController.CloneCase(caseManager, 3);

		For (SplitCaseController.SplitCase splitCase : splitList) {
			splitCase.SelectedAttachments=(List<ContentDocumentLink>) responseObj.get('attachments');
		}
		system.debug(splitList);
        
		SplitCaseController.performSplitCase(caseManager, JSON.serialize(splitList), 'Test');
        }
       
	}
}