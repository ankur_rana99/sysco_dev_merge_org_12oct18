global with sharing class CsvLoadBatch implements Database.batchable < String >, Database.Stateful
{

  private String m_csvFile;
  CSV_Load_Configuration__c config;

  //for manage error
  Integer errorCount = 0;

  //for mapping
  private Map<Integer, String> headerIndex = new Map<Integer, String> ();
 

  //for inovice status set map
  private Map<String, String> invStatusMap = new Map<String, String> ();


  //for Supplier detail
  //Comment By Mekyush
  /*private String supplierName;
    private String supplierId;
    private String supplierNumber;*/
  //Comment By Mekyush

  public Integer lineCounter { get; set; }
  private String invoiceKey;

  private String batchNo;

  Private String dateFormat;
  Private String regExpDate;
 


  Boolean isError = false;
  String errorText = '';
  String originalFileName;
  String content = '';

 String hisId;

  private String oldInvoiceKey = '';

  //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Starts ---// 
  final String childObjectName;
  String childNamespace;
  public boolean hasError;
  public string errMsg;
  string initCurrentState;
  string initUserAction;
  Map<String, Map<String, String>> mapObjectHeaderAPI;
  Map<String, Map<String, String>> mapObjectApiDataType;
  Map<String, Map<String, Boolean>> mapObjectHeaderRequired;

  Map<String, integer> mapObjectUniqueNumber;
  Map<String, String> mapObjectRefToChildObj;
  list<string> lstLoadObject;

  Map<String, Map<String, Map<String, string>>> mapObjFieldKeyId;
  Map<String, Schema.Sobjectfield> fieldMap;

  String childObjId;

  boolean isErrorInPrevRec = false;


  //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Ends---// 

  public CsvLoadBatch(String bNo, String fileName, blob file,String parentId) {
   

    batchNo = bNo;
    hisId = parentId;

    system.debug('parentId in CsvLoadBatch ' + hisId);
    //for set file Name
    originalFileName = fileName;

    //for set line index
    lineCounter = 0;

    //for get all data from file
    m_csvFile = file.toString();

    //for fecth config
    //A  config = [ Select Invoice_Column_Start_Index__c,Invoice_Column_End_Index__c,Invoice_Unique_Key_Column_Index__c,Line_Item_Identify_Column_Index__c,Charges_Identify_Column_Index__c,Date_Format__c From CSV_Load_Configuration__c LIMIT 1];
    hasError = false;
    config = CSV_Load_Configuration__c.getOrgDefaults();

      system.debug(' @@ config : ' + config);
    childObjectName = config.ChildObject_Name__c;
     system.debug(' @@ childObjectName : ' + childObjectName);
    String additionalObj = config.Additional_Object_Name__c;
    String additionalObjUniqueCol = config.Additional_Object_UniqueKey_Column_Index__c;
    String additionalObjRefFieldToChldObj = config.Additional_Object_Reference_Field__c;
    initCurrentState = config.Initial_Current_State__c;
    initUserAction = config.Initial_User_Action__c;
    if (childObjectName == null || childObjectName == '') {
      errMsg = 'Please Configure Primary Object details in CSV Load Configuration Custom Setting';
      hasError = true;
      return;
    }
    if (additionalObj != null && (additionalObjUniqueCol == null || additionalObjRefFieldToChldObj == null ||
                                  additionalObj.split(',').size() != additionalObjUniqueCol.split(',').size() ||
                                  additionalObj.split(',').size() != additionalObjRefFieldToChldObj.split(',').size())) {

      errMsg = 'Please Configure Additional Objects details in CSV Load Configuration Custom Setting';
      hasError = true;
      return;
    }

    //set date format
    String prefix = getNameSpace('CsvLoadBatch');
    if (config.get(prefix + 'Date_Format__c') != null) {
      dateFormat = String.valueOf(config.get(prefix + 'Date_Format__c'));
    } else {
      dateFormat = 'dd/mm/yyyy';
    }

    //set reg expression for date
    if (dateFormat.equalsIgnoreCase('dd/mm/yyyy')) {
      regExpDate = '\\d{2}/\\d{2}/\\d{4}';
    } else if (dateFormat.equalsIgnoreCase('mm/dd/yyyy')) {
      regExpDate = '\\d{2}/\\d{2}/\\d{4}';
    } else if (dateFormat.equalsIgnoreCase('dd/yyyy/mm')) {
      regExpDate = '\\d{2}/\\d{4}/\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('mm/yyyy/dd')) {
      regExpDate = '\\d{2}/\\d{4}/\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('yyyy/mm/dd')) {
      regExpDate = '\\d{4}/\\d{2}/\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('yyyy/dd/mm')) {
      regExpDate = '\\d{4}/\\d{2}/\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('dd-mm-yyyy')) {
      regExpDate = '\\d{2}-\\d{2}-\\d{4}';
    } else if (dateFormat.equalsIgnoreCase('mm-dd-yyyy')) {
      regExpDate = '\\d{2}-\\d{2}-\\d{4}';
    } else if (dateFormat.equalsIgnoreCase('dd-yyyy-mm')) {
      regExpDate = '\\d{2}-\\d{4}-\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('mm-yyyy-dd')) {
      regExpDate = '\\d{2}-\\d{4}-\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('yyyy-mm-dd')) {
      regExpDate = '\\d{4}-\\d{2}-\\d{2}';
    } else if (dateFormat.equalsIgnoreCase('yyyy-dd-mm')) {
      regExpDate = '\\d{4}-\\d{2}-\\d{2}';
    }

    // START get mapping for all objects

    //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Starts ---//
    list<CSV_Load_Mapping__c> SSMlist = CSV_Load_Mapping__c.getall().values();
     system.debug(' @@ SSMlist : ' + SSMlist);

    mapObjectHeaderAPI = new Map<String, Map<String, String>> ();
    mapObjectApiDataType = new Map<String, Map<String, String>> ();
    mapObjectHeaderRequired = new Map<String, Map<String, Boolean>> ();
    mapObjectUniqueNumber = new Map<String, integer> ();
    mapObjectRefToChildObj = new Map<String, String> ();

    Map<String, String> mapHeaderAPI = new Map<String, String> ();
    Map<String, String> mapApiDataType = new Map<String, String> ();
    Map<String, boolean> mapHeaderRequired = new Map<String, boolean> ();

    lstLoadObject = new list<String> ();

    if (childObjectName != null && childObjectName != '') {


      lstLoadObject.add(childObjectName);
      if (additionalObj != null && additionalObj != '') {
        lstLoadObject.addall(additionalObj.split(','));
      }
      integer i = 0;
      for (String st : lstLoadObject) {
        st = st.trim();
         system.debug(' @@ st : ' + st);

        mapHeaderAPI = new Map<String, String> ();
        mapApiDataType = new Map<String, String> ();
        mapHeaderRequired = new Map<String, boolean> ();

        for (CSV_Load_Mapping__c obj : SSMlist) {
          if (obj.Object__c.equalsIgnoreCase(st)) {
            mapHeaderAPI.put(obj.Header_Name__c, obj.API_Name__c);
            mapApiDataType.put(obj.API_Name__c, obj.Data_Type__c);
            mapHeaderRequired.put(obj.Header_Name__c, obj.isRequired__c);
          }
        }
        mapObjectHeaderAPI.put(st, mapHeaderAPI);
        mapObjectApiDataType.put(st, mapApiDataType);
        mapObjectHeaderRequired.put(st, mapHeaderRequired);

        if (st.equalsignoreCase(childObjectName)) {
          mapObjectUniqueNumber.put(childObjectName, Integer.ValueOf(config.get(prefix + 'ChildObject_Unique_Key_Column_Index__c')));

        } else {
          mapObjectUniqueNumber.put(st, integer.ValueOf(additionalObjUniqueCol.split(',') [i]));
          mapObjectRefToChildObj.put(st, additionalObjRefFieldToChldObj.split(',') [i]);
          i++;
        }


      }

      Schema.SObjectType targetType = Schema.getGlobalDescribe().get(childObjectName); //From the Object Api name retrieving the SObject
      Sobject Object_name = targetType.newSObject();
      Schema.Sobjecttype objType = Object_name.getSObjectType();
      Schema.Describesobjectresult sobjRes = objType.getDescribe();
      fieldMap = sobjRes.fields.getMap();
    }
    //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Ends---//

    // END get mapping for all objects

    //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Starts ---// 
    CSV_Load_Configuration__c csvConf;
    if (CSV_Load_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
      csvConf = CSV_Load_Configuration__c.getInstance(Userinfo.getUserid());
    } else {
      csvConf = CSV_Load_Configuration__c.getOrgDefaults();
    }
     System.debug('#####csvConf ' + csvConf);
    CSVLoadConfig obj;
    try {
      if (csvConf.CSV_Load_Config_Class_Name__c != null && !String.valueOf(csvConf.CSV_Load_Config_Class_Name__c).equals('')) {
        obj = (CSVLoadConfig) Type.forName(csvConf.CSV_Load_Config_Class_Name__c).newInstance();
      }

       System.debug('#####obj ' + obj);
    } catch(exception e) {
      System.debug('#####====e==' + e);
    }
    if (obj != null) {
      mapObjFieldKeyId = obj.execute(null);
    }
    System.debug('====mapObjFieldKeyId====' + mapObjFieldKeyId);


    //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | ends---// 
    childNameSpace = '';
  
  }


  global Iterable < String > start(Database.batchableContext batchableContext)
  {
    CSVIteratorPLM it = null;

    try {

      it = new CSVIteratorPLM(m_csvFile, ParserPLM.LF);

    } catch(Exception e) {

      it = new CSVIteratorPLM(m_csvFile, ParserPLM.CRLF);

    }
    return it;
  }
 global static String getNamespace(String clsName) 
    {
    Map<String, String> persistNameSpace = new Map<String, String> ();
        if (persistNameSpace != null && persistNameSpace.get(clsName) != null)
        {
            return persistNameSpace.get(clsName);
        }
        else
        {
            return '';
        }
    }
     global static SObject getNewSobject(String t) {
        /* Call global describe to get the map of string to token. */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        /* Get the token for the sobject based on the type. */
        Schema.SObjectType st = gd.get(t);
        /* Instantiate the sobject from the token. */
        Sobject s = st.newSobject();
        return s;
    }


  global void execute(Database.BatchableContext batchableContext, List<String> scope)
  {
    //string POIDValidate;
  //  string BuyerIDValidate;
    String csvFile = '';
    for (String row : scope)
    {
      csvFile += row + ParserPLM.crlf;
    }
    system.debug(' @@@@@@ csvFile  :  '+ csvFile );
    List<List<String>> lines = CSVReaderPLM.readCSVFile(csvFile);
    system.debug(' @@@@@@ lines  :  '+ lines.size() );
    system.debug(' @@@@@@ lineCounter  :  '+ lineCounter);
    if (lineCounter == 0) {
      for (List<String> line : lines) {

        //for set header index
        Integer hIndex = 0;
        for (String column : line) {
          headerIndex.put(hIndex++, column.trim());
        }
        content += generateErrorRecord(line) + 'Error' + '\n';
        lineCounter++;

      }
    } else {
      boolean isErrorInChildObj = false;
      system.debug(' @@@@@@ lstLoadObject  :  '+ lstLoadObject);
      for (String loadObj : lstLoadObject) {
        System.debug('===loadObj===' + loadObj);
        for (List<String> line : lines) {
          System.debug('===line===' + line+loadObj);

          //for set header index
          //for insert invoice
          isError = false;
          errorText = '';

          //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Starts---// 
          Sobject childSobject;
          list<sObject> tmpSobject;

          Map<String, String> mapHeaderAPI = new Map<String, String> ();
          Map<String, String> mapApiDataType = new Map<String, String> ();
          Map<String, boolean> mapHeaderRequired = new Map<String, boolean> ();


          //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | ends---// 
          //for put static values
          try {
            mapHeaderAPI = mapObjectHeaderAPI.get(loadObj);
            mapApiDataType = mapObjectApiDataType.get(loadObj);
            mapHeaderRequired = mapObjectHeaderRequired.get(loadObj);

            //A String uniqueKeyAPI = mapHeaderAPI.get(headerIndex.get(Integer.valueOf(config.get('ChildObject_Unique_Key_Column_Index__c')) - 1)); 
            //kapil : 16.12.2015 | OpenWealth | make variable outside if() and check for unique key defined or not
            String uniqueKeyAPI = null;
            String uniqueKeyRec;
            boolean isUniqueKeyMissingForExtraObj = false;
            boolean isRecordSkipped = false;
            boolean isUniqueKeyRequired = true;
            //kapil : 16.12.2015 | OpenWealth | if unique key defined, check for uniqueKeyRec
            if (mapObjectUniqueNumber.get(loadObj) != null)
            {
              uniqueKeyAPI = mapHeaderAPI.get(headerIndex.get(mapObjectUniqueNumber.get(loadObj) - 1));
              if (line.size() > mapObjectUniqueNumber.get(loadObj) && line.get(mapObjectUniqueNumber.get(loadObj) - 1) != null && line.get(mapObjectUniqueNumber.get(loadObj) - 1).trim().length() > 0) {
                uniqueKeyRec = line.get(mapObjectUniqueNumber.get(loadObj) - 1);
                 System.debug('uniqueKeyRec=' + uniqueKeyRec);
              } else {
                if (loadObj.equalsIgnoreCase(childObjectName)) {
                  isError = true;
                  errorText = 'Unique key is not given for ' + loadObj + ' Object';
                  content += generateErrorRecord(line) + errorText + '\n';
                  errorCount++;
                } else {
                  System.debug('===loadObj in====' + loadObj);
                  isUniqueKeyMissingForExtraObj = true;
                }
              }
            } else
            {
              //kapil : 16.12.2015 | OpenWealth | if unique key not defined, set isUniqueKeyRequired, so we can use it while insert/update case
              isUniqueKeyRequired = false;
            }
            if (loadObj.equalsIgnoreCase(childObjectName)) {

              invoiceKey = uniqueKeyRec;
               System.debug('====invoiceKey===' + invoiceKey);
              if (isErrorInPrevRec) {
                //null check added : Kapil Adiyecha 05.01.2016 | one record skipped after exception issue
                if (oldInvoiceKey != null && oldInvoiceKey.equalsIgnoreCase(invoiceKey)) {
                  isErrorInChildObj = true;
                } else {
                  isErrorInChildObj = false;
                }
              }
            }
            System.debug('====oldInvoiceKey===' + oldInvoiceKey);

            //if(!isError && uniqueKeyRec != oldInvoiceKey && !isErrorInChildObj) {
            //kapil : 16.12.2015 | OpenWealth | added uniqueKeyAPI!=null --so unique key is not configured
            if (!isError && uniqueKeyRec != oldInvoiceKey && !isErrorInChildObj && uniqueKeyAPI != null) {
              //A tmp = [ Select id From Invoice__c Where Invoice_Key__c = :invoiceKey ];

              String query = 'Select id from ' + loadObj + ' where ' + uniqueKeyAPI + '= :uniqueKeyRec';
              System.debug('====query=====' + query);
              if (Schema.getGlobalDescribe().get(loadObj).getDescribe().isAccessible()) {
                tmpSobject = database.query(query);
              }
              System.debug('===tmpSobject===' + tmpSobject);
              if (tmpSobject.size() > 0) {
                //A  invoiceObj  = tmp.get(0);
                childSobject = tmpSobject.get(0);
              } else {
                childSobject =getNewSobject(loadObj);
              }
              if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'Batch_No__c').getDescribe().isUpdateable()){
                childSobject.put(childNamespace + 'Batch_No__c', batchNo);
              }
              childSobject.put(uniqueKeyAPI, uniqueKeyRec);
              if (childObjId != null && mapObjectRefToChildObj != null && mapObjectRefToChildObj.get(loadObj) != null) {
                childSobject.put(mapObjectRefToChildObj.get(loadObj), childObjId);
              }

            }
            else
            {
              childSobject = getNewSobject(loadObj);
              if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'Batch_No__c').getDescribe().isUpdateable()){
                childSobject.put(childNamespace + 'Batch_No__c', batchNo);
              }
              if (childObjId != null && mapObjectRefToChildObj != null && mapObjectRefToChildObj.get(loadObj) != null) {
                childSobject.put(mapObjectRefToChildObj.get(loadObj), childObjId);
              }
            }
            
            System.debug('==childSobject==@@' + childSobject);
            Integer hIndex = 0;
            String dataType;
            String apiName;
            String headerName;
            boolean isRequired = false;
            //kapil : 16.12.2015 | OpenWealth | added !isUniqueKeyRequired --so unique key is not configured
            if (!isError && (uniqueKeyRec != oldInvoiceKey || !isUniqueKeyRequired) && !isErrorInChildObj) {

              for (String column : line) {

                if (!isError)
                {
                  if (headerIndex.get(hIndex) != null) {
                    //for check column is for invoice or not..
                    if (mapHeaderAPI.containsKey(headerIndex.get(hIndex))) {
                      if (isUniqueKeyMissingForExtraObj) {
                        isRecordSkipped = true;
                        if (column != null && column.trim().length() > 0) {
                          isError = true;
                          isRecordSkipped = false;
                          errorText = 'Unique key is not given for ' + loadObj + ' Object';
                          content += generateErrorRecord(line) + errorText + '\n';
                          errorCount++;
                          break;
                        }

                      }
                      system.debug(' @@@@@@@@@@@ 2');
                      isRequired = mapHeaderRequired.get(headerIndex.get(hIndex));

                      system.debug('isRequired---' + isRequired);

                      headerName = headerIndex.get(hIndex).trim();
                      system.debug('--headerName ---' + headerName);
                      apiName = mapHeaderAPI.get(headerIndex.get(hIndex)).trim();
                      system.debug('--apiName ---' + apiName);

                      //for check required field validation
                      if (isRequired) {

                        if (column == null || column.trim().length() == 0) {

                          isError = true;
                          errorText = 'Required Field Missing' + ' : ' + apiname;
                          content += generateErrorRecord(line) + errorText + '\n';
                          //if(errorCount == 0) {
                          errorCount++;
                          //}

                        }

                      }

                      //for get data type
                      if (mapApiDataType.get(mapHeaderAPI.get(headerIndex.get(hIndex))) != null) {
                        dataType = mapApiDataType.get(mapHeaderAPI.get(headerIndex.get(hIndex))).trim();
                      }
                      system.debug(' @@ dataType : ' + dataType);
                      if (dataType.equalsIgnoreCase('Number')) {

                        if (column != null && column.trim().length() > 0) {
                          //Pattern Change : Kapil Adiyecha 04.01.2016 | openwealth Imanage#11319 | allow decimal value
                          if (Pattern.matches('^[0-9]+(\\.[0-9]{0,})?$', column.trim())) {
                            childSobject.put(apiName, Double.valueOf(column.trim()));
                            system.debug('@@#$ Number ' + apiName + ' : ' + column.trim());

                          } else {

                            isError = true;
                            errorText = 'Invalid Value' + ' : ' + apiname + '  value : ' + column.trim();
                            content += generateErrorRecord(line) + errorText + '\n';
                            //if(errorCount == 0) {
                            errorCount++;
                            //}

                          }
                        }

                      } else if (dataType.equalsIgnoreCase('Text')) {

                        if (column != null && column.trim().length() > 0) {

                          childSobject.put(apiName, column.trim());

                        }

                      } else if (dataType.equalsIgnoreCase('Reference')) {

                        String fieldName = apiName;
                        String fieldvalue;


                        //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Starts ---// 
                         System.debug('mapObjFieldKeyId  : ' + mapObjFieldKeyId);

                         System.debug('mapObjFieldKeyId.get(loadObj)  : ' + mapObjFieldKeyId.get(loadObj));
                         System.debug('mapObjFieldKeyId.get(loadObj).get(apiName)   : ' + mapObjFieldKeyId.get(loadObj).get(apiName) );
                        
                        if (column != null && column.length() > 0 && mapObjFieldKeyId != null &&
                            mapObjFieldKeyId.get(loadObj) != null &&
                            mapObjFieldKeyId.get(loadObj).get(apiName) != null) {


                          fieldValue = mapObjFieldKeyId.get(loadObj).get(apiName).get(column.trim());
                        }
                        //--- Added by Ashish | JIRA:ESMPROD-140 | 2-11-2015 | Ends---// 

                        //for insert proper value
                        if (fieldValue != null && fieldValue.trim().length() > 0) {

                          System.debug('fieldName,fieldvalue  : ' + fieldName + '  ' + fieldvalue);
                          childSobject.put(fieldName, fieldvalue);

                        } else if (column != null && column.trim().length() > 0) {

                          isError = true;
                          errorText = 'foreign key' + ' : ' + column.trim() + ' not found for ' + apiName;
                          content += generateErrorRecord(line) + errorText + '\n';
                          errorCount++;

                        }
                        system.debug('@@#$ Reference ' + apiName + ' : ' + column.trim());

                      } else if (dataType.equalsIgnoreCase('Date')) {

                        //if value is available then put
                        if (column != null && column.trim().length() > 0) {

                          if (Pattern.matches(regExpDate, column.trim())) {

                            String[] valueArr = column.trim().split('/');
                            String[] valueArr1 = column.trim().split('-');
                            //system.debug('@@#$ Date ' + apiName + ' : ' + column.trim());
                            Date myDate;
                            if (dateFormat.equalsIgnoreCase('dd/mm/yyyy')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[2]), Integer.valueOf(valueArr[1]), Integer.valueOf(valueArr[0]));
                              //regExpDate = '\\d{2}/\\d{2}/\\d{4}';
                            } else if (dateFormat.equalsIgnoreCase('mm/dd/yyyy')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[2]), Integer.valueOf(valueArr[0]), Integer.valueOf(valueArr[1]));
                              //regExpDate = '\\d{2}/\\d{2}/\\d{4}';
                            } else if (dateFormat.equalsIgnoreCase('dd/yyyy/mm')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[1]), Integer.valueOf(valueArr[2]), Integer.valueOf(valueArr[0]));
                              //regExpDate = '\\d{2}/\\d{4}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('mm/yyyy/dd')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[1]), Integer.valueOf(valueArr[0]), Integer.valueOf(valueArr[2]));
                              //regExpDate = '\\d{2}/\\d{4}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('yyyy/mm/dd')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[0]), Integer.valueOf(valueArr[1]), Integer.valueOf(valueArr[2]));
                              //regExpDate = '\\d{4}/\\d{2}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('yyyy/dd/mm')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr[0]), Integer.valueOf(valueArr[2]), Integer.valueOf(valueArr[1]));
                              //regExpDate = '\\d{4}/\\d{2}/\\d{2}';
                            }
                            else if (dateFormat.equalsIgnoreCase('dd-mm-yyyy')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[2]), Integer.valueOf(valueArr1[1]), Integer.valueOf(valueArr1[0]));
                              //regExpDate = '\\d{2}/\\d{2}/\\d{4}';
                            } else if (dateFormat.equalsIgnoreCase('mm-dd-yyyy')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[2]), Integer.valueOf(valueArr1[0]), Integer.valueOf(valueArr1[1]));
                              //regExpDate = '\\d{2}/\\d{2}/\\d{4}';
                            } else if (dateFormat.equalsIgnoreCase('dd-yyyy-mm')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[1]), Integer.valueOf(valueArr1[2]), Integer.valueOf(valueArr1[0]));
                              //regExpDate = '\\d{2}/\\d{4}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('mm-yyyy-dd')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[1]), Integer.valueOf(valueArr1[0]), Integer.valueOf(valueArr1[2]));
                              //regExpDate = '\\d{2}/\\d{4}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('yyyy-mm-dd')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[0]), Integer.valueOf(valueArr1[1]), Integer.valueOf(valueArr1[2]));
                              //regExpDate = '\\d{4}/\\d{2}/\\d{2}';
                            } else if (dateFormat.equalsIgnoreCase('yyyy-dd-mm')) {
                              myDate = Date.newInstance(Integer.valueOf(valueArr1[0]), Integer.valueOf(valueArr1[2]), Integer.valueOf(valueArr1[1]));
                              //regExpDate = '\\d{4}/\\d{2}/\\d{2}';
                            }
                            childSobject.put(apiName, myDate);

                          } else {

                            isError = true;
                            errorText = 'Incorrect Date Format' + ' [' + apiName + '] : ' + column.trim();
                            content += generateErrorRecord(line) + errorText + '\n';
                            errorCount++;

                          }

                        }

                      } else if (dataType.equalsIgnoreCase('Pick List')) {

                        if (column != null && column.trim().length() > 0) {

                          childSobject.put(apiName, column.trim());

                        }

                      } else {

                        if (column != null && column.trim().length() > 0) {

                          childSobject.put(apiName, column.trim());

                        }

                      }
                    }
                  }

                } else {

                  //error is occured
                  system.debug('@@#$ Reference ' + apiName + ' : ' + column.trim());
                  break;
                }
                hIndex++;
              }
            }

            system.debug('21-' + isError);
            system.debug('===isRecordSkipped===' + isRecordSkipped);
            if (isRecordSkipped) {
              break;
            }
            system.debug('===isUniqueKeyRequired===' + isUniqueKeyRequired);
            system.debug('=uniqueKeyRec=' + uniqueKeyRec);
            system.debug('=oldInvoiceKey==' + oldInvoiceKey);
            system.debug('===uniqueKeyRec != oldInvoiceKey===' + (uniqueKeyRec != oldInvoiceKey));
            system.debug('===isErrorInChildObj===' + isErrorInChildObj);

            if ((uniqueKeyRec != oldInvoiceKey || !isUniqueKeyRequired) && !isErrorInChildObj) {
              if (!isError) {
                //every thing is perfect so insert or update invoice
                try {

                system.debug('no error');
                  //kapil : 16.12.2015 | OpenWealth | -if isUniqueKeyRequired = true then and then only update, otherwise insert
                  if (isUniqueKeyRequired && tmpSobject!=null && tmpSobject.size() > 0) {

                    system.debug('@@#$ update');

                    //START: Kapil Adiyecha 2015.12.28 | OpenWealth | added filename and batch no    
                    if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'CSV_Batch_No__c').getDescribe().isUpdateable()){                                       
                      childSobject.put(childNamespace + 'CSV_Batch_No__c', batchNo + '-' + lineCounter);
                    }
                    if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'CSV_File_Name__c').getDescribe().isUpdateable()){
                      childSobject.put(childNamespace + 'CSV_File_Name__c', originalFileName);
                    }
                    //END: Kapil Adiyecha 2015.12.28 | OpenWealth | added filename and batch no

                    System.debug('===childSobject===' + childSobject);
                    if (Schema.getGlobalDescribe().get(loadObj).getDescribe().isUpdateable()) { //START: Disha Modi 2017.04.11 | Security Scan 
                      update childSobject;

                      system.debug('@@#$ update childSobject.Id '+ childSobject.Id);
                    }


                  } else {
                    if (loadObj.equalsIgnoreCase(childObjectName)) {
                      system.debug('@@#$ insert '+ childSobject);

                      system.debug('initCurrentState '+initCurrentState);
                      if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'Current_State__c').getDescribe().isUpdateable()){
                        childSobject.put(childNamespace + 'Current_State__c', initCurrentState);
                      }
                       system.debug('initUserAction '+initUserAction);
                      if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'User_Action__c').getDescribe().isUpdateable()){
                        childSobject.put(childNamespace + 'User_Action__c', initUserAction);
                      }

                       system.debug('initUserAction '+initUserAction);
                      //START: Kapil Adiyecha 2015.12.28 | OpenWealth | added filename and batch no    
                      if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'CSV_Batch_No__c').getDescribe().isUpdateable()){
                        childSobject.put(childNamespace + 'CSV_Batch_No__c', batchNo + '-' + lineCounter);
                      }
                      if(Schema.getGlobalDescribe().get(childObjectName).getDescribe().fields.getMap().get(childNamespace+'CSV_File_Name__c').getDescribe().isUpdateable()){
                        childSobject.put(childNamespace + 'CSV_File_Name__c', originalFileName);
                      }

                       system.debug('childSobject '+childSobject);
                      //END: Kapil Adiyecha 2015.12.28 | OpenWealth | added filename and batch no

                      /*A    invoiceObj.Input_Source__c='Portal - Spreasheet Upload'; A*/
                      //A  childSobject.put(childNamespace+'Source__c','CSV Upload');

            
                    }
                    if (Schema.getGlobalDescribe().get(loadObj).getDescribe().isCreateable()) { //START: Disha Modi 2017.04.11 | Security Scan 
                     insert childSobject;
                      //A  isNew.put(invoiceObj.Name,invoiceObj.Name);
                      System.debug('===childSobject===' + childSobject.Id);
                    }

                  }
                  if (loadObj.equalsIgnoreCase(childObjectName) && childSobject != null) {
                    childObjId = String.ValueOf(childSobject.get('Id'));

                    System.debug('==childObjId ==' + childObjId);
                   // isNew.put(String.ValueOf(childSobject.get('Name')), String.ValueOf(childSobject.get('Name')));
                    invStatusMap.put(childObjId, 'Success');
                  }

                  
                } catch(Exception e) {
                  System.debug('####=====e====' + e);
                  isError = true;
                  errorCount++;
                  errorText = e.getMessage();
                  content += generateErrorRecord(line) + errorText + '\n';
                }
              }


              if (isError) {

                system.debug('error');            

                  system.debug(' errorText '+errorText);      
                //enter error into error object

       //vish  
       
                           

       //vish


         } else if (!loadObj.equalsIgnoreCase(childObjectName)) {
                if (childObjId != null) {
                  if (!invStatusMap.containsKey(childObjId)) {
                    invStatusMap.put(childObjId, 'Success');
                  }
                }
              }
            }

          } catch(Exception e) {

            //for print exception 
            system.debug('exception into insert invoice : ' + e);
            /*A   ErrorMailUtil.sendTextEmail(e); */

          }
          oldInvoiceKey = invoiceKey;
          //Kapil Adiyecha 2015.12.28 | OpenWealth | added filename and batch no
          lineCounter++;
        } //loop for CSV record (for loop)
        if (isErrorInChildObj) {
          isErrorInPrevRec = true;
          break;
        } else {
          isErrorInPrevRec = false;
        }
      } //loop for all objects  (for loop)
    } // lineCounter != 0 if loop over


    system.debug('----- lineCounter -----' + lineCounter);
    //for store line item or charges

  } // execute method


  global void finish(Database.BatchableContext batchableContext) {
  
    System.debug('----------hisId------'+hisId);

    if (errorCount != 0) {

    CSV_Load_History__c history = new CSV_Load_History__c();
    history.Error_Status__c = true;
    history.Id = hisId;
    update history;

     System.debug('----------hisId------'+history.Id);
      Attachment err = new Attachment();
      if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('Name').getDescribe().isCreateable()){
        err.Name = 'Error' + batchNo + '.csv';
      }
      if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('Description').getDescribe().isCreateable()){
        err.Description = 'Error File';
      }
      if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('Body').getDescribe().isCreateable()){
        err.Body = Blob.valueOf(content);
      }
      if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('parentId').getDescribe().isCreateable()){
        err.parentId = hisId;
      }
      try {
        if (Attachment.getSObjectType().getDescribe().isCreateable()) { //START: Disha Modi 2017.01.24 | Security Scan 
          insert err;
        }
         System.debug('----------err--------'+err.Id);
        System.debug('---------err--------'+err.Name);

      } catch(Exception e) {
        System.debug('Exception to insert error attchment on history object : ' + e.getMessage());
      }
    }

   
  }

  private String generateErrorRecord(List<String> record) {
    String row = '';
    for (String column : record) {
      if (column == null || column.equalsIgnoreCase('null')) {
        row += ',';
      } else {
        if (column.contains('null')) {
          row += column.replace('null', '') + ',';
        } else {
          row += column + ',';
        }
      }
    }
    return row;
  }

}