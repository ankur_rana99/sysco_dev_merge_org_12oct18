public with sharing class ChangeOwnerController {
    @AuraEnabled
    public static List<User> userDetail(String owner,List<String> caseId){
        List<User> response = new  List<User>();
        try{
            List<User> userList  = new List<User>();
                userList = [Select Id,Name,SmallPhotoUrl from User where isActive = true and profile.Name !=null AND UserType != 'Guest' order by Name];
            
            List<CaseManager__c> caseList = new List<CaseManager__c>();
            if(owner!='' || owner!= null){
                
                for(CaseManager__c ca:[Select Id , Owner.Name,Owner.Id,Assigned_Date__c from CaseManager__c where Id in:caseId ]){
                    ca.OwnerId = owner;
					ca.Assigned_Date__c=Datetime.now();
                    caseList.add(ca);
                }
                if(caseList.size()>0){
                	update caseList;                
            	}
            }
            response =  userList;            
        }
        
        catch(Exception e){
            ExceptionHandlerUtility.writeException('ChangeOwnerController', e.getMessage(), e.getStackTraceString());
            response=null;
        }
        return response;
    }
    
    @AuraEnabled
    public static List<Group> queueData(String owner,List<String> caseId){
        try{
            String currentUserId = UserInfo.getUserId();
            List<Group> queueList = new List<Group>();
            List<Group> listOfUserQueue  = new List<Group>();
            List<QueueSobject> caseManagerQueue = [SELECT QueueId,SobjectType FROM QueueSobject Where SobjectType = 'CaseManager__c'];
            Set<String> queueIdSet = new Set<String>();
            for(QueueSobject queueSobj : caseManagerQueue){
            	queueIdSet.add(queueSobj.QueueId);
            }
            queueList = [Select Id,Name from Group where type = 'Queue' AND Id IN:queueIdSet order by Name];
           
            List<CaseManager__c> caseList = new List<CaseManager__c>();
            if(owner!='' || owner!= null){
                
                for(CaseManager__c ca:[Select Id ,Assigned_Date__c, Owner.Name,Owner.Id from CaseManager__c where Id in:caseId ]){
                    ca.Assigned_Date__c=Datetime.now();
                    ca.OwnerId = owner;                    
                    caseList.add(ca);
                }
	            if(caseList.size()>0){
	                update caseList;
	            }
            }
            return queueList;
        }        
        catch(Exception e){
            //System.debug('error ' +e);
            throw new AuraHandledException( ' Yor Error Message ==>'+e);
            
        }
        
        
    }
}