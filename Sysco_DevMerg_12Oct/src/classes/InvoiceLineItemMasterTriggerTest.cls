@isTest
public class InvoiceLineItemMasterTriggerTest {
    public static testMethod void testFirst() {

        Account acc = new Account(name = 'Test Account');
        insert acc;
        

        
      /*  Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;
  */
  
      /*  Invoice_Configuration__c invconfig = new Invoice_Configuration__c();
        invconfig.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
        invconfig.Auto_Match_Flow_Method__c = 'Sequence';
        invconfig.Auto_Validation_Current_States__c = 'Ready For Validation';
        invconfig.Enable_Auto_Flipping_With_GRN__c = true;
        invconfig.Enable_Quantity_Calculation__c = true;
        invconfig.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
        invconfig.GRN_Flag_In_PO_Header__c = true;
        invconfig.Invoice_Reject_Current_State__c = 'Ready For Processing';
//      invconfig.Pay_Me_Early_State__c = 'Ready For Processing';
        invconfig.PO_Number_Field_for_Search__c = 'Name';
        invconfig.Quantity_Calculation_Flag_In_PO_Header__c = true;
        //invconfig.Route_User_Type__c='Processor,Administrator';
        insert invconfig;*/

        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;
        
        Invoice_Configuration__c invconfig = new Invoice_Configuration__c();
    invconfig.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
    invconfig.Auto_Match_Flow_Method__c = 'Sequence';
    invconfig.Auto_Validation_Current_States__c = 'Ready For Validation';
    invconfig.Enable_Auto_Flipping_With_GRN__c = true;
    invconfig.Enable_Quantity_Calculation__c = true;
    invconfig.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
    invconfig.GRN_Flag_In_PO_Header__c = true;
    invconfig.Invoice_Reject_Current_State__c = 'Ready For Processing';
//  invconfig.Pay_Me_Early_State__c = 'Ready For Processing';
    invconfig.PO_Number_Field_for_Search__c = 'Name';
    invconfig.Quantity_Calculation_Flag_In_PO_Header__c = true;
    //invconfig.Route_User_Type__c='Processor,Administrator';
    insert invconfig;


        Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
        trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
        trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
        trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
        trgconfig.Enable_User_Usermaster_Sync__c = true;
//      trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
        insert trgconfig;

       
        List<Invoice__c> invl = new List<Invoice__c> ();
        invl.addAll(ESMDataGenerator.insertPOInvoice(5));
        invl.addAll(ESMDataGenerator.insertNONPOInvoice(5));

        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', invl.size());
        List<PO_Line_Item__c> poLineList = ESMDataGenerator.createPOline(poList);
        List<Invoice_PO_Detail__c> invPOList = ESMDataGenerator.createIvPODetail(invl, poList);

        List<GRN__c> grnlist = ESMDataGenerator.createGRN(poList);
        List<GRN_Line_Item__c> grnlinelist = ESMDataGenerator.createGRNLine(poList, poLineList);

        Approval_Detail__c apdtl = new Approval_Detail__c();
        apdtl.Status__c = 'Pending';
        insert apdtl;

        List<Invoice_Line_Item__c> invlineList = ESMDataGenerator.createInvoiceLine(invl, poList, poLineList, grnlist, grnlinelist);
        Integer cnt=0;
        for (Invoice_Line_Item__c invll:invlineList)
        {
            if (cnt != 2)
            {
                invll.Quantity_Calculation_Required__c = true;
            }else{
                invll.Quantity_Calculation_Required__c = false;
            }
            cnt++;
        }
        System.debug('invlineList45--'+invlineList);
        insert invlineList;
        delete invlineList;
        invconfig.Invoice_Reject_Current_State__c = '';
        invconfig.Enable_Quantity_Calculation__c = false;
        update invconfig;

        invlineList = ESMDataGenerator.createInvoiceLine(invl, poList, poLineList, grnlist, grnlinelist);
        insert invlineList;
        for (Invoice_Line_Item__c invll:invlineList)
        {
            if (cnt != 2)
            {
                invll.Quantity_Calculation_Required__c = true;
            }else{
                invll.Quantity_Calculation_Required__c = false;
            }
            cnt++;
        }
        update invlineList;
    }

}