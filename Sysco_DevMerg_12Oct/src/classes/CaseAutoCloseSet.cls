global class CaseAutoCloseSet implements Schedulable{
    global void execute(SchedulableContext SC)
    {
        //public static void SetCaseDetails() 
        list<CaseManager__c> cmlist=new list<CaseManager__c>();
        //for(CaseManager__c cmg: [SELECT Id, Name, User_Action__c, Current_State__c,Auto_Closed__c,Ageing_Since_Last_Response_in_Days__c FROM CaseManager__c where Current_State__c <>'Pending For Archival' order by CreatedDate desc])
        for(CaseManager__c cmg : [SELECT Id, Name, User_Action__c, Current_State__c,Auto_Closed__c,Ageing_Since_Last_Response_in_Days__c FROM CaseManager__c where Current_State__c <>'Pending For Archival' AND Current_State__c <> 'Pending on Global Team' AND Ageing_Since_Last_Response_in_Days__c >=3 order by CreatedDate desc])
        {
            //if(cmg.Ageing_Since_Last_Response_in_Days__c >= 3 && cmg.Current_State__c <> 'Pending on Global Team')
            //{
                cmg.User_Action__c='Closed';
                cmg.Pending_Reason__c = 'Rejected';
                cmg.Auto_Closed__c=true;
                cmlist.add(cmg);
            //}
        }
        try{
            update cmlist;
        }catch(Exception e){
            System.debug('msg :'+e.getMessage());
           System.debug('trace :'+e.getStackTraceString());
        }
    }
}