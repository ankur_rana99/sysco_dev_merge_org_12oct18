@isTest
public class VendorInquiryviewControllerTest {
	public static testMethod void testFirst() {
		CaseManager__c CaseObj = new CaseManager__c();
		CaseObj.Priority__c = 'High';
		CaseObj.Comments__c = 'hello';
		CaseObj.Status__c = 'pending';
		CaseObj.Amount__c = 100;
		CaseObj.Subject__c = 'omm';
		System.debug('CaseObj--' + CaseObj);
		insert CaseObj;
        
        EmailMessage interaction = new EmailMessage();
		interaction.Subject = 'Subject for Email';
		interaction.TextBody = 'hello';
		interaction.HtmlBody = 'world';
		interaction.FromName = 'abc@def.com';
		System.debug('interaction--' + interaction);
		insert interaction;
        
        ContentDocument doc = new ContentDocument();
		System.debug('doc--' + doc);
		List<String> doclst = new List<String>();
        doclst.add(doc.Id);
        
		VendorInquiryviewController.getinitialData(CaseObj.Id);
		VendorInquiryviewController.getattachementData(CaseObj.Id,interaction.Id);
		VendorInquiryviewController.getUserid();
		VendorInquiryviewController.getPicklistvalues();
		VendorInquiryviewController.saveCaseData(CaseObj,'abcd',doclst);
		VendorInquiryviewController.getCase(CaseObj.Id);
		VendorInquiryviewController.newUploadedAttachments(doclst);
        VendorInquiryviewController.deleteAttachments(doclst);
       //VendorInquiryviewController.getCustomSettingsDetails();



	}

}