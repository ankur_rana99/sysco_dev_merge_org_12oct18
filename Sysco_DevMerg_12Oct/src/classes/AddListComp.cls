public class AddListComp {
    
	@auraEnabled
    public static List<Invoice_Line_Item__c> getInvoiceLineItems(){
        system.debug('apex controller');
        return [SELECT Invoice_Line_No__c FROM Invoice_Line_Item__c Limit 10];
    }
    @auraEnabled
    public static Invoice_Line_Item__c getInvoiceLine(){
        system.debug('apex controller');
        return [SELECT Invoice_Line_No__c FROM Invoice_Line_Item__c Limit 1];
    }
    /*@AuraEnabled
	public static string SaveInvoiceData(List<Invoice_Line_Item__c> invLineItemList) {
		try {
			System.debug('SaveRecordData:'+invLineItemList);
			//InvoiceController.saveInvoice(invoiceObject, invLineItemList, poList, otherChargesList, pageMode);
			//update invoiceObject;
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return null;
	}*/
    @AuraEnabled
	public static string SaveInvoiceData(string invLineItemList) {
		try {
			System.debug('SaveRecordData:'+invLineItemList);
			//InvoiceController.saveInvoice(invoiceObject, invLineItemList, poList, otherChargesList, pageMode);
			//update invoiceObject;
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return invLineItemList;
	}
    @AuraEnabled
	public static List<Invoice_Line_Item__c> addData(List<Invoice_Line_Item__c> invLineItemList) {
		try {
			System.debug('SaveRecordData:'+invLineItemList);
            Invoice_Line_Item__c invl = new Invoice_Line_Item__c();
			invl.Invoice_Line_No__c = 'aaaa';   
            invLineItemList.add(invl);
			//InvoiceController.saveInvoice(invoiceObject, invLineItemList, poList, otherChargesList, pageMode);
			//update invoiceObject;
		} catch (Exception ex) {
			return null;
		}
		return invLineItemList;
	}
}