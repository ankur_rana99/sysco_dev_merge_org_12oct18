public class TATCalculation {
    @InvocableMethod(label = 'Statewise TAT Calculation' description = 'Statewise TAT Calculation')
    public static void calculateStateChange(List<Invoice__c> Invoices) {
        calculateStateChange(Invoices, 'Invoice__c');
    }
    public static void calculateStateChange(List<sObject> caseTrackeList, String objectName) {
        try {
            List<sObject> updtcaseTrackeList = new List<sObject>();
            MAP<ID, Tat_Rule_Config__c> tatRuleConfiges = new MAP<ID, Tat_Rule_Config__c> ([select id, Name, JSON__c, Order__c from Tat_Rule_Config__c where Object_Name__c = :objectName and IsActive__c = true limit 999]);
            String caseId = caseTrackeList.get(0).Id;
            //List<MileStone__c> caseTrasitionList = [select id, Current_State__c, Start_Date__c, End_Date__c, Invoice__c, Vendor_Maintenance__c from MileStone__c where (Invoice__c = :caseId or Vendor_Maintenance__c = :caseId) and End_Date__c = null];
            String query = 'select id, TargetTATTime__c, Current_State__c, Start_Date__c, End_Date__c, ';
            query += objectName;
            query += ' from MileStone__c where ';
            query += objectName;
            query += ' = :caseId and End_Date__c = null';
            System.debug('query ---------------'+query);
            List<MileStone__c> caseTrasitionList = Database.query(query);

            Map<id, List<MileStone__c>> caseManagerTransitionMap = new Map<id, List<MileStone__c>> ();

            for (MileStone__c caseTransition : caseTrasitionList) {
                List<MileStone__c> tempList = caseManagerTransitionMap.get(String.valueOf(caseTransition.get(objectName)));
                if (tempList == null) {
                    tempList = new List<MileStone__c> ();
                }
                tempList.add(caseTransition);
                caseManagerTransitionMap.put(Id.valueOf(String.valueOf(caseTransition.get(objectName))), tempList);
            }
            List<MileStone__c> caseTransitionInsertList = new List<MileStone__c> ();
            List<MileStone__c> caseTransitionUpdateList = new List<MileStone__c> ();
            List<Tat_Rule_Config__c> tatRuleConfigesList = [select id, Name, JSON__c, Order__c from Tat_Rule_Config__c where Object_Name__c = :objectName and IsActive__c = true order by Order__c];
            for (sObject caseTrackerNew : caseTrackeList) {
                sObject invObjUpdt = Schema.getGlobalDescribe().get(objectName).newSObject();
                String TATRuleId = '';
                System.debug('Current_State__c ---------------'+string.valueOf(caseTrackerNew.get('Current_State__c')));
                System.debug('Next_State__c ---------------'+string.valueOf(caseTrackerNew.get('Next_State__c')));
                if (string.valueOf(caseTrackerNew.get('Current_State__c')) != null && string.valueOf(caseTrackerNew.get('Next_State__c')) != null && string.valueOf(caseTrackerNew.get('Current_State__c')) != string.valueOf(caseTrackerNew.get('Next_State__c'))) {
                    MileStone__c currentTransition = null;

                    List<MileStone__c> transList = caseManagerTransitionMap.get(caseTrackerNew.id);
                    if (transList != null) {
                        for (MileStone__c trans : transList) {
                            if (trans.Current_State__c == caseTrackerNew.get('Current_State__c')) {
                                currentTransition = trans;
                                break;
                            }
                        }
                    }

                    string businessHrsId = '';
                    
                    
                    TATHelper.TATRule trule = findRule(caseTrackerNew, tatRuleConfigesList, true);
                    if (trule != null) {
						System.debug('TAT Rule -----'+trule);
                        Datetime targetTATDateTime = null;
                        DateTime TATDateTime = null;
			                
                        invObjUpdt.put('id', caseTrackerNew.id); // uncommented by Nikunj,Kuldeeep
                        invObjUpdt.put('Tat_Rule__c', trule.id);
                        if (!string.isEmpty(trule.JSONValue.calendar)) {
							System.debug('trule.JSONValue.calendar -----'+trule.JSONValue.calendar);
							System.debug('tatTimeUnit -----'+trule.JSONValue.tatTimeUnit);
							System.debug('tatTime -----'+trule.JSONValue.tatTime);
                            //invObjUpdt.put('Target_TAT_Time__c', (BusinessHours.addGmt(trule.JSONValue.calendar, datetime.now(), getMiliSecondByUnit(trule.JSONValue.tatTimeUnit, trule.JSONValue.tatTime))));
                            String calid = trule.JSONValue.calendar;
                            DateTime pstartdate = Datetime.now().addHours(-6);
                            integer sthrs=Integer.valueOf(Datetime.now().format('HH','America/El_Salvador'));
                			integer stmin=Integer.valueOf(Datetime.now().format('mm','America/El_Salvador'));
                			system.debug('calid :'+ calid +'--pstartdate :'+ pstartdate+'--sthrs : '+sthrs + '--stmin : '+stmin);
                			String dayOfWeek = pstartdate.format('E','America/El_Salvador');
							
							integer tattime = 2;
			                Integer tathours = 18;
			                long twodaysmilisecond = tathours * 60 * 60 * 1000L;
			                System.debug('Day of Week : ' + dayOfWeek);
			                
			                if (dayOfWeek=='Sunday' || dayOfWeek=='Saturday' || dayOfWeek=='Sun' || dayOfWeek=='Sat')
			                {
			                    system.debug('Weekends');
			                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
			                    TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
			                    system.debug('TATDateTime---if---'+TATDateTime);
			                    invObjUpdt.put('Target_TAT_Time__c', TATDateTime);
			                }
			                // Within SHIFT WINDOW
			                else if((sthrs >= 8 && stmin >= 1) && sthrs <= 24){
			                    system.debug('Within Shift Window AND SAME DAY');
			                    tattime = 3;
			                    tathours = 27;
			                    twodaysmilisecond = tathours * 60 * 60 * 1000L;
			                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
			                    TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
			                    system.debug('TATDateTime---Else if1---'+TATDateTime);
			                    invObjUpdt.put('Target_TAT_Time__c', TATDateTime);
			                }
			                else if (sthrs<=8){
			                    system.debug('Before Shift Window 00 AM to 08 AM');
			                    targetTATDateTime=BusinessHours.add(calid,pstartdate,twodaysmilisecond);
			                    TATDateTime = DateTime.newInstance(targetTATDateTime.year(), targetTATDateTime.month(), targetTATDateTime.day(), 07, 59, 00);
			                    system.debug('TATDateTime---Else if2---'+TATDateTime);
			                    invObjUpdt.put('Target_TAT_Time__c', TATDateTime);
			                }
										
							                        }
                        MileStone__c currentTransitionNew = new MileStone__c();
                        currentTransitionNew.put(objectName, caseTrackerNew.Id);
                        currentTransitionNew.Current_State__c = String.valueOf(caseTrackerNew.get('Next_State__c'));
                        currentTransitionNew.Start_Date__c = System.now();
                        if (targetTATDateTime != null) {
                            currentTransitionNew.TargetTATTime__c = TATDateTime;
                        }
                        caseTransitionInsertList.add(currentTransitionNew);
                    }
                    TATHelper.TATRule truleUpt = findRule(caseTrackerNew, tatRuleConfigesList, false);
                    if (truleUpt != null) {
                        //update invObjUpdt;

						System.debug('TruleUpt --'+truleUpt);
						invObjUpdt.put('Tat_Rule__c', truleUpt.id);
						// New ADD by Nikunj,Kuldeeep
                        invObjUpdt.put('id', caseTrackerNew.id);
                        invObjUpdt.put('PassCount__c', caseTrackerNew.get('PassCount__c'));
                        invObjUpdt.put('FailCount__c', caseTrackerNew.get('FailCount__c'));
                        Tat_Rule_Config__c rule = tatRuleConfiges.get(truleUpt.id);
                        if (rule != null) {
                            TATHelper.JSONValue jsonVal = (TATHelper.JSONValue) System.JSON.deserialize(rule.JSON__c, TATHelper.JSONValue.class);
                            businessHrsId = jsonVal.calendar;
                        }
                        if (currentTransition != null) {
							System.debug('TruleUpt1 --'+truleUpt);
                            DateTime dt = System.now();
                            currentTransition.End_Date__c = dt;

                            if (!string.isEmpty(businessHrsId)) {
								System.debug('businessHrsId --'+businessHrsId);
								System.debug('currentTransition.Start_Date__c --'+currentTransition.Start_Date__c);
								System.debug('currentTransition.End_Date__c --'+currentTransition.End_Date__c);
                                currentTransition.Time_Taken_In_Mins__c = getDateDiffByBusinessHours(businessHrsId, currentTransition.Start_Date__c, currentTransition.End_Date__c);
								System.debug('currentTransition.Time_Taken_In_Mins__c --'+currentTransition.Time_Taken_In_Mins__c);
								System.debug('currentTransition.End_Date__c --' + currentTransition.End_Date__c);
								System.debug('currentTransition.TargetTATTime__c --' + currentTransition.TargetTATTime__c);
								
								if (currentTransition.End_Date__c <= currentTransition.TargetTATTime__c)
								{
									
									invObjUpdt.put('PassCount__c', Integer.valueOf(caseTrackerNew.get('PassCount__c'))+1);
								}
								else
								{
									
									invObjUpdt.put('FailCount__c', Integer.valueOf(caseTrackerNew.get('FailCount__c'))+1);
								}
                            }
                            //if (targetTATDateTime != null) {
                                //currentTransition.TargetTATTime__c = targetTATDateTime;
                            //}
							System.debug('IF over--');
                            caseTransitionUpdateList.add(currentTransition);
                        }
                    }
                }
				System.debug('Loop over --');
                updtcaseTrackeList.add(invObjUpdt);
            }

            if (caseTransitionInsertList.size() > 0) {
				System.debug('Insert milestone --');
                insert caseTransitionInsertList;
                update updtcaseTrackeList;
            }
            if (caseTransitionUpdateList.size() > 0) {
				System.debug('update milestone --');
                update caseTransitionUpdateList;
                setActualTatTime(updtcaseTrackeList, objectName, updtcaseTrackeList);
				System.debug('Set Actual TAT Time done --');
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATCalculation', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Calculate Date Diff based on bussiness hours.
      Dependencies: Called from "CaseTrackerTrigger"
     */
    @testVisible
    private static Decimal getDateDiffByBusinessHours(string businessHrsId, Datetime dt1, Datetime dt2) {
        long Diff1 = BusinessHours.diff(businessHrsId, dt1, dt2);
        Decimal dateDiff = Diff1 / 1000.0 / 60.0;
        return dateDiff;
    }

    private static long getMiliSecondByUnit(string unit, integer tatTime) {
        long returnVal;
        if (unit.endsWithIgnoreCase('Days')) {
            returnVal = tatTime * 9 * 60 * 60 * 1000L; // 9 hours business calendar setup
        }
        else if (unit.endsWithIgnoreCase('Hours')) {
            returnVal = tatTime * 60 * 60 * 1000L;
        }
        else {
            returnVal = tatTime * 60 * 1000L;
        }
        return returnVal;
    }

    public static void setActualTatTime(List<sObject> caseList, String objectName, List<sObject> invObjUpdt) {
        MAP<ID, Tat_Rule_Config__c> tatRuleConfiges = new MAP<ID, Tat_Rule_Config__c> ([select id, Name, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c where Object_Name__c = :objectName and IsActive__c = true limit 999]);
        Map<Id, User> userMap = new Map<Id, User> ([select id from user]);
        
        List<sObject> updateList = new List<sObject> ();
        List<user> updateUserList = new List<user> ();

		System.debug('caseList.size() --'+caseList.size());
		System.debug('caseList --'+caseList);
        //for (sObject caseTrackerObj : caseList) {
        for (Integer i = 0; i < caseList.size(); i++) {
            sObject caseTrackerObj = caseList.get(i);
            sObject caseTrackerObjUpdt = invObjUpdt.get(i);
            sObject caseObjNew = Schema.getGlobalDescribe().get(objectName).newSObject();
            //caseObjNew.id = caseTrackerObj.id; // old code
			
			// New Code Nikunj B and Kuldeep
			
			caseObjNew.id = caseTrackerObjUpdt.id; 
			caseObjNew.put('FailCount__c', caseTrackerObjUpdt.get('FailCount__c'));
			caseObjNew.put('PassCount__c', caseTrackerObjUpdt.get('PassCount__c'));
		
			string businessHrsId = '';
            if (caseTrackerObjUpdt.get('Tat_Rule__c') != '') {
                Tat_Rule_Config__c rule = tatRuleConfiges.get(String.valueOf(caseTrackerObjUpdt.get('Tat_Rule__c')));
                if (rule != null) {
                    TATHelper.JSONValue jsonVal = (TATHelper.JSONValue) System.JSON.deserialize(rule.JSON__c, TATHelper.JSONValue.class);
                    businessHrsId = jsonVal.calendar;
                }
            }
            Datetime actualTatTime = datetime.now();
            if (!string.isEmpty(businessHrsId)) {
                actualTatTime = BusinessHours.addGmt(businessHrsId, datetime.now(), 0);

            }
            caseObjNew.put('Actual_TAT_Time__c', actualTatTime);
			System.debug('actualTatTime --'+actualTatTime);
			if (caseTrackerObjUpdt.get('OwnerId')!=null)
			{
				if (String.valueOf(caseTrackerObjUpdt.get('OwnerId')).startsWith('005')) {
					System.debug('Target_TAT_Time__c'+caseTrackerObjUpdt.get('Target_TAT_Time__c'));
					if (Datetime.valueOf(caseTrackerObjUpdt.get('Target_TAT_Time__c')) >= actualTatTime) {
						User ur = userMap.get(Id.valueOf(String.valueOf(caseTrackerObjUpdt.get('OwnerId'))));
						updateUserList.add(ur);
					}
				}
			}
            updateList.add(caseObjNew); 
		}
		System.debug('Actual TAT Time Loop over --');
        update updateUserList;
        update updateList;
		System.debug('Actual TAT Time update over --');
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check both date are in same month.
      Dependencies: Called from "setUserScore"
    */
    public static boolean isSameMonth(Date firstDate, Date secondDate) {
        return(firstDate.month() == secondDate.month() && firstDate.Year() == secondDate.Year());
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check both date are in same week.
      Dependencies: Called from "setUserScore"
    */
    public static boolean isSameWeek(Date firstDate, Date secondDate) {
        Date weekStart = firstDate.toStartofWeek();
        return weekStart.daysBetween(secondDate) < 7;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Find matching TAT rule based on case fields.
      Dependencies: Called from "CalculateTAT" method.
     */
    private static TATHelper.TATRule findRule(sObject caseTrackerObject, List<Tat_Rule_Config__c> tatRuleConfiges, boolean isComingToState) {
        
        for (Tat_Rule_Config__c TATRuleConfig : tatRuleConfiges) {

            TATHelper.TATRule TATRule = new TATHelper.TATRule();
            TATRule.id = TATRuleConfig.id;
            TATRule.JSONValue = (TATHelper.JSONValue) System.JSON.deserialize(TATRuleConfig.JSON__c, TATHelper.JSONValue.class);
            if (TATRule.JSONValue != null && TATRule.JSONValue.criterias != null) {

                boolean isMatch = false;
                for (TATHelper.Criteria cre : TATRule.JSONValue.criterias) {
                    if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                        string fieldValue = string.valueOf(caseTrackerObject.get(cre.Field));
                        if (isComingToState && cre.Field.equals('Current_State__c'))
                        {
                            fieldValue = string.valueOf(caseTrackerObject.get('Next_State__c'));
                        }
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                        double fieldValue = double.valueOf(caseTrackerObject.get(cre.Field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                        boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.Field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    if (!isMatch) {
                        break;
                    }
                }

                if (isMatch) {
                    return TATRule;
                }
            }
        }
        return null;

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for string value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, string fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == car.Value;
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != car.Value;
        }
        else if (car.operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(car.Value)) {
                List<string> tempList = car.Value.split(',');
                for (string str : tempList) {
                    if (car.type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for double value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, double fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less than')) {
            return fieldValue < double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater than')) {
            return fieldValue > double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= double.valueOf(car.Value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, boolean fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(car.Value);
        }
        return false;
    }
}