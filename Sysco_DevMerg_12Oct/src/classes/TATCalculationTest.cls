@isTest
public class TATCalculationTest{

 public static testMethod void testFirst(){
 System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
        
        
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
           
        Invoice__c invobj = new Invoice__c();    
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);    
        invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c='Payment Feed Received';
        //invobj.Current_State__c = 'Pending Merchandise Processing';
        invobj.Next_State__c = 'Ready for Posting';
         System.debug('invobj' + invobj);
        insert invobj;
        CaseManager__c cm = new CaseManager__c();
        cm.Amount__c = 100;
          insert cm ;
        List<CaseManager__c> listcm = new List<CaseManager__c>();
        listcm.add([select id,name,Amount__c,OwnerId from CaseManager__c where id =:cm.id]);
        System.debug(listcm);
        MileStone__c ms = new MileStone__c();
        ms.Current_State__c = 'Start';
        ms.Start_Date__c = Date.Today();
        ms.End_Date__c =Date.Today();
        insert ms;
        System.debug('ms--' +ms);
        list<Invoice__c> listInv = new list<Invoice__c>();
        listInv.add(invobj);
     
        TATCalculation.calculateStateChange(listInv);
        TATCalculation.calculateStateChange(listInv,'Invoice__c');

        
        TATCalculation.setActualTatTime(listcm,'Invoice__c',listInv);
        //TATCalculation.getDateDiffByBusinessHours('1','7-8-2018','7-8-2018');
        Tat_Rule_Config__c trc = new Tat_Rule_Config__c();
        trc.JSON__c = '{"template":"","tatTimeUnit":"Minutes","tatTime":1,"sendNotification":false,"notificationTimeUnit":"Minutes","notificationTime":0,"criterias":[{"value":"Ready For QC","type":"PICKLIST","operator":"equals","objName":"Invoice__c","field":"Current_State__c"}],"calendar":"01m1N000000AkzVQAS","additionalEmails":""}';
        trc.IsActive__c = true;
        trc.Object_Name__c = 'Invoice__c';
        trc.Order__c = 7;
        trc.Rule_Name__c = 'Ready For QC';
        insert trc;
        system.debug('trc--' +trc);
        list<Tat_Rule_Config__c> listTat= new list<Tat_Rule_Config__c>();
        listTat.add(trc);
        //TATCalculation.findRule(trc,listTat,false);
        //
        TATHelper.Criteria wrapCriteria = new TATHelper.Criteria ();
        wrapCriteria.field = 'User_Action__c';
        wrapCriteria.operator = 'equals';
        wrapCriteria.value = 'Reject';
        //wrapCriteria.operator = 'not equal to';
        wrapCriteria.type = 'String';
        wrapCriteria.objName = 'Invoice__c';

     
        TATHelper.Criteria wrapCriteria1 = new TATHelper.Criteria ();
        wrapCriteria1.operator = 'not equal to';
        TATHelper.Criteria wrapCriteria2 = new TATHelper.Criteria ();
        wrapCriteria2.operator = 'starts with';
        //TATHelper.Criteria wrapCriteria3 = new TATHelper.Criteria ();
        //wrapCriteria3.operator = 'end with';
        //TATHelper.Criteria wrapCriteria4 = new TATHelper.Criteria ();
        //wrapCriteria4.operator = 'contains';
        //TATHelper.Criteria wrapCriteria5 = new TATHelper.Criteria ();
        //wrapCriteria5.operator = 'does not contain';
        //TATHelper.Criteria wrapCriteria6 = new TATHelper.Criteria ();
        //wrapCriteria6.operator = 'contains in list';
        //TATHelper.Criteria wrapCriteria7 = new TATHelper.Criteria ();
        //wrapCriteria7.operator = 'PICKLIST';
         //wrapCriteria.type = 'String';
        //wrapCriteria.value = 'Reject';
        //TATCalculation.isMatch(wrapCriteria,true);
        //TATCalculation.isMatch(wrapCriteria,false);
        //TATCalculation.isMatch(wrapCriteria,'equals');
        //
     //TATCalculation.isMatch(wrapCriteria,double.valueOf(2.4));
     TATCalculation.isMatch(wrapCriteria,'abc');
     TATCalculation.isMatch(wrapCriteria1,'xyz');
     //TATCalculation.isMatch(wrapCriteria2,'pqr');
     //TATCalculation.isMatch(wrapCriteria3,'mno');
    // TATCalculation.isMatch(wrapCriteria4,'wxy');
     //TATCalculation.isMatch(wrapCriteria5,'abc');
     //TATCalculation.isMatch(wrapCriteria6,'abc');
     
 }
    
}