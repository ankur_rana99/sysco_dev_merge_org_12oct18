@isTest
public class OnChangeManagerTest {
    static String namespace = UtilityController.esmNameSpace;
    public static testMethod void testFirst(){
  System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
        
        Invoice__c invobj = new Invoice__c();    
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);    
        invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c='Vouched';
        invobj.Invoice_Type__c = 'M';
            
        insert invobj;
        
        Invoice_Line_Item__c InvLine = new  Invoice_Line_Item__c();
        InvLine.Invoice_Line_No__c='INV';
            //InvLine.Invoice__c=inv.id;
        insert InvLine;
        system.debug('-----InvLine'+InvLine);
        List<Invoice_Line_Item__c> ListInvLine = new List<Invoice_Line_Item__c>();
        ListInvLine.add(InvLine);
        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', 1);
        GRN_Line_Item__c gl = new GRN_Line_Item__c();
        gl.Amount__c = 10;
     
        gl.Quantity__c = 10;
   
        List<GRN_Line_Item__c> ListGrnLine = new List<GRN_Line_Item__c>();
        ListGrnLine.add(gl);
        
        PO_Line_Item__c poL = new PO_Line_Item__c();
       	 poL.Purchase_Order__c=poList[0].id;
         poL.Rate__c = 123;
         insert poL; 
        system.debug('-----poL'+poL);
        List<PO_Line_Item__c> ListPoLine = new List<PO_Line_Item__c>();
        ListPoLine.add(poL);
        
        Map<Object, Object> validationMap = new Map<Object, Object>();
            validationMap.put('test','abc');
        
        Map<Object, Object> extraParam = new Map<Object, Object>();
            extraParam.put('lstPO','abc1');

          OnChangeManager ocm=new OnChangeManager();
          ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,namespace+'Vendor__c','t',namespace,extraParam);
          ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,namespace+'Purchase_Order__c','Invoice_Type__c',namespace,extraParam);
          ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,namespace+'Supplier_Profile__c','t',namespace,extraParam);
          ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,namespace+'Remit_To_Country_Territory__c','t',namespace,extraParam);
         ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,'','',namespace,extraParam);
              
 }
 public static testMethod void testSecond(){
     System_Configuration__c sysconfig = new System_Configuration__c();
  		sysconfig.Module__c = 'Custom_Record_History';
        sysconfig.Value__c = 'Invoice__c';
        insert sysconfig;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;

        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;

        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

        Trigger_Configuration__c tconfig = new Trigger_Configuration__c ();
        tconfig.Enable_HeroKu_User_Buyer_Sync__c = true;
        insert tconfig;
     
        Invoice__c invobj = new Invoice__c();    
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);    
        invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c='Vouched';
        invobj.Invoice_Type__c = 'M';
            
        insert invobj;
        
        Invoice_Line_Item__c InvLine = new  Invoice_Line_Item__c();
        InvLine.Invoice_Line_No__c='INV';
            //InvLine.Invoice__c=inv.id;
        insert InvLine;
        system.debug('-----InvLine'+InvLine);
        List<Invoice_Line_Item__c> ListInvLine = new List<Invoice_Line_Item__c>();
        ListInvLine.add(InvLine);
        
        GRN_Line_Item__c gl = new GRN_Line_Item__c();
        gl.Amount__c = 10;
     
        gl.Quantity__c = 10;
   
        List<GRN_Line_Item__c> ListGrnLine = new List<GRN_Line_Item__c>();
        ListGrnLine.add(gl);
        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', 1);
        PO_Line_Item__c poL = new PO_Line_Item__c();
       	poL.Purchase_Order__c=poList[0].id;
         poL.Rate__c = 123;
         insert poL; 
        system.debug('-----poL'+poL);
        List<PO_Line_Item__c> ListPoLine = new List<PO_Line_Item__c>();
        ListPoLine.add(poL);
        
        Map<Object, Object> validationMap = new Map<Object, Object>();
            validationMap.put('test','abc');
        
        Map<Object, Object> extraParam = new Map<Object, Object>();
            extraParam.put('lstPO','abc1');

          OnChangeManager ocm=new OnChangeManager();
         
          ocm.execute(invobj,ListInvLine,ListPoLine,ListGrnLine,namespace+'Remit_To_Country_Territory__c','t',namespace,extraParam);
         
              
 }

}