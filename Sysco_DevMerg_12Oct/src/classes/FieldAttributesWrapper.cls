public class FieldAttributesWrapper {

/* Wrapper class for Passing filed range 
	created by Shital Rathod
	JIRA - CASP2-11 
for provide range in filter critearia
*/
    @AuraEnabled public String fieldType; 
    @AuraEnabled public String fieldLabel;
    @AuraEnabled public String fieldValue1; // for range value
    @AuraEnabled public String fieldValue2; // for range value
	@AuraEnabled public String selectedOperator1;
    @AuraEnabled public String fieldAPI;
	@AuraEnabled public Map<String,String> picklistmap;
	
     public FieldAttributesWrapper(String fieldType,String fieldLabel,String fieldValue1,String fieldValue2,String selectedOperator1,String fieldAPI,Map<String,String> picklistmap){
            this.fieldType = fieldType;
            this.fieldLabel = fieldLabel;
            this.fieldValue1 = fieldValue1;            
            this.fieldValue2 = fieldValue2; 
			this.selectedOperator1 = selectedOperator1;                      
            this.fieldAPI = fieldAPI;
			this.picklistmap = picklistmap;                                    
        }
}