@isTest
public class PurchaseOrderControllerTest {
    public static testMethod void testFirst() {
    System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;
        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c = 'Pending Merchandise Processing';
        insert invobj;
        Account acc = new Account(name = 'User2545Name@test.com');
        insert acc;
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User u = new user();
        u.LastName = 'Test Code';
        u.Email = 'test@test.com';
        u.Alias = 'Tcode';
        u.Username = 'User2545Name@test.com';
        u.CommunityNickname = 'test12';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profileId.Id;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u;

        System_Configuration__c sysconfig = new System_Configuration__c();
        sysconfig.Module__c = 'Purchase_Order';
        sysconfig.Sub_Module__c = 'Share_PO_With_Vendor';
        sysconfig.Value__c = 'true';
        insert sysconfig;

        PurchaseOrderController.getPurchaseOrderServer('Invoice__c', '[{"attachment":"test"}]', invobj.Id, 'abc');
        PurchaseOrderController.getSelectedPurchaseOrderServer('text', 'Invoice__c');

        List<Invoice__c> invl = new List<Invoice__c> ();
        invl.addAll(ESMDataGenerator.insertPOInvoice(2));
        invl.addAll(ESMDataGenerator.insertNONPOInvoice(2));
        List<Purchase_Order__c> poList = ESMDataGenerator.createPO('', invl.size());
        for (Purchase_Order__c pos : poList)
        {
            pos.Vendor__c = acc.Id;
        }
        upsert poList;
        delete poList;
    }
    public static testMethod void testSecond() {
System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;

        Invoice__c invobj = new Invoice__c();
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);
        invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c = 'Pending Merchandise Processing';
        insert invobj;

        PurchaseOrderController.getPurchaseOrderServer('Invoice__c', '[{"attachment":"test"}]', invobj.Id, null);
    }
}