@isTest
public class FinanceDAPTriggerTest {
    public static testMethod void method1(){
        
        Finance_DAP__c fin = new Finance_DAP__c();
        fin.Approval_Limit__c = 20000;
        fin.DAP_Title__c = 'test tittle';
        fin.Entity__c = 'test entity';
        fin.Function_Area__c = 'functional';
        fin.Transactions_or_Areas__c = 'test area';
        insert fin;
    }
    
    public static testMethod void method2(){
        
        Functional_DAP__c fin = new Functional_DAP__c();
        fin.Approval_Limit__c = 20000;
        fin.DAP_Title__c = 'test tittle';
        fin.Entity__c = 'test entity';
        fin.Function_Area__c = 'functional';
        fin.Transactions_or_Areas__c = 'test area';
        insert fin;
    }

}