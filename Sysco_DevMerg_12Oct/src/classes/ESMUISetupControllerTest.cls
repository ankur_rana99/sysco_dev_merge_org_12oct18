@isTest
public class ESMUISetupControllerTest {
    static String namespace = UtilityController.esmNameSpace;
     public static testMethod void testFirst(){
         ESMUISetupController.Criteria crt=new ESMUISetupController.Criteria();
         crt.field='test';
         crt.operator='&&';    
         crt.value='test';
         crt.type='';

         ESMUISetupController.ActionField actfld=new ESMUISetupController.ActionField ();
         actfld.field ='test';
         actfld.action ='abc';
         
         ESMUISetupController.Rule ruleObj = new ESMUISetupController.Rule();
         ruleObj.field = 'Current_State__c';
         ruleObj.criterias = new List<ESMUISetupController.Criteria>();
         ruleObj.actions = new List<ESMUISetupController.ActionField>();

         ESMUISetupController.JSONValue jSONValueObj = new ESMUISetupController.JSONValue();
         jSONValueObj.rules = new List<ESMUISetupController.Rule>();
         jSONValueObj.defaultHiddenFields = new List<String>();

         ESMUISetupController.UIRule uiRuleObj=new ESMUISetupController.UIRule();
         uiRuleObj.JSONValue = jSONValueObj;
         uiRuleObj.layoutName = 'Edit';
         uiRuleObj.id = '';
         uiRuleObj.isActive = true;
         uiRuleObj.type = 'Processing Field';

         ESMUISetupController.Option opt=new ESMUISetupController.Option();
         opt.value ='test';
         opt.text='abc';
         ESMUISetupController.Option opt1=new ESMUISetupController.Option('test','demo');
         List<ESMUISetupController.Option> lstopt=new List<ESMUISetupController.Option>();
         
         ESMUISetupController.FieldInfo fldinfo=new ESMUISetupController.FieldInfo();
         fldinfo.label='test';
         fldinfo.apiName='abc';
         fldinfo.type='test';
         fldinfo.isHidden=false;
         fldinfo.isForCriteria=true;
         fldinfo.pickListValue=new List<ESMUISetupController.Option>();
         fldinfo.operator= new List<String>();
         
         ESMUISetupController.LayoutField layoutField=new ESMUISetupController.LayoutField();
         layoutField.fields=new List<ESMUISetupController.FieldInfo>();
         layoutField.sectionFields=new List<ESMUISetupController.SectionField>();

         ESMUISetupController.SectionField sectionFieldObj=new ESMUISetupController.SectionField();
         sectionFieldObj.sectionName='Invoice Header';
//         sectionFieldObj.fields=new List<ESMUISetupController.FieldInfo>();

         ESMUISetupController.Record rec=new ESMUISetupController.Record();
         rec.Id='test';
         rec.Name='demo';
         
         ESMUISetupController.Result res=new ESMUISetupController.Result();
         res.records=new List<ESMUISetupController.Record>();

         List<ESMUISetupController.Record> listrec=new List<ESMUISetupController.Record>();
         
         UI_Rule_config__c obj=new UI_Rule_config__c();
         obj.IsActive__c = true;
         obj.JSON__c = '{}';
         obj.Rule_Name__c = 'Case Manager Layout';
         obj.Type__c = 'Processing Field';
         insert obj;

         List<ESMUISetupController.UIRule> allUIRules = ESMUISetupController.getAllRules();
         for (ESMUISetupController.UIRule uiRuleObj1 : allUIRules) {
            uiRuleObj.id = uiRuleObj1.id;
         }
         ESMUISetupController.saveRule(uiRuleObj);
         ESMUISetupController.removeRule(obj.Id);
         ESMUISetupController.getLayoutFields(namespace + 'Case Manager Layout');
         
         ESMUISetupController.LayoutField layoutFieldsNew=ESMUISetupController.getFieldSetFields('DefaultFieldSet');
         
         try{
             ESMUISetupController.getSobjectId();
         }
         catch(Exception ex){}
         
         try{
             List<ESMUISetupController.Option> layout=ESMUISetupController.getLayouts();
         }
         catch(Exception ex){}
    }
}