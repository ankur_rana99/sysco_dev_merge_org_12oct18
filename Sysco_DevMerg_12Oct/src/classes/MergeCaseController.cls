public class MergeCaseController {
    @AuraEnabled
    public static List<CaseManager__c> MergeCase(List<CaseManager__c> mergeCases, CaseManager__c mainCase, String comments) {
        try
        {
            List<Comments__c> commentList = new List<Comments__c> ();
            List<Relationship__c> relationshipList = new List<Relationship__c> ();

            Set<Id> caseIds = new Set<Id> ();

            for (CaseManager__c mergeCase : mergeCases) {
                mergeCase.User_Action__c= 'Merge And Close';
              

                Comments__c commentObj = new Comments__c();
                commentObj.CaseManager__c = mergeCase.id;
                commentObj.Comment__c= comments;
                commentList.add(commentObj);


                Relationship__c mergeParent = new Relationship__c();
                mergeParent.CaseManager__c = mainCase.id;
                mergeParent.Sub_Case__c = mergeCase.id;
                mergeParent.Type__c = 'Merge';
                relationshipList.add(mergeParent);

                Relationship__c mergeChild = new Relationship__c();
                mergeChild.CaseManager__c = mergeCase.id;
                mergeChild.Sub_Case__c = mainCase.id;
                mergeChild.Type__c = 'Merge';
                relationshipList.add(mergeChild);

                caseIds.add(mergeCase.id);
            }


           //Added by Ashish for CAS-53
      List<EmailMessage> emailMessages = new List<EmailMessage>();
      List<EmailMessage> newEmailMessages = new List<EmailMessage>();
      List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
      List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
      List<oldNewEmailContentDocWrapper> oldNewEmailWrapperList = new List<oldNewEmailContentDocWrapper>();
      List<Id> emailMessagesIdList = new List<Id>();
      Set<Id> emailMessagesIdSet = new Set<Id>();
      
      try{
      emailMessages = [Select Id, FromAddress, ToAddress, BccAddress, CcAddress, HtmlBody, Subject, TextBody, FromName,HasAttachment,Headers, CreatedDate, RelatedToId, Incoming,HasAttachment__c,Is_Note__c,Is_Private__c from EmailMessage Where RelatedToId In :caseIds Order By CreatedDate ASC];  
      
      for(EmailMessage email : emailMessages){
        if (emailMessagesIdSet.add(email.id)) {
             emailMessagesIdList.add(email.id);
          }
      }
      if(emailMessagesIdList.size() >0){
        contentDocumentLinks = [SELECT ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, ContentDocument.Owner.Name,LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId IN:emailMessagesIdList order by SystemModstamp desc];  
      }
      
          
      for(EmailMessage email : emailMessages){
        EmailMessage newEmailMsg = email.clone(false,true,true,true);
        newEmailMsg.RelatedToId = mainCase.id;
                newEmailMsg.Subject = replaceCaseNumberSubject(email.Subject,mainCase.Name,ESMConstants.SUBJECT_START_IDENTIFIER, ESMConstants.SUBJECT_END_IDENTIFIER, ESMConstants.REGEX_FOR_CASE_SEARCH_IN_SUBJECT);
        oldNewEmailWrapperList.add(new oldNewEmailContentDocWrapper(email.Id,newEmailMsg,new List<ContentDocumentLink>()));
      }
                
      if(ContentDocumentLinks.size() > 0){
                  for(ContentDocumentLink cdLink : ContentDocumentLinks){
                        for(oldNewEmailContentDocWrapper wrp : oldNewEmailWrapperList){
                if(wrp.oldEmailMessageId == cdLink.LinkedEntityId){
                  ContentDocumentLink cntent = cdLink.clone(false,true,true,true);
                            cntent.ShareType = 'V';
                  wrp.contentDocLinkList.add(cntent);
                }
              }
            }
                  }
                  
      for(oldNewEmailContentDocWrapper wrp : oldNewEmailWrapperList){
          newEmailMessages.add(wrp.newEmailMessage);
        }
      
        if(newEmailMessages.size()>0){
                    insert newEmailMessages;
                }
                
                for(oldNewEmailContentDocWrapper wrp : oldNewEmailWrapperList){
                  if(wrp.contentDocLinkList.size() >0){
                    for(ContentDocumentLink cdnLink : wrp.contentDocLinkList){
                      cdnLink.LinkedEntityId = wrp.newEmailMessage.Id;
                      newContentDocumentLinks.add(cdnLink );
                    }
                    
                  }
        }
        
        if(newContentDocumentLinks.size() > 0){
          insert newContentDocumentLinks;
        }
                  
      }catch(Exception ex){
        throw new AuraHandledException(ex.getMessage());
      }
      
      
      
            //CAS - 53 Ends
            
            Comments__c mergeComment = new Comments__c();
            mergeComment.CaseManager__c = mainCase.id;
            mergeComment.Comment__c= comments;
            commentList.add(mergeComment);

            if(mergeCases.size()>0){
              update mergeCases;    
            }
            

            //System.debug('caseIds=>' + caseIds);

            Set<String> ContentDocumentIds = new Set<String> ();
            for (ContentDocumentLink cotentDocumentLink :[Select Id, ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId in :caseIds AND ShareType = 'V']) {
                ContentDocumentIds.add(cotentDocumentLink.ContentDocumentId);
            }

            List<ContentVersion> contentversionList = new List<ContentVersion> ();
            for (ContentVersion contentversionObj :[SELECT ContentDocumentId, Title, VersionData, PathOnClient FROM ContentVersion WHERE ContentDocumentId in :ContentDocumentIds])
            {
                ContentVersion newContVersion = new ContentVersion();
                newContVersion.Title = contentversionObj.Title;
                newContVersion.PathOnClient = contentversionObj.PathOnClient;
                newContVersion.VersionData = contentversionObj.VersionData;
                contentversionList.add(newContVersion);
            }
            
            if(contentversionList.size()>0){
              insert contentversionList;    
            }
            

            List<ContentVersion> conetntVersionListWithId = [SELECT id, ContentDocumentId, Title FROM ContentVersion where Id = :contentversionList];
            List<ContentDocumentLink> contentDocLinkList = new List<ContentDocumentLink> ();

            for (ContentVersion contentVersionObj : conetntVersionListWithId) {
                ContentDocumentLink contentDocumentLinkObj = new ContentDocumentLink();
                contentDocumentLinkObj.ContentDocumentId = contentVersionObj.ContentDocumentId;
                contentDocumentLinkObj.LinkedEntityId = mainCase.Id;
                contentDocumentLinkObj.ShareType = 'V';
                contentDocLinkList.add(contentDocumentLinkObj);
            }
            
            if(contentDocLinkList.size()>0){
              insert contentDocLinkList;    
            }
            
            if(commentList.size()>0){
              insert commentList;    
            }
            
            if(relationshipList.size()>0){
               insert relationshipList;   
            }
            
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return mergeCases;
    }
    
    // This is our wrapper/container class. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
    public class oldNewEmailContentDocWrapper {
        public Id oldEmailMessageId {get; set;}
    public EmailMessage newEmailMessage {get; set;} 
    public List<ContentDocumentLink> contentDocLinkList {get; set;}
        public oldNewEmailContentDocWrapper(Id oldEmailId, EmailMessage newEmail, List<ContentDocumentLink> cntentDocLinkList) {
          oldEmailMessageId = oldEmailId;
          newEmailMessage = newEmail;
          contentDocLinkList = cntentDocLinkList;
        }
    }
       @testvisible
    private static String replaceCaseNumberSubject(String sourceString,String replaceByString, String startIdentifier, String endIdentifier, String regex){
      Pattern patternObj = Pattern.compile(regex);
    Matcher matcherObj = patternObj.matcher(sourceString);
    while (matcherObj.find()) {
          String caseStr = matcherObj.group();
          sourceString = sourceString.replace(caseStr,startIdentifier + replaceByString + endIdentifier);
            }
            return sourceString;  
    }
}