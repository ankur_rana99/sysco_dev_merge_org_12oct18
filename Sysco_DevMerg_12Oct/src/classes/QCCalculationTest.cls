@isTest
public class QCCalculationTest {
    static String namespace = UtilityController.esmNameSpace;
    static List<CaseManager__c> caseList = new List<CaseManager__c> ();
    static List<CaseManager__c> oldCaseList = new List<CaseManager__c> ();
    static List<Invoice__c> invoiceList = new List<Invoice__c> ();
    public static testmethod void TestMethod1() {
        system.assert(true, true);
        System_Configuration__c sysconfig1 = new System_Configuration__c();
        sysconfig1.Module__c = 'Custom_Record_History';
        sysconfig1.Value__c = 'Invoice__c';
        insert sysconfig1;

        System_Configuration__c sysconfig2 = new System_Configuration__c();
        sysconfig2.Module__c = 'Invoice__c';
        sysconfig2.Sub_Module__c = 'OCR_Accuracy_Current_State';
        sysconfig2.Value__c = 'Awaiting OCR Feed';
        insert sysconfig2;
        System_Configuration__c sysconfig3 = new System_Configuration__c(); 
        sysconfig3.Module__c='Heroku User Service'; 
        sysconfig3.Sub_Module__c ='HeroKu Base URL'; 
        insert sysconfig3;
      
        User_Master__c supervisor = ESMDataGenerator.insertBuyerUser('super@test.com', null);
        Sysco_SystemConfiguration__c sysconfigset = new Sysco_SystemConfiguration__c();
        sysconfigset.Name = 'Test';
        sysconfigset.Buyer_Portal_Owner_Id__c = supervisor.Id;
        sysconfigset.CC_Email_Address__c = 'Test';
        sysconfigset.client_id__c = 'Test';
        sysconfigset.client_secret__c = 'Test';
        sysconfigset.EndPointURL__c = 'Test';
        sysconfigset.SOQLQueryLimit__c = 'Test';
        sysconfigset.SyscoAPIC__c = 'Test';
        sysconfigset.TO_Email_Address__c = 'Test';
        sysconfigset.WebserviceRetryLimit__c = 5;
        insert sysconfigset;

     Trigger_Configuration__c trgconfig = new Trigger_Configuration__c();
    trgconfig.Enable_HeroKu_User_Buyer_Sync__c = false;
    trgconfig.Enable_Invoice_Line_Item_Trigger__c = true;
//      trgconfig.Enable_Invoice_master_Trigger__c = true;
    trgconfig.Enable_Sharing_To_Delegate_User__c = false;
//      trgconfig.Enable_Sharing_to_vendor__c = true;
    trgconfig.Enable_User_Usermaster_Sync__c = true;
    //  trgconfig.Enable_Purchase_Order_Master_Trigger__c = true;
    insert trgconfig;
        caseList = ESMDataGenerator.createCases(1);
        //oldCaseList = ESMDataGenerator.createOldCases(1);
        CaseManager__c ct = new CaseManager__c();
        ct.Subject__c = 'Invalid Invoice';
        ct.Status__c = 'Ready For Processing';
        ct.User_Action__c='test1';
        ct.id=caseList.get(0).id;
        oldCaseList.add(ct);
        
        System.debug('OLD '+oldCaseList.get(0).id);
        System.debug('NEW '+caseList.get(0).id);
        QCCalculation.getRandomNumber(10, 20);
        invoiceList = ESMDataGenerator.insertNONPOInvoice(1);
        ESMDataGenerator.CreateRuleAlwaysMoveToQCAmount();
       // ESMDataGenerator.createUser();
        Map<Id, CaseManager__c> caseTrackerOldMap = new Map<Id, CaseManager__c> ();

        caseTrackerOldMap.put(oldCaseList.get(0).id, oldCaseList.get(0));
        QCCalculation.checkForQC(caseList, caseTrackerOldMap);
        QCCalculation.getQCFormFieldInfo(caseList.get(0).id);
        QCCalculation.checkForQC(invoiceList);
        QCCalculation.getRandomNumber(10, 20);
        List<QCHelper.Criteria> lstCriteria = new List<QCHelper.Criteria> ();
        QCHelper.Criteria wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'User_Action__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'contains in list';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Reject';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Exception_Reason__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'contains in list';
        wrapCriteria.type = 'MULTIPICKLIST';
        wrapCriteria.value = 'Currency Mismatch';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'equals';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'not equal to';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        // wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'less than';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'greater than';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'less or equal';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Amount__c';
        //wrapCriteria.objName = namespace + 'Invoice__c';
        wrapCriteria.operator = 'greater or equal';
        wrapCriteria.type = 'NUMBER';
        wrapCriteria.value = '5';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Current_State__c';
        wrapCriteria.operator = 'starts with';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Current_State__c';
        wrapCriteria.operator = 'end with';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Current_State__c';
        wrapCriteria.operator = 'contains';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Current_State__c';
        wrapCriteria.operator = 'does not contain';
        wrapCriteria.type = 'PICKLIST';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'Current_Approver__c';
        wrapCriteria.operator = 'contains';
        wrapCriteria.type = 'REFERENCE';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);

        wrapCriteria = new QCHelper.Criteria();
        wrapCriteria.field = namespace + 'ISTATMET__c';
        wrapCriteria.operator = 'contains';
        wrapCriteria.type = 'BOOLEAN';
        wrapCriteria.value = 'Create';
        lstCriteria.add(wrapCriteria);
        for (QCHelper.Criteria cre1 : lstCriteria)
        {
            QCCalculation.isMatch(cre1, 'test');
            QCCalculation.isMatch(cre1, 123);
            QCCalculation.isMatch(cre1, true);

        }

        QCCalculation.getRandomNumber(10, 20);
    }
}