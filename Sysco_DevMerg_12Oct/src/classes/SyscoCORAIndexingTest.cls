@isTest
public class SyscoCORAIndexingTest{
       public static testMethod void testFirst(){
 		System_Configuration__c sysconfig=new System_Configuration__c ();
        sysconfig.Module__c='Custom_Record_History';
        sysconfig.Value__c ='Invoice__c';
        insert sysconfig;
        System_Configuration__c sysconfig1=new System_Configuration__c ();
        sysconfig1.Module__c='Invoice__c';
        sysconfig1.Sub_Module__c='OCR_Accuracy_Current_State';
        sysconfig1.Value__c ='Awaiting OCR Feed';
        insert sysconfig1;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
           
		Invoice__c invobj = new Invoice__c();    
    	invobj.Invoice_Date__c = Date.Today();
    	invobj.Net_Due_Date__c = Date.Today().addDays(60);    
    	invobj.Total_Amount__c = 500.00;
        invobj.Current_State__c='Vouched';
        //invobj.Current_State__c = 'Pending Merchandise Processing';
    	 System.debug('invobj' + invobj);
        insert invobj;
        System.debug('invobj1----------------' + invobj);

        list<Invoice__c> ListInv = new list<Invoice__c>();
        ListInv.add(invobj);
        SyscoCORAIndexing.executeInvoiceIndexing(ListInv);
              
    }

}