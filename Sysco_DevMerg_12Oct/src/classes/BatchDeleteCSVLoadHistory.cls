/**
    * Buyer Portal.
    * @category  Batch Class
    * @author    Nilesh Patil
    * @create date   17/05/2018
*/


global class BatchDeleteCSVLoadHistory implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        Datetime N_DAYS_AGO = System.today()-30; //getting value from custom setting
        String query = 'SELECT Id,createddate  FROM CSV_Load_History__c where createddate <:N_DAYS_AGO' ;
        system.debug('@@@@ Query is @@@@ :  '+query );
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<CSV_Load_History__c> CSV_Load_HistoryRecords){
        // process each batch of records
        System.debug('CSV_Load_HistoryRecords size : '+CSV_Load_HistoryRecords.size());
        try {
            Database.delete (CSV_Load_HistoryRecords);
        } catch(Exception e) {
            System.debug(e);
        }
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}