public with sharing class PayMeEarlyController {

	@AuraEnabled
	public static List<EsmHelper.dataHelperRecord> getSObjectDetails(String recId, Sobject mainObj) {
		List<EsmHelper.dataHelperRecord> dh = new List<EsmHelper.dataHelperRecord> ();
		List<EsmHelper.Columns> colsInvDtlSec = new List<EsmHelper.Columns> ();

		List<EsmHelper.Columns> colsInvPaySec = new List<EsmHelper.Columns> ();
		String errmsg='';
		String sObjName = '';
		Sobject invObj;
		try 
		{	
		System.debug('recId' + recId);
		System.debug('mainObj' + mainObj);
		string esmNamespace = UtilityController.esmNameSpace;
		//String recId = 'a1737000001QgK9AAK';
		
		String keyPrefix = recId.substring(0, 3);
		for (Schema.SObjectType obj : Schema.getGlobalDescribe().Values()) {
			String prefix = obj.getDescribe().getKeyPrefix();
			if (prefix == keyPrefix) {
				sObjName = esmNamespace + obj.getDescribe().getName();
				break;
			}
		}
		//Invoice__c invObj;	
		//String sObjName = 'Invoice__c';
		//Invoice__c invObj = Database.query(CommonFunctionController.getSOQLQuery(sObjName, 'Id= \'a1737000001QgK9AAK\''));

	
		if (mainObj != null)
		{
			invObj = mainObj;
			System.debug('invObj:' + invObj);
		}
		else
		{
			invObj = Database.query(CommonFunctionController.getSOQLQuery(sObjName, 'Id=:recId'));
			System.debug('invObj:' + invObj);
		}


		List<Schema.DescribeFieldResult> fdsinvd = CommonFunctionController.getFieldsFromFieldSetNew(sObjName, esmNamespace +'Pay_Me_Early_Invoice_Details');
		List<Schema.DescribeFieldResult> fdspd = CommonFunctionController.getFieldsFromFieldSetNew(sObjName,esmNamespace + 'Pay_Me_Early_Payment_Details');
		
		for (Schema.DescribeFieldResult fd : fdsinvd) {
			EsmHelper.Columns col = new EsmHelper.Columns();
			col.name = fd.getName();
			col.label = fd.getLabel();
			System.debug('fd.getReferenceTo()----' + fd.getReferenceTo());
			System.debug('fd.getReferenceTo()----' + fd.getReferenceTo());
			System.debug('fd.getReferenceTo().size()----' + fd.getReferenceTo().size());
			if (fd.getReferenceTo() != null && fd.getReferenceTo().size() > 0)
			{
				col.type = 'reference';

			}
			colsInvDtlSec.add(col);

		}
		
		for (Schema.DescribeFieldResult fd : fdspd)
		{
			EsmHelper.Columns col = new EsmHelper.Columns();
			col.name = fd.getName();
			col.label = fd.getLabel();
			System.debug('fd.getReferenceTo()----' + fd.getReferenceTo());
			System.debug('fd.getReferenceTo()----' + fd.getReferenceTo());
			System.debug('fd.getReferenceTo().size()----' + fd.getReferenceTo().size());
			if (fd.getReferenceTo() != null && fd.getReferenceTo().size() > 0)
			{
				col.type = 'reference';

			}
			colsInvPaySec.add(col);
		

		}	
		}	
		catch(Exception ex){
		
		errmsg=ex.getMessage();
		System.debug(errmsg);
		
		
		}
		dh.add(UtilityController.getWrapperFromJson(invObj, sObjName, colsInvDtlSec,errmsg));
		dh.add(UtilityController.getWrapperFromJson(invObj, sObjName, colsInvPaySec,errmsg));
		return dh;	
		
	}
	@AuraEnabled
	public static double getDiscountVal(String recId) {
		string esmNamespace = UtilityController.esmNameSpace;
		System.debug('entered getDiscountVal');
		String sObjName = esmNamespace + 'Payment_Term__c';
		double DisPercentage = 0;
		try 
		{	       

		
		List<Payment_Term__c> Paymentlist = Database.query(CommonFunctionController.getSOQLQuery(sObjName, 'Id=:recId'));
		
		System.debug('Paymentlist' + Paymentlist);

		for (Payment_Term__c Pymlist : Paymentlist)
		{
			//DisPercentage = Pymlist.Discount_Percentage__c;
			DisPercentage = Double.valueOf(Pymlist.get(esmNamespace+'Discount_Percentage__c'));
			System.debug('in loop DisPercentage' + DisPercentage);
			
		}
		}
		catch(Exception ex){
		System.debug(ex.getMessage());
		}
		System.debug('out loop DisPercentage' + DisPercentage);
		return DisPercentage;
	}
	@AuraEnabled
	public static void updateInvoice(sObject invobject) {
		System.debug('invobject' + invobject);
		update invobject;


	}
	/*@AuraEnabled
	public static String getNamespace(sObject invobject) {
	
	string esmNamespace = UtilityController.esmNameSpace;
	//String esmNamespace ='akritiv__';
	return esmNamespace;
	}*/


	

}