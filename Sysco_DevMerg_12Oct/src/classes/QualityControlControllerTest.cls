@isTest
public class QualityControlControllerTest{

     public static String esmNameSpace = UtilityController.esmNameSpace;

    public static testmethod void testFirst() {
    
        system.assert(true,true);
        ESMDataGenerator.insertSystemConfiguration();
      
        Purchase_Order__c  po = new Purchase_Order__c();
        po.PO_No__c = 'PO-1001';
        po.Amount__c = 123;
        insert po;
        
        Invoice__c invobj = new Invoice__c();       
        invobj.Invoice_Date__c = Date.Today();
        invobj.Net_Due_Date__c = Date.Today().addDays(60);      
        invobj.Total_Amount__c = 500.00;  
        invobj.Invoice_No__c='INV-1001';
        insert invobj;
        System.debug('invobj2' + invobj);
    
        QualityCheck__c qcObj = ESMDataGenerator.insertQualityCheck(invobj.id);   
        
            
        QualityControlController.getQualityControls(qcObj.id ,esmNameSpace +'FieldSetForTestClass');
           
        QualityControlController.getQualityControls(qcObj.id ,esmNameSpace +'fieldset');          
        QualityControlController.getQCFieldSetName(qcObj.id );      
        QualityControlController.saveQualityData(qcObj );      
        CaseManager__c cm = TestDataGenerator.getCase(); 
        // QualityControlController.getFieldsApex(cm.id);    
        
        
    }
    
    
}