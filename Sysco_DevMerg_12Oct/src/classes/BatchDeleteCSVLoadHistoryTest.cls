@isTest
public class BatchDeleteCSVLoadHistoryTest {

    public static testmethod void testcase1(){
        CSV_Load_History__c clh= new CSV_Load_History__c();
        clh.Email__c='test@test.com';
        clh.Error_Status__c=false;
        clh.File_Name__c='testfile';
        
        insert clh;
        
        
       // BatchDeleteCSVLoadHistory bdclh = new BatchDeleteCSVLoadHistory();
       // database.executeBatch(bdclh,200);
        
        Test.StartTest();
        BatchDeleteCSVLoadHistoryScheduler bdclhs = new BatchDeleteCSVLoadHistoryScheduler();
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, bdclhs);
        Test.stopTest();
    }
}