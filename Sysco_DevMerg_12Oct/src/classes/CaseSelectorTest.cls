@isTest
public class CaseSelectorTest {
     public static testMethod void unitTest01(){
   /* Set<String> relationShipFields=new Set<String>();
    relationShipFields.add('Id');
    Set<String> relationShipQueries=new Set<String>();
    relationShipQueries.add('Name');
    Set<Id> idSet=new Set<Id>();
	idSet.add('a1xm0000002DYDUAA4');
      
    CaseSelector obj1=new CaseSelector();
    CaseSelector obj=new CaseSelector(relationShipFields,relationShipQueries);
 //  obj.selectCasesByClause('');
  //  obj.selectSharedCases('where Id=\'abc\'');
  //  obj.selectMyTaskCases('where Id=\'abc\'');
 //   obj.selectClosedCases('where Id=\'abc\'');
  //  obj.selectUnassignedCases('where Id=\'abc\'');
  //  obj.selectAllCases();
   // obj.addRelationShip(relationShipFields);
   //bj.selectCasesByIds(idSet);*/
         Test.startTest();
			 CaseManager__c ct =TestDataGenerator.createCaseTracker();
			 CaseSelector cs =new CaseSelector();
			 cs.addRelationShip(new Set<String>{'Emails'});
			 cs.selectCasesByIds(new Set<Id>{ct.Id});
			 cs.selectCasesByClause(' (Id != null) ');
		//	 cs.selectSharedCases('');
			 cs.selectClosedCases('');
			 cs.selectMyTaskCases('');
			// cs.selectUnassignedCases('');
			 CaseSelector cs1 =new CaseSelector(new Set<String>{'Vendor__r.Name'},new Set<String>{'Histories','Emails'});
			 cs1.selectAllCases();
		 Test.stopTest();
		 system.assert(true);
   }
}