public class DashboardMessageController {
	@AuraEnabled 
    public static String getOpcoCode(){
        String opcoCode = '';
        Id loggedInUserId = UserInfo.getUserId();
        List<String> opcoCodeSet = new List<String>();
        //List<User> currentUser = [SELECT Id,Name,Contact.Id,Contact.Account.Id,Contact.Account.OPCO_Code__c,Contact.Account.OPCO_Code__r.Name FROM User WHERE Id=:loggedInUserId];
        List<User> currentUser = [SELECT Id,Name,Contact.Id,Contact.Account.Id,Contact.Account.Vendor_No__c FROM User WHERE Id=:loggedInUserId];
        System.debug('currentUser : ' + currentUser);
        if(currentUser != null && currentUser.size() > 0){
            User u = currentUser.get(0);
            if(u.Contact != null && u.Contact.Id != null && u.Contact.Account.Id != null && u.Contact.Account != null && u.Contact.Account.Vendor_No__c != null){
                List<Account> lstAcc = [SELECT Id, Name, OPCO_Code__r.OPCO_Code__c FROM Account WHERE Vendor_No__c =:u.Contact.Account.Vendor_No__c];
                if(lstAcc.size()>0){
                    for(Account a:lstAcc){                        
                        if(a.OPCO_Code__r.OPCO_Code__c!=null && a.OPCO_Code__r.OPCO_Code__c!=''){                         
                            opcoCodeSet.add(a.OPCO_Code__r.OPCO_Code__c);   
                        }
                    }
                } 
            }
        }
        return (opcoCodeSet.size()>0) ? String.join(opcoCodeSet,',') : '';
    }
}