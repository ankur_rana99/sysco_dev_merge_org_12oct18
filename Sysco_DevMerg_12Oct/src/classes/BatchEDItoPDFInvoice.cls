global class BatchEDItoPDFInvoice implements Database.Batchable<sObject>
{
    list<ContentVersion> contentVersionList{get;set;}
    Invoice_Configuration__c ic = new Invoice_Configuration__c();
    global BatchEDItoPDFInvoice(){
        ic=[select id,EDI_PDF_Process_Current_State__c,Retry_Count__c from Invoice_Configuration__c];
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //ic=[select id,EDI_PDF_Process_Current_State__c,Retry_Count__c from Invoice_Configuration__c];
        string status='\''+ic.EDI_PDF_Process_Current_State__c.replace(',','\',\'')+'\'';
        //String query = 'SELECT Id,PDF_creation_Retry_count__c,Comments__c,(select id from Invoice_Line_Items_Invoice__r) from Invoice__c where Current_State__c in ('+status+')';
        String query = 'SELECT Id,PDF_creation_Retry_count__c,Comments__c,(select id from Invoice_Line_Items_Invoice__r) from Invoice__c where Name in (\'INV-00005732\')';
        system.debug('------Query------->'+query);
        system.debug('------Retry_Count__c------->'+ic.Retry_Count__c);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Invoice__c> scope)
    {
        try
        {
            contentVersionList=new list<ContentVersion>();
            system.debug('------scope------->'+scope);
            for(Invoice__c inv:scope)
            {
                if(inv.Invoice_Line_Items_Invoice__r!=null)
                {
                    try
                    {
                        //Code added by dhwanil to add pdf in the attachement
                        PageReference pdf = Page.EDItoPDF;
                        pdf.getParameters().put('invid',(String)inv.id);
                        pdf.setRedirect(true);
                        // create the new attachment
                        //Attachment attach = new Attachment();
                        // the contents of the attachment from the pdf
                        Blob body;
                        if(!Test.isRunningTest()){
                            body = pdf.getContent();
                        }else{
                            body = blob.valueOf('Test Body');
                        }
                        
                        //attach.Body = body;
                        // add the user entered name
                        //attach.Name = inv.id + '.pdf';
                        //attach.IsPrivate = false;
                        // attach the pdf to the account
                        //attach.ParentId = inv.id;
                      
                        
                        ContentVersion cv = new ContentVersion();
                        cv.Title = 'EDI-'+inv.id;
                        cv.PathOnClient = 'EDI-'+inv.id+'.pdf';
                        cv.VersionData = body;
                        cv.Is_Primary_Doc__c=true;
                        cv.IsMajorVersion = true;
                        contentVersionList.add(cv);
                        inv.User_Action__c='EDI Success';
                    }
                    catch(Exception e)
                    {
                        if(inv.PDF_creation_Retry_count__c == null){
                            inv.PDF_creation_Retry_count__c = 0;
                        }
                        system.debug('------Retry_Count__c------->'+ic.Retry_Count__c);
                        if(ic.Retry_Count__c <= inv.PDF_creation_Retry_count__c){
                            inv.Comments__c = 'Data for PDF creation is not correct';
                            inv.User_Action__c='EDI Fail';
                        }
                        else{
                            inv.PDF_creation_Retry_count__c += 1;
                        }
                        //inv.User_Action__c='EDI Fail';
                    }
                }
            }
            insert contentVersionList;
            List<ContentVersion> conetntVersionListWithId = [SELECT id, ContentDocumentId, Title FROM ContentVersion where Id = :contentVersionList];
            
            system.debug('------conetntVersionListWithId-------'+conetntVersionListWithId);
            ContentWorkspace shareWorkspace = [select id from ContentWorkspace where name = :ESMConstants.WORKSPACE_NAME limit 1];
            system.debug('------shareWorkspace -------'+shareWorkspace);
            
            List<ContentDocumentLink> contentDocumentLiskLIst = new List<ContentDocumentLink> ();
            List<ContentWorkspaceDoc> contenctWorkspaceDocList = new List<ContentWorkspaceDoc> ();

            for (ContentVersion contentVersionObj : conetntVersionListWithId) {
                String FileName=contentVersionObj.Title;
                FileName=FileName.replace('EDI-','');
                FileName=FileName.replace('.pdf','');
                system.debug('--------'+FileName);
                ContentDocumentLink contentDocumentLinkObj = new ContentDocumentLink();
                contentDocumentLinkObj.ContentDocumentId = contentVersionObj.ContentDocumentId;
                contentDocumentLinkObj.LinkedEntityId = FileName;
                contentDocumentLinkObj.ShareType = 'V';
                system.debug('------contentDocumentLinkObj-------'+contentDocumentLinkObj);
                contentDocumentLiskLIst.add(contentDocumentLinkObj);
                system.debug('------contentDocumentLiskLIst-------'+contentDocumentLiskLIst);
                ContentWorkspaceDoc contentWorspaceDocObj = new ContentWorkspaceDoc();
                contentWorspaceDocObj.ContentDocumentId = contentVersionObj.ContentDocumentId;
                contentWorspaceDocObj.ContentWorkspaceId = shareWorkspace.id;
                contenctWorkspaceDocList.add(contentWorspaceDocObj);

            }
            if (contentDocumentLiskLIst.size() > 0) {
                insert contentDocumentLiskLIst;
                insert contenctWorkspaceDocList;
                system.debug('------contentDocumentLiskLIst-------'+contentDocumentLiskLIst);
            }

            update scope;
        }
        catch(Exception e)
        {
            
            system.debug('------Batch Process Error------:'+e.getcause()+':'+e.getlinenumber());
        }
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
}