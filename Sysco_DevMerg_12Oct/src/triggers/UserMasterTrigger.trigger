trigger UserMasterTrigger on User_Master__c (before insert, before update, after insert, after update) {
    if(Trigger.isBefore){
        set<String> uniqueUserName = new set<String>();
        for(User_Master__c newBu : Trigger.new){
            if(!uniqueUserName.add(newBu.User_Name__c)){
                newBu.addError(Label.User_Name_is_already_exist_in_system);
            }
        }
        system.debug('uniqueUserName=>'+uniqueUserName);
        map<String, User_Master__c> userMap = new map<String, User_Master__c>([SELECT Id,User_Name__c FROM User_Master__c where User_Name__c IN :uniqueUserName]);
        system.debug('userMap=>'+userMap);
        if(Trigger.isInsert){
            for(User_Master__c newBu : Trigger.new){
                for(User_Master__c newBuTemp : userMap.Values()){
                    if(newBuTemp.User_Name__c == newBu.User_Name__c){
                        newBu.addError(Label.User_Name_is_already_exist_in_system);
                    }
                }
            }
        }else{
            for(User_Master__c newBu : Trigger.newMap.Values()){
                for(User_Master__c newBuTemp : userMap.Values()){
                    if(newBuTemp.User_Name__c == newBu.User_Name__c && newBu.Id != newBuTemp.Id){
                        newBu.addError(Label.User_Name_is_already_exist_in_system);
                    }
                }
            }
        }
    } else {
        if(Trigger_Configuration__c.getOrgDefaults().Enable_HeroKu_User_Buyer_Sync__c){
            String akritivesm = UtilityController.esmNameSpace;
            Set<Id> ids = new Set<Id>();
            HerokuUserService hu =  new HerokuUserService();
            if(Trigger.isInsert){
                ids.addAll(trigger.newMap.keySet());
            }else{
                Map<String,Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(akritivesm+'User_Master__c').getDescribe().fields.getMap();
                for(User_Master__c newBu : Trigger.newMap.Values()){
                    User_Master__c oldBu = Trigger.oldMap.get(newBu.id);
                    for(Schema.SObjectField fieldDef : objectFields.values()){
                        String field = fieldDef.getDescribe().getName();
                        if(!field.equalsIgnoreCase('CreatedBy') && !field.equalsIgnoreCase('LastModifiedBy') && !field.equalsIgnoreCase('isdeleted') &&  !field.equalsIgnoreCase('createdbyid') && !field.equalsIgnoreCase('lastmodifieddate') && !field.equalsIgnoreCase('lastmodifiedbyid') && !field.equalsIgnoreCase('systemmodstamp') && !field.equalsIgnoreCase('lastactivitydate') && 
                            !field.equalsIgnoreCase('OwnerId') && !field.equalsIgnoreCase('CreatedDate') && !field.equalsIgnoreCase('LastModifiedDate') && !field.equalsIgnoreCase(akritivesm+'Last_Login_Date__c') && 
                            newBu.get(field) != oldBu.get(field)){
                            ids.add(newBu.Id);
                            break;
                        }
                    }
                    
                }
            }
            if(0 < ids.size()){
        //        hu.createHerokuUserDetails(ids);
            }
        }
    }
}