trigger PurchaseOrderMasterTrigger on Purchase_Order__c(before insert, before update, after insert, after update) {
    try {
        /*-------------Start JIRA (CAS-136)-----------------------*/
      If(Trigger.isBefore){
        map<string, id> acmap = new map<string, id>();
        set<String> F_venlist = new set<String>();
        for(Purchase_Order__c po: trigger.new){
            if(po.Freight_Vendor_No__c != null){
                F_venlist.add(po.Freight_Vendor_No__c);
            }
        }
        
            
        if(F_venlist!=null && F_venlist.size()>0){
            for(Account ac: [select id, name,Vendor_No__c,OPCO_Code__c from Account where Vendor_No__c IN:F_venlist]){
                acmap.put(ac.OPCO_Code__c+'-'+ac.Vendor_No__c, ac.id);
            }
        }
        
        
        if(acmap!= null && acmap.size()>0){
            for(Purchase_Order__c po: trigger.new){
                if(po.Freight_Vendor_No__c != null){
                    po.Freight_Vendor__c=acmap.get(string.valueof(po.OPCO_Code__c+'-'+po.Freight_Vendor_No__c));
                }
            }
        }
    }
    /*-------------END JIRA (CAS-136)-----------------------*/
        //--- Share Purchase Order with Vendor ---//
        //---- Vendor hierarchy setup by Vishwa | CASP2-8
        if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
            
                List<SObject> insertShareObj = new List<SObject> ();
               
            
                System.debug('---------in if----------------');
                Set<Id> parentId = Trigger.NewMap.keySet();

                List<Purchase_Order__Share> deleteShareObj = Database.query('SELECT Id FROM Purchase_Order__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');

                set<string> vendorId = new Set<String> ();
              
                for (Purchase_Order__c obj : Trigger.New) {

                    if (obj.Vendor__c != null) {
                        vendorId.add(obj.Vendor__c);
                    }
                    
                }
               System.debug('vendorId:' + vendorId);
           /*   Map<String, String> mapUserNameVendorId = new Map<String, String> ();
                if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

                    for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
                        mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
                    }
                }
                System.debug('mapUserNameVendorId:' + mapUserNameVendorId);*/
                 
                set<string> contactId = new Set<String> ();
                Map<String,  set<string>> mapVendorIdContactId = new Map<String,  set<string>> ();
                if (Contact.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Contact.fields.Id.isAccessible() && Schema.sObjectType.Contact.fields.AccountId.isAccessible()) {

                    for (Contact con :[SELECT Id, AccountId FROM Contact WHERE AccountId IN :vendorId]) {
                        contactId.add(con.Id);
                       mapVendorIdContactId.put(con.AccountId, contactId);
                    }
                }
                 System.debug('mapVendorIdContactId:' + mapVendorIdContactId);
                    
                Map<String, Id> mapContactIdUId = new Map<String, Id> ();
               if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName, ContactId FROM User WHERE ContactId IN :contactId]){
                        mapContactIdUId.put(u.ContactId, u.id);
                    }
                }
                  System.debug('mapContactIdUId:' + mapContactIdUId);  

             /*   Map<String, String> mapUserNameUId = new Map<String, String> ();
                if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
                        mapUserNameUId.put(u.UserName, u.id);
                    }
                }*/

                for (Purchase_Order__c obj : Trigger.New) {
                    
                    if (obj.Vendor__c != null && mapVendorIdContactId.get(obj.Vendor__c) != null){

                       System.debug('mapVendorIdContactId.get(obj.Vendor__c):' + mapVendorIdContactId.get(obj.Vendor__c));
                       for (String conId :mapVendorIdContactId.get(obj.Vendor__c)){
                        
                        if(mapContactIdUId.get(conId)!=null){
       
                        System.debug('mapContactIdUId.get(conId):' + mapContactIdUId.get(conId));
                        Purchase_Order__Share shareObj = new Purchase_Order__Share();
                        shareObj.put('ParentId', obj.id);
                        shareObj.put('UserOrGroupId', mapContactIdUId.get(conId));
                        shareObj.put('AccessLevel', 'Read');
                        shareObj.put('RowCause', Schema.Purchase_Order__Share.RowCause.Manual);
                        insertShareObj.add(shareObj);
                       
                        System.debug('shareObj:' + shareObj);
                        }
                      }

                    }
                }
                if (deleteShareObj != null && deleteShareObj.size() > 0) {
                    List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
                }
                system.debug('insertShareObj:'+insertShareObj);
                if (insertShareObj != null && insertShareObj.size() > 0) {
                    List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
                }

               
            
            
        }
        //---- Vendor hierarchy setup by Vishwa | CASP2-8
        /*if (sysconfig != null && sysconfig.size() > 0 && sysconfig.get(0).Value__c == 'true') {
            if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {

                List<SObject> insertShareObj = new List<SObject> ();
                Set<Id> parentId = Trigger.NewMap.keySet();

                List<Purchase_Order__share> deleteShareObj = [SELECT Id FROM Purchase_Order__share WHERE ParentId IN :parentId AND AccessLevel != 'All'];

                set<string> vendorId = new Set<String> ();

                for (Purchase_Order__c obj : Trigger.New) {

                    if (obj.Vendor__c != null) {
                        vendorId.add(obj.Vendor__c);
                    }
                }

                Map<String, String> mapUserNameVendorId = new Map<String, String> ();

                if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

                    for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
                        mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
                    }
                }

                Map<String, String> mapUserNameUId = new Map<String, String> ();

                if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
                        mapUserNameUId.put(u.UserName, u.id);
                    }
                }

                for (Purchase_Order__c obj : Trigger.New) {
                    if (obj.Vendor__c != null && mapUserNameVendorId.get(obj.Vendor__c) != null && mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)) != null) {

                        Purchase_Order__Share shareObj = new Purchase_Order__Share();
                        shareObj.put('ParentId', obj.id);
                        shareObj.put('UserOrGroupId', mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)));
                        shareObj.put('AccessLevel', 'Edit');
                        shareObj.put('RowCause', Schema.Purchase_Order__Share.RowCause.Manual);
                        insertShareObj.add(shareObj);

                    }
                }

                if (deleteShareObj != null && deleteShareObj.size() > 0) {
                    List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
                }
                if (insertShareObj != null && insertShareObj.size() > 0) {
                    List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
                }
            }
        }*/
        
        //To cover the exception scenario for the POH while running from the Exchange
        
        If((Trigger.isBefore) && (Trigger.isUpdate || Trigger.isInsert)){
            List<Account> accList = new List<Account>();
            String isrecordfound = '';
            Set<String> venIdkeys = new Set<String>();
            Map<String, Purchase_Order__c> vendorIds = new Map<String, Purchase_Order__c>();
            
            for(Purchase_Order__c po:trigger.new){
                vendorIds.put(po.Vendor_Number__c,po);            
            }    
            
            system.debug('vendorIds :' + vendorIds);
            system.debug('vendorIds Size :' + vendorIds.size());
            
            accList = [Select Id,Unique_Key__c,Vendor_No__c from Account where Vendor_No__c IN: vendorIds.keyset()];
             system.debug('accList.size() :' + accList.size());
             
            
            for(Purchase_Order__c po:trigger.new){
                if(po.Source_System__c == 'Workday'){
                    isrecordfound = 'N';
                    if(po.Vendor_Number__c != null){                        
                        for(Account acc: accList ){
                            if(acc.Unique_Key__c != null && acc.Unique_Key__c != ''){
                                if(acc.Vendor_No__c == po.Vendor_Number__c){
                                    isrecordfound = 'Y' ;
                                    System.debug('acc.Unique_Key__c:' + acc.Unique_Key__c);
                                    po.Vendor__c = acc.Id; 
                                    break;
                                }
                            }                     
                        }
                        System.debug('In first if condition');
                        if(isrecordfound == 'N'){
                            po.Vendor_NotFound_InVMD__c = '';
                            System.debug('No Related Vendor Num');
                        }else{
                            System.debug('Found');
                        }
                    }
                    else{
                         po.Vendor_NotFound_InVMD__c = '';
                         System.debug('No Related Vendor Num, No CSV Vendor Id is blank');
                    }
                }
            }
         }
    }
    catch(Exception e) {
        Trigger.New[0].addError(UtilityController.customDebug(e, 'PurchaseOrderMasterTrigger'));
    }
}