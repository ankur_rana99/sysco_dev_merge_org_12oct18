trigger CaseManagerMasterTrigger on CaseManager__c(before insert, before update, after insert, after update) {
  try {
      
    String esmNameSpace = UtilityController.esmNameSpace;
	// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Start
	map<String, Account> mapVendorNoWithVendor = new map<String, Account>();
	// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Ends
      if(UtilityController.vendorID ==  null){
            UtilityController.vendorID = new set<ID>();
            for(Account venObj:[SELECT Id,Vendor_No__c FROM Account LIMIT 50000]){
               // UtilityController.VendorMap.put(venObj.Vendor_No__c, venObj);
                UtilityController.vendorID.add(venObj.Id);
				// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Start
				if(venObj.Vendor_No__c != null){
					mapVendorNoWithVendor.put(venObj.Vendor_No__c, venObj);
				}
				// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Ends
            }
          System.debug('UtilityController.vendorID ******'+UtilityController.vendorID);
        }
      
    //--- Capture Invoice Comments ---//
    Map<Boolean,string> errorMap = new Map<Boolean,string>();
    if (Trigger.isBefore) {

      errorMap = CommonFunctionController.insertCommentHistory(Trigger.New, esmNameSpace+'CaseManager__c', esmNameSpace,true);
      if(errorMap.keySet().contains(true)){
        Trigger.New[0].addError(errorMap.get(true));
      }

    }
    // --- Share CaseManager with Vendor ---//
        if (Trigger.isBefore && Trigger.isUpdate) {        
            CaseManagerHandler.updatePrevQueue(trigger.new, trigger.old);
            //To populate report related fields
            CaseManagerHandler.populateReportRelatedFields(trigger.new, trigger.old);       

 
        }
        if (Trigger.isAfter && Trigger.isUpdate) {
            //TATTriggerHelper.calculateStateChange(Trigger.New, Trigger.OldMap);
            //TATTriggerHelper.setActualTatTime(Trigger.New, Trigger.OldMap);
        }
        if (Trigger.isAfter && Trigger.isInsert) {
            //TATTriggerHelper.CreateCaseTransition(trigger.new);
        }
        
        if (Trigger.isbefore && (Trigger.isUpdate || Trigger.isInsert)) {
             if (trigger.isBefore && trigger.isInsert) {
                    //TATTriggerHelper.calculateTAT(trigger.new);
             }
            // [Start] TAT to be based on first response time(over due) - Exclude Non Business hours changes
                BusinessHours stdBusinessHours = [select id from businesshours where isDefault = true limit 1];
                Decimal diffhrours = 0.00;
                Decimal diffhrours1 = 0.00;
                Decimal diffhrours2 = 0.00;
                Decimal diffhrours3 = 0.00;
                for (CaseManager__c ct : trigger.new) {
                    /*System.debug('Send_Time_with_Sysco__c -- '+ct.Send_Time_with_Sysco__c);
                    System.debug('ct.Current_State__c -- '+ct.Current_State__c);
                    System.debug('ct.End_Time_with_Sysco__c -- '+ct.End_Time_with_Sysco__c);*/
                    String ownerType = String.valueOf(ct.OwnerId.getSObjectType());
                    System.debug('ownerType -- '+ownerType);
                    if ('Group'.equalsignorecase(ownerType)) {
                        ownerType = 'Queue';
                    }
					ct.put('OwnerType__c', ownerType);
					// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Start
					if(!String.isBlank(ct.Supplier_Employee_Number__c) && mapVendorNoWithVendor != null && mapVendorNoWithVendor.get(ct.Supplier_Employee_Number__c) != null){
						ct.put('Vendor__c', mapVendorNoWithVendor.get(ct.Supplier_Employee_Number__c));
					}
					// change for populating vendor on the basis of supplier no. provided in supplier portal - by Vaibhav - 01 Oct, 2018 - Ends
					if(stdBusinessHours != NULL && ct.createdDate != null   && ct.End_Time_with_Sysco__c != null) 
                    {
                            if(ct.Send_Time_with_Sysco__c==null)
                            {
                                ct.Send_Time_with_Sysco__c=ct.createdDate;
                            }
                            diffhrours= (BusinessHours.diff(stdBusinessHours.id,ct.createdDate ,ct.End_Time_with_Sysco__c ));
                            //Decimal inhrs=(diffhrours*0.0000166667);
                            Decimal inhrs=(diffhrours / 60000) / 60;
                            Decimal toround = inhrs;
                            Decimal rounded = toRound.setScale(2, RoundingMode.HALF_DOWN);
                            //system.debug(rounded);
                            
                            ct.Total_Time_with_Sysco__c = 0; 
                            Decimal test2 = ct.Total_Time_with_Sysco__c + rounded;
                            ct.Total_Time_with_Sysco__c= test2;
                    }

                    //[Start] TAT on Assigned Date
                    if(stdBusinessHours != NULL && ct.Assigned_Date__c != null   && ct.End_Time_with_Sysco__c != null && ct.Is_Freeze_Assigned_Date_TAT__c==false)
                    {
                        diffhrours1= (BusinessHours.diff(stdBusinessHours.id,ct.Assigned_Date__c ,ct.End_Time_with_Sysco__c ));
                        //Decimal inhrs=(diffhrours1*0.0000166667);
                        Decimal inhrs=(diffhrours1 / 60000) / 60;
                        Decimal toround = inhrs;
                        Decimal rounded = toRound.setScale(2, RoundingMode.HALF_DOWN);
                        //system.debug(rounded);
                        ct.TAT_on_Assigned_Date__c = 0; 
                        Decimal test2 = ct.TAT_on_Assigned_Date__c + rounded;
                        ct.TAT_on_Assigned_Date__c= test2/24;
                        
                    }
                    if(ct.End_Time_with_Sysco__c != null ){
                            system.debug('is frezz');
                            ct.Is_Freeze_Assigned_Date_TAT__c=true;
                    }
                    //[End] TAT on Assigned Date

                    //[Start] Closure from Creation in days
                    if(stdBusinessHours != NULL && ct.CreatedDate != null   && ct.Closure_Date__c != null)
                    {
                        diffhrours2= (BusinessHours.diff(stdBusinessHours.id,ct.CreatedDate ,ct.Closure_Date__c));
                        //Decimal inhrs=(diffhrours2*0.0000166667);
                        Decimal inhrs=(diffhrours2 / 60000) / 60;
                        Decimal toround = inhrs;
                        Decimal rounded = toRound.setScale(2, RoundingMode.HALF_DOWN);
                        //system.debug(rounded);
                       
                        ct.Closure_from_Creation_in_days__c = 0; 
                        Decimal test2 = ct.Closure_from_Creation_in_days__c + rounded;
                        ct.Closure_from_Creation_in_days__c= (test2/24).intValue();

                    }
                    //[End] Closure from Creation in days

                    //[Start] Closure from Assignment in Days
                    if(stdBusinessHours != NULL && ct.Assigned_Date__c != null   && ct.Closure_Date__c != null)
                    {
                        diffhrours3= (BusinessHours.diff(stdBusinessHours.id,ct.Assigned_Date__c ,ct.Closure_Date__c));
                        Decimal inhrs=(diffhrours3 / 60000) / 60;
                        Decimal toround = inhrs;
                        Decimal rounded = toRound.setScale(2, RoundingMode.HALF_DOWN);
                        ct.Closure_from_Assignment_in_Days__c = 0; 
                        Decimal test2 = ct.Closure_from_Assignment_in_Days__c + rounded;
                        ct.Closure_from_Assignment_in_Days__c= (test2/24).intValue();

                    }
                    //[End] Closure from Assignment in Days
                    //ct.Last_Response_Date__c=Datetime.now();
                }
                // [End] TAT to be based on first response time(over due) - Exclude Non 

   
        }
 
      // ------- Vendor Inquiry sharing Issue ---- Added by Shital Rathod----Start---
    if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
            	   
            System.debug('---------in if----------------');
            List<SObject> insertShareObj = new List<SObject> ();
            Set<Id> parentId = Trigger.NewMap.keySet();
            System.debug('parentId'+parentId);
           List<CaseManager__share> deleteShareObj = Database.query('SELECT Id FROM ' + esmNameSpace + 'CaseManager__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');
            
            set<string> vendorId = new Set<String> ();
            
            for (CaseManager__c obj : Trigger.New) {
                if (obj.Vendor__c != null) {
                    vendorId.add(obj.Vendor__c);
                }
            }
            
            Map<String, list<String>> mapContactVendorId = new Map<String, list<String>> ();
            set<String> Contactset = new set<String> ();
            if (contact.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.contact.fields.Id.isAccessible() && Schema.sObjectType.contact.fields.AccountId.isAccessible()) {
                for (contact cont :[SELECT Id, AccountId FROM contact WHERE AccountId IN :vendorId]) {
                    list<String> con = new list<String> ();
                    if (mapContactVendorId.containsKey(cont.AccountId))
                        con = mapContactVendorId.get(cont.AccountId);
                    con.add(cont.id);
                    Contactset.add(cont.id);
                    mapContactVendorId.put(cont.AccountId, con);
                }
            }
            
            System.debug('--------mapContactVendorId' + mapContactVendorId);
            Map<String, String> mapContactUserId = new Map<String, String> ();
            if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName,ContactId')) {
                for (User u :[Select Id, FirstName, LastName, IsActive, UserName, ContactId FROM User WHERE ContactId IN :Contactset AND IsActive = True]) {
                    mapContactUserId.put(u.ContactId, u.id);
                }
            }
            
            System.debug('--------mapContactUserId' + mapContactUserId);
            
            for (CaseManager__c obj : Trigger.New) {
                if (obj.Vendor__c != null && mapContactVendorId.get(obj.Vendor__c) != null) {
                    list<string> conList = mapContactVendorId.get(obj.Vendor__c);
                    for (string conid : conList)
                    {
                        if (mapContactUserId.containsKey(conid))
                        {
                            string uid = mapContactUserId.get(conid);
                            CaseManager__share shareObj = new CaseManager__share();
                            shareObj.put('ParentId', obj.id);
                            shareObj.put('UserOrGroupId', uid);
                            // ------------JIRA-CASP2-82--- added by Shital Rathod --- start
                            shareObj.put('AccessLevel', 'Edit');
                             // ------------JIRA-CASP2-82--- added by Shital Rathod --- end
                            shareObj.put('RowCause', Schema.CaseManager__share.RowCause.Manual);
                            insertShareObj.add(shareObj);
                        }
                    }
                }
            }
            System.debug('--------insertShareObj' + insertShareObj);
            if (deleteShareObj != null && deleteShareObj.size() > 0) {
                List<Database.DeleteResult> sr = Database.delete(deleteShareObj,false);
            }
            
            if (insertShareObj != null && insertShareObj.size() > 0) {
                List<Database.SaveResult> sr = Database.insert(insertShareObj,false);
            }
            
        }
       // ------- Vendor Inquiry sharing Issue ---- Added by Shital Rathod----end---    
  }
  catch(Exception e) {
    System.debug(ExceptionHandlerUtility.customDebug(e,'55554545'));
    system.debug('================'+e.getlinenumber());
    Trigger.New[0].addError(UtilityController.customDebug(e, 'CaseManagerMasterTrigger'));
  }

}