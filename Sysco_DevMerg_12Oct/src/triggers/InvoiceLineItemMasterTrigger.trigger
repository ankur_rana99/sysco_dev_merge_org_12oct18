trigger InvoiceLineItemMasterTrigger on Invoice_Line_Item__c(before insert, before update, after insert, after update, after delete) {
    try {
        Trigger_Configuration__c tgrConf;
        if (Trigger_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
            tgrConf = Trigger_Configuration__c.getInstance(Userinfo.getUserid());
        } else {
            tgrConf = Trigger_Configuration__c.getOrgDefaults();
        }
        //System.debug('tgrConf --------'+tgrConf);
        /*Invoice_Configuration__c invConf;
if(Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
invConf = Invoice_Configuration__c.getInstance(Userinfo.getUserid());
} else {
invConf = Invoice_Configuration__c.getOrgDefaults();
}*/
        //System.debug('invConf --------'+invConf);
        //List<System_Configuration__c> sysConfigs = new List<System_Configuration__c>();
        //sysConfigs = [select id, Name, Module__c, Sub_Module__c, Value__c from System_Configuration__c where Module__c = 'Quantity_Calculation' and (Sub_Module__c = 'Enable_Quantity_Calculation' or Sub_Module__c = 'Invoice_Reject_Current_State') limit 999];
        //System.debug('sysConfigs --------'+sysConfigs);
		//Added By Sonam | 12-09-2018 | CAS-1383 | Start
		set < id > setInvIds = new set < id > ();
		map < Id, Invoice__c > invMap = new map < Id, Invoice__c > ();
		//Added By Sonam | 12-09-2018 | CAS-1383 | End
        if (tgrConf.Enable_Invoice_Line_Item_Trigger__c) {
            String esmNameSpace = UtilityController.esmNameSpace;
            if (Trigger.IsBefore) {
                //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | BEGIN @@@@@@@//
                map < String, Utility_Vendor_Mapping__c > mapKeyUtilityVendor = new map < String, Utility_Vendor_Mapping__c > ();
                map < String, String > invStatusMap = new map < String, String > ();
				//Updated By Sonam | 12-09-2018 | CAS-1383 | Start
                //map < Id, Invoice__c > invMap = new map < Id, Invoice__c > ();
                //set < id > setInvIds = new set < id > ();
				//Updated By Sonam | 12-09-2018 | CAS-1383 | Start
                set < id > setOpcoIds = new set < id > ();
                set < Id > setVendorIds = new set < Id > ();
				Map<Id,Integer> InvoiceLineCountMap = new Map<Id,Integer>();
				string currentInvId = '';
                List < Invoice__c > invList = new List < Invoice__c > ();
                List < Invoice__c > invACKList = new List < Invoice__c > ();
                for (Invoice_Line_Item__c invl: Trigger.New) {
                    setInvIds.add(invl.Invoice__c);
                }
				if(Trigger.IsInsert) {
                    Integer cnt = 0;
                    for(Invoice_Line_Item__c invl : [SELECT Id,Name,Invoice__c,Invoice_Line_No__c,Invoice_Line_Item_No__c FROM Invoice_Line_Item__c WHERE Invoice__c IN:setInvIds]){
                        if(currentInvId != String.valueOf(invl.Invoice__c)){
                            cnt = 0;
                            currentInvId = String.valueOf(invl.Invoice__c);
                        }
                        cnt++;
                        InvoiceLineCountMap.put(invl.Invoice__c,cnt);
                    }
                }
                // Change added for Workday by - Vaibhav - *****start***
                map < String, WD_UtilityListing__c > mapUtilityListings = new map < String, WD_UtilityListing__c > ();
                List < WD_UtilityListing__c > lstWDListings = [SELECT Id, Cost_Center__c, Profit_Center__c, Spend_Categories__c, Vendor__c FROM WD_UtilityListing__c];
                if (lstWDListings != null && lstWDListings.size() > 0) {
                    for (WD_UtilityListing__c utilityListing: lstWDListings) {
                        mapUtilityListings.put(utilityListing.Vendor__c, utilityListing);
                    }
                }
                System.debug('mapUtilityListings===> ' + mapUtilityListings);
                
                // Change added for Workday by - Vaibhav - *****Ends***
                System.debug('setInvIds===> ' + setInvIds);
                for (Invoice__c invObj: Database.Query(CommonFunctionController.getSOQLQuery('Invoice__c', 'id IN : setInvIds'))) {
                    System.debug('invObj===> ' + invObj);
                    invMap.put(invObj.Id, invObj);
                    // Checking if there is atleast one Utility Type Invocie // 
                     System.debug('invObj.WorkType__c===> ' + invObj.WorkType__c);
                    // Insufficient Cross  ref entity issue -Added by Shital Rathod - start - 5-9-2018---
                        // comment Utilities if condition
                        if ('Utilities'.equals(invObj.WorkType__c)) {
                            setOpcoIds.add(invObj.OPCO_Code__c);
                            setVendorIds.add(invObj.Vendor__c);
                            invList.add(invObj);
                        }else{
                           setOpcoIds.add(invObj.OPCO_Code__c);
                           setVendorIds.add(invObj.Vendor__c); 
                            invList.add(invObj);
                        }
                    
                    // Insufficient Cross  ref entity issue issue -Added by Shital Rathod - END - 5-9-2018---
                }
                System.debug('invList ======> ' + invList);
                //Changed  to check with Set instead of boolean
                if (setOpcoIds.size() > 0 && setVendorIds.size() > 0) {
                    // Query Utility Vendor Mapping only if there is atleast one Utility type of Invocie //Changed the query
                    for (Utility_Vendor_Mapping__c utilVenObj: [SELECT id, OPCO_Code__c, Vendor_ID__c, GL_Account__c, Account_Number__c FROM Utility_Vendor_Mapping__c WHERE OPCO_Code__c IN: setOpcoIds AND Vendor_ID__c IN: setVendorIds]) {
                        mapKeyUtilityVendor.put(utilVenObj.OPCO_Code__c + '-' + utilVenObj.Vendor_ID__c, utilVenObj);
                    }
                }
                //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | END @@@@@@@//
                //Integer lineCnt = 1;
                //String currentInvId = '';
                //@@@@@ Added by Ravi - one line, Initiation of Invoice - for Workday Utilities
                Invoice__c inv = new Invoice__c();
                for (Invoice_Line_Item__c invl: Trigger.New) {

                    if (UtilityController.currentInvId != String.valueOf(invl.Invoice__c)) {
                        UtilityController.lineItemCnt = 1;
                        UtilityController.currentInvId = String.valueOf(invl.Invoice__c);
                    }
                    //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | BEGIN @@@@@@@//
                    if (invMap.get(invl.Invoice__c) != null && 'Utilities'.equals(invMap.get(invl.Invoice__c).WorkType__c)) {
                        try {
                            if (invMap.get(invl.Invoice__c).isDuplicate__c == false) {

                                // To update the spend category, cost center and profit center on Line---By Vaibhav---- Start
                                if (mapUtilityListings != null && 'Workday'.equals(invMap.get(invl.Invoice__c).ERP_Type__c) && (invl.Spend_Category__c == null && invl.Cost_Code__c == null && invl.Profit_Center__c == null)) {
                                    invl.Spend_Category__c = mapUtilityListings.get(invMap.get(invl.Invoice__c).Vendor__c).Spend_Categories__c;
                                    invl.Cost_Code__c = mapUtilityListings.get(invMap.get(invl.Invoice__c).Vendor__c).Cost_Center__c;
                                    invl.Profit_Center__c = mapUtilityListings.get(invMap.get(invl.Invoice__c).Vendor__c).Profit_Center__c;
                                     //@@@@@ Added by Ravi - Start - for Workday Utilities
                                    invl.Prepaid__c = 'N';
                                    inv = [Select Id,Name,Tax__c from Invoice__c where id =:  invl.Invoice__c limit 1 ];
                                    if(inv.Tax__c > 0)
                                       inv.Tax_Type_WD__c = 'ENTER_TAX_DUE';
                                    else
                                       inv.Tax_Type_WD__c = 'CALC_USE_TAX'; 
                                    update inv;
                                    //@@@@@ Added by Ravi - End - for Workday Utilities
                                }
                                // To update the spend category, cost center and profit center on Line---Vaibhav---- Ends

                                if (invl.GL_Code__c == null && !'Workday'.equals(invMap.get(invl.Invoice__c).ERP_Type__c)) {
                                    invl.GL_Code__c = mapKeyUtilityVendor.get(invMap.get(invl.Invoice__c).OPCO_Code__c + '-' + invMap.get(invl.Invoice__c).Vendor__c).GL_Account__c;
                                    invStatusMap.put(invl.Invoice__c, 'GL Coding Success');
                                }
                                /*else {
                                                                    // In case User Manually Stamped GL Code on Invoice Line, means GL Code is not null
                                                                    invStatusMap.put(invl.Invoice__c,'GL Coding Success');
                                                                }*/
                            } else {
                                // For Duplicate Invoice
                                invStatusMap.put(invl.Invoice__c, 'Duplicate Invoice');
                            }
                        }
                        Catch(Exception ex) {
                            // Here Null Pointer is catched from mapKeyUtilityVendor MAP.
                            invl.Exception_Reason__c = 'GL Coding Failed';
                            invStatusMap.put(invl.Invoice__c, 'GL Coding Failed');
                        }
                    }
                    //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | END @@@@@@@//
                    //Integer oldCount = Integer.valueOf(invMap.get(invl.Invoice__c).Line_Item_Count__c);
                    invl.Old_Quantity__c = invl.Quantity__c;
                    invl.Old_Quantity__c = invl.Old_Quantity__c != null ? invl.Old_Quantity__c : 0;
                    invl.Old_GRN_Line_Item__c = invl.GRN_Line_Item__c;

					system.debug('InvoiceLineCountMap:'+InvoiceLineCountMap);
                }
                //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | BEGIN @@@@@@@//
                if (invList.size() > 0) {
                    for (Invoice__c invObj: invList) {
                        if (invStatusMap != null && invStatusMap.size() > 0) {
                            invObj.User_Action__c = invStatusMap.get(invObj.id) != null ? invStatusMap.get(invObj.id) : null;
                        }
                        invObj.Line_Item_Count__c = invObj.Line_Item_Count__c != null ? invObj.Line_Item_Count__c : 0;
                        /*if(Trigger.isDelete){
invObj.Line_Item_Count__c = invObj.Line_Item_Count__c - (Trigger.new).size();
} else {
invObj.Line_Item_Count__c = invObj.Line_Item_Count__c + LineItemCountMap.get(invObj.Id);
}*/
                    }
                    system.debug('invList######'+invList);
                    update invList;
                }
                //@@@@@@@ Added by Kuldeep Singh | JIRA:CAS-178 & CAS-265 | 16-04-2018 | END @@@@@@@//
            }
            //--- Added by Dilshad | JIRA:ESMPROD-278 | 20-05-2016 | Starts ---//
            //--- Remaining Quantity Updation Logic | Starts ---//
            boolean enableQuantityCalculation = false;
            String rejectedState = '';
            Invoice_Configuration__c invConf = CommonFunctionController.getInvoiceConfiguration();
            enableQuantityCalculation = Boolean.valueOf(invConf.get(esmNameSpace + 'Enable_Quantity_Calculation__c'));
            rejectedState = String.valueOf(invConf.get(esmNameSpace + 'Invoice_Reject_Current_State__c'));
            /*for(System_Configuration__c sConf : sysConfigs){
if(sConf.Sub_Module__c.equals('Enable_Quantity_Calculation') && sConf.Value__c.equalsIgnoreCase('true'))
{
enableQuantityCalculation = true;
}
if(sConf.Sub_Module__c.equals('Invoice_Reject_Current_State'))
{
rejectedState = sConf.Value__c;
}
}*/
			//Added By Sonam | 12-09-2018 | CAS-1383 | Start
			if (Trigger.IsBefore) {
				if (Trigger.New != null) {
					for(Invoice_Line_Item__c invl : Trigger.New){
						setInvIds.add(invl.Invoice__c);
					}
				}
				System.debug('setInvIds===>2 ' + setInvIds);
                for (Invoice__c invObj: Database.Query(CommonFunctionController.getSOQLQuery('Invoice__c', 'id IN : setInvIds'))) {
                    invMap.put(invObj.Id, invObj);
				}
				if (Trigger.New != null) {
					for(Invoice_Line_Item__c invl : Trigger.New){
						if (invl.Invoice_Line_No__c != null) {
							invl.Invoice_Line_Item_No__c = invMap.get(invl.Invoice__c).Cora_Ref_No__c + '-' + invl.Invoice_Line_No__c;
						}
					}
				}
			}
			if (Trigger.IsAfter && (Trigger.IsInsert || Trigger.IsDelete)) {
				if (Trigger.Old != null) {
					for(Invoice_Line_Item__c invl : Trigger.Old){
						setInvIds.add(invl.Invoice__c);
						if (invl.Updated_By_Buyer__c) {
							UtilityController.isBuyerUser = true;
						}
					}
				}
				if (Trigger.New != null) {
					for(Invoice_Line_Item__c invl : Trigger.New){
						setInvIds.add(invl.Invoice__c);
						if (invl.Updated_By_Buyer__c) {
							UtilityController.isBuyerUser = true;
						}
					}
				}
				System.debug('setInvIds===>2 ' + setInvIds);
                for (Invoice__c invObj: Database.Query(CommonFunctionController.getSOQLQuery('Invoice__c', 'id IN : setInvIds'))) {
                    invMap.put(invObj.Id, invObj);
				}
				System.debug('invMap===>2 ' + invMap);
				System.debug('Last updated by  isBuyerUser--------'+UtilityController.isBuyerUser);
				if (!UtilityController.isBuyerUser) {
					List<Invoice_Line_Item__c> invlinelst = [SELECT Id,Name,Invoice_Line_No__c,Invoice_Line_Item_No__c,Invoice__c FROM Invoice_Line_Item__c WHERE Invoice__c IN:setInvIds];
					system.debug('invlinelst-size:'+invlinelst.size());
					for (Integer i=0;i<invlinelst.size();i++) {
						Invoice_Line_Item__c invl = invlinelst.get(i);
						invl.Invoice_Line_No__c = String.ValueOf(i+1);
						invl.Invoice_Line_Item_No__c = invMap.get(invl.Invoice__c).Cora_Ref_No__c + '-' + invl.Invoice_Line_No__c;
						system.debug('invl----------------------:'+i+':'+invl);
					}
					update invlinelst;
				}
            }
			//Added By Sonam | 12-09-2018 | CAS-1383 | End
            System.debug('enableQuantityCalculation --------' + enableQuantityCalculation);
            System.debug('rejectedState --------' + rejectedState);
            if (enableQuantityCalculation) {
                if (Trigger.IsAfter) {
                    System.debug('in afetr --------');
                    Map < Id, String > InvlGRNLQntyMap = new Map < Id, String > ();
                    Map < Id, String > InvlPolQntyMap = new Map < Id, String > ();

                    if (Trigger.isDelete) { // When Invoice Line Item is DELETED //
                        for (Invoice_Line_Item__c invl: Trigger.Old) {
                            if (invl.Quantity_Calculation_Required__c) {
                                if (invl.GRN_Line_Item__c != null) {
                                    if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                        system.debug('InvlGRNLQntyMap.get(invl.GRN_Line_Item__c):' + InvlGRNLQntyMap.get(invl.GRN_Line_Item__c));
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                                if (invl.PO_Line_Item__c != null) {

                                    if (InvlPolQntyMap.get(invl.PO_Line_Item__c) != null) {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c, (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                            }
                        }

                    } else if (Trigger.isInsert) { // When new Invoice Line Item is INSERTED //
                        System.debug('in is insert --------');
                        List < String > invoicesIds = new List < String > ();
                        for (Invoice_Line_Item__c invl: Trigger.New) {
                            system.debug('I invl.Invoice__c ------------' + invl.Invoice__c);
                            if (invl.Invoice__c != null) {
                                invoicesIds.add(invl.Invoice__c);
                            }
                        }
                        system.debug('I invoicesIds ------------' + invoicesIds);
                        MAP < ID, Invoice__c > invoices = new MAP < ID, Invoice__c > ([select id, Current_State__c, Previous_State__c from Invoice__c where id in: invoicesIds limit 999]);
                        system.debug('I invoices ------------' + invoices);
                        for (Invoice_Line_Item__c invl: Trigger.New) {
                            system.debug('invl.Quantity_Calculation_Required__c ------------' + invl.Quantity_Calculation_Required__c);
                            if (invl.Quantity_Calculation_Required__c) {
                                system.debug('I invl:::1::::' + invl);
                                if (invl.Invoice__c != null) {
                                    system.debug('I inv.invoice --------------' + invoices.get(invl.Invoice__c));
                                    system.debug('I inv.invoice.Current_State__c --------------' + invoices.get(invl.Invoice__c).Current_State__c);
                                }
                                if (invl.Invoice__c != null && invoices.get(invl.Invoice__c) != null && rejectedState != null && invoices.get(invl.Invoice__c).Current_State__c.equals(rejectedState)) {
                                    system.debug('I IIIFFFFFFFFF');
                                    if (invl.GRN_Line_Item__c != null) {
                                        if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, 0 + '~' + 0);
                                        }
                                    }
                                    if (invl.PO_Line_Item__c != null) {

                                        if (InvlPolQntyMap.get(invl.PO_Line_Item__c) != null) {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, 0 + '~' + 0);
                                        }
                                    }
                                } else {
                                    system.debug('I EEELLLLSSSSEEE');
                                    if (invl.GRN_Line_Item__c != null) {

                                        if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                            system.debug('InvlGRNLQntyMap.get(invl.GRN_Line_Item__c):' + InvlGRNLQntyMap.get(invl.GRN_Line_Item__c));
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, ((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + 0);
                                        } else {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (invl.Quantity__c != null ? invl.Quantity__c : 0) + '~' + 0);
                                        }
                                    }

                                    if (invl.PO_Line_Item__c != null) {
                                        if (InvlPolQntyMap.get(invl.PO_Line_Item__c) != null) {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, ((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, (invl.Quantity__c != null ? invl.Quantity__c : 0) + '~' + 0);
                                        }
                                    }
                                }
                            }
                        }
                    } else if (Trigger.isUpdate) { // When Invoice Line Item is UPDATED //
                        List < String > invoicesIds = new List < String > ();
                        for (Invoice_Line_Item__c invl: Trigger.New) {
                            system.debug('I invl.Invoice__c ------------' + invl.Invoice__c);
                            if (invl.Invoice__c != null) {
                                invoicesIds.add(invl.Invoice__c);
                            }
                        }
                        system.debug('I invoicesIds ------------' + invoicesIds);
                        MAP < ID, Invoice__c > invoices = new MAP < ID, Invoice__c > ([select id, Current_State__c, Previous_State__c from Invoice__c where id in: invoicesIds limit 999]);
                        system.debug('I invoices ------------' + invoices);
                        for (Invoice_Line_Item__c invl: Trigger.New) {
                            system.debug('U invl:::1::::' + invl);
                            if (invl.Quantity_Calculation_Required__c) {
                                if (Trigger.isUpdate && invl.Invoice__c != null && invoices.get(invl.Invoice__c) != null && rejectedState != null && invoices.get(invl.Invoice__c).Current_State__c.equals(rejectedState) && !(invoices.get(invl.Invoice__c).Current_State__c.equals(invoices.get(invl.Invoice__c).Previous_State__c))) {

                                    if (invl.GRN_Line_Item__c != null) {
                                        if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                        }
                                    }
                                    if (invl.PO_Line_Item__c != null) {

                                        if (InvlPolQntyMap.get(invl.PO_Line_Item__c) != null) {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                        }
                                    }


                                } else {
                                    if (invl.GRN_Line_Item__c != null) {

                                        if (invl.GRN_Line_Item__c == Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) {
                                            if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                                InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, ((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                            } else {
                                                InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (invl.Quantity__c != null ? invl.Quantity__c : 0) + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                            }
                                        } else if (invl.GRN_Line_Item__c != Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) {
                                            if (InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null) {
                                                InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, ((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                            } else {
                                                InvlGRNLQntyMap.put(invl.GRN_Line_Item__c, (invl.Quantity__c != null ? invl.Quantity__c : 0) + '~' + 0);
                                            }

                                            if (Trigger.oldMap.get(invl.Id).GRN_Line_Item__c != null) {
                                                if (InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) != null) {
                                                    InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c, (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[1])));
                                                } else {
                                                    InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                                }
                                            }
                                        }
                                    } else if (invl.GRN_Line_Item__c == null && Trigger.oldMap.get(invl.Id).GRN_Line_Item__c != null) {
                                        if (InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) != null) {
                                            InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c, 0 + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c, 0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                        }
                                    }

                                    if (invl.PO_Line_Item__c != null) {
                                        if (InvlPolQntyMap.get(invl.PO_Line_Item__c) != null) {

                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, ((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                        } else {
                                            InvlPolQntyMap.put(invl.PO_Line_Item__c, (invl.Quantity__c != null ? invl.Quantity__c : 0) + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    List < GRN_Line_Item__c > grnLineItemUpdateList = new List < GRN_Line_Item__c > ();
                    if (InvlGRNLQntyMap.size() > 0) {
                        if (GRN_Line_Item__c.getSObjectType().getDescribe().isAccessible() &&
                            Schema.sObjectType.GRN_Line_Item__c.fields.Id.isAccessible() &&
                            Schema.sObjectType.GRN_Line_Item__c.fields.Name.isAccessible() &&
                            Schema.sObjectType.GRN_Line_Item__c.fields.ESM_Remaining_Quantity__c.isAccessible() &&
                            Schema.sObjectType.GRN_Line_Item__c.fields.PO_Line_Item__c.isAccessible()) {
                            grnLineItemUpdateList = [SELECT Id, Name, ESM_Remaining_Quantity__c, PO_Line_Item__c FROM GRN_Line_Item__c WHERE Id IN: InvlGRNLQntyMap.keySet()];
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'GRN_Line_Item__c'));
                        }
                        if (grnLineItemUpdateList != null && grnLineItemUpdateList.size() > 0) {
                            for (GRN_Line_Item__c grnl: grnLineItemUpdateList) {
                                if (InvlGRNLQntyMap.get(grnl.Id) != null) {
                                    if (grnl.ESM_Remaining_Quantity__c == null) {
                                        grnl.ESM_Remaining_Quantity__c = 0;
                                    }
                                    grnl.ESM_Remaining_Quantity__c = grnl.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlGRNLQntyMap.get(grnl.Id).split('~')[0]) + Decimal.ValueOf(InvlGRNLQntyMap.get(grnl.Id).split('~')[1]);
                                }
                            }
                        }
                    }
                    List < PO_Line_Item__c > poLineItemUpdateList = new List < PO_Line_Item__c > ();
                    if (InvlPolQntyMap.size() > 0) {
                        if (PO_Line_Item__c.getSObjectType().getDescribe().isAccessible() &&
                            Schema.sObjectType.PO_Line_Item__c.fields.Id.isAccessible() &&
                            Schema.sObjectType.PO_Line_Item__c.fields.Name.isAccessible() &&
                            Schema.sObjectType.PO_Line_Item__c.fields.ESM_Remaining_Quantity__c.isAccessible()) {
                            poLineItemUpdateList = [SELECT Id, Name, ESM_Remaining_Quantity__c FROM PO_Line_Item__c WHERE Id IN: InvlPolQntyMap.keySet()];
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'PO_Line_Item__c'));
                        }
                        if (poLineItemUpdateList != null && poLineItemUpdateList.size() > 0) {
                            for (PO_Line_Item__c pol: poLineItemUpdateList) {
                                if (InvlPolQntyMap.get(pol.Id) != null) {
                                    if (pol.ESM_Remaining_Quantity__c == null) {
                                        pol.ESM_Remaining_Quantity__c = 0;
                                    }
                                    pol.ESM_Remaining_Quantity__c = pol.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlPolQntyMap.get(pol.Id).split('~')[0]) + Decimal.ValueOf(InvlPolQntyMap.get(pol.Id).split('~')[1]);
                                }
                            }
                        }
                    }
                    system.debug('poLineItemUpdateList:' + poLineItemUpdateList);
                    if (grnLineItemUpdateList.size() > 0) {
                        if (GRN_Line_Item__c.getSObjectType().getDescribe().isUpdateable() &&
                            Schema.sObjectType.GRN_Line_Item__c.fields.ESM_Remaining_Quantity__c.isUpdateable()) {
                            update grnLineItemUpdateList;
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'GRN_Line_Item__c'));
                        }
                    }

                    if (poLineItemUpdateList.size() > 0) {
                        if (PO_Line_Item__c.getSObjectType().getDescribe().isUpdateable() &&
                            Schema.sObjectType.PO_Line_Item__c.fields.ESM_Remaining_Quantity__c.isUpdateable()) {
                            update poLineItemUpdateList;
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'PO_Line_Item__c'));
                        }
                    }

                }
            }
            /*
//--- Remaining Quantity Updation Logic | Ends ---//
if(Trigger.IsBefore && Trigger.isInsert) {

//---- Auto Matching | Starts ---//
if(invConf.Auto_Match_Flow_Method__c != null && invConf.Auto_Match_Flow_Method__c != ''){

boolean skipAutoMatch = false;
Set<string> poSet = new set<string>();
for(Invoice_Line_Item__c invl : Trigger.New){
system.debug('invl:'+invl);
if(invl.PO_Line_Item__c != null || invl.Purchase_Order__c == null){
// When PO Line Item is already mapped (i.e. when it Invoice Line Item is create in ESM) then there is no need of auto match
// When Purchase Order is not mapped on Invoice Line Item (i.e. in case of NON PO Invoice) then there is no need of auto match
skipAutoMatch = true;
break;
}
poSet.add(invl.Purchase_Order__c);
}
if(!skipAutoMatch){
Map<String,string> polinvlMap = new Map<String,string>();    
Map<string,string> InvMap = new Map<string,string>();
Map<string,set<string>> InvPOMap = new Map<string,set<string>>();
string polQuery = 'SELECT Id,Name';
List<PO_GRN_Line_Invoice_Line_Mappings__c> cusmdt = [select Invoice_Line_Item_Field_Name__c, PO_GRN_Line_Item_Field_Name__c from PO_GRN_Line_Invoice_Line_Mappings__c];


for (PO_GRN_Line_Invoice_Line_Mappings__c cml : cusmdt) {
if(cml.Used_In_Auto_Match__c){
polinvlMap.put(String.valueOf(cml.PO_GRN_Line_Item_Field_Name__c), String.valueOf(cml.Invoice_Line_Item_Field_Name__c));
polQuery += ',' + cml.PO_GRN_Line_Item_Field_Name__c;
}
}
if(!polQuery.containsIgnoreCase('Purchase_Order__c')){
polQuery += ','+esmNameSpace+'Purchase_Order__c';
}
polQuery += ','+esmNameSpace+'GRN_Match_Required_Formula__c,PO_Line_No__c';
polQuery += ' FROM '+esmNameSpace+'PO_Line_Item__c WHERE '+esmNameSpace+'Purchase_Order__c IN:poSet'; 

List<PO_Line_Item__c> polList = new List<PO_Line_Item__c>();
if(UtilityController.isFieldAccessible(esmNameSpace+'PO_Line_Item__c',null)){
polList = database.query(polQuery);
}
List<GRN_Line_Item__c> grnLineItemList = new List<GRN_Line_Item__c>();
if(UtilityController.isFieldAccessible(esmNameSpace+'GRN_Line_Item__c',null)){
grnLineItemList = [SELECT Id,Name,PO_Line_Item__c,Purchase_Order__c,GRN__c,PO_Line_Item__r.GRN_Match_Required_Formula__c FROM GRN_Line_Item__c WHERE Purchase_Order__c IN:poSet];
}

Map<String,String> GRNLineMap = new Map<String,String>();
Map<String,String> GRNMap = new Map<String,String>();
if(grnLineItemList != null && grnLineItemList.size() > 0){
for(GRN_Line_Item__c grnl : grnLineItemList){
if(grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c != null && grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c){
if(GRNLineMap.get(grnl.PO_line_Item__c)!= null){
GRNLineMap.put(grnl.PO_line_Item__c,GRNLineMap.get(grnl.PO_line_Item__c)+'~'+grnl.id);
GRNMap.put(grnl.PO_line_Item__c,GRNMap.get(grnl.PO_line_Item__c)+'~'+grnl.GRN__c);
}
else{
GRNLineMap.put(grnl.PO_line_Item__c,grnl.id);
GRNMap.put(grnl.PO_line_Item__c,grnl.GRN__c);
}
}
}
}
set<string> cPOSet = new set<string>();
for(Invoice_Line_Item__c invl : Trigger.New){
if(InvPOMap.get(invl.Invoice__c) != null){
cPOSet = InvPOMap.get(invl.Invoice__c);
cPOSet.add(invl.Purchase_Order__c);
InvPOMap.put(invl.Invoice__c,cPOSet);
} else {
cPOSet = new set<string>();
cPOSet.add(invl.Purchase_Order__c);
InvPOMap.put(invl.Invoice__c,cPOSet);
}
if(invl.PO_Line_Item__c == null){
invl.Is_Mismatch__c = true;
boolean Matched = true;

if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Sequence')){
for(String polFieldName:polinvlMap.keySet()){
String invlFieldName = polinvlMap.get(polFieldName);
for(PO_Line_Item__c pol:polList){
Matched = false;
if(pol.Purchase_Order__c == invl.Purchase_Order__c){
if(pol.get(polFieldName) != null && pol.get(polFieldName).equals(invl.get(invlFieldName))){
Matched = true;                                         
}
if(Matched){
invl.PO_Line_Item__c = pol.Id;
break;                                      
}
}
}
if(invl.PO_Line_Item__c != null){
break;                                      
}
}
}
else if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Parallel')){
for(PO_Line_Item__c pol:polList){
Matched = true;
if(pol.Purchase_Order__c == invl.Purchase_Order__c){
for(String polFieldName:polinvlMap.keySet()){
String invlFieldName = polinvlMap.get(polFieldName);
system.debug('pol.get(polFieldName):'+pol.get(polFieldName));
system.debug('invl.get(invlFieldName):'+invl.get(invlFieldName));
if((pol.get(polFieldName) != null && invl.get(invlFieldName) == null) || (pol.get(polFieldName) == null && invl.get(invlFieldName) != null)){
Matched = false;
}
else if(pol.get(polFieldName) != null && !pol.get(polFieldName).equals(invl.get(invlFieldName))){
Matched = false;
}
}
if(Matched){
invl.PO_Line_Item__c = pol.Id;
if(invl.Invoice_Line_Item_No__c == null){
invl.Invoice_Line_Item_No__c = pol.Purchase_Order__c + '~' + pol.PO_Line_No__c + '~' + CommonFunctionController.getRandomnumber(10);
}
break;                                      
}
}
}
}
if(invl.PO_Line_Item__c != null){
invl.Is_Mismatch__c = false;
if(InvMap.get(invl.Invoice__c) == null || (InvMap.get(invl.Invoice__c) != null && InvMap.get(invl.Invoice__c) != 'Fail')){
InvMap.put(invl.Invoice__c, 'Pass');
} 
if(GRNLineMap != null && GRNLineMap.size() > 0 && GRNLineMap.get(invl.PO_Line_Item__c) != null && !GRNLineMap.get(invl.PO_Line_Item__c).contains('~')){
invl.GRN_Line_Item__c = GRNLineMap.get(invl.PO_Line_Item__c);
invl.GRN__c = GRNMap.get(invl.PO_Line_Item__c);
}
try{
if(invl.Quantity__c != null){
if(UserInfo.isMultiCurrencyOrganization() && invl.Quantity__c != null && invl.Rate_Currency__c != null){
invl.Amount_Currency__c = invl.Quantity__c * invl.Rate_Currency__c.setscale(3,System.RoundingMode.HALF_UP);
} else if(!UserInfo.isMultiCurrencyOrganization() && invl.Quantity__c != null && invl.Rate__c != null){
invl.Amount__c = invl.Quantity__c * invl.Rate__c.setscale(3,System.RoundingMode.HALF_UP);
}
}
} catch(exception ex){}
}
else {
invl.Exception_Reason__c = 'PO Line Not Found';
invl.Is_Mismatch__c = true;
InvMap.put(invl.Invoice__c, 'Fail');
if(invl.Invoice_Line_Item_No__c == null){
invl.Invoice_Line_Item_No__c = invl.Purchase_Order__c + '~null~' + CommonFunctionController.getRandomnumber(10);
}
}
}

}

if(InvMap != null && InvMap.size() > 0){
List<Invoice__c> invList = new List<Invoice__c>();
if(UtilityController.isFieldAccessible(esmNameSpace+'Invoice__c','Id,Name,'+esmNameSpace+'Exception_Reason__c,'+esmNameSpace+'User_Actions__c,'+esmNameSpace+'Current_State__c')){
invList = [SELECT Id,Name,Exception_Reason__c,User_Action__c,Current_State__c FROM Invoice__c WHERE Id IN:InvMap.keySet()];
}
if(invList != null && invList.size() > 0){
for(Invoice__c inv:InvList){
if(InvMap.get(inv.Id).equalsIgnoreCase('Fail') || (inv.Exception_Reason__c != null && inv.Exception_Reason__c.contains('PO Line Not Found'))){
if(inv.Exception_Reason__c != null && !inv.Exception_Reason__c.contains('PO Line Not Found')){
inv.Exception_Reason__c = inv.Exception_Reason__c+';PO Line Not Found';
} else {
inv.Exception_Reason__c = 'PO Line Not Found';
}
inv.User_Action__c = 'Auto Match Fail';
} else {
inv.User_Action__c = 'Auto Match Pass';
}
}
system.debug('InvMap:'+InvMap);
if(Invoice__c.getSObjectType().getDescribe().isUpdateable()
&& Schema.sObjectType.Invoice__c.fields.Exception_Reason__c.isAccessible()
&& Schema.sObjectType.Invoice__c.fields.User_Action__c.isAccessible()){
update invList;
} else {
throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice__c'));
}
}
}
//--- Update InvoicePOMatrix ---//
if(InvPOMap != null && InvPOMap.size() > 0){
List<Invoice_PO_Detail__c> invPoList = new List<Invoice_PO_Detail__c>();
for(String inv: InvPOMap.keySet()){
for(String po: InvPOMap.get(inv)){
Invoice_PO_Detail__c invpoObj = new Invoice_PO_Detail__c();
invpoObj.Invoice__c = inv;
invpoObj.Purchase_Order__c = po;
invPoList.add(invpoObj);
}
}
if(invPoList != null && invPoList.size() > 0){
if(Invoice_PO_Detail__c.getSObjectType().getDescribe().isCreateable()
&& Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isCreateable()
&& Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isCreateable()){
insert invPoList;
} else {
throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
}
}
}
}
}
//---- Auto Matching | Ends ---//

}*/
        }
    } catch (exception ex) {
        Trigger.New[0].addError(UtilityController.customDebug(ex, 'InvoiceLineItemMasterTrigger'));
    }
}