trigger updateVendorId on Purchase_Order__c  (before insert,before Update)  {
    List<Account> accList = new List<Account>();
    Purchase_Order__c Pord;  
    String isrecordfound = '';
    Set<String> venIdkeys = new Set<String>();
    Map<String, Purchase_Order__c> vendorIds = new Map<String, Purchase_Order__c>();
    
    for(Purchase_Order__c po:trigger.new){
        vendorIds.put(po.Vendor_Number__c,po);            
    }    
    
    system.debug('vendorIds :' + vendorIds);
    system.debug('vendorIds Size :' + vendorIds.size());
    
    accList = [Select Id,Unique_Key__c,Vendor_No__c from Account where Vendor_No__c IN: vendorIds.keyset()];
     system.debug('accList.size() :' + accList.size());
     
    
    for(Purchase_Order__c po:trigger.new){
        if(po.Source_System__c == 'Workday'){
            isrecordfound = 'N';
            if(po.Vendor_Number__c != null){
                
                for(Account acc: accList ){
                    if(acc.Unique_Key__c != null && acc.Unique_Key__c != ''){
                        if(acc.Vendor_No__c == po.Vendor_Number__c){
                            isrecordfound = 'Y' ;
                            System.debug('acc.Unique_Key__c:' + acc.Unique_Key__c);
                            po.Vendor__c = acc.Id; 
                            break;
                        }
                    }                     
                }
                System.debug('In first if condition');
                if(isrecordfound == 'N'){
                    po.Vendor_NotFound_InVMD__c = '';
                    System.debug('No Related Vendor Num');
                }else{
                    System.debug('Found');
                }
            }
            else{
                 po.Vendor_NotFound_InVMD__c = '';
                 System.debug('No Related Vendor Num, No CSV Vendor Id is blank');
            }
        }
    }
}