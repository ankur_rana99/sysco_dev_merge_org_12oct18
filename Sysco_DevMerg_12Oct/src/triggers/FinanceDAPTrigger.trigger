trigger FinanceDAPTrigger on Finance_DAP__c (after insert, after update) {
    //After insert and update - DOA should be recalculated and DOA Metrix Table will be deleted first and then repopulated by below batch class
    Database.executeBatch(new DoADataServiceBatch(), 2000);
}