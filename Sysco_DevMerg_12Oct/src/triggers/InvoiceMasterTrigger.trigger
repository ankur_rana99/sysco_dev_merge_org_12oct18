trigger InvoiceMasterTrigger on Invoice__c(before insert, before update, after insert, after update) {
    try {
    
        // -------- Added by Hitakshi | 23-08-2018 | JIRA CAS-1354 | Ends ------ //
        Trigger_Configuration__c tc;
        if (Trigger_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
          tc = Trigger_Configuration__c.getInstance(Userinfo.getUserid());
        } else {
          tc = Trigger_Configuration__c.getOrgDefaults();
        }
        // -------- Added by Hitakshi | 23-08-2018 | JIRA CAS-1354 | Ends ------ //
        String esmNameSpace = UtilityController.esmNameSpace;
        Map<String, string> sysConfMap = new Map<String, string> ();

        Map<Boolean, string> errorMap = new Map<Boolean, string> ();
        //--- Added by Dilshad | Populating Vendor on invoice based on Vendor_No__c | Starts ---//
        if(UtilityController.VendorMap == null){
            UtilityController.VendorMap = new Map<String, Account>();
            for(Account venObj:[SELECT Id, Vendor_No__c FROM Account LIMIT 50000]){
                UtilityController.VendorMap.put(venObj.Vendor_No__c, venObj);
            }
        }
        //--- Added by Dilshad | Populating Vendor on invoice based on Vendor_No__c | Ends ---//
        //--- Added by Dilshad | Populating GL_Account_Number__c on invoice based on gl_code_text__c | Starts ---//
        if(UtilityController.GLCodeMap == null){
            UtilityController.GLCodeMap = new Map<String, GL_Code__c>();
            for(GL_Code__c glCode:[SELECT Id, Name FROM GL_Code__c LIMIT 50000]){
                UtilityController.GLCodeMap.put(glCode.Name, glCode);
            }
        }
        //GL_Account_Number__c
        //--- Added by Dilshad | Populating GL_Account_Number__c on invoice based on gl_code_text__c | Ends ---//
        //--- Added by Dilshad | Populating OPCO_Code__c on invoice based on OPCO__c | Starts ---//
        if(UtilityController.OpCoCodeMap == null){
            UtilityController.OpCoCodeMap = new Map<String, OPCO_Code__c>();
            for(OPCO_Code__c opco:[SELECT Id, Name, OPCO_Code__c FROM OPCO_Code__c LIMIT 50000]){
                UtilityController.OpCoCodeMap.put(opco.OPCO_Code__c, opco);
            }
        }
        //--- Added by Dilshad | Populating OPCO_Code__c on invoice based on OPCO__c | Ends ---//
        
        if (Trigger.isBefore) {
            if(UtilityController.mapUserMasters == null){
                UtilityController.mapUserMasters = new map<Id, User_Master__c>([SELECT Id, Name, User_Type__c FROM User_Master__c]);
            }
            // For updating duplicate invoices
            /*Set<String> setDuplicateInvoices = InvoiceValidationExceptionController.setDuplicateCheckForInvoices(trigger.New);
            for(Invoice__c invoice : Trigger.new){
                if(setDuplicateInvoices.contains(invoice.Id)){
                    invoice.isDuplicate__c = true;
                    if(invoice.Last_Modified_By__c != null && UtilityController.mapUserMasters.get(invoice.Last_Modified_By__c) != null && !String.isBlank(UtilityController.mapUserMasters.get(invoice.Last_Modified_By__c).User_Type__c) && UtilityController.mapUserMasters.get(invoice.Last_Modified_By__c).User_Type__c == 'Requestor/Buyer'){
                        invoice.addError(System.Label.Duplicate_Invoice);
                    }
                }
            }*/
            if(UtilityController.sysConfMap == null){
                UtilityController.sysConfMap = new Map<String, String>();
                for (System_Configuration__c sysConf :[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c]) {
                    UtilityController.sysConfMap.put(sysConf.Module__c + '|' + sysConf.Sub_Module__c, sysConf.Value__c);
                }
            }
            //--- Capture Invoice Comments ---//
            errorMap = CommonFunctionController.insertCommentHistorySysco(Trigger.New, esmNameSpace + 'Invoice__c', esmNameSpace, UtilityController.sysConfMap);

            //--- Capture Exception History ---//
            if (!UtilityController.exceptionHistoryCaptured) {
                errorMap = CommonFunctionController.InsertExceptionHistory(Trigger.New, esmNameSpace, UtilityController.sysConfMap);
                UtilityController.exceptionHistoryCaptured = true;
            }
            if (errorMap.keySet().contains(true)) {
                Trigger.New[0].addError(errorMap.get(true));
            }
        }
        //--- Capture Invoice History ---//
        if(Trigger.isAfter) {
            /*  System.debug('UtilityController.contentDocumentLinkList-----' + UtilityController.contentDocumentLinkList);
            if (UtilityController.contentDocumentLinkList != null && UtilityController.contentDocumentLinkList.size() > 0)
            {
                upsert UtilityController.contentDocumentLinkList;
            } */
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//  
            triggerHelper.captureRecordHistory(Trigger.New, Trigger.old, Trigger.isInsert, Trigger.isUpdate, Trigger.newMap, Trigger.oldMap, 'Invoice__c', UtilityController.sysConfMap);
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
            // Utilities Update
            set<Id> utilitySet = new set<Id>();
            // Save button Issue Cross Reference ID exception & Attchment not saved issue----SHital Rathod--- changes start
            List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink> ();
            for(Invoice__c inv : Trigger.new){
                if(UtilityController.iscontentinserted != true){ 
                    // throwing null exception, so placing a null check - Vaibhav
                    if(inv != null && inv.ContentVersionIDS__c != null){ 
                        for (string cvid : inv.ContentVersionIDS__c.split(',')) {
                            System.debug('inv.ContentVersionIDS__c===' + cvid);
                            contentDocumentLinkList.add(new ContentDocumentLink(
                                                                            ContentDocumentId = cvid,
                                                                            LinkedEntityId = inv.Id,
                                                                            ShareType = 'V',
                                                                            Visibility = 'AllUsers'));
                        }
                    }
                }            
                // Populating Utilities ID in SET
                if('Utilities'.equals(inv.Worktype__c)){
                    utilitySet.add(inv.Id);
                }
                    
            }
            if(contentDocumentLinkList.size() > 0){
                if (UtilityController.isFieldUpdateable('ContentDocumentLink', null) && UtilityController.isFieldCreateable('ContentDocumentLink', null)) {
                    System.debug('ContentDocumentLinks*****' + contentDocumentLinkList);
                    upsert contentDocumentLinkList; 
                    UtilityController.iscontentinserted = true;
                }
            } 
            // Save button Issue Cross Reference ID exception & Attchment not saved issue----SHital Rathod--- changes end
            if(!utilitySet.IsEmpty() && !UtilityController.utilityRunning){
                //updatig Invoice Line
                UtilityController.utilityRunning = true;
                System.debug('Going to Update Utilities Lines :');
                update [SELECT Id, Name FROM Invoice_Line_Item__c WHERE Invoice__c IN : utilitySet];
            }
        }
        //--- Start | Entrying data in Invoice PO Detail ---//
        if (Trigger.isBefore) {
            UtilityController.InvPOMap = new Map<String, string> ();
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//
            if(triggerHelper.setDuplicateInvoices == null) {
                triggerHelper.setDuplicateInvoices = InvoiceValidationExceptionController.setDuplicateCheckForInvoices(Trigger.New);
            }
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
            for (Invoice__c obj : Trigger.New) {
                //--- Added by Dilshad | Populating Vendor on invoice based on Vendor_No__c | Starts ---//
                if(obj.Vendor__c == null && obj.Vendor_No__c != null) {
                    if(UtilityController.VendorMap.get(obj.Vendor_No__c) != null) {
                        obj.Vendor__c = UtilityController.VendorMap.get(obj.Vendor_No__c).Id;
                    } else {
                        obj.addError(Label.Vendor_not_found_in_CORA);
                    }
                } 
                //--- Added by Dilshad | Populating Vendor on invoice based on Vendor_No__c | Ends ---//
                //--- Added by Dilshad | Populating GL_Account_Number__c on invoice based on gl_code_text__c | Starts ---//
                if(obj.GL_Account_Number__c == null && obj.gl_code_text__c != null) {
                    if(UtilityController.GLCodeMap.get(obj.gl_code_text__c) != null) {
                        obj.GL_Account_Number__c = UtilityController.GLCodeMap.get(obj.gl_code_text__c).Id;
                    } else {
                        obj.addError(Label.GL_Code_not_found_in_CORA);
                    }
                } 
                //--- Added by Dilshad | Populating GL_Account_Number__c on invoice based on gl_code_text__c | Ends ---//
                //--- Added by Dilshad | Populating OPCO_Code__c on invoice based on OPCO__c | Starts ---//
                if(obj.OPCO_Code__c == null && obj.OPCO__c != null) {
                    if(UtilityController.OpCoCodeMap.get(obj.OPCO__c) != null) {
                        obj.OPCO_Code__c = UtilityController.OpCoCodeMap.get(obj.OPCO__c).Id;
                    } else {
                        obj.addError(Label.OpCo_Code_not_found_in_CORA);
                    }
                } 
                //--- Added by Dilshad | Populating OPCO_Code__c on invoice based on OPCO__c | Ends ---//
                //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//
                if(triggerHelper.setDuplicateInvoices.contains(obj.Id)){
                //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
                    obj.isDuplicate__c = true;
                    if(obj.Last_Modified_By__c != null && UtilityController.mapUserMasters.get(obj.Last_Modified_By__c) != null && !String.isBlank(UtilityController.mapUserMasters.get(obj.Last_Modified_By__c).User_Type__c) && UtilityController.mapUserMasters.get(obj.Last_Modified_By__c).User_Type__c == 'Requestor/Buyer'){
                        if(!String.isBlank(obj.User_Action__c) && obj.User_Action__c != 'Reject' && obj.User_Action__c != 'Send Back to AP'){
                            obj.addError(System.Label.Duplicate_Invoice);
                        }
                    }
                }
                System.debug('obj.Purchase_Order__c:' + obj.Purchase_Order__c);
                if (obj.Purchase_Order__c != null) {
                    UtilityController.InvPOMap.put(obj.Invoice_No__c + '~' + obj.Vendor__c + '~' + obj.Total_Amount__c, obj.Purchase_Order__c);
                    obj.PO_Record_Id__c = obj.Purchase_Order__c;
                    obj.Purchase_Order__c = null;
                }
                // -------Shital changes start --- for CASP2-33----start--  
                if(obj.Supplier_Work_Type__c != null && obj.Supplier_Work_Type__c != ''){
                    obj.WorkType__c= obj.Supplier_Work_Type__c;
                }
                if(obj.Supplier_Document_Type__c!= null && obj.Supplier_Document_Type__c!= ''){
                    obj.Document_Type__c= obj.Supplier_Document_Type__c;
                }
                // ------Shital changes --- for CASP2-33 --ends---
                
                //--- Added by Vishwa for CASP2-10 |start --------||commented CASP2-10 code as created formula--//
              /*--  if('Vouched'.equals(obj.Current_State__c))
                {
                    if('Paid'.equals(obj.Invoice_Status__c))
                        obj.Supplier_Status__c='Paid';
                    else
                        obj.Supplier_Status__c='In Process';
                }
                else if('Rejected'.equals(obj.Current_State__c))
                {
                    obj.Supplier_Status__c='Rejected';
                }
                else
                {
                    obj.Supplier_Status__c='In Process';
                } --*/
                //--- Added by Vishwa for CASP2-10 |start --------//
            }
        }

        if (Trigger.isAfter && UtilityController.InvPOMap != null && UtilityController.InvPOMap.size() > 0) {
            List<Invoice_PO_Detail__c> InvPODetailToInsert = new List<Invoice_PO_Detail__c> ();

            for (Invoice__c invObj : Trigger.New) {
                Invoice_PO_Detail__c invPOObj = new Invoice_PO_Detail__c();
                invPOObj.Invoice__c = invObj.id;
                invPOObj.Purchase_Order__c = UtilityController.InvPOMap.get(invObj.Invoice_No__c + '~' + invObj.Vendor__c + '~' + invObj.Total_Amount__c);
                InvPODetailToInsert.add(invPOObj);

            }
            if (InvPODetailToInsert.size() > 0) {
                if (Invoice_PO_Detail__c.getSObjectType().getDescribe().isCreateable()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isCreateable()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isCreateable()) {
                    if (Invoice_PO_Detail__c.getSObjectType().getDescribe().isUpdateable()
                        && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isUpdateable()
                        && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isUpdateable()) {
                        upsert InvPODetailToInsert;
                    } else {
                        Trigger.New[0].addError(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                    }
                } else {
                    Trigger.New[0].addError(UtilityController.getCreateExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                }
            }
        }
        //--- Ends | Entrying data in Invoice PO Detail ---//
        
        //---- Vendor hierarchy setup by Vishwa | CASP2-8
        if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
            System.debug('---------in if----------------'+UtilityController.sysConfMap.get('Vendor_Master|Share_Invoice_With_Vendor'));
           // Insuffiecient Creoss  ref entitity issue -Added by Shital Rathod - start - 5-9-2018---
           // REmoved triggerHelper.sharedWithVendor from if condition - by Shital Rathod
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//    
            if (UtilityController.sysConfMap.get('Vendor_Master|Share_Invoice_With_Vendor') != null && UtilityController.sysConfMap.get('Vendor_Master|Share_Invoice_With_Vendor') == 'true') {
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
            // Insuffiecient Creoss  ref entitity issue -Added by Shital Rathod - End - 5-9-2018---
            
                List<SObject> insertShareObj = new List<SObject> ();
               
                Set<Id> parentId = Trigger.NewMap.keySet();
                System.debug('parentId---'+parentId);
                List<Invoice__share> deleteShareObj = Database.query('SELECT Id FROM Invoice__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');
                 //===== Save Button LImit Issue added by SHITAL RATHOD====start=  
                 system.debug('deleteShareObj---'+deleteShareObj);
                 set<String> vendorId = new Set<String> ();
                List<String> vendorNo = new List<String> ();
				set<String> sourcesystem = new Set<String> ();
 
                // Insuffiecient Creoss  ref entitity issue -Added by Shital Rathod - start - 5-9-2018---

                for (Invoice__c obj : Trigger.New) {
                    if (obj.Vendor__c != null) {
                        //vendorId.add(obj.Vendor__c);
                        vendorNo.add(obj.Vendor_No__c);
                        //sourcesystem.add(obj.Vendor__r.Source_System__c);
                     }  
                 }
                system.debug('vendorNo-----'+vendorNo);
				system.debug('sourcesystem-----'+sourcesystem);
               /* if(UtilityController.vendorID == null){
                    UtilityController.vendorID = new set<ID>();
                     for (Invoice__c obj : Trigger.New) {
                         UtilityController.vendorID.add(obj.Vendor__c);
                     }
                     system.debug('UtilityController.vendorID--in IF-'+UtilityController.vendorID);
                }*/
               // Insuffiecient Creoss  ref entitity issue -Added by Shital Rathod - End - 5-9-2018---
                Map<String,  set<string>> mapObjIdvendorId = new Map<String,  set<string>> ();
                Map<ID, Account> allAccounts = new Map<ID, Account>([SELECT Id, Parent_Vendor__c,OPCO_Code__c,Vendor_No__c,Unique_Key__c,Source_System__c FROM Account where Vendor_No__c in: vendorNo limit 1000]);
                System.debug('allAccounts==== '+allAccounts);
               //===== Save Button LImit Issue added by SHITAL RATHOD====end=;
			   For(Account str : [SELECT Id, Parent_Vendor__c,OPCO_Code__c,Vendor_No__c,Unique_Key__c,Source_System__c FROM Account where Vendor_No__c in: vendorNo]){
					vendorId.add(str.Id);
				 }
				 System.debug('vendorId==== '+vendorId);
                for (Invoice__c obj : Trigger.New) {
                    
                    Id vendor=obj.Vendor__c;
                     System.debug('vendor&&&'+vendor);
                    // Insufficient Cross  ref entity issue issue -Added by Shital Rathod - start - 5-9-2018---
                        /*    for(;vendor!=null;)
                            {
                            if (obj.Vendor__c != null) {
                                vendorId.add(vendor);
                                Account Acc = allAccounts.get(vendor);
                             Account Acc = Database.query('SELECT Id, Parent_Vendor__c FROM Account WHERE Id=:vendor');
                               vendor = Acc.Parent_Vendor__c;
                                System.debug('Parent_Vendor__c '+vendor);
                            }
                                */
                     // Insufficient Cross  ref entity issue -Added by Shital Rathod - End - 5-9-2018---
                     mapObjIdvendorId.put(obj.Id, vendorid);         
                }
                
           
               System.debug('mapObjIdvendorId=======:' + mapObjIdvendorId);
           /*   Map<String, String> mapUserNameVendorId = new Map<String, String> ();
                if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

                    for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
                        mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
                    }
                }
                System.debug('mapUserNameVendorId:' + mapUserNameVendorId);*/
                 
                set<string> contactId = new Set<String> ();
                Map<String,  set<string>> mapVendorIdContactId = new Map<String,  set<string>> ();
                if (Contact.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Contact.fields.Id.isAccessible() && Schema.sObjectType.Contact.fields.AccountId.isAccessible()) {
				 				 
				  System.debug('vendorId--&&&'+vendorId); 
				    for(String venId: vendorid)
                    {
                      set<string> SpecificContactId = new Set<String> ();
                      for (Contact con :[SELECT Id, AccountId FROM Contact WHERE AccountId=:venId]) {
                        contactId.add(con.Id);
                        SpecificContactId.add(con.Id); 
						System.debug('contactId--&&&'+contactId);
						System.debug('SpecificContactId--&&&'+SpecificContactId);                           
                        mapVendorIdContactId.put(con.AccountId, SpecificContactId);
                      }
                    }
                }
                 System.debug('mapVendorIdContactId:===' + mapVendorIdContactId);

                    
                Map<String, Id> mapContactIdUId = new Map<String, Id> ();
               if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName, ContactId FROM User WHERE ContactId IN :contactId]){
                        mapContactIdUId.put(u.ContactId, u.id);
                    }
                }
                  System.debug('mapContactIdUId:' + mapContactIdUId);  

               /* Map<String, String> mapUserNameUId = new Map<String, String> ();
                if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
                        mapUserNameUId.put(u.UserName, u.id);
                    }
                }*/

           for (Invoice__c obj : Trigger.New) {
               System.debug('mapObjIdvendorId.get(obj.Id):-- ' + mapObjIdvendorId.get(obj.Id));
                    
                       for(String venId: mapObjIdvendorId.get(obj.Id))
                            { 
                            
                            if (venId != null && mapVendorIdContactId.get(venId) != null){
        
                               System.debug('mapVendorIdContactId.get(venId):' + mapVendorIdContactId.get(venId));
                               for (String conId :mapVendorIdContactId.get(venId)){
                                
                                if(mapContactIdUId.get(conId)!=null){
               
                                System.debug('mapContactIdUId.get(conId):-----' + mapContactIdUId.get(conId));
                                Invoice__Share shareObj = new Invoice__Share();
                                shareObj.put('ParentId', obj.id);
                                shareObj.put('UserOrGroupId', mapContactIdUId.get(conId));
                               // Insufficient Cross  ref entity issue -Added by Shital Rathod - Start - 5-9-2018---
                                shareObj.put('AccessLevel', 'Read');
                               // Insufficient Cross  ref entity issue -Added by Shital Rathod - End - 5-9-2018---
                                shareObj.put('RowCause', Schema.Invoice__Share.RowCause.Manual);
                                insertShareObj.add(shareObj);
                               
                                System.debug('shareObj:' + insertShareObj);
                                }
                              }
        
                            }
                        }
                   }
                        if (deleteShareObj != null && deleteShareObj.size() > 0) {
                            List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
                        }
        
                        if (insertShareObj != null && insertShareObj.size() > 0) {
                            List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
                           //insert insertShareObj;
                            
                        }
                       
            }
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//
            triggerHelper.sharedWithVendor = true;
            //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
        }
            
        //---- Vendor hierarchy setup by Vishwa | CASP2-8
            //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | Starts ---//
    // Added For Accuracy Report
    if (Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
      //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//
      if (tc.Enable_Accuracy_Report__c && !triggerHelper.captureAccuracyReport) {
        //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
        // -------- Added by Hitakshi | 22-08-2018 | JIRA CAS-1354 | Start ------ //
        System.debug('UtilityController.sysConfMap===' + UtilityController.sysConfMap);
        String currentStateFromConfig = UtilityController.sysConfMap.get(esmNameSpace + 'Invoice__c|OCR_Accuracy_Current_State');
        System.debug('currentStateFromConfig' + currentStateFromConfig);
        if (currentStateFromConfig != null && currentStateFromConfig != '') {

          Map<String, string> APFieldADHfieldMap = new Map<String, string> ();

          for (OCR_Accuracy_Report_Mapping__c OCRConf :[SELECT Id, FieldName_on_Object__c, FieldName_on_OCR__c, Object_Name__c FROM OCR_Accuracy_Report_Mapping__c]) {
            APFieldADHfieldMap.put(OCRConf.Object_Name__c + '~' + OCRConf.FieldName_on_Object__c, OCRConf.FieldName_on_OCR__c);

          }
          System.debug('--------APFieldADHfieldMap ' + APFieldADHfieldMap);

          list<OCR_Accuracy_Report_HeaderLevel__c> lstAccuracyForInsert = new list<OCR_Accuracy_Report_HeaderLevel__c> ();
          //  list<OCR_Accuracy_Report_HeaderLevel__c> lstAccuracyForUpdate = new list<OCR_Accuracy_Report_HeaderLevel__c> ();
          OCR_Accuracy_Report_HeaderLevel__c objAccuracyReport;
          Set<Id> idList = Trigger.newMap.keySet();
          List<OCR_Accuracy_Report_HeaderLevel__c> existingData = new List<OCR_Accuracy_Report_HeaderLevel__c> ();
          //String query = ;
          //if (UtilityController.isFieldAccessible(esmNameSpace+'OCR_Accuracy_Report_HeaderLevel__c', null)) {
          //  existingData = ;
          //}
          //Map<Id, OCR_Accuracy_Report_HeaderLevel__c> existingDataMap = new Map<Id, OCR_Accuracy_Report_HeaderLevel__c> ();
          Set<Id> existingInvIdSet = new Set<Id> ();
          Set<Id> InvIdSet = new Set<Id> ();
          for (OCR_Accuracy_Report_HeaderLevel__c obj : database.query(CommonFunctionController.getCreatableFieldsSOQL(esmNameSpace + 'OCR_Accuracy_Report_HeaderLevel__c', esmNameSpace + 'Invoice__c IN :idList', false))) {
            if (obj.get(esmNameSpace + 'Invoice__c') != null) {
              existingInvIdSet.add((Id) obj.get(esmNameSpace + 'Invoice__c'));
            }
          }

          System.debug('--------existingInvIdSet ' + existingInvIdSet);


          if (APFieldADHfieldMap != null && APFieldADHfieldMap.size() > 0) {
            for (Invoice__c childobj : Trigger.new) {
              String currentState = '';
              currentState = childobj.Previous_State__c;
              System.debug('--------currentState ' + currentState);
              System.debug('--------currentStateFromConfig ' + currentStateFromConfig);

              if (currentState != null && currentState != '' && currentState.equalsIgnoreCase(currentStateFromConfig)) {
                System.debug('--------childobj.Id ' + childobj.Id);
                if (!existingInvIdSet.contains(childobj.Id)) {

                  objAccuracyReport = new OCR_Accuracy_Report_HeaderLevel__c();
                  System.debug('--------objAccuracyReport ---- ' + objAccuracyReport);
                  objAccuracyReport.put(esmNameSpace + 'Invoice__c', childobj.get('Id'));
                  for (string s : APFieldADHfieldMap.keyset()) {
                    System.debug('s+++++' + s);
                    if (s.startswith(esmNameSpace + 'Invoice__c~')) {
                      String sInv = s.split('~') [1];
                      System.debug('sInv====367' + sInv);
                      System.debug('APFieldADHfieldMap.get(s)' + APFieldADHfieldMap.get(s));
                      if (string.valueof(childobj.get(sInv)) != null && string.valueof(childobj.get(sInv)) != '') {
                        objAccuracyReport.put(APFieldADHfieldMap.get(s), childobj.get(sInv));
                      } else {
                        objAccuracyReport.put(APFieldADHfieldMap.get(s), null);
                      }


                    }
                  }
                  System.debug('--------objAccuracyReport ' + objAccuracyReport);
                  lstAccuracyForInsert.add(objAccuracyReport);
                }
              }
            }
          }
          Set<String> OCRheaderSet = new Set<String> ();
          Map<String, OCR_Accuracy_Report_HeaderLevel__c> OCRheaderMap = new Map<String, OCR_Accuracy_Report_HeaderLevel__c> ();
          if (lstAccuracyForInsert.size() > 0) {
            System.debug('--------lstAccuracyForInsert ' + lstAccuracyForInsert);
            if (UtilityController.isFieldCreateable(esmNameSpace + 'OCR_Accuracy_Report_HeaderLevel__c', null)) {
              insert lstAccuracyForInsert;
              for (OCR_Accuracy_Report_HeaderLevel__c obj : lstAccuracyForInsert) {
                if (obj.get(esmNameSpace + 'Invoice__c') != null) {
                  OCRheaderMap.put((Id) obj.get(esmNameSpace + 'Invoice__c'), obj);
                  //OCRheaderSet.add(obj.id);
                  System.debug('--------OCRheaderMap ' + OCRheaderMap);
                }
              }

            }
          }
          ///-------------------Code for Line Level Accuracy///
          list<OCR_Accuracy_Report_LineLevel__c> lstAccuracyForInsertLine = new list<OCR_Accuracy_Report_LineLevel__c> ();
          if (APFieldADHfieldMap != null && APFieldADHfieldMap.size() > 0 && OCRheaderMap != null && OCRheaderMap.size() > 0) {

            for (Invoice_Line_Item__c childobj : database.query(CommonFunctionController.getCreatableFieldsSOQL(esmNameSpace + 'Invoice_Line_Item__c', esmNameSpace + 'Invoice__c IN :idList', false))) {
              //  System.debug('currentState At 400 line'+currentState);
              System.debug('OCRheaderMap.get(childobj.Invoice__c)' + OCRheaderMap.get(childobj.Invoice__c));
              //if (currentState != null && currentState != '' && currentState.equalsIgnoreCase(currentStateFromConfig)) {
              OCR_Accuracy_Report_HeaderLevel__c OCRheaderObj = OCRheaderMap.get(childobj.Invoice__c);

              OCR_Accuracy_Report_LineLevel__c objAccuracyReportLineLevel = new OCR_Accuracy_Report_LineLevel__c();
              System.debug('--------objAccuracyReportLineLevel ---- ' + objAccuracyReportLineLevel);
              objAccuracyReportLineLevel.put(esmNameSpace + 'Invoice_Line_Item__c', childobj.get('Id'));
              objAccuracyReportLineLevel.put(esmNameSpace + 'OCR_Accuracy_Report_HeaderLevel__c', OCRheaderObj.Id);


              for (string s : APFieldADHfieldMap.keyset())
              {
                if (s.startswith(esmNameSpace + 'Invoice_Line_Item__c~')) {

                  String sInvLN = s.split('~') [1];
                  System.debug('sInvLN====' + sInvLN);
                  System.debug('APFieldADHfieldMap.get(s)' + APFieldADHfieldMap.get(s));
                  if (string.valueof(childobj.get(sInvLN)) != null && string.valueof(childobj.get(sInvLN)) != '')
                  {
                    objAccuracyReportLineLevel.put(APFieldADHfieldMap.get(s), childobj.get(sInvLN));
                  }
                  else
                  {
                    objAccuracyReportLineLevel.put(APFieldADHfieldMap.get(s), null);
                  }

                }
              }
              System.debug('--------objAccuracyReportLineLevel ' + objAccuracyReportLineLevel);


              lstAccuracyForInsertLine.add(objAccuracyReportLineLevel);
              //  }
            }
          }
          if (lstAccuracyForInsertLine.size() > 0) {

            System.debug('--------lstAccuracyForInsertLine ' + lstAccuracyForInsertLine);

            if (UtilityController.isFieldCreateable(esmNameSpace + 'OCR_Accuracy_Report_LineLevel__c', null)) {
              insert lstAccuracyForInsertLine;
            }
          }
        }
        // -------- Added by Hitakshi | 22-08-2018 | JIRA CAS-1354 | Ends ------ //
        /* if (lstAccuracyForUpdate.size() > 0) {
          System.debug('--------lstAccuracyForUpdate ' + lstAccuracyForUpdate);
          if (UtilityController.isFieldUpdateable('OCR_Accuracy_Report_HeaderLevel__c', null)) {
          Database.update(lstAccuracyForUpdate, false);
          }
          }*/
                //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Starts ---//
                triggerHelper.captureAccuracyReport = true;
                //--- Changed by Dilshad | JIRA:CAS-1380 | 04-09-2018 | Ends ---//
            }
        }
    //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | End ---//
    }
    
    catch(Exception e) {
        Trigger.New[0].addError(UtilityController.customDebug(e, 'InvoiceMasterTrigger'));
    }
}