trigger PaymentMasterTrigger on Payment__c (before insert, before update, after insert, after update) {
        try {
            //---- Vendor hierarchy setup by Vishwa | CASP2-46

            if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
           
                List<SObject> insertShareObj = new List<SObject> ();
               
            
                System.debug('---------in if----------------');
                Set<Id> parentId = Trigger.NewMap.keySet();

                List<Payment__share> deleteShareObj = Database.query('SELECT Id FROM Payment__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');

                set<string> vendorId = new Set<String> ();
                Map<String,  set<string>> mapObjIdvendorId = new Map<String,  set<string>> ();
                Map<ID, Account> allAccounts = new Map<ID, Account>([SELECT Id, Parent_Vendor__c FROM Account]);
                System.debug('allAccounts '+allAccounts);
                for (Payment__c obj : Trigger.New) {
                    
                    Id vendor=obj.Vendor__c;
                     System.debug('vendor '+vendor);
                    for(;vendor!=null;)
                    {
            //        if (obj.Vendor__c != null) {
                        vendorId.add(vendor);
                        Account Acc = allAccounts.get(vendor);
                   //   Account Acc = Database.query('SELECT Id, Parent_Vendor__c FROM Account WHERE Id=:vendor');
                        vendor = Acc.Parent_Vendor__c;
                        System.debug('Parent_Vendor__c '+vendor);
                    }
                    mapObjIdvendorId.put(obj.Id, vendorId);         
                }
                
           
               System.debug('vendorId:' + vendorId);
           /*   Map<String, String> mapUserNameVendorId = new Map<String, String> ();
                if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

                    for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
                        mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
                    }
                }
                System.debug('mapUserNameVendorId:' + mapUserNameVendorId);*/
                 
                set<string> contactId = new Set<String> ();
                Map<String,  set<string>> mapVendorIdContactId = new Map<String,  set<string>> ();
                if (Contact.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Contact.fields.Id.isAccessible() && Schema.sObjectType.Contact.fields.AccountId.isAccessible()) {

                    for(String venId: vendorId)
                    {
                      set<string> SpecificContactId = new Set<String> ();
                      for (Contact con :[SELECT Id, AccountId FROM Contact WHERE AccountId=:venId]) {
                        contactId.add(con.Id);
                        SpecificContactId.add(con.Id);                          
                        mapVendorIdContactId.put(con.AccountId, SpecificContactId);
                      }
                    }
                }
                 System.debug('mapVendorIdContactId:' + mapVendorIdContactId);
                    
                Map<String, Id> mapContactIdUId = new Map<String, Id> ();
               if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName, ContactId FROM User WHERE ContactId IN :contactId]){
                        mapContactIdUId.put(u.ContactId, u.id);
                    }
                }
                  System.debug('mapContactIdUId:' + mapContactIdUId);  

             /*   Map<String, String> mapUserNameUId = new Map<String, String> ();
                if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
                        mapUserNameUId.put(u.UserName, u.id);
                    }
                }*/

           for (Payment__c obj : Trigger.New) {
               System.debug('mapObjIdvendorId.get(obj.Id): ' + mapObjIdvendorId.get(obj.Id));
                    for(String venId: mapObjIdvendorId.get(obj.Id))
                    { 
                    
                    if (venId != null && mapVendorIdContactId.get(venId) != null){

                       System.debug('mapVendorIdContactId.get(venId):' + mapVendorIdContactId.get(venId));
                       for (String conId :mapVendorIdContactId.get(venId)){
                        
                        if(mapContactIdUId.get(conId)!=null){
       
                        System.debug('mapContactIdUId.get(conId):' + mapContactIdUId.get(conId));
                        Payment__share shareObj = new Payment__share();
                        shareObj.put('ParentId', obj.id);
                        shareObj.put('UserOrGroupId', mapContactIdUId.get(conId));
                        shareObj.put('AccessLevel', 'Read');
                        shareObj.put('RowCause', Schema.Payment__share.RowCause.Manual);
                        insertShareObj.add(shareObj);
                       
                        System.debug('shareObj:' + shareObj);
                        }
                      }

                    }
                }
           }
                if (deleteShareObj != null && deleteShareObj.size() > 0) {
                    List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
                }

                if (insertShareObj != null && insertShareObj.size() > 0) {
                    List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
                }
               
            
            
        }
            
        //---- Vendor hierarchy setup by Vishwa | CASP2-46
        }
      catch(Exception e) {
        Trigger.New[0].addError(UtilityController.customDebug(e, 'PaymentMasterTrigger'));
    }
}