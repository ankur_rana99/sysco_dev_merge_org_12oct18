trigger ACKupdateTrigger on Sysco_ACK_Interface__c (after insert) {
    if(Trigger.IsAfter){
        Set<String> invnos = new Set<String>();
        Map<String, Invoice__c> invMap = new Map<String,Invoice__c>();
        Map<String,List<Invoice_Line_Item__c>> InvLineMap = new Map<String,List<Invoice_Line_Item__c>>();
        Map<id,Invoice__c> dupcheck = new map<id,Invoice__c>();
        List<Invoice__c> updateInvList = new List<Invoice__c>();
        List<Invoice_Line_Item__c> updateInvlineList = new List<Invoice_Line_Item__c>();
        List<Invoice_ACK_Error_Code_Description__c> confgset = [Select id, name,Error_Message__c from Invoice_ACK_Error_Code_Description__c];
         Sysco_SystemConfiguration__c adminid = Sysco_SystemConfiguration__c.getValues('ERPConfig');
        Map<String, String> errorMessage = new Map<String,String>();
      // User us = [Select id,name from User where name =:'Admin UAT'];
       // System.debug('user id is : '+us.id);
        system.debug('current usr is :'+userInfo.getUserId());
        for(Sysco_ACK_Interface__c ack : Trigger.New){
            System.debug('invoice processing : '+ack.Cora_Ref_No__c);
            invnos.add(ack.Cora_Ref_No__c);
        }
        System.debug('Total invoices'+invnos.size());
        for(Invoice_ACK_Error_Code_Description__c customset : confgset){
            errorMessage.put(customset.name,customset.Error_Message__c);
        }
        List<Invoice__c> invos = [select id,Name,Cora_Ref_No__c,OPCO_Code__c,Total_Amount__c,ACK_Created_Date__c,Batch_No__c,Invoice_ERP_Status__c,SUS_Error_Message_Code__c,Transaction_Code__c,Transaction_Type__c,Current_State__c,User_Action__c,ERP_Status_Message__c,(select id,Name,Batch_No__c,ACK_Created_Date__c,Amount__c,Invoice_Line_No__c,Transaction_Code__c,Transaction_Type__c,Invoice_Line_Status__c from Invoice_Line_Items_Invoice__r) from Invoice__c where Cora_Ref_No__c IN : invnos];
        for(Invoice__c inv : invos){
            invMap.put(inv.Cora_Ref_No__c,inv);
            InvLineMap.put(inv.Cora_Ref_No__c,inv.Invoice_Line_Items_Invoice__r);
            
        }
        for(Sysco_ACK_Interface__c ack : Trigger.New){
            // Temporay Sol Begin - Adding a IF Condition to avoid Null pointer
            if(ack.Cora_Ref_No__c != null && ack.Cora_Ref_No__c != '' && invMap.get(ack.Cora_Ref_No__c) != null ) {
                
                System.debug('current state :'+invMap.get(ack.Cora_Ref_No__c).Current_State__c);
                
                if(invMap.get(ack.Cora_Ref_No__c).Current_State__c == 'Awaiting ERP ACK' || invMap.get(ack.Cora_Ref_No__c).Current_State__c == 'Vouched'){
                    system.debug('Current invoice: '+ack.Cora_Ref_No__c);
                    
                    if(ack.Invoice_Line_No__c == '0'){
                        
                        Invoice__c invupdate = invMap.get(ack.Cora_Ref_No__c);
                        invupdate.ACK_Created_Date__c = ack.ACK_Created_Date__c;
                        invupdate.Batch_No__c = ack.Batch_ID__c;
                        invupdate.Invoice_ERP_Status__c = ack.Invoice_ERP_Status__c == 'OPN' ? 'Open' : ack.Invoice_ERP_Status__c == 'ERR' ? 'Error' : ack.Invoice_ERP_Status__c == 'VCH' ? 'Vouched' : ack.Invoice_ERP_Status__c == 'PD' ? 'Paid' : ack.Invoice_ERP_Status__c == 'RVS' ? 'Reversal' : ack.Invoice_ERP_Status__c == 'APR' ? 'Approved' : null;
                        if(ack.SUS_Error_Message_Code__c != null){
                            invupdate.SUS_Error_Message_Code__c = ack.SUS_Error_Message_Code__c;
                            //invupdate.Last_Modified_By__c = adminid.Admin_id__c;
                            if(invupdate.ERP_Status_Message__c == null){
                                invupdate.ERP_Status_Message__c = '';
                            }
                            List<String> errCodes = (ack.SUS_Error_Message_Code__c).Split('-');
                            for(String errcd : errCodes){
                                
                                
                                invupdate.ERP_Status_Message__c += invupdate.ERP_Status_Message__c != '' ? ('; '+errorMessage.get(errcd)) : errorMessage.get(errcd);
                            }
                        }
                        
                        if(ack.Invoice_ERP_Status__c == 'OPN' || ack.Invoice_ERP_Status__c == 'ERR'){
                            
                            invupdate.User_Action__c = 'Posting ACK Fail';
                        }
                        else if(ack.Invoice_ERP_Status__c == 'VCH'){
                            List<Invoice_Line_Item__c> invlines = InvLineMap.get(ack.Cora_Ref_No__c);
                            Invoice_Line_Item__c invli = new Invoice_Line_Item__c();
                            for(Invoice_Line_Item__c invl : invlines){
                                if(!string.isBlank(invl.Invoice_Line_Status__c)){
                                    invl.Invoice_Line_Status__c += '; '+'Invoice Vouched';
                                    updateInvlineList.add(invl);
                                }
                            }
                            invupdate.User_Action__c = 'Posting ACK Success';
                        }
                        else{
                            
                        }
                        updateInvList.add(invupdate);
                    }
                    else{
                        List<Invoice_Line_Item__c> invlines = InvLineMap.get(ack.Cora_Ref_No__c);
                        Invoice_Line_Item__c invli = new Invoice_Line_Item__c();
                        for(Invoice_Line_Item__c invl : invlines){
                            if(invl.Invoice_Line_No__c == ack.Invoice_Line_No__c){
                                invli = invl;
                                break;
                            }
                        }
                        Invoice__c invupdate = invMap.get(ack.Cora_Ref_No__c);
                        
                        If(invli != null){
                            invli.ACK_Created_Date__c = ack.ACK_Created_Date__c;
                            invli.Batch_No__c = ack.Batch_ID__c;
                            if(ack.SUS_Error_Message_Code__c != null){
                                if(invli.Invoice_Line_Status__c == null){
                                    invli.Invoice_Line_Status__c = '';
                                }
                                if(ack.SUS_Error_Message_Code__c != null){   
                                    invupdate.User_Action__c = 'Posting ACK Fail';
                                    List<String> errCodes = (ack.SUS_Error_Message_Code__c).Split('-');
                                    for(String errcd : errCodes){
                                       
                                        invli.Invoice_Line_Status__c += invli.Invoice_Line_Status__c != '' ? ('; '+errorMessage.get(errcd)) : errorMessage.get(errcd);
                                    }
                                }
                            }
                            updateInvlineList.add(invli);
                            Boolean isavailable = false;
                            for(Invoice__c temp : updateInvList){
                                if(temp.id == invupdate.id){
                                    isavailable = true;
                                    break;
                                }
                            }
                            if(!isavailable){
                                updateInvList.add(invupdate);
                            }
                            
                        }
                    }
                }
                else if(invMap.get(ack.Cora_Ref_No__c).Current_State__c == 'Posting Failed'){
                    //User_Master__c us1 = [Select id,name from User_Master__c where id =:'a0b1g000000FuRL'];
        
                    system.debug('Current invoice from posting failed section: '+ack.Cora_Ref_No__c);
                    Invoice__c tempcheck =invMap.get(ack.Cora_Ref_No__c);
                    tempcheck.ERP_Status_Message__c += ';'+'Ack received on Posting failed state which is not expected';
                    //tempcheck.Last_Modified_By__c = us1.id;
                    Boolean isavailable = false;
                    for(Invoice__c temp : updateInvList){
                                if(temp.id == tempcheck.id){
                                    isavailable = true;
                                    break;
                                }
                            }
                            if(!isavailable){
                                updateInvList.add(tempcheck);
                            }
                }
                // Temporay Solution End - Closing IF Condition
            }
        }
        if(updateInvlineList != null && updateInvlineList.size()>0){
            update updateInvlineList;
        }
        if(updateInvList != null && updateInvList.size()>0){
            for(invoice__c invo : updateInvList){
                System.debug('----invoices--- : '+invo.name);
                system.debug('last modified by -------------: '+invo.Last_Modified_By__c);
            }
            update updateInvList;
        }
        delete [select id from Sysco_ACK_Interface__c where id in :Trigger.new];
        
    }
    
    
}