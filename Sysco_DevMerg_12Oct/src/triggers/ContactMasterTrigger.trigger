trigger ContactMasterTrigger on Contact (after insert,after update) {
   
    try{   
          //---- Vendor hierarchy setup by Vishwa | CASP2-39
        if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
               List<User> insertObj = new List<User> ();
               set<string> contactId = new Set<String> ();
               for(Contact con : Trigger.new){
                   contactId.add(con.Id); 
                   
                }
                 
            //  Map<ID, Account> allAccounts = new Map<ID, Account>([SELECT Id, Parent_Vendor__c FROM Account]);
                Map<String, User> allUsers = new Map<String, User> ();
               if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                    for (User u :[Select Id, FirstName, LastName, IsActive, UserName, ContactId, Email FROM User WHERE ContactId IN :contactId]){
                        allUsers.put(u.Email, u);
                    }
                }
                System.debug('allUsers: ' + allUsers);  
            
                if(UtilityController.sysConfMap == null){
                UtilityController.sysConfMap = new Map<String, String>();
                for (System_Configuration__c sysConf :[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c]) {
                    UtilityController.sysConfMap.put(sysConf.Module__c + '|' + sysConf.Sub_Module__c, sysConf.Value__c);
                   }
                }
                for(Contact con : Trigger.new){
                  User insObj = new User();
                 
                  insObj.Username=con.Email;
                  insObj.LastName=con.lastName;
                  insObj.Email=con.Email;
                  insObj.Alias=con.lastName;
                  insObj.CommunityNickname=con.lastName;
                  insObj.TimeZoneSidKey=UserInfo.getTimeZone()+'';
                  insObj.LocaleSidKey=UserInfo.getLocale();
                  insObj.EmailEncodingKey='ISO-8859-1';                 
                  insObj.ProfileId=UtilityController.sysConfMap.get('Vendor_Master|Supplier_Profile_ID');
                  insObj.LanguageLocaleKey= UserInfo.getLanguage();
                  insObj.ContactId=con.Id;

                  for(User user :allUsers.values())
                  {
                      if(user.Email==insObj.Email)
                      {
                         insObj.Id=user.Id;
                         break;
                      }
                  }
                  allUsers.put(con.Email, insObj);
                }
                System.debug('allUsers after: ' + allUsers); 
            
                for(User user :allUsers.values())
                {
                    insertObj.add(user);
                }
                System.debug('insertObj: ' + insertObj); 
            
                if (insertObj != null && insertObj.size() > 0) {
                    List<Database.upsertResult> sr = Database.upsert(insertObj, false);
                }

         }     
          //---- Vendor hierarchy setup by Vishwa | CASP2-39
    }catch(Exception e) {
        Trigger.New[0].addError(UtilityController.customDebug(e, 'ContactMasterTrigger'));
    }
   

}