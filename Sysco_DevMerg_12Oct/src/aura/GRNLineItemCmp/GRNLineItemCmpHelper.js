({
	getGRNLineItemDetails : function(component, event) {
	/*if(JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"].length >0){
		for(var i = 0 ;i<JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"].length;i++)
		{
			if(JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"][i]["objectName"] == "GRN_Line_Item__c")
				{
				console.log(JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"][i]["columns"]);
				var columns = JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"][i]["columns"];
				component.set("v.recId",event.getParam("evtId")); 
				component.set("v.section",JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"][i]);
				this.getGRNLineData(component, event, columns, event.getParam("evtId"));
				break;
				}
			}
		}*/
		var layout =component.get("v.layOutJson");
		var columns = layout["columns"];
		this.getGRNLineData(component, event, columns,component.get("v.mainObject.Id"));
    },
    getGRNLineData: function(component, event, objectLsts,objId) {
        console.log('objeclineeeetLsts');
	    console.log(JSON.stringify(component.get("v.mainObject")));
		var action1 = component.get("c.getGRNLineDataServer");	
        //Set the Object parameters and Field Set name 
        action1.setParams({
            strObjectName: 'GRN_Line_Item__c',
            jsonObj: JSON.stringify(objectLsts),
		    objId : objId,
			poFlip: component.get("v.poFlipCreate"),
        });
        action1.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
                console.log('resChi');
                console.log(response.getReturnValue());
                console.log(response.getReturnValue().DataList);
                console.log(response.getReturnValue().Columns);
                component.set("v.objList", response.getReturnValue().DataList);
                 component.set("v.columns", response.getReturnValue().Columns);

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);
    },
	 loadSelectedGRNlineHelper: function(component, event) {
        console.log(event.getParam("polines"));
         var action2 = component.get("c.getPOLineFromPODataServer");
        //Set the Object parameters and Field Set name 
        action2.setParams({
            strObjectName: 'GRN_Line_Item__c',
		    objId :  event.getParam("polines"),
        });
          action2.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
                console.log('resPOLIne from GRN Chi');
                component.set("v.objList", response.getReturnValue().DataList);

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action2);
    },

})