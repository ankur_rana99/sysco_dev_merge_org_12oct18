({
	getGRNLineItem: function (cmp, event, helper) { 
 		helper.getGRNLineItemDetails(cmp, event);
 	},
	 loadSelectedGRNline: function (cmp, event, helper) { 
 		helper.loadSelectedGRNlineHelper(cmp, event);
 	},
    sectionOne : function(component, event, helper) {
         var acc = component.find('HeaderSec');	 
			for(var cmp in acc) {
				$A.util.toggleClass(acc[cmp], 'slds-show');  
				$A.util.toggleClass(acc[cmp], 'slds-hide');  
	  		}
	},
})