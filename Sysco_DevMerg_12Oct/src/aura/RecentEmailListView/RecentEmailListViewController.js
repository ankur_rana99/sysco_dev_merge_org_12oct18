({
    doInit : function(component, event, helper) {
        var action = component.get("c.getRecentInteraction");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                component.set("v.InteractionList", res);
                component.set("v.loadSpinner", false);
            }else{
                console.log('issue');
            }
            
        });
        $A.enqueueAction(action);
    }
})