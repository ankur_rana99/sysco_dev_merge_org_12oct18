({

    //for Range filetr functionality
    //created by : Shital Rathod
    //JIRA CASP2 - 11
    doInit: function(component, event, helper) {


        var opts = [{
                "class": "optionClass",
                label: "Range",
                value: "Range"
            },
            {
                "class": "optionClass",
                label: "=",
                value: "="
            },
            {
                "class": "optionClass",
                label: "<>",
                value: "<>"
            },
            {
                "class": "optionClass",
                label: ">",
                value: ">"
            },
            {
                "class": "optionClass",
                label: ">=",
                value: ">="
            },
            {
                "class": "optionClass",
                label: "<",
                value: "<"
            },
            {
                "class": "optionClass",
                label: "<=",
                value: "<="
            }
        ];

        component.set("v.operatorList", opts);

        var optsDate = [{
                "class": "optionClass",
                label: "Range",
                value: "Range"
            },
            /*{
                "class": "optionClass",
                label: "L30",
                value: "-30"
            },
            {
                "class": "optionClass",
                label: "L60",
                value: "-60"
            },
            {
                "class": "optionClass",
                label: "L90",
                value: "-90"
            },
            {
                "class": "optionClass",
                label: "N30",
                value: "+30"
            },
            {
                "class": "optionClass",
                label: "N60",
                value: "+60"
            },
            {
                "class": "optionClass",
                label: "N60",
                value: "+90"
            }*/
        ];

        component.set("v.dateOpratorList", optsDate);


        var obj = component.get('v.fieldcriteria.picklistmap');
        if (component.get("v.fieldcriteria.fieldType") == "PICKLIST" || component.get("v.fieldcriteria.fieldType") == "MULTIPICKLIST") {
            var options = [];
            var count = 0;
            var option = new Object();
			option.value = "";
            option.label = '---select---';
            option.selected = false;
            options[count] = option;

            console.log('picklistmap====' + JSON.stringify(component.get('v.fieldcriteria.picklistmap')));
            for (var key in component.get('v.fieldcriteria.picklistmap')) {
                console.log('key==' + component.get('v.fieldcriteria.picklistmap')[key]);
                console.log('*****' + component.get('v.fieldcriteria.picklistmap')[key]['value']);
                option = new Object();
                option.value = key;
                option.label = component.get('v.fieldcriteria.picklistmap')[key];
                console.log('Value---' + option.value);
                console.log('label---' + option.label);
                //var field_value = component.get("v.currentObject."+fieldName);
                /*if(field_value != undefined && field_value.includes(option.value)){
                	option.selected = true;
                } else {
                	option.selected = false;
                }*/
                options[count + 1] = option;
                count++;
            }

            console.log('---------options' + options);

            component.set("v.options", options);

        }

    },
    firePicklistEventDate: function(component, event, helper) {
        var selectedval = component.get("v.fieldcriteria.selectedOperator1");
        console.log('selectedval==' + selectedval);
        var val1 = component.get("v.fieldcriteria.fieldValue1");
        var val2 = component.get("v.fieldcriteria.fieldValue2");
        console.log('val1Date==' + val1);
        console.log('val2Date==' + val2);
        // SHital changes start===
        // Shital changes end====

    },

    firePicklistEvent: function(component, event, helper) {
        var selectedval = component.get("v.fieldcriteria.selectedOperator1");
        console.log('selectedval==' + selectedval);
		 var val1 = component.get("v.fieldcriteria.fieldValue1");
         var val2 = component.get("v.fieldcriteria.fieldValue2");
		 component.set('v.fieldcriteria.fieldValue2', val2);
        console.log('val1==' + val1);
        console.log('val2==' + val2);
		var el = component.find("toValidate");
		//$A.util.addClass(el, 'showMe');

        if (selectedval != 'Range') {
		//alert('If not range');
            val2 = '';
            component.set('v.fieldcriteria.fieldValue2', val2);
            //var el = component.find("toValidate");
            $A.util.addClass(el, 'hideMe');
      
        } 
		if (selectedval == 'Range') {
		//alert('equal to range');
        	
            $A.util.addClass(el, 'showMe');
        }
            

    },

    firePicklistEvent1: function(component, event, helper) {
        var selectedval = component.get("v.fieldcriteria.fieldValue1");
        console.log('selectedval==' + selectedval);

    },


})