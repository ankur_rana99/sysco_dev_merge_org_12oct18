({
    
     makeMeOwner : function(component, event, isRedirect) {
	console.log('make me owner');
        var ct= component.get("v.selectedCases");
		//alert(component.get("v.loginUserId"));
        ct[0].OwnerId = component.get("v.loginUserId");
       //console.log(ct);
        //component.set("v.ct",ct[0]);
          
        var compEvent = component.getEvent("caseAction");
        var myCompEvent = component.getEvent("caseAction");
        
        var caseObj = ct[0];
        compEvent.setParams({
                "Action": "update",
				"Case" :caseObj,
            	"callback" :function(response){
                   // console.log(response.getState());
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var message = '';
                         
                        if(!isRedirect){  
                            message =  "Case-"+caseObj.Name+" is accepted!";
                        }else{
                            message = "Case-"+caseObj.Name+" is accepted and navigated to detail page!";
                        }
                        var appEvent = $A.get("e.c:ShowToast");
         				appEvent.setParams({ "title" : "Success !","message":message});
                         appEvent.fire();
						  var comVar =component.get("v.refreshCounter");
						  component.set("v.refreshCounter",comVar+1);
						  component.set("v.showHideSplitMerge","");
						  var emptyList = [];
						  component.set("v.selectedCases", emptyList); 
						//  var oTextarea = component.find("checkbox-20"+caseObj.Id);
						 //oTextarea.set("v.value", false);

						  //component.set("v.selectedCases", ''); 
                        //component.destroy(); 
                        //component.set("v.templates",response.getReturnValue().templates);
                        //component.set("v.body",component.get("v.templates")[0].Body);
                        
                    }else if (state === "INCOMPLETE") {
                        
                    }else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                }
            });	
        compEvent.fire();	
    },
    
})