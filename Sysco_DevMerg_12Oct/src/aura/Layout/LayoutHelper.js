({
    getFields : function(component, event) {
        var action = component.get("c.getFields");
		var sfdcSessionId = '{!GETSESSIONID()}';
        var requestParm ={};
        requestParm["objectName"]=component.get("v.sObjectName");
        requestParm["recordTypeName"]=component.get("v.recordTypeName");
        requestParm["sessionId"]=window.SessionId;
		//requestParm["sessionId"]=userInfo.getSessionId();
		requestParm["caseManagerId"]=component.get("v.sObject").Id;
       
        action.setParams({ requestParm : JSON.stringify(requestParm)});
        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var layoutMeta=response.getReturnValue().sectionFields;
                for(var i=0;i<layoutMeta.length;i++){
                            layoutMeta[i]['isOpen']=true;
                    
                }
				var layoutJSON=response.getReturnValue().fieldJson;
				component.set("v.layoutJSON",layoutJSON);
				component.set("v.layoutMeta",layoutMeta);
				
              //  component.set('v.layoutMeta',response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    validate: function(component, event) {
        var layoutMeta=component.get("v.layoutMeta");
        var sObject=component.get("v.sObject");
         sObject['hasError']=false;
        for(var i=0;i<layoutMeta.length;i++){
            var lMeta =layoutMeta[i];
            for(var j=0;j< lMeta.relatedFields.length;j++){
                var fieldDetails =lMeta.relatedFields[j];
                
                if(fieldDetails.fieldBehavior=='Required' && (sObject[fieldDetails.fieldAPIName]==undefined || sObject[fieldDetails.fieldAPIName].trim() =='')){
                   //  alert(sObject[fieldDetails.fieldAPIName]);// && sObject[fieldDetails.fieldAPIName] ==""
                     sObject['hasError']=true;
                    this.showToast(component,'','Please enter value in required fields!','error');
                    var element=document.getElementById(fieldDetails.fieldAPIName);
                    if(!this.isScrolledIntoView(element)){
                        element.scrollIntoView();
                    }
                    
                }
				 if(fieldDetails.fieldType=='TIME')
					{                    
							if(sObject[fieldDetails.fieldAPIName]!=undefined && sObject[fieldDetails.fieldAPIName]!=''){
									sObject[fieldDetails.fieldAPIName]=sObject[fieldDetails.fieldAPIName]+':00';
							}
                                  
					}  
					if(fieldDetails.fieldType=='DATETIME')
					{                                                                    
							if(sObject[fieldDetails.fieldAPIName]!=undefined && sObject[fieldDetails.fieldAPIName]!='' && !sObject[fieldDetails.fieldAPIName].endsWith('.000Z')){
									sObject[fieldDetails.fieldAPIName]=sObject[fieldDetails.fieldAPIName]+':00.000Z';
							}                          
					}
            }
        }
        component.set("v.sObject",sObject);
    },
    isScrolledIntoView : function (el){
        var rect = el.getBoundingClientRect();
        var elemTop = rect.top;
        var elemBottom = rect.bottom;
        var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        return isVisible;
    },
	/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
    getFieldDetails : function(component, event) {
        var params = event.getParam('arguments');
        var sObject=component.get("v.sObject");
        if (params) {
            var fieldName = params.fieldName;
        }
        var layoutMeta=component.get("v.layoutMeta");
        for(var i=0;i<layoutMeta.length;i++){
            var lMeta =layoutMeta[i];
            for(var j=0;j< lMeta.relatedFields.length;j++){
                var fieldDetails =lMeta.relatedFields[j];
                if(fieldDetails.fieldAPIName==fieldName){
				
                //  component.set("v.methodResponse",fieldDetails.dependentValues[sObject[fieldDetails.controlField]]);
                  component.set("v.methodResponse",fieldDetails);
                } 
            }
        }
       
    },
    showToast: function(component,title,message,msgtype){
        
          var  type = msgtype;
          var  css = 'toast-top-center';
          var  msg = message;
    
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 3000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250
        toastr.options.positionClass = css;
        toastr[type](msg);
    },
    fieldChangeHelper: function(component,event,helper,fieldName) {
		//alert(6);
        //var fieldName =component.get("v.value");
		var layoutJSON=component.get("v.layoutJSON");
		var sobjValue=component.get("v.sObject");
		var layoutMeta=component.get("v.layoutMeta");
		//console.log(sobjValue);
		var actionFields=[];
		var actionFieldsOther=[];
		if(layoutJSON!=null && layoutJSON.rules!=null){
			$.each(layoutJSON.rules,function(index, value ){
					if(value.criterias!=null){
						if(value.field==fieldName){
							var isMatch=helper.isMatchCriteria(value.criterias,sobjValue);
							if(isMatch){
								if(value.actions!=null){
									$.each(value.actions,function(innerIndex, innerValue ){
										actionFields.push(innerValue);
									});
								}
							}
						}
						else{
							var isMatch=helper.isMatchCriteria(value.criterias,sobjValue);
							if(isMatch){
								if(value.actions!=null){
									$.each(value.actions,function(innerIndex, innerValue ){
										actionFieldsOther.push(innerValue);
									});
								}
							}
						}
					}
				
			});
		}
		if(layoutJSON!=null){
			$.each(layoutMeta,function(index, value ){
				$.each(value.relatedFields,function(indexInner, fieldDetails ){
					if(layoutJSON.defaultHiddenFields!=null){
						$.each(layoutJSON.defaultHiddenFields, function( ind, defValue ) {
							if(fieldDetails.fieldAPIName==defValue){
								fieldDetails.display='none';
								fieldDetails.fieldBehavior='';
								$('#'+defValue).hide();
								$('#'+defValue).find('.slds-select').removeAttr('required');
								$('#'+defValue).find('.slds-input').removeAttr('required');
							}
						});
					}
				});
			});
			$.each(layoutMeta,function(index, value ){
				$.each(value.relatedFields,function(indexInner, fieldDetails ){
					if(actionFields.length>0){
						helper.executeMatchAction(actionFields,fieldDetails);
					}
					if(actionFieldsOther.length>0){
						helper.executeMatchAction(actionFieldsOther,fieldDetails);
					}
				});
			});
		}
		
		component.set("v.layoutMeta",layoutMeta);
        component.set("v.sObject",component.get("v.sObject"));
	},
    isMatchCriteria: function(criterias,sobjValue) {
		var isMatch=false;
		$.each(criterias,function(innerIndex, innerValue ){
			var fieldValue=sobjValue[innerValue.field];
			if(fieldValue!=null){
				if(innerValue.operator=='equals'){
					isMatch=fieldValue==innerValue.value;
				}
				else if(innerValue.operator=='not equal to'){
					isMatch=fieldValue!=innerValue.value;
				}
				else if(innerValue.operator=='less than'){
					isMatch=fieldValue>innerValue.value;
				}
				else if(innerValue.operator=='greater than'){
					isMatch=fieldValue<innerValue.value;
				}
				else if(innerValue.operator=='less or equal'){
					isMatch=fieldValue>=innerValue.value;
				}
				else if(innerValue.operator=='greater or equal'){
					isMatch=fieldValue<=innerValue.value;
				}
				else if(innerValue.operator=='contains in list'){
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=true;
							return;
						}
					});
				}
				else if(innerValue.operator=='does not contains in list'){
					isMatch=true;
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=false;
							return;
						}
					});
				}
			}
			if(isMatch==false){
				return false;
			}
		});
		return isMatch;
	},/* Purpose: To update PredictedValue status of Indexing Field		end	*/
	/*
    refreshReadOnlyFields : function(component,event){
         
         var action = component.get("c.getFields");
		var sfdcSessionId = '{!GETSESSIONID()}';
        var requestParm ={};
        requestParm["objectName"]=component.get("v.sObjectName");
        requestParm["recordTypeName"]=component.get("v.recordTypeName");
        requestParm["sessionId"]=window.SessionId;
		//requestParm["sessionId"]=userInfo.getSessionId();
		requestParm["caseManagerId"]=component.get("v.sObject").Id;
        console.log("sessionId-->",sfdcSessionId);
		console.log("object param-->",requestParm);
        action.setParams({ requestParm : JSON.stringify(requestParm)});
        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var oldLayoutMeta = component.get('v.layoutMeta');
                var newLayoutMeta=response.getReturnValue().sectionFields;
                var informationSection;
                for(var i=0;i<newLayoutMeta.length;i++){
                    
                    if(newLayoutMeta[i]['sectionName'] === 'Information'){
                        newLayoutMeta[i]['isOpen']=false;
                        informationSection = newLayoutMeta[i];
                    }
                }
                for(var i=0;i<oldLayoutMeta.length;i++){
                     if(oldLayoutMeta[i]['sectionName'] === 'Information'){
                         oldLayoutMeta[i] = informationSection;
                    }
                }
                component.set("v.layoutMeta",oldLayoutMeta);
                
                for(var i=0;i<oldLayoutMeta.length;i++){
                     if(oldLayoutMeta[i]['sectionName'] === 'Information'){
                         //oldLayoutMeta[i] = informationSection;
                         oldLayoutMeta[i]['isOpen']= true;
                    }
                }
				//var layoutJSON=response.getReturnValue().fieldJson;
				//component.set("v.layoutJSON",layoutJSON);
				component.set("v.layoutMeta",oldLayoutMeta);
                console.log(component.get("v.layoutMeta"));
                //component.set('v.layoutMeta',response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    }*/
    
    executeMatchAction:function(actionFields,fieldDetails){
		$.each(actionFields,function(ind, actionField){
			if(fieldDetails.fieldAPIName==actionField.field){
				if(actionField.action=='Hidden'){
					fieldDetails.display='none';
					fieldDetails.fieldBehavior='';
					$('#'+actionField.field).hide();
					$('#'+actionField.field).find('.slds-select').removeAttr('required');
					$('#'+actionField.field).find('.slds-input').removeAttr('required');
				}
				else if(actionField.action=='Editable'){
					fieldDetails.display='';
					fieldDetails.fieldBehavior='';
					$('#'+actionField.field).show();
					$('#'+actionField.field).find('.slds-select').removeAttr('required');
					$('#'+actionField.field).find('.slds-input').removeAttr('required');
				}
				else if(actionField.action=='Mandatory'){
					fieldDetails.display='';
					fieldDetails.fieldBehavior='Required';
					$('#'+actionField.field).show();
					$('#'+actionField.field).find('.slds-select').prop('required',true);
					$('#'+actionField.field).find('.slds-input').prop('required',true);
				}
			}
		});
	}
    	
})