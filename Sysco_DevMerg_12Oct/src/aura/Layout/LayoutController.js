({
    doInit: function(component, event, helper) {
		//debugger;
        var sObj=component.get('v.sObject');
        if(sObj==null){
            var sObject={"sobjectType":component.get("v.sObjectName")};
            component.set('v.sObject',sObject);
            
        }
        helper.getFields(component, event);
    },
    handleClick: function(component, event, helper) {
		alert(JSON.stringify(component.get("v.sObject")));
	},
    validate: function(component, event, helper) {
        helper.validate(component, event);
	},
	/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
    getFieldDetails: function(component, event, helper) {
        helper.getFieldDetails(component, event);
    },
	/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
    tooglePannel: function(component, event, helper) {
        var layoutMeta=component.get("v.layoutMeta");
        for(var i=0;i<layoutMeta.length;i++){
            if(layoutMeta[i].sectionName==event.currentTarget.dataset.value){
                if(layoutMeta[i]['isOpen']){
                    layoutMeta[i]['isOpen']=false;
                }else{
                    layoutMeta[i]['isOpen']=true;
                }
            }
        }
        component.set("v.layoutMeta",layoutMeta);
	},
	onRender : function(component, event, helper) {
    		if(document.getElementById('User_Action__c')){
        	 document.getElementById('User_Action__c').style.display = 'none';
         }
    },
    handleUserActionChange: function(component, event, helper) {
		var userAction = event.getParam("userAction");
		var eventSource=event.getParam("eventSource");

        var emailJSON = event.getParam("emailJSON");
        var emailBody = event.getParam("emailBody");
        var sObject = component.get("v.sObject");
        var nameSpace = component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace = (nameSpace == 'c') ? '' : nameSpace + '__';

        if (eventSource == 'CaseDetailButton') {
            sObject[nameSpace + "User_Action__c"] = userAction;
            component.set("v.sObject", sObject);
            helper.fieldChangeHelper(component, event, helper, nameSpace + "User_Action__c");
            helper.validate(component, event);
            /*
            if(!Boolean(component.get("v.sObject")['hasError'])){
            }
            */

        }
        if (eventSource !== 'Email') {
            return false;
        }


        sObject[nameSpace + "User_Action__c"] = userAction;
        component.set("v.sObject", sObject);
        helper.fieldChangeHelper(component, event, helper, nameSpace + "User_Action__c");
        helper.validate(component, event);
        if (!Boolean(component.get("v.sObject")['hasError'])) {
            $("#caseTrackerSpinner").removeClass("slds-hide");
            var action = component.get("c.sendMailAndUpdateCase");
            console.log(action);
			 action.setParams({ emailJSON :emailJSON,emailBody:emailBody,caseManager:sObject });
			 action.setCallback(this, function(response) {
					$("#caseTrackerSpinner").addClass("slds-hide");
					var state = response.getState();
					if (state === "SUCCESS") {
						if(response.returnValue.ErrorMessage!=null){
							var appEvent = $A.get("e.c:ShowToast");
							appEvent.setParams({
								"title": "Error !",
								"message": response.returnValue.ErrorMessage,
								"type":"error"
							});
							appEvent.fire();
						}
						else{
							
							var appEvent = $A.get("e.c:ShowToast");
							appEvent.setParams({
                            "title": "Success !",
                            "message": "Case submitted successfully"
							});
							appEvent.fire();

							var compEvent = component.getEvent("caseAction");
							compEvent.setParams({
								"Action": "redirect",
								"Case": component.get("v.sObject")
							});
							compEvent.fire();
						}
					}else if (state === "INCOMPLETE") {
						//alert('INCOMPLETE');                
					}else if (state === "ERROR") {
						//alert('ERROR'); 
						var errors = response.getError();
						if (errors) {
							if (errors[0] && errors[0].message) {
								console.log("Error message: " + 
										 errors[0].message);
							}
						} else {
							console.log("Unknown error");
						}
					}
		     });
			 $A.enqueueAction(action);
        }
		else{
			//$A.util.addClass(component.find("caseDetails"), "slds-hide");
			//$A.util.removeClass(component.find("openCaseDetailsContainer"), "slds-hide");
			//$A.util.addClass(component.find("CloseBtn"), "slds-hide");
        
			//component.set("v.isOpen",false);
		}
		//alert("User Action Change=>"+userAction);
	}
})