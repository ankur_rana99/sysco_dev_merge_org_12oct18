({
	 doinit : function(component,event, helper){ 
         var intials = component.get("v.Interaction.fromName");
		if (intials) {
			intials = intials.charAt(0).toUpperCase();
			//intials='A';
			component.set("v.initialName", intials);
		}
         var interDt = component.get("v.Interaction.createdDate");
		var interDate = new Date(interDt);
		var today = new Date();
		var isToday = (today.toDateString() == interDate.toDateString());
		if (isToday) {
			var todayAMPM = helper.formatAMPM(interDate);
			//console.log(todayAMPM);
			component.set("v.interItemDate", todayAMPM);
		} else {
			var timeDiff = Math.abs(today.getTime() - interDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			if (diffDays < 7) {
				var weekDay = helper.foramteWeekDate(interDate);
				component.set("v.interItemDate", weekDay);
			} else {
				component.set("v.interItemDate", helper.formatDate(interDate));
			}
		}
    },
})