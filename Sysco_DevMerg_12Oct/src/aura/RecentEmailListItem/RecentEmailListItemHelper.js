/*
    Name           : RecentEmailListItemHelper.js
    Author         : Ashish Kr.
    Description    : InteractionListItem helper used for displaying Particular Interaction Item
    JIRA ID        : PUX-15 
*/
({
	/* Start : Formatiing date based on Interaction Time */
	formatAMPM: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? ' PM' : ' AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	},
	foramteWeekDate: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? ' PM' : ' AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		var weekDy = days[date.getDay()];
		var strTime = weekDy + ' ' + hours + ':' + minutes + ' ' + ampm;
		return strTime;
	},
	formatDate: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		var year = date.getFullYear();
		return month + "/" + day + "/" + year + ' ' + hours + ':' + minutes + ' ' + ampm;
	},
	/* End : Formatiing date based on Interaction Time */
})