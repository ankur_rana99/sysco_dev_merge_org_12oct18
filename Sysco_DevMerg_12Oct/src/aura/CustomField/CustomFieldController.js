({
    doInit : function(component, helper) {
       	var invObj = component.get('v.customObject');
        var FieldName = component.get('v.fieldName');
        var outputText = component.find("outputTextId");
        if(FieldName != undefined) {
            var outputVal = '';
            if(invObj[FieldName] != undefined){
            	outputVal = invObj[FieldName];
        	}
        	outputText.set("v.value",FieldName+': '+outputVal);
    	}
    }
})