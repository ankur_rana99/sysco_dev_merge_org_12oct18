({
    doInit: function(component, event, helper) {},
    toggleSubCase: function(component, event, helper) {
		var namespace  = component.get("v.namespace");
        try {
            var subCaseContainer = component.find("subCaseContainer");
            var toggleBtn = component.find("toggleBtn");
            if (toggleBtn.get('v.iconName') == 'utility:chevronright') {
                toggleBtn.set('v.iconName', 'utility:switch');
            } else {
                toggleBtn.set('v.iconName', 'utility:chevronright');
            }
            $A.util.toggleClass(subCaseContainer, 'hide');
        } catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
    },

    selectAttachment: function(component, event, helper) {
        helper.attachmentHelper(component, event, helper);
    },
    handleAttachments: function(component, event, helper) {
		var namespace  = component.get("v.namespace");
        try {
            if (component.get("v.isFirstCall")) {
                component.set("v.isFirstCall", false);
                var attachmentList = component.get('v.attachments');
                var attachmentLabel = component.find("checkboxMenuLabel");
                if (attachmentList.length > 0) {
                    attachmentLabel.set("v.label", "All Attachments Selected");
                    component.set("v.selectedAttachments", component.get('v.attachments'));
                }
            }
        } catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
    }
})