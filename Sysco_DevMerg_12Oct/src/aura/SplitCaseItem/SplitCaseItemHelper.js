({
    attachmentHelper: function(component, event, helper) {
		 var namespace  = component.get("v.namespace");
		 try{
			
			 var allMenuItems = component.find("checkboxList");
			 var allAttachments = component.get('v.attachments');
			 var selectedAttachments = [];

			 if(allAttachments.length==1){
				if(allMenuItems.get("v.selected")){
					 $.each(allAttachments,function(innerIndex,attachment){
							if(attachment.ContentDocument.Title == allMenuItems.get("v.label")){
								selectedAttachments.push(attachment);
							}	
					});
				}
			 }
			 else{
				 $.each(allMenuItems,function(index,menuItem){
					if (menuItem!=null && menuItem.get("v.selected")){
						 $.each(allAttachments,function(innerIndex,attachment){
							  if(attachment.ContentDocument.Title == menuItem.get("v.label")){
								  selectedAttachments.push(attachment);
							  }	
						 });
					}
				 });
			 }
			 component.set("v.selectedAttachments",selectedAttachments);

			var resultCmp = component.find("checkboxMenuLabel");
			if (selectedAttachments.length == allAttachments.length) {
				resultCmp.set("v.label", 'All Attachments Selected');
			} else if (selectedAttachments.length == 0) {
				resultCmp.set("v.label", 'Select Attachments');
			} else if (selectedAttachments.length == 1) {
				resultCmp.set("v.label", selectedAttachments[0].ContentDocument.Title);
			} else if (selectedAttachments.length >= 2) {
				resultCmp.set("v.label", selectedAttachments.length + ' Attachments Selected');
			}
		}
		catch (e) {
             this.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
	},
	showToast: function(component, nameSpacePerfix, title, message, msgtype){
		nameSpacePerfix=nameSpacePerfix==''?'c':'CoraPLM';
        var tosterError = $A.get("e."+nameSpacePerfix+":ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
    },
})