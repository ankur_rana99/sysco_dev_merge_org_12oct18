({
    getLookUpValues: function(component, event, helper) {
        var action = component.get("c.getLookupValue");
        action.setParams({
            objType: component.get("v.fieldDetails").relationship,
            whereClause: ''
        });
        action.setCallback(helper, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.lookupValues', response.getReturnValue().value);
                var values = response.getReturnValue().value; 
                for (var i = 0; i < values.length; i++) {
                    if (component.get("v.ref") == values[i].Id) {
                        component.set("v.searchValue",values[i].Name)
                    }
                }
                component.set('v.fieldName', response.getReturnValue().fldName);
                component.set('v.fieldLabel', response.getReturnValue().fldLabel);
                component.set('v.nooflabels', component.get('v.fieldLabel').length);
                var listval = [];
                var values = response.getReturnValue().value;
                var fldName = response.getReturnValue().fldName;
                for (var i = 0; i < values.length; i++) {
                    var v1 = {};
                    v1['Id'] = values[i]['Id'];
                    v1['Name'] = values[i]['Name'];
                    for (var j = 1; j <= fldName.length; j++) {
                        v1['field' + j] = values[i][fldName[j - 1]];
                    }
                    listval.push(v1);
                }
                component.set('v.listValues', listval);
            } else if (state === "INCOMPLETE") {
                console.log('e');
            } else if (state === "ERROR") {
                console.log('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
	formatValue: function(component, event, helper) {
		//alert(component.find(inputField));
		//var fieldName =component.get("v.value");
	},
    afterRender: function (component,event, helper) {
        
		var val=component.get("v.ref");
		//alert(val);
		if (val!='' && val != undefined && component.get("v.type") == "TIME" && !val.toString().includes(":")) {
            //alert(val);
			  var hours = val / (1000*60*60);
			  var absoluteHours = Math.floor(hours);
			  var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
			  //Get remainder from hours and convert to minutes
			  var minutes = (hours - absoluteHours) * 60;
			  var absoluteMinutes = Math.floor(minutes);
			  var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;

			   //Get remainder from minutes and convert to seconds
			  var seconds = (minutes - absoluteMinutes) * 60;
			  var absoluteSeconds = Math.floor(seconds);
			  var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;


			  var time= h + ':' + m ;
			  component.set("v.ref",time);
			  
		}
		
	},
    fieldChangeHelper: function(component, event, helper) {
	//debugger;
        var fieldName =component.get("v.value");
		var layoutJSON=component.get("v.layoutJSON");
		var sobjValue=component.get("v.sObject");
		var layoutMeta=component.get("v.layoutMeta");
		var actionFields={};
		if(layoutJSON!=null && layoutJSON.rules!=null){
			$.each(layoutJSON.rules,function(index, value ){
				if(value.criterias!=null){
					var isMatch=helper.isMatchCriteria(value.criterias,sobjValue);
					if(isMatch){
						if(value.actions!=null){
							$.each(value.actions,function(innerIndex, innerValue ){
								actionFields[innerValue.field]=innerValue;
							});
						}
					}
				}
			});
		}
		if(layoutJSON!=null){
			$.each(layoutMeta,function(index, value ){
				$.each(value.relatedFields,function(indexInner, fieldDetails ){
					var actionField=actionFields[fieldDetails.fieldAPIName];
					if(actionField!=null){
						helper.executeMatchAction(actionField,fieldDetails);
					}
				});
			});

			$.each(layoutMeta,function(index, value ){
				$.each(value.relatedFields,function(indexInner, fieldDetails ){
					var actionField=actionFields[fieldDetails.fieldAPIName];
					if(actionField==null){
						helper.executeDefaultAction(fieldDetails);
					}
				});
			});

		}
		
		component.set("v.layoutMeta",layoutMeta);
        component.set("v.sObject",component.get("v.sObject"));
	},
	isMatchCriteria: function(criterias,sobjValue) {
		var isMatch=false;
		$.each(criterias,function(innerIndex, innerValue ){
			var fieldValue=sobjValue[innerValue.field];
			if(fieldValue!=null){
				if(innerValue.operator=='equals'){
					isMatch=fieldValue==innerValue.value;
				}
				else if(innerValue.operator=='not equal to'){
					isMatch=fieldValue!=innerValue.value;
				}
				else if(innerValue.operator=='less than'){
					isMatch=fieldValue>innerValue.value;
				}
				else if(innerValue.operator=='greater than'){
					isMatch=fieldValue<innerValue.value;
				}
				else if(innerValue.operator=='less or equal'){
					isMatch=fieldValue>=innerValue.value;
				}
				else if(innerValue.operator=='greater or equal'){
					isMatch=fieldValue<=innerValue.value;
				}
				else if(innerValue.operator=='contains in list'){
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=true;
							return;
						}
					});
				}
				else if(innerValue.operator=='does not contains in list'){
					isMatch=true;
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=false;
							return;
						}
					});
				}
			}
			if(isMatch==false){
				return false;
			}
		});
		return isMatch;
	},
	executeMatchAction:function(actionField,fieldDetails){
		if(actionField.action=='Hidden'){
			if($('#'+actionField.field).find(".errorMsgLbl").length==1){
				$('#'+actionField.field).find(".errorMsgLbl").remove();
			}
			fieldDetails.display='none';
			fieldDetails.fieldBehavior='';
			fieldDetails.errors='';
			$('#'+actionField.field).hide();
			$('#'+actionField.field).find('.slds-select').removeAttr('required');
			$('#'+actionField.field).find('.slds-input').removeAttr('required');

		}
		else if(actionField.action=='Editable'){
			if($('#'+actionField.field).find(".errorMsgLbl").length==1){
				$('#'+actionField.field).find(".errorMsgLbl").remove();
			}
			fieldDetails.display='';
			fieldDetails.fieldBehavior='Edit';
			fieldDetails.errors='';
			$('#'+actionField.field).show();
			$('#'+actionField.field).find('.slds-select').removeAttr('required');
			$('#'+actionField.field).find('.slds-input').removeAttr('required');
		}
		else if(actionField.action=='Mandatory'){
			fieldDetails.display='';
			fieldDetails.fieldBehavior='Required';
			$('#'+actionField.field).show();
			$('#'+actionField.field).find('.slds-select').prop('required',true);
			$('#'+actionField.field).find('.slds-input').prop('required',true);
		}
		else if(actionField.action=='Readonly'){
			if($('#'+actionField.field).find(".errorMsgLbl").length==1){
				$('#'+actionField.field).find(".errorMsgLbl").remove();
			}
			fieldDetails.display='';
			fieldDetails.fieldBehavior='Readonly';
			$('#'+actionField.field).show();
			$('#'+actionField.field).find('.slds-select').removeAttr('required');
			$('#'+actionField.field).find('.slds-input').removeAttr('required');
			fieldDetails.errors='';
		}
			
	},
	executeDefaultAction:function(fieldDetails){
		if(fieldDetails.defaultBehaviour=='Hidden'){
			if($('#'+fieldDetails.fieldAPIName).find(".errorMsgLbl").length==1){
				$('#'+fieldDetails.fieldAPIName).find(".errorMsgLbl").remove();
			}
			fieldDetails.display='none';
			fieldDetails.fieldBehavior='';
			fieldDetails.errors='';
			$('#'+fieldDetails.fieldAPIName).hide();
			$('#'+fieldDetails.fieldAPIName).find('.slds-select').removeAttr('required');
			$('#'+fieldDetails.fieldAPIName).find('.slds-input').removeAttr('required');
		}
		else if(fieldDetails.defaultBehaviour=='Edit'){
			if($('#'+fieldDetails.fieldAPIName).find(".errorMsgLbl").length==1){
				$('#'+fieldDetails.fieldAPIName).find(".errorMsgLbl").remove();
			}		
			fieldDetails.display='';
			fieldDetails.fieldBehavior='Edit';
			fieldDetails.errors='';
			$('#'+fieldDetails.fieldAPIName).show();
			$('#'+fieldDetails.fieldAPIName).find('.slds-select').removeAttr('required');
			$('#'+fieldDetails.fieldAPIName).find('.slds-input').removeAttr('required');
		}
		else if(fieldDetails.defaultBehaviour=='Mandatory'){
			fieldDetails.display='';
			fieldDetails.fieldBehavior='Required';
			$('#'+fieldDetails.fieldAPIName).show();
			$('#'+fieldDetails.fieldAPIName).find('.slds-select').prop('required',true);
			$('#'+fieldDetails.fieldAPIName).find('.slds-input').prop('required',true);
			
		}	
	}

})