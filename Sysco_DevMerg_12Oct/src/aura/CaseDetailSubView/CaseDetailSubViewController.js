/*
    Name           : CaseDetailSibViewController.js
    Author         : Parth Lukhi
    Description    : CaseDetailSibViewController JS used for Case Detail Subview part
    JIRA ID        : PUX-15 
*/
({

//It will be called at very first time when component render
	//To set fieldsets in list
	doInit: function(component, event, helper) {
		/*PUX-213*/
		var arrayOfFieldset = [];	
		arrayOfFieldset.push('StandardReadonly');
		arrayOfFieldset.push('StandardReadOnlyOneLine');
		component.set('v.fieldSetList',arrayOfFieldset);
		/*PUX-213*/
	},
	/* Start : acceptCase  function  is used for Accept button event*/
	acceptCase: function(component, event, helper) {
		helper.makeMeOwner(component, event, false);
	}
	/* End : acceptCase  function  is used for Accept button event*/
})