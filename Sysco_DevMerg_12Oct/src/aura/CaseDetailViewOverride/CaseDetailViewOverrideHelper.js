/*
    Name           : CaseDetailViewOverrideHelper.js
    Author         : Ashish Kr.
    Description    : Helper JS used for getting Case Detail View 
    JIRA ID        : 
*/
({
    getCaseDetail: function(component, event ,helper) {
		var namespace=component.get("v.namespace");
		var errorMessageVar = '';
		var addCaseDetailViewTitleVar='Case Detail View Override';
        var action = component.get("c.getCaseDetail");
        action.setParams({
            caseId: component.get("v.caseid")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Case", response.getReturnValue());
                component.set("v.renderComponents", true);

            } else{
				 if (state === "INCOMPLETE") {
					errorMessageVar = (namespace == '') ? $A.get("$Label.c.Error_Message_Incomplete") : $A.get("$Label.CoraPLM.Error_Message_Incomplete");
				
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
							errorMessageVar=errors[0].message;
                    }
                } else {
						errorMessageVar='Unknown error';
                }
            }
				helper.showToast(component, namespace, addCaseDetailViewTitleVar, errorMessageVar, 'error');
            }
        });
        $A.enqueueAction(action);
    },
    getLoginUser: function(component, event,helper) {
    
        //Start Code for PUX-221
		var namespace=component.get("v.namespace");
		var errorMessageVar = '';
		var addCaseDetailViewTitleVar='Case Detail View Override';
        
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.loginUserId", response.getReturnValue().UserId);
				component.set("v.namespace", namespace);
				component.set("v.sessionId", response.getReturnValue().UserSessionId);
				component.set("v.instanceName", response.getReturnValue().InstanceName);
				
                //End Code for PUX-221
            } else{
				 if (state === "INCOMPLETE") {
					errorMessageVar = (namespace == '') ? $A.get("$Label.c.Error_Message_Incomplete") : $A.get("$Label.CoraPLM.Error_Message_Incomplete");
				
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
							errorMessageVar=errors[0].message;
                    }
                } else {
						errorMessageVar='Unknown error';
                }
				}
				helper.showToast(component, namespace, addCaseDetailViewTitleVar, errorMessageVar, 'error');
            }
        });
        $A.enqueueAction(action);
    },
	showToast: function(component, nameSpacePerfix, title, message, msgtype){
        var tosterError = $A.get("e."+nameSpacePerfix+":ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
    }
    
})