/**
  * Authors		: Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : Helper File for newCaseGen.cmp 
  * Dependencies : PLMUtilitiesController.apxc
  * -------------------------------------------------
  *Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({
	 /*
	 * Authors: Atul Hinge
	 * Purpose: Create Layout for fields
	 * Dependencies:  NewCaseGenHelper.js
	 * 
     *Start : called upon case create button click , it calls CaseAction Event :create */
    createLayout: function(component, event, files) {
         component.set("v.showSpinner",true);
         var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
        //console.log(createCmpEvent);
        createCmpEvent.setParams({
			"objectType": "CaseManager__c",
			"fsName": "NewCase",
			"paramName": "case",
			"component": component,
			"size": 2,
			"callback": function(response) {
                 component.set("v.showSpinner",false);
				var breadCrumbEvent = component.getEvent("caseAction");
				breadCrumbEvent.setParams({
					"Action": "setBreadCrumb"
				});
				breadCrumbEvent.setParams({
					"TabName": "New Case"
				});
				breadCrumbEvent.fire();
                component.set("v.processingfields", response);
                      
			}
		});
		createCmpEvent.fire();

		//Added for New Field set start
		 var createCmpEvent1 = $A.get("e.c:GetFieldSetComponent");
        createCmpEvent1.setParams({
			"objectType": "CaseManager__c",
			"fsName": "Query_Classification",
			"paramName": "case",
			"component": component,
			"size": 2,
			"callback": function(response) {
				var breadCrumbEvent = component.getEvent("caseAction");
				breadCrumbEvent.setParams({
					"Action": "setBreadCrumb"
				});
				breadCrumbEvent.setParams({
					"TabName": "New Case"
				});
				breadCrumbEvent.fire();
                component.set("v.processingfields1", response);
                      
			}
		});
		createCmpEvent1.fire();
		//Added for New Field set end
	},
	createCaseHelper: function(component, event) {
        //debugger;
		 var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';

		var btn = event.getSource();
		btn.set("v.disabled",true);//Disable the button

       
		var compEvent = component.getEvent("caseAction");
        component.set("v.showSpinner",true);
        var caseObject=component.get("v.case");
                //   alert(nameSpace);
        caseObject[nameSpace+'Comments__c'] = component.get("v.casecomment");
		caseObject[nameSpace+'Closure_Comments__c'] = component.get("v.caseclosurecomment");
       // caseObject.CoraPLM__Comments__c = component.get("v.casecomment");
        //Code start for PUX-469
        caseObject[nameSpace+"Input_Source__c"]='Manual';
        //Code start for PUX-469
		//console.log(btn.get("v.label"));
		//console.log('Check is assigned -->',caseObject.Is_Assigned__c);
		if(btn.get("v.label")=='Create and Close')
		{
			caseObject.Create_and_ClosePCKLIST__c='true';
		}
	
		//console.log('caseObject'+caseObject);
		//console.log(caseObject);
        compEvent.setParams({
			"Action": "create",
			"Case": caseObject,
			"fromView": component.get('v.fromView'),
			"callback": function(response) {
				if(response && response != 'error'){
          component.set("v.case", response.getReturnValue()[0]);
				  component.set("v.fileUploadStatus",'create');
					btn.set("v.disabled",false);//Enable the button
          }else{
        	  		component.set("v.fileUploadStatus",'hold');
					component.set("v.showSpinner",false);
					btn.set("v.disabled",false);//Enable the button
				}	
			}
		});
		compEvent.fire();
	},
	/*	 End :  called upon case create button click*/
})