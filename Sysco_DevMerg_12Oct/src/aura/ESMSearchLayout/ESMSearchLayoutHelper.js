({
	getObjectName : function(component,event) {
		var searchConfigName = component.get("v.searchConfigName");
		var action = component.get("c.getObjectName");		
        action.setParams({ searchConfigName : searchConfigName });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.breadParent",response.getReturnValue());
				console.log(response.getReturnValue());
            }else if (state === "INCOMPLETE") {
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + 
                             //       errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})