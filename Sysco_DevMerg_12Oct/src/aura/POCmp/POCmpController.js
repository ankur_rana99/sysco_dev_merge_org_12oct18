({
	addPO : function(component, event, helper) {
		helper.addPOHelper(component, event);
	},
	getPurchaseOrders: function (cmp, event, helper) { 
 		helper.getPurchaseOrderHelper(cmp, event);
 	},
    sectionOne : function(component, event, helper) {
         var acc = component.find('HeaderSec');	 
			for(var cmp in acc) {
				$A.util.toggleClass(acc[cmp], 'slds-show');  
				$A.util.toggleClass(acc[cmp], 'slds-hide');  
	  		}
	},
	deleteField: function(component, event, helper) {
        helper.deleteFieldHelper(component, event);
    },
})