/*
* Authors	   :  Atul Hinge
* Date created :  14/11/2017
* Purpose      :  [PUX-30,PUX-27,PUX-270] This component Display attachment.
* Dependencies :  CaseDetailView.cmp,AttachmentListView.cmp,SubTabViewer.cmp
* JIRA ID      :  PUX-30,PUX-27,PUX-270
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: To start or close spiner */
     toggleSpinner: function (cmp, event) {
         var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
	
})