({
    /*Start : Harshil Added on 02/02/2018 For PUX-299 */
 /*   doInit : function(component, event, helper) {        
            component.set("v.sessionId", window.SessionId);  
        
	},
    scriptsLoaded : function(component, event, helper) {
    if (component.get("v.sessionId") != null && 
					component.get("v.sessionId") != undefined && 
                    component.get("v.sessionId") != ""){
        // debugger;
                   // alert('F : ' + window.SessionId);
                    $.cometd.init({
                             url: window.location.protocol+'//'+window.location.hostname+'/cometd/41.0/',
                             requestHeaders: { Authorization: 'OAuth '+window.SessionId},
                             appendMessageTypeToURL : false
                    });      
                    $.cometd.subscribe('/event/Case_Notifications_Event__e', function(message) {
                                 alert('Subscribed');
								 toastr.success(JSON.stringify(message.data.payload.Case_Id__c), 'Case Detail', {timeOut: 8000})
                                 
								 var msg='Created Case Id  :  ' + JSON.stringify(message.data.payload.Case_Id__c);                        
                                 console.log(msg);
                    });
                   
                }                                                    
	},
    */
    /*End : Harshil Added on 02/02/2018*/
	//to Display Email Screen
    toggleScreen : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
            component.set("v.Case",Case);
            component.set("v.InteractionId",params.interactionId);
            var title='';
            if(params.Action == 'reply'){
                title = 'Reply';
            }else if(params.Action == 'replyall'){
                title = 'Reply All';
            }else if(params.Action == 'forward'){
                title = 'Forward';
            }
            component.set("v.Action",title);
            component.set("v.afterSendCallback",params.callback);
            
        }
        component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));
        
    },
    //to update Case object record
    updateCase : function(component, event, helper) {

        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
             var callback1 = params.callback;
             
        }
        console.log(Case);
        var action = component.get("c.updateCaseApex");
        action.setParams({ ct : Case });
        action.setCallback(this, function(response) {
           callback1(response);
            var state = response.getState();
            if (state === "SUCCESS") {
            }else if (state === "INCOMPLETE") {
				//alert('INCOMPLETE');                
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
       });

       $A.enqueueAction(action);
	},
    //to update Case Owner field
    updateCaseOwner : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
            var callback1 = params.callback;
            
        }
               var action = component.get("c.updateOwner");
        action.setParams({ ct : Case });
        
        action.setCallback(this, function(response) {
            callback1(response);
            var state = response.getState();
            //alert(response.getState());
            
            if (state === "SUCCESS") {
            }else if (state === "INCOMPLETE") {
                alert('INCOMPLETE');                
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //to create New Case object record
    createCase : function(component, event, helper) {
	 console.log('createCase');
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
			 var callback1 = params.callback;
        }
        var action = component.get("c.createCaseApex");
        action.setParams({ ct : Case});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var message='';
            console.log(state);

            if (state === "SUCCESS") {
                 message = response.getReturnValue()[0].Name+ " Created Successfully";
				helper.showToast(component,'Create Case',message);
                callback1(response);
            }else if (state === "INCOMPLETE") {
				//alert('INCOMPLETE');                
				callback1(response);
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
					helper.showToast(component,'Error in CC','Case Creation Failed');
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                    callback1(response);
                } else {
                    console.log("Unknown error");
                }
            }
       });
    	if(Case.Subject__c && Case.Subject__c.length >255){
    		helper.showToast(component,'Validation Error','Subject is too long.','error');
    		callback1('error');
    	}else{
    		$A.enqueueAction(action);
    	}
       
	},
     //to display success message
    showToast: function(component, event, helper) {
        console.log('');
        helper.showToast(component,event.getParam('title'),event.getParam('message'),event.getParam('type'));
        
    },
    closeToast: function(component, event, helper) {
		component.set("v.showMessage",!component.get("v.showMessage"));
     },
    getFieldSetMember : function(component, event, helper) {
      	helper.getFieldSetMember(component, event);
     },
    getFieldSetComponentHandler: function(component, event, helper) {
       component.getFieldSetMember(event.getParam('objectType'),event.getParam('fsName'),event.getParam('paramName'),event.getParam('size'),event.getParam('component'),event.getParam('callback'));
     },
    //to validate inputs before going to save
     validateInputs: function(component, event, helper) {
         helper.validateUtility(event.getParam('components'),event.getParam('callback'),null);
      },

})