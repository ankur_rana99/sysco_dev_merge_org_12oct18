({
	getManageJsonColumns: function(component, event) {
		var sectionsJson = component.get("v.layoutJson");
		if(sectionsJson != undefined && (sectionsJson.headerSection != undefined || sectionsJson.additionalHeaderSection != undefined) && (sectionsJson.headerSection.active || sectionsJson.additionalHeaderSection.active)) {
			var headerSection = [];
			if(sectionsJson.headerSection != undefined && sectionsJson.headerSection.active)
			{
				headerSection[0] = JSON.stringify(sectionsJson.headerSection.columns);
				if(sectionsJson.additionalHeaderSection != undefined && sectionsJson.additionalHeaderSection.active)
				{
					headerSection[1] = JSON.stringify(sectionsJson.additionalHeaderSection.columns);
				}
			}

			var action1 = component.get("c.getJsonBinding");

			//Set the Object parameters and Field Set name 
			action1.setParams({
				objName: component.get("v.objName"),
				//currentObject: component.get("v.currentObject"),
				jsonObj: headerSection
			});
			action1.setCallback(this, function(response) {
				if (response.getState() === 'SUCCESS') {
					var columns = response.getReturnValue().Columns;
					for (var i = 0; i < columns.length; i++) {
						if(sectionsJson.headerSection != undefined && sectionsJson.headerSection.active)
						{
							for (var k = 0; k < sectionsJson.headerSection.columns.length; k++) { 
								if(sectionsJson.headerSection.columns[k].name == columns[i].name)
								{
									sectionsJson.headerSection.columns[k].metaData = undefined;
									sectionsJson.headerSection.columns[k].metaData = columns[i].metaData;
									sectionsJson.headerSection.columns[k].criteria = JSON.parse(sectionsJson.headerSection.columns[k].criteria);
									break;
								}
							}
						}
						if(sectionsJson.additionalHeaderSection != undefined && sectionsJson.additionalHeaderSection.active)
						{
							for (var k = 0; k < sectionsJson.additionalHeaderSection.columns.length; k++) { 
								if(sectionsJson.additionalHeaderSection.columns[k].name == columns[i].name)
								{
									sectionsJson.additionalHeaderSection.columns[k].metaData = undefined;
									sectionsJson.additionalHeaderSection.columns[k].metaData = columns[i].metaData;
									sectionsJson.headerSection.columns[k].criteria = JSON.parse(sectionsJson.headerSection.columns[k].criteria);
									break;
								}
							}
						}
					}
					component.set("v.layoutJsonNew",sectionsJson);
					component.set("v.currentObject",component.get("v.currentObject"));
				} else if (response.getState() === 'ERROR') {
					var errors = response.getError(response.getReturnValue());
					if (errors) {
						if (errors[0] && errors[0].message) {
							alert("Error message header: " + 
								errors[0].message);
						}
					} else {
						alert("Unknown error");
					}
				} else {
					alert('Something went wrong, Please check with your admin');
				}
			});
			$A.enqueueAction(action1);
		}
    },

	helperFun : function(component,event,secId) {
	 var acc = component.find(secId);	 
			for(var cmp in acc) {
			$A.util.toggleClass(acc[cmp], 'slds-show');  
			$A.util.toggleClass(acc[cmp], 'slds-hide');  
	   }
	},
})