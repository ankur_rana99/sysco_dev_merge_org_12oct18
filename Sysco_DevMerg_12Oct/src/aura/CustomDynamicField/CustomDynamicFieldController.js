({
	doInit : function(component, helper) {
	
       	var invObj = component.get('v.currentObject');
        var fieldName = component.get('v.fieldName') != undefined && component.get('v.fieldName') != null && component.get('v.fieldName') != "" ? component.get('v.fieldName') : component.get("v.fieldMetadata.name");
		if(component.get("v.fieldMetadata.type") == "reference") {
			fieldName = fieldName.substring(0, fieldName.length - 3)+'__r';
		}
		var fieldLabel = component.get('v.labelRequired') ? (component.get('v.fieldLabel') != undefined ? component.get('v.fieldLabel') : component.get("v.fieldMetadata.label")) : '';
        var fieldValue = component.getReference("v.currentObject."+fieldName);
		component.set("v.fieldValue",fieldValue);
		component.set("v.fieldLabel",fieldLabel);
		component.set("v.fieldName",fieldName);
		if(component.get("v.fieldMetadata.type") != undefined && component.get("v.fieldMetadata.type") != ""){
			if(component.get("v.fieldMetadata.type") == "string") {
				component.set("v.uiDataType", "text");
			} else if(component.get("v.fieldMetadata.type") == "boolean") {
				component.set("v.uiDataType", "checkbox");
			/*} else if(component.get("v.fieldMetadata.type") == "date") {
				component.set("v.uiDataType", "date");*/
			} else if(component.get("v.fieldMetadata.type") == "datetime") {
				component.set("v.uiDataType", "datetime-local");
			} else if(component.get("v.fieldMetadata.type") == "url") {
				component.set("v.uiDataType", "url");
			} else if(component.get("v.fieldMetadata.type") == "email") {
				component.set("v.uiDataType", "email");
			}
		} else {
			component.set("v.uiDataType", "text");
		}
		//--- reading criteria ---//
		var action = component.get("c.handleRefresh");
		$A.enqueueAction(action);
		//--- Setting required attribute ---//
       
		
        
		//--- For Picklist ---//
		if(component.get("v.fieldMetadata.type") == "picklist" || component.get("v.fieldMetadata.type") == "multipicklist") {
			var options = [];
			var option = new Object();
			option.value = "";
			option.label = '---select---';
			options[0] = option;
			for (var key in component.get("v.fieldMetadata.picklistValues")) {
				option = new Object();
				option.value = component.get("v.fieldMetadata.picklistValues")[key]['value'];
				option.label = component.get("v.fieldMetadata.picklistValues")[key]['label'];
				var field_value = component.get("v.currentObject."+fieldName);
				if(field_value != undefined && field_value.includes(option.value)){
					option.selected = true;
				} else {
					option.selected = false;
				}
				options[Number(Number(key) + 1)] = option;
			}
			component.set("v.options", options);
			component.set("v.parentField", component.get("v.fieldMetadata.controllerName"));
		}
		if((component.get("v.fieldMetadata.type") == "picklist" || component.get("v.fieldMetadata.type") == "multipicklist") && component.get("v.onloadEvt") && component.get("v.fieldMetadata.controllerName") != null && component.get("v.fieldMetadata.controllerName")!=undefined && component.get("v.fieldMetadata.controllerName") != ''){
			var compEventonload = $A.get("e.c:onLoadPicklist");
			compEventonload.setParams({
				"controller": component.get("v.fieldMetadata.controllerName"),
			});
			compEventonload.fire();
			component.set("v.onloadEvt",false);
		}

        
    },
	
	handleRefresh : function(component, event, helper) {
		if(component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if(criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0)  {
                component.set("v.render",helper.validatecriteria(criJson,component.get("v.parentObject"),component.get("v.parentMetadata")));
				if(component.get("v.render") == true) {
					var field = component.get("v.fieldCriteria");
					field.className = "slds-show";
					component.set("v.fieldCriteria", field);
				}
				if(component.get("v.render") == false) {
					var field = component.get("v.fieldCriteria");
					field.className = "slds-hide";
					component.set("v.fieldCriteria", field);
				}
            }
        }
		if(component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
            if(criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                component.set("v.required",helper.validatecriteria(criJson,component.get("v.parentObject"),component.get("v.parentMetadata")));
            }
        } else {
			var required = component.get('v.fieldCriteria.required') != undefined ? component.get('v.fieldCriteria.required') : component.get('v.required');
			component.set("v.required",required);
		}
		//--- Setting readonly attribute ---//
		console.log(component.get("v.fieldName")+'==='+component.get("v.fieldCriteria")["readonlyCriteria"]);
		if(component.get("v.fieldCriteria")["readonlyCriteria"] != null && component.get("v.fieldCriteria")["readonlyCriteria"] != undefined && component.get("v.fieldCriteria")["readonlyCriteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["readonlyCriteria"];
			console.log(component.get("v.fieldName")+'==='+criJson.group.rules.length);
			if(criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
				console.log(component.get("v.fieldName"));
				console.log("parentObject:"+component.get("v.parentObject"));
				console.log("parentObject:"+component.get("v.parentMetadata"));
				console.log(component.get("v.fieldName")+'==='+helper.validatecriteria(criJson,component.get("v.parentObject"),component.get("v.parentMetadata")));
                component.set("v.readonly",helper.validatecriteria(criJson,component.get("v.parentObject"),component.get("v.parentMetadata")));
			}
        } else {
			var readonly = component.get('v.readonly') != undefined ? component.get('v.readonly') : (component.get('v.fieldCriteria.readonly') != undefined && component.get('v.fieldCriteria.readonly') != "" ? component.get('v.fieldCriteria.readonly') : false);
			component.set("v.readonly",readonly);
			
		}
		
		if(window.globalvalues && window.globalvalues.pagemode == 'VIEW') {
			component.set("v.readonly",true);
		}
		var inputclassname = "";
		if(!component.get("v.labelRequired")){
			inputclassname = "no-label ";		
		}
		if(component.get("v.required")) {
			inputclassname += "is-required";
		}
        component.set("v.inputclassname", inputclassname);
	},

    validate : function(component, event) {
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
		var isValid = true;
		if(component.get("v.render") && component.get("v.required")) {
			if(component.get("v.fieldMetadata.type") == "date") {
			}
			else if(component.get("v.fieldMetadata.type") == "reference") {
				var refId = component.find('inputFieldRef');	
				if(refId != null && refId != undefined && refId.validate != null && refId.validate != undefined){
					refId.validate(function(value){
						if (!value) {
							isValid = false;
						}
					});
				}
			} else if(component.get("v.fieldMetadata.type") == "multipicklist" && (component.find("inputField").get("v.value") == null || component.find("inputField").get("v.value") == '')) {
				isValid = false;
			} else {
				if(component.find("inputField") != null && component.find("inputField") != undefined) {
					var allValid = component.find("inputField").get("v.validity");
					component.find("inputField").showHelpMessageIfInvalid();
					isValid = allValid.valid;
				}
			}
			if (callback) {
				callback(isValid);
			}
		} else {
            if (callback) {
                callback(true);
            }
		}
	},
	firePicklistEvent: function(component, event, helper) {
        var parKey = 0;
        for (var key in component.get("v.fieldMetadata.picklistValues")) {
            if (component.get("v.fieldValue") === component.get("v.fieldMetadata.picklistValues")[key]['value']) {
                parKey = key;
                break;
            }
        }
		var compEvent = $A.get("e.c:PicklistChange");
        compEvent.setParams({
            "eventByObj": component.get("v.fieldMetadata.name"),
            "selectedValue": component.get("v.fieldValue"),
            "parKey": parKey,
        });
        compEvent.fire();
        var RefreshEvent = $A.get("e.c:Refresh");
        if (component.get("v.fieldCriteria")["callOnChangeManager"] != undefined && component.get("v.fieldCriteria")["callOnChangeManager"] == true) {
            RefreshEvent.setParams({
                "callOnChangeManager": true,
                "fieldName": component.get("v.fieldMetadata.name")
            });
        }
        RefreshEvent.fire();
    },
	handleFirePicklistEvent: function(component, event, helper) {
        var eventByObj = event.getParam("eventByObj");
        var selectedValue = event.getParam("selectedValue");
        var parKey = event.getParam("parKey");
        var retvalue = [];
        var obj1 = new Object();
        obj1.value = '';
        obj1.label = '---select---';
        retvalue[0] = obj1;
        if (eventByObj == component.get("v.parentField") && component.get("v.parentField") != undefined) {
            //validFor
            var cnt = 1;
            for (var key in component.get("v.fieldMetadata.picklistValues")) {
                try {
                    var obj = new Object();
                    var valifFor = component.get("v.fieldMetadata.picklistValues")[key]['validFor']
                    var decoded = atob(valifFor);
                    var index = Number(Number(parKey));
                    var bits = decoded.charCodeAt(index >> 3);
                    var vallue = ((bits & (0x80 >> (index % 8))) != 0);
                    //console.log('vallue--' + vallue);
                    if (vallue) {
                        //console.log('vallue--' + component.get("v.fieldMetadata.picklistValues")[key]['label']);
                        obj.value = component.get("v.fieldMetadata.picklistValues")[key]['value'];
                        obj.label = component.get("v.fieldMetadata.picklistValues")[key]['label'];
                        retvalue[Number(cnt)] = obj;
                        cnt++;
                    }
                } catch (err) {
                    console.log(err);
                }
            }
            var opts = retvalue;
            component.set("v.options", opts);
        }
    },
	handleonLoadPicklist: function(component, event) {	
	 var status= component.get("v.fieldValue");
	 var eventForObj = event.getParam("controller");
		if(status!=null && status!=undefined && status!='' && eventForObj == component.get("v.fieldMetadata.name")){
			var parKey = 0;
			for (var key in component.get("v.fieldMetadata.picklistValues")) {
				if (component.get("v.fieldValue") === component.get("v.fieldMetadata.picklistValues")[key]['value']) 
				{
					parKey = key;
					break;
				}
			}
			 var compEvent = $A.get("e.c:PicklistChange");
			compEvent.setParams({
				"eventByObj": component.get("v.fieldMetadata.name"),
				"selectedValue": component.get("v.fieldValue"),
				"parKey": parKey,
			});
			compEvent.fire();
        }
	},
	
})