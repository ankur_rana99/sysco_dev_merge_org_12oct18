({
	doInit : function(component, event, helper) {
    
		 var tempPageNo = component.get("v.tempPageNumber");
        component.set("v.pageNumber", tempPageNo);
    //    helper.getListViews(component,helper);
	    helper.getSampleFiles(component, event);
		helper.getHistoryDtl(component, event);
    },

	redirectToSobject: function (component, event, helper) {
	 var Id=event.currentTarget.dataset.id;
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": Id,
      "slideDevName": "Detail"
    });
    navEvt.fire();
},

	  handleActive : function(cmp, event, helper) {
        //Code start for PUX-358
		//helper.getViews(cmp, event, helper);
		console.log(event.getSource().get("v.accesskey"));
		cmp.set('v.selectedTabIdServer',event.getSource().get("v.accesskey"));
	
		
	//	cmp.set('v.sortField','LastModifiedDate');
	//	cmp.set('v.sortDirection','desc');
		//PUX-390
	
		var tab = event.getSource();
	
		console.log("Id "+tab.get('v.id'));
		
		cmp.set("v.selectedTabId", tab.get('v.id'));
      
		window.selectedTab=event.getSource().get("v.id");

		helper.getRecords(cmp,helper);
    },

	  renderPages: function(component, event, helper) {
        //Start : PUX-207, PUX-501	
        if (component.get("v.pageNumber") != component.get("v.tempPageNumber")) {
           	helper.getHistoryDtl(component, event);
        }
        //End : PUX-207, PUX-501	
    },

	
	validateFile :function(component,event, helper) {

	 component.set('v.historyDtl','');
	 component.set('v.historyDtl.Attachments','');
	  component.set('v.render',false);
	  var fileInput = component.find("file").getElement();
	console.log(fileInput);
	
    var file = fileInput.files[0];
	

	if(file=='' || file==undefined ||  file==null )
	{
	 var message="Please select a csv file.";
	helper.showToast(component,'error',message,"error");
	}
	else if(file.type!='application/vnd.ms-excel')
	{
	var message="Please select a csv file.";
	helper.showToast(component,'error',message,"error");
	}
	else
	{

var action = component.get("c.checkFileNameNew");

 action.setParams({
            strFileName : file.name,
			
        });

    action.setCallback(this, function(response){
        var state = response.getState();
        console.log(state);
        if (state === "SUCCESS") {
		if(response.getReturnValue())
		{
		 var message="Duplicate File. Please select another file.";
	    helper.showToast(component,'error',message,"error");
			
		}
		else
		{
			helper.readcsvFile(component,helper);
		}

             
        }
    });
    $A.enqueueAction(action);
       }



	  ////
	
	},
})