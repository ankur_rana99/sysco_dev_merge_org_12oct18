({
getSobjectDetails : function(component, event) {
	
		var action = component.get("c.getSObjectDetails");
		console.log("Record Id",component.get("v.recordId"));
		
		console.log("v.mainObject"+component.get("v.mainObject"));
		
		action.setParams({
					  
					  mainObj:component.get("v.mainObject"),			  					  
					  recId:component.get("v.recordId"),
					  
					}); 
		 action.setCallback(this, function(response) {	
		 var state =response.getState();
		 if (state === "SUCCESS") {
			console.log(response.getReturnValue());
					
			var allData = response.getReturnValue();
			console.log("allData:"+allData);
			var errInvDtlSec = allData[0].errmsg;
			var errInvPaySec = allData[1].errmsg;

			var esmNamespace = allData[0].esmNameSpace;
			console.log("esmNamespace"+esmNamespace);

			

			console.log("errInvDtlSec"+errInvDtlSec);
			console.log("errInvPaySec"+errInvPaySec);
			
			if(errInvDtlSec != null)
			{
			this.showToast(component,'Error in CC',errInvDtlSec,'error');
			}
			else if(errInvPaySec != null)
			{
			this.showToast(component,'Error in CC',errInvPaySec,'error');
			}
			else{
			var mainObj =allData[1].record;
			
			component.set("v.mainObject",mainObj);			
			console.log("v.mainObject",mainObj);
			console.log(mainObj.Total_Amount__c);
			component.set("v.totalamt", mainObj.Total_Amount__c);
			component.set("v.colsInvDtlSec", allData[0].Columns);			
			component.set("v.colsInvPaySec", allData[1].Columns);	
			console.log("v.colsInvDtlSec");			
			console.log("v.colsInvPaySec");
			
			var earlyPayDate=mainObj.Early_Payment_Date__c;
			component.set("v.myDate",earlyPayDate)

			console.log("earlyPayDate"+earlyPayDate);
			
			var chkpyterm=mainObj.Payment_Terms__c;
			var netduedt=mainObj.Net_Due_Date__c;
			var netamt=mainObj.Total_Amount__c;

			var today = new Date();

			var currDay =Date.parse(today);
			var netduedate=Date.parse(netduedt);
			console.log("chkpyterm",chkpyterm);
			console.log("netduedt",netduedt);
			console.log("netduedate",netduedate);
						
			
			if(chkpyterm == null || chkpyterm == ''){
			this.showToast(component,'Error in CC',$A.get("$Label.c.This_invoice_does_not_have_payment_terms"),'error');
			}
			else if(netduedt == null){
			
			this.showToast(component,'Error in CC',$A.get("$Label.c.This_invoice_does_not_have_Due_Date"),'error');
			}
			else if(netamt == '' || netamt == null){
			this.showToast(component,'Error in CC',$A.get("$Label.c.Invoice_is_in_credit_balance_hence_pay_me_early_is_not_possible"),'error');
			
			}
			else if(netduedate < currDay){
			this.showToast(component,'Error in CC',$A.get("$Label.c.This_invoice_is_past_due_Please_select_an_invoice_that_is_within_payment_term"),'error');
			
			}
			}
			
								
		  }
		  else if (state === "INCOMPLETE") {
				
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
					}
				}
				});	
		  $A.enqueueAction(action);
	},

helperFun : function(component,event,secId) {
	 var acc = component.find(secId);
			for(var cmp in acc) {
			$A.util.toggleClass(acc[cmp], 'slds-show');  
			$A.util.toggleClass(acc[cmp], 'slds-hide');  
	   }
	},

getcomponentdata : function(component, event) {	
	var amount=component.get('v.totalamt');
	console.log("amount:"+amount);
	var obj = component.get('v.mainObject');
	console.log("obj"+obj);
	var tot_amt=obj.Total_Amount__c;
	console.log("tot_amt"+tot_amt);
	
	var Netdate = new Date(obj.Net_Due_Date__c);
	console.log("date",Netdate);
			
		var InvoiceDate = new Date(obj.Invoice_Date__c);
		console.log("InvoiceDate",InvoiceDate);	

		var earlyPaymentDt = new Date(component.get("v.myDate"));
		console.log("earlyPaymentDt on HelperenderPage:",earlyPaymentDt);

		var PaymetntTerm =obj.Payment_Term__c;
		console.log("PaymetntTerm",PaymetntTerm);
		

		var btn = component.find('save');
		if(PaymetntTerm == null || PaymetntTerm == ''){
		this.showToast(component,'Error in CC',$A.get("$Label.c.This_invoice_does_not_have_payment_terms"),'error');
					 
			console.log("btn",btn);
			btn.set("v.disabled",true);
					
		}
		else if(Netdate == null || Netdate== ''){
		this.showToast(component,'Error in CC',$A.get("$Label.c.This_invoice_does_not_have_Due_Date"),'error');
					 
			console.log("btn",btn);
			btn.set("v.disabled",true);
					
		}
		else if(tot_amt == '' || tot_amt == null){
		this.showToast(component,'Error in CC',$A.get("$Label.c.Invoice_is_in_credit_balance_hence_pay_me_early_is_not_possible"),'error');
			console.log("btn",btn);
			btn.set("v.disabled",true);
		}
		
		else{
		
			var discperc1=0;
			var duepayDiff;
			var dueDateDiff;
			var buyerEffectiveRate;
			var discountAmt=0;
			var finalTotAmt=0;
			
					var today = new Date();
					var currDay =Date.parse(today);
					console.log("currDay",currDay);
					var invdt = Date.parse(InvoiceDate);
					console.log("invdt",invdt);
					var earlypaydt = Date.parse(earlyPaymentDt);
					console.log("earlypaydt",earlypaydt);
					var netduedate = Date.parse(Netdate);
					console.log("netduedate",netduedate);
					var btn = component.find('save');
					if(earlypaydt > netduedate){
					this.showToast(component,'Error in CC',$A.get("$Label.c.Early_payment_date_cannot_be_later_than_due_date"),'error');
					
					discperc1=0;
					discountAmt=0;
					finalTotAmt=0;
					console.log("Disper"+discperc1);
					component.set("v.mainObject.Discount_Percent__c",discperc1);
					component.set("v.mainObject.Discount_Amount__c",discountAmt);
					//component.set("v.mainObject.Total_Amount__c",obj.Total_Amount__c);
					component.set("v.mainObject.Total_Amount__c",tot_amt);
					 
					 console.log("btn",btn);
					 btn.set("v.disabled",true);
					 console.log("btn1",btn.get("v.disabled",true));
		  
					}
			
					if((earlypaydt < invdt) || (earlypaydt < currDay)){
					this.showToast(component,'Error in CC',$A.get("$Label.c.Early_payment_date_cannot_be_before_present_date_and_invoice_date"),'error');
					
					discperc1=0;
					discountAmt=0;
					finalTotAmt=0;
					console.log("Disper"+discperc1);
					component.set("v.mainObject.Discount_Percent__c",discperc1);
					component.set("v.mainObject.Discount_Amount__c",discountAmt);
					//component.set("v.mainObject.Total_Amount__c",obj.Total_Amount__c);
					component.set("v.mainObject.Total_Amount__c",tot_amt);
					 
					 console.log("btn",btn);
					 btn.set("v.disabled",true);
					 console.log("btn1",btn.get("v.disabled",true));
					
					}else if(earlypaydt < netduedate){

					component.set("v.messageType", '' );
					component.set("v.message", '' );
					var action = component.get("c.getDiscountVal");
					action.setParams({
					   recId: PaymetntTerm
					}); 
					action.setCallback(this, function(response) {
					var state =response.getState();

					if (state === "SUCCESS") {
					var tmp = response.getReturnValue();
					component.set("v.discountPer",tmp);
					discperc1=tmp;
					

					//Date Difference Between Invoice date and Early Payment Date
					 var invpayDiff = earlypaydt - invdt;
					
					var createDateDiff = Math.floor(invpayDiff / (1000 * 60 * 60 * 24));
					

					//Date Difference Between NetDuedate and Early Payment Date
			
					duepayDiff = netduedate - earlypaydt;
					 
					dueDateDiff = Math.floor(duepayDiff / (1000 * 60 * 60 * 24));					

					buyerEffectiveRate=discperc1*dueDateDiff;
										
					discountAmt=(amount*buyerEffectiveRate)/100;					
					
					finalTotAmt=amount - discountAmt;					

					component.set("v.mainObject.Discount_Percent__c",discperc1);
					component.set("v.mainObject.Discount_Amount__c",discountAmt);
					component.set("v.mainObject.Total_Amount__c",finalTotAmt);
					component.set("v.mainObject.Early_Payment_Date__c",earlyPaymentDt);
				
					 console.log("btn",btn);
					 btn.set("v.disabled",false);
					 console.log("btn1",btn.get("v.disabled",false));
					 console.log("tot_amt"+tot_amt);
					 console.log("finalTotAmt:"+finalTotAmt);
					 console.log("amount1:"+amount);
					}

					else if (state === "INCOMPLETE") {

					//Logic
					} else if (state === "ERROR") {
					
					var errors = response.getError();
					if (errors) {

						if (errors[0] && errors[0].message) {
							console.log("Error message: " + errors[0].message);
						}

					} else {

						console.log("Unknown error");
						}
					}

				
			});
			$A.enqueueAction(action);
			
					
					}else{
					console.log("no msg");
			
					}	
					}  
	},

updateinvdata : function(component,event) {

	var obj = component.get('v.mainObject');
	console.log("obj"+obj);	

	var Netdate = new Date(obj.Net_Due_Date__c);
	var InvoiceDate = new Date(obj.Invoice_Date__c);
	var earlyPaymentDt = new Date(component.get("v.myDate"));
	var today = new Date();
	var currDay =Date.parse(today);
	var invdt = Date.parse(InvoiceDate);
	var earlypaydt = Date.parse(earlyPaymentDt);
	var netduedate = Date.parse(Netdate);
	console.log("earlyPaymentDt",earlyPaymentDt);
	
	if (earlypaydt < netduedate){
		var action = component.get("c.updateInvoice");
		component.set("v.messageType", '' );
		component.set("v.message", '' );
		action.setParams({
			   invobject: obj,			   
			}); 
		action.setCallback(this, function(response) {	
		 var state =response.getState();
		 if (state === "SUCCESS") {
		 //this.showToast(component,'Error in CC',"Early Payment Saved Successfully...!!!",'error');
		 component.set("v.isDisplay",false);
	 
		 }
		 else if (state === "INCOMPLETE") {
				
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
					}
				}	
		 });
	 $A.enqueueAction(action);
	 
	 	
	/*var evt = $A.get("e.force:navigateToSObject");
	console.log("navigate method called");
    evt.setParams({
        "recordId": recId,	
    });
    evt.fire();*/

	 }
	},

showToast: function(component,title,message,type){
        //   console.log(type);
        var  type = type;
        if(type == undefined){
            type = 'success';
        }
        var  css = 'toast-top-center';
        var  msg = message;
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 5000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250;
        toastr.options.positionClass = css;
        toastr[type](msg);
    },

navigateToParentComponent : function(component, event, helper) {
/*	var obj = component.get('v.mainObject');
	var recId=obj.Id;
	console.log("recId"+recId);
    var evt = $A.get("e.force:navigateToSObject");
	console.log("navigate method called");
    evt.setParams({
        "recordId": recId,	
    });
    evt.fire();*/
	//$A.get('e.force:refreshView').fire();	
	component.set("v.isDisplay",false);
},

/*getobjNameSpace : function(component, event) {
	
		var action = component.get("c.getNamespace");
		action.setCallback(this, function(response) {	
		 var state =response.getState();
		 if (state === "SUCCESS") {
			console.log(response.getReturnValue());
					
			var nmspace = response.getReturnValue();
			console.log("nmspace"+nmspace);
		 }
		  else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
					}
				}
				});	
		  $A.enqueueAction(action);
	},
	   */ 
})