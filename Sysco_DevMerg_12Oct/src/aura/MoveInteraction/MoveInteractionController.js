({
    doInit : function(component, event, helper) {
        var CaseName = component.get("v.Case.Name");
        var action = component.get("c.getOwnerCases");
        
        action.setCallback(this,function(response){
            var state = response.getState();
            
            if(state=="SUCCESS"){
            	var casesList = response.getReturnValue();
            	for (var i = casesList.length - 1; i >= 0; --i) {
					    if (casesList[i].Name == CaseName) {
					        casesList.splice(i,1);
					        break;
					    }
					}
                component.set('v.caseList',casesList);
                console.log(component.get('v.caseList'));
            } else if(state=="ERROR"){
                var errors = response.getError();
                component.set("v.errorMessage", errors[0].message );
                var eerr = component.get("v.errorMessage");
                component.set("v.showError",true);
                
            }
        });
        $A.enqueueAction(action);
    },
    showList : function(component,event,helper){
    	var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "slds-hide");
         var options = component.get("v.caseList");
        var val = component.get("v.searchValue");
        console.log(val);
        var filterValues = [];
        for (var i = 0; i < options.length; i++) {
        	var cases = options[i];
            if (val!=null && cases.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                filterValues.push(options[i]);
            }
            else if(val == null){
            	filterValues.push(options[i]);
            }
        }
        component.set("v.filterdCaseList", filterValues);
        if (filterValues.length == 0 || event.getParams().keyCode==27) {
            
            var toggleText = component.find("listbox");
            //$A.util.toggleClass(toggleText, "show");
            //$A.util.removeClass(toggleText, "show");
            $A.util.addClass(toggleText, 'slds-hide');
            
        }
    },
    hideList : function(component,event,helper){
    	var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "slds-hide");
    },
    getSelectedValue : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        
        //component.set("v.ref", selectedItem.dataset.record);
        
        component.set("v.searchValue", selectedItem.dataset.name);
        component.set("v.searchCase", selectedItem.dataset.name);
        component.set("v.selectedName", selectedItem.dataset.record);
        console.log(component.get("v.selectedName"));
        
        var toggleText = component.find("listbox");
        
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "slds-hide");
    },
    assignCase : function(component, event, helper) {
        
        var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "slds-hide");
        var options = component.get("v.caseList");
        
        var val = component.get("v.searchValue");
        var filterValues = [];
        for (var i = 0; i < options.length; i++) {
            var cases = options[i];
            if (cases.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                filterValues.push(options[i]);
            }
        }
        component.set("v.filterdCaseList", filterValues);
        if (filterValues.length == 0 || event.getParams().keyCode==27) {
            
            var toggleText = component.find("listbox");
            //$A.util.toggleClass(toggleText, "show");
            //$A.util.removeClass(toggleText, "show");
            $A.util.addClass(toggleText, 'slds-hide');
            
        }
    },
    moveInteraction : function(component,event,helper){
        if(component.get('v.selectedName') && component.get('v.searchValue') && component.get('v.searchValue') == component.get('v.searchCase')){
        var action = component.get("c.moveInteractionToCase");
        action.setParams({
            "interactionId" : component.get("v.Interaction.Id"),
            "CaseId": component.get('v.selectedName'),
            "CaseNumber" : component.get('v.searchValue')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var message='';
            if(state=="SUCCESS"){
                message = response.getReturnValue();
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Success ",
                    "message": message
                });
                appEvent.fire(); 
                /*
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    "title": "Success!",
                    "message" : message,
                    "type" : "Success"
                });
				toastEvent.fire();
                */
                
                component.set('v.showModal',false);
                var refreshInteraction = $A.get('e.c:RefreshInteractionList');
                refreshInteraction.fire();
                 var refreshInteractionCountEvt = $A.get("e.c:RefreshInteractionCountEvent");
				        refreshInteractionCountEvt.fire();
                
            } else if(state=="ERROR"){
                
                var errors = response.getError();
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Success ",
                    "message": errors[0].message,
                    "type" : "error"
                });
                appEvent.fire(); 
                
                component.set("v.errorMessage", errors[0].message );
                var eerr = component.get("v.errorMessage");
                component.set("v.showError",true);
                
            }
        });
        $A.enqueueAction(action);
        }else{
        		var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Error ",
                    "message": 'Please select a case from the available cases!',
                    "type" : "error"
                });
                appEvent.fire(); 
        }
        
    },
    closeModal : function(component,event,helper){
        component.set('v.showModal',false);
    }
})