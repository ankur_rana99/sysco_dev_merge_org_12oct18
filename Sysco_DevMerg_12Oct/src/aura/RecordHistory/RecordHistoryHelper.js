({
	  getRecordHistory: function(component, event) {		
		console.log('recordcomp');
		console.log('sObjectName');
		console.log(component.get("v.sObjectName"));		
		var sObjectName =  component.get("v.sObjectName") ; 
		var recordId = component.get("v.obj.Id") ;   //'a1737000001QgK9AAK';
		//this.getInvoiceHistoryData(component, event, event.getParam("evtId"),sObjectName);
		this.getInvoiceHistoryData(component, event, recordId,sObjectName);
    },

	getInvoiceHistoryData : function(component, event,objId,sObjectName) {
		console.log('record history data component called');	
		console.log('record objId :' + objId);
		var curPageNo = component.get("v.pageNumber");
		var action1 = component.get("c.getRecordHistoryDataServer");
		//Set the Object parameters and Field Set name 
        action1.setParams({          
            ObjRecordId: objId,        
			sObjName : sObjectName ,  
			pageNumber: component.get("v.pageNumber"),
            recordsToDisplay: component.get("v.recordsToDisplay")
        });
        action1.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
				var resp = response.getReturnValue();
				if(resp.serverError != null && resp.serverError != ""){
					this.showToast("Record History Error!","error","sticky",resp.serverError);
				} else {
					console.log('res record history');
					console.log(response.getReturnValue());
					component.set("v.objList", response.getReturnValue().cases);
					console.log('objLis:'+component.get("v.objList"));
					component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / response.getReturnValue().pageSize));
					//Start : PUX-504
					component.set("v.totalRecords", response.getReturnValue().total);
				}
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
						this.showToast("Validation Alert!","error","sticky","Error message: " + errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            } else {
                alert('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);
		
	},
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},
})