({
    init: function(component, event, helper) {
        var fieldname = component.get("v.fieldMetadata.name");
        if((window.globalvalues && window.globalvalues.pagemode == 'VIEW') || (component.get("v.pagemode") == 'VIEW')) {
            component.set("v.readonly", true);
            component.set("v.required", false);
        }
        //--- reading criteria ---//
        var action = component.get("c.handleRefresh");
        $A.enqueueAction(action);
        //--- Setting required attribute ---//
        if (fieldname != null && fieldname != undefined) {
            fieldname = fieldname.substring(0, fieldname.length - 3);
            component.set("v.selectedRecord", component.getReference("v.obj." + fieldname + "__r"));
            component.set("v.actualId", component.getReference("v.obj." + component.get("v.fieldMetadata.name")));
            if (component.get("v.selectedRecord") != null && component.get("v.selectedRecord") != undefined && component.get("v.selectedRecord") != '') {
                var forclose = component.find("lookup-pill");
                $A.util.addClass(forclose, 'slds-show');
                $A.util.removeClass(forclose, 'slds-hide');
                
                var forclose = component.find("searchRes");
                $A.util.addClass(forclose, 'slds-is-close');
                $A.util.removeClass(forclose, 'slds-is-open');
                
                var lookUpTarget = component.find("lookupField");
                $A.util.addClass(lookUpTarget, 'slds-hide');
                $A.util.removeClass(lookUpTarget, 'slds-show');
            }
        }
    },
    setParData: function(component, event) {
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        var record = component.get("v.selectedRecord");
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        component.set("v.SearchKeyWord", '');
        component.set("v.listOfSearchRecords", '');
        component.set("v.selectedRecord", '');
        component.set("v.lock", true);
        if (callback) callback(record);
    },
    onfocus: function(component, event, helper) {
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        var getInputkeyWord = component.get("v.SearchKeyWord");
        helper.searchHelper(component, event, getInputkeyWord);
    },
    keyPressController: function(component, event, helper) {
        var getInputkeyWord = component.get("v.SearchKeyWord");
        if (getInputkeyWord != undefined && getInputkeyWord.length > 0) {
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component, event, getInputkeyWord);
        } else {
            component.set("v.listOfSearchRecords", null);
            component.set("v.lock", true);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    clear: function(component, event, heplper) {
        component.set("v.lock", true);
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        component.set("v.SearchKeyWord", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", null);
        component.set("v.actualId", null);
        var RefreshEvent = $A.get("e.c:Refresh");
        if (component.get("v.fieldCriteria")!=null && component.get("v.fieldCriteria")["callOnChangeManager"]!=null && component.get("v.fieldCriteria")["callOnChangeManager"] != undefined && component.get("v.fieldCriteria")["callOnChangeManager"] == true) {
            
            RefreshEvent.setParams({
                "callOnChangeManager": true,
                "fieldName": component.get("v.fieldMetadata.name"),
                "fieldValue" : null,
            });
            
        }
        RefreshEvent.fire();
        component.set("v.isDirty",true);
    },
    handleComponentEvent: function(component, event, helper) {
        component.set("v.lock", true);
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        component.set("v.selectedRecord", selectedAccountGetFromEvent);
        if (component.get("v.selectedRecord") != null && component.get("v.selectedRecord") != undefined) {
            component.set("v.actualId", component.get("v.selectedRecord.Id"));
            component.set("v.isDirty",true);
            var RefreshEvent = $A.get("e.c:Refresh");
            if (component.get("v.fieldCriteria")!=null && component.get("v.fieldCriteria")["callOnChangeManager"]!=null && component.get("v.fieldCriteria")["callOnChangeManager"] != undefined && component.get("v.fieldCriteria")["callOnChangeManager"] == true) {
                
                RefreshEvent.setParams({
                    "callOnChangeManager": true,
                    "fieldName": component.get("v.fieldMetadata.name"),
                    "fieldValue" : component.get("v.actualId"),
                });
                
            }
            RefreshEvent.fire();
        }
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');
        
    },
    handleComponentBlurEvent: function(component, event, helper) {
        setTimeout(function() {
            helper.hideBox(component, event);
            component.set("v.lock", true);
        }, 50);
    },
    handleComponentBlurFrmBoxEvent: function(component, event, helper) {
        helper.hideBox(component, event);
    },
    hideSpinner: function(component, event, helper) {
        var spinner = component.find('spinner');
        if (spinner != null && spinner != undefined) {
            var evt = spinner.get("e.toggle");
            evt.setParams({
                isVisible: false
            });
            evt.fire();
        }
    },
    showSpinner: function(component, event, helper) {
        var spinner = component.find('spinner');
        if (spinner != null && spinner != undefined) {
            var evt = spinner.get("e.toggle");
            evt.setParams({
                isVisible: true
            });
            evt.fire();
        }
    },
    validate: function(component, event) {
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (component.get("v.render")) {
            var validRef = true;
            if (component.get("v.required")) {
                if (component.get("v.actualId") == null || component.get("v.actualId") == '' || component.get("v.actualId") == undefined) {
                    validRef = false;
                    var forclose = component.find("searchRes");
                    $A.util.addClass(forclose, 'slds-has-error');
                }
            }
            if (callback) callback(validRef);
        } else {
            if (callback) callback(true);
        }
    },
    handleRefresh: function(component, event, helper) {
        if (component.get("v.criteriaObj")!=null && component.get("v.criteriaObj")!=undefined && component.get("v.fieldCriteria") != null && component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                component.set("v.render", helper.validatecriteria(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                if (component.get("v.render") == true) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-show";
                    component.set("v.fieldCriteria", field);
                }
                if (component.get("v.render") == false) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-hide";
                    component.set("v.fieldCriteria", field);
                }
            }
        }
        if((window.globalvalues && window.globalvalues.pagemode != 'VIEW') || (component.get("v.pagemode")!= 'VIEW')) {
            if (component.get("v.criteriaObj")!=null && component.get("v.criteriaObj")!=undefined && component.get("v.fieldCriteria") != null && component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
                var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
                if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                    component.set("v.required", helper.validatecriteria(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                }
            }
            else {
                var required = component.get('v.fieldCriteria.required') != undefined ? component.get('v.fieldCriteria.required') : component.get('v.required');
                component.set("v.required",required);
            }
        }
        if((window.globalvalues && window.globalvalues.pagemode != 'VIEW') || (component.get("v.pagemode")!= 'VIEW')) {
            if (component.get("v.criteriaObj")!=null && component.get("v.criteriaObj")!=undefined && component.get("v.fieldCriteria") != null && component.get("v.fieldCriteria")["readonlyCriteria"] != null && component.get("v.fieldCriteria")["readonlyCriteria"] != undefined && component.get("v.fieldCriteria")["readonlyCriteria"] != "") {
                var criJson = component.get("v.fieldCriteria")["readonlyCriteria"];
                if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                    component.set("v.readonly",helper.validatecriteria(criJson,component.get("v.criteriaObj"),component.get("v.objFieldMetadata")));
                }
            }
            else {
                var readonly = component.get('v.readonly') != undefined ? component.get('v.readonly') : (component.get('v.fieldCriteria.readonly') != undefined && component.get('v.fieldCriteria.readonly') != "" ? component.get('v.fieldCriteria.readonly') : false);
                component.set("v.readonly",readonly);
            }
        }else {
            //component.set("v.readonly",true);
        }
    },
})