({
	searchHelper : function(component,event,getInputkeyWord) {
	  // call the apex class method 
	   var obj = '';
	  if(component.get("v.fieldMetadata.referenceTo")!=null && component.get("v.fieldMetadata.referenceTo")!=undefined 
		&& component.get("v.fieldMetadata.referenceTo")!=''){
		obj = component.get("v.fieldMetadata.referenceTo")[0];
	  }else{
		obj = component.get("v.objectAPIName");
	  }
	  var invoice = component.get("v.criteriaObj");
	  if(invoice!= null)
		invoice["sobjectType"] = 'Invoice__c';
     var action = component.get("c.fetchLookUpValues");
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : obj,
			'searchBy' : (component.get("v.searchBy")!=null && component.get("v.searchBy")!='' && component.get("v.searchBy")!=undefined) ? component.get("v.searchBy"):'Name',
			'searchFilter' : component.get("v.searchFilter"),
			'mainObject' :  invoice
          });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            } 
			else if (response.getState() === 'ERROR') 
			{
				var errors = response.getError(response.getReturnValue());
				if (errors) {
					if (errors[0] && errors[0].message) {
						   console.log(errors[0].message);

					}
				}
			}
        });
        $A.enqueueAction(action);
	},
	hideBox:function(component, event) {
		if (event.getType() == "ui:blur" && component.get("v.lock") && (component.get("v.selectedRecord") == null || component.get("v.selectedRecord") == undefined)) {
				var forclose = component.find("searchRes");
				$A.util.addClass(forclose, 'slds-is-close');
				$A.util.removeClass(forclose, 'slds-is-open');
		}
	},
	validatecriteria: function(criteria,model,fieldMetadata,throwException) {
		var exp = this.createGroupExpression(criteria,"",criteria.group.operator,model,fieldMetadata);
			var result = false;
			try{
				var result = (exp === "" || exp == "true") ? true : ((exp == "false") ? false : true);
			}catch(e){
				if(throwException){
					throw "Invalid Criteria!!";
				}
				result = false;
			}
			return result;
		},
		createRuleExpression: function(rule,model,fieldMetadata) {
			var exp = "";
			var conditionValue = true;
			if(rule.field){
				var modelValue = model[rule.field.name] ;
				var ruleValue = rule.data[rule.field.name];
				var type = fieldMetadata[rule.field.name].type;

				switch(type){ // double,currency,string,email,picklist,boolean,reference
				case "double":
				case "currency":
					modelValue 	= (modelValue) 	? modelValue 	: 0;
					ruleValue 	= (ruleValue) 	? ruleValue 	: 0;
					break;
					
				case "string":
				case "email":
				case "picklist":
					modelValue 	= (modelValue) 	? "'" + modelValue 	+ "'" : "''";
					ruleValue 	= (ruleValue) 	? "'" + ruleValue 	+ "'" : "''";
					break;

				case "reference":
					modelValue 	= (modelValue) 	? "'" + $filter('limitTo')(modelValue, 15, 0) 	+ "'" : "''";
					ruleValue 	= (ruleValue) 	? "'" + $filter('limitTo')(ruleValue, 15, 0) 	+ "'" : "''"
					break;
				
				case "boolean":
					modelValue 	= (modelValue) 	? modelValue 	: false;
					ruleValue 	= (ruleValue) 	? ruleValue 	: false;
					break;
				}
				switch(rule.condition){
				case "==":
					conditionValue = (modelValue == ruleValue);
					break;
				case "!=":
					conditionValue = (modelValue != ruleValue);
					break;
				case ">=":
					conditionValue = (modelValue >= ruleValue);
					break;
				case "<=":
					conditionValue = (modelValue <= ruleValue);
					break;
				case ">":
					conditionValue = (modelValue > ruleValue);
					break;
				case "<":
					conditionValue = (modelValue < ruleValue);
					break;
				}
			}
			return conditionValue;
		},
		createGroupExpression: function(criteria,exp,operator,model,fieldMetadata) {
			for(var i = 0; i < criteria.group.rules.length; i++){
				var rule = criteria.group.rules[i];
				if(rule.group){
					exp += this.createGroupExpression(rule,"",rule.group.operator,model,fieldMetadata);
				}else{
					exp += this.createRuleExpression(rule,model,fieldMetadata);
				}
				exp += " " + operator + " ";
			}
			exp = exp.slice(0,-4);
			if(exp !== ""){
				var arr = exp.split(" "+operator+" ");
				if(operator == "||")
				{
					var i = 0;
					for(i = 0; i < arr.length; i++){
						if(arr[i].trim() == "true")
						{
							exp = "true";
							break;
						}
					}
					if(i == arr.length)
					{
						exp = "false";
					}
				}
				else if(operator == "&&")
				{
					var i = 0;
					for(i = 0; i < arr.length; i++){
						if(arr[i].trim() == "false")
						{
							exp = "false";
							break;
						}
					}
					if(i == arr.length)
					{
						exp = "true";
					}
				}
			}
			return exp;
		},
})