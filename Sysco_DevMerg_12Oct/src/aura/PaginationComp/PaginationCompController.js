/*
  Authors	   :   Rahul Pastagiya
  Date created :   22/09/2017
  Purpose      :   Client side controller to privde logic for all component actions
  Dependencies :   None
  __________________________________________________________________________
  Modifications:
		    Date: 14-12-2017
			Changed By : Rahul Pastagiya
		    Purpose of modification:  PUX-504 - How many records on first page not mentioned
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To display first page in pagination
	*/
	firstPage: function(component, event, helper) {
		component.set("v.pageNumber", 1);
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: To display previous page relative to current selected page number
	*/
	prevPage: function(component, event, helper) {
		//component.set("v.direction", "prev");
		component.set("v.pageNumber", Math.max(parseInt(component.get("v.pageNumber")) - 1, 1));
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: To display next page relative to current selected page number
	*/
	nextPage: function(component, event, helper) {
		//component.set("v.direction", "next");
		component.set("v.pageNumber", Math.min(parseInt(component.get("v.pageNumber")) + 1, component.get(
			"v.maxPageNumber")));
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: To display last page in pagination
	*/
	lastPage: function(component, event, helper) {
		//component.set("v.direction", "last");
		component.set("v.pageNumber", component.get("v.maxPageNumber"));
	},
	enablePageNoField: function(component, event, helper) {
		component.set("v.enabledPageNoField", true);
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: To fecth records for entred page Number. It will work on entre and blur event
        else{
                 var message = $A.get("$Label.c.PaginationInvalidInput");
                 document.getElementById("currentPageNo").value = component.get('v.pageNumber');
                 var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success !",
                        "message": message,
						 "type": "warning"
                    });
                    appEvent.fire();
             }
	*/
	fetchPageNoRecord: function(component, event, helper) {
		if (event.keyCode == 13 || event.keyCode == undefined) {
			var curPageNo = document.getElementById("currentPageNo").value;
			if (curPageNo != '' && curPageNo.match(/^[1-9]\d*$/g) && parseInt(curPageNo) <= parseInt(component.get(
					'v.maxPageNumber'))) {
				if (parseInt(curPageNo) != parseInt(component.get('v.pageNumber'))) {
					component.set("v.pageNumber", document.getElementById("currentPageNo").value);
				}
				component.set("v.enabledPageNoField", false);
			} else {
				document.getElementById("currentPageNo").value = component.get('v.pageNumber');
			}
		}
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: PUX-504 : How many records on first page not mentioned
	*/
	setRecordValues: function(component, event, helper) {
		console.log('called');
		var displayString = '';
		var currentPageNo = component.get('v.pageNumber');
		var maxPageNo = component.get('v.maxPageNumber');
		var totalrecords = component.get('v.totalRecords');
		if (currentPageNo != maxPageNo) {
			displayString += parseInt(currentPageNo - 1) * 15 + 1;
			displayString += ' - ';
			displayString += parseInt(currentPageNo - 1) * 15 + 15;
		} else {
			displayString += parseInt(currentPageNo - 1) * 15 + 1;
			displayString += ' - ';
			displayString += totalrecords
		}
		displayString += ' of ';
		if (parseInt(totalrecords) > 2000) {
			displayString += '2000+';
		} else {
			displayString += totalrecords;
		}
		component.set('v.displayString', displayString);
	}
})