({
    init: function(component, event, helper) {
        if (window.globalvalues && window.globalvalues.pagemode == 'VIEW') {
            component.set("v.readonly", true);
			component.set("v.required", false);
        }
        if (component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                component.set("v.render", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                if (component.get("v.render") == true) {
                    var field = component.get("v.fieldCriteria");				
                    field.className = "slds-show";
                    component.set("v.fieldCriteria", field);
                }
                if (component.get("v.render") == false) {
                    var field = component.get("v.fieldCriteria");
					//console.log('field :');
					//console.log(field);
                    field.className = "slds-hide";
                    component.set("v.fieldCriteria", field);
                }
            }
        }
        if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
        var retvalue = [];
        var obj1 = new Object();
        obj1.value = "";
        obj1.label = '---select---';
        retvalue[0] = obj1;
        for (var key in component.get("v.fieldMetadata.picklistValues")) {
            var obj = new Object();
            obj.value = component.get("v.fieldMetadata.picklistValues")[key]['value'];
            obj.label = component.get("v.fieldMetadata.picklistValues")[key]['label'];
            retvalue[Number(Number(key) + 1)] = obj;
        }
        var opts = retvalue;
        component.set("v.options", opts);
        component.set("v.parentObject", component.get("v.fieldMetadata.controllerName"));
		if(component.get("v.onloadEvt")  && component.get("v.fieldMetadata.controllerName") != null && component.get("v.fieldMetadata.controllerName")!=undefined && component.get("v.fieldMetadata.controllerName") != ''){
			var compEventonload = $A.get("e.c:onLoadPicklist");
			compEventonload.setParams({
				"controller": component.get("v.fieldMetadata.controllerName"),
			});
			compEventonload.fire();
			component.set("v.onloadEvt",false);
		}
        component.set("v.status", component.getReference("v.obj." + component.get("v.fieldMetadata.name")));
    },
    firePicklistEvent: function(component, event, helper) {
        //component.set("v.status",component.get("v.status"));
        var parKey = 0;
        for (var key in component.get("v.fieldMetadata.picklistValues")) {
            if (component.get("v.status") === component.get("v.fieldMetadata.picklistValues")[key]['value']) {
                parKey = key;
                break;
            }
        }
        var compEvent = $A.get("e.c:PicklistChange");
        compEvent.setParams({
            "eventByObj": component.get("v.fieldMetadata.name"),
            "selectedValue": component.get("v.status"),
            "parKey": parKey,
        });
        compEvent.fire();
        var RefreshEvent = $A.get("e.c:Refresh");
        if (component.get("v.fieldCriteria")["callOnChangeManager"] != undefined && component.get("v.fieldCriteria")["callOnChangeManager"] == true) {
            RefreshEvent.setParams({
                "callOnChangeManager": true,
                "fieldName": component.get("v.fieldMetadata.name")
            });
        }
        RefreshEvent.fire();
    },
    handleFirePicklistEvent: function(component, event, helper) {
        var eventByObj = event.getParam("eventByObj");
        var selectedValue = event.getParam("selectedValue");
        var parKey = event.getParam("parKey");
        var retvalue = [];
        var obj1 = new Object();
        obj1.value = '';
        obj1.label = '---select---';
        retvalue[0] = obj1;
        if (eventByObj == component.get("v.parentObject") && component.get("v.parentObject") != undefined) {
            //validFor
            var cnt = 1;
            for (var key in component.get("v.fieldMetadata.picklistValues")) {
                try {
                    var obj = new Object();
                    var valifFor = component.get("v.fieldMetadata.picklistValues")[key]['validFor']
                    var decoded = atob(valifFor);
                    var index = Number(Number(parKey));
                    var bits = decoded.charCodeAt(index >> 3);
                    var vallue = ((bits & (0x80 >> (index % 8))) != 0);
                    //console.log('vallue--' + vallue);
                    if (vallue) {
                        //console.log('vallue--' + component.get("v.fieldMetadata.picklistValues")[key]['label']);
                        obj.value = component.get("v.fieldMetadata.picklistValues")[key]['value'];
                        obj.label = component.get("v.fieldMetadata.picklistValues")[key]['label'];
                        retvalue[Number(cnt)] = obj;
                        cnt++;
                    }
                } catch (err) {
                    console.log(err);
                }
            }
            var opts = retvalue;
            component.set("v.options", opts);
        }
    },

    handleRefresh: function(component, event, helper) {
        if (component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                component.set("v.render", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                if (component.get("v.render") == true) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-show";
                    component.set("v.fieldCriteria", field);
                }
                if (component.get("v.render") == false) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-hide";
                    component.set("v.fieldCriteria", field);
                }
            }
        }
        if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
    },
    validate: function(component, event) {	
        var params = event.getParam('arguments');	
        var callback;
        if (params) {
            callback = params.callback;
        }

       if(component.get("v.render"))
		{
			var allValid = component.find("formValidate").get("v.validity");			
			component.find("formValidate").showHelpMessageIfInvalid();
			if (callback) callback(allValid.valid);
		}else{
			if (callback) callback(true);
		}
    },
	 handleonLoadPicklist: function(component, event) {	
	 var status= component.get("v.status");
	 var eventForObj = event.getParam("controller");
		if(status!=null && status!=undefined && status!='' && eventForObj == component.get("v.fieldMetadata.name")){
			var parKey = 0;
			for (var key in component.get("v.fieldMetadata.picklistValues")) {
				if (component.get("v.status") === component.get("v.fieldMetadata.picklistValues")[key]['value']) 
				{
					parKey = key;
					break;
				}
			}
			 var compEvent = $A.get("e.c:PicklistChange");
			compEvent.setParams({
				"eventByObj": component.get("v.fieldMetadata.name"),
				"selectedValue": component.get("v.status"),
				"parKey": parKey,
			});
			compEvent.fire();
        }
	},

})