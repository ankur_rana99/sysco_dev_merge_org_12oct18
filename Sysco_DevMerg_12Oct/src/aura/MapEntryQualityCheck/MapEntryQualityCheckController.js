/*
Authors: Niraj Prajapati
Date created: 06/11/2017
Purpose: This controller will get value from Map and set UI display/format attribute
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/ 

({
	/*
		Authors: Niraj Prajapati
		Purpose: It will be called at very first time when component render,To get value from Map and set UI display/format attribute
		Dependencies: MapEntryQualityCheck.cmp
                
    */
	
	doInit: function(component, event, helper) {
		var key = component.get("v.key");
		var map = component.get("v.map");
        var mapval = component.get("v.mapValue");
		var noOfCols = component.get("v.noOfCols");
		if (noOfCols == 3) {
			component.set("v.colWidth", 2);
			component.set("v.size", 2);
		} else if (noOfCols == 1) {
			component.set("v.colWidth", 6);
			component.set("v.size", 6);
		} else {
			component.set("v.colWidth", 3);
			component.set("v.size", 3);
		}
		component.set("v.label", map[key]);
		component.set("v.value", mapval[key]);
	},
})