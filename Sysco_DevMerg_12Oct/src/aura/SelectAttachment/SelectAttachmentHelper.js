/*
* Authors	   :  Atul Hinge
* Date created :  13/09/2017
* Purpose      :  To create new or select existing attachment to send along with Email.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: to initialize component, get list of all attachment */
    initialize : function(component, helper) {
        component.set('v.selectedAttachment',[]);
        var action = component.get("c.getAttachments");
        action.setParams({ caseId :component.get("v.Case.Id")});
        action.setCallback(helper, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.Attachments',helper.addIcon(response.getReturnValue().attachments));
            	
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    /* Authors: Atul Hinge || Purpose: to display selected attachment count */
    updateCount : function(component, event, helper) {
        var attachments =component.get("v.Attachments");
        var selectedcount=0;
        for(var i=0;i<attachments.length;i++){
            if(attachments[i]['isSelected']){
                selectedcount++;
            }
		 }
        component.set("v.footerMsg", selectedcount+" of "+attachments.length+" Attachments selected");
    },
    /* Authors: Atul Hinge || Purpose: hide or show spiner */
     show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    /* Authors: Atul Hinge || Purpose: to map icon based of file extension */
    addIcon :function(attachments) {
        for(var i=0;i<attachments.length;i++){
            var extension =attachments[i].ContentDocument.FileExtension;
            attachments[i]['isPreviewAvailable']=false;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            	
            attachments[i].Size=size;
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
            }else{
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },
    /* Authors: Atul Hinge || Purpose: toogle between create new and Exixting Attachment */
    toogleNavagation :function(component,whichOne) {
         if(whichOne=='primary'){
                $A.util.addClass(component.find('primary'),'slds-is-active');
                $A.util.removeClass(component.find('new'),'slds-is-active');
                
                $A.util.removeClass(component.find('primaryAttachmentList'),'slds-hide');
                $A.util.addClass(component.find('newAttachmentList'),'slds-hide');
               
            }else if(whichOne=='new'){
                $A.util.addClass(component.find('new'),'slds-is-active');
                $A.util.removeClass(component.find('primary'),'slds-is-active');
                
                $A.util.removeClass(component.find('newAttachmentList'),'slds-hide');
                $A.util.addClass(component.find('primaryAttachmentList'),'slds-hide');
                component.set("v.fileUploadStatus","new");
            }
    }
})