/*
* Authors	   :  Atul Hinge
* Date created :  13/09/2017
* Purpose      :  To create new or select existing attachment to send along with Email.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: Initialize component  */         
    doInit : function(component, event, helper) {
        helper.initialize(component, helper);
    },
    /* Authors: Atul Hinge || Purpose: toogle between create new and Exixting Attachment  */ 
    toogleNavagation:function(component, event, helper){
        helper.toogleNavagation(component,event.currentTarget.id);
    },
    /* Authors: Atul Hinge || Purpose: to hide attachment select box  */ 
    toogleModal:function(component, event, helper){
        $A.util.toggleClass(component.find('modal'),'slds-hide');
        
    },
    /* Authors: Atul Hinge || Purpose: call when fileUpload Status changed */
    onFileUploadStatusChange:function(component, event, helper){
        var status=component.get("v.fileUploadStatus");
        if(status=="complete"){
            var attachments=[];
            var uploadedAttachment=helper.addIcon(component.get("v.fileToBeUploaded"));
            for(var i=0;i<uploadedAttachment.length;i++){
                 uploadedAttachment[i]['isSelected']=true;
                attachments.push(uploadedAttachment[i]);
            }
            var oldAttachments=component.get('v.Attachments');
            for(var i=0;i<oldAttachments.length;i++){
                attachments.push(oldAttachments[i]);
            }
            component.set('v.Attachments',attachments);
            helper.updateCount(component, event, helper);
            helper.toogleNavagation(component,'primary');
        }
        
    },
    /* Authors: Atul Hinge || Purpose: to add selected attachments to Email*/
    addAttachments:function(component, event, helper){
        var attachments =component.get("v.Attachments");
        var selectedFileObjects=[];
       
            for(var i=0;i<attachments.length;i++){
                if(attachments[i]['isSelected']){
                    selectedFileObjects.push(attachments[i]);
                }
            }
            
        if(selectedFileObjects.length>0){
            component.set("v.showError",false);
            component.set("v.selectedFileObjects",selectedFileObjects);
            $A.util.toggleClass(component.find('modal'),'slds-hide');
        }else{
            component.set("v.showError",true);
        }
    },
    /* Authors: Atul Hinge || Purpose: to select attachment */
    selectAttachment:function(component, event, helper){
        var selectedAttachment =[];
        var attachments=component.get("v.Attachments");
        for(var i=0;i<attachments.length;i++){
            if(event.currentTarget.id==attachments[i].ContentDocument.Id){
                attachments[i]['isSelected']=(!attachments[i]['isSelected']);
            }
            selectedAttachment.push(attachments[i]);
        }
        component.set("v.Attachments",selectedAttachment);
        component.set("v.showError",false);
        helper.updateCount(component, event, helper);
        
    }, 
    
})