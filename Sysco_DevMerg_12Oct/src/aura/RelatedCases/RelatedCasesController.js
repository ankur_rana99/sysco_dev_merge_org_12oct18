/*
  Authors	   :   Mohit Garg
  Date created :   13/02/2018
  Purpose      :   To show the related cases for any case.
  Dependencies :   CaseDetailView.cmp, RelatedCasesController.cls
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
    doInit: function(component, event, helper) {
        helper.getRelatedCases(component, event);
    },
    redirectRelatedCase: function(component, event, helper) {
       // var target = event.getSource().get("v.name");
       var attr = 'data-value' ;
       var target = event.target.attributes[attr].value;
        component.set("v.relatedCase", target);
        console.log(component.get("v.relatedCase"));
       helper.handleCaseAction(component, event);
    },
    

})