({
    getPOLineHelper: function(component, event) {
		var layout =component.get("v.layOutJson");
		var columns = layout["columns"];
		this.getPOLineData(component, event, columns, component.get("v.mainObject.Id"));
    },
    getPOLineData: function(component, event, objectLsts,objId) {
        var action1 = component.get("c.getPOLineDataServer");
        action1.setParams({
            strObjectName: 'PO_Line_Item__c',
            jsonObj: JSON.stringify(objectLsts),
		    objId : component.get("v.mainObject.Id"),
			poFlip: component.get("v.poFlipCreate"),
        });
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
			if(response.getReturnValue().errmsg !=null && response.getReturnValue().errmsg!=undefined && response.getReturnValue().errmsg!=''){
					this.showToast("Validation Alert!","error","sticky", response.getReturnValue().errmsg);
				}else{
                 component.set("v.objList", response.getReturnValue().DataList);
                 component.set("v.columns", response.getReturnValue().Columns);
				}

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                         this.showToast("Validation Alert!","error","sticky", errors[0].message);
                    }
                } else {
                   this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
                }
            } else {
                this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
            }
        });
        $A.enqueueAction(action1);
    },
    loadSelectedPolineHelper: function(component, event) {
         var action2 = component.get("c.getPOLineFromPODataServer");
        action2.setParams({
            strObjectName: 'PO_Line_Item__c',
		    objId :  event.getParam("polines"),
        });
          action2.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
				if(response.getReturnValue().errmsg !=null && response.getReturnValue().errmsg!=undefined && response.getReturnValue().errmsg!=''){
					this.showToast("Validation Alert!","error","sticky", response.getReturnValue().errmsg);
				}else{
					component.set("v.objList", response.getReturnValue().DataList);
				}

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                       this.showToast("Validation Alert!","error","sticky", errors[0].message);
                    }
                } else {
                   this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
                }
            } else {
               this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
            }
        });
        $A.enqueueAction(action2);
    },
	 handleSelectAllContactHelper: function(component, event, helper) {
	 if (component.get("v.objList") != undefined && component.get("v.objList") != null && component.get("v.objList").length > 0) {
	     //var getID = component.get("v.contactList");
	     var checkvalue = component.find("selectAll").get("v.value");
	     var checkContact = component.find("checkPOLine");
	     if (checkvalue == true) {
	         for (var i = 0; i < checkContact.length; i++) {
	             checkContact[i].set("v.value", true);
	         }
	     } else {
	         for (var i = 0; i < checkContact.length; i++) {
	             checkContact[i].set("v.value", false);
	         }
	     }
	 }
    },
	 addPOLinesHelper: function(component, event, helper) {
	 var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
	  var selectedContacts = [];
        var checkvalue = component.find("checkPOLine");
         
        if(!Array.isArray(checkvalue)){
            if (checkvalue!=undefined && checkvalue.get("v.value") == true) {
                selectedContacts.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue!=undefined && checkvalue[i].get("v.value") == true) {
                    selectedContacts.push(checkvalue[i].get("v.text"));
                }
            }
        }
		var obj =new Object();
		obj.polines = selectedContacts;
		obj.polinesobj = component.get("v.objList");
		if (callback) callback(obj);
	 },
	 showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},
})