({
 	getPOLine: function (cmp, event, helper) { 
 		helper.getPOLineHelper(cmp, event);
 	},
    loadSelectedPoline: function (cmp, event, helper) { 
 		helper.loadSelectedPolineHelper(cmp, event);
 	},
	 handleSelectAllContact: function (cmp, event, helper) { 
 		helper.handleSelectAllContactHelper(cmp, event);
 	},
	/*addPOLines: function (cmp, event, helper) { 
 		helper.addPOLinesHelper(cmp, event);
 	},*/
	addPOLinesMain: function (cmp, event, helper) { 
 		helper.addPOLinesHelper(cmp, event);
 	},
	sectionOne : function(component, event, helper) {
         var acc = component.find('HeaderSec');	 
			for(var cmp in acc) {
				$A.util.toggleClass(acc[cmp], 'slds-show');  
				$A.util.toggleClass(acc[cmp], 'slds-hide');  
	  		}
	},
	 validate: function(component, event, helper) {
        var isValid = true;
        var allStrCmp = component.find('formfield');
        for (var k in allStrCmp) {
            allStrCmp[k].validate(function(value) {
                if (!value) {
                    isValid = false;
                }
            });
        }
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (callback) callback(isValid);
    },
})