({
	getinitialData: function(component, event) {
	// JIRA | CASP2-118  | Shital Rathod changes End
		var action1 = component.get("c.getinitialData");
		var action2 = component.get("c.getUserid");
		var action3 = component.get("c.getPicklistvalues");
		var action4 = component.get("c.getCase");
		var caseId = component.get("v.invoiceid");
        
        action4.setParams({
				CaseObj: caseId,
		});
        action1.setParams({
				CaseObj: caseId,
				
		});
      
		action1.setCallback(this, function(response) {
		// JIRA | CASP2-172  | Somnath changes Start 
			 if (response.getState() === 'SUCCESS') {
                console.log('v.interactionList------'+JSON.stringify(response.getReturnValue()))
                console.log('v.interactionList-ID-----'+JSON.stringify(response.getReturnValue()[0].Id));
				component.set("v.Interactionid", response.getReturnValue()[0].Id);
				if(response.getReturnValue()!=null){
				for(var i=0;i<response.getReturnValue().length;i++){
				var tempdate = response.getReturnValue()[i].CreatedDate;
				var newdate = new Date(tempdate).toGMTString();
				response.getReturnValue()[i].CreatedDate = newdate;
					}
				}
                component.set("v.interactionList", response.getReturnValue());
				this.tempFun(component,event);
			    // JIRA | CASP2-172  | Somnath changes End
			
              /* var custs = [];
				//var custval = [];
               var conts = response.getReturnValue();
              for(var key in conts){
                    custs.push({value:conts[key], key:key});
					//custval.push(conts[key]);
                }
				
				console.log('CUSTTT -- '+JSON.stringify(custs));
				console.log('custval -- '+JSON.stringify(custval));
                component.set("v.attachmentInteractionList", conts);
				component.set("v.attachmentList", custval);
				

				var arrayOfMapKeys = [];
                var StoreResponse = response.getReturnValue();
                console.log('StoreResponse' + JSON.stringify(StoreResponse));
                component.set('v.attachmentInteractionList', StoreResponse);
				component.set('v.fullMap', StoreResponse);
 
                for (var singlekey in StoreResponse) {
                    arrayOfMapKeys.push(singlekey);
                }
                console.log('arrayOfMapKeys' + arrayOfMapKeys);    
                component.set('v.lstKey', arrayOfMapKeys);*/
                
			    $A.enqueueAction(action2);

			} 

			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });

		action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.userid", response.getReturnValue());
				$A.enqueueAction(action3);
			}
        });

		action3.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
				//console.log('options -- '+response.getReturnValue());
                component.set("v.options", response.getReturnValue());
				$A.enqueueAction(action4);
			}
        });
		// JIRA | CASP2-172  | Somnath changes Start
		action4.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
				//console.log('Caseobj -- '+JSON.stringify(response.getReturnValue()));
                component.set("v.case", response.getReturnValue());
				component.set("v.selectedpri",component.get("v.case.Priority__c"));
				component.set("v.currentState",component.get("v.case.Current_State__c"));
				component.set("v.refName",component.get("v.case.Name"));
				component.set("v.fileloaded", true);
				
				$A.enqueueAction(action4);
			}
        });
		
		$A.enqueueAction(action1);  
	},

	tempFun: function(component, event) {
	var caseId = component.get("v.invoiceid");
	var action = component.get("c.getattachementData");
	console.log('Interaction Id in TEMp fun-----'+component.get("v.Interactionid"));
	action.setParams({
				CaseObj: caseId,
				Interactionid:component.get("v.Interactionid")
            	});
				action.setCallback(this, function(response) {
				if (response.getState() === 'SUCCESS') {
                component.set("v.attachmentList", response.getReturnValue());
			} 
			//$A.enqueueAction(action);
		});
		$A.enqueueAction(action);
	},
	// JIRA | CASP2-118  | Shital Rathod changes End


	handleCancel: function(component, event) {
	    // Added By Niza Khunger -- CASP2-125 --Start
        var docIds = component.get("v.docIDs");
		var action = component.get("c.deleteAttachments");
        action.setParams({ docuIDs :docIds});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        // Added By Niza Khunger -- CASP2-125 --End
		component.set("v.isDisplay",false);
	},

	SaveRecordData : function(component, event,CaseObj) {
		//Updated By Niza Khunger -- Start-- 21 Sept 2018 Status should not be equal to close
		//component.set("v.case.Current_State__c",'Closed'); 
		//Updated By Niza Khunger  -- End-- 21 Sept 2018 Status should not be equal to close
		component.set("v.case.Priority__c",component.get("v.selectedpri"));
		
		var emailbody=component.get("v.myVal");
		// JIRA | CASP2-118  | Shital Rathod changes Strat
        var InteractionId =component.get("v.docIDs");
        console.log("emailbody--:"+emailbody);
        console.log("InteractionId--:"+InteractionId);
		var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
				mailbody:emailbody,
				DocId :InteractionId,
			});
			// JIRA | CASP2-118  | Shital Rathod changes End
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("Mail Message:"+JSON.stringify(response.getReturnValue()));
					component.set("v.email", response.getReturnValue()[0]);
					console.log("Mail Message ID:"+component.get("v.email[0].Id"));
					component.set("v.fileUploadStatus",'create');
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The record has been updated successfully."
					});
					toastEvent.fire();
					$A.get('e.force:refreshView').fire();
                }
			});
			$A.enqueueAction(action);
	},

	CloseRecord: function(component, event,CaseObj) { 
		// Updated By Niza --start-- Case Record Updation while Closing 21 Sept 2018
		component.set("v.case.Current_State__c",'Closed'); 
		component.set("v.case.Status__c",'Closed'); 
		component.set("v.case.Priority__c",component.get("v.selectedpri"));
		var emailbody=component.get("v.myVal");
		var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
				mailbody:emailbody
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("Message:"+response.getReturnValue());
					component.set("v.email", response.getReturnValue()[0]);
					console.log("Mail Message:"+component.get("v.email[0].Id"));
					component.set("v.fileUploadStatus",'create');
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The record has been updated successfully."
					});
					toastEvent.fire();
					component.set("v.isDisplay",false);
					$A.get('e.force:refreshView').fire();
						
                }
			});
			$A.enqueueAction(action);
			// Updated By Niza --start-- Case Record Updation while Closing 21 Sept 2018
	},
	
   	// Added By Niza Khunger -- CASP2-125 -- Start
	 refreshTable : function(component, event) { 
        var docIDs = component.get("v.docIDs");
        var action = component.get("c.newUploadedAttachments");
			action.setParams({
				docIDs: component.get("v.docIDs")
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("List:"+response.getReturnValue());
					component.set("v.Attachments", response.getReturnValue());
					//Niza-- SP: Alert should not be their while document is uploaded via VM -- Start -- 25/9
					//alert("Document(s) successfully Deleted!");
					//Niza-- SP: Alert should not be their while document is uploaded via VM -- End -- 25/9
				}
			})
			$A.enqueueAction(action); 
        },
	// Added By Niza Khunger -- CASP2-125 --End
 
				
})