({
	doInit: function(component, event, helper) {
        
		
        // ------------JIRA-CASP2-82--- added by Shital Rathod --- start
		component.set('v.myVal', '<b></b>');
        //------------JIRA-CASP2-82--- added by Shital Rathod --- end
        
        //Added By Niza CASP2-125 --Start--
        var action1 = component.get("c.getCustomSettingsDetails");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.accept", storeResponse);
            } 
        });
        
        $A.enqueueAction(action1);

		
        //Added By Niza CASP2-125 --End--
       
		helper.getinitialData(component, event);
		console.log('intID-------'+component.get("v.Interactionid"));
		//helper.GetInteractionlist(component, event);
    },
    
	handleCancel: function(component, event, helper) {
        helper.handleCancel(component, event);
    },
    
    handleActive: function(cmp, event, helper) {		
		var tab = event.getSource();
		var tabName = tab.get('v.id');
        console.log('Tab NAME -- '+tabName);
         helper.getinitialData(component, event);
		/*if(tabName == 'Case History'){
			var comVar = cmp.get("v.caseHistroyRefCounter");
			cmp.set("v.caseHistroyRefCounter", comVar+1);
			console.log(cmp.get("v.caseHistroyRefCounter"));
		}*/
          /* if(tabName == 'Attachments'){
             
              
           }*/
       
		cmp.set("v.selectedTabId", tab.get('v.id'));
	},
    
	toggle: function (component, event, helper) {
         var sel = component.find("priority");
         var opt =	sel.get("v.value");
		 component.set('v.selectedpri', opt);
		 component.set("v.case.Priority__c",component.get("v.selectedpri"));
		 //console.log('Selected Prio options -- '+opt);
    },

	SubmitRecordData: function(component,event,helper){
		helper.SaveRecordData(component, event,component.get("v.case"));
	},

	CloseRecord: function(component,event,helper){
		var r = confirm($A.get("$Label.c.confirmClose"));
		if(r){
		helper.CloseRecord(component, event,component.get("v.case"));
		}
		else{
		return false;
		}
	},
	hideDeleteFileMenu: function(component, event, helper) {
		var toggleText = component.find("Confirmation");
        $A.util.toggleClass(toggleText, "slds-hide");
	 },
	 deleteConform : function(component, event, helper) {
        $A.util.toggleClass(component.find("Confirmation"), "slds-hide");
        var temp=component.get("v.callBackOnDelete");
        temp();
    },
	// JIRA | CASP2-172  | Somnath changes Start
    // Added By Niza Khunger -- CASP2-125 -- Start--
	handleUploadFinished: function (component, event) { 
            //  Get the list of uploaded files
			component.set("v.showSpinner",true);
            var docIds = component.get("v.docIDs");
            var uploadedFiles = event.getParam("files"); 
            for (var i = 0; i < uploadedFiles.length; i++) {
                    docIds.push(uploadedFiles[i].documentId);
               }
            component.set("v.docIDs",docIds);
            var docIDs = component.get("v.docIDs");
            var action = component.get("c.newUploadedAttachments");
    			action.setParams({
    				docIDs: docIDs
    			});
    			action.setCallback(this, function(response){
    				var state = response.getState();
    				if (state === "SUCCESS") {
    					console.log("List:"+response.getReturnValue());
    					component.set("v.Attachments", response.getReturnValue());
    					//Niza-- SP: Alert should not be their while document is uploaded via VM -- Start -- 25/9
    					//alert('Document(s) successfully uploaded');
    					//Niza-- SP: Alert should not be their while document is uploaded via VM -- End -- 25/9
    				}
					component.set("v.showSpinner",false);
    			})
    			$A.enqueueAction(action); 
    },
	// JIRA | CASP2-172  | Somnath changes End
    // Added By Niza Khunger -- CASP2-125 -- End--

   // Added By Niza Khunger -- CASP2-125 -- Start--
   handleDeleteButton : function(component, event, helper) { 
       event.preventDefault();
        // Get the value from the field that's in the form
        var docIdtemp = component.get("v.docIDs");
        var newArray = [];
        var docIDtests = event.target.getElementsByClassName('attachment-name')[0].value;
        
        var action = component.get("c.deleteAttachments");
			action.setParams({
				docuIDs: event.target.getElementsByClassName('attachment-name')[0].value
			});
			
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                    for(var i=0;i<docIdtemp.length;i++){
                         if(docIdtemp[i]!= event.target.getElementsByClassName('attachment-name')[0].value) 
                                newArray.push(docIdtemp[i]);
                    }
                    component.set("v.docIDs",newArray);
                    helper.refreshTable(component, event);
            }
            else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
   // Added By Niza Khunger -- CASP2-125 -- End--
    
       
   
})