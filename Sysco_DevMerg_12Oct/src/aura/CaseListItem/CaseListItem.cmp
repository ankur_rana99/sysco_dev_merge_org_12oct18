<!--
  Authors	   :   Rahul Pastagiya
  Date created :   14/08/2017
  Purpose      :   This is helper/child component for CaseList component
	               This component will be called for each Case from CaseList Component
  Dependencies :   CaseListItemController.js, CaseListItem.css, CaseListItemController.cls
  __________________________________________________________________________
  	Modifications 1:
                   Date: 02/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-678] Configuration ability of Case view

  	Modifications 2:
                   Date: 06/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-12] Optimize the list view to ensure key information is readily available

	Modifications 2:
                   Date: 07/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-722] Visibility of quick reply, star, read email & reply/reply all/forward icons on all tabs for list view  
 -->
<aura:component controller="CaseListItemController">
    <!--External Attributes-->
    <aura:attribute name="ct" type="CaseManager__c" />
    <aura:attribute name="selectedTab" type="String" />
    <aura:attribute name="loginUserId" type="String" />
    <aura:attribute name="rowIndex" type="String" />
    <aura:attribute name="displayField" type="object" />
    <!--PUX-678-->
    <aura:attribute name="fieldSetKeyList" type="List" default="[]"  />
    <aura:attribute name="fieldSetFieldLabelMap" type="Map"/>
    <aura:attribute name="filedSetCol1" type="Integer" />
    <aura:attribute name="filedSetCol2" type="Integer"/>
    <aura:attribute name="currentListItem" type="String" />
    <!--PUX-678-->
    <!--External Attributes-->
    <!--Internal Attributes-->
    <aura:attribute name="editableMode" type="Boolean" default="True" />
    <aura:attribute name="showEmailPopover" type="Boolean" default="false" />
    <aura:attribute name="showAdditionalDetail" type="Boolean" default="false" />
    <aura:attribute name="toAddress" type="String" default="--" />
    <aura:attribute name="fromAddress" type="String" default="--" />
    <aura:attribute name="subject" type="String" default="--" />
    <aura:attribute name="items" type="Sobject" />
    <aura:attribute name="oneLineEmialTxtBody" type="String" default="--" />
    <aura:attribute name="latestEmailTxtBody" type="String" default="--" />
    <!--PUX-331-->
    <aura:attribute name="expectedDate" type="integer" default="01" />
    <aura:attribute name="expectedMonth" type="String" default="Jan" />
	<aura:attribute name="expectedTime" type="String"  default="00:00 AM"/>
    <!--PUX-331-->
    <aura:attribute name="emailCount" type="integer" default="2" />
    <aura:attribute name="isFavCase" type="Boolean" default="false" />
    <!--Internal Attributes-->
    <aura:attribute name="selCaseList" type="String" />
    <aura:attribute name="chkList" type="String[]"/>
    <!--PUX-678-->
  	<aura:attribute name="fieldSetMap" type="map" />
    <aura:attribute name="render" type="boolean" />
    <aura:attribute name="isInvalidDate" type="boolean" default="false"/>
    <!--PUX-678-->
    <!-- Event Register -->
    <aura:registerEvent name="caseAction" type="c:CaseAction" />
    <aura:registerEvent name="selectedCasesEvent" type="c:selectedCases" />
    <aura:registerEvent name="refreshView" type="force:refreshView" />
    <aura:registerEvent name="SelectedCaseEvent" type="c:ApplicationCaseEvent" />
    <!-- Event Register -->
    <!-- START : Event Handlers -->
    <aura:handler event="c:CheckedAll" action="{!c.checkedAll}" />
    
    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <aura:handler name="change" value="{!v.currentListItem}" action="{!c.closeOtherPopups}" />
    <!-- End : Event Handlers -->
    <!--Display Logic-->	
    <div class="{! 'slds-grid leftBoder context-menu-one-'+v.ct.Id }" id="{!'container-'+v.ct.Id}" aura:id="ddmmo" style="{! 'background-color: #f5f6fa;border-left: 6px solid '+v.ct.Indicator__c}">
       
        <article class="slds-card Shape-Copy hover-Effect" style="border:0px;">
            <div class="slds-card__header slds-grid slds-grid_vertical-align-center sldc-card_header-2 fadeIn" style="cursor:pointer">
                <header class="slds-media slds-media_center slds-has-flexi-truncate slds-size_5-of-12">
                    <div class="slds-media__figure slds-media__figure-2">
                        <span class="slds-icon_container" title="Select Case for Actions">
							<span class="slds-form-element">
								<span class="slds-form-element__control">
                                     
									<span class="slds-checkbox">
										<input type="checkbox" name="options" id="{! 'checkbox-20'+v.ct.Id}" aura:id="chkBox" onclick="{!c.toggleCheckBox}" value="{!v.ct.Id}"/>
										<label class="slds-checkbox__label" for="{! 'checkbox-20'+v.ct.Id}">
											<span class="slds-checkbox_faux Check_box" style="margin-right: 0px;"></span>
                        </label>
                        </span>
                        </span>
                        </span>
                        </span>
                    </div>
                    <div class="slds-media__body  hover-pointer" onclick="{!c.handleCaseAction}" id="CaseListItemDiv" style="padding-right:15px">
                        <span class="Not-yet-received-my hover-pointer trimDivContent" id="subject" data-value="{!v.ct.Id}" title="{!v.ct.Subject__c}">[#{!v.ct.Name}]&nbsp;{!v.ct.Subject__c}
							<img id="reply"  src="{!($Resource.ThickRowViewPackage+'/escalation_flag/escalationFlag2.png')}" alt="meaningful text" width="20" style ="{! if(!v.ct.Escalation__c,'width: 18px; padding-bottom: 4px;padding-left: 2px;display:none;','width: 18px; padding-bottom: 4px;padding-left: 2px;display:inline-flex;') }" title="{!$Label.c.case_escalated_label}"/>
						</span>
                        <div class="Please-provide-us-wi  emailDes trimDivContent" style="{! if(v.ct.Emails != null,'display:block','display:none')}">
                             <aura:unescapedHtml value="{! v.oneLineEmialTxtBody}" />
                         <br/>  
                            <div>{!$Label.c.Created_by_label}:
                                <span class="text-style-1"><ui:outputText value="{!v.ct.Emails[0].FromName}" />, 
                                    <aura:if isTrue="{!not(v.isInvalidDate)}">
										<ui:outputDateTime value="{!v.ct.Emails[0].CreatedDate}" format="dd/MM/yyyy hh:mm"/>
                                    </aura:if>
                                    </span>
                            </div>
                        </div>
                    </div>
                </header>                
                <div class="slds-no-flex slds-size_2-of-12">
                    <aura:if isTrue='{!v.render}'>
                        <aura:iteration items="{!v.fieldSetKeyList}" var="fs" indexVar="index">
                            <aura:if isTrue="{!lessthan(index,v.filedSetCol1)}">
                                <c:MapEntry map="{!v.fieldSetMap}" key="{!fs}" labelMap="{!v.fieldSetFieldLabelMap}" />    
                            </aura:if>
                        </aura:iteration> 
                    </aura:if>                                       
                </div>
                <!--Start : PUX-503 - Style added for better UI-->
                <div class="slds-no-flex slds-size_2-of-12 " >
                     <aura:if isTrue='{!v.render}'>
                         <aura:iteration items="{!v.fieldSetKeyList}" var="fs" indexVar="index">
                            <aura:if isTrue="{!greaterthan(index,v.filedSetCol2)}">
                                <aura:if isTrue="{!lessthan(index,8)}">
                                   <c:MapEntry map="{!v.fieldSetMap}" key="{!fs}" labelMap="{!v.fieldSetFieldLabelMap}" />    
                                </aura:if>
                            </aura:if>
                        </aura:iteration>
                    </aura:if> 
                </div>                
                <div class="slds-no-flex slds-size_1-of-12" style="text-align: center;">
                    <!--PUX-334-->
                   <!--  <Div class="Rectangle-8" style="{! if(v.ct.OverdueIn__c == 'Overdue','background-color:#fc6a6c;','background-color:forestgreen;')}" onclick="{!c.showAdditionalFields}">
                        <span class="Overdue" title="{! if(v.ct.OverdueIn__c == 'Overdue','Overdue','Overdue In')}">{!v.ct.OverdueIn__c}</span>
                    </Div>
                     -->
                     <div onclick="{!c.showAdditionalFields}" class="hover-pointer" title="More Details">
                    	<lightning:icon iconName="utility:threedots" size="x-small" alternativeText="More Details"/>
                    </div>
                    <!--PUX-334-->
                </div>
                <!-- PUX-722: Condition is remove to diplay button in all tabs-->
                <div class="slds-no-flex slds-size_1-of-12" style="padding-left: 0px;" >
                    <span class="slds-no-flex slds-size_9-of-12" style="margin-top: 5%;padding-left: 4px">
                       
                        <span style="{! if(v.ct.Unread_Email_Count__c>0,'margin-right:-10px','margin-right:7px')}" title="{!$Label.c.Unread_Email_Count}">
                            <img src="{!($Resource.ThickRowViewPackage+'/mail-icon/mail-icon-copy@2x.png')}" alt="meaningful text" width="21" onclick="{!c.showEmail}" />
                            <span class="numberCircle" style="{! if(v.ct.Unread_Email_Count__c>0,'display:inline-block;','display:none')}">{!v.ct.Unread_Email_Count__c}</span>
                        </span>
                        <c:QuickReply ct="{!v.ct}" />
                        <span class="slds-no-flex slds-size_2-of-12">
						<div class="slds-dropdown-trigger slds-dropdown-trigger_click" aura:id="sortDropdown" onclick="{!c.openSortDropDown}">
                            <button class="slds-button slds-button_icon slds-button_icon-border-filled slds-button_icon-x-small" aria-haspopup="true" title="More options for Reply, Reply All, Forward" onblur="{!c.closeSortDropDown}">
                                <lightning:icon iconName="utility:down" size="xx-small" alternativeText="down" />
                                <span class="slds-assistive-text">More options for Reply, Reply All, Forward</span>
                            </button>
                            <div class="slds-dropdown slds-dropdown_right slds-dropdown_actions" style="z-index:1;">
                                <ul class="slds-dropdown__list" role="menu">
                                    <li class="slds-dropdown__item" style="display:block;margin:0px 5px 0px 5px" role="presentation">
                                        <a id="reply" href="javascript:void(0);" role="presentation" onmousedown="{!c.handleCaseAction}" class="sortOption">
                                            <span class="slds-truncate">Reply</span>
                                        </a>
                                    </li>
                                    <li class="slds-dropdown__item" style="display:block;margin:0px 5px 0px 5px" role="presentation">
                                        <a id="replyall" href="javascript:void(0);" role="presentation" onmousedown="{!c.handleCaseAction}" class="sortOption">
                                            <span class="slds-truncate">Reply All</span>
                                        </a>
                                    </li>
                                    <li class="slds-dropdown__item" style="display:block;margin:0px 5px 0px 5px" role="presentation">
                                        <a id="forward" href="javascript:void(0);" role="presentation" onmousedown="{!c.handleCaseAction}" class="sortOption">
                                            <span class="slds-truncate">Forward</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </span>
                        <!--img id="reply" src="{!($Resource.ThickRowViewPackage+'/Reply_icon/Reply_icon_line@2x.png')}" alt="meaningful text" width="20" style="object-fit: contain;margin-right: 13px;" onclick="{!c.handleCaseAction}" />
                        <img id="replyall" src="{!($Resource.ThickRowViewPackage+'/Reply_all_icon/Reply_all_icon_line@2x.png')}" alt="meaningful text" width="23" style="object-fit: contain;margin-right: 13px;" onclick="{!c.handleCaseAction}" />
                        <img id="forward" src="{!($Resource.ThickRowViewPackage+'/Forward_icon/Forward_icon_line@2x.png')}" alt="meaningful text" width="20" style="object-fit: contain;" onclick="{!c.handleCaseAction}" /-->
                    </span>
                </div>
                <div class="slds-no-flex slds-size_1-of-12" style="text-align: center;">
                    <!--<Div class="Rectangle-8" style="{! if(v.ct.TAT_on_Creation_Date__c == 'TAT Miss' || v.ct.TAT_on_Creation_Date__c =='Outside TAT','background-color:#fc6a6c;','background-color:forestgreen;')}">-->
                    <Div class="Rectangle-8" style="{! if(v.ct.TAT_on_Creation_Date__c == 'TAT Met' || v.ct.TAT_on_Creation_Date__c =='Within TAT','background-color:forestgreen;color:white;',if(v.ct.TAT_on_Creation_Date__c =='Outside TAT','background-color:#FFFF00;color:black;','background-color:#FF0000;color:white;'))}">
                        <span class="" title="TAT on Creation Date">{!v.ct.TAT_on_Creation_Date__c}</span>
                    </Div>
                    <!--<Div class="Rectangle-8" style="{! if(v.ct.TAT_on_Assigned_Date1__c == 'TAT Miss' || v.ct.TAT_on_Assigned_Date1__c =='Outside TAT','background-color:#fc6a6c;margin-top:2px;','background-color:forestgreen;margin-top:2px;')}">-->
                    <Div class="Rectangle-8" style="{! if(v.ct.TAT_on_Assigned_Date1__c == 'TAT Met' || v.ct.TAT_on_Assigned_Date1__c =='Within TAT','background-color:forestgreen;margin-top:2px;color:white;',if(v.ct.TAT_on_Assigned_Date1__c =='Outside TAT','background-color:#FFFF00;margin-top:2px;color:black;','background-color:#FF0000;margin-top:2px;color:white;'))}">
                        <span class="" title="TAT on Assigned Date">{!v.ct.TAT_on_Assigned_Date1__c}</span>
                    </Div>
                </div>
            </div>
        </article>
    </div>

    <!--Start code for PUX-679-->
    <aura:if isTrue="{!v.showEmailPopover}">
        <div>
            <div class="emailCss" style="margin-left: 1%">
                
                <section aura:id="emailPopUp"  class=" emailCss toggleDisplay slds-picklist  slds-popover slds-nubbin_top-right-cli" role="dialog" aria-label="Dialog Title" aria-describedby="dialog-body-id-31" aria-modal="true" style="width: 99%; z-index: 0">
                    
                    <div class="slds-popover__body" id="dialog-body-id-31" >
                        <ui:scrollerWrapper class="scroll" >
                            <div><b>From : </b> {!v.fromAddress} </div>
                            <div><b>To : </b> {!v.toAddress}</div>
                            <div><b>Subject : </b> {!v.subject}</div>
                            <aura:unescapedHtml value="{!v.latestEmailTxtBody}" />
                        </ui:scrollerWrapper>
                    </div>
                    <footer class="slds-popover__footer">
                        <div align="right" style="float:right;" class="hover-pointer" id="pasteButton" > 
                            <lightning:button variant="brand" label="Close" onclick="{!c.showEmail}" />
                        </div>
                    </footer>
                </section>
            </div>
            
        </div>
    </aura:if>
    <!--End Code for PUX-679-->
    <!--Display Logic-->
    
   <!--Start code for PUX-12-->
   <aura:if isTrue="{!v.showAdditionalDetail}">
   <div>
    <div class="emailCss" style="margin-left: 1%">
       <section aura:id="additionalFields"  class=" emailCss toggleDisplay slds-picklist slds-popover slds-nubbin_top-right-cli" role="dialog" aria-label="Dialog Title" aria-modal="true" aria-describedby="dialog-body-id-31" style="width: 99%; z-index: 0">			           
			<div class="slds-popover__body" id="dialog-body-id-31" >
                <aura:if isTrue='{!v.render}'>
                    <div class=" slds-grid slds-wrap" aura:id="highlightPanelContainer">
                        <aura:iteration items="{!v.fieldSetKeyList}" var="fs" indexVar="index">
                            <c:MapEntryChild map="{!v.fieldSetMap}" key="{!fs}" noOfCols="3" labelMap="{!v.fieldSetFieldLabelMap}" />
                        </aura:iteration> 
                    </div>                        
                </aura:if>                     
			</div>
            <footer class="slds-popover__footer">
    			<div align="right" style="float:right;" class="hover-pointer" id="pasteButton" > 
                     <lightning:button variant="brand" label="Close" onclick="{!c.showAdditionalFields}" />
                </div>
  			</footer>
		</section>
       </div>
   </div>
    </aura:if>
    <!--Display Logic-->
</aura:component>