({
    doInit: function(component, event, helper) {
        
        // Added By Niza Khunger -- CASP2-134 -- Start--
        var action1 = component.get("c.getUserID");
        var action2 = component.get("c.getCustomSettingsDetails");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.UserId", storeResponse);
            }
             $A.enqueueAction(action2);
        });
       
        
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.accept", storeResponse);
            }
        });
       
        $A.enqueueAction(action1);
        // Added By Niza Khunger -- CASP2-134 -- End--
        
		helper.getFields(component, event);
    },
    
	handleCancel: function(component, event, helper) {
        helper.handleCancel(component, event);
    }, 
    //Updated By Somnath start CASP2-119
    SubmitRecordData: function(component,event,helper){
        var docIds = component.get("v.docIDs");
		helper.SaveRecordData(component, event,component.get("v.caseObj"),docIds,component.get("v.caseObjList"));
		
	},
	//Updated By Somnath start CASP2-119
    // Added By Niza Khunger -- CASP2-134 -- Start--
	handleUploadFinished: function (component, event) { 
            //  Get the list of uploaded files
            var docIds = component.get("v.docIDs");
            var uploadedFiles = event.getParam("files"); 
            for (var i = 0; i < uploadedFiles.length; i++) {
                    docIds.push(uploadedFiles[i].documentId);
               }
            component.set("v.docIDs",docIds);
            var docIDs = component.get("v.docIDs");
            var action = component.get("c.newUploadedAttachments");
    			action.setParams({
    				docIDs: docIDs
    			});
    			action.setCallback(this, function(response){
    				var state = response.getState();
    				if (state === "SUCCESS") {
    					console.log("List:"+response.getReturnValue());
    					component.set("v.Attachments", response.getReturnValue());
    					//Niza-- SP: Alert should not be their while document is uploaded via VM -- Start -- 25/9
    					//alert('Document(s) successfully uploaded');
    					//Niza-- SP: Alert should not be their while document is uploaded via VM -- End -- 25/9
    				}
    			})
    			$A.enqueueAction(action); 
    },
    // Added By Niza Khunger -- CASP2-134 -- End--

   // Added By Niza Khunger -- CASP2-134 -- Start--
   handleDeleteButton : function(component, event, helper) { 
       event.preventDefault();
        // Get the value from the field that's in the form
        var docIdtemp = component.get("v.docIDs");
        var newArray = [];
        var docIDtests = event.target.getElementsByClassName('attachment-name')[0].value;
        
        var action = component.get("c.deleteAttachments");
			action.setParams({
				docuIDs: event.target.getElementsByClassName('attachment-name')[0].value
			});
			
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                    for(var i=0;i<docIdtemp.length;i++){
                         if(docIdtemp[i]!= event.target.getElementsByClassName('attachment-name')[0].value) 
                                newArray.push(docIdtemp[i]);
                    }
                    component.set("v.docIDs",newArray);
                    helper.refreshTable(component, event);
            }
            else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
   // Added By Niza Khunger -- CASP2-134 -- End--
  
})