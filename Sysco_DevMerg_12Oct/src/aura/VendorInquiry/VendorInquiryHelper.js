({
// Updated By Somnath start CASP2-119
	getFields: function(component, event) {
		var action1 = component.get("c.getFields");
		var action2 = component.get("c.getUsernameEmail");
        console.log('invoiceid====='+component.get("v.invoiceid"));
		if(component.get("v.invoiceid")!='na' && component.get("v.invoiceid")!=undefined)
		{
     		var action3 = component.get("c.getInvoicenumber");
		}
		//console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                //console.log('records--'+response.getReturnValue().record);
                //console.log('columns--'+response.getReturnValue().Columns);
                component.set("v.caseObj", response.getReturnValue().record);
				component.set("v.caseObjList", response.getReturnValue().record);
			    component.set("v.columns", response.getReturnValue().Columns);
				if(component.get("v.invoiceno")!='na' && component.get("v.invoiceid")!=undefined)
				{
					component.set("v.caseObj.Invoice__r.Id",component.get("v.invoiceid"));
					component.set("v.caseObj.Invoice__c",component.get("v.invoiceid"));
					//component.set("v.caseObj.Invoice__r.Name",component.get("v.invoiceno"));
					//component.set("v.caseObj.Subject__c",component.get("v.invoiceno"));
				}
				 component.set("v.caseObj",component.get("v.caseObj"));
				$A.enqueueAction(action2);
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });

		action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                //console.log('records--'+response.getReturnValue().UserName);
				//console.log('records--'+response.getReturnValue().Email);
				component.set("v.username", response.getReturnValue().UserName);
				component.set("v.usermailid", response.getReturnValue().Email);
                component.set("v.caseObj.SuppliedEmail__c", response.getReturnValue().Email);
                component.set("v.caseObj.SuppliedName__c", response.getReturnValue().UserName);
				if(component.get("v.invoiceid")!='na')
				{
					$A.enqueueAction(action3);
				}
			}
        });

		if(component.get("v.invoiceid")!='na' && component.get("v.invoiceid")!=undefined)
		{
			var invid=component.get("v.invoiceid");
			console.log('invid--',invid);
			action3.setParams({
					invoiceid: invid,
			});

			action3.setCallback(this, function(response) {
				if (response.getState() === "SUCCESS") {
					component.set("v.invoiceno", response.getReturnValue());
					component.set("v.caseObj.Invoice__r.Name",component.get("v.invoiceno"));
				}
			});
		}
        $A.enqueueAction(action1);    
    },
	// Updated By Somnath end CASP2-119
	// Updated By Somnath start CASP2-119,122
	showToast: function(title, type,mode,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "mode": mode,
            "message": message
        });
        toastEvent.fire();
    },
	SaveRecordData : function(component, event,CaseObj, docIDs,caseObjList) {
			//console.log(component.get("v.myVal"));
			console.log(CaseObj);
			for(var i =0;i<component.get("v.columns").length;i++){
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Process__c' && (caseObjList[0].Process__c=='' || caseObjList[0].Process__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Process");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Mailbox_Name__c' && (caseObjList[0].Mailbox_Name__c=='' || caseObjList[0].Mailbox_Name__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Mail Box Name");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Reason__c' && (caseObjList[0].Reason__c=='' || caseObjList[0].Reason__c == undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Reason");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'OpCo__c' && (caseObjList[0].OpCo__c=='' || caseObjList[0].OpCo__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter OpCo");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Priority__c' && (caseObjList[0].Priority__c=='' || caseObjList[0].Priority__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Priority");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Subject__c' && (caseObjList[0].Subject__c=='' || caseObjList[0].Subject__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Subject");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Invoice__c' && (caseObjList[0].Invoice__c=='' || caseObjList[0].Invoice__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Invoice");
				return false;
				}
				if((caseObjList!=undefined && component.get("v.columns")[i].name == 'Comments__c' && (caseObjList[0].Comments__c=='' || caseObjList[0].Comments__c==undefined) && component.get("v.columns")[i].required == true)){
				this.showToast("Error!","error","sticky","Please Enter Comments");
				return false;
				}
			}
        	var InvId=component.get("v.invoiceid");
        	
        	if(InvId==null && InvId==undefined)
            {
				component.set("v.caseObj.Invoice__c",component.get("v.caseObj.Invoice__r.Id"));
                
            }
        	else
            {
                component.set("v.caseObj.Invoice__c",InvId);
            }
			component.set("v.caseObj.Input_Channel__c",'vendor');
        
        	//console.log('Case Invoice----- :'+component.get("v.caseObj.Invoice__c"));
			//console.log('After -- '+component.get("v.caseObj.Vendor__c"));
			var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
				docIDs:docIDs
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS" && response.getReturnValue()!=null) {
				   console.log("Message:"+response.getReturnValue());
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The "+response.getReturnValue()+ " has been updated successfully."
					});
					toastEvent.fire();
					//this.handleCancel(component, event);
					$A.get('e.force:refreshView').fire();
                }
			});
			$A.enqueueAction(action);
		//	this.getFields(component, event);
	},
	// Updated By Somnath end CASP2-119,122
	handleCancel: function(component, event) {
	     // Added By Niza Khunger -- CASP2-134 --Start
		var action = component.get("c.deleteAttachments");
        action.setParams(
            { 
            docuIDs :component.get("v.docIDs")   
            }
            );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.toggleSpinner(component);	
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        // Added By Niza Khunger -- CASP2-134 --Start
        
		component.set("v.isDisplay",false);
	},
	
	// Added By Niza Khunger -- CASP2-134 -- Start
	 refreshTable : function(component, event) { 
        var docIDs = component.get("v.docIDs");
        var action = component.get("c.newUploadedAttachments");
			action.setParams({
				docIDs: component.get("v.docIDs")
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("List:"+response.getReturnValue());
					component.set("v.Attachments", response.getReturnValue());
					//Niza-- SP: Alert should not be their while document is uploaded via VM -- Start -- 25/9
					//alert("Document(s) successfully Deleted!");
					//Niza-- SP: Alert should not be their while document is uploaded via VM -- End -- 25/9
				}
			})
			$A.enqueueAction(action); 
        },
	// Added By Niza Khunger -- CASP2-134 --End
 
	
	
})