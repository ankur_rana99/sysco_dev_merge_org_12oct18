({
	getInvList : function(component, event) {
        console.log('helper');
		var action = component.get("c.getInvoiceLineItems");
        action.setCallback(this, function(response){
            
            var state = response.getState();
            console.log('state:'+state);
            if (state === "SUCCESS") {
                component.set("v.invlinelst", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getInvLineitem : function(component, event) {
        console.log('--getInvLineitem--');
		var action = component.get("c.getInvoiceLine");
        action.setCallback(this, function(response){
            
            var state = response.getState();
            console.log('state:'+state);
            if (state === "SUCCESS") {
                component.set("v.invlineitem", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    submitRecordData : function(component, event) {
		this.saveRecordData(component, event);
	},
    saveRecordData : function(component, event) {
        var action = component.get("c.SaveInvoiceData");
        
        action.setParams({
            invLineItemList: JSON.stringify(component.get("v.invlinelst"))
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //cmp.set("v.objectList", response.getReturnValue());
                console.log("Message:"+response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " +errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    addData : function(component, event) {
        var action = component.get("c.addData");
        
        action.setParams({
            invLineItemList: component.get("v.invlinelst")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.invlinelst", response.getReturnValue());
                console.log("Message:"+response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
    }
})