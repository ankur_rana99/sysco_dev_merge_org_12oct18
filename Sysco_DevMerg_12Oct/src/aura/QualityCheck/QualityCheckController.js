/*
Authors: Niraj Prajapati
Date created: 01/11/2017
Purpose: This controller will get the fields as per field set configured on Quality Check Object.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/ 

({
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called at very first time when component render,To fetch field set wise fields
		Dependencies: QualityControl.cmp
                
    */
    doInit: function(component, event, helper) {
	//alert(1);
	var action = component.get("c.getQCFieldSetName");
	/*if(component.get("v.invoiceId")== undefined){
		component.set("v.invoiceId","a17370000004zEMAAY");
	}*/
	action.setParams({ caseId : component.get("v.invoiceId")});
	action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                //alert(1);
                //console.log(JSON.stringify(response.getReturnValue().qcInfo.qcFormFieldsetName));
                if(response.getReturnValue().qcInfo.qcFormFieldsetName != null && response.getReturnValue().qcInfo.qcFormFieldsetName !='')
                {
					component.set("v.isQCAvailable", true);
                    component.set("v.qcInfo",response.getReturnValue().qcInfo);}
                else{
                     component.set("v.qcInfo","");
                    component.set("v.isQCAvailable", false);}
					 component.set("v.sObjectName",response.getReturnValue().sObjName);
					
					helper.getFields(component, event);
					helper.getQC(component, event);

			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	
        
      //  $A.enqueueAction(action);
      
    },
	SubmitRecordData: function(component,event,helper){
		component.set("v.caseObj.Invoice__c",component.get("v.invoiceId"));
		helper.SaveRecordData(component, event,component.get("v.caseObj"));
	},
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called to get all Quliaty check records.
		Dependencies: QualityControl.cmp
                
    */
    getQC: function(component, event, helper){
        helper.getQC(component, event);
    },
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called to insert Quliaty check records.
		Dependencies: QualityControl.cmp
                
    */
   
   insertQC : function(component, event, helper){
       console.log('hello insert');
		if(component.get("v.sObjectName")=="Invoice__c"){
			component.set("v.caseObj.Invoice__c",component.get("v.invoiceId"));
		}else if(component.get("v.sObjectName")=="CaseManager__c"){	
			component.set("v.caseObj.CaseManager__c",component.get("v.invoiceId"));
		}
		helper.SaveRecordData(component, event,component.get("v.caseObj"));
    },
	getQcObject: function(component, event, helper){
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
		if (callback) callback(component.get("v.caseObj"));
	}
})