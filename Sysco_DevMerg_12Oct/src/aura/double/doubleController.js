({
	init : function(component, event, helper) {
		var fieldName = component.get("v.fieldMetadata.name");
		if(component.get("v.fieldMetadata.type") == "reference") {
			fieldName = fieldName.substring(0, fieldName.length - 3)+'__r';
		} 
		component.getReference("v.obj."+fieldName);
		if(component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "")
        {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
            {
                component.set("v.render",_criteriahelper.validate(criJson,component.get("v.criteriaObj"),component.get("v.objFieldMetadata")));
				if(component.get("v.render") == true)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-show";
					component.set("v.fieldCriteria", field);
				}
				if(component.get("v.render") == false)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-hide";
					component.set("v.fieldCriteria", field);
				}
            }
        }
	},

	handleRefresh : function(component, event, helper) {
		if(component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "")
        {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
            {
                component.set("v.render",_criteriahelper.validate(criJson,component.get("v.criteriaObj"),component.get("v.objFieldMetadata")));
				if(component.get("v.render") == true)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-show";
					component.set("v.fieldCriteria", field);
				}
				if(component.get("v.render") == false)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-hide";
					component.set("v.fieldCriteria", field);
				}
            }
        }
	}
})