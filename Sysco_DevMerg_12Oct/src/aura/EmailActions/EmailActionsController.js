({
	/*
	 * Authors: Parth Lukhi
	 * Purpose: On CLick of  Reply,ReplyAll,Forward ICons 
	 * Dependencies:  InteractionListItemHelper.js
	 * Start : On CLick of  Reply,ReplyAll,Forward ICons */
	handleCaseAction: function(component, event, helper) {
        var compEvent = component.getEvent("caseAction");
        var params={
            "Case": component.get("v.case"),
            "interactionId":  component.get("v.interactionId"),
            "callback":function(){                
            },
       };        
		if (event.currentTarget.id == "reply_sup" || event.currentTarget.id == "reply_sub") {
			params["Action"]="reply";
        } else if (event.currentTarget.id == "replyall_sup" || event.currentTarget.id == "replyall_sub") {
			params["Action"]="replyall";
        } else if (event.currentTarget.id == "forward_sup" || event.currentTarget.id == "forward_sub") {
			params["Action"]="forward";
        }
		compEvent.setParams(params);
        compEvent.fire();
	},
})