/*
* Authors	   :  Atul Hinge
* Date created :  14/08/2017
* Purpose      :  For email releated activity like reply,replyAll, forward.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-113,PUX-39,PUX-118,PUX-113,PUX-132,PUX-217,PUX-218,PUX-225, PUX-194
* -----------------------------------------------------------
* Modifications: 
*        Date:  27/11/2017
*        Purpose of modification:  Inline image paste in email body
*        Method/Code segment modified: handlePasteFunction      
*/
({
    //added by Atul Hinge.
    //To initialised component data
    initializeComponent : function(component, event) {
        
        if(component.get("v.screenNo")!=2){
            component.set("v.showSpinner",true);
            var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
            createCmpEvent.setParams({
                "objectType":"CaseManager__c",
                "fsName":"EmailProcessingFields",
                "paramName":"Case",
                "size":3,
                "component": component,
                "callback" : function( response){
                    component.set("v.showSpinner",false);
                    component.set("v.processingfields",response);
                } ,
                "Action":'getFieldset',
            });
            createCmpEvent.fire();
        } 
        var action = component.get("c.initializeComponent");
        //  action.setParams({ interactionId : component.get("v.interactionId")});
        //alert(component.get("v.interactionId"));
        var parameters = {
            "interactionId" : component.get("v.interactionId"),
            "Mailbox" : component.get("v.Case").Mailbox_Name__c	,
            "emailTemplateFolder" : 'Notification'
        };
        action.setParams({
            "requestParm": JSON.stringify(parameters)
        });
        action.setCallback(this, function(response) {
            var title=component.get('v.title');
            var state = response.getState();
            if (state === "SUCCESS") {
                var responceVar =response.getReturnValue();
                component.set("v.orgWideEmailAddress",response.getReturnValue().orgWideEmailAddress);
                //component.set("v.fromAddress",response.getReturnValue().orgWideEmailAddress[0].DisplayName);
                //component.set("v.selectedFromAddress",response.getReturnValue().orgWideEmailAddress[0].Id);
                component.set("v.templates",response.getReturnValue().templates);
               // alert(response.getReturnValue().templates);
			   /* Purpose: To update PredictedValue status of Indexing Field		start	*/	
               if(response.getReturnValue().predictedTemplate!=undefined && (title=='Reply' || title=='Reply All')){					
					component.set("v.selectedTemplate" ,response.getReturnValue().predictedTemplate);				
				}
				else{
                    if(response.getReturnValue().templates.length >0){
                    	component.set("v.selectedTemplate",response.getReturnValue().templates[0].value);    
                    }
				}
				/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
                //console.log(response.getReturnValue().trailMailBody);
                /**PUX-262**/
                if(response.getReturnValue().trailMailBody == ''){	
                    if(response.getReturnValue().templates.length >0){
                    	component.set("v.emailbody",this.populateValues(component,response.getReturnValue().templates[0].body));
                    }
                }else{
                    
                    component.set("v.emailbody",response.getReturnValue().trailMailBody);
                }
                component.set("v.trailMailBody",response.getReturnValue().trailMailBody);
                component.set("v.subject",this.populateValues(component,response.getReturnValue().templates[0].Subject));
                component.set("v.allContacts",response.getReturnValue().contacts); 
                //EmailMessage
                var emailMessage =response.getReturnValue().EmailMessage;
                
                if(title=='Reply' || title=='Reply All'){
				/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
					this.changeTemplate(component,event);
				/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
                    if(emailMessage.FromAddress !== undefined){
                        component.set("v.toContacts",this.populateContacts(component,emailMessage.FromAddress.split(";")));
                    }
                }
                if(title=='Reply All'){
                    if(emailMessage.CcAddress !== undefined){
                        component.set("v.ccContacts",this.populateContacts(component,emailMessage.CcAddress.split(";")));
                    }
                    if(emailMessage.BccAddress !== undefined){
                        component.set("v.bccContacts",this.populateContacts(component,emailMessage.BccAddress.split(";")));
                    }
                }
				//var owea=component.get("v.orgWideEmailAddress");
				var orgWideEmailAddresses = response.getReturnValue().orgWideEmailAddress;
				component.set('v.orgWideEmailAddressList',orgWideEmailAddresses);
                var mailboxAddress = component.get('v.mailBoxAddress');
                var matchedOrgWideAddress = orgWideEmailAddresses.find(CallbackFunctionToFindMailBoxName,mailboxAddress);
                if(!$A.util.isUndefinedOrNull(matchedOrgWideAddress) && matchedOrgWideAddress){
                    component.set("v.fromAddress",matchedOrgWideAddress.DisplayName + ' < '+matchedOrgWideAddress.Address + ' >');
                    if(matchedOrgWideAddress.Id!=null){
                        component.set("v.selectedFromAddress",matchedOrgWideAddress.Id);
                    }
                    else{
                        component.set("v.selectedFromAddress",'');
                    }
                }else{
                    if(orgWideEmailAddresses.length >0 && orgWideEmailAddresses[0].DisplayName){
                    	component.set("v.fromAddress",orgWideEmailAddresses[0].DisplayName + ' < '+orgWideEmailAddresses[0].Address + ' >');    
                    }
                    if(orgWideEmailAddresses[0].Id!=null){
                        component.set("v.selectedFromAddress",orgWideEmailAddresses[0].Id);
                    }
                    else{
                        component.set("v.selectedFromAddress",'');
                    }
                }
            }else if (state === "INCOMPLETE") {
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
            
       function CallbackFunctionToFindMailBoxName(ordWideAddress) {
           return ordWideAddress.DisplayName === this.toString();
        }
    },
    //added by Atul Hinge.
    //To change Template
    changeTemplate : function(component, event) {
        var templates=component.get("v.templates");
        for(var i =0 ; i<templates.length ;i++){			
            if(templates[i].value==component.get("v.selectedTemplate") && component.get("v.selectedTemplate")!=="TrailMail"){      
                var templateEmailBody =  this.populateValues(component,templates[i].body);
                var trailEmailBody ='<br/>' + this.populateValues(component, component.get("v.trailMailBody"));
                var finalEmailBody = templateEmailBody + trailEmailBody;
                component.set("v.emailbody",finalEmailBody);
                //component.set("v.subject",this.populateValues(component,templates[i].Subject));  
            }
        }
    },
    //added by Atul Hinge.
    //To send email for (Reply,ReplyAll,Forward,Compose)
    sendEmail : function(component, event) {
        component.set("v.showSpinner",true);
        var action = component.get("c.sendMail");
        var emailWrapper={};
        /*-- Start : PUX-199 */
        if(!this.validateEmailDetails(component, event)){
				return false;
			}
        
        if(component.get("v.toContacts")=='' || component.get("v.toContacts")==undefined ){
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'hideme');
            $A.util.addClass(cmpTarget, 'showme');
            component.set("v.showSpinner",false);
            return false;
        }else{
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'showme');
        } 
        /*-- End : PUX-199 */
        
        emailWrapper.sendTo=this.getEmailList(component,component.get("v.toContacts"));
        emailWrapper.sendCc=this.getEmailList(component,component.get("v.ccContacts"));
        emailWrapper.sendBcc=this.getEmailList(component,component.get("v.bccContacts"));
        emailWrapper.sendEsc=this.getEmailList(component,component.get("v.escContacts"));
        
        emailWrapper.subject=component.get("v.subject");
        emailWrapper.fromAddress=component.get("v.selectedFromAddress");
		//alert(emailWrapper.fromAddress);
        emailWrapper.attachmentIds=[];
        var nameSpace = component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var attachments=component.get("v.selectedFileObjects");
        for(var i=0;i<attachments.length;i++){
            emailWrapper.attachmentIds.push(attachments[i].ContentDocument.Id);
        }
        var caseTracker=component.get("v.Case");
        if(component.get("v.responseRequired")){
            caseTracker[nameSpace+"User_Action__c"]='Send For Email Resolution';
        }
        action.setParams({ emailJSON : JSON.stringify(emailWrapper),
                          emailBody:component.get("v.emailbody"),
                          ct:caseTracker,
                          isUpdate:false,
                         });                
        action.setCallback(this, function(response) {
            component.set("v.showSpinner",false);
            var state = response.getState();
            if (state === "SUCCESS") {
              if(component.get('v.saveAndNext')){
                  console.log('saving and sending email');
            	    this.updateCase(component,event);
               }
                var callback=component.get("v.callback");
                if(typeof callback ==='function'){
                    callback(response.getReturnValue());
                }
                var refreshInteractionCountEvt = $A.get("e.c:RefreshInteractionCountEvent");
				refreshInteractionCountEvt.fire();
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({ "title" : "Success !","message":"Email has been sent" });
                appEvent.fire();
                component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));
                /* Start : PUX-189 */
                component.set("v.interactionRefCounter",component.get("v.interactionRefCounter")+1);
                /* End : PUX-189 */
                
            }else if (state === "INCOMPLETE") {
                alert('incomplete');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    //added by Atul Hinge.
    //replace regular expression in EmailTemplate with proper values
    populateValues : function(component, str) {
        while(str.indexOf("{!")>=0){
            var expr =str.substring(str.indexOf("{!"),str.indexOf("}")+1);
            str =str.replace(expr,this.getValue(component,expr.substring(2,expr.indexOf("}"))));
        }
     //   console.log(str);
        return str;        
    },
    //added by Atul Hinge.
    //To populate contacts which are not in contact object list
    populateContacts : function(component,emailIds) {
        var contacts=[];
        for(var i=0;i<emailIds.length;i++){
            var obj = {
                "Name": emailIds[i],
                "Id": emailIds[i],
                "Email": emailIds[i]
            };
            contacts.push(obj);
        }
        return contacts;
    },
    getValue : function(component, str) {
        var obj =component.get("v.Case");
        return obj[str.split('.')[1]];
    },
    //added by Atul Hinge.
    //To update case detail record
    updateCase: function(component, event) {
        var caseId = component.get("v.Case.Id");
        var compEvent = component.getEvent("caseAction");
        //console.log(component.get('v.Case'));
        compEvent.setParams({
            "Action": "update",
            "Case": component.get("v.Case"),
            "callback": function(response) {
                component.set("v.ct", response.getReturnValue()[0]);
                console.log(component.get('v.saveAndNext'));
                //if(!component.get('v.saveAndNext')){
                	var appEvent = $A.get("e.c:ShowToast");
	                appEvent.setParams({
	                    "title": "Success !",
	                    "message": " Case is successfully saved!"
	                });
	                appEvent.fire();
                //}
            }
        });
        compEvent.fire();
    },
    //added by Atul Hinge.
    //returns a list of all Email addressed in Contact list;
    getEmailList : function(component, contactList) {
        var emails=[];
        for(var i=0;i<contactList.length;i++){
            emails.push(contactList[i].Email);
        }
        return emails;
    },
    //added by Atul Hinge.
    //To prepopulate contacts
    getPreContacts : function(component, str) {
        var interaction = response.getReturnValue().EmailMessage.FromAddress; 
        var contacts=response.getReturnValue().contacts;
        var toContacts=[];
        for(var i=0;i<contacts.length;i++){
            if(contacts[i].Email==interaction){
                toContacts.push(contacts[i]);
            }
        }
    },
    validateEmailDetails:function(component,event){
		try{
			var bccValue = component.get("v.showBCC");
			var ccValue = component.get("v.showCC");
			var escValue = component.get("v.showESC");
       		var valid = true
			var fromAddress=component.get("v.selectedFromAddress");
			//console.log(fromAddress);
			if(fromAddress==null || fromAddress==''){
				var cmpTarget = component.find('messageFromId');
				$A.util.removeClass(cmpTarget, 'hideme');
				$A.util.addClass(cmpTarget, 'showme');
				component.set("v.showSpinner",false);
				valid =  false;
			}
			/*-- Start : PUX-199 */
			var toAll = component.get("v.toContacts");
			for(var i=0; i<toAll.length; i++){
				var atpos = toAll[i].Email.indexOf("@");
				var dotpos = toAll[i].Email.lastIndexOf(".");
				if(atpos < 1 || dotpos< atpos+2)
				{
					var cmpTarget = component.find('messageId');
					$A.util.removeClass(cmpTarget, 'hideme');
					$A.util.addClass(cmpTarget, 'showme');
					component.set("v.showSpinner",false);
                    
                   	console.log('invalid To address');
					valid =  false;
				}
			}

			if(component.get("v.toContacts")=='' || component.get("v.toContacts")==undefined ){
				var cmpTarget = component.find('messageId');
				$A.util.removeClass(cmpTarget, 'hideme');
				$A.util.addClass(cmpTarget, 'showme');
				component.set("v.showSpinner",false);
				valid =  false;
			}else{
				var cmpTarget = component.find('messageId');
				$A.util.removeClass(cmpTarget, 'showme');
			} 
			/*-- End : PUX-199 */
			/*-- Start :PUX-784 */
			var ccAll = component.get("v.ccContacts");
			if(ccAll.length>0){
				for(var i=0; i<ccAll.length; i++){
                var atpos = ccAll[i].Email.indexOf("@");
                var dotpos = ccAll[i].Email.lastIndexOf(".");
                
	                if(atpos < 1 || dotpos< atpos+2)
	                {	
	                	//this.superAfterRender();
	                    var cmpTargetCC = component.find('messageIdForCC');
	                    $A.util.removeClass(cmpTargetCC, 'hideme');
	                    $A.util.addClass(cmpTargetCC, 'showme');
	                    component.set("v.showSpinner",false);
	                    valid =  false;
	                }
	            }
			
			}else{
					var cmpTargetCC = component.find('messageIdForCC');
					$A.util.removeClass(cmpTargetCC, 'showme');
					$A.util.addClass(cmpTargetCC, 'hideme');
			
			}
            
			
			
			var bccAll = component.get("v.bccContacts");
			if(bccAll.length>0){
				for(var i=0; i<bccAll.length; i++){
				var atpos = bccAll[i].Email.indexOf("@");
				var dotpos = bccAll[i].Email.lastIndexOf(".");
                var cmpTargetBCC = component.find('messageIdForBCC');
				if(atpos < 1 || dotpos< atpos+2)
				{
					$A.util.removeClass(cmpTargetBCC, 'hideme');
					$A.util.addClass(cmpTargetBCC, 'showme');
					component.set("v.showSpinner",false);
                    //console.log('invalid CC address');
						valid =  false;
					}
				}
			
			}else{
				var cmpTargetBCC = component.find('messageIdForBCC');
					$A.util.removeClass(cmpTargetBCC, 'showme');
					$A.util.addClass(cmpTargetBCC, 'hideme');
			
			}
			
			/*
			if((component.get("v.bccContacts")=='' || component.get("v.bccContacts")==undefined) && bccValue)
			{
					var cmpTargetBCC = component.find('messageIdForBCC');
					$A.util.removeClass(cmpTargetBCC, 'hideme');
					$A.util.addClass(cmpTargetBCC, 'showme');
					component.set("v.showSpinner",false);
					valid =  false;
			}else{
					var cmpTargetBCC = component.find('messageIdForBCC');
					$A.util.removeClass(cmpTargetBCC, 'showme');
					$A.util.addClass(cmpTargetBCC, 'hideme');
			}
*/

			var escAll = component.get("v.escContacts");
			if(escAll.length >0){
				for(var i=0; i<escAll.length; i++){
					var atpos = escAll[i].Email.indexOf("@");
					var dotpos = escAll[i].Email.lastIndexOf(".");
						if(atpos < 1 || dotpos< atpos+2)
						{
							var cmpTargetESC = component.find('messageIdForESC');
							$A.util.removeClass(cmpTargetESC, 'hideme');
							$A.util.addClass(cmpTargetESC, 'showme');
							component.set("v.showSpinner",false);
							valid =  false;
						}
				}
			}else{
					var cmpTargetESC = component.find('messageIdForESC');
					$A.util.removeClass(cmpTargetESC, 'showme');
					$A.util.addClass(cmpTargetESC, 'hideme');
			}
			/*
			if((component.get("v.escContacts")=='' || component.get("v.escContacts")==undefined) && escValue)
			{
					var cmpTargetESC = component.find('messageIdForESC');
					$A.util.removeClass(cmpTargetESC, 'hideme');
					$A.util.addClass(cmpTargetESC, 'showme');
					component.set("v.showSpinner",false);
					valid = false;
			}else{
					var cmpTargetESC = component.find('messageIdForESC');
					$A.util.removeClass(cmpTargetESC, 'showme');
					$A.util.addClass(cmpTargetESC, 'hideme');
			}*/
		}
		catch (e) {
            this.showToast(component,'JS Error: ', e.message, 'error');
        }
		return valid;
	},
	processUserAction:function(component,event){
		try{
			var selectedMenuItemValue = component.get("v.selectedUserAction");
			component.set("v.showSpinner",true);

			if(!this.validateEmailDetails(component, event)){
				return false;
			}
			var emailWrapper=this.createEmailDetailJSON(component, event);

			component.set("v.showSpinner",false);

			var appEventUserActionChange = $A.get("e.c:userActionChange");
			console.log(component.get('v.emailbody'));
			component.set('v.emailbody', component.get('v.emailbody'));
			appEventUserActionChange.setParams({
				"userAction":selectedMenuItemValue,
				"emailJSON": JSON.stringify(emailWrapper),
				"emailBody": component.get("v.emailbody"),
				"eventSource":"Email",                   
			});

			appEventUserActionChange.fire();
		}
		catch (e) {
			console.log('error');
            this.showToast(component,'JS Error: ', e.message, 'error');
        }
	},
	createEmailDetailJSON:function(component,event){
		try{
			var emailWrapper={};
			/*-- End :PUX-784 */
			emailWrapper.sendTo=this.getEmailList(component,component.get("v.toContacts"));
			emailWrapper.sendCc=this.getEmailList(component,component.get("v.ccContacts"));
			emailWrapper.sendBcc=this.getEmailList(component,component.get("v.bccContacts"));
			emailWrapper.sendEsc=this.getEmailList(component,component.get("v.escContacts"));
        
			emailWrapper.subject=component.get("v.subject");
			emailWrapper.fromAddress=component.get("v.selectedFromAddress");
			emailWrapper.attachmentIds=[];
			var nameSpace = component.getConcreteComponent().getDef().getDescriptor().getNamespace();
			
			var attachments=component.get("v.selectedFileObjects");
			for(var i=0;i<attachments.length;i++){
				emailWrapper.attachmentIds.push(attachments[i].ContentDocument.Id);
			}
            //console.log('emailWrapper');
            //console.log(emailWrapper);
			return emailWrapper;
		}
		catch (e) {
            this.showToast(component,'JS Error: ', e.message, 'error');
        }
	},
	showToast: function(component, title, message, msgtype){
        var tosterError = $A.get("e.c:ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
    },
	
    /*
		updateFromNameHelper : function(component,event){
        var mailboxAddress = component.get('v.Case.Mailbox_Name__c');
        var orgWideEmailAddresses = component.get('v.orgWideEmailAddressList');
        var matchedOrgWideAddress = orgWideEmailAddresses.find(CallbackFunctionToFindMailBoxName,mailboxAddress);
        component.set("v.fromAddress",matchedOrgWideAddress.DisplayName + ' < '+matchedOrgWideAddress.Address + ' >');
        if(matchedOrgWideAddress.Id!=null){
            component.set("v.selectedFromAddress",matchedOrgWideAddress.Id);
        }
        else{
            component.set("v.selectedFromAddress",'');
        }
        
        function CallbackFunctionToFindMailBoxName(ordWideAddress) {
           return ordWideAddress.DisplayName === this.toString();
        }
    }
     */
        
})