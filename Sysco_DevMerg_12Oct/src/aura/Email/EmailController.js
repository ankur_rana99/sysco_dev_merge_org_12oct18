/*
* Authors	   :  Atul Hinge
* Date created :  14/08/2017
* Purpose      :  For email releated activity like reply,replyAll, forward.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-113,PUX-39,PUX-118,PUX-113,PUX-132,PUX-217,PUX-218,PUX-225,PUX-194
* -----------------------------------------------------------
* Modifications: 
*        Date:  27/11/2017
*        Purpose of modification:  Inline image paste in email body
*        Method/Code segment modified: handlePaste   
*/
({	
    //added by Atul Hinge.
    //To initialised component data
    doInit : function(component, event, helper) {
        component.set('v.Case.User_Action__c','');
        component.set("v.selectedFileObjects",[]);
        var CaseMgr = component.get('v.Case');
        if(CaseMgr.Mailbox_Name__c){
            var mailBoxAddress = CaseMgr.Mailbox_Name__c;
            if(!$A.util.isUndefinedOrNull(mailBoxAddress) && mailBoxAddress){
                component.set('v.mailBoxAddress', mailBoxAddress);
            }
        }
        
        helper.initializeComponent(component, event);
        var fromAddress={};
        if(CaseMgr.OwnerId.indexOf($A.get("$SObjectType.CurrentUser.Id")) == -1){
				component.set("v.screenNo",2);
			}
        //component.set("v.fromAddress",'');
        if(component.get("v.screenNo")!=2){
            component.set("v.screenNo",1);
        }  
        
        var userAction=[];
        $("#User_Action__c").find("select > option").each( function(){
            if($(this).val()!=''){
                if($(this).val() != 'Make Available'){
                    userAction.push($(this).val());
                }
            }
        });
        component.set('v.LoginUserId', window.LoginUserId);
        if(component.get('v.LoginUserId') == component.get('v.Case.OwnerId')){
            component.set('v.userOwnerMatch', true); 
        }else{
            component.set('v.userOwnerMatch', false); 
        }
        component.set("v.userActions",userAction);

    },
    //added by Atul Hinge.
    //To Send Email
    sendEmail : function(component, event, helper) {
        helper.sendEmail(component, event);
        
    },
    //added by Atul Hinge.
    //To save caseTracker object
    save:function(component, event, helper) {
	
        var appEvent = $A.get("e.c:ValidateComponentEvent");
        var test=component.get("v.processingfields");
        appEvent.setParams({ "components" : test,"callback":function(response){ 
            if(response.success== true){
                helper.updateCase(component, event);
                component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));
            }
        }
                           });
        appEvent.fire();
    },
    //added by Atul Hinge.
    //To close Email Editor
    close : function(component, event, helper) {
        component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));  
    },
    //added by Atul Hinge.
    //To move to next screen
    next:function(component, event, helper) {

        var appEvent = $A.get("e.c:ValidateComponentEvent");
        var test=component.get("v.processingfields");
        appEvent.setParams({ "components" : test,"callback":function(response){ 
            if(response.success== true){
            	component.set("v.saveAndNext",true);
                //helper.updateCase(component, event);
                component.set("v.screenNo",component.get('v.screenNo')+1);
            }
        }
    });
        appEvent.fire();
        
    },
    //added by Atul Hinge.
    //To move to next screen
    Previous:function(component, event, helper) {
        component.set("v.screenNo",component.get('v.screenNo')-1);
    },
    toogleFromAddressEditor:function(component, event, helper) {
        component.set("v.showFromAddressSelector",!component.get('v.showFromAddressSelector'));
        component.find("selectFrom").focus();
    },
    onFromAddressChange:function(component, event, helper) {
        // component.get
        var owea=component.get("v.orgWideEmailAddress");
        var selectedFromAddress=component.get("v.selectedFromAddress");
        for(var i=0;i<owea.length;i++){
            if(selectedFromAddress==owea[i].Id){
                component.set("v.fromAddress",owea[i].DisplayName)
            }
        }
        component.set("v.showFromAddressSelector",!component.get('v.showFromAddressSelector'));
    },
    //added by Atul Hinge.
    //To display cc Email addresses
    toggleCC:function(component, event, helper) {
        component.set("v.showCC",!component.get('v.showCC'));
    },
    //added by Atul Hinge.
    //To display bcc Email addresses
    toggleBCC:function(component, event, helper) {
        component.set("v.showBCC",!component.get('v.showBCC'));
    },
    //added by Atul Hinge.
    //To display cc Email addresses
    toggleESC:function(component, event, helper) {
        component.set("v.showESC",!component.get('v.showESC'));
    },
    //added by Atul Hinge.
    //To Change Email Templates
    changeTemplate :function(component, event, helper){
        helper.changeTemplate(component, event);
    },
    //added by Atul Hinge.
    // call if case record changes 
    caseFieldChange:function(component, event, helper) {
        console.log('case field changes');
        console.log(component.get('v.Case'));
        //helper.changeTemplate(component, event);
    },
    //added by Atul Hinge.
    //display or hide Tool bar on email editor
    toogleToolbar:function(component, event, helper) {
        //component.set("v.disabledCategories",!component.get('v.disabledCategories'));
        var divsToHide = document.getElementsByClassName("slds-rich-text-editor__toolbar slds-shrink-none");

        for(var i = 0; i < divsToHide.length; i++)
        {
            if(divsToHide[i].style.display=="none")
        		divsToHide[i].style.display="";
            else
                divsToHide[i].style.display="none";
        }
    },
    //added by Atul Hinge.
    //display or hide attachment select section
    toogleModal:function(component, event, helper){
        component.find('addAttachment').toogle();
    },
    //added by Atul Hinge.
    //display or hide selected attachment list
    selectedFileObjectsChange:function(component, event, helper){
        
        if(component.get("v.selectedFileObjects").length>0){
            component.set("v.showAttached",true);
        }else{
            component.set("v.showAttached",false);
        }
        
    },
    //added by Atul Hinge.
    //remove attachment from selected attachment list
    removeAttachment:function(component, event, helper){
        var removeName=event.getSource().get("v.name");
        
        var uploadedFileObjects=component.get("v.selectedFileObjects");
        var uploadedFileObjectsSet=[];
        for(var i=0;i<uploadedFileObjects.length;i++){
            if(uploadedFileObjects[i].Id !=removeName){
                uploadedFileObjectsSet.push(uploadedFileObjects[i]);
            }
        }
        component.set("v.selectedFileObjects",uploadedFileObjectsSet);
        
        
    },
    /* Start : PUX-199  */
    changeToAddress:function(component, event, helper){
        var cmpTarget = component.find('messageId');
        $A.util.addClass (cmpTarget, 'hideme');
        $A.util.removeClass(cmpTarget, 'showme');
    },
    /* End : PUX-199  */
    
    handleMenuSelect:function(component, event, helper){
	try{
			var selectedMenuItemValue = event.getParam("value");
			component.set("v.selectedUserAction",selectedMenuItemValue);
			helper.processUserAction(component, event);
		}
		catch (e) {
            helper.showToast(component,'JS Error: ', e.message, 'error');
        }
	},
    processUserAction:function(component, event, helper){
		helper.processUserAction(component, event);
	},
	handleUserActionChange:function(component, event, helper){
		var userAction = event.getParam("userAction");
		var eventSource=event.getParam("eventSource");
		if(eventSource=='ProcessingField'){
			component.set("v.selectedUserAction",userAction);
		}
		console.log(component.get('v.emailbody'));
	}
	
    /*
    handleMenuSelect:function(component, event, helper){
        var selectedMenuItemValue = event.getParam("value");
        
        console.log(selectedMenuItemValue);
        component.set("v.showSpinner",true);
        var action = component.get("c.sendMail");
        var bccValue = component.get("v.showBCC");
        var ccValue = component.get("v.showCC");
        var emailWrapper={};
        if(component.get("v.toContacts")=='' || component.get("v.toContacts")==undefined ){
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'hideme');
            $A.util.addClass(cmpTarget, 'showme');
            component.set("v.showSpinner",false);
            return false;
        }else{
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'showme');
        } 
        if((component.get("v.ccContacts")=='' || component.get("v.ccContacts")==undefined) && ccValue)
        {	
            var cmpTargetCC = component.find('messageIdForCC');
            $A.util.removeClass(cmpTargetCC, 'hideme');
            $A.util.addClass(cmpTargetCC, 'showme');
            component.set("v.showSpinner",false);
            return false;
        }else{
            var cmpTargetCC = component.find('messageIdForCC');
            $A.util.removeClass(cmpTargetCC, 'showme');
            $A.util.addClass(cmpTargetCC, 'hideme');
        }
        if((component.get("v.bccContacts")=='' || component.get("v.bccContacts")==undefined) && bccValue)
        {
            var cmpTargetBCC = component.find('messageIdForBCC');
            $A.util.removeClass(cmpTargetBCC, 'hideme');
            $A.util.addClass(cmpTargetBCC, 'showme');
            component.set("v.showSpinner",false);
            return false;
        }else{
            var cmpTargetBCC = component.find('messageIdForBCC');
            $A.util.removeClass(cmpTargetBCC, 'showme');
            $A.util.addClass(cmpTargetBCC, 'hideme');
        }
        
        emailWrapper.sendTo=helper.getEmailList(component,component.get("v.toContacts"));
        emailWrapper.sendCc=helper.getEmailList(component,component.get("v.ccContacts"));
        emailWrapper.sendBcc=helper.getEmailList(component,component.get("v.bccContacts"));
        emailWrapper.sendEsc=helper.getEmailList(component,component.get("v.escContacts"));
        
        emailWrapper.subject=component.get("v.subject");
        emailWrapper.fromAddress=component.get("v.selectedFromAddress");
        //alert(emailWrapper.fromAddress);
        emailWrapper.attachmentIds=[];
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var attachments=component.get("v.selectedFileObjects");
        for(var i=0;i<attachments.length;i++){
            emailWrapper.attachmentIds.push(attachments[i].ContentDocument.Id);
        }
        
        component.set("v.showSpinner",false);
        if(selectedMenuItemValue =='Make Available'){
            var enablePicklistEventChange = $A.get("e.c:TogglePicklistStatus");
            enablePicklistEventChange.setParams({
                "status": 'enable'
            });
            enablePicklistEventChange.fire();
        }
        	
		var appEventUserActionChange = $A.get("e.c:userActionChange");
        appEventUserActionChange.setParams({
            "userAction":selectedMenuItemValue,
			"emailJSON": JSON.stringify(emailWrapper),
            "emailBody": component.get("v.emailbody"),                  
        });

        appEventUserActionChange.fire();

    },
*/

    
    /*
	updateFromName : function(component,event,helper){
        helper.updateFromNameHelper(component,event);
    }
     */
})