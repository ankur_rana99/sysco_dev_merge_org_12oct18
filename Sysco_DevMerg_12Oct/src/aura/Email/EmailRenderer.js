({
    rerender: function(component, event, helper) {
        this.superRerender();
        // do custom rerendering here
        // interact with the DOM here
        if (component.get("v.pasteCount") == 1 && document.getElementsByClassName('ql-editor')[0] != undefined) {
            document.getElementsByClassName('ql-editor')[0].addEventListener("paste", handlePaste);

            function handlePaste(event) {
                var item = event.clipboardData.items[0];
                if (item.type.indexOf("image") - 1 && item.type != 'text/plain') {
                    var blob = item.getAsFile();
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        // var emailB = component.get('v.emailbody');
                        // emailB = emailB + '<img height="100" src="'+event.target.result+'">';
                        // component.set('v.emailbody',emailB);
                        html = '<img width=658 height=369 src="' + event.target.result + '">';
                        var sel, range;
                        if (window.getSelection) {
                            // IE9 and non-IE
                            sel = window.getSelection();
                            if (sel.getRangeAt && sel.rangeCount) {
                                range = sel.getRangeAt(0);
                                range.deleteContents();
                                // Range.createContextualFragment() would be useful here but is
                                // non-standard and not supported in all browsers (IE9, for one)

                                var el = document.createElement("div");
                                el.innerHTML = html;
                                var frag = document.createDocumentFragment(),
                                    node, lastNode;
                                while ((node = el.firstChild)) {
                                    lastNode = frag.appendChild(node);
                                }
                                range.insertNode(frag);
                                // Preserve the selection
                                if (lastNode) {
                                    range = range.cloneRange();
                                    range.setStartAfter(lastNode);
                                    range.collapse(true);
                                    sel.removeAllRanges();
                                    sel.addRange(range);
                                }
                            }
                        } else if (document.selection && document.selection.type != "Control") {
                            // IE < 9
                            document.selection.createRange().pasteHTML(html);
                        }
                    };
                    reader.readAsDataURL(blob);
                }
            }
            component.set("v.pasteCount", component.get("v.pasteCount") + 1);
        }
    }
})