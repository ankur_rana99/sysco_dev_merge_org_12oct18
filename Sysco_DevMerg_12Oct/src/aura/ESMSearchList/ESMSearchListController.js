/*
 *	Name 		   : CaseListController.js
 *   Author         : Niraj Prajapati
 *   Date   		   : 09-Aug-2017
 *   Description    : To display all Cases tab wise with the help of CaseListItem Component.
 *   JIRA ID        : PUX-23
 *
 *	____________________________________________________________
 *	Modifications: 1
 *			Authors:  Tushar Moradiya
 *			Date:  18-Sep-2017
 *			Purpose of modification:  Implement filters on list page.
 *			Method/Code segment modified: PUX-305.
 *	____________________________________________________________
 *	Modifications: 2
 *			Authors:  Tushar Moradiya
 *			Date:  11-Dec-2017
 *			Purpose of modification: Filter removed while we go bake on case list view
 *			Method/Code segment modified: PUX-421
 */
({
    /*
		Authors: Tushar Moradiya
		Purpose: To assign default value when page is load.
				 It will be called at very first time when component render	
				 To fetch all cases as per selected tab and prepare filter dropdown according	
	*/
    doInit: function(component, event, helper) {
        /* Start : PUX-304 *///component.toString().split('":"')[1].split('"}')[0]
        // Start : PUX-501showHideSplitMerge
        
        console.log('==============selectedCases=================='+component.get("v.selectedCases"));
		component.get("v.showHideSplitMerge",'none');
        var tempPageNo = component.get("v.tempPageNumber");
        component.set("v.pageNumber", tempPageNo);
        // End : PUX-501
        var conMainEle = component.find("filterMainDiv");
        if (component.get('v.condition') == 'Unassigned') {
            if ($A.util.hasClass(conMainEle, 'slds-hide')) {
                $A.util.removeClass(conMainEle, 'slds-hide');
            }
        } else {
            $A.util.removeClass(conMainEle, 'slds-show');
            $A.util.addClass(conMainEle, 'slds-hide');
        }
        var emptyList = [];
        component.set('v.caseList', emptyList);
        //PUX-207 
        if (component.get('v.resetPagination')) {
            component.set('v.pageNumber', 1);
            //component.set('v.pageNo',1);
        }
        component.set('v.maxPage', 1);
        //component.set('v.direction','first');
        //PUX-207
        component.set('v.caseList', emptyList);
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.addClass(noRecordfoundMsgDiv, 'toggleDiv');
        /* End : PUX-304 */
        /* Start : PUX-305 ,PUX-543*/
        var conEle = component.find("filterDiv");
        if ($A.util.hasClass(conEle, 'slds-is-open')) $A.util.removeClass(conEle, 'slds-is-open');
        /* End : PUX-543 */

       
        component.set("v.selectedFilter", null);
       
        helper.getCases(component, event);
        /* End : PUX-305 */
        //	PUX-390 
        helper.getSortDropdownList(component, event);
	//	 helper.bulkPermissionCheckForUser(component, event);
		helper.getFieldSetMember(component, event, helper);
		helper.getFieldSetMemberWrap(component, event, helper);
		helper.getSearchConfig(component, event);	
        console.log('==============helper.isVendor==================');
		helper.isVendor(component, event, helper);
        //	PUX-390
    },
    /*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Req is being processed at server side	
	*/
    waiting: function(component, event, helper) {
        document.getElementById("Accspinner").style.display = "block";
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.addClass(noRecordfoundMsgDiv, 'toggleDiv');
    },
    
    /*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Res is received at client side	
	*/
    doneWaiting: function(component, event, helper) {
        if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
            document.getElementById("Accspinner").style.display = "none";
        }
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.removeClass(noRecordfoundMsgDiv, 'toggleDiv');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: It will be called on change page number in pagination
	*/
    renderPages: function(component, event, helper) {
        //Start : PUX-207, PUX-501	
        if (component.get("v.pageNumber") != component.get("v.tempPageNumber")) {
            // Rahul P. | 24-09-2018 | CASP2-75 | starts
            helper.getCasesWrap(component, event);
            // Rahul P. | 24-09-2018 | CASP2-75 | ends
        }
        //End : PUX-207, PUX-501	
    },
 
    /*
		Authors: Tushar Moradiya
		Purpose: Expand filter Div on click of filter button 
		//PUX-305	, PUX-543
	*/
    expandFilterDiv: function(component, event, helper) {
        var mainDiv = component.find('filterDiv');
        $A.util.toggleClass(mainDiv, 'slds-is-open');
    },
    /*
		Authors: Tushar Moradiya
		Purpose: Apply selected filter on cases
		//PUX-305	
	*/
    filterCase: function(component, event, helper) {
        var conEle = component.find("filterDiv");

        
            component.set("v.errors", "");
            component.set('v.sortField', 'LastModifiedDate');
            component.set('v.sortDirection', 'desc');
            component.set('v.pageNumber', 1);
            /* Code start for PUX-421 */
             
            /* Code end for PUX-421 */
			/* ---- added by Shital Rathod---- Jira-CASP2-11 reset button  & range functionality changes Start-----*/
           // helper.getCases(component, event);
			helper.getCasesWrap(component, event);
			/* ---- added by Shital Rathod---- Jira-CASP2-11 reset button  & range functionality changes END-----*/
            //start PUX-543
            if ($A.util.hasClass(conEle, 'slds-is-open'))
                $A.util.removeClass(conEle, 'slds-is-open');
            //end PUX-543
        /* Rahul P. | 24-09-2018| CASP2-75 | starts |  ESMPROD-1106 */
			var appEvent = $A.get("e.c:CheckedAll");
			appEvent.setParams({
				"action": 'deselected',
			});
			appEvent.fire();
			 /* Rahul P. | 24-09-2018| CASP2-75 | ends |  ESMPROD-1106 */
        
    },
	
    /* ---- added by Shital Rathod---- Jira-CASP2-11 reset button  & range functionality changes Start-----*/
	resetFilter: function(component, event, helper) {
	
		var oldValue = component.get("v.selectedFilter");
		console.log('==oldValue=='+JSON.stringify(oldValue));
		console.log('==wraplist=='+component.get("v.wraplist"));
		var wraplist = component.get("v.wraplist");
		console.log('******RESet'+JSON.stringify(wraplist));
		//component.set("v.selectedFilter", []);
	   // var  searchConfigName = component.get('v.searchConfigName');
		var values = [];
		 //for (var i = 0; i < oldValue.length; i++) {
			//console.log('in loop===='+JSON.stringify(oldValue[i]));	
			 for (var key in wraplist) {
				console.log('key== of Wrap'+JSON.stringify(wraplist[key].fieldValue1));
				wraplist[key].fieldValue1 = null;
                 wraplist[key].fieldValue2 = null;

			 }
				//invObj[i] = null;
				//wraplist[oldValue[i]] = null;
           // }
				console.log('====invOBj*****'+JSON.stringify(wraplist));
				component.set("v.wraplist",wraplist);
				component.set("v.selectedFilter",values);
				helper.getCasesWrap(component, event);

    },
    /* ---- added by Shital Rathod---- Jira-CASP2-11 reset button & range functionality changes end -----*/
    /*
		Authors: Tushar Moradiya
		Purpose: used for remove selected filter when close filter pill
		//PUX-305	
	*/
    handleRemove: function(component, event, helper) {
        var conEle = component.find("filterDiv");
        //start PUX-543
        if ($A.util.hasClass(conEle, 'slds-is-open'))
            $A.util.removeClass(conEle, 'slds-is-open');
        //end PUX-543
        var filterId = event.getSource().get("v.name");
		console.log('filterId==='+JSON.stringify(filterId));
        var oldValue = component.get("v.selectedFilter");
		console.log('oldValue==='+JSON.stringify(oldValue));
        var caseId = event.getSource().get("v.label").split(':')[1];
		//====== for remove indivisual Filter - added by Shital ======
		var wraplist = component.get("v.wraplist");
		var values = [];
		 
	 
            for (var key in wraplist) {
				
				if(wraplist[key].fieldAPI == filterId){
				console.log('==in IF===key==='+key);
					wraplist[key].fieldValue1 = null;
                    wraplist[key].fieldValue2 = null;
				}
				
			 }
        
		console.log('====invOBj==='+wraplist); 
        component.set("v.wraplist",wraplist);
        component.set("v.selectedFilter", values);
        //alert('in '+window.overDueIn);
      //  var action = component.get("c.getCases");
        helper.getCasesWrap(component, event);
    },
    //PUX-305 end
    /*
		Authors: Parth Lukhi
		Purpose: handleNewCase function is used for navigation to New Case on click of New Case button
		Dependencies: PLMLayout.cmp
                
    */
    handleNew: function(component, event, helper) {
		var ctarget = event.currentTarget;
		var id_str = ctarget.dataset.value;
		console.log('=='+id_str);
        var searchConfig = component.get("v.SearchConfigWrapper");
		var compEvent = component.getEvent("caseAction");
		var selected = component.get("v.selectedCases");	
		console.log('selected PO=='+JSON.stringify(component.get("v.selectedCases")));	
		for(var key in selected){
            //ankur 30.09.18
            //alert("This PO# : "+selected[key].PO_No__c+ selected[key].isLocked__c+" is already  consumed");
            if(selected[key].isLocked__c == true){
                alert("This PO# : "+selected[key].PO_No__c+" is already  consumed");
				return;
            }
            //ankur 30.09.18 end
			else if(selected[key].Merchandise_Vouched_Status__c == 'VC' || selected[key].Freight_Vouched_Status__c=='VC'){
                alert("This PO# : "+selected[key].PO_No__c+" is already vouched");
				return;
			}
		}
		
		if((id_str == 'New' || id_str == 'Clone') && !searchConfig.isCreatePer){
			alert("Insufficient access for create, please contact administrator.");
			return;
		}
	//	helper.handleNew(searchConfig.ObjectAPIName__c);
        compEvent.setParams({
            "Action": id_str,
			"SelectedCases":component.get("v.selectedCases"),
        });
        compEvent.fire();
    },
    /* Start : PUX-304 */
    /*
		Authors: Parth Lukhi
		Purpose: handleMenuSelect is called when action is selected from menu button on list view
		Dependencies: PLMLayout.cmp
                
    */
    handleMenuSelect: function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        var id = event.getSource().get("v.name");
        if (selectedMenuItemValue == "Accept") {
            helper.makeMeOwner(component, event, false);
        } else if (selectedMenuItemValue == "Split") {
            var splitCaseEvt = component.getEvent("splitMergeCaseEvent");
            splitCaseEvt.setParams({
                "cases": component.get("v.selectedCases"),
                "action": "split",
                "TabName": "Split Case"
            });
            // Fire the event
            splitCaseEvt.fire();
        } else if (selectedMenuItemValue == "Merge") {
            var mergeCaseEvt = component.getEvent("splitMergeCaseEvent");
            mergeCaseEvt.setParams({
                "cases": component.get("v.selectedCases"),
                "action": "merge",
                "TabName": "Merge Case"
            });
            // Fire the event
            mergeCaseEvt.fire();
        }
        /** Added By Uzeeta
         * Purpose : Change Owner Functionality */
       else if (selectedMenuItemValue == "BulkAssignment") {
           console.log(component.get("v.selectedCases"));
           component.set("v.isDisplay",true);
          
        }else if (selectedMenuItemValue == "BulkAllocation") {
           console.log(component.get("v.selectedCases"));
           component.set("v.isDisplayBulkAllocation",true);
          
        }
    },
    /* End : PUX-304 */
    /*
		Authors: Tushar Moradiya
		Purpose: close exapanded div on cancel or apply button
		//PUX-305	
	*/
    //PUX-305 start
    collepsFilterDivNew: function(component, event, helper) {
         
     //   component.set("v.selectedFilter", null);
      //  component.set("v.errors", '');
        var mainDiv = component.find('filterDiv');
        //start PUX-543
        $A.util.removeClass(mainDiv, 'slds-is-open');
        //end PUX-543
        //helper.showPopupHelper(component, 'modaldialog', 'slds-');
        //-- Added by Dilshad | JIRA:CAS-1151 | 17-07-2018 | Start --//
        var invNew = new Object;
        component.set("v.invObj", invNew);
        //-- Added by Dilshad | JIRA:CAS-1151 | 17-07-2018 | Start --//		
    },
    /*
		Authors: Tushar Moradiya
		Purpose: enabled/disabled input filter text box on selection of dropdown
		//PUX-305	
	*/
    
    /*
		Authors: Tushar Moradiya
		Purpose: validate date and email value of input filter based on selected filter
		//PUX-305	
	*/
   
    //PUX-305 end
    /*
		Authors: Rahul Pastagiya
		Purpose: Open Sorting dropdown on Case list page
		
	*/
    openSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.toggleClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Close Sorting dropdown on Case list page
		
	*/
    closeSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.removeClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Select sorting value from Sorting dropdown on Case list page
		
	*/
    selectedSortValue: function(cmp, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        var sort = ctarget.dataset.sort;
        var currentSelected = cmp.get('v.sortField');

        if (currentSelected == id_str) {
            if (sort == 'desc') {
                sort = 'asc';
            } else {
                sort = 'desc';
            }
        }
		 else {
            sort = 'desc'
        }
        ctarget.dataset.sort = sort;
        cmp.set('v.sortField', id_str);
        cmp.set('v.sortDirection', sort);
        cmp.set('v.pageNumber', 1);
        // Rahul P. | 24-09-2018 | CASP2-75 | starts
        helper.getCasesWrap(cmp, event);
		// Rahul P. | 24-09-2018 | CASP2-75 | ends

    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Reset sort on List view page
		
	*/
    resetSort: function(cmp, event, helper) {
        cmp.set('v.sortField', 'LastModifiedDate');
        cmp.set('v.sortDirection', 'desc');
        cmp.set('v.pageNumber', 1);
        helper.getCases(cmp, event);
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: toggle Sorting direction i.e desc or asc
		
	*/
    toggleSortDirection: function(cmp, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
    },
    //PUX-390	  
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "CaseManager__c",
			
        });
        createRecordEvent.fire();
    },

	checkedAll : function (component, event, helper) {		
		var selectedCheckBox = component.find("chkBox").getElements();
		var selection = $(selectedCheckBox).is(':checked');
		var action;
		if (selection == true) {
			action = 'selected';
		} else {
			action = 'deselected';
		}
		var appEvent = $A.get("e.c:CheckedAll");
		appEvent.setParams({
			"action": action,
		});
		appEvent.fire();
    } ,

	unChecked : function (component, event, helper) {	

		if(component.get("v.caseList.length") != 0 && (component.get("v.selectedCases.length") != component.get("v.caseList.length"))){
				console.log('Inside');
				var selectedCheckBox = component.find("chkBox").getElements();
				var selection = $(selectedCheckBox).is(':checked');
				if(selection == true){										
					$(selectedCheckBox).prop('checked', false);
				}
		}else if(component.get("v.caseList.length") != 0 && (component.get("v.selectedCases.length") == component.get("v.caseList.length"))){
				console.log('Inside else');
				var selectedCheckBox = component.find("chkBox").getElements();
				var selection = $(selectedCheckBox).is(':checked');
				if(selection == false){										
					$(selectedCheckBox).prop('checked', true);
				}
	
		}	
    } ,

	createInquiry: function (component, event, helper) {	
		component.set("v.isDisplayCreateInquiry",true);					
    } ,

	payMeEarly: function (component, event, helper) {	
		component.set("v.isDisplayPayMeEarly",true);	
						
    } ,

	download : function (component, event, helper) {	
		helper.download(component, event);	
						
    } ,

	poFlip: function (component, event, helper) {	
		var evt = $A.get("e.force:navigateToComponent");
		evt.setParams({
			componentDef : "c:MainPageInvoiceCreate",
			componentAttributes: {
				poFlip : 'a0L370000043Hh0,a1537000000KRhS',
			}
		});
		evt.fire();						
    } ,
	//added CASP2-138 somnath Start
	refreshButton:function(component, event, helper) {
		$A.get('e.force:refreshView').fire();
	},
	//added CASP2-138 somnath End 
})