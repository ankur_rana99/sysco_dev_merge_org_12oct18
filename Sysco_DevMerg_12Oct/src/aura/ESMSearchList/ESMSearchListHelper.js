/*
 *	Name 		   : CaseListController.js
 *   Author         : Niraj Prajapati
 *   Date   		   : 09-Aug-2017
 *   Description    : To display all Cases tab wise with the help of CaseListItem Component.
 *   JIRA ID        : PUX-23
 *
 *	____________________________________________________________
 *	Modifications: 1
 *			Authors:  Tushar Moradiya
 *			Date:  18-Sep-2017
 *			Purpose of modification:  Implement filters on list page.
 *			Method/Code segment modified: PUX-305  
 	Modifications 2:
                   Date: 02/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-678] Configuration ability of Case view
 */
({
    /*
		Authors: Tushar Moradiya
		Purpose: used for get cases on load and after applying filter
		//PUX-305	
	*/
    getCases: function(component, event) {

        var action = component.get("c.getCases");
        // Rahul P. | 24-09-2018 | CASP2-75 | starts
		if(component.find("chkBox")!=null || component.find("chkBox")!=undefined)
		{
			var selectedCheckBox = component.find("chkBox").getElements();
			$(selectedCheckBox).prop("checked", false);
		}
		// Rahul P. | 24-09-2018 | CASP2-75 | ends
        //Start : PUX-501	
		var searchConfigName = component.get('v.searchConfigName');
        var curPageNo = component.get("v.pageNumber")
        component.set("v.tempPageNumber", curPageNo);
        //End : PUX-501
        /* start : PUX-305 ,PUX-421*/
        component.set("v.filterMap", {});
        
        var selectFieldSecond, selectFieldThird;
         
        /* start : PUX-305 */
        var filterMap = component.get("v.filterMap");
          
        /* end : PUX-421 */
        /* Code start for PUX-358 */
        var overDue = window.overDueIn;
        //alert(overDue);
        if (overDue != undefined && overDue != '') {
            /* Code start for PUX-485 */
            if (overDue == 'Overdue')
                overDue = 'Overdue';
            else
                overDue = 'Pending';
            /* Code end for PUX-485 */
        } else {
            overDue = '';
        }
        /* Code End for PUX-358 */
        /* end : PUX-305  */
        //remove alert
        //alert(JSON.stringify(filterMap));
        //action.setStorable();

		console.log('condition '+ component.get("v.condition"));
		
		console.log('filetrjsOn=Wraplist===== '+ JSON.stringify(component.get("v.wraplist")));
		
        action.setParams({
            whereClause: component.get("v.condition"),
            //PUX-207
			searchConfigName :  searchConfigName,
            sortField: component.get("v.sortField"),
            sortDirection: component.get("v.sortDirection"),
            pageNumber: component.get("v.pageNumber"),
            recordsToDisplay: component.get("v.recordsToDisplay"),
            //PUX-207
            /* start : PUX-305 PUX-421 */
            selectedQueue: '',
            filterMap: null,
            overDueIn: overDue,
            /* end : PUX-305 PUX-421 */
			filterJson:JSON.stringify(component.get("v.invObj")),
        });

	
        component.set("v.showSpinner", true);
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
		/* start : PUX-305  PUX-421 */
                //PUX-678
                component.set("v.fieldSetKeyList", response.getReturnValue().fieldSetKeyList);  
                component.set("v.fieldSetFieldLabelMap", response.getReturnValue().keyFieldLabelMap);
                //PUX-678
                component.set("v.caseList", response.getReturnValue().cases);
                
                var tempList = [];
                
                if(response.getReturnValue().pillAPiMap != null && response.getReturnValue().pillAPiMap != undefined){
                    var cnt=1;
                	for(var key in response.getReturnValue().pillAPiMap)
                    {                       
                        tempList.push({
                            'fieldName': response.getReturnValue().pillAPiMap[key],
                            'displayValue':response.getReturnValue().pilllableMap[response.getReturnValue().pillAPiMap[key]],
                            'key': key
                        });
                        cnt++;
                    }    
                }
                
                /* Code start for PUX-358 */
                if (overDue != undefined && overDue != '') {
                    /* Code start for PUX-485 */
                    if (overDue == 'Overdue') tempList.push({
                        'fieldName': 'OverDue In',
                        'displayValue': 'Not In OverDue',
                        'key': 'filter4'
                    });
                    else tempList.push({
                        'fieldName': 'Status In',
                        'displayValue': 'Awaiting Response',
                        'key': 'filter4'
                    });
                    /* Code end for PUX-485 */
                }
                /* Code End for PUX-358 */
                component.set("v.selectedFilter", tempList);
                /* end : PUX-305 PUX-421*/
                //	console.log('Records No : '+response.getReturnValue().length+'\nmaxPage : '+Math.floor((response.getReturnValue().length+9)/10));
                //PUX-207							
                component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
                //Start : PUX-504
                component.set("v.totalRecords", response.getReturnValue().total);
                //End : PUX-504
                //PUX-207
                //this.renderPage(component);
                //component.find("selectedfilterDiv").set("v.value", test);
            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " +
                        //  errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

	/* ---- added by Shital Rathod- Jira-CASP2-11 reset button & range functionality changes Start -----*/
	 getCasesWrap: function(component, event) {

        var action = component.get("c.getCasesWrap");
            // Rahul P. | 24-09-2018 | CASP2-75 | starts
		if(component.find("chkBox")!=null || component.find("chkBox")!=undefined)
		{
			var selectedCheckBox = component.find("chkBox").getElements();
			$(selectedCheckBox).prop("checked", false);
		}
		// Rahul P. | 24-09-2018 | CASP2-75 | ends
        //Start : PUX-501	
		var searchConfigName = component.get('v.searchConfigName');
        var curPageNo = component.get("v.pageNumber")
        component.set("v.tempPageNumber", curPageNo);
        //End : PUX-501
        /* start : PUX-305 ,PUX-421*/
        component.set("v.filterMap", {});
        
        var selectFieldSecond, selectFieldThird;
         
        /* start : PUX-305 */
        var filterMap = component.get("v.filterMap");
          
        /* end : PUX-421 */
        /* Code start for PUX-358 */
        var overDue = window.overDueIn;
        //alert(overDue);
        if (overDue != undefined && overDue != '') {
            /* Code start for PUX-485 */
            if (overDue == 'Overdue')
                overDue = 'Overdue';
            else
                overDue = 'Pending';
            /* Code end for PUX-485 */
        } else {
            overDue = '';
        }
        /* Code End for PUX-358 */
        /* end : PUX-305  */
        //remove alert
        //alert(JSON.stringify(filterMap));
        //action.setStorable();

		console.log('condition '+ component.get("v.condition"));
		console.log('filetrjsOn====== '+ JSON.stringify(component.get("v.invObj")));
		console.log('filetrjsOn=Wraplist===== '+ JSON.stringify(component.get("v.wraplist")));
		
        action.setParams({
            whereClause: component.get("v.condition"),
            //PUX-207
			searchConfigName :  searchConfigName,
            sortField: component.get("v.sortField"),
            sortDirection: component.get("v.sortDirection"),
            pageNumber: component.get("v.pageNumber"),
            recordsToDisplay: component.get("v.recordsToDisplay"),
            //PUX-207
            /* start : PUX-305 PUX-421 */
            selectedQueue: '',
            filterMap: null,
            overDueIn: overDue,
            /* end : PUX-305 PUX-421 */
			filterJson:JSON.stringify(component.get("v.wraplist")),
        });


        component.set("v.showSpinner", true);
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
		/* start : PUX-305  PUX-421 */
                //PUX-678
                component.set("v.fieldSetKeyList", response.getReturnValue().fieldSetKeyList);  
                component.set("v.fieldSetFieldLabelMap", response.getReturnValue().keyFieldLabelMap);
                //PUX-678
                component.set("v.caseList", response.getReturnValue().cases);
                
                var tempList = [];
                console.log('====pillAPiMapWrap==='+response.getReturnValue().pillAPiMapWrap);
                if(response.getReturnValue().pillAPiMapWrap != null && response.getReturnValue().pillAPiMapWrap != undefined){
                    var cnt=1;
					
                	for(var key in response.getReturnValue().pillAPiMapWrap)
                    {                       
                        tempList.push({
                            'fieldName': response.getReturnValue().pillAPiMapWrap[key],
                            'displayValue':response.getReturnValue().pilllableMapWrap[response.getReturnValue().pillAPiMapWrap[key]],
                            'key': key
                        });
                        cnt++;
                    }    
                }
                console.log('----overDue--'+overDue);
                /* Code start for PUX-358 */
                if (overDue != undefined && overDue != '') {
                    /* Code start for PUX-485 */
                    if (overDue == 'Overdue') tempList.push({
                        'fieldName': 'OverDue In',
                        'displayValue': 'Not In OverDue',
                        'key': 'filter4'
                    });
                    else tempList.push({
                        'fieldName': 'Status In',
                        'displayValue': 'Awaiting Response',
                        'key': 'filter4'
                    });
                    /* Code end for PUX-485 */
                }
                /* Code End for PUX-358 */
                component.set("v.selectedFilter", tempList);
                /* end : PUX-305 PUX-421*/
                //	console.log('Records No : '+response.getReturnValue().length+'\nmaxPage : '+Math.floor((response.getReturnValue().length+9)/10));
                //PUX-207							
                component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
                //Start : PUX-504
                component.set("v.totalRecords", response.getReturnValue().total);
                //End : PUX-504
                //PUX-207
                //this.renderPage(component);
                //component.find("selectedfilterDiv").set("v.value", test);
            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " +
                        //  errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
	/* ---- added by Shital Rathod---- Jira-CASP2-11 reset button & range functionality changes END -----*/

    /*
    	Authors: Rahul Pastagiya
    	Purpose: It will be called on change page number in pagination
    */
    renderPage: function(component) {
        var records = component.get("v.caseList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * 10, pageNumber * 10);
        //	consoleProxy.log('Render Page '+pageRecords);
        component.set("v.currentList", pageRecords);
    },

	bulkPermissionCheckForUser: function(component, event) {
        var action = component.get("c.bulkPermissionCheckForUser");
		var searchConfigName = component.get('v.searchConfigName');
        action.setParams({ searchConfigName :  searchConfigName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.bulkAccess", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

		getSearchConfig: function(component, event) {
        var action = component.get("c.getSerachConfiguration");
		var searchConfigName = component.get('v.searchConfigName');
        action.setParams({ configName :  searchConfigName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.SearchConfigWrapper", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: To get sort dropdown list data
        //PUX-390 
	*/
    getSortDropdownList: function(component, event) {
        var action = component.get("c.getSortFieldList");
		var searchConfigName = component.get('v.searchConfigName');
		console.log(searchConfigName);
        action.setParams({ searchConfigName :  searchConfigName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.sortDropdownList", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //PUX-390 : End
    /*
		Authors: Tushar Moradiya
		Purpose: To get all related Queue name based on logic user cases
		//PUX-305	
	*/
    /* start : PUX-305 */
    getQueueDropdown: function(component, event) {
        var action = component.get("c.getQueueDropdown");
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.displayField", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* end : PUX-305 */
    /*
    	Authors: Niraj Prajapati
    	Purpose: To update Owner Id as current login user id
    	Dependencies: CaseDetailSection.cmp	
    */
    makeMeOwner: function(component, event, isRedirect) {
        var ct = component.get("v.selectedCases");
        //Start code change for PUX-277/PUX-275
        ct[0].OwnerId = component.get("v.loginUserId");
        
        //End code change for PUX-277/PUX-275
        var compEvent = component.getEvent("caseAction");
        var caseObj = ct[0];
        compEvent.setParams({
            /*Code Start For PUX-295 */
            "Action": "updateOwner",
            /*Code End For PUX-295 */
            "Case": caseObj,
            "callback": function(resp) {
                
                var state = resp.getState();
                if (state === "SUCCESS") {
                    var message = '';
                    if (!isRedirect) {
                        message = "Case-" + caseObj.Name + " is accepted!";
                    } else {
                        message = "Case-" + caseObj.Name + " is accepted and navigated to detail page!";
                    }
                    var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success !",
                        "message": message
                    });
                    appEvent.fire();
                    var comVar = component.get("v.refreshCounter");
                    component.set("v.refreshCounter", comVar + 1);
                    component.set("v.showHideSplitMerge", "");
                    var emptyList = [];
                    component.set("v.selectedCases", emptyList);
                } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                    //Start code change for PUX-277/PUX-275
                    //ct[0].OwnerId = owner;
                    //component.set("v.case", ct[0]);
                    var np = component.toString().split('":"')[1].split('"}')[0];
                    
                    var errorMessage = (np == 'c') ? $A.get("$Label.c.Error_Message") : $A.get("$Label.CoraPLM.Error_Message");
                    var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success !",
                        "message": errorMessage
                    });
                    
                    appEvent.fire();
                    //End code change for PUX-277/PUX-275
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            }
        });
        compEvent.fire();
    },
    /* End : PUX-304 */
    /*
		Authors: Tushar Moradiya
		Purpose: To get fieldset for filter dropdown
		//PUX-305	
	*/
    /* start : PUX-305 */
    getFieldDropdown: function(component, event) {

        var action = component.get("c.getFieldDropdown");
		var searchConfigName = component.get('v.searchConfigName');
		console.log(searchConfigName);
        action.setParams({ searchConfigName :  searchConfigName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.fields", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }, 
	
	isVendor: function(component, event) {
        var action = component.get("c.isVendor");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isVendor", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* end : PUX-305 */

	getFieldSetMember: function(component, event) {
		var action1 = component.get("c.getFieldsetMember");
		var searchConfigName = component.get('v.searchConfigName');
         action1.setParams({ searchConfigName :  searchConfigName}); 
		//console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
               
				console.log('records--'+response.getReturnValue());
				component.set("v.invObj", response.getReturnValue().record);
			    component.set("v.columns", response.getReturnValue().Columns);	
					
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
		$A.enqueueAction(action1);   
	},

	getFieldSetMemberWrap: function(component, event) {
	//alert('called');
		var action1 = component.get("c.getFieldsetMember1");
		var searchConfigName = component.get('v.searchConfigName');
         action1.setParams({ searchConfigName :  searchConfigName}); 
		//console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
		console.log('records--'+response.getReturnValue());
            if (response.getState() === 'SUCCESS') {
				
				component.set("v.wraplist", response.getReturnValue().fieldAttrList);
			   // component.set("v.columns", response.getReturnValue().Columns);	
					
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
		$A.enqueueAction(action1);   
	},

	download : function (component, event) {	
		var poFlipList =  component.get("v.selectedCases");
		var searchConfigName =  component.get("v.searchConfigName");
			if(poFlipList !=null && poFlipList !=undefined){
				var pol = '';						
				for(var k in poFlipList){ 
					console.log(k);
					pol += poFlipList[k]["Id"] + ',';
				}
			//	console.log(pol);
			}
			  window.open('/apex/SearchDownloadPageESM?key='+pol+'&searchConfigName='+searchConfigName,'_self'); 
								
    } ,

	handleNew: function(objectApi) {
		window.open("https://syscoapflow--initialdev.lightning.force.com/lightning/o/"+objectApi+"/new","_blank");	
    }
})