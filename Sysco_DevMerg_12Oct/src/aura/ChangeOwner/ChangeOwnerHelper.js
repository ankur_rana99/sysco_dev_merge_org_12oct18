({
    
    showToast:function(component,event){
        var emptyList = [];
                component.set('v.changeOwnerCase', emptyList);
                var caseList = component.get('v.changeOwnerCase');
                var compEventRedirect = component.getEvent("caseAction");
				   compEventRedirect.setParams({
					
					"Case": component.get("v.changeOwnerCase")
				 });
				 compEventRedirect.fire();
       
    },
    changeOwner : function(component,event,helper){
    				var ownerName = component.get("v.selectedName");
                    var selectCaseId = component.get("v.caseToChange");
				    if(component.get('v.selectUser') == 'User'){
			    		var action = component.get("c.userDetail");
				    }else if(component.get('v.selectUser') == 'Queue'){
				    	var action = component.get("c.queueData");	
				    }
                    
                    action.setParams({
                        "owner": ownerName,
                        "caseId": selectCaseId
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();

                        if (state == "SUCCESS") {
                            component.set("v.isDisplay", false);
                            var comVar = component.get("v.refreshCounter");
                            component.set("v.refreshCounter", comVar + 1);
                            var appEvent = $A.get("e.c:ShowToast");
                            appEvent.setParams({
                                "title": "Success ",
                                "message": 'Owner is changed successfully!'
                            });
                            appEvent.fire();
                            helper.showToast(component,event);
                        } else if (state == "ERROR") {
                            var errors = response.getError();
                            component.set("v.errorMessage", errors[0].message);
                            var eerr = component.get("v.errorMessage");
                            component.set("v.showError", true);

                        }
                    });
                	$A.enqueueAction(action); 
    	}
})