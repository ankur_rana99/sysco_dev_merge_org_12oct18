({
    doInitUser: function(component) {
        var caseIdList = [];
        var changeOwn = component.get("v.changeOwnerCase");
        var arrayLength = changeOwn.length;
        for (var i = 0; i < arrayLength; i++) {
            var caseId = changeOwn[i].Id;
            caseIdList.push(caseId);
            component.set("v.caseToChange", caseIdList);
        }

        var action = component.get("c.userDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var first = response.getReturnValue();
                console.log("first==user==", first);
                if (first != null) {
                    var firstUser = first[0].Id;
                    component.set("v.userList", response.getReturnValue());
                    //component.set("v.selectedName" , firstUser);    
                }

            }

        });
        $A.enqueueAction(action);
    },
    initQueue: function(component, event, helper) {
        var action = component.get("c.queueData");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                //    var first = response.getReturnValue();
                //console.log("first==que==",first);
                // var firstUser = first[0].Id;
                component.set("v.queueList", response.getReturnValue());
                // component.set("v.selectedName" , firstUser);

            }
        });
        $A.enqueueAction(action);
    },
    UserView: function(component, event, helper) {

        component.set('v.searchValue', '');

        var eventsource = event.getSource();
        var picklistVal = eventsource.get("v.value");
        component.set('v.selectUser', picklistVal);
        /*	if(picklistVal == 'Queue'){
			component.set("v.selectedName" , component.get("v.queueList")[0].QueueId); 
		}else{
			component.set("v.selectedName" , component.get("v.userList")[0].QueueId); 
		}
      */
    },
    handleCase: function(component, event) {

        var caseId = event.getParam("caseId");

        component.set("v.caseSelectId", caseId);
        var selectId = component.get("v.caseSelectId");

    },
    changeOwner: function(component, event, helper) {
        var selectedOption = component.get('v.selectUser');
        if (component.get('v.selectedName') && component.get('v.searchValue') && component.get('v.searchValue') == component.get('v.searchUserQueue')) {
            var chownr = component.get("v.searchValue");
                if (chownr != null) {
                		helper.changeOwner(component,event,helper);
                }
        } else {

            var appEvent = $A.get("e.c:ShowToast");
            appEvent.setParams({
                "title": "Error ",
                "message": 'Please select a ' + selectedOption + ' from the available ' + selectedOption + 's !',
                "type": "error"
            });
            appEvent.fire();
        }

    },
    hideChangeOwner: function(component, event) {
        component.set("v.isDisplay", false);
    },
    hideModal: function(component, event) {
        component.set("v.showError", false);
        component.set("v.isDisplay", false);

    },
    getSelectedValue: function(component, event, helper) {
        var selectedItem = event.currentTarget;

        //component.set("v.ref", selectedItem.dataset.record);

        component.set("v.searchValue", selectedItem.dataset.name);
        component.set("v.searchUserQueue", selectedItem.dataset.name);
        component.set("v.selectedName", selectedItem.dataset.record);
        if (component.get("v.selectUser") == 'User') {
            var toggleText = component.find("listbox");
        }

        if (component.get("v.selectUser") == 'Queue') {
            var toggleText = component.find("queueListbox");
        }
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "slds-hide");
    },
    assignUserList: function(component, event, helper) {

        if (component.get("v.selectUser") == 'User') {
            var toggleText = component.find("listbox");
            $A.util.removeClass(toggleText, "slds-hide");
            var options = component.get("v.userList");
        }
        if (component.get("v.selectUser") == 'Queue') {
            var toggleText = component.find("queueListbox");
            $A.util.removeClass(toggleText, "slds-hide");
            var options = component.get("v.queueList");
        }
        var val = component.get("v.searchValue");
        var filterValues = [];
        for (var i = 0; i < options.length; i++) {
            var contact = options[i];
            if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                filterValues.push(options[i]);
            }
        }
        component.set("v.filterdUserList", filterValues);
        if (filterValues.length == 0 || event.getParams().keyCode == 27) {
            if (component.get("v.selectUser") == 'User') {
                var toggleText = component.find("listbox");
                $A.util.addClass(toggleText, 'slds-hide');
            }
            if (component.get("v.selectUser") == 'Queue') {
                var toggleText = component.find("queueListbox");
                $A.util.addClass(toggleText, 'slds-hide');
            }
        }

    }
})