({
    doInit: function(component,event,helper){
        var caseIdList =[];
        var changeOwn = component.get("v.changeOwnerCase");
        var arrayLength = changeOwn.length;
        for( var i=0; i<arrayLength;i++){
            var caseId = changeOwn[i].Id;
            caseIdList.push(caseId);
            component.set("v.caseToChange" ,caseIdList);
        }   
		
        var action = component.get("v.action");
        if(action == 'assignment'){
            helper.getUsers(component,event);
        }else{
        	helper.getUsers(component,event);
        	helper.getQueues(component,event);
        }
		
    },
    UserView : function(component,event,helper){
        var eventsource = event.getSource();
        var picklistVal = eventsource.get("v.value");
       console.log(picklistVal);
        component.set('v.selectUser',picklistVal);
		if(picklistVal == 'Queue'){
			component.set("v.selectedName" , component.get("v.queueList")[0].QueueId); 
		}else{
			component.set("v.selectedName" , component.get("v.userList")[0].QueueId); 
		}
        
        
    },
    handleCase : function(component, event) {
       
        var caseId = event.getParam("caseId");
        
        component.set("v.caseSelectId",caseId);
        var selectId = component.get("v.caseSelectId");
       
    },
    changeOwner:function(component,event,helper){        
        helper.changeUserAndQueue(component,event);
    },
    hideChangeOwner:function(component,event){
        component.set("v.isDisplay",false);
		
    //    var comVar = component.get("v.refreshCounter"); 
     //   component.set("v.refreshCounter", comVar + 1); 
    },
    hideModal: function(component,event){
      //  component.set("v.showError" ,false);
        component.set("v.isDisplay",false);
     //   var comVar = component.get("v.refreshCounter"); 
   //     component.set("v.refreshCounter", comVar + 1); 
    }
})