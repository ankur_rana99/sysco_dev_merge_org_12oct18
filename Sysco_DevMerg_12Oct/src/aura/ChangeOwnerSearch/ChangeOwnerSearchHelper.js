({
    getUsers: function(component,event){
             
        var action = component.get("c.getUsers");
		action.setParams({           
			"searchConfigName":component.get("v.searchConfigName"),
			"moduelName":component.get("v.action")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state== "SUCCESS"){
                var first = response.getReturnValue();
                var firstUser = first[0].Id;
                component.set("v.userList",response.getReturnValue());
                component.set("v.selectedName" , firstUser);               
            }
            
        });
        $A.enqueueAction(action); 	
    },
    
    getQueues : function(component,event){
        var action = component.get("c.getQueues");
		action.setParams({           
			"searchConfigName":component.get("v.searchConfigName")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                 var first = response.getReturnValue();
                 var firstUser = first[0].Id;
                 component.set("v.queueList" ,response.getReturnValue());
               
            }
        });
        $A.enqueueAction(action);
    },
    
     changeUserAndQueue:function(component,event){        
        var ownerName = component.get("v.selectedName");
        var selectCaseId = component.get("v.caseToChange");
        var action = component.get("c.changeUserAndQueue");
        action.setParams({
            "owner" : ownerName,
            "caseId" :selectCaseId,
			"searchConfigName":component.get("v.searchConfigName")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            
            if(state=="SUCCESS"){
			var changeOwn = [];
		component.set("v.changeOwnerCase",changeOwn);
                component.set("v.isDisplay", false);
                var comVar = component.get("v.refreshCounter"); 
                component.set("v.refreshCounter", comVar + 1); 
                 var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success ",
                        "message": 'Owner change successfully'
                    });
                    appEvent.fire();
            } else if(state=="ERROR"){
               
                var errors = response.getError();
				
                component.set("v.errorMessage", errors[0].message );
                var eerr = component.get("v.errorMessage");
                component.set("v.showError",true);
                
            }
        });       
         $A.enqueueAction(action); 
    },

})