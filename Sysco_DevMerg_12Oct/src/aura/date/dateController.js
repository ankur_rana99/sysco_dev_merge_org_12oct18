({
	init : function(component, event, helper) {
		component.set("v.vals",component.getReference("v.obj."+component.get("v.fieldMetadata.name")));
		if(window.globalvalues && window.globalvalues.pagemode == 'VIEW')
		{
			component.set("v.readonly",true);
			component.set("v.required", false);
		}
		if(component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "")
        {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
            {
                component.set("v.render",_criteriahelper.validate(criJson,component.get("v.criteriaObj"),component.get("v.objFieldMetadata")));
				if(component.get("v.render") == true)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-show";
					component.set("v.fieldCriteria", field);
				}
				if(component.get("v.render") == false)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-hide";
					component.set("v.fieldCriteria", field);
				}
            }
        }
		if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
	},

	handleRefresh : function(component, event, helper) {
		if(component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "")
        {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
            {
                component.set("v.render",_criteriahelper.validate(criJson,component.get("v.criteriaObj"),component.get("v.objFieldMetadata")));
				if(component.get("v.render") == true)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-show";
					component.set("v.fieldCriteria", field);
				}
				if(component.get("v.render") == false)
				{
					var field = component.get("v.fieldCriteria");
					field.className = "slds-hide";
					component.set("v.fieldCriteria", field);
				}
            }
        }
		if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
	},
	validate : function(component, event) {
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
		if(component.get("v.render"))
		{
			var allValid = component.find("formValidate").get("v.validity");
			component.find("formValidate").showHelpMessageIfInvalid();
			if (callback) callback(allValid.valid);
		}else{
			if (callback) callback(true);
		}
	},

})