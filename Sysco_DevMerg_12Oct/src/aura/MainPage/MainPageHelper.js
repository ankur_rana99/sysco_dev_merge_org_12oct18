({
    getLoginUser: function(component, event) {
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.UserName", response.getReturnValue().UserName);
                window.InstanceName = response.getReturnValue().InstanceName;
                component.set("v.Instance", InstanceName);
                component.set("v.renderComponents", true);
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showToast("Validation Alert!","error","sticky",errors[0].message);
                    } 
                } else {
                    this.showToast("Validation Alert!","error","sticky","Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getSobjectDetails: function(component, event) {
        var action = component.get("c.getSObjectDetails");
        action.setParams({
            layout: component.get("v.pagemode"),
            recId: component.get("v.recordId"),
            sObjectName: component.get("v.sObjectName")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.customLabel",response.getReturnValue().customLabels);
                if (response.getReturnValue().isAccesible == true) {
                    var layoutjson = JSON.parse(response.getReturnValue().layoutJson);
                    if (layoutjson["layoutJson"][0] == undefined) {
                        //Show toast - Blocker
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Validation Alert!",
                            "type": "error",
                            "mode": "sticky",
                            "message": "Layout configuration not found."
                        });
                        toastEvent.fire();
                    } else {
                        if (component.get("v.pagemode") != 'NEW') {
                            component.set("v.sObject", response.getReturnValue().currentObject);
                            component.set("v.currentObject", response.getReturnValue().currentObject);
                        }
                        var jsonCon = JSON.parse(response.getReturnValue().layoutJson);
                        //component.set("v.sObjectName", jsonCon["sObjectName"]);
                        var jsonData = JSON.parse(jsonCon["layoutJson"][0]["Config_Json__c"]);
                        
                        component.set("v.layoutJson", jsonData);
                        component.set("v.objMetadata", jsonCon["sObjectMetadata"]);
                        component.set("v.fieldMetadata", jsonCon["sObjectFieldMetadata"]);
                        
                        component.set("v.layoutJsonAvailable", true);
                        var intervalId;
                        var checkTheAction = function() {
                            if (component.get("v.layoutJsonAvailable") == true && component.get("v.JSAvailable") == true) {
                                window.clearInterval(intervalId);
                                //Check rendering criteria
                                jsonData.PageLayout.POReference.render = true;
                                if (jsonData.PageLayout.POReference.POReferenceRequired == true && jsonData.PageLayout.POReference.criteria != null && jsonData.PageLayout.POReference.criteria != undefined && jsonData.PageLayout.POReference.criteria != "") {
                                    var criJson = jsonData.PageLayout.POReference.criteria;
                                    if (criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                                        jsonData.PageLayout.POReference.render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
                                    }
                                }
                                if (jsonData.PageLayout.lineItemComponents != undefined && jsonData.PageLayout.lineItemComponents.length > 0) {
                                    for (var i = 0; i < jsonData.PageLayout.lineItemComponents.length; i++) {
                                        jsonData.PageLayout.lineItemComponents[i].render = true;
                                        var criJson = jsonData.PageLayout.lineItemComponents[i].criteria;
                                        if (criJson != undefined && criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                                            jsonData.PageLayout.lineItemComponents[i].render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
                                        }
                                    }
                                }
                                if (jsonData.PageLayout.headerLevelComponents != undefined && jsonData.PageLayout.headerLevelComponents.length > 0) {
                                    for (var i = 0; i < jsonData.PageLayout.headerLevelComponents.length; i++) {
                                        jsonData.PageLayout.headerLevelComponents[i].render = true;
                                        var criJson = jsonData.PageLayout.headerLevelComponents[i].criteria;
                                        if (criJson != undefined && criJson.group != undefined && criJson.group.rules != undefined && criJson.group.rules.length > 0) {
                                            jsonData.PageLayout.headerLevelComponents[i].render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
                                        }
                                    }
                                }
                                console.log('jsonData==='+jsonData);
                                component.set("v.layoutJson", jsonData);
                            }
                        };
                        intervalId = window.setInterval(checkTheAction, 2000);
                    }
                }else{
                    component.set("v.recordError",$A.get('$Label.c.Insufficient_privilege_to_Edit_Record'));
                    //component.set("v.recordError","Insufficient previlage to Edit Record");
                }
            }
            else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showToast("Validation Alert!","error","sticky",errors[0].message);
                    }
                } else {
                    this.showToast("Validation Alert!","error","sticky","Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    submitRecordData: function(component, event) {
        
        var attachList = component.get("v.attachList");;
        var primaryDocJson = component.get("v.primaryDocJson");
        
        //--- check user action is "Validate" or not ---//
        var childCmpInvLine = component.find("invlineDet");
        var childInvDetail = component.find("invDetailComp");
        var childOtherDetail = component.find("addlineDet");
        var invLineValid = false;
        var invDetailValid = false;
        var isOtherValid = false;
        var isValidUA = true;
        var invLineAdded = false;
        isValidUA = this.validateUserActionHelper(component, event);
        console.log('isValidUA==='+isValidUA);
        if(childCmpInvLine!=null && childCmpInvLine!=undefined && component.get("v.invLineList")!=null && childCmpInvLine.validate!=null)
        {
            console.log('In IF==='+childCmpInvLine);
            if(component.get("v.invLineList").length > 0 || (component.get("v.sObject.User_Action__c") == 'Save'
                                                             || component.get("v.sObject.User_Action__c") == 'Reject' || component.get("v.sObject.User_Action__c") == 'Document Reclassify'))
            {
                invLineAdded =true;
            }
            childCmpInvLine.validate(function(t) {
                if(t){
                    console.log('t===');
                    invLineValid = t;
                }
            });
        }else{
            invLineValid = true;
            invLineAdded = true;
        }
        if(childInvDetail!=null && childInvDetail!=undefined)
        {
            childInvDetail.validate(function(t) {
                if(t){
                    invDetailValid = t;
                }
            });
        }else{
            invDetailValid = true;
        }
        if(childOtherDetail!=null && childOtherDetail!=undefined && component.get("v.additionalChargesList")!=null && childOtherDetail.validate!=null)
        {
            childOtherDetail.validate(function(t) {
                if(t){
                    isOtherValid = t;
                }
            });
        }else{
            isOtherValid = true;
        }
        if(invLineValid && invDetailValid && isOtherValid && isValidUA && invLineAdded){
            this.callSaveOrValidate(component, event);
        }
        else if(!invLineAdded){
            this.showToast("Validation Alert!", "error", "sticky",$A.get('$Label.c.Invoice_Line_Not_Found'));
        }
            else if(!isValidUA){
                //this.showToast("Validation Alert!", "error", "sticky", component.get("v.customLabel")['Invalid_User_Action']);
            }else{
                this.showToast("Validation Alert!", "error", "sticky", component.get("v.customLabel")['Fill_All_Required_Detail']);
            }
    },
    callSaveOrValidate: function(component, event) {
        var target = event.getSource().get("v.alternativeText");
        if (component.get("v.isDirty") && target!='Validate' && component.get("v.pagemode") !== 'NEW' && component.get("v.sObject.User_Action__c") != 'Save' 
            && component.get("v.sObject.User_Action__c") != 'Reject' && component.get("v.sObject.User_Action__c") != 'Document Reclassify')
        {
            this.showToast("Validation Alert!", "error", "sticky",component.get("v.customLabel")['Validate_First']);
        }else if (component.get("v.pagemode") !== 'NEW' && target!='Validate' && component.get("v.sObject.User_Action__c") == 'Send For Posting' && 
                  component.get("v.sObject.Exception_Reason__c") != null && component.get("v.sObject.Exception_Reason__c") != undefined 
                  && component.get("v.sObject.Exception_Reason__c") != ''	&& component.get("v.sObject.User_Action__c") != 'Save'
                  && component.get("v.sObject.User_Action__c") != 'Reject' && component.get("v.sObject.User_Action__c") != 'Document Reclassify')
        {
            this.showToast("Validation Alert!", "error", "sticky", component.get("v.customLabel")['Resolve_Error_Prior_Posting']);
        }
            else {
                /*if (component.get("v.sObject.User_Action__c") == 'Send to SBS Cypress Team' || component.get("v.sObject.User_Action__c") == 'Send to Sysco Field - Buyer' ||
	        component.get("v.sObject.User_Action__c") == 'Send to Sysco Field - Requestors' || component.get("v.sObject.User_Action__c") == 'Send to Sysco Field - Inventory Control' ||
	        component.get("v.sObject.User_Action__c") == 'Send to Sysco VRFS' || component.get("v.sObject.User_Action__c") == 'Send to Requestor') {
	        if (component.get("v.isValidateClicked")) {
	            this.saveRecordData(component, event);
	        } else {
	            this.showToast("Validation Alert!", "error", "sticky", "Please validate Invoice.");
	        }
	    } else */
         
         if (component.get("v.sObject.User_Action__c") == "Validate") {
             this.validateRecordData(component, event);
         } else if (component.get("v.sObject.User_Action__c") == "QC Reject") {
             var qcComp = component.find("qualityCheck");
             var qcObject = [];
             if (qcComp!=undefined){
                 qcComp.getQcObject(function(resObj) {
                     qcObject = resObj;
                 });
             }else{
                 this.showToast("Validation Alert!", "error", "sticky", "Configuration Issue: Please configure Qc Component");
             }
             if (qcComp!=undefined && qcObject != null && qcObject != undefined) {
                 var allEmpty = true;
                 for (var k in qcObject) {
                     if (qcObject[k] != null && qcObject[k] != undefined && qcObject[k] != '') {
                         allEmpty = false;
                         break;
                     }
                 }
                 if (allEmpty) {
                     this.renderComponentHelper(component, event, 'QualityCheck');
                     this.showToast("Validation Alert!", "error", "sticky", "Please enter atleast one qc value");
                 } else {
                     if(target!='Validate'){
                         if(component.get("v.sObject.Exception_Reason__c") != null && component.get("v.sObject.Exception_Reason__c") != undefined ){
                             document.getElementById("divToastMsg").innerHTML = "Following Exception has been occured on Invoice. " +component.get("v.sObject.Exception_Reason__c")  ;
                             document.getElementById("showToast").style.display = "block";
                         }else{
                             this.saveRecordData(component, event);
                         }
                     }else{
                         this.validateRecordData(component, event);
                     }
                 }
             } else {
                 this.showToast("Validation Alert!", "error", "sticky", "Please enter atleast one qc value");
             }
         } else {
             if(target!='Validate'){
                 if(component.get("v.sObject.Exception_Reason__c") != null && component.get("v.sObject.Exception_Reason__c") != undefined ){
                     document.getElementById("divToastMsg").innerHTML = "Following Exception has been occured on Invoice. " +component.get("v.sObject.Exception_Reason__c")  ;
                     document.getElementById("showToast").style.display = "block";
                 }else{
                     this.saveRecordData(component, event);
                 }
             }else{
                 this.validateRecordData(component, event);
             }
         }
     }
    },
    validateRecordData: function(component, event) {
        component.set("v.sObject.Exception_Reason__c",null);
        var action = component.get("c.validateInvoiceData");
        action.setParams({
            invoiceObject: component.get("v.sObject"),
            invLineItemList: component.get("v.invLineList"),
            otherChargesList: component.get("v.additionalChargesList"),
            poList: component.get("v.POList"),
            childNameSpace: component.get("v.childNameSpace")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var map = response.getReturnValue();
                if(map == null)
                {
                    this.showToast("Validation Alert!","Server side error occurred. Please contact Administrator");
                }
                if(map != null && map != "")
                {
                    if(map.currentObject != null && map.currentObject != "")
                    {
                        component.set("v.sObject",map.currentObject);
                        if(component.get("v.sObject.Exception_Reason__c") == null || component.get("v.sObject.Exception_Reason__c") == undefined){
                            component.set("v.sObject.Exception_Reason__c",null);
                        }
                    }
                    if(map.invLines != null && map.invLines != "")
                    {
                        var invNew = new Object;
                        var invLinesNew = new Array();
                        for(var k in map.invLines)
                        {
                            invNew = map.invLines[k];
                            invNew["sobjectType"] = 'Invoice_Line_Item__c';
                            invLinesNew[k] = invNew;
                        }
                        component.set("v.invLineList",invLinesNew);
                    }
                    if(map.validationMap != null && map.validationMap != "")
                    {
                        if((map.validationMap.tempExceptionReason == null || map.validationMap.tempExceptionReason == "" || map.validationMap.tempExceptionReason == "{}") 
                           && map.validationMap.jsonValidationExceptionMsg != null && map.validationMap.jsonValidationExceptionMsg != "" && map.validationMap.jsonValidationExceptionMsg != "{}" 
                           && JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception !=null)
                        {
                            //Show toast - Blocker
                            this.showToast("Validation Alert!","error","sticky","Following Exception has been occured on Invoice. "+JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception);
                            component.set("v.isDirty",false);
                        }
                        else if(map.validationMap.tempExceptionReason != null && map.validationMap.tempExceptionReason != "" && map.validationMap.tempExceptionReason != "{}")
                        {
                            //Show toast
                            this.showToast("Validation Alert!","error","sticky",JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception);
                            component.set("v.isDirty",false);
                            //document.getElementById("divToastMsg").innerHTML = JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception;
                            //document.getElementById("showToast").style.display = "block";
                        }
                            else
                            {
                                //this.saveRecordData(component, event);
                                this.showToast("Success!",null,null,component.get("v.customLabel")['Validated_Successfully']);
                                component.set("v.isDirty",false);
                            }
                    }
                    else if(map.currentObject != null && map.currentObject != "")
                    {
                        component.set("v.isDirty",false);
                        this.showToast("Success!",null,null,component.get("v.customLabel")['Validated_Successfully']);
                        //this.saveRecordData(component, event);
                    }
                }
                else{
                    this.showToast("Validation Alert!","error","sticky","no response");
                    component.set("v.isDirty",false);
                }
            }
            else if (response.getState() === 'ERROR') 
            {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showToast("Validation Alert!","error","sticky",errors[0].message);
                        component.set("v.isDirty",false);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveRecordData: function(component, event) {
        var invoiceObjName = component.get("v.childNameSpace") + "Invoice__c";
        var sObjectName = component.get("v.sObjectName");
        
        if (invoiceObjName == sObjectName) {
            var qcObject = [];
            if (component.get("v.sObject.Current_State__c") == "Ready For QC" ) 
            {
                var qcComp = component.find("qualityCheck");
                qcComp.getQcObject(function (resObj){
                    qcObject =resObj;
                });
            }else{
                qcObject =null;
            }
            var invLineDetail = component.get("v.invLineList");
            var PODetail = component.get("v.POList");
            var POLineDetail = component.get("v.POLineListNew");
            //component.set("v.invObj",component.get("v.sObject"));
            console.log('LINE===='+JSON.stringify(component.get("v.invLineList")));
            var action = component.get("c.SaveInvoiceData");
            if(component.get("v.pagemode") =='EDIT'){
                component.set("v.sObject.Is_Dirty__c",component.get("v.isDirty"));
            }
            var invObject = component.get("v.sObject");
            component.set("v.invObj",component.get("v.sObject"));
            // set the sobjectType!
            //component.set("v.additionalChargesList",component.get("v.additionalChargessend"));
            // var addChargers=component.get('v.additionalChargesList');
            //addChargers.sobjectType='Invoice_Line_Item__c';
        
            action.setParams({
                invoiceObject: component.get("v.invObj"),
                invLineItemList: component.get("v.invLineList"),
                poList: component.get("v.POList"),
                otherChargesList: component.get("v.additionalChargesList"),
                pageMode: component.get("v.pagemode"),
                primaryDocJsonAtt : component.get("v.primaryDocJson"),
                NewAttachmentsAtt  : component.get("v.attachList"),
                qcObject : qcObject
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //cmp.set("v.objectList", response.getReturnValue());
                    //alert("Message:" + response.getReturnValue());
                    //---Jira # ESMPROD-1261 --| start--- 
                         this.saveAlert(component, event,response.getReturnValue());
                     //---Jira # ESMPROD-1261 --| End--- 
                    /*---Jira # ESMPROD-1261 --| start--- Commented for loop 
                    for(var kSucess in response.getReturnValue()){
                        if(kSucess == 'true' ){
                            this.showToast("Success!",null,null,"Record has been saved.");
                            component.set("v.isDisplay",false);
                            //component.set("v.pagemode",'EDIT');
                            window.globalvalues.pagemode =component.get("v.pagemode");
                            var navEvt = $A.get("e.force:navigateToSObject");
								navEvt.setParams({
								"recordId": retId,
								"slideDevName": "related"
							});
							navEvt.fire();
                        }else{
                            this.showToast("Error!","error","sticky",response.getReturnValue()[kSucess]);
                        }
                    }
                    ---Jira # ESMPROD-1261 --| End--- Commented for loop--*/ 
                }
                else if (response.getState() === 'ERROR') {
                    var errors = response.getError(response.getReturnValue());
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            this.showToast("Validation Alert!","error","sticky",errors[0].message);
                        }
                    } else {
                        this.showToast("Validation Alert!","error","sticky","Unknown error");
                    }
                } else {
                    this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin");
                }
            });
            $A.enqueueAction(action);
            
        }
    },
    
    AddInvoiceLineHelper: function(component, event) {
        var poLine;
        var poLineList;
        var childCmpPOLine = component.find("PolineItemDet");
        var validForm = true;
        childCmpPOLine.addPOLinesMain(function(result) {
            poLine = result["polines"];
            poLineList = result["polinesobj"];
        });
        for (var key in poLine) {
            for (var key1 in poLineList) {
                if (poLineList[key1]["Id"] == poLine[key]) {
                    if (poLineList[key1]["Quanity_To_Invoice__c"] === null || poLineList[key1]["Quanity_To_Invoice__c"] === undefined || Number(poLineList[key1]["Quanity_To_Invoice__c"]) === 0) {
                        validForm = false;
                    }
                    if (poLineList[key1]["Rate__c"] === null || poLineList[key1]["Rate__c"] === undefined || Number(poLineList[key1]["Rate__c"]) === 0) {
                        validForm = false;
                    }
                }
            }
        }
        if (validForm) {
            var childCmpInvLine = component.find("invlineDet");
            var grnList = component.get("v.grnLineList");
            // call the aura:method in the child component
            childCmpInvLine.setInvoiceLineFromPO(poLine, poLineList, grnList);
            component.set("v.isOpen", false);
            document.getElementById("modalPopup").style.display = "none";
            component.set("v.isDirty",true);
            this.calulateTotalAmount(component, event);
            //component.set("v.sObject",component.get("v.sObject"));
        } else {
            this.showToast("Validation Alert!","error","sticky",$A.get('$Label.c.Improper_PO_Line'));
        }
        
    },
    calulateTotalAmount: function(component, event) {
        component.set("v.sObject", component.get("v.sObject"));
        if(component.get("v.pagemode") === 'NEW'){
            var totalamt = 0;
            if(component.get("v.sObject.Amount__c")!=null && component.get("v.sObject.Amount__c")!= undefined)
            {
                totalamt = totalamt + Number(component.get("v.sObject.Amount__c"));
            }
            if(component.get("v.sObject.Other_Charges_Surcharges__c")!=null && component.get("v.sObject.Other_Charges_Surcharges__c")!= undefined)
            {
                totalamt = totalamt +  Number(component.get("v.sObject.Other_Charges_Surcharges__c"));
            }
            if(component.get("v.sObject.Tax__c")!=null && component.get("v.sObject.Tax__c")!= undefined)
            {
                totalamt = totalamt +  Number(component.get("v.sObject.Tax__c"));
            }
            component.set("v.sObject.Total_Amount__c",totalamt);
        }
    },
    showToast: function(title, type,mode,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "mode": mode,
            "message": message
        });
        toastEvent.fire();
    },
    renderComponentHelper: function(component, event,tab) {
        if(document.getElementById("NotesAttachmentsDiv"))
        {
            document.getElementById("NotesAttachmentsDiv").style.display = "none";
        }
        if(document.getElementById("InvoiceHistoryDiv"))
        {
            document.getElementById("InvoiceHistoryDiv").style.display = "none";
        }
        if(document.getElementById("InteractionDiv"))
        {
            document.getElementById("InteractionDiv").style.display = "none";
        }
        if(document.getElementById("QualityCheckDiv"))
        {
            document.getElementById("QualityCheckDiv").style.display = "none";
        }
        if(document.getElementById("routeDiv")){
            document.getElementById("routeDiv").style.display = "none";
        }
        var layOutJson = component.get("v.layoutJson").PageLayout.headerLevelComponents.length;
        for(var i=0;i<=layOutJson;i++){
            var dynamicDiv = "RelatedListDetails"+i+"Div";
            if(document.getElementById(dynamicDiv))
            {
                document.getElementById(dynamicDiv).style.display = "none";
            }
        }
        if(document.getElementById(tab+'Div'))
        {
            document.getElementById(tab+'Div').style.display = "block";
        }
    },
    validateUserActionHelper: function(component, event) {
        //-----temporary solution for record type feature ----------------- Start  --------------------//
       
        if(component.get("v.sObject.User_Action__c") !== undefined && component.get("v.sObject.User_Action__c") !=='Reject' && component.get("v.sObject.Exception_Reason__c") === 'Duplicate Invoice' && component.get("v.sObject.Is_Dirty__c") === false){
            this.showToast("Validation Alert!","error","sticky","Only Reject is Allowed as this is a Duplicate Invoice");
            return false;
        }
        
        // Manual Credit Memo as document type is not allowed
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") === 'Start' && component.get("v.sObject.WorkType__c") === 'Utilities' )
        {
            this.showToast("Validation Alert!","error","sticky","Utilities as Document Type cannot be created manually");
            return false;
        }
        
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Pending SBS Cypress Team' )
        {
            if(	component.get("v.sObject.WorkType__c") !=='Normal Invoices' && component.get("v.sObject.User_Action__c") !== undefined && 
               component.get("v.sObject.User_Action__c") ==='Submit for Coding'){
                this.showToast("Validation Alert!","error","sticky","Only Non PO Normal Invoices can be submitted for Coding & Approval");
                return false;
            }
            if(component.get("v.sObject.Document_Type__c") !=='PO Invoice' && component.get("v.sObject.User_Action__c") !== undefined && 
               (component.get("v.sObject.User_Action__c") ==='Resolve' || component.get("v.sObject.User_Action__c") ==='Approve')){
                this.showToast("Validation Alert!","error","sticky","You can Approve only PO Invoices, please use other available actions.");
                return false;
            }
        }
        
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual NP Processing' )
        {
            // Non PO Normal Invoices Rule for Ready For Posting //  
            if(	component.get("v.sObject.WorkType__c") ==='Normal Invoices' && component.get("v.sObject.Approval_Status__c") !== 'Approved' && component.get("v.sObject.User_Action__c") !== undefined && 
               (component.get("v.sObject.User_Action__c") === 'Send For Posting' || component.get("v.sObject.User_Action__c") === 'Approve')){
                if(component.get("v.sObject.Under_Billing__c") === true && confirm('This is an Under Billing Invoice and Not Approved. Do you wish to Continue?')){
                    return true;
                }
                else
                {
                    this.showToast("Validation Alert!","error","sticky","Invoice is not Approved. Please take required approvals before posting.");
                    return false;
                }		
            }
            // Credit Memo - Agreements Rule for Vendor AR Team //
            if(	component.get("v.sObject.WorkType__c") !=='Agreements' && component.get("v.sObject.User_Action__c") !== undefined && 
               component.get("v.sObject.User_Action__c") ==='Send to Vendor AR'){
                this.showToast("Validation Alert!","error","sticky","Credit Memo & Agreements can be sent to Vendor AR Team for posting.");
                return false;
            }
            // Non PO Normal Invoices Rule for Submit for Coding and Approval //
            if(	component.get("v.sObject.WorkType__c") !=='Normal Invoices' && component.get("v.sObject.User_Action__c") !== undefined && 
               component.get("v.sObject.User_Action__c") ==='Submit for Coding'){
                this.showToast("Validation Alert!","error","sticky","Only Non PO, Normal Invoices can be submitted for Coding and Approval");
                return false;
            }
            // Send to Buyer Rule for Repayments //
            if(component.get("v.sObject.WorkType__c") !=='Repayments' && component.get("v.sObject.WorkType__c") !=='Repayments' && component.get("v.sObject.User_Action__c") !== undefined && 
               component.get("v.sObject.User_Action__c") ==='Send to Buyer'){
                this.showToast("Validation Alert!","error","sticky","Only Repayments can be sent to buyer");
                return false;
            }
            // Send to Inventory Control Rule for Repayments //
            // added a change for workday to exclude - by Vaibhav - 07/08/18
            if(component.get("v.sObject.ERP_Type__c") != 'Workday' && component.get("v.sObject.WorkType__c") !=='Repayments' && component.get("v.sObject.User_Action__c") !== undefined && 
               component.get("v.sObject.User_Action__c") ==='Send to Inventory Control'){
                this.showToast("Validation Alert!","error","sticky","Only Repayments can be sent to Inventory Control");
                return false;
            }
        }
        // Only Send to Buyer is allowed for Drop Shipments as a First User Action
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual Processing' && component.get("v.sObject.WorkType__c") === 'Drop Shipments')
        {
            if(component.get("v.sObject.DRP_Flag__c") === false && component.get("v.sObject.User_Action__c") != 'Send to Buyer'){
                this.showToast("Validation Alert!","error","sticky","Please take Send to Buyer user action first for Dropship.");
                return false;
            }
        }
        
        //---- PC Short Code is Applicable only for SMS Input Source ----//
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual Processing')
        {
            for (var key in component.get("v.invLineList")) {
                if (component.get("v.invLineList")[key]["Price_Short_Code__c"] === 'PC')
                {
                    if(component.get("v.sObject.Input_Source__c") != 'SMS'){
                        this.showToast("Validation Alert!","error","sticky","PC Short Code is Applicable only for SMS Input Source");
                        return false;
                    }
                }
            }
        }
        
        ///////////////////////FOr ERP Workday///////Begins//////////
      //--- Doc type != Indirect, this doc type is Invalid for PO Invoice ---//
        if(component.get("v.sObject.Current_State__c") !== undefined)
        {
            if(component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.Document_Type__c") == 'PO Invoice' && component.get("v.sObject.WorkType__c") != 'Indirect PO' ){
                this.showToast("Validation Alert!","error","sticky","This document type is invalid for PO Invoices");
                return false;
            }
        }
        if(component.get("v.sObject.Current_State__c") !== undefined)
        {
            if(component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.Document_Type__c") == 'Credit Memo' && component.get("v.sObject.WorkType__c") != 'Credit Memo' ){
                this.showToast("Validation Alert!","error","sticky","This document type is invalid for Credit Memo Invoices");
                return false;
            }
        }
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual Processing')
        {
            if(component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.User_Action__c") != null && component.get("v.sObject.User_Action__c") != '' &&  component.get("v.sObject.User_Action__c") != null && component.get("v.sObject.User_Action__c") != '' &&  component.get("v.sObject.User_Action__c") != 'Document Reclassify' && component.get("v.sObject.User_Action__c") != 'Reject' && component.get("v.sObject.User_Action__c") != 'Save' && component.get("v.sObject.User_Action__c") != 'Send to SBS Cypress Team' && component.get("v.sObject.User_Action__c") != 'Send For Posting' && component.get("v.sObject.User_Action__c") != 'Send to Buyer'){
                this.showToast("Validation Alert!","error","sticky","Workday invoices can not be sent to Send To Inventory Control");
                return false;
            }
        }
        
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual NP Processing')
        {
            if(component.get("v.sObject.ERP_Type__c") === 'Workday' && component.get("v.sObject.User_Action__c") != null && component.get("v.sObject.User_Action__c") != '' &&  (component.get("v.sObject.User_Action__c") === 'Send To Vendor AR' || component.get("v.sObject.User_Action__c") === 'Send to Inventory Control')){
                this.showToast("Validation Alert!","error","sticky","Workday invoices can not be sent to Send To Vendor AR or Inventory Control");
                return false;
            }
        }
        
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Pending SBS Cypress Team')
        {
            if(component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.User_Action__c") != null && component.get("v.sObject.User_Action__c") != '' &&  component.get("v.sObject.User_Action__c") == 'Approve'){
                this.showToast("Validation Alert!","error","sticky","Invalid action for workday invoices in this current state");
                return false;
            }
        }
        
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Pending SBS Supervisor')
        {
            if(component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.User_Action__c") != null && component.get("v.sObject.User_Action__c") != '' &&  component.get("v.sObject.User_Action__c") == 'Approve'){
                this.showToast("Validation Alert!","error","sticky","Invalid action for workday invoices in this current state");
                return false;
            }
        }
        /*if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.Document_Type__c") == 'Non PO Invoice'){
        	for (var key in component.get("v.invLineList")) {
                if(component.get("v.invLineList")[0]["Cost_Code__r"] !== undefined && component.get("v.invLineList")[0]["Cost_Code__r"] != null && component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] !== undefined && component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] != null && component.get("v.invLineList")[0]["Project__c"] === undefined &&  component.get("v.invLineList")[0]["Project__c"] == null && (component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Data Processing - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Finance,Vendor Management & Procurement - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Enterprise Project Management Office - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Master Data Management Project Cost - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Project Enterprise Sales Marketing - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Enterprise Information Management - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Emerging Technologies and Mobile - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Enterprise Technology Services - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Infra HLP Security QA CAFINA - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Quality Analysis and Testing - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Security and Administration - Corporate - Sysco' ||
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Training and Development - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Application Development - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Enterprise Architecture - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Integration Deployment - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Infrastructure - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Communication - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Service Desk - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Marketing - Corporate - Sysco' || 
        				component.get("v.invLineList")[0]["Cost_Code__r"]["Name"] === 'BT - Sales - Corporate - Sysco') ){
                	this.showToast("Validation Alert!","error","sticky","Please provide Project for the specified cost code");
                    return false;
                }
            }
        }*/
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.ERP_Type__c") == 'Workday' && component.get("v.sObject.Document_Type__c") == 'PO Invoice')
        {
            console.log('In next If testing....' + component.get("v.sObject.Current_State__c"));
            if(component.get("v.sObject.Purchase_Order_Number__c") == null || component.get("v.sObject.Purchase_Order_Number__c") == '' || component.get("v.sObject.Purchase_Order_Number__c") == '21002400'){
                this.showToast("Validation Alert!","error","sticky","Please link a related Purchase Order on this Invoice.");
                return false;
            }
            console.log('ENDSS....');
        }
        ///////////////////////FOr ERP Workday///////Ends//////////
        //--- CAS-594 ---//
        if(component.get("v.sObject.Current_State__c") !== undefined && component.get("v.sObject.Current_State__c") ==='Ready For Manual Processing')
        {
            for (var key in component.get("v.invLineList")) {
                if (component.get("v.invLineList")[key]["Price_Short_Code__c"] === 'PR')
                {
                    if(component.get("v.sObject.User_Action__c") != 'Send to Buyer'){
                        this.showToast("Validation Alert!","error","sticky","Only Send to Buyer	allowed.");
                        return false;
                    }
                }
            }
        }
        return true;
        //-----temporary solution for record type feature --------------  END ------------------------//
    },
    saveAlert: function(component, event,tab) {
        //---Jira # ESMPROD-1261 --| start--- 
		if(tab !=null && tab !=undefined && tab['success']!=null){
			this.showToast("Validation Alert!",null,null,tab['success']);
			component.set("v.isDisplay",false);
			window.globalvalues.pagemode =component.get("v.pagemode");
		}
		else if(tab !=null && tab !=undefined && tab['displayError']!=null){
			this.showToast("Validation Alert!","error","sticky",tab['displayError']);
		}
		else if(tab !=null && tab !=undefined && tab['failure']!=null){
			this.showToast("Validation Alert!","error","sticky",tab['failure']);
		}
		else{
			this.showToast("Success!",null,null,"Error in saving data.");
		}
        //---Jira # ESMPROD-1261 --| End--- 
	},
})