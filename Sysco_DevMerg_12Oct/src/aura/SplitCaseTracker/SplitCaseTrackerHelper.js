({
    noOfSplitsHelper: function(component, event) {
        var namespace = component.get("v.namespace");
        try {
            var parentCase = component.get('v.ct');
            var noOfSplit = component.get('v.noOfSplits').toString();
            //component.set("v.showSpinner",true);
            var splitCaseAction = component.get("c.CloneCase");
            splitCaseAction.setParams({
                parentCase: parentCase,
                noOfSplit: noOfSplit
            });
            splitCaseAction.setCallback(this, function(response) {
                var state = response.getState();
                //component.set("v.showSpinner",false);
                if (state === "SUCCESS") {
                    var returnValue = response.getReturnValue();

                    $.each(returnValue, function(index, splitCase) {
                        var layoutMeta = component.get("v.layoutMeta");
                        var newObject = jQuery.extend(true, {}, layoutMeta);
                        var newObjArray = [];
                        newObjArray.push(newObject);

                        var deepCopyObj = JSON.parse(JSON.stringify(layoutMeta));
                        splitCase.layoutMeta = deepCopyObj;
                        //component.set(component.get('v.ct'));
                        //splitCase.layoutMeta=component.get("v.layoutMeta");
                    });

                    component.set("v.SplitCaseList", returnValue);
                    //component.set("v.SplitCaseList",component.get("v.SplitCaseList"));
                } else {
                    var errorMessage = '';
                    if (state === "INCOMPLETE") {
                        errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
                    } else if (state === "ERROR") {
                        errorMessage = action.getError()[0].message;
                    }
                    this.showToast(component, namespace, 'Split Case:', errorMessage, 'error');
                }

            });
            $A.enqueueAction(splitCaseAction);
        } catch (e) {
            this.showToast(component, namespace, 'JS Error: ', e.message, 'error');
        }
    },
    getSplitCaseField: function(component, event, helper) {
        var namespace = component.get("v.namespace");
        try {
            var caseManager = component.get('v.ct');
            //component.set("v.showSpinner",true);
            var action = component.get("c.getSplitCaseFields");
            action.setParams({
                "caseManager": caseManager,
                "sessionId": component.get("v.sessionId")
            });
            action.setCallback(this, function(response) {
                //component.set("v.showSpinner",false);
                var state = response.getState();
                if (state === "SUCCESS") {
                    var layoutMeta = response.getReturnValue().sectionFields;
                    component.set("v.layoutMeta", layoutMeta);
                } else {

                    var errorMessage = '';
                    if (state === "INCOMPLETE") {
                        errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
                    } else if (state === "ERROR") {
                        errorMessage = action.getError()[0].message;
                    }
                    this.showToast(component, namespace, 'New Case:', errorMessage, 'error');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            this.showToast(component, namespace, 'JS Error: ', e.message, 'error');
        }
    },
    getAttachmentHelper: function(component, event, helper) {
        var namespace = component.get("v.namespace");
        try {
            var caseManagerId = component.get('v.ct.Id').toString();
            var returnAttachments = component.get("c.getAttachments");
            returnAttachments.setParams({
                caseId: caseManagerId
            });
            returnAttachments.setCallback(this, function(response) {
                var state = response.getState();
                var attachmentList = [];
                if (state === "SUCCESS") {
                    component.set('v.mainCaseAttachments', helper.addIcon(response.getReturnValue().attachments));
                } else {
                    var errorMessage = '';
                    if (state === "INCOMPLETE") {
                        errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
                    } else if (state === "ERROR") {
                        errorMessage = action.getError()[0].message;
                    }
                    this.showToast(component, namespace, 'Split Case:', errorMessage, 'error');
                }
            });
            $A.enqueueAction(returnAttachments);
        } catch (e) {
            this.showToast(component, namespace, 'JS Error: ', e.message, 'error');
        }
    },
    // To add Attachment specific icon 
    addIcon: function(attachments) {
        for (var i = 0; i < attachments.length; i++) {
            var extension = attachments[i].ContentDocument.FileExtension;

            attachments[i]['isPreviewAvailable'] = false;
            var size = attachments[i].ContentDocument.ContentSize;
            if ((size / 1000).toFixed(2) < 1) {
                size = size + ' BYTES';
            } else if (((size / 1000) / 1024).toFixed(2) < 1) {
                size = (size / 1000).toFixed(2) + ' KB';
            } else {
                size = ((size / 1000) / 1024).toFixed(2) + ' MB';
            }

            attachments[i].Size = size;
            if (extension == 'pdf') {
                attachments[i]['icon'] = 'doctype:pdf';
                attachments[i]['isPreviewAvailable'] = true;
            } else if (extension == 'xlsx') {
                attachments[i]['icon'] = 'doctype:excel';

            } else if (extension == 'html') {
                attachments[i]['icon'] = 'doctype:html';
                attachments[i]['isPreviewAvailable'] = true;
            } else if (extension == 'docx' || extension == 'doc') {
                attachments[i]['icon'] = 'doctype:word';

            } else if (extension == 'txt') {
                attachments[i]['icon'] = 'doctype:txt';

            } else if (extension == 'csv') {
                attachments[i]['icon'] = 'doctype:csv';

            } else if (extension == 'jpg' || extension == 'png' || extension == 'gif' || extension == 'jpg') {
                attachments[i]['icon'] = 'doctype:image';
                attachments[i]['isPreviewAvailable'] = true;
            } else {

                attachments[i]['icon'] = 'doctype:attachment';
            }

        }
        return attachments;
    },
    showToast: function(component, nameSpacePerfix, title, message, msgtype) {
        nameSpacePerfix = nameSpacePerfix == '' ? 'c' : 'CoraPLM';
        var tosterError = $A.get("e." + nameSpacePerfix + ":ShowToast");
        tosterError.setParams({
            "title": title,
            "message": message,
            "type": msgtype
        });
        tosterError.fire();
    },
    validate: function(component, event, caseManager, layoutMeta) {
        //var layoutMeta=component.get("v.layoutMeta");
        caseManager['hasError'] = false;
        var showErrorTost = false;
        for (var i = 0; i < layoutMeta.length; i++) {
            var lMeta = layoutMeta[i];
            for (var j = 0; j < lMeta.relatedFields.length; j++) {
                var fieldDetails = lMeta.relatedFields[j];
                if (fieldDetails.fieldBehavior == 'Required' && (caseManager[fieldDetails.fieldAPIName] == undefined || caseManager[fieldDetails.fieldAPIName] == '')) {
                    caseManager['hasError'] = true;
                    showErrorTost = true;

                    if (fieldDetails.fieldType == 'DATETIME') {
                        fieldDetails.errors = 'Complete this field';
                    }
                }

            }
        }
        if (showErrorTost) {
            //component.set("v.layoutMeta",layoutMeta);
            //alert('Please enter value in required fields!');
        }
        //component.set("v.sObject",sObject);
    }
})