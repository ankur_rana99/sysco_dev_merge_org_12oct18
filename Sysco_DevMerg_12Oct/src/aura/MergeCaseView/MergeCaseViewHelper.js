({
	showToast: function(component, nameSpacePerfix, title, message, msgtype){
		nameSpacePerfix=nameSpacePerfix==''?'c':'CoraPLM';
        var tosterError = $A.get("e."+nameSpacePerfix+":ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
	}
})