({
    doInit : function(component, event, helper) {
	//if(component.get('v.showDiv')){
        var checkChosen = component.get('v.isChosen');
        if(checkChosen==true){
            var emailMsgObj = component.get('v.selectedCase.Emails');
            if (emailMsgObj == undefined) {
                component.set('v.oneLineEmail', 'No interaction yet!');
            } else if (emailMsgObj[0].TextBody != undefined) {
                component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
            }
        }else{
            var emailMsgObj = component.get('v.merge[0].Emails');
            if (emailMsgObj == undefined) {
                component.set('v.oneLineEmail', 'No interaction yet!');
            } else if (emailMsgObj[0].TextBody != undefined) {
                component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
            }
        }
	//	}
    },
    
    handlechosenCases : function(component, event, helper){
        var action = event.getParam("action");
        var selectedCase = event.getParam("selectedCase");
        component.set('v.isChosen', true);
        component.set('v.selectedCase',selectedCase);
        var emailMsgObj = component.get('v.selectedCase.Emails');
        if (emailMsgObj == undefined) {
            component.set('v.oneLineEmail', 'No interaction yet!');
        } else if (emailMsgObj[0].TextBody != undefined) {
            component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
        }
    },
    
    closeMergeWindow : function(component, event, helper){
        var compEventRedirect = component.getEvent("caseAction");
        compEventRedirect.setParams({
            "Action": "redirect",
            "Case": component.get("v.merge[0]")
        });
        compEventRedirect.fire();
        var setBreadcrumb = component.getEvent("caseAction");
        setBreadcrumb.setParams({
            "Action" : "setBreadCrumb",
            "TabName" : "My Cases" 
        });
        setBreadcrumb.fire();
    },
	mergeCase : function(component,event,helper){
		var namespace= component.get("v.namespace");
		try{
			var mergeComments = component.get("v.comment");
    
			var commentComponent=component.find("mergeComment");
    
			if(mergeComments==null || mergeComments==''){
				commentComponent.showHelpMessageIfInvalid();
				return false;
			}
			component.set("v.showSpinner",true);
			var mergeItemList = component.get('v.merge');
			var selectedCase = component.get('v.isChosen') ? component.get('v.selectedCase') : component.get('v.merge[0]');
			var childCases=[];
			$.each(mergeItemList,function(index,caseObj){
				if(caseObj.Id!=selectedCase.Id){
					childCases.push(caseObj);
				}
			});
			var action = component.get("c.MergeCase");
				action.setParams({
					"mergeCases" : childCases,
					"mainCase" : selectedCase,
					"comments" : mergeComments
				});
				action.setCallback(this,function(response){
					var state = response.getState();
					component.set("v.showSpinner",false);
					if(state==="SUCCESS"){
						var compEventRedirect = component.getEvent("caseAction");
						compEventRedirect.setParams({
							"Action": "redirect",
							"Case": component.get("v.merge[0]")
						});
						compEventRedirect.fire(); 
    
					  var setBreadcrumb = component.getEvent("caseAction");
						setBreadcrumb.setParams({
							"Action" : "setBreadCrumb",
							 "TabName" : "My Cases" 
						});
						setBreadcrumb.fire();
						


						//START - Showing merge success message on UI
						var appEvent = $A.get("e.c:ShowToast");
						appEvent.setParams({
							"title": "Success ",
							"message": 'Cases Merged successfully!'
						});
						appEvent.fire();
					}
					else{
						var errorMessage ='';
						if (state === "INCOMPLETE"){
							errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
						}else if(state === "ERROR"){
							errorMessage = action.getError()[0].message;
						}
						helper.showToast(component,namespace,'Merge Case:', errorMessage, 'error');
					}
				});
				$A.enqueueAction(action);
		}
		catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
	},
    performMerge : function(component,event,handler){
        var mergeComments = component.get("v.comment");
        //console.log('mergeComments :' +mergeComments);
        if(mergeComments == '' || mergeComments == null){
            alert("Please provide Merge Comments before Merging the Case");
        }
        else{
            var mergeItemList = component.get('v.merge');
            var chosen = component.get('v.isChosen');
            if(chosen==false){
                var selectedCase = component.get('v.merge[0]');
            }else{
                var selectedCase = component.get('v.selectedCase');  
            }
            component.set("v.showSpinner",true);
            var action = component.get("c.performMergeAction");
            action.setParams({
                "mergeList" : mergeItemList,
                "mainCase" : selectedCase,
                "mergeComments" : mergeComments
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                component.set("v.showSpinner",false);
                if(state==="SUCCESS"){
                    var compEventRedirect = component.getEvent("caseAction");
                    compEventRedirect.setParams({
                        "Action": "redirect",
                        "Case": component.get("v.merge[0]")
                    });
                    compEventRedirect.fire(); 
                   
                  var setBreadcrumb = component.getEvent("caseAction");
                    setBreadcrumb.setParams({
                        "Action" : "setBreadCrumb",
                         "TabName" : "My Cases" 
                    });
                    setBreadcrumb.fire();
                    //START - Showing merge success message on UI
                    var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success ",
                        "message": 'Cases Merged successfully!'
                    });
                    appEvent.fire();   
                    //END - Showing merge success message on UI
                    
                }else if (state === "ERROR"){
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }  
    },
	showToast: function(component, nameSpacePerfix, title, message, msgtype){
		nameSpacePerfix=nameSpacePerfix==''?'c':'CoraPLM';
        var tosterError = $A.get("e."+nameSpacePerfix+":ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
    }
})