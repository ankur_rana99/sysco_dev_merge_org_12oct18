/*
Authors: Ashish Kumar
Date created: 18/11/2017
Purpose: This controller will used to handle search result records.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified:  
*/ 
({
    init : function(component, event, helper) {
        // do init
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            // record is loaded (render other component which needs record data value)
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
            component.find("recordLoader").reloadRecord();
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
    
    handleCaseAction: function(component, event, helper) {
        var compEvent = component.getEvent("caseAction");
        var callback = function(response) {}
        compEvent.setParams({
            "Action": "redirectToDetail",
            "Case": component.get("v.simpleRecord")
        });
        compEvent.fire();
    },
})