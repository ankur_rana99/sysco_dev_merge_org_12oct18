/*
* Authors	   :  Atul Hinge
* Date created :  24/10/2017
* Purpose      :  To create new attachment.
* Dependencies :   SelectAttachment.cmp,Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252	
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: Called when component render completely */ 
	doneRendering : function(component, event, helper) {
       // alert(window.location.hostname.split('.')[0]+'--c.eu11.visual.force.com');
         var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        //nameSpace=(nameSpace=='c')?'':nameSpace+'__';
		
		console.log('test'+component.get("v.instanceName"));
		var InstanceName=component.get("v.instanceName");
		console.log('InstanceName'+InstanceName);
        component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com');
		helper.sendToVF(component, event);
   },
    /* Authors: Atul Hinge || Purpose: Called when component inilize */
    doInit : function(component, event, helper) {
        
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
		
		console.log('test'+component.get("v.instanceName"));
		var InstanceName=component.get("v.instanceName");
		console.log('InstanceName'+InstanceName);
         component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com');
		helper.receiveFromVF(component, event);
	},
    /* Authors: Atul Hinge || Purpose: Called when Status changes */
    onStatusChange : function(component, event, helper) {
       // if(component.get("v.status")=="create"){
            helper.sendToVF(component, event);
        //}

	},
})