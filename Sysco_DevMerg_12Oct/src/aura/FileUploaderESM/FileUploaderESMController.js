/*
* Authors	   :  Atul Hinge
* Date created :  24/10/2017
* Purpose      :  To create new attachment.
* Dependencies :   SelectAttachment.cmp,Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252	
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: Called when component render completely */ 
	doneRendering : function(component, event, helper) {
	
       // alert(window.location.hostname.split('.')[0]+'--c.eu11.visual.force.com');
         var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
         console.log(nameSpace + " doneRendering");
		 	var InstanceName=component.get("v.Instance");
        //nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        //component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com');
        if(!component.get('v.isSupplierPortal')){
			component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com');
		}else{
			component.set("v.vfHost",window.location.hostname.split('.')[0]+'.'+InstanceName.toLowerCase()+'.force.com');
		}
		console.log(" doneRendering======="+window.location.hostname.split('.')[0]);

		helper.sendToVF(component, event);
   },
    /* Authors: Atul Hinge || Purpose: Called when component inilize */
    doInit : function(component, event, helper) {
  		// Attachment Issue - Added by Shital Rathod - Start
        // Added By Niza
        console.log('isSupplierPortal--*'+component.get("v.isSupplierPortal"));
        var action = component.get('c.getPortalUserDetails'); 
        action.setCallback(this, function(a){
             console.log('888--*'+a.getReturnValue());
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log('--*'+a.getReturnValue());
                component.set('v.isPortalUser', a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        var fullURL = window.location.href;
        var splitURL = fullURL.split('https://')[1];
        var desiredURL = splitURL.split('/s/')[0];
        console.log('---------'+desiredURL);
        component.set("v.desiredURL",desiredURL);
        console.log('--*'+window.location.href); 
        // End By Niza
       // Attachment Issue - Added by Shital Rathod - End 
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
       console.log("nameSpace "+nameSpace);
	     console.log("ID  "+component.get("v.relatedId"));
		console.log("InstanceName "+component.get("v.Instance"));
		var InstanceName=component.get("v.Instance");
        if(!component.get('v.isSupplierPortal')){
			component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com');
		}else{
			component.set("v.vfHost",window.location.hostname.split('.')[0]+'.'+InstanceName.toLowerCase()+'.force.com');
		}
		helper.receiveFromVF(component, event);
        /* component.set("v.vfHost",window.location.hostname.split('.')[0]+'--'+nameSpace.toLowerCase()+'.'+InstanceName.toLowerCase()+'.visual.force.com'); */
		
	},
    /* Authors: Atul Hinge || Purpose: Called when Status changes */
    onStatusChange : function(component, event, helper) {
     console.log("onStatusChange");
        if(component.get("v.status")=="create"){
            helper.sendToVF(component, event);
        }

	},
})