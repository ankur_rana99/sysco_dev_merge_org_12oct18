({
	 getCaseManagerData: function(component, event) {
	 	console.log(component.get("v.obj.Id"));
		var sectionsJson = component.get("v.layOutJson");
		var sections = [];
		for (i = 0; i < sectionsJson.length; i++) { 
			sections[i] = JSON.stringify(sectionsJson[i].columns);
		}
		console.log(sections);
        var action1 = component.get("c.getCaseManagerDataFromServer");
        //Set the Object parameters and Field Set name 
        action1.setParams({
			objName: component.get("v.objName"),
			currentObject: component.get("v.obj"),
			jsonObj: sections
        });
        action1.setCallback(this, function(response) {
            console.log('response');
			 var spaceList = component.get("v.caseList");
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
                console.log('resChi');
                console.log(response.getReturnValue());
                console.log(response.getReturnValue().DataList);
                console.log(response.getReturnValue().Columns);
				var columns = response.getReturnValue().Columns;
				console.log('columns.length '+columns.length);
				for (var i = 0; i < columns.length; i++) { 
					for (var j = 0; j < sectionsJson.length; j++) { 
						var section = sectionsJson[j];
						for (var k = 0; k < section.columns.length; k++) { 
							if(section.columns[k].name == columns[i].name)
							{
								section.columns[k].metaData = columns[i].metaData;
								break;
							}
						}
					}
				}
				console.log('sectionsJson--'+sectionsJson);
				component.set("v.layoutJsonNew",sectionsJson); 
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("case managerError message: " + 
                            errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            } else {
                alert('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);
    },
	saveObjectData : function(component, event) {
		component.find("recordData").saveRecord(function(saveResult) {
            alert(saveResult.state);
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                alert("save success");
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                // handle the error state
                console.log('Problem saving Quality Record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        });
	},
})