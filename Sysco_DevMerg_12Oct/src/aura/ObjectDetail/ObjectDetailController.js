({
	getCaseManagerSectionDetails: function (component, event, helper) {
		/*component.set("v.objName",event.getParam("evtobjName")); 
		component.set("v.objMetadata",event.getParam("evtobjMetadata")); 
		component.set("v.fieldMetadata",event.getParam("evtfieldMetadata")); 
		component.set("v.layoutSectionsValue",event.getParam("evtlayoutSectionsValue")); 
		component.set("v.recId",event.getParam("evtId")); 
	
		component.set("v.layoutJson",JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["layoutDetailSections"]); */
		helper.getCaseManagerData(component, event);
	
		//JSON.parse(jsonCon["layoutJson"][0]["Config_Json__c"])["PageLayout"]["layoutDetailSections"][0]
	},
	saveObjectData : function(component,events,helper) {
		helper.saveObjectData(component,events);
	},
	init:function (component, event, helper) {
		console.log('EditCase - Init');
		component.set('v.counter',2);
	},
	rerender : function(cmp, helper){
		alert('rerender detail');
	},
	validate: function(component, event, helper) {
		var isValid = true;
		var allCmp = component.find('sectionform');	 
		for(var k in allCmp) {
			allCmp[k].validate(function(value){
				 if (!value) 
				 {
                    isValid = false;
                 }
			});
		}
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (callback) callback(isValid);
	}
})