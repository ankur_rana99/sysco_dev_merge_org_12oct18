({
	doInit: function (component, event, helper) {
		var action = component.get('c.getOpcoCode');
        action.setCallback(this, function(data) {
            component.set("v.opcoCode", data.getReturnValue());
        });
        $A.enqueueAction(action);
	}
})