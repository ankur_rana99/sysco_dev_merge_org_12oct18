<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
    </brand>
    <formFactors>Large</formFactors>
    <label>Exchange</label>
    <navType>Standard</navType>
    <tab>Manager</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Exchange_UtilityBar</utilityBar>
</CustomApplication>
